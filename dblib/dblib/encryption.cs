﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Security;
using System.Security.Cryptography;
using FirebirdSql.Data.FirebirdClient;
using FirebirdSql.Data.Isql;


namespace dblib
{


    public class version
    {
        /*
        Version control
         * major.minor[.build[.revision]]
         * Last updated:  Max - 10.11.2014
         */
        public static string GetLibVersion()
        {
            return "0.0.0.4";
        }
    }

    public class Encryption
    {
        public static void EncryptString(string inputString, string outputFile, string skey)
        {
            try
            {
                using (RijndaelManaged aes = new RijndaelManaged())
                {
                    byte[] key = ASCIIEncoding.UTF8.GetBytes(skey);
                    byte[] IV = ASCIIEncoding.UTF8.GetBytes("5635655800633252");

                    using (FileStream fsCrypt = new FileStream(outputFile, FileMode.Create))
                    {
                        using (ICryptoTransform encryptor = aes.CreateEncryptor(key, IV))
                        {
                            using (CryptoStream cs = new CryptoStream(fsCrypt, encryptor, CryptoStreamMode.Write))
                            {
                                int data;
                                int i = 0;
                                while ((i != inputString.Length))
                                {
                                    data = (byte)inputString[i];
                                    cs.WriteByte((byte)data);
                                    i++;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
               /* try
                {
                    if (!File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\ECZ\\errorlog.txt"))
                    {
                        Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\ECZ");
                        File.Create(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\ECZ\\errorlog.txt");
                    }
                    using (StreamWriter w = File.AppendText(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\ECZ\\errorlog.txt"))
                    {
                        w.WriteLine(DateTime.Now.ToLongTimeString(),
                            DateTime.Now.ToLongDateString());
                        w.WriteLine("Error in dblib.dll while trying to decrypt into file: " + ex.Message);
                    }
                }
                catch
                { }*/
            }
        }

        public static string DecryptToString(string location, string skey)
        {
            string output = "";
            try
            {
                using (RijndaelManaged aes = new RijndaelManaged())
                {
                    byte[] key = ASCIIEncoding.UTF8.GetBytes(skey);
                    byte[] IV = ASCIIEncoding.UTF8.GetBytes("5635655800633252");

                    using (FileStream fsCrypt = new FileStream(location, FileMode.Open, FileAccess.Read))
                    {
                        using (ICryptoTransform decryptor = aes.CreateDecryptor(key, IV))
                        {
                            using (CryptoStream cs = new CryptoStream(fsCrypt, decryptor, CryptoStreamMode.Read))
                            {
                                int data;
                                while ((data = cs.ReadByte()) != -1)
                                {
                                    output += Convert.ToChar(data);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                try
                {
                   /* if (!File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\ECZ\\errorlog.txt"))
                    {
                        Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\ECZ");
                        File.Create(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\ECZ\\errorlog.txt");
                    }
                    using (StreamWriter w = File.AppendText(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\ECZ\\errorlog.txt"))
                    {
                        w.WriteLine(DateTime.Now.ToLongTimeString(),
                            DateTime.Now.ToLongDateString());
                        w.WriteLine("Error in dblib.dll while trying to decrypt into file: " + ex.Message);
                        w.WriteLine(ex.ToString());

                    }*/
                }
                catch
                { }
            }
            return output;
        }

        public static void DecryptToFile(string inputFile, string outputFile, string skey)
        {
            try
            {
                using (RijndaelManaged aes = new RijndaelManaged())
                {
                    byte[] key = ASCIIEncoding.UTF8.GetBytes(skey);
                    byte[] IV = ASCIIEncoding.UTF8.GetBytes("5635655800633252");

                    using (FileStream fsCrypt = new FileStream(inputFile, FileMode.Open, FileAccess.Read))
                    {
                        using (FileStream fsOut = new FileStream(outputFile, FileMode.Create))
                        {
                            using (ICryptoTransform decryptor = aes.CreateDecryptor(key, IV))
                            {
                                using (CryptoStream cs = new CryptoStream(fsCrypt, decryptor, CryptoStreamMode.Read))
                                {
                                    int data;
                                    while ((data = cs.ReadByte()) != -1)
                                    {
                                        fsOut.WriteByte((byte)data);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                /*try
                {
                    if (!File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\ECZ\\errorlog.txt"))
                    {
                        Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\ECZ");
                        File.Create(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\ECZ\\errorlog.txt");
                    }
                    using (StreamWriter w = File.AppendText(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\ECZ\\errorlog.txt"))
                    {
                        w.WriteLine(DateTime.Now.ToLongTimeString(),
                            DateTime.Now.ToLongDateString());
                        w.WriteLine("Error in dblib.dll while trying to decrypt into file: " + ex.Message);
                        w.WriteLine(ex.ToString());

                    }
                }
                catch { }*/
            }
        }
    }

    public class OldEncryption
    {
        private static char decode(int s)
        {
             char result = ' ';

            switch (s)
            {
                case 0: result = '1'; break;
                case 1: result = '2'; break;
                case 2: result = '3'; break;
                case 3: result = '4'; break;
                case 4: result = '5'; break;
                case 5: result = '6'; break;
                case 6: result = '7'; break;
                case 7: result = '8'; break;
                case 8: result = '9'; break;
                case 9: result = '0'; break;
                case 10: result = 'q'; break;
                case 11: result = 'w'; break;
                case 12: result = 'e'; break;
                case 13: result = 'r'; break;
                case 14: result = 't'; break;
                case 15: result = 'y'; break;
                case 17: result = 'i'; break;
                case 18: result = 'o'; break;
                case 19: result = 'p'; break;
                case 20: result = 'a'; break;
                case 21: result = 's'; break;
                case 22: result = 'd'; break;
                case 23: result = 'f'; break;
                case 25: result = 'h'; break;
                case 26: result = 'j'; break;
                case 27: result = 'k'; break;
                case 28: result = 'l'; break;
                case 29: result = 'z'; break;
                case 30: result = 'x'; break;
                case 31: result = 'c'; break;
                case 33: result = 'b'; break;
                case 34: result = 'n'; break;
                case 35: result = 'm'; break;
                case 36: result = '('; break;
                case 37: result = ')'; break;
                case 38: result = '_'; break;
                case 39: result = '-'; break;
                case 41: result = '+'; break;
                case 42: result = 'Q'; break;
                case 43: result = 'W'; break;
                case 44: result = 'E'; break;
                case 45: result = 'R'; break;
                case 46: result = 'T'; break;
                case 47: result = 'Y'; break;
                case 49: result = 'I'; break;
                case 50: result = 'O'; break;
                case 51: result = 'P'; break;
                case 52: result = '['; break;
                case 53: result = ']'; break;
                case 54: result = 'A'; break;
                case 55: result = 'S'; break;
                case 57: result = 'F'; break;
                case 58: result = 'G'; break;
                case 59: result = 'H'; break;
                case 60: result = 'J'; break;
                case 61: result = 'K'; break;
                case 62: result = 'L'; break;
                case 63: result = 'Z'; break;
                case 65: result = 'C'; break;
                case 66: result = 'V'; break;
                case 67: result = 'B'; break;
                case 68: result = 'N'; break;
                case 69: result = 'M'; break;
                case 70: result = '!'; break;
                case 71: result = '@'; break;
                case 73: result = '$'; break;
                case 74: result = '%'; break;
                case 75: result = '^'; break;
                case 76: result = '&'; break;
                case 77: result = '*'; break;
                case 78: result = '?'; break;
                case 79: result = 'л'; break;
                case 81: result = '<'; break;
                case 82: result = '>'; break;
                case 83: result = '~'; break;
                case 84: result = '|'; break;
                case 85: result = '\\'; break;
                case 86: result = '/'; break;
                case 87: result = '{'; break;
                case 89: result = '"'; break;
                case 90: result = ':'; break;
                case 91: result = ';'; break;
                case 92: result = '`'; break;
                case 93: result = '№'; break;
                case 94: result = 'ф'; break;
                case 95: result = 'ы'; break;
                case 97: result = 'а'; break;
                case 98: result = 'п'; break;
                case 99: result = 'р'; break;
                case 100: result = 'о'; break;
                case 101: result = '·'; break;
                case 102: result = '¤'; break;
                case 103: result = 'ж'; break;
                case 105: result = 'ъ'; break;
                case 106: result = 'х'; break;
                case 107: result = 'з'; break;
                case 108: result = 'щ'; break;
                case 109: result = 'ш'; break;
                case 110: result = 'г'; break;
                case 111: result = 'н'; break;
                case 113: result = 'к'; break;
                case 114: result = 'у'; break;
                case 115: result = 'ц'; break;
                case 116: result = 'й'; break;
                case 117: result = 'я'; break;
                case 118: result = 'ч'; break;
                case 119: result = 'с'; break;
                case 121: result = 'и'; break;
                case 122: result = 'т'; break;
                case 123: result = 'ь'; break;
                case 124: result = 'б'; break;
                case 125: result = 'ю'; break;
                case 126: result = 'Й'; break;
                case 127: result = 'Ц'; break;
                case 129: result = 'К'; break;
                case 130: result = 'Е'; break;
                case 131: result = 'Н'; break;
                case 132: result = 'Г'; break;
                case 133: result = 'Ш'; break;
                case 134: result = 'Щ'; break;
                case 135: result = 'З'; break;
                case 137: result = 'Ъ'; break;
                case 138: result = 'Ю'; break;
                case 139: result = 'Б'; break;
                case 140: result = 'Ь'; break;
                case 141: result = 'Т'; break;
                case 142: result = 'И'; break;
                case 143: result = 'М'; break;
                case 145: result = 'Ч'; break;
                case 146: result = 'Я'; break;
                case 147: result = 'Э'; break;
                case 148: result = 'Ж'; break;
                case 149: result = 'Д'; break;
                case 150: result = 'Л'; break;
                case 151: result = 'О'; break;
                case 153: result = 'П'; break;
                case 154: result = 'А'; break;
                case 155: result = 'В'; break;
                case 156: result = 'Ы'; break;
                case 157: result = 'Ф'; break;
                case 158: result = '.'; break;
                case 159: result = ','; break;
                case 161: result = 'ё'; break;
                case 162: result = 'Є'; break;
                case 163: result = 'є'; break;
                case 164: result = 'Ї'; break;
                case 165: result = 'ї'; break;
                case 166: result = 'Ў'; break;
                case 167: result = 'ў'; break;
                case 169: result = '•'; break;
            }

            return result;
        }

        public static string OldDecryptFile(string inputFile)
        {
            string path = "";
            string[] input1 = File.ReadAllLines(inputFile);
            for (int i = 1; i < input1[0].Length / 3; i++)
            {
                path += decode(Convert.ToInt32(input1[0].Substring(i * 3 - 3, 3)) - i);
            }
            return path;
        }
    }

        public class databaseConnection : IDisposable
        {
            private FbConnection con;
            private string connectionString;
            public FbCommand command = new FbCommand();

            public databaseConnection(string connectionString)
            {
                this.connectionString = connectionString;
            }

            public void AddSql(string SQL)
            {
                this.OpenConnection();
                this.command.CommandText = SQL;
                this.command.Connection = this.con;
            }
            public FbDataReader getFromDatabase()
            {
                return this.command.ExecuteReader();
            }

            
            public void OpenConnection()
            {
                if (con == null )
                {
                    con = new FbConnection(this.connectionString);
                    con.Open();
                }
            }
            public void CloseConnection()
            {
                if (con != null)
                { 
                    con.Close();
                }
            }

            public void addParam(int param)
            {
                this.command.Parameters.Add("@m" + command.Parameters.Count, param);
            }

            public void addParam(string param)
            {
                this.command.Parameters.Add("@m" + command.Parameters.Count, param);
            }
            public FbDataReader getFromDatabase(string SQL)
            {
                OpenConnection();

                //command = new FbCommand(SQL, con);
                command.CommandText = SQL;
                command.Connection = con;
                FbDataReader result = command.ExecuteReader();

                return result;
            }

            public Boolean IsWorking()
            {
                if (this.con.State == System.Data.ConnectionState.Open)
                { 
                    return true;
                }
                return false;
            }

            public Boolean insertIntoDatabase(string SQL)
            {
                OpenConnection();

                FbCommand cmd = new FbCommand(SQL, con);
                con.Open();
                try
                {
                    cmd.ExecuteNonQuery();
                    return true;
                }
                catch
                {
                    return false;
                }

            }


            public void Dispose()
            {
                con.Close();
            }
        }
}
