﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraBars.Helpers;
using DevExpress.XtraBars;
using DevExpress.XtraSpreadsheet;
using DevExpress.Spreadsheet;
using System.Threading;
using System.Collections.Concurrent;
using System.Drawing.Drawing2D;
using System.Collections;
using System.Threading.Tasks;

namespace SpreadEditor
{
    public partial class Spread : UserControl
    {
        private List<string> macroses { get; set; }
        private DataSet MyData = new DataSet();
        public Color TextBaseColor = Color.Black;
        public Color MacroColor = Color.FromArgb(255, 174, 201);
        public DevExpress.Spreadsheet.IWorkbook WorkBook { get; set; }
        public DevExpress.Spreadsheet.Worksheet LastSheet { get; set; }
        //private Thread tr;
        //public delegate void UPD();
       // public UPD UpdateCell;
        public static DevExpress.Spreadsheet.SearchOptions SearchOptions;
        public static bool isSearchOptionsInitiated = false;
        Brush MacroBrush = new SolidBrush(Color.FromArgb(50, 255, 174, 201));       
        public static void InitSearchOptions()
        {
            if (isSearchOptionsInitiated)
                return;
            isSearchOptionsInitiated = true;
            SearchOptions = new DevExpress.Spreadsheet.SearchOptions();
            SearchOptions.MatchCase = true;
            SearchOptions.SearchIn = SearchIn.ValuesAndFormulas;
            SearchOptions.SearchBy = SearchBy.Columns | SearchBy.Rows;
            SearchOptions.MatchEntireCellContents = false;
        }
        public Spread()
        {
            InitializeComponent();
            InitSkinGallery();
            InitParameters();   
        }
        public static bool vis = false;
        public void MacroVision(bool vision = true)
        {
            vis = vision;
            spreadsheetControl.CustomDrawCellBackground -= CustomDrawCellBackground;
            spreadsheetControl.ActiveSheetChanged -= ActiveSheetChanged;
            spreadsheetControl.CellEndEdit -= CellEndEdit;
            spreadsheetControl.DocumentLoaded -= DocumentLoaded;
            if (vision)
            {
                InitSearchOptions();
                spreadsheetControl.CustomDrawCellBackground += CustomDrawCellBackground;
                spreadsheetControl.ActiveSheetChanged += ActiveSheetChanged;
                spreadsheetControl.CellEndEdit += CellEndEdit;
                spreadsheetControl.DocumentLoaded += DocumentLoaded;
                CustomDrawableMacroCellOptions.RescanActiveSheetCollection(spreadsheetControl.ActiveWorksheet);
            }
            else
                CustomDrawableMacroCellOptions.collection.Clear();
        }

        private void DocumentLoaded(object sender, EventArgs e)
        {
            CustomDrawableMacroCellOptions.RescanActiveSheetCollection(spreadsheetControl.ActiveWorksheet);
        }

        private void ActiveSheetChanged(object sender, ActiveSheetChangedEventArgs e)
        {
            InitSearchOptions();
            CustomDrawableMacroCellOptions.RescanActiveSheetCollection(spreadsheetControl.ActiveWorksheet);
        }

        private void CustomDrawCellBackground(object sender, CustomDrawCellBackgroundEventArgs e)
        {
            if (!spreadsheetControl.VisibleRange.Contains(e.Cell)||!CustomDrawableMacroCellOptions.collection.isContainsCell(e.Cell))
                return;
            e.Graphics.SmoothingMode = SmoothingMode.HighSpeed;
            e.Graphics.FillRectangle(MacroBrush, e.Bounds);
        }

        public DevExpress.Spreadsheet.Worksheet GetCurrentSheet()
        {
            try { WorkBook.BeginUpdate(); LastSheet = WorkBook.Worksheets.ActiveWorksheet; }
            finally { WorkBook.EndUpdate(); }
            return LastSheet;
        }
        public DevExpress.Spreadsheet.Worksheet GetSheet(int index = 0)
        {
            try { WorkBook.BeginUpdate(); LastSheet = WorkBook.Worksheets[index]; }
            finally { WorkBook.EndUpdate(); }
            return LastSheet;
        }
        public DevExpress.Spreadsheet.Worksheet GetSheet(string name)
        {
            DevExpress.Spreadsheet.Worksheet sheet;
            try { WorkBook.BeginUpdate(); sheet = WorkBook.Worksheets[name]; }
            finally { WorkBook.EndUpdate(); }
            return sheet;
        }
        public DevExpress.Spreadsheet.Range GetRange(DevExpress.Spreadsheet.Worksheet sheet,int left,int top,int right,int bottom)
        {
            if (sheet == null)
                return null;
            return sheet.Range.FromLTRB(left, top, right, bottom);
        }
        public DevExpress.Spreadsheet.Cell GetCell(DevExpress.Spreadsheet.Range range,int row,int column)
        {
            if (range == null)
                return null;
            return range[row, column];
        }
        
        private void InitParameters()
        {
            WorkBook = spreadsheetControl.Document;
            WorkBook.DocumentSettings.R1C1ReferenceStyle = false;
        }

        void InitSkinGallery()
        {
            SkinHelper.InitSkinGallery(rgbiSkins, true);
        }
        public void SetMacroColor()
        {
            if (macroses == null)
                return;
            SearchOptions options = new SearchOptions();
            options.MatchCase = true;
        }
           
        public void SetMacroses(List<string> lst, List<string> listAdded = null)
        {
           /* macroses = lst;
            if (listAdded != null)
                foreach (string item in listAdded)
                    macroses.Insert(0, item);
            UpdateCell = new UPD(CellUpdater);
            tr = new Thread(()=> ScanPacka());
            tr.Start();*/
        }
        public class NewData
        {
            public string Macro;
            public string[] Data;

            public NewData(string Macro)
            {
                this.Macro = Macro;
            }
            public void SetData(string data)
            {
                if (Data == null)
                {
                    Data = new string[1];
                    Data[1] = data;
                }
                else
                {
                    Array.Resize(ref Data, Data.Count() + 1);
                    Data[Data.Length - 1] = data;
                }
            }
        }
       // List<CellPacka> Packa = new List<CellPacka>();
        private void CellUpdater()
        {
            /*
            spreadsheetControl.CellEndEdit -= CellEndEdit;
            Packa[0].cell.BeginUpdate();
            if (!Packa[0].MacroContains)
            {
                if (Packa[0].cell.Font.Color == MacroColor)
                    Packa[0].cell.Font.Color = TextBaseColor;
            }
            else
            {
                if (Packa[0].cell.Font.Color != MacroColor)
                    Packa[0].cell.Font.Color = MacroColor;
            }
            Packa[0].cell.EndUpdate();
            spreadsheetControl.CellEndEdit += CellEndEdit;
           // spreadsheetControl.CellValueChanged += new DevExpress.XtraSpreadsheet.CellValueChangedEventHandler(this.spreadsheetControl_CellValueChanged);
            Packa.RemoveAt(0);*/
        }

        private void CellEndEdit(object sender, SpreadsheetCellValidatingEventArgs e)
        {
            new Task(() =>
            {
                Thread.Sleep(100);
                var item = new CustomDrawableMacroCellOptions.CustomDrawableMacroCell(e.Cell);
                if ((!e.Cell.HasFormula && e.Cell.DisplayText.Contains("{")) || e.Cell.Formula.Contains("{"))
                {
                    CustomDrawableMacroCellOptions.collection.AddItem(item);
                }
                else
                    CustomDrawableMacroCellOptions.collection.RemoveItem(item);
            }).Start();
          
        }

        private void ScanPacka()
        {    
          /*  while (true)
            {
                if (Packa.Count > 0)
                {
                    for (int j = 0; j < macroses.Count; j++)
                    {
                        if (Packa[0].cell.DisplayText.Contains(macroses[j]))
                        {
                            Packa[0].isMacro();
                            break;
                        }
                    }
                    this.Invoke(this.UpdateCell);                    
                }               
                if (Packa.Count == 0)
                    Thread.Sleep(125);
            }*/
        }
        private void spreadsheetControl_CellValueChanged(object sender, SpreadsheetCellEventArgs e)
        {            
            //e.Cell.Style.Font.Color = Color.Green;           
        }
        private class CellPacka:List<CellPacka>
        {
           public bool MacroContains=false;
           public Cell cell;
            public CellPacka(Cell c)
            {
                cell = c;
            }
            public void isMacro()
            {
                MacroContains = true;
            }           
        }

        private void barButtonItem1_ItemClick(object sender, ItemClickEventArgs e)
        {
            Cell cell = spreadsheetControl.ActiveCell;
            cell.Alignment.RotationAngle = 0;
            /*spreadsheetControl.ActiveCell.BeginUpdate();
            var formatting = spreadsheetControl.ActiveCell.BeginUpdateFormatting();
            spreadsheetControl.ActiveCell.Style.Alignment.RotationAngle = 90;
            spreadsheetControl.ActiveCell.Alignment.RotationAngle = 90;
            formatting.Alignment.RotationAngle = 90;
            spreadsheetControl.ActiveCell.EndUpdateFormatting(formatting);
            spreadsheetControl.ActiveCell.AutoFitRows();
            spreadsheetControl.ActiveCell.EndUpdate();*/
        }

        private void spreadsheetControl_SelectionChanged(object sender, EventArgs e)
        {
            
        }

        private void barButtonItem3_ItemClick(object sender, ItemClickEventArgs e)
        {
            try { WorkBook.BeginUpdate();
                WorkBook.Worksheets.ActiveWorksheet.ActiveView.PaperKind=System.Drawing.Printing.PaperKind.A3; }
            finally { WorkBook.EndUpdate(); }
        }
    }
    public static class CustomDrawSpreadExtensions
    {
        public static void Clear(this BlockingCollection<CustomDrawableMacroCellOptions.CustomDrawableMacroCell> blockingCollection)
        {
            if (blockingCollection == null)
            {
                throw new ArgumentNullException("blockingCollection");
            }           
            while (blockingCollection.Count > 0)
            {
                CustomDrawableMacroCellOptions.CustomDrawableMacroCell item;
                blockingCollection.TryTake(out item);
            }
        }
        public static bool isContainsCell(this BlockingCollection<CustomDrawableMacroCellOptions.CustomDrawableMacroCell> blockingCollection, Cell cell)
        {
            return blockingCollection.Where(i => i.ContainCell(cell)).FirstOrDefault() != null;
        }
        public static void RemoveItem(this BlockingCollection<CustomDrawableMacroCellOptions.CustomDrawableMacroCell> blockingCollection, CustomDrawableMacroCellOptions.CustomDrawableMacroCell item)
        {
            if (blockingCollection == null)
            {
                throw new ArgumentNullException("blockingCollection");
            }
            if (!blockingCollection.Contains(item))
                return;
           var itemCollection=blockingCollection.TakeWhile(i => !i.Equals(item));
            blockingCollection.Clear();
            foreach (var exist in itemCollection)
                blockingCollection.AddItem(exist, false);
        }
        public static void AddItem(this BlockingCollection<CustomDrawableMacroCellOptions.CustomDrawableMacroCell> blockingCollection, CustomDrawableMacroCellOptions.CustomDrawableMacroCell item, bool CheckSafe=true)
        {
            if (blockingCollection == null)
            {
                throw new ArgumentNullException("blockingCollection");
            }
            if (CheckSafe)
            {
                if (!blockingCollection.Contains(item))
                    blockingCollection.TryAdd(item);
            }
            else
                blockingCollection.TryAdd(item);
        }
    }
    public class CustomDrawableMacroCellOptions
    {       
        public static Color macroColor = Color.FromArgb(204, 51, 0);
        public static BlockingCollection<CustomDrawableMacroCell> collection = new BlockingCollection<CustomDrawableMacroCell>();       
        public static void RescanActiveSheetCollection(Worksheet scan)
        {
            collection.Clear();
            if (!Spread.vis)
                return;
            //Interlocked
            foreach (var item in scan.Search("{", Spread.SearchOptions).Select(i => new CustomDrawableMacroCell(i)))            
                collection.TryAdd(item);
        }        
#pragma warning disable CS0659 // Тип переопределяет Object.Equals(object o), но не переопределяет Object.GetHashCode()
        public class CustomDrawableMacroCell
#pragma warning restore CS0659 // Тип переопределяет Object.Equals(object o), но не переопределяет Object.GetHashCode()
        {
            private Cell cell; 
            public CustomDrawableMacroCell(Cell cell)
            {
                this.cell=cell;
            }
            public bool ContainCell(Cell cell)
            {
                return this.cell.RowIndex == cell.RowIndex && this.cell.ColumnIndex == cell.ColumnIndex;
            }
            public override bool Equals(object obj)
            {
                var item = obj as CustomDrawableMacroCell;

                if (item == null)
                {
                    return false;
                }

                return item.cell.RowIndex==cell.RowIndex&&item.cell.ColumnIndex==cell.ColumnIndex;
            }         
        }
    }
}