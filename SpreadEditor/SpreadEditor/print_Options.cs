﻿using DevExpress.Spreadsheet;
using System;
using System.Windows.Forms;

namespace SpreadEditor
{
    public partial class print_Options : Form
    {
        public delegate void OptionsHandler(PrintOptions Options);
        public event OptionsHandler SetPrintOptions;
        PrintOptions Options;
        public print_Options(Worksheet sheet)
        {
            InitializeComponent();
            Options = new PrintOptions(sheet);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {            
            //Options.printOptions.;
            SetPrintOptions?.Invoke(Options);
            this.Close();
        }
    }
    public class PrintOptions
    {
        public WorksheetPrintOptions printOptions;
        private Worksheet sheet;
        public PrintOptions(Worksheet sheet)
        {
            this.sheet = sheet;
            printOptions = sheet.PrintOptions;
        }
    }
}
