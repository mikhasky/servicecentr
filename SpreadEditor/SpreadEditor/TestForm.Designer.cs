﻿namespace SpreadEditor
{
    partial class TestForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.spread1 = new SpreadEditor.Spread();
            this.SuspendLayout();
            // 
            // spread1
            // 
            this.spread1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spread1.LastSheet = null;
            this.spread1.Location = new System.Drawing.Point(0, 0);
            this.spread1.Name = "spread1";
            this.spread1.Size = new System.Drawing.Size(1022, 552);
            this.spread1.TabIndex = 0;
            // 
            // TestForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1022, 552);
            this.Controls.Add(this.spread1);
            this.Name = "TestForm";
            this.Text = "TestForm";
            this.ResumeLayout(false);

        }

        #endregion

        private Spread spread1;
    }
}