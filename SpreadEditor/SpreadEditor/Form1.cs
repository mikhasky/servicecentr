﻿using DevExpress.XtraBars.Helpers;
using DevExpress.XtraBars.Ribbon;
//using Formatting = DevExpress.Spreadsheet.Formatting;
namespace SpreadEditor
{
    public partial class Form1 : RibbonForm
    {
        public DevExpress.Spreadsheet.IWorkbook WorkBook { get; set; }
        public DevExpress.Spreadsheet.Worksheet LastSheet { get; set; }
        public Form1()
        {
            InitializeComponent();
            InitSkinGallery();
            InitParameters();
            //Worksheet worksheet = WorkBook.Worksheets[0];
            /*Cell c1 = spreadsheetControl.Document.Worksheets[0].Cells[1, 1];
            Cell c2 = spreadsheetControl.Document.Worksheets[0].Cells[2, 2];
            Cell c3 = spreadsheetControl.Document.Worksheets[0].Cells[3, 3];
            c1.Value = "лол";
            c2.Value = "тест";
            c3.Value = "ывоала";
            c1.Font.Color = Color.Blue;
            c2.Font.Color = Color.Choc*/  

        }
        public DevExpress.Spreadsheet.Worksheet GetSheet(int index = 0)
        {
            try { WorkBook.BeginUpdate(); LastSheet = WorkBook.Worksheets[index]; }
            finally { WorkBook.EndUpdate(); }
            return LastSheet;
        }
        public DevExpress.Spreadsheet.Worksheet GetSheet(string name)
        {
            DevExpress.Spreadsheet.Worksheet sheet;
            try { WorkBook.BeginUpdate(); sheet = WorkBook.Worksheets[name]; }
            finally { WorkBook.EndUpdate(); }
            return sheet;
        }
        public DevExpress.Spreadsheet.Range GetRange(DevExpress.Spreadsheet.Worksheet sheet,int left,int top,int right,int bottom)
        {
            if (sheet == null)
                return null;
            return sheet.Range.FromLTRB(left, top, right, bottom);
        }
        public DevExpress.Spreadsheet.Cell GetCell(DevExpress.Spreadsheet.Range range,int row,int column)
        {
            if (range == null)
                return null;
            return range[row, column];
        }
        
        private void InitParameters()
        {
            WorkBook = spreadsheetControl.Document;
            WorkBook.DocumentSettings.R1C1ReferenceStyle = true;
           
        }

        void InitSkinGallery()
        {
            SkinHelper.InitSkinGallery(rgbiSkins, true);
        }

    }
}