﻿using System;
using System.Collections;
using System.Windows.Forms;

namespace MKP.Helper.ListView
{
    /// <summary>
    /// This class is an implementation of the 'IComparer' interface.
    /// </summary>
    public class CListViewColumnSorter : IComparer
    {
        /// <summary>
        /// Specifies the column to be sorted
        /// </summary>
        private int ColumnToSort;
        /// <summary>
        /// Specifies the order in which to sort (i.e. 'Ascending').
        /// </summary>
        private SortOrder OrderOfSort;
        /// <summary>
        /// Case insensitive comparer object
        /// </summary>
        private CaseInsensitiveComparer ObjectCompare;

        public ESortType SortType
        {
            get; set;
        } = ESortType.Text;

        /// <summary>
        /// Class constructor.  Initializes various elements
        /// </summary>
        public CListViewColumnSorter()
        {
            // Initialize the column to '0'
            ColumnToSort = 0;

            // Initialize the sort order to 'none'
            OrderOfSort = SortOrder.None;

            // Initialize the CaseInsensitiveComparer object
            ObjectCompare = new CaseInsensitiveComparer();
        }

        /// <summary>
        /// This method is inherited from the IComparer interface.  It compares the two objects passed using a case insensitive comparison.
        /// </summary>
        /// <param name="x">First object to be compared</param>
        /// <param name="y">Second object to be compared</param>
        /// <returns>The result of the comparison. "0" if equal, negative if 'x' is less than 'y' and positive if 'x' is greater than 'y'</returns>
        public int Compare(object x, object y)
        {
            int compareResult = 0;
            ListViewItem listviewX, listviewY;

            // Cast the objects to be compared to ListViewItem objects
            listviewX = (ListViewItem)x;
            listviewY = (ListViewItem)y;

            // Compare the two items
            switch (SortType) {
                case ESortType.Text:
                    compareResult = ObjectCompare.Compare(listviewX.SubItems[ColumnToSort].Text, listviewY.SubItems[ColumnToSort].Text);
                    break;
                case ESortType.Numeric:
                    try
                    {
                        // Parse the two objects passed as a parameter as a double.
                        double val1 = double.Parse(listviewX.SubItems[ColumnToSort].Text);
                        double val2 = double.Parse(listviewY.SubItems[ColumnToSort].Text);
                        compareResult = ObjectCompare.Compare(val1, val2);
                    }
                    // If neither compared object has a valid date format, compare
                    // as a string.
                    catch
                    {
                        // Compare the two items as a string.
                        compareResult = String.Compare(((ListViewItem)x).SubItems[ColumnToSort].Text,
                                    ((ListViewItem)y).SubItems[ColumnToSort].Text);
                    }
                    
                    break;
                case ESortType.Date:
                    try
                    {
                        // Parse the two objects passed as a parameter as a DateTime.
                        DateTime _val1 = DateTime.Parse(listviewX.SubItems[ColumnToSort].Text);
                        DateTime _val2 = DateTime.Parse(listviewY.SubItems[ColumnToSort].Text);
                        compareResult = ObjectCompare.Compare(_val1, _val2);
                    }
                    // If neither compared object has a valid date format, compare
                    // as a string.
                    catch
                    {
                        // Compare the two items as a string.
                        compareResult = String.Compare(((ListViewItem)x).SubItems[ColumnToSort].Text,
                                    ((ListViewItem)y).SubItems[ColumnToSort].Text);
                    }
                    break;
            }
            // Calculate correct return value based on object comparison
            if (OrderOfSort == SortOrder.Ascending)
            {
                // Ascending sort is selected, return normal result of compare operation
                return compareResult;
            }
            else if (OrderOfSort == SortOrder.Descending)
            {
                // Descending sort is selected, return negative result of compare operation
                return (-compareResult);
            }
            else
            {
                // Return '0' to indicate they are equal
                return 0;
            }
        }

        /// <summary>
        /// Gets or sets the number of the column to which to apply the sorting operation (Defaults to '0').
        /// </summary>
        public int SortColumn
        {
            set
            {
                ColumnToSort = value;
            }
            get
            {
                return ColumnToSort;
            }
        }

        /// <summary>
        /// Gets or sets the order of sorting to apply (for example, 'Ascending' or 'Descending').
        /// </summary>
        public SortOrder Order
        {
            set
            {
                OrderOfSort = value;
            }
            get
            {
                return OrderOfSort;
            }
        }

    }

    /// <summary>
    /// Сортировать по какому типу
    /// </summary>
    public enum ESortType
    {
        Text,
        Numeric,
        Date
    }
}
