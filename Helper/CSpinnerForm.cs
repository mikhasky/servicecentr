﻿using System;
using System.Windows.Forms;
using System.Threading;

namespace MKP.Helper
{
    /// <summary>
    /// событие на ошибку при выполнение со спиннером
    /// </summary>
    /// <param name="sender">обьект</param>
    /// <param name="sender">Ошибка при выполнении</param>
    public delegate void ErrorRunWithSpinnerEventHandler(object sender, Exception error);

    /// <summary>
    /// Форма с лоадером
    /// </summary>
    public class CSpinnerForm : Form, Spinner.ICSpinne
    {
        const int SLEEP_SECONDS = 1000;

        #region Props
        /// <summary>
        /// запущен ли спиннер
        /// </summary>
        public bool IsSpinnerRunning
        {
            get; private set;
        }
        /// <summary>
        /// Текст на спинере
        /// </summary>
        public string SpinnerText
        {
            get; set;
        }
        /// <summary>
        /// Текст описания на спинере
        /// </summary>
        public string SpinnerDescriptionText
        {
            get; set;
        }
        #endregion

        #region Event
        /// <summary>
        /// НА ошибку при выполнениии со спинером
        /// </summary>
        public event ErrorRunWithSpinnerEventHandler OnErrorRunWithSpinner;
        #endregion

        #region Methods
        /// <summary>
        /// Выполнить екшен в спинере
        /// </summary>
        /// <param name="action"></param>
        /// <param name="showProgressBar"></param>
        public void Run(Action action)
        {
            if(IsSpinnerRunning)
            {
                action();
                return;
            }

            IsSpinnerRunning = true;
            MKP.Controls.WhaitForm.FormLoad.ShowFormLoad(this, SpinnerText, SpinnerDescriptionText);
            try
            {
                Thread.Sleep(SLEEP_SECONDS);
                action();
            }catch(Exception error)
            {
                OnErrorRunWithSpinner?.Invoke(this, error);
            }
            finally
            {
                MKP.Controls.WhaitForm.FormLoad.CloseFormLoad();
                IsSpinnerRunning = false;
            }
        }
        #endregion
    }
    /// <summary>
    /// Контрол с лоадером
    /// </summary>
    public class CSpinnerControl : UserControl, Spinner.ICSpinne
    {
        public const int SLEEP_SECONDS = 1000;

        #region Props
        /// <summary>
        /// запущен ли спиннер
        /// </summary>
        public bool IsSpinnerRunning
        {
            get; private set;
        }
        /// <summary>
        /// Текст на спинере
        /// </summary>
        public string SpinnerText
        {
            get; set;
        }
        /// <summary>
        /// Текст описания на спинере
        /// </summary>
        public string SpinnerDescriptionText
        {
            get; set;
        }
        #endregion

        #region Event
        /// <summary>
        /// НА ошибку при выполнениии со спинером
        /// </summary>
        public event ErrorRunWithSpinnerEventHandler OnErrorRunWithSpinner;
        #endregion

        #region Methods
        /// <summary>
        /// выполнить екшен
        /// </summary>
        /// <param name="action"></param>
        public void Run(Action action)
        {
            if (IsSpinnerRunning)
            {
                action();
                return;
            }

            IsSpinnerRunning = true;
            MKP.Controls.WhaitForm.FormLoad.ShowFormLoad(this, SpinnerText, SpinnerDescriptionText);
            try
            {
                Thread.Sleep(SLEEP_SECONDS);
                action();
            }
            catch (Exception error)
            {
                OnErrorRunWithSpinner?.Invoke(this, error);
            }
            finally
            {
                MKP.Controls.WhaitForm.FormLoad.CloseFormLoad();
                IsSpinnerRunning = false;
            }
        }
        #endregion
    }
}
