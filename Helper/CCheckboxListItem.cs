﻿namespace MKP.Helper
{
    /// <summary>
    /// Класс помощник для работі с CheckBoxList
    /// </summary>
    public class CCheckBoxListItem
    {
        public string Text
        {
            get; set;
        }

        public object Tag
        {
            get;set;
        }

        public override string ToString()
        {
            return this.Text;
        }
    }
}
