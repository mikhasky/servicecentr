﻿using System;

namespace MKP.Helper.Spinner
{
    /// <summary>
    /// Интерфейс для визуальных компонентов со спинером
    /// </summary>
    public interface ICSpinne
    {
        bool IsSpinnerRunning { get; }

        string SpinnerText { get; set; }

        string SpinnerDescriptionText { get; set; }

        event ErrorRunWithSpinnerEventHandler OnErrorRunWithSpinner;

        void Run(Action action);
    }
}