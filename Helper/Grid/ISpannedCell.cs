﻿using System.Windows.Forms;

namespace MKP.Helper.Grid
{
    interface ISpannedCell
    {
        int ColumnSpan { get; }
        int RowSpan { get; }
        DataGridViewCell OwnerCell { get; }
    }
}