﻿using System.Windows.Forms;
using System.ComponentModel;

namespace MKP.Helper.Grid
{
    public class CellPresenter
    {
        #region Properties
        [Browsable(false)]
        public DataGridViewCell Cell { get; private set; }
        public bool ReadOnly
        {
            get { return Cell.ReadOnly; }
            set { Cell.ReadOnly = value; }
        }
        public string Value
        {
            get { return (string)Cell.Value; }
            set { Cell.Value = value; }
        }
        public int ColumnSpan
        {
            get
            {
                var cell = Cell as DataGridViewTextBoxCellEx;
                if (cell != null)
                    return cell.ColumnSpan;

                var cell2 = Cell as DataGridViewImageCellEx;
                if (cell2 != null)
                    return cell2.ColumnSpan;

                return 1;
            }
            set
            {
                var cell = Cell as DataGridViewTextBoxCellEx;
                if (cell != null)
                    cell.ColumnSpan = value;

                var cell2 = Cell as DataGridViewImageCellEx;
                if (cell2 != null)
                    cell2.ColumnSpan = value;

            }
        }
        public int RowSpan
        {
            get
            {
                var cell = Cell as DataGridViewTextBoxCellEx;
                if (cell != null)
                    return cell.RowSpan;
                var cell2 = Cell as DataGridViewImageCellEx;
                if (cell2 != null)
                    return cell2.RowSpan;

                return 1;
            }
            set
            {
                var cell = Cell as DataGridViewTextBoxCellEx;
                if (cell != null)
                    cell.RowSpan = value;
                var cell2 = Cell as DataGridViewImageCellEx;
                if (cell2 != null)
                    cell2.RowSpan = value;
            }
        }
        public bool ColumnFrozen
        {
            get { return Cell.OwningColumn.Frozen; }
            set { Cell.OwningColumn.Frozen = value; }
        }
        public bool RowFrozen
        {
            get { return Cell.OwningRow.Frozen; }
            set { Cell.OwningRow.Frozen = value; }
        }
        public DataGridViewCellStyle CellStyle
        {
            get { return Cell.Style; }
            set { Cell.Style = value; }
        }
        public int ColumnDividerWidth
        {
            get { return Cell.OwningColumn.DividerWidth; }
            set { Cell.OwningColumn.DividerWidth = value; }
        }
        public int RowDividerHeight
        {
            get { return Cell.OwningRow.DividerHeight; }
            set { Cell.OwningRow.DividerHeight = value; }
        }
        #endregion

        #region ctor
        public CellPresenter(DataGridViewCell cell)
        {
            Cell = cell;
        }
        #endregion
    }

    public class RowPresenter
    {
        DataGridViewRow m_Row;
        public bool Visible
        {
            get { return m_Row.Visible; }
            set { m_Row.Visible = value; }
        }
        public bool Frozen
        {
            get { return m_Row.Frozen; }
            set { m_Row.Frozen = value; }
        }
        public int Index
        {
            get { return m_Row.Index; }
        }

        #region ctor
        public RowPresenter(DataGridViewRow row)
        {
            m_Row = row;
        }
        #endregion
    }
}
