﻿using System;
using System.Windows.Forms;

namespace MKP.Helper.Control
{
    /// <summary>
    /// Вспомогательній контрол для фильтров где нужно активация или нет того фильтра и вібор изнесеольких вариантов
    /// </summary>
    public partial class UCEnabledComboBox : UserControl
    {
        #region Properties
        /// <summary>
        /// Лейбл
        /// </summary>
        public string Label
        {
            get { return checkBox1.Text; }
            set { checkBox1.Text = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool Checked
        {
            get { return checkBox1.Checked; }
            set { checkBox1.Checked = value;
                SetEnable();
            }
        }

        /// <summary>
        /// комбобокс
        /// </summary>
        public ComboBox ComboBox
        {
            get { return comboBox1; }
        }
        #endregion

        public UCEnabledComboBox()
        {
            InitializeComponent();
            checkBox1.Checked = false;
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            SetEnable();
        }

        private void SetEnable()
        {
            comboBox1.Enabled = checkBox1.Checked;
        }
    }
}
