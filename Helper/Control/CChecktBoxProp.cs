﻿using System;
using System.Windows.Forms;

namespace MKP.Helper.Control
{
    public class CCheckBoxProp : CheckBox, IControlProp
    {
        /// <summary>
        /// Имя поля
        /// </summary>
        public string PropName
        {
            get; set;
        }
        /// <summary>
        /// Тип
        /// </summary>
        public int Type
        {
            get; set;
        }

        /// <summary>
        /// Занчение
        /// </summary>
        /// <returns></returns>
        public object GetValueFromControl()
        {
            return this.Checked;
        }
    }
}
