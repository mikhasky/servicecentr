﻿using System;
using System.Windows.Forms;

namespace MKP.Helper.Control
{
    public class CComboBoxProp : ComboBox, IControlProp
    {
        /// <summary>
        /// Имя поля
        /// </summary>
        public string PropName
        {
            get; set;
        }
        /// <summary>
        /// Тип
        /// </summary>
        public int Type
        {
            get; set;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public object GetValueFromControl()
        {
            if ((this.SelectedItem as CComboBoxItem) == null)
                return 0;
            return (this.SelectedItem as CComboBoxItem).Id;
        }
    }
}
