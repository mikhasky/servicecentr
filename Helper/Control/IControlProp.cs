﻿namespace MKP.Helper.Control
{
    /// <summary>
    /// Вспомогательный класс для контролов редактировая
    /// </summary>
    public interface IControlProp
    {
        /// <summary>
        /// Тип
        /// </summary>
        int Type
        {
            get; set;
        }
        /// <summary>
        /// Имя поля
        /// </summary>
        string PropName
        {
            get; set;
        }

        /// <summary>
        /// Поулчить занчение 
        /// </summary>
        /// <returns></returns>
        object GetValueFromControl();
    }
}
