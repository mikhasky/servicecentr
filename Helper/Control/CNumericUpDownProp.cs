﻿using System;
using System.Windows.Forms;

namespace MKP.Helper.Control
{
    public class CNumericUpDownProp : NumericUpDown, IControlProp
    {
        /// <summary>
        /// Имя поля
        /// </summary>
        public string PropName
        {
            get; set;
        }
        /// <summary>
        /// Тип
        /// </summary>
        public int Type
        {
            get; set;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public object GetValueFromControl()
        {
            return this.Value;
        }
    }
}
