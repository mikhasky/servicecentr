﻿using System;
using System.Windows.Forms;

namespace MKP.Helper
{
    /// <summary>
    /// Класс для настледования для пользоватльских[ контролов (для которых возможно внешнени обновление)
    /// тоесть используються спровчники которые внешне могут редатироваться
    /// 
    /// </summary>
    public class CControlOutputUpdate : UserControl
    {
        /// <summary>
        /// Наренль на которой удет отображаться тчо нужно обновить контрол
        /// </summary>
        Panel _panel;

        bool _isNeedUpdate;
        /// <summary>
        /// Нужно ли обнвить данне в контроле
        /// </summary>
        public bool IsNeedUpdate
        {
            get { return _isNeedUpdate; }
            set
            {
                if (_isNeedUpdate != value)
                {
                    _isNeedUpdate = value;
                    _SetNeedUpdate(_isNeedUpdate);
                }
            }
        }
        /// <summary>
        /// Задает поведение при изменние статуса нужно ли обновлять или нет
        /// </summary>
        /// <param name="_isNeedUpdate"></param>
        private void _SetNeedUpdate(bool _isNeedUpdate)
        {
            //throw new NotImplementedException();
            this.SuspendLayout();
            if (_isNeedUpdate)
            {
                if (_panel != null)
                    this.Controls.Remove(_panel);

                _panel = new Panel();
                _panel.Dock = DockStyle.Fill;

                Label _label = new Label();
                _label.Text = "Дані потрібно оновити";
                _label.Dock = DockStyle.Top;
                _label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
                _label.Height = this.Height / 2;

                Button _button = new Button();
                _button.Text = "Оновити";
                _button.Click += _button_Click;
                _button.Top = _label.Height + 20;
                _button.Left = this.Width / 2 - 10;

                _panel.Controls.Add(_button);
                _panel.Controls.Add(_label);

                this.Controls.Add(_panel);
                _panel.BringToFront();
            }else
            {
                if (_panel == null)
                    return;

                this.Controls.Remove(_panel);
                _panel = null;
            }
            this.ResumeLayout();
        }

        private void _button_Click(object sender, EventArgs e)
        {
            UpdateControl();
            IsNeedUpdate = false;
        }

        /// <summary>
        /// Обновление данных контрола
        /// </summary>
        public virtual void UpdateControl()
        {

        }


        public CControlOutputUpdate():base()
        {

        }
        
    }
}
