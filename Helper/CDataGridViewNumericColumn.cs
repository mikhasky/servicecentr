﻿using System.Windows.Forms;

namespace MKP.Helper
{
    public class CDataGridViewNumericColumn: DataGridViewTextBoxColumn
    {
        public CDataGridViewNumericColumn(bool isDouble = false):base()
        {
            if (isDouble)
                this.ValueType = typeof(double);
            else
                this.ValueType = typeof(int);
        }
    }
}
