﻿using System.ComponentModel;
using System.Windows.Forms;

namespace MKP.Helper
{
    public class CRadioButtonListView: System.Windows.Forms.ListView
    {
        /*[Browsable(false), EditorBrowsable(EditorBrowsableState.Never),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public new bool OwnerDraw
        {
            get { return base.OwnerDraw; }
        }*/
        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public new bool CheckBoxes
        {
            get { return base.CheckBoxes; }
        }
        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public new bool MultiSelect
        {
            get { return base.MultiSelect; }
        }
        [EditorBrowsable(EditorBrowsableState.Never), Browsable(false),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public new System.Windows.Forms.View View
        {
            get { return base.View; }
        }

        public CRadioButtonListView()
        {
            base.MultiSelect = false;
            base.View = System.Windows.Forms.View.Details;
            base.CheckBoxes = true;
            //base.OwnerDraw = true;

        }

        protected override void OnItemChecked(ItemCheckedEventArgs e)
        {
            base.OnItemChecked(e);

            if (!e.Item.Checked)
                return;

            this.BeginUpdate();
            foreach(ListViewItem item in CheckedItems)
            {
                if (item != e.Item)
                    item.Checked = false;
            }
            this.EndUpdate();
        }

        protected override void OnDrawColumnHeader(DrawListViewColumnHeaderEventArgs e)
        {
            e.DrawDefault = true;
            base.OnDrawColumnHeader(e);
        }

        protected override void OnDrawSubItem(DrawListViewSubItemEventArgs e)
        {
            e.DrawDefault = true;
            base.OnDrawSubItem(e);
        }

        //protected override void OnDrawItem(DrawListViewItemEventArgs e)
        //{
        //    e.DrawFocusRectangle();
        //    base.OnDrawItem(e);

        //    Rectangle r = e.Bounds; Point p;
        //    var flags = TextFormatFlags.Default | TextFormatFlags.NoPrefix;
        //    var selected = (e.State & ListViewItemStates.Selected) == ListViewItemStates.Selected;
        //    var state = selected ?
        //        (Enabled ? RadioButtonState.CheckedNormal :
        //                   RadioButtonState.CheckedDisabled) :
        //        (Enabled ? RadioButtonState.UncheckedNormal :
        //                   RadioButtonState.UncheckedDisabled);
        //    if (RightToLeft == System.Windows.Forms.RightToLeft.Yes)
        //    {
        //        /*p = new Point(r.Right - r.Height + (ItemHeight - s.Width) / 2,
        //            r.Top + (ItemHeight - s.Height) / 2);
        //        r = new Rectangle(r.Left, r.Top, r.Width - r.Height, r.Height);*/
        //        p = new Point(r.Left, r.Top);
        //        flags |= TextFormatFlags.RightToLeft | TextFormatFlags.Right;
        //    }
        //    else
        //    {
        //        p = new Point(r.Left, r.Top);
        //        r = new Rectangle(r.Left + r.Height, r.Top, r.Width - r.Height, r.Height);
        //    }
        //    var bc = selected ? (Enabled ? SystemColors.Highlight :
        //        SystemColors.InactiveBorder) : BackColor;
        //    var fc = selected ? (Enabled ? SystemColors.HighlightText :
        //        SystemColors.GrayText) : ForeColor;
        //    using (var b = new SolidBrush(bc))
        //        e.Graphics.FillRectangle(b, e.Bounds);
        //    RadioButtonRenderer.DrawRadioButton(e.Graphics, p, state);
        //    TextRenderer.DrawText(e.Graphics, e.Item.Text, Font, r, fc, bc, flags);
        //}
    }
}
