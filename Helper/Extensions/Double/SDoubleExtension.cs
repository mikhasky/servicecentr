﻿using System;
using System.Linq;
using MKP.Helper.Extensions.Int;

namespace MKP.Helper.Extensions.Double
{
    /// <summary>
    /// Расширение для double
    /// </summary>
    public static class SDoubleExtension
    {
        /// <summary>
        /// Перевод числа в строку прописью (расширение SDoubleExtension)
        /// </summary>
        /// <param name="doubleValue">значение которое нужно перевести прописью</param>
        /// <returns></returns>
        public static string GetStringWrite(this double doubleValue)
        {
            return _DoubleToWriteString(doubleValue);
        }
        /// <summary>
        /// Получить строчное представление(прописью)
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private static string _DoubleToWriteString(double value)
        {
            int m1, m2, m3;
            int sum = (int)Math.Truncate(value);

            string str = sum.GetStringWrite(value - sum > 0);

            string valueString = value.ToString();
            if (value - sum > 0)
            {
                string[] arrString = valueString.Split(',');
                string valueDrobString = arrString[1];
                while (valueDrobString.Length>0 && valueDrobString.Last() == '0')
                {
                    valueDrobString.Remove(valueDrobString.Last());
                }

                if (valueDrobString.Length == 0)
                    return str;

                int valueDop = int.Parse(valueDrobString);

                str += (arrString[0].Last() == '1' ? "ціла " :"цілих ") + valueDop.GetStringWrite(true) + GetDrobName(valueDrobString.Length, valueDrobString.Last()=='1');
            }

            return str;
        }
        /// <summary>
        /// Названние дробной части
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        private static string GetDrobName(int length, bool isOne = false)
        {
            string label = "";
            switch (length)
            {
                case 1:
                    label = "десят";
                    break;
                case 2:
                    label = "сот";
                    break;
                case 3:
                    label = "тисячн";
                    break;
                case 4:
                    label = "десятитисячн";
                    break;
                case 5:
                    label = "стотисячн";
                    break;
                case 6:
                    label = "мільйон";
                    break;

                default:
                    return "-";
            }

            return label + (isOne ? "a" : "их");
        }
    }
}
