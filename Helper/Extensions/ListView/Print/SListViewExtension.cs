﻿using System.Windows.Forms;

namespace MKP.Helper.Extensions.ListView.Print
{
    /// <summary>
    /// Расширение для ЛистВьюва (SListViewExtension)
    /// </summary>
    public static class SListViewExtensionPrint
    {
        /// <summary>
        /// Массив для печати (расширение SListViewExtension)
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public static string[,] GetPrintArrayData(this System.Windows.Forms.ListView list)
        {
            string[,] array = new string[list.Items.Count + 1, list.Columns.Count];
            int i = 0;

            foreach (ColumnHeader column in list.Columns)
            {
                array[0, i] = column.Text;
                i++;
            }

            i = 1;
            foreach (ListViewItem item in list.Items)
            {
                int j = 0;
                foreach (ListViewItem.ListViewSubItem _subItem in item.SubItems)
                {
                    array[i, j] = _subItem.Text;
                    j++;
                }

                i++;
            }

            return array;
        }
    }
}
