﻿namespace MKP.Helper.Extensions.Int
{
    /// <summary>
    /// Расширение для int
    /// </summary>
    public static class SIntExtension
    {
        /// <summary>
        /// Перевод числа в строку прописью (расширение SIntExtension)
        /// </summary>
        /// <param name="IntValue">значение которое нужно перевести прописью</param>
        /// <returns></returns>
        public static string GetStringWrite(this int intValue, bool isDrob = false)
        {
            return _IntToWriteString(intValue, isDrob);
        }

        /// <summary>
        /// Получить строкупо разряду
        /// </summary>
        /// <param name="str"></param>
        /// <param name="m1">Сотни</param>
        /// <param name="m2">десятки</param>
        /// <param name="m3">еденицы</param>
        /// <param name="flag">флаг 1-милионы, 2-тысячи, 3-еденицы</param>
        private static void _GetStringByRangValue(ref string str, int m1, int m2, int m3, int flag, bool isDrob = false)
        {
            switch (m1)
            {
                case 1:
                    str += "сто ";
                    break;
                case 2:
                    str += "двісті ";
                    break;
                case 3:
                    str += "триста ";
                    break;
                case 4:
                    str += "чотириста ";
                    break;
                case 5:
                    str += "п'ятсот ";
                    break;
                case 6:
                    str += "шістсот ";
                    break;
                case 7:
                    str += "сімсот ";
                    break;
                case 8:
                    str += "вісімсот ";
                    break;
                case 9:
                    str += "дев'ятсот ";
                    break;
            }

            if (m2 == 0 || m2 > 1)
            {
                if (m2 > 1)
                {
                    switch (m2)
                    {
                        case 2:
                            str += "двадцять ";
                            break;
                        case 3:
                            str += "тридцять ";
                            break;
                        case 4:
                            str += "сорок ";
                            break;
                        case 5:
                            str += "п'ятдесят ";
                            break;
                        case 6:
                            str += "шістдесят ";
                            break;
                        case 7:
                            str += "сімдесят ";
                            break;
                        case 8:
                            str += "вісімдесят ";
                            break;
                        case 9:
                            str += "дев'яносто ";
                            break;
                    }
                }

                switch (m3)
                {
                    case 1:
                        if (flag == 2 || isDrob) str += "одна ";
                        else str += "один ";
                        break;
                    case 2:
                        if (flag == 2 || isDrob) str += "дві ";
                        else str += "два ";
                        break;
                    case 3:
                        str += "три ";
                        break;
                    case 4:
                        str += "чотири ";
                        break;
                    case 5:
                        str += "п'ять ";
                        break;
                    case 6:
                        str += "шість ";
                        break;
                    case 7:
                        str += "сім ";
                        break;
                    case 8:
                        str += "вісім ";
                        break;
                    case 9:
                        str += "дев'ять ";
                        break;
                }
            }
            else if (m2 == 1)
            {
                switch (m3)
                {
                    case 0:
                        str += "десять ";
                        break;
                    case 1:
                        str += "одинадцять ";
                        break;
                    case 2:
                        str += "дванадцять ";
                        break;
                    case 3:
                        str += "тринадцять ";
                        break;
                    case 4:
                        str += "чотирнадцять ";
                        break;
                    case 5:
                        str += "п'ятнадцять ";
                        break;
                    case 6:
                        str += "шістнадцять ";
                        break;
                    case 7:
                        str += "сімнадцять ";
                        break;
                    case 8:
                        str += "вісімнадцять ";
                        break;
                    case 9:
                        str += "дев'ятнадцять ";
                        break;
                }
            }


            if (flag == 1)
            {
                if (m3 == 0 && m2 == 0 && m1 == 0) str += "";
                else if (m2 == 1)
                {
                    str += "мільйонів ";
                }
                else
                {
                    if (m3 == 1) str += "мільйон ";
                    else if (m3 >= 2 && m3 <= 4) str += "мільйона ";
                    else str += "мільйонів ";
                }
            }
            else if (flag == 2)
            {
                if (m3 == 0 && m2 == 0 && m1 == 0) str += "";
                else if (m2 == 1)
                {
                    str += "тисяч ";
                }
                else
                {
                    if (m3 == 1) str += "тисяча ";
                    else if (m3 >= 2 && m3 <= 4) str += "тисячі ";
                    else str += "тисяч ";
                }
            }
            else
            {
                if (m3 == 0 && m2 == 0 && m1 == 0) str += "нуль ";
            }
        }
        /// <summary>
        /// Получить строчное представление(прописью)
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private static string _IntToWriteString(int value, bool isDrob = false)
        {
            int m1, m2, m3;
            int sum = value;

            string str = "";

            m1 = sum / 100000000;
            m2 = sum / 10000000 - (sum / 100000000) * 10;
            m3 = sum / 1000000 - (sum / 10000000) * 10;
            _GetStringByRangValue(ref str, m1, m2, m3, 1);

            m1 = sum / 100000 - (sum / 1000000) * 10;
            m2 = sum / 10000 - (sum / 100000) * 10;
            m3 = sum / 1000 - (sum / 10000) * 10;
            _GetStringByRangValue(ref str, m1, m2, m3, 2);

            m1 = sum / 100 - (sum / 1000) * 10;
            m2 = sum / 10 - (sum / 100) * 10;
            m3 = sum / 1 - (sum / 10) * 10;
            _GetStringByRangValue(ref str, m1, m2, m3, 3, isDrob);

            return str;
        }
    }
}
