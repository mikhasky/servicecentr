﻿namespace MKP.Helper
{
    public class CComboBoxItem
    {
        int id;
        string value;
        object tag;

        public string Text { get { return value; } }

        public string Value
        {
            get { return value; }
        }

        public int Id
        {
            get { return id; }
        }

        public object Tag
        {
            get { return tag; }
        }

        public CComboBoxItem(int _id, string _value)
        {
            id = _id;
            value = _value;
        }

        public CComboBoxItem(int _id, string _value, object _tag)
        {
            id = _id;
            value = _value;
            tag = _tag;
        }

        public CComboBoxItem()
        {
        }

        public override string ToString()
        {
            return Value;
        }
    }
}
