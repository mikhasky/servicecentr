﻿using System.Windows.Forms;

namespace MKP.Helper
{
    /// <summary>
    /// Колонка с чекбоксами
    /// </summary>
    public class CCheckBoxColumn: DataGridViewCheckBoxColumn
    {
        CCheckBoxHeaderCell checkheader;
        /// <summary>
        /// Конструктор 1
        /// </summary>
        public CCheckBoxColumn()
        {

        }
        /// <summary>
        /// Контсруктор 2
        /// </summary>
        /// <param name="enableCheckAll">Віводить ли чек бокс вібор всех</param>
        public CCheckBoxColumn(bool enableCheckAll):this()
        {
            if (enableCheckAll)
            {
                checkheader = new CCheckBoxHeaderCell();
                checkheader.OnCheckBoxHeaderClick += checkheader_OnCheckBoxHeaderClick;
                this.HeaderCell = checkheader;
            }
        }
        /// <summary>
        /// Событие на нажатие чек алл
        /// </summary>
        /// <param name="e"></param>
        private void checkheader_OnCheckBoxHeaderClick(CheckBoxHeaderCellEventArgs e)
        {
            if (this.DataGridView.Rows.Count == 0)
                return;

            this.DataGridView.BeginEdit(true);
            foreach (DataGridViewRow item in this.DataGridView.Rows)
            {
                item.Cells[this.Index].Value = e.IsChecked;
            }
            this.DataGridView.EndEdit();
        }
    }
}
