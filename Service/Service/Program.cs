﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Service
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
            /*
            Console.Title = "CLIENT";

            Uri address = new Uri("http://localhost:1515/iConstract");

            EndpointAddress EndPoint = new EndpointAddress(address);

            ChannelFactory<iConstract> factory = new ChannelFactory<iConstract>(new BasicHttpBinding(), EndPoint);

            iConstract channel = factory.CreateChannel();

            while (true)
            {
                string text = Console.ReadLine();
                if (text == "quit")
                {
                    break;
                }
                channel.Say(text);
            }

            Console.WriteLine("Confirm");
            Console.ReadKey();
            */
        }
    }
}
