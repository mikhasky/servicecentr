﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraRichEdit.API.Native;
using DevExpress.XtraRichEdit;
using DevExpress.Office.Utils;
using DevExpress.Office.Services;
using System.IO;
using DevExpress.XtraPrinting;
using MKP.EditorDelegates;
using MKP.TextEditor.DevExpressMacroSets;
using System.Text.RegularExpressions;
using DevExpress.XtraRichEdit.Layout.Export;
using System.Drawing.Drawing2D;
using System.Threading.Tasks;
using System.Threading;
using DevExpress.Office;
using System.Drawing.Printing;

namespace MKP
{
    public partial class EditorControl : UserControl
    {        
        public event TextChangedDelegate RichTextChanged;
        private List<string> macroses { get; set; }
        private DataSet MyData = new DataSet();
        NewData[] data;
        public Color TextBaseColor = Color.Black;
        public Color MacroColor = Color.FromArgb(255, 174, 201);
        Brush MacroBrush = new SolidBrush(Color.FromArgb(100, 255, 174, 201));
        private bool _DrawMacroses = false;      
        /*public float MarginsBottom {
            get {
                if (richEditControl1 != null && richEditControl1.Document != null && richEditControl1.Document.Sections.Count != 0)
                    return richEditControl1.Document.Sections[0].Margins.Bottom;
                else
                    return 0;
            }
            set {
                richEditControl1.Document.Sections[0].Margins.Bottom = value;}
        }
        public float MarginsTop
        {
            get {
                if (richEditControl1 != null && richEditControl1.Document != null && richEditControl1.Document.Sections.Count != 0)
                    return richEditControl1.Document.Sections[0].Margins.Top;
                else
                    return 0;              
            }
            set { richEditControl1.Document.Sections[0].Margins.Top = value; }
        }

        public float MarginsLeft
        {
            get { return richEditControl1.Document.Sections[0].Margins.Left; }
            set { richEditControl1.Document.Sections[0].Margins.Left = value; }
        }
        public float MarginsRight
        {
            get { return richEditControl1.Document.Sections[0].Margins.Right; }
            set { richEditControl1.Document.Sections[0].Margins.Right = value; }
        }
        public float MarginsFooter
        {
            get { return richEditControl1.Document.Sections[0].Margins.FooterOffset; }
            set { richEditControl1.Document.Sections[0].Margins.FooterOffset = value; }
        }
        public float MarginsHeader
        {
            get { return richEditControl1.Document.Sections[0].Margins.HeaderOffset; }
            set { richEditControl1.Document.Sections[0].Margins.HeaderOffset = value; }
        }
        public PaperKind Paper
        {
            get { return richEditControl1.Document.Sections[0].Page.PaperKind; }
            set { richEditControl1.Document.Sections[0].Page.PaperKind = value; }
        }
        public bool Landscape
        {
            get { return richEditControl1.Document.Sections[0].Page.Landscape; }
            set { richEditControl1.Document.Sections[0].Page.Landscape = value; }
        }
        public string TextValue
        {
            get { return richEditControl1.Document.Text; }
            set {
                if (value!= richEditControl1.Document.Text)
                richEditControl1.Document.Text = value;
            }
        }
        public string RtfTextValue
        {
            get { return richEditControl1.Document.RtfText; }
            set {
                if (value!= richEditControl1.Document.RtfText)
                richEditControl1.Document.RtfText = value;
            }
        }*/
        /// <summary>
        /// Выделять макросы?
        /// </summary>
        public bool DrawMacroses { get { return _DrawMacroses; } set { _DrawMacroses = value;SetDraw(value); } }
       
        private void SetDraw(bool value)
        {
            //richEditControl1.ActiveViewChanged -= ActiveViewChanged;
            richEditControl1.CustomDrawActiveView -= CustomDrawActiveView;
            if (value)
            {
               // richEditControl1.ActiveViewChanged += ActiveViewChanged;
                richEditControl1.CustomDrawActiveView += CustomDrawActiveView;
            }
        }        
        private const int InchesToDocumentCoeff = 300;
        public RectangleF[] GetRealPixelRectangle(List<RectangleF> lst,float OffsetX=0,float OffsetY=0)
            {
            int len = lst.Count;           
            for (int i = 0; i < len; i++)
            {
                lst[i] = new RectangleF(Units.MillimetersToDocumentsF(lst[i].X+ OffsetX), Units.MillimetersToDocumentsF(lst[i].Y+ OffsetY), Units.MillimetersToDocumentsF(lst[i].Width), Units.MillimetersToDocumentsF(lst[i].Height));
                lst[i] = Units.DocumentsToPixels(lst[i], richEditControl1.DpiX, richEditControl1.DpiY);
            }
           /* switch (richEditControl1.Document.Unit)
             {
                 case DevExpress.Office.DocumentUnit.Document:
                     for (int i = 0; i < len; i++)
                     {          
                        lst[i]= Units.DocumentsToPixels(new RectangleF(lst[i].X, lst[i].Y, lst[i].Width, lst[i].Height), richEditControl1.DpiX, richEditControl1.DpiY);
                           //lst[i]=new Rectangle((int)Units.InchesToDocumentsF(lst[i].X*300), (int)Units.InchesToDocumentsF(lst[i].Y*300), (int)Units.InchesToDocumentsF(lst[i].Width*300), (int)Units.InchesToDocumentsF(lst[i].Bottom*300));
                     }
                    break;
                case DocumentUnit.Millimeter:
                    for (int i = 0; i < len; i++)
                    {
                        
                    }
                    break;      
             }*/
              return lst.ToArray();
            //var t = lst.Select(i => Units.DocumentsToPixels(i, dpiX, dpiY)).ToArray();
          //  return t;
        }
        public void SetForceMargins(bool ForceMargins)
        {
            richEditControl1.MarginChanged -= FindMarginChanged;
            if (ForceMargins)
            {
                foreach (var item in richEditControl1.Document.Sections)
                {
                    item.Margins.Top = 0;
                    item.Margins.Left = 0;
                }
                richEditControl1.MarginChanged += FindMarginChanged;
            }
        }

        private void FindMarginChanged(object sender, EventArgs e)
        {
            foreach (var item in richEditControl1.Document.Sections)
            {
                item.Margins.Top = 0;
                item.Margins.Left = 0;
            }
        }

        private const DocumentUnit GeneralUnit = DocumentUnit.Millimeter;
        private void RestoreUnit(DocumentUnit unit)
        {
            if (richEditControl1.Unit != unit)
                richEditControl1.Unit = unit;
        }
        public void SetOriginalColontitules(Document document)
        {
            foreach (var section in document.Sections)
            {
                section.UnlinkFooterFromPrevious();
                section.UnlinkFooterFromNext();
                section.UnlinkHeaderFromPrevious();
                section.UnlinkHeaderFromNext();
            }
        }
        private void CustomDrawActiveView(object sender, RichEditViewCustomDrawEventArgs e)
        {
            if (!_DrawMacroses)
                return;
            var oldUnit = richEditControl1.Unit;
            if (oldUnit != GeneralUnit)
                richEditControl1.Unit = GeneralUnit;
            List<RectangleF> lst = new List<RectangleF>();
             foreach (var item in richEditControl1.Document.FindAll(EditorControl.CommonRegex, richEditControl1.ActiveView.GetVisiblePagesRange()))                          
                 lst.AddRange(BuildCommonRectangles(item).ToArray());            
             foreach (var item in richEditControl1.Document.FindAll(EditorControl.CommonArrayRegex, richEditControl1.ActiveView.GetVisiblePagesRange()))            
                 lst.AddRange(BuildCommonRectangles(item).ToArray());
            if (lst.Count == 0)
            {
                RestoreUnit(oldUnit);
                return;
            }      
             e.Cache.Graphics.SmoothingMode = SmoothingMode.HighSpeed;
            // foreach (var item in lst)            
            //  e.Cache.FillRectangle(MacroBrush,item);
            float offsetY= richEditControl1.Options.HorizontalRuler.Visibility != RichEditRulerVisibility.Hidden ? -6.5f : .0f;
            float offsetX = richEditControl1.Options.VerticalRuler.Visibility != RichEditRulerVisibility.Hidden ? .0f : 6.5f;
            e.Cache.Graphics.FillRectangles(MacroBrush, GetRealPixelRectangle(lst,offsetX,offsetY));//GetRealPixelRectangle(lst)
            RestoreUnit(oldUnit);
            //e.Cache.Dispose();

        }
        private volatile bool reScanMacro = false;
        private volatile bool isScanned = false;
        //List<DrawRectangle> DrawRectangles = new List<DrawRectangle>();          
        //bool FreezeDraw { get; set; } = false;
        public EditorControl()
        {
            System.Windows.Forms.Control.CheckForIllegalCrossThreadCalls = false;
            InitializeComponent();
            if (!ribbonControl1.Minimized)
            ribbonControl1.Minimized = true;
            if (richEditControl1.Document.DefaultCharacterProperties.FontName != "Times New Roman")
            richEditControl1.Document.DefaultCharacterProperties.FontName = "Times New Roman";
            if (richEditControl1.Document.Unit != DevExpress.Office.DocumentUnit.Millimeter)
            richEditControl1.Document.Unit = DevExpress.Office.DocumentUnit.Millimeter;
            if (richEditControl1.Options.HorizontalRuler.Visibility != RichEditRulerVisibility.Hidden)
            richEditControl1.Options.HorizontalRuler.Visibility = RichEditRulerVisibility.Hidden; 
           // richEditControl1.CustomMarkDraw += DrawKeyWords;
           // richEditControl1.Document.ContentChanged += change;            
           //richEditControl1.ForceSyntaxHighlight();    
        }
        
        void CreateCustomMarks(DocumentRange range)
        {            
            richEditControl1.Invoke(new MethodInvoker(() => richEditControl1.Document.CustomMarks.Create(range.Start, range)));
            #region comment  
            //richEditControl1.GetLayoutPhysicalBoundsFromPosition(range.End).Height
            /* DevExpress.XtraRichEdit.API.Native.SearchOptions options = DevExpress.XtraRichEdit.API.Native.SearchOptions.None;           
             DocumentRange[] ranges = richEditControl1.Document.FindAll(text, options);
             if (ranges != null && ranges.Length > 0)
             {
                 foreach (DocumentRange range in ranges)
                 {
                     richEditControl1.Document.CustomMarks.Create(range.Start, range);
                     richEditControl1.Document.CustomMarks.Create(range.End, range);
                 }
             }*/
            #endregion
        }
        void ClearCustomMarks()
        {           
            for (int i = richEditControl1.Document.CustomMarks.Count - 1; i >= 0; i--)
            {
                DevExpress.XtraRichEdit.API.Native.CustomMark mark = richEditControl1.Document.CustomMarks[i];
                richEditControl1.Document.CustomMarks.Remove(mark);
            }           
        }
        private List<RectangleF> BuildCommonRectangles(DocumentRange range)
        {
            int end = range.End.ToInt();
            int start = range.Start.ToInt();
            List<RectangleF> lst = new List<RectangleF>();          
            var bounds = richEditControl1.GetBoundsFromPositionF(range.Start);//.GetLayoutPhysicalBoundsFromPosition(range.Start);
            
            float startX = bounds.X;
            float startY = bounds.Y;
            var endBounds = richEditControl1.GetBoundsFromPositionF(range.End);
            bool first = true;           
            if (bounds.Y == endBounds.Y)
            {
                // lst.Add(Units.DocumentsToPixels(new Rectangle((bounds.X*100), (bounds.Y * 100), ((endBounds.X - bounds.X) * 100), bounds.Bottom * 100),richEditControl1.DpiX, richEditControl1.DpiY));
                lst.Add(new RectangleF(bounds.X-7, bounds.Y, (endBounds.X - bounds.X), bounds.Height));
                return lst;
            }
            else
                for (int i = start + 1; i < end; i++)
                {
                    bounds = richEditControl1.GetBoundsFromPosition(richEditControl1.Document.CreatePosition(i));
                    if (bounds.Y != startY || i == end - 1)
                    {
                        if (first)
                            lst.Add(new RectangleF(startX-7, startY, (endBounds.X - startX), bounds.Height));
                        else
                            lst.Add(new RectangleF(startX-7,startY, (endBounds.X - startX), bounds.Height));
                        first = false;
                        startX = bounds.X;
                        startY = bounds.Y;
                    }
                    endBounds = bounds;
                }
            return lst;
        }
        private List<DrawRectangle> BuildRectangles(DocumentRange range)
        {
            int end = range.End.ToInt();
            int start = range.Start.ToInt();
            List<DrawRectangle> lst = new List<DrawRectangle>();
            var bounds = richEditControl1.GetLayoutPhysicalBoundsFromPosition(range.Start);
            int startX=bounds.X;
            int startY = bounds.Y;
            var endBounds = richEditControl1.GetLayoutPhysicalBoundsFromPosition(range.End);
            bool first = true;  
            if (bounds.Y == endBounds.Y)
            {
                lst.Add(new DrawRectangle() { rectangle = new Rectangle((int)(bounds.X / 2.977777777777778), (int)(bounds.Y / 3.1), (int)((endBounds.X - bounds.X)/3.14),(int)(bounds.Height / 3.117647058823529)), range = range });
                return lst;
            }else
            for (int i = start+1; i < end; i++)
            {                    
                    bounds = richEditControl1.GetLayoutPhysicalBoundsFromPosition(richEditControl1.Document.CreatePosition(i));                   
                   if (bounds.Y != startY||i==end-1)
                    {              
                        if (first)          
                        lst.Add(new DrawRectangle() {rectangle= new Rectangle((int)(startX/2.97),(int)( startY/ 3.1), (int)((endBounds.X - startX)/ 3.09), bounds.Height / 3),range=range });
                        else
                            lst.Add(new DrawRectangle() { rectangle = new Rectangle((int)(startX / 2.957), (int)(startY / 3.1), (int)((endBounds.X - startX) / 2.95), bounds.Height / 3), range = range });
                        first = false;
                        startX = bounds.X;
                        startY = bounds.Y;
                    }
                    endBounds = bounds;
                }
            return lst;            
        }
        private void DrawKeyWords(object sender, RichEditCustomMarkDrawEventArgs e)
        {
            
            
            //if (FreezeDraw)
             //   return;
            /* DevExpress.XtraRichEdit.Model.CustomMark mark=new DevExpress.XtraRichEdit.Model.CustomMark()
             var f =new CustomMarkVisualInfo();
             e.VisualInfoCollection.Add();*/
            List<Rectangle> lst = new List<Rectangle>();
            foreach (CustomMarkVisualInfo info in e.VisualInfoCollection)
            {
                var range = (DocumentRange)info.UserData;
                var item = BuildRectangles(range); //DrawRectangles.Where(i => i.range == range).ToList();
                if (item != null&&item.Count>0)
                    lst.AddRange(item.Select(i=>i.rectangle));
            }
            if (lst.Count > 0)
            {
                e.Graphics.SmoothingMode = SmoothingMode.HighSpeed;
                e.Graphics.FillRectangles(MacroBrush, lst.ToArray());
            }
                //for (int i = 0; i < lst.Count; i++)
                  //  e.Graphics.FillRectangle(MacroBrush, lst[i]);                 
            /*foreach (CustomMarkVisualInfo info in e.VisualInfoCollection)
            {
                DocumentRange end = (DocumentRange)info.UserData;
                var pos=richEditControl1.GetLayoutPhysicalBoundsFromPosition(end.End);             
               // CustomMark mark = doc.CustomMarks.GetByVisualInfo(info);               
                e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
                if (pos.Y/3 == info.Bounds.Location.Y)
                {
                    Rectangle rect = new Rectangle(info.Bounds.Location.X, info.Bounds.Location.Y, info.Bounds.Location.X - info.Bounds.Location.X, info.Bounds.Height);
                    e.Graphics.FillRectangle(MacroBrush, rect);
                }
                else
                {

                }               
            }*/
            //DocumentRange range = (DocumentRange)info.UserData;                
            //if (mark.Position < doc.Selection.Start) curColor = Color.Green;
            /*  Pen p = new Pen(MacroColor, 2);
              p.StartCap = LineCap.Flat;
              p.EndCap = LineCap.ArrowAnchor;                

             // var loc1 = richEditControl1.GetBoundsFromPosition(range.Start);
             // var loc2 = richEditControl1.GetBoundsFromPosition(range.End);
              e.Graphics.DrawLine(p,new Point(start.Bounds.Location.X, start.Bounds.Location.Y+ start.Bounds.Height),new Point(info.Bounds.Location.X, info.Bounds.Location.Y+ info.Bounds.Height));*/
        }

        private void VerificationDrawRectangles()
        {
           // FreezeDraw = true;
           // DrawRectangles = new List<DrawRectangle>();
           // foreach (var info in richEditControl1.Document.CustomMarks)            
             //   DrawRectangles.AddRange(BuildRectangles((DocumentRange)info.UserData));
            //FreezeDraw = false;
        }

        public void ActivateTextChange(bool activate)
        {
            richEditControl1.TextChanged -= ChangedText;
            if (activate)
            richEditControl1.TextChanged += ChangedText;
        }

        private void ChangedText(object sender, EventArgs e)
        {
            RichTextChanged?.Invoke();
        }

        public bool SetVisiBleRIbbon { get { return ribbonControl1.Visible; } set { ribbonControl1.Visible = value; } }
       public RichEditRulerVisibility SetLinearVisibleHorizontal { get { return 
                    richEditControl1.Options.HorizontalRuler.Visibility; } set { richEditControl1.Options.HorizontalRuler.Visibility = value;} }
        public RichEditRulerVisibility SetLinearVisibleVertical
        {
            get
            {
                return
richEditControl1.Options.VerticalRuler.Visibility;
            }
            set { richEditControl1.Options.VerticalRuler.Visibility = value; }
        }

        public void CLearDataSet()
        {
            MyData = new DataSet();
        }
        public void AddDataTable(object obj)
        {
            try
            {
                CLearDataSet();
                if (obj is DataTable)
                {
                    MyData.Tables.Add(obj as DataTable);
                }
                else if (obj is string[])
                {
                    MyData.Tables.Add((obj as string[]).ToDataTable());
                }
                else if (obj is List<string>)
                {
                    MyData.Tables.Add((obj as List<string>).ToDataTable());
                }
                else if (obj is string)
                {
                    MyData.Tables.Add((obj as string).ToDataTable());
                }
                richEditControl1.Options.MailMerge.DataSource = MyData.Tables[0];
            }
            catch { }
        }
        public void SetIndent(float left, float right, float up, float down)
        {
            richEditControl1.Document.BeginUpdate();
            try
            {            
                foreach (Paragraph p in richEditControl1.Document.Paragraphs)
                {                    
                    p.LeftIndent += Units.InchesToDocumentsF(left);
                    p.RightIndent += Units.InchesToDocumentsF(right);
                }
            }
            finally
            {
                richEditControl1.Document.EndUpdate();
            }
        }
        private void ResponseMacroses()
        {
            if (MyData == null)
                return;
        }
        public Color GetBaseColor()
        {
            return TextBaseColor;
        }
        public void SetBaseColor(Color col)
        {
            TextBaseColor = col;
        }
        public void SetBlackColor()
        {
            /*DocumentRange myRange = richEditControl1.Document.CreateRange(richEditControl1.Document.CreatePosition(0), richEditControl1.Document.Length);*/
            try
            {
                //richEditControl1.Document.BeginUpdate();                
                CharacterProperties cp = richEditControl1.Document.BeginUpdateCharacters(richEditControl1.Document.Range);
                cp.ForeColor = TextBaseColor;
                richEditControl1.Document.EndUpdateCharacters(cp);
            }
            catch (Exception err) { MessageBox.Show(err.ToString()); }
            /* foreach (Paragraph p in richEditControl1.Document.Paragraphs)
             {
                 p.BackColor = Color.Black;
             }*/
        }
        public bool Indexes(string str, ref int index)
        {
            //if (index != -1)
            // index++;
            try
            {
                if (index == -1)
                    index = richEditControl1.Document.Text.IndexOf(str);
                else
                    index = richEditControl1.Document.Text.IndexOf(str, index);
                return index == -1 ? false : true;
            }
            catch
            {
               // MessageBox.Show(str+" "+index);
                return false;
            }
        }
        private static String HexConverter(System.Drawing.Color c)
        {
            return "#" + c.R.ToString("X2") + c.G.ToString("X2") + c.B.ToString("X2");
        }

        private static String RGBConverter(System.Drawing.Color c)
        {
            return "RGB(" + c.R.ToString() + "," + c.G.ToString() + "," + c.B.ToString() + ")";
        }
        public void SetNewMacroColor(Color col)
        {
            MacroColor = col;
        }
        private static string patternMacroArrayCommon { get; set; } = @"\[(\s?[a-zA-Z0-9А-ЯЁа-яё. ()\/';:.,*&^%$#@!=+_?№{-])*\s?]";
        private static string patterCommon { get; set; } = @"\{(\s?[a-zA-Z0-9А-ЯЁа-яё. ()\/';:.,*&^%$#@!=+_?№{-])*\s?}";
        public static Regex CommonRegex { get; private set; } = new Regex(patterCommon, RegexOptions.CultureInvariant | RegexOptions.Compiled);
        public static Regex CommonArrayRegex { get; private set; } = new Regex(patternMacroArrayCommon, RegexOptions.CultureInvariant | RegexOptions.Compiled);
        //public string HexColor = HexConverter(MacroColor);
        public void SetAllMacroColor()
        {
            if (!barCheckItem1.Checked)
            {
                ScanColor.Cancel();
                return;
            }
            var draw=(new ScanColor(richEditControl1));
            draw.SetDrawColorRanges -= ReloadMarks;
            draw.SetDrawColorRanges += ReloadMarks;
            draw.Scan();
            //richEditControl1.Document.CustomMarks.Create(item.Start, MacroColor);
            /*try
            {
               // string html = richEditControl1.Document.HtmlText;
                richEditControl1.Document.BeginUpdate();
                CharacterProperties cp = richEditControl1.Document.BeginUpdateCharacters(richEditControl1.Document.Range);
                cp.ForeColor = Color.Black;
                richEditControl1.Document.EndUpdateCharacters(cp);
                foreach (var item in richEditControl1.Document.FindAll(CommonRegex))
                {                   
                    cp = richEditControl1.Document.BeginUpdateCharacters(item);
                    cp.ForeColor = MacroColor;
                    richEditControl1.Document.EndUpdateCharacters(cp);
                }
            }
            catch
            { }
            finally {               
                richEditControl1.Document.EndUpdate();
            }*/
        }
        private void SetMarks(DocumentRange[] ranges)
        {           
                foreach (var item in ranges)
                    CreateCustomMarks(item);
            MarkLock = false;
        }
        bool MarkLock = false;
        DocumentRange[] CustomDrawRanges = new DocumentRange[0];
        private void ReloadMarks(DocumentRange[] ranges)
        {
            while (MarkLock)
                Thread.Sleep(10);
            MarkLock = true;
            ClearCustomMarks();
            CustomDrawRanges = ranges;
                //richEditControl1.Invoke(new MethodInvoker(() => SetMarks(ranges)));
           // else
                //SetMarks(ranges);
            //VerificationDrawRectangles();
            
        }

        public void SetMacroColor()
        {
            if (macroses == null)
                return;
            try
            {
                richEditControl1.Document.BeginUpdate();
                for (int i = 0; i < macroses.Count; i++)
                {
                    {
                        int index = -1;
                        if (richEditControl1.Document.Text.Contains(macroses[i]))
                        {
                            while (Indexes(macroses[i], ref index))
                            {

                                var finds = richEditControl1.Document.FindAll(macroses[i], SearchOptions.CaseSensitive);
                                foreach (var item in finds)
                                {
                                    CharacterProperties cp = richEditControl1.Document.BeginUpdateCharacters(item);
                                    if (cp.ForeColor != MacroColor)
                                    {
                                        cp.ForeColor = MacroColor;
                                    }
                                    richEditControl1.Document.EndUpdateCharacters(cp);
                                }
                                index += macroses[i].Length;
                            }
                        }
                    }
                }
            }
            catch { }
            finally
            {
                richEditControl1.Document.EndUpdate();
            }
        }
        public void SetMacroses(List<string> lst,List<string> listAdded=null)
        {
            macroses = lst;
            if (listAdded != null)
                foreach (string item in listAdded)
                    macroses.Insert(0, item);
        }
        public void PreloadOptions(string path)
        {
           
        }
        public void ViewMergedData(bool view)
        {
            richEditControl1.Options.MailMerge.ViewMergedData = view;
        }      
        /// <summary>
        /// Загрузка результата в макросы на выполнение
        /// </summary>
        public void LoadWorkMacroses()
        {
            DataTable DT = new DataTable();
            for (int i = 0; i < data.Length; i++)
            {
                DT.Columns.Add(data[i].Macro);                
            }
            for (int i = 0; i < data.Length; i++)
            {
                DataRow DR = DT.NewRow();
                for (int j = 0; j < data[0].Data.Length; j++)
                {
                    DR[data[i].Macro] = data[0].Data[j];
                }
                DT.Rows.Add(DR);
            }
            AddDataTable(DT);
        }
        /// <summary>
        /// Загрузка макроса и результата
        /// </summary>
        /// <param name="Macro"></param>
        /// <param name="Data"></param>
        public void SetData(string Macro, string Data)
        {
            if (data == null)
            {
                data = new NewData[1];
                data[1] = new NewData(Macro);
                data[1].SetData(Data);
            }
            else
            {
                for (int i = 0; i < data.Length; i++)
                {
                    if (data[i].Macro == Macro)
                    {
                        data[i].SetData(Data);
                        return;
                    }
                }
                Array.Resize(ref data, data.Count() + 1);
                data[data.Length - 1] = new NewData(Macro);
                data[data.Length - 1].SetData(Data);
            }
        }

        public void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (richEditControl1.Document.Tables.Count <= 0)
                return;
            richEditControl1.Document.BeginUpdate();
            float width = richEditControl1.Document.Sections[0].Page.Width-GraphicsUnitConverter.Convert(richEditControl1.Document.Sections[0].Margins.Right, GraphicsUnit.Pixel, GraphicsUnit.Document);
            foreach (Table tab in richEditControl1.Document.Tables)
            {
                    if (tab.PreferredWidth > width)
                    tab.PreferredWidth = width; 
            }
            richEditControl1.Document.EndUpdate();
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //richEditControl1.Options.Comments.VisibleAuthors.
           
            //richEditControl1.Document.Bookmarks.Create(richEditControl1.Document.Selection,"test");
            // richEditControl1.Document.Comments.Create(richEditControl1.Document.Selection, "");            
        }

        private void barButtonItem2_ItemClick_1(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {          
          

        }
        private bool StopChange = false;
        private void timer1_Tick(object sender, EventArgs e)
        {
            StopChange = false;
        }

        private void PaintMacroRecalculate_Tick(object sender, EventArgs e)
        {
            if (!StopChange)
                SetPacka();
        }
        private void change(object sender, EventArgs e)
        {
            PaintMacroRecalculate.Stop();
            timer1.Stop();
            if (!StopChange)
            {
                PaintMacroRecalculate.Start();
            }
            else
                timer1.Start();
            ScanMacro();
        }
       
        private void ScanMacro()
        {
            reScanMacro = true;
        }
        private void SetPacka()
        {
            if (!barCheckItem1.Checked)
                return;
            PaintMacroRecalculate.Stop();
            StopChange = true;
            SetAllMacroColor();
            PaintMacroRecalculate.Stop();
            timer1.Start();
        }
        private void barButtonItem2_ItemClick_2(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // var footer = sect.BeginUpdateFooter();
            /*|| footer == null*/
            var sect = richEditControl1.Document.GetSection(richEditControl1.Document.CaretPosition);
            if (sect == null)
                return;
            var header = sect.BeginUpdateHeader();            
            if (header == null)
                return;
            var margins = richEditControl1.Document.GetSection(header.Range.End);
            barEditItem4.EditValueChanged -= barEditItem4_EditValueChanged;
            barEditItem5.EditValueChanged -= barEditItem5_EditValueChanged;
            barEditItem4.EditValue = margins.Margins.HeaderOffset;
            barEditItem5.EditValue = margins.Margins.FooterOffset;
            barEditItem4.EditValueChanged += barEditItem4_EditValueChanged;
            barEditItem5.EditValueChanged += barEditItem5_EditValueChanged;
            sect.EndUpdateHeader(header);
           // sect.EndUpdateFooter(footer);
        }
        private void barEditItem4_EditValueChanged(object sender, EventArgs e)
        {
            var sect = richEditControl1.Document.GetSection(richEditControl1.Document.CaretPosition);
            if (sect == null)
                return;
            var header = sect.BeginUpdateHeader();
            if (header == null)
                return;
            var margins = richEditControl1.Document.GetSection(header.Range.End);
            margins.Margins.HeaderOffset=Convert.ToSingle(barEditItem4.EditValue);
            sect.EndUpdateHeader(header);
        }
        private void barEditItem5_EditValueChanged(object sender, EventArgs e)
        {
            var sect = richEditControl1.Document.GetSection(richEditControl1.Document.CaretPosition);
            if (sect == null)
                return;
            var footer = sect.BeginUpdateFooter();
            if (footer == null)
                return;
            var margins = richEditControl1.Document.GetSection(footer.Range.End);
            margins.Margins.FooterOffset = Convert.ToSingle(barEditItem5.EditValue);
            sect.EndUpdateFooter(footer);
        }

       
    }
    public class DrawRectangle
    {
        public DocumentRange range;
        public Rectangle rectangle;
    }
    public class NewData
    {
        public string Macro;
        public string[] Data;

        public NewData(string Macro)
        {
            this.Macro = Macro;
        }
        public void SetData(string data)
        {
            if (Data == null)
            {
                Data = new string[1];
                Data[1] = data;
            }
            else
            {
                Array.Resize(ref Data, Data.Count() + 1);
                Data[Data.Length - 1] = data;
            }
        }
    }
    public delegate void SetDocument(DocumentRange[] ranges);
    public class ScanColor
    {
        private static DocumentRange[] ranges=null;
        private static ScanColor prevScan;
        public CancellationTokenSource cancelTokenSource;
        public CancellationToken token;
        public static RichEditControl rec = new RichEditControl();
        public event SetDocument SetDrawColorRanges;
        private static DateTime date = new DateTime();
        bool BlockScan = false;
        //public DateTime ResponseTime;
        public ScanColor(RichEditControl rtf)
        {
            cancelTokenSource = new CancellationTokenSource();
            token = cancelTokenSource.Token;            
            Cancel();
            if ((DateTime.Now - date).TotalMilliseconds < 500)
                BlockScan = true;
            date = DateTime.Now;
            prevScan = this;
            rec = rtf;
            this.SetDrawColorRanges += SetRanges;
        }

        private void SetRanges(DocumentRange[] ranges)
        {
            ScanColor.ranges = ranges;
        }
        //TODO?
        private bool ComparsionRanges(DocumentRange[] ranges)
        {
            int len = ranges.Length;
            for (int i = 0; i < len; i++)            
                if (prevScan.cancelTokenSource.IsCancellationRequested||ranges[i] != ScanColor.ranges[i])
                    return false;                    
            return true;
        }
        public static void Cancel()
        {
            if (prevScan!=null)
            prevScan.cancelTokenSource.Cancel();
           // if ((prevScan.ResponseTime-DateTime.Now).TotalMilliseconds<50)
        }

        internal void Scan()
        {
            if (prevScan.cancelTokenSource.IsCancellationRequested || BlockScan)
            {
                BlockScan = false;
                return;
            }
            var lst = rec.Document.FindAll(EditorControl.CommonRegex);
            new Task(() => {              
                if ((ranges == null|| ranges.Length != lst.Length || !ComparsionRanges(lst))&& !prevScan.cancelTokenSource.IsCancellationRequested)               
                    SetDrawColorRanges.Invoke(lst);
            }, token,TaskCreationOptions.PreferFairness).Start();
        }
    }
    public class DBUriStreamProvider : IUriStreamProvider
    {
        static readonly string prefix = "dbimg://";
        DataTable table;
        string columnName;

        public DBUriStreamProvider(DataTable table, string columnName)
        {
            this.table = table;
            this.columnName = columnName;
        }

        #region IUriStreamProvider Members
        public Stream GetStream(string uri)
        {
            uri = uri.Trim();
            if (!uri.StartsWith(prefix))
                return null;
            string strId = uri.Substring(prefix.Length).Trim();
            int id;
            if (!int.TryParse(strId, out id))
                return null;
            DataRow row = table.Rows.Find(id);
            if (row == null)
                return null;
            byte[] bytes = row[columnName] as byte[];
            if (bytes == null)
                return null;
            return new MemoryStream(bytes);
        }
        #endregion
    }
}
