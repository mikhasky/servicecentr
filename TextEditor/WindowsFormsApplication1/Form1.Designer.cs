﻿namespace MKP
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.editorControl1 = new MKP.EditorControl();
            this.SuspendLayout();
            // 
            // editorControl1
            // 
            this.editorControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.editorControl1.DrawMacroses = false;
            //this.editorControl1.Landscape = false;
            this.editorControl1.Location = new System.Drawing.Point(0, 0);
           // this.editorControl1.MarginsBottom = 19.98486F;
            //this.editorControl1.MarginsFooter = 12.48833F;
            //this.editorControl1.MarginsHeader = 12.48833F;
            //this.editorControl1.MarginsLeft = 29.98611F;
            //this.editorControl1.MarginsRight = 14.99306F;
            //this.editorControl1.MarginsTop = 19.98486F;
            this.editorControl1.Name = "editorControl1";
            //this.editorControl1.Paper = System.Drawing.Printing.PaperKind.Letter;
            //this.editorControl1.RtfTextValue = resources.GetString("editorControl1.RtfTextValue");
            this.editorControl1.SetLinearVisibleHorizontal = DevExpress.XtraRichEdit.RichEditRulerVisibility.Hidden;
            this.editorControl1.SetLinearVisibleVertical = DevExpress.XtraRichEdit.RichEditRulerVisibility.Auto;
            this.editorControl1.SetVisiBleRIbbon = true;
            this.editorControl1.Size = new System.Drawing.Size(1083, 491);
            this.editorControl1.TabIndex = 0;
            //this.editorControl1.TextValue = "";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1083, 491);
            this.Controls.Add(this.editorControl1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        public EditorControl editorControl1;
    }
}

