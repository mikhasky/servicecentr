﻿using DevExpress.XtraEditors;
using DevExpress.XtraSpellChecker;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

namespace MKP.SpellCheckerWork
{
  public static class SpellWork
    {
        private static MemoEdit checker;
        public static SpellChecker spellChecker1;
        private static DictionaryCollection dCollection;
        public static bool isLoaded { get; set; }= false;
        /// <summary>
        /// Была ли команда на загрузку словаря, по умолчанию false - небыло
        /// </summary>
        public static bool isHaveLoadCommand { get; set; } = false;
        private static CultureInfo ruCulture= new CultureInfo("ru-RU");
        private static List<Control> ctrlCollection=new List<Control>();
        private static List<DevExpress.XtraRichEdit.RichEditControl> richCollection = new List<DevExpress.XtraRichEdit.RichEditControl>();        
        private static System.IO.Stream GetStream(string name)
        {
            return Assembly.GetExecutingAssembly().GetManifestResourceStream(name);            
        }
        public static void Load()
        {
            if (isLoaded)
                return;
            isHaveLoadCommand = true;
            // var f= Assembly.GetExecutingAssembly().GetManifestResourceStream("MKP.SpellDictionaries.ru_RU.aff");            
            checker = new MemoEdit();            
            spellChecker1 = new SpellChecker();
            spellChecker1.CheckCompleteFormShowing += Uncheck;
            spellChecker1.OptionsFormShowing += Uncheck;          
            spellChecker1.SpellingFormShowing += Uncheck;
            dCollection = new DictionaryCollection();
            spellChecker1.ParentContainer = checker;
            spellChecker1.Dictionaries.Clear();            
            SpellCheckerOpenOfficeDictionary openOfficeDictionaryRus = new SpellCheckerOpenOfficeDictionary();                
            openOfficeDictionaryRus.LoadFromStream(GetStream("MKP.SpellDictionaries.ru_RU.dic"), GetStream("MKP.SpellDictionaries.ru_RU.aff"), GetStream("MKP.SpellDictionaries.RussianAlphabet.txt"));
            openOfficeDictionaryRus.Culture = ruCulture;
           dCollection.Add(openOfficeDictionaryRus);
            SpellCheckerOpenOfficeDictionary openOfficeDictionaryEng = new SpellCheckerOpenOfficeDictionary();
            openOfficeDictionaryEng.LoadFromStream(GetStream("MKP.SpellDictionaries.en_US.dic"), GetStream("MKP.SpellDictionaries.en_US.aff"), GetStream("MKP.SpellDictionaries.EnglishAlphabet.txt"));
            openOfficeDictionaryEng.Culture = new CultureInfo("en-US");
            dCollection.Add(openOfficeDictionaryEng);
            spellChecker1.Dictionaries.AddRange(dCollection);
            spellChecker1.Culture = CultureInfo.InvariantCulture;//ruCulture;
            spellChecker1.SpellCheckMode = SpellCheckMode.AsYouType;
            spellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = true;
            spellChecker1.CheckAsYouTypeOptions.ShowSpellCheckForm = false;            
            isLoaded = true;
            ActiveWork();
        }
        
        private static void Uncheck(object sender, FormShowingEventArgs e)
        {
            e.Handled = true;
        }
        private static void ActiveWork()
        {
            while (ctrlCollection.Count != 0)
            {
                var ctrl = ctrlCollection.First();
                Register(ctrl);
                ctrlCollection.Remove(ctrl);
            }
            while (richCollection.Count != 0)
            {
                var ctrl = richCollection.First();
                Register(ctrl);
                richCollection.Remove(ctrl);
            }
        }
        //openOfficeDictionaryRus.DictionaryPath = @"SpellDictionaries\ru_RU.dic";
        // openOfficeDictionaryRus.GrammarPath = @"SpellDictionaries\ru_RU.aff";
        //openOfficeDictionaryRus.AlphabetPath = @"SpellDictionaries\RussianAlphabet.txt";
        /// <summary>
        /// Регистрация контрола для проверки раписания, если словарь ещё не загружен, то возвращает null
        /// </summary>
        public static SpellChecker Register(Control ctrl)
        {
            if (!isLoaded)
            {
                ctrlCollection.Add(ctrl);
                return null;
            }          
            SpellChecker chk = new SpellChecker();
            chk.Culture = CultureInfo.InvariantCulture; //new CultureInfo("ru-RU"); // CultureInfo.InvariantCulture;
            chk.Dictionaries.AddRange(dCollection);
            chk.SpellCheckMode = SpellCheckMode.AsYouType;
            chk.ParentContainer = ctrl;
            chk.CheckAsYouTypeOptions.CheckControlsInParentContainer = true;
            return chk;
        }
        public static SpellChecker Register(DevExpress.XtraRichEdit.RichEditControl ctrl)
        {
            if (!isLoaded)
            {
                richCollection.Add(ctrl);
                return null;
            }
            SpellChecker chk = new SpellChecker();
            chk.Culture = CultureInfo.InvariantCulture; //new CultureInfo("ru-RU"); // CultureInfo.InvariantCulture;
            chk.Dictionaries.AddRange(dCollection);
            chk.SpellCheckMode = SpellCheckMode.AsYouType;
            chk.ParentContainer = ctrl;            
            chk.CheckAsYouTypeOptions.CheckControlsInParentContainer = true;
            ctrl.SpellChecker = chk;
            return chk;
        }
        public static List<int> GetArrayErrors(string[] lines)
        {
            List<int> lst = new List<int>();            
            for (int i = 0,count=lines.Length; i < count; i++)
            {              
                //if (spellChecker1.CheckText(lines[i]).Count != 0)
                if (spellChecker1.IsMisspelledWord(lines[i], CultureInfo.InvariantCulture))
                    lst.Add(i);
            }
            return lst;      
        }
        public static bool isHaveError(string str)
        {
            return spellChecker1.IsMisspelledWord(str, CultureInfo.InvariantCulture);
        }

     

        public static List<int> GetArrayErrors(List<string> lines)
        {
            return GetArrayErrors(lines.ToArray());
        }
    }
}
