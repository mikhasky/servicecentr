﻿using System;
using System.Windows.Forms;

namespace MKP
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
           // SpellCheckerWork.SpellWork.Load();
            Application.Run(new FormTestMemoEditor());
            //Application.Run(new Form1());
        }
    }
}
