﻿using KOATUU.LoadMainData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KOATUU
{
    /// <summary>
    /// Базовый класс города
    /// </summary>
    public class BaseTowns
    {
        /// <summary>
        /// id города
        /// </summary>
        public string id;
        /// <summary>
        /// тип города (da2)
        /// </summary>
        public string type;
        /// <summary>
        /// Название города
        /// </summary>
        public string name;
        /// <summary>
        /// Конструктор города
        /// </summary>
        /// <param name="id">id города</param>
        /// <param name="type">тип города(da2 - с./м./смт.)</param>
        /// <param name="name">Название города</param>
        public BaseTowns(string id, string type, string name)
        {
            this.id = id;
            this.type = type;
            this.name = name;
        }
    }
    /// <summary>
    /// Базовый класс района/области
    /// </summary>
    public class Regions
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="id">айди</param>
        /// <param name="name">название</param>
        public Regions(string id, string name)
        {
            idRegion = id;this.name = Data.GetFirstUpper(name);           
        }
        public Regions(string id, string name, string type)
        {
            idRegion = id; this.name = Data.GetFirstUpper(name);
            this.type = type;
        }
        public string type;
        /// <summary>
        /// айди
        /// </summary>
        public string idRegion;
        /// <summary>
        /// название
        /// </summary>
        public string name;

    }
}
