﻿using FirebirdSql.Data.FirebirdClient;
using KOATUU.LoadMainData;
using LinqToDB;
using MKP.DBImplementation;
using Serializable;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace KOATUU
{
    public static class KOATUU_DATA
    {
        public static short GetDa15(string id)
        {
            if (id.EndsWith("10100000"))
                return 0;
            else if (id.EndsWith("100") || id.EndsWith("100"))
                return 1;
            else
                return 2;
        }
        /// <summary>
        /// получение типа города для da2
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static string getType(string type)
        {
            switch (type)
            {
                case "c":
                case "C":
                case "с":
                case "С":
                case "щ":
                case "Щ":
                    return "с.";
                case "T":
                case "t":
                case "Т":
                case "т": return "смт.";
                case "M":
                case "m":
                case "м":
                case "М": return "м.";
            }
            return string.Empty;
        }
        /// <summary>
        /// список необработанных городов, которые надо будет внести руками
        /// </summary>
        public static List<Regions> towns;
        /// <summary>
        /// список необработанных областей, которые надо будет внести руками
        /// </summary>
        public static List<Regions> regions;
        /// <summary>
        /// список необработанных районов, которые надо будет внести руками
        /// </summary>
        public static List<Regions> districts;
        private static void InsertDAO(string name, string dao7)
        {
            using (var db = new MKP.DataModels.BDETALONDB(Data.ConnectionString))
            {
                try
                {
                    db.BeginTransaction();
                    int id = db.Gen_ID<MKP.DataModels.DAO>();
                    var dao = new MKP.DataModels.DAO()
                    {
                        DAO1 = id,
                        DAO2 = 0,
                        DAO3 = name,
                        DAO4 = string.Empty,
                        DAO5 = 1,
                        DAO6 = string.Empty,
                        DAO7 = dao7

                    };
                    db.Insert(dao);
                    db.CommitTransaction();
                    dbMaster.FirstInsertOMG<DAO>(Data.tableDAO, new DAO() { DAO1 = id, DAO2 = 0, DAO3 = name, DAO4 = string.Empty, DAO5 = 1, DAO6 = string.Empty, DAO7 = dao7 });
                }
                catch (Exception)
                {
                    db.RollbackTransaction();
                    throw;
                }
            }
        }
        private static int GetDa1()
        {
            return WorkWhithDB.GetDataForQuery<int>("select GEN_ID(GEN_DA,1) as ID from rdb$database", Data.ConnectionString).Single();
        }
        private static void InsertDAR(string name, string dar6, int dar2)
        {
            using (var db = new MKP.DataModels.BDETALONDB(Data.ConnectionString))
            {
                try
                {
                    db.BeginTransaction();
                    int id = db.Gen_ID<MKP.DataModels.DAR>();
                    var dar = new MKP.DataModels.DAR()
                    {
                        DAR1 = id,
                        DAR2 = dar2,
                        DAR3 = name,
                        DAR4 = 1,
                        DAR5 = string.Empty,
                        DAR6 = dar6
                    };
                    db.Insert(dar);
                    db.CommitTransaction();
                    dbMaster.FirstInsertOMG<DAR>(Data.tableDAR, new DAR() { DAR1 = id, DAR2 = dar2, DAR3 = name, DAR4 = 1, DAR5 = string.Empty, DAR6 = dar6 });
                }
                catch (Exception)
                {
                    db.RollbackTransaction();
                    throw;
                }
            }
        }
        private static void InsertDA(string name, string da2, string da3, short da15, int da13, int da14, string da16)
        {
            using (var db = new MKP.DataModels.BDETALONDB(Data.ConnectionString))
            {
                try
                {
                    db.BeginTransaction();
                    int id = GetDa1();
                       // db.Gen_ID<MKP.DataModels.DA>();
                    var da = new MKP.DataModels.DA()
                    {
                        DA1 = id,
                        DA2 = da2,
                        DA3 = name.Length>30?name.Remove(30):name,
                        DA9 = DateTime.Now,
                        DA10 = 0,
                        DA12 = 0,
                        DA13 = da13,
                        DA14 = da14,
                        DA15 = da15,
                        DA16 = da16,
                        DA17 = string.Empty
                    };
                    db.Insert(da);
                    db.CommitTransaction();
                    dbMaster.FirstInsertOMG<DA>(Data.tableDA, new DA() {
                        DA1 = id,
                        DA2 = da2,
                        DA3 = da.DA3,
                        DA9 = DateTime.Now,
                        DA10 = 0,
                        DA12 = 0,
                        DA13 = da13,
                        DA14 = da14,
                        DA15 = da15,
                        DA16 = da16
                    });
                }
                catch (Exception)
                {
                    db.RollbackTransaction();
                    throw;
                }
            }
        }
        private static void AdaptationRegions()
        {
            List<Regions> lst = regions;
            for (int a = 0; a < lst.Count; a++)
            {
                var r = Data.tableDAO.Where(i => i.DAO7 == lst[a].idRegion).FirstOrDefault();
                if (r == null)
                    r = Data.tableDAO.Where(i => i.DAO3.ToLower() == lst[a].name.ToLower()).FirstOrDefault();
                if (r == null)
                    InsertDAO(lst[a].name, lst[a].idRegion);
                else
                    Data.UpdateDAO(lst[a].idRegion, lst[a].name, r.DAO1.ToString());
               // lst.RemoveAt(a); a--;
            }
            regions = lst;
            // districts.Clear();
        }
        private static void AdaptationDistricts()
        {
            List<Regions> lst = districts;
            for (int a = 0; a < lst.Count; a++)
            {
                string dao7 = lst[a].idRegion.Remove(2);
                var _dao = Data.tableDAO.Where(i => i.DAO7 == dao7).FirstOrDefault();
                if (_dao == null)
                {
                    continue;
                }
                var r = (from dar in Data.tableDAR.Where(i => i.DAR6 == lst[a].idRegion && i.DAR2 == _dao.DAO1) select dar).FirstOrDefault();
                if (r == null)
                    r = (from dar in Data.tableDAR.Where(i => i.DAR3.ToLower() == lst[a].name.ToLower() && i.DAR2 == _dao.DAO1) select dar).FirstOrDefault();
                if (r == null)
                    InsertDAR(lst[a].name, lst[a].idRegion, _dao == null ? 0 : _dao.DAO1);
                else
                    Data.UpdateDAR(lst[a].idRegion, lst[a].name, r.DAR1.ToString());
                //lst.RemoveAt(a); a--;
            }
            districts = lst;
        }
        private static void AdaptationTowns()
        {
            List<Regions> lst = towns.Where(i => i.idRegion.EndsWith("00000")).ToList();
    
            TownUp(lst);
            lst = null;
            towns = towns.Where(i => !i.idRegion.EndsWith("00000")).ToList();
            List<Regions> rc_lst = towns.Where(i => i.idRegion.EndsWith("100")).ToList();
            TownUpRc(rc_lst);
            rc_lst = null;
            towns = towns.Where(i => !i.idRegion.EndsWith("100")).ToList();
            List<Regions> _lst = towns.Where(i => i.idRegion[2] == '2').ToList();
            TownOther(_lst);
            _lst = null;
            towns = towns.Where(i => i.idRegion[2] != '2').ToList();
            TownNext(towns);
            towns.Clear();
            // towns.AddRange(lst);
            // towns.AddRange(_lst);
            // towns.AddRange(_lst);
        }

        private static void TownNext(List<Regions> _lst)
        {
            int len = _lst.Count;
            for (int a = 0; a < len; a++)
            {
                string dao7 = _lst[a].idRegion.Remove(2);
                var _dao = Data.tableDAO.Where(i => i.DAO7 == dao7).FirstOrDefault();
                if (_dao == null)
                {
                    continue;
                }
                var r = (from da in Data.tableDA.Where(i => i.DA16 == _lst[a].idRegion && i.DA13 == _dao.DAO1) select da).FirstOrDefault();
                if (r == null)
                    r = (from da in Data.tableDA.Where(i => i.DA3.ToLower() == _lst[a].name.ToLower() && i.DA13 == _dao.DAO1 && string.IsNullOrEmpty(i.DA16)) select da).FirstOrDefault();
                if (r == null)
                    InsertDA(_lst[a].name, getType(_lst[a].type), _lst[a].idRegion, 3, _dao.DAO1, 0, _lst[a].idRegion);
                else if (Data.tableDA.Where(i => i.DA3.ToLower() == _lst[a].name.ToLower() && i.DA16 == _lst[a].idRegion&&i.DA15==3&&i.DA14==0).FirstOrDefault() == null)
                    Data.UpdateDa(_lst[a].name, _lst[a].idRegion, r.DA1.ToString(), "3","0", getType(_lst[a].type));
               // _lst.RemoveAt(a);
               // a--;
            }
        }

        private static void TownUpRc(List<Regions> lst)
        {
            for (int a = 0; a < lst.Count; a++)
            {
                string dao7 = lst[a].idRegion.Remove(2);
                var _dao = Data.tableDAO.Where(i => i.DAO7 == dao7).FirstOrDefault();
                string dar6 = lst[a].idRegion.Remove(5);
                var _dar = Data.tableDAR.Where(i => i.DAR6 == dar6).FirstOrDefault();
                if (_dao == null || _dar == null)
                {
                    continue;
                }
                var r = (from da in Data.tableDA.Where(i => i.DA16 == lst[a].idRegion) select da).FirstOrDefault();
                if (r == null)
                    r = (from da in Data.tableDA.Where(i => i.DA3.ToLower() == lst[a].name.ToLower() && i.DA13 == _dao.DAO1 && i.DA14 == _dar.DAR1&&string.IsNullOrEmpty(i.DA16)) select da).FirstOrDefault();
                if (r == null)
                    InsertDA(lst[a].name, getType(lst[a].type), lst[a].idRegion, GetDa15(lst[a].idRegion), _dao.DAO1, 0, lst[a].idRegion);
                else
                    if (Data.tableDA.Where(i => i.DA3.ToLower() == lst[a].name.ToLower() && i.DA16 == lst[a].idRegion && i.DA15 == 1 && i.DA14 == _dar.DAR1).FirstOrDefault() == null)
                    Data.UpdateDa(lst[a].name, lst[a].idRegion, r.DA1.ToString(), "1", _dar.DAR1.ToString(), getType(lst[a].type));
               // lst.RemoveAt(a);
              //  a--;
            }
        }

        private static void TownOther(List<Regions> _lst)
        {
            int len = _lst.Count;
            for (int a = 0; a < len; a++)
            {              
                string dao7 = _lst[a].idRegion.Remove(2);
                var _dao = Data.tableDAO.Where(i => i.DAO7 == dao7).FirstOrDefault();
                string dar6 = _lst[a].idRegion.Remove(5);
                var _dar = Data.tableDAR.Where(i => i.DAR6 == dar6).FirstOrDefault();
                if (_dao == null || _dar == null)
                {
                    continue;
                }
                var r = (from da in Data.tableDA.Where(i => i.DA16 == _lst[a].idRegion && i.DA14 == _dar.DAR1 && i.DA13 == _dao.DAO1) select da).FirstOrDefault();
                if (r == null)
                    r = (from da in Data.tableDA.Where(i => i.DA3.ToLower() == _lst[a].name.ToLower() && i.DA14 == _dar.DAR1 && i.DA13 == _dao.DAO1 && string.IsNullOrEmpty(i.DA16)) select da).FirstOrDefault();
                if (r == null)
                    InsertDA(_lst[a].name, getType(_lst[a].type), _lst[a].idRegion, GetDa15(_lst[a].idRegion), _dao.DAO1, _dar.DAR1, _lst[a].idRegion);
                else if (Data.tableDA.Where(i=>i.DA3.ToLower()== _lst[a].name.ToLower()&&i.DA16== _lst[a].idRegion&&i.DA15==2&&i.DA14==_dar.DAR1).FirstOrDefault()==null)
                    Data.UpdateDa(_lst[a].name, _lst[a].idRegion, r.DA1.ToString(), "2",_dar.DAR1.ToString(), getType(_lst[a].type));
               //_lst.RemoveAt(a);
               // a--;
            }
        }

        private static void TownUp(List<Regions> lst)
        {
            string da2;
            for (int a = 0; a < lst.Count; a++)
            {                
                string dao7 = lst[a].idRegion.Remove(2);
                var _dao = Data.tableDAO.Where(i => i.DAO7 == dao7).FirstOrDefault();   
                // string dar6 = lst[a].idRegion.Remove(5);
                // var _dar = Data.tableDAR.Where(i => i.DAR6 == dar6).FirstOrDefault();
                if (_dao == null)//|| _dar == null
                {
                    continue;
                }
                da2 = getType(lst[a].type);
                if (string.IsNullOrEmpty(da2))
                    da2 = "м.";
                var r = (from da in Data.tableDA.Where(i => i.DA16 == lst[a].idRegion) select da).FirstOrDefault();
                if (r == null)
                    r = (from da in Data.tableDA.Where(i => i.DA3.ToLower() == lst[a].name.ToLower() && i.DA13 == _dao.DAO1&& string.IsNullOrEmpty(i.DA16)) select da).FirstOrDefault();
                if (r == null)
                {                    
                    InsertDA(lst[a].name, da2, lst[a].idRegion, GetDa15(lst[a].idRegion), _dao.DAO1, 0, lst[a].idRegion);
                }
                else
                {
                    Data.UpdateDa(lst[a].name, lst[a].idRegion, r.DA1.ToString(), "0", "0", da2, _dao.DAO1.ToString());
                }
               // lst.RemoveAt(a);
               // a--;
            }
        }

        private static void Adaptation()
        {
            regions = new List<Regions>();
            districts = new List<Regions>();
            towns = new List<Regions>();
            string id, name, type;
            foreach (DataRow dr in Data.DT.Rows)
            {
                name = dr[2].ToString();
                if (name == "nu")
                    continue;               
                id = dr[0].ToString();
                if (id.Length == 9)
                    id = id.Insert(0, "0");
                if (id == "0510100000")
                    id = id;
                //if (id == "5310100000")
                //    id = id;
                type = dr[1].ToString();
               /* if (id.StartsWith("01"))
                    continue;*/
                if (id.EndsWith("00000000"))
                    FillRegion(name, id.Remove(2), type);
                else
                    if (id.EndsWith("00000") && (!Data.towns.Contains(type)||type==string.Empty) && id[2]!='1' && !id.EndsWith("0000000"))
                {
                    FillDistrict(name, id.Remove(5), type);
                }
                else
                    FillTown(name, id, type);
            }
            AdaptationRegions();
            AdaptationDistricts();
            AdaptationTowns();
        }
        private static void FillRegion(string name, string id, string type)
        {
            if (Data._blackDAO.Where(i => i.StartsWith(id)).FirstOrDefault() != null /*|| id.StartsWith("01")*/)
                return;
            int idx = name.IndexOf(" область");
            if (idx > 0)
                name = name.Remove(idx);
            // else
            //   return;
            regions.Add(new Regions(id, name.Replace('i', 'і').Replace('I', 'І'), type));
        }
        private static void FillDistrict(string name, string id, string type)
        {
            int oblid = Convert.ToInt32(id);
            /*oblid < 1300 || oblid >= 85000 ||*/
            if (name.StartsWith("міста обласного"))
                return;
            int idx = name.IndexOf(" район");
            if (idx > 0)
                name = name.Remove(idx);
            districts.Add(new Regions(id, name.Replace('i', 'і').Replace('I', 'І'), type));
        }
        private static void FillTown(string name, string id, string type)
        {
            if (!Data.towns.Contains(type) || name.Contains("/") || name.Contains("типу") || name.Contains("міста")||name.Contains("підпорядковані")||name.Contains("сільради")||name.Contains("райони ")||name.Contains("пункти") || name.Contains("селища"))
                return;
            towns.Add(new Regions(id, name.Replace('i', 'і').Replace('I', 'І'), type));
        }
        /// <summary>
        /// Сканирование данных, загруженных с документа
        /// </summary>
        public static void Start(string ConnectionBD,Form1 frm=null)
        {
            Data.ConnectionString = ConnectionBD;
            Data.LoadFile();
            Adaptation();
           /* for (int i = 0; i < Data.DT.Rows.Count; i++)            
                findRegions(Data.DT.Rows[i]);
            for (int i = 0; i < Data.DT.Rows.Count; i++)
                findDistricts(Data.DT.Rows[i]);
            for (int i = 0; i < Data.DT.Rows.Count; i++)
                Work(Data.DT.Rows[i]);*/
          /*  if (regions.Count == 0 && districts.Count == 0 && towns.Count == 0)
                MessageBox.Show("Синхронизация выполнена успешно!");
            else
            {
                if (regions.Count != 0)
                {
                    MessageBox.Show("Обнаружены новые области, привяжите к существующим областям или создайте новые");
                }
                if (districts.Count != 0)
                {
                    MessageBox.Show("Обнаружены новые районы, привяжите к существующим районами или создайте новые");
                }
                if (towns.Count != 0)
                {
                    MessageBox.Show("Обнаружены новые города/сёла, привяжите к существующим городам/сёлам или создайте новые");
                }
            }
            if (frm == null)
            {
                frm = new Form1();
                frm.FillMerger();
                frm.Show();
                frm.Visible = true;
            }
            else
            {
                frm.FillMerger();
                frm.Visible = true;
            }*/
        }
        /// <summary>
        /// ищем район в указанной строке
        /// </summary>
        /// <param name="dr"></param>
        public static void findDistricts(DataRow dr)
        {
            districts = new List<Regions>();
            {
                string type = dr[1].ToString();
                string oblid = dr[0].ToString();
                //проверяем, является ли запись районом, и не является ли областью 
                if (oblid.Contains("00000000") || Data.rags.Contains(type)|| !oblid.EndsWith("00000"))
                    return;
                //убираем лишний участок кода района
                oblid = oblid.Remove(5);
                //коды ниже 1300 - крымские, игнорируем
                if (Convert.ToInt32(oblid) < 1300)
                    return;
                string name = dr[2].ToString().ToLower();
                //Убираем приписку район
                int idx = name.IndexOf(" район");
                if (idx > 0)
                    name = name.Remove(idx);
                else
                    return;
                //получаем айди области из айди района        
                string dao7 = oblid.Remove(2);
                //ищем район по айди или названию, подключая область с айди
                var dar = (from _dar in Data.tableDAR.Where(j => j.DAR6 == oblid || j.DAR3.ToLower() == name.ToLower())
                           join dao in Data.tableDAO.Where(j => j.DAO7 == dao7) on _dar.DAR2 equals dao.DAO2
                           select _dar).FirstOrDefault();
                //если не нашли
                if (dar == null)
                {                     
                    dar = Data.tableDAR.Where(j => j.DAR6 == oblid).FirstOrDefault();
                    if (dar == null)
                    {
                        districts.Add(new Regions(oblid, Data.GetFirstUpper(name)));
                        return;
                    }                   
                     else
                        Data.UpdateDAR(oblid, Data.GetFirstUpper(name), dar.DAR1.ToString());
                    // dar = Data.tableDAR.Where(j => j.DAR3.ToLower() == name.ToLower()).FirstOrDefault();
                    // if (dar==null)
                    // districts.Add(new Regions(oblid, Data.GetFirstUpper(name)));
                    // else
                    //   Data.UpdateDAR(oblid, Data.GetFirstUpper(name), dar.DAR1.ToString/());
                }
                else
                    Data.UpdateDAR(oblid, Data.GetFirstUpper(name), dar.DAR1.ToString());

            }
        }

        /// <summary>
        /// ищем области
        /// </summary>
        /// <param name="dr"></param>
        public static void findRegions(DataRow dr)
        {
            regions = new List<Regions>();
            {
                //является ли запись областью
                if (!dr[0].ToString().Contains("00000000"))
                    return;
                //получаем айди области
                string oblid = dr[0].ToString().Replace("00000000", string.Empty);
                //проверяем не лежит ли область в чёрном списке
                if (Data._blackDAO.Contains(oblid))
                    return;
                //имя области
                string name = dr[2].ToString().ToLower();
                //убираем приписку область
                int idx = name.IndexOf(" область");
                if (idx > 0)
                    name = name.Remove(idx);
                //ищем область по айди или названию          
                var dao = Data.tableDAO.Where(j => j.DAO7 == oblid || j.DAO3.ToLower() == name.ToLower()).FirstOrDefault();
                //если не нашли - добавляем в список необработанных областей
                if (dao == null)
                    regions.Add(new Regions(oblid, Data.GetFirstUpper(name)));
                else
                {
                    //обновляем данные области если нашли
                    Data.UpdateDAO(oblid, Data.GetFirstUpper(name), dao.DAO1.ToString());
                    // dao.DAO3 = name;
                    // dao.DAO7 = oblid;
                }
            }
        }
        /// <summary>
        /// ищем города
        /// </summary>
        /// <param name="dr"></param>
        public static void Work(DataRow dr)
        {
           /* towns = new List<BaseTowns>();
            {
                //проверяем, есть ли в белом списке тип города
                if (!Data.towns.Contains(dr[1].ToString()))
                    return;
                //ищем город в бд
                if (Data.FindDa(dr))
                    return;
                //если не нашли - добавляем в список необработанных городов 
                towns.Add(new BaseTowns(dr[0].ToString(), dr[1].ToString(), dr[2].ToString()));
            }*/
        }
    }
}
