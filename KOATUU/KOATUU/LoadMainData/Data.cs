﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.IO;
using System.IO.Compression;
using Ionic.Zip;
using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using Serializable;
using FirebirdSql.Data.FirebirdClient;

namespace KOATUU.LoadMainData
{
  public static class Data
    {
        #region переменные
        /// <summary>
        /// строка подключения
        /// </summary>
        public static string ConnectionString = @"User = MKP;Password = 17051945;Database = D:\BD\BD;DataSource = 127.0.0.1;Port = 3050;Dialect = 3;Charset = WIN1251;Role =;Connection lifetime = 15;Pooling = true;MinPoolSize = 0;MaxPoolSize = 50;Packet Size = 8192;ServerType = 0";
        /// <summary>
        /// Города
        /// </summary>
        public static List<DA> tableDA;
        /// <summary>
        /// район
        /// </summary>
        public static List<DAR> tableDAR;
        /// <summary>
        /// области
        /// </summary>
        public static List<DAO> tableDAO;
        /// <summary>
        /// страна
        /// </summary>
        public static List<DAS> tableDAS;
        /// <summary>
        /// ссылка на архив КОАТУУ
        /// </summary>
        private const string _linkKOATUU = @"http://www.ukrstat.gov.ua/klasf/st_kls/koatuu.zip";
        /// <summary>
        /// Содержимое КОАТУУ из ексель файла
        /// </summary>
        public static DataTable DT;
        /// <summary>
        /// чёрный список областей по номеру КОАТУУ
        /// </summary>
        public static List<string> _blackDAO = new List<string>() {"85"};
        #endregion/// <summary>
        #region загрузчик
        /// Загруза базы данных, заполнение дататейбла, загрузка и распаковка архива КОАТУУ
        /// </summary>
        public static void Loader()
        {
            if (ConnectionString == null)
                return;
            //Загрузка БД
            if (tableDA==null)
            tableDA = dbMaster.GetData<DA>("select * from da",ConnectionString);
            if (tableDAR == null)
                tableDAR = dbMaster.GetData<DAR>("select * from dar", ConnectionString);
            if (tableDAO == null)
                tableDAO = dbMaster.GetData<DAO>("select * from dao", ConnectionString);
            if (tableDAS == null)
                tableDAS = dbMaster.GetData<DAS>("select * from das order by das1", ConnectionString);
            tableDAO.ForEach(i => i.DAO3=i.DAO3.Replace('i', 'і').Replace('I', 'І'));
            tableDAR.ForEach(i => i.DAR3=i.DAR3.Replace('i', 'і').Replace('I', 'І'));
            tableDA.ForEach(i => i.DA3=i.DA3.Replace('i', 'і').Replace('I', 'І'));
            //делаем названия городов маленькими
            //  for (int i = 0; i < tableDA.Count; i++)
            //   tableDA[i].DA3 = tableDA[i].DA3.ToLower().Trim();

            string separator = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
           
        }
        /// <summary>
        /// установка значений string.Empty для null значений
        /// </summary>
        /// <param name="dgvr"></param>
        public static void SetEmptyRow(System.Windows.Forms.DataGridViewRow dgvr)
        {
            for (int i = 0; i < dgvr.Cells.Count; i++)
            {
                if (dgvr.Cells[i].Value == null) dgvr.Cells[i].Value = string.Empty;
            }
        }
        //загрузка данных
        public static void LoadFile()
        {
            WebClient webClient = new WebClient();
            //загрузка архива КОАТУУ
            try
            {
                webClient.DownloadFile(_linkKOATUU,
                "koatuu.zip");
            }
            catch (System.Net.WebException webEx)
            {
                System.Windows.Forms.MessageBox.Show("Отсутствует подключение к оф. сайту КОАТУУ");
                return;
            }
            var zip = new ZipFile("koatuu.zip", Encoding.GetEncoding(866));
            try
            {
                //Распакова архива
                zip.ExtractAll("koatuu", ExtractExistingFileAction.OverwriteSilently);
            }
            catch (System.IO.IOException e)
            {
                //Удаление директории, если при распаковке произошла ошибка
                Directory.Delete("koatuu", true);
                //Повторная распаковка
                zip.ExtractAll("koatuu", ExtractExistingFileAction.OverwriteSilently);
            }
            //Находим самый большой файл, который является файлом КОАТУУ
            string[] files = Directory.GetFiles("koatuu");
            string file = string.Empty;
            long leng = 0;
            for (int i = 0; i < files.Length; i++)
            {
                var fi = (new System.IO.FileInfo(files[i]));
                var l = fi.Length;
                if (leng < l)
                {
                    file = fi.FullName;
                    leng = l;
                }
            }
            Excel.Application xlApp_P;
            Excel.Workbook xlWorkBook_P;
            Excel.Worksheet xlWorkSheet_P;
            Excel.Range range_P;
            xlApp_P = new Excel.Application();
            xlWorkBook_P = xlApp_P.Workbooks.Open(file, 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
            xlWorkSheet_P = (Excel.Worksheet)xlWorkBook_P.Worksheets.get_Item(1);
            range_P = xlWorkSheet_P.UsedRange;
            object[,] data = range_P.Value2;
            DT = new DataTable();
            //Заполняем дататейбл с екселя
            // Create new Column in DataTable
            for (int cCnt = 1; cCnt <= range_P.Columns.Count; cCnt++)
            {
                var Column = new DataColumn();
                Column.DataType = System.Type.GetType("System.String");
                Column.ColumnName = cCnt.ToString();
                DT.Columns.Add(Column);
                double ConvertVal;
                // Create row for Data Table
                for (int rCnt = 1; rCnt <= range_P.Rows.Count; rCnt++)
                {
                    string CellVal = String.Empty;
                    try
                    {
                        CellVal = Convert.ToString((data[rCnt, cCnt]));
                    }
                    catch (Microsoft.CSharp.RuntimeBinder.RuntimeBinderException)
                    {
                        ConvertVal = (double)(data[rCnt, cCnt]);
                        CellVal = ConvertVal.ToString();
                    }
                    catch (System.InvalidCastException)
                    {
                        ConvertVal = (double)(data[rCnt, cCnt]);
                        CellVal = ConvertVal.ToString();
                    }
                    DataRow Row;
                    // Add to the DataTable
                    if (cCnt == 1)
                    {
                        Row = DT.NewRow();
                        Row[cCnt.ToString()] = CellVal;
                        DT.Rows.Add(Row);
                    }
                    else
                    {
                        Row = DT.Rows[rCnt - 1];
                        Row[cCnt.ToString()] = !string.IsNullOrEmpty(CellVal) ? CellVal.ToLower().Trim() : string.Empty;
                    }
                }
            }
        }
        /// <summary>
        /// обновление таблицы районов в БД
        /// </summary>
        /// <param name="KOATUU"></param>
        /// <param name="name"></param>
        /// <param name="dar1"></param>
        public static void UpdateDAR(string KOATUU, string name, string dar1)
        {
            name = name.Length <= 30 ? name : name.Remove(30);
            using (FbConnection fConnection = new FbConnection(Data.ConnectionString))
            {
                fConnection.Open();
                using (FbTransaction fTrans = fConnection.BeginTransaction())
                {
                    using (FbCommand fCommand = new FbCommand(@"UPDATE dar
                            SET dar3 = @p1,dar6=@p2 WHERE dar1 = @p3", fConnection, fTrans))
                    {
                        fCommand.Parameters.Add("@p1", name);
                        fCommand.Parameters.Add("@p2", KOATUU);
                        fCommand.Parameters.Add("@p3", dar1);
                        fCommand.ExecuteNonQuery();
                        fTrans.Commit();
                        dbMaster.UpdateOMG<DAR>(tableDAR, "dar3", name, dar1);
                        dbMaster.UpdateOMG<DAR>(tableDAR, "dar6", KOATUU, dar1);
                    }
                }
            }
        }
        /// <summary>
        /// Получаем название строки с большой буквы
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string GetFirstUpper(string name)
        {
            return name[0].ToString().ToUpper() + name.Remove(0, 1);
        }
        #endregion 
        #region фильтры
        /// <summary>
        /// Чёрный список символов по которым игнорируются районы с кодом района 
        /// </summary>
        public static string[] rags = new string[] { "м", "m", "М", "М", "P", "p", "р", "Р" };
        /// <summary>
        /// Белый список фильтра городов, символы, которые стоят в екселе напротив городов/сёл/смт
        /// </summary>
        public static string[] towns = new string[] {"C","С","Щ","Т","T","М","М","с","c", "щ", "т", "t", "м", "m","" };
        #endregion/// <summary>
        /// Поиск полученного из файла города в базе по коду области, района, города или имени, является ли городом
        /// </summary>
        /// <param name="dr"></param>
        /// <returns></returns>
        public static bool FindDa(DataRow dr)
        {
            string name = dr[2].ToString();
            string id = dr[0].ToString();
            string idRegion = id.Remove(2);
            string idObl = id.Remove(5);
            var find = (from da in tableDA.Where(i => i.DA3.ToLower().Trim() == name.ToLower().Trim() || i.DA16 == id)
                        join dar in tableDAR.Where(i=>i.DAR6==idObl) on da.DA14 equals dar.DAR1
                        join dao in tableDAO.Where(i => i.DAO7 == idRegion) on dar.DAR2 equals dao.DAO1
                        select da).FirstOrDefault();
            if (find == null)
            {
                find = (from da in tableDA.Where(i => i.DA3 == name.ToLower() || i.DA16 == id)
                            join dar in tableDAR on da.DA14 equals dar.DAR1
                            join dao in tableDAO.Where(i => i.DAO7 == idRegion) on dar.DAR2 equals dao.DAO1
                            select da).FirstOrDefault();
                /*  if (find == null)
                  {
                      find = (from da in tableDA.Where(i => i.DA16 == id) select da).FirstOrDefault();
                  }*/
                if (find == null)
                {
                    if (!string.IsNullOrEmpty(id.Trim()))
                    find = tableDA.Where(i => i.DA16 == id).FirstOrDefault();
                    if (find==null)
                    return false;
                }
            }
            string da1 = find.DA1.ToString();
            UpdateDa(Data.GetFirstUpper(name), id, da1);
            return true;
        }
        /// <summary>
        /// Обновление информации о городе
        /// </summary>
        /// <param name="name"></param>
        /// <param name="id"></param>
        /// <param name="da1"></param>
        public static void UpdateDa(string name, string da16, string da1)
        {
            string da2 = string.Empty;
            SetDa(ref da2, ref name);
            name = name.Length <= 30 ? name : name.Remove(30);
            using (FbConnection fConnection = new FbConnection(ConnectionString))
            {
                fConnection.Open();
                using (FbTransaction fTrans = fConnection.BeginTransaction())
                {
                    using (FbCommand fCommand = new FbCommand(@"UPDATE da
                            SET da16 = @p1,da3=@da3,da2=@da2 WHERE da1 = @p2", fConnection, fTrans))
                    {
                        fCommand.Parameters.Add("@p1", da16);
                        fCommand.Parameters.Add("@p2", da1);
                        fCommand.Parameters.Add("@da2", da2);
                        fCommand.Parameters.Add("@da3", name);                    
                        fCommand.ExecuteNonQuery();
                        fTrans.Commit();                        
                        dbMaster.UpdateOMG<DA>(tableDA,"da16", da16, da1);
                        dbMaster.UpdateOMG<DA>(tableDA, "da3", name, da1);
                    }
                }
            }
        }
        public static void UpdateDa(string name, string da16, string da1, string da15 = "")
        {
            string da2 = string.Empty;
            SetDa(ref da2, ref name);
            name = name.Length <= 30 ? name : name.Remove(30);
            using (FbConnection fConnection = new FbConnection(ConnectionString))
            {
                fConnection.Open();
                using (FbTransaction fTrans = fConnection.BeginTransaction())
                {
                    using (FbCommand fCommand = new FbCommand(@"UPDATE da
                            SET da16 = @p1,da3=@da3,da2=@da2,da15=@da15 WHERE da1 = @p2", fConnection, fTrans))
                    {
                        fCommand.Parameters.Add("@p1", da16);
                        fCommand.Parameters.Add("@p2", da1);
                        fCommand.Parameters.Add("@da2", da2);
                        fCommand.Parameters.Add("@da3", name);
                        fCommand.Parameters.Add("@da15", da15);
                        fCommand.ExecuteNonQuery();
                        fTrans.Commit();
                        dbMaster.UpdateOMG<DA>(tableDA, "da15", da15, da1);
                        dbMaster.UpdateOMG<DA>(tableDA, "da16", da16, da1);
                        dbMaster.UpdateOMG<DA>(tableDA, "da3", name, da1);
                    }
                }
            }
        }
        public static void UpdateDa(string name, string da16, string da1, string da15,string dar1,string _da2)
        {
            string da2 = _da2;
            if (string.IsNullOrEmpty(da2))
            SetDa(ref da2, ref name);
            name = name.Length <= 30 ? name : name.Remove(30);
            using (FbConnection fConnection = new FbConnection(ConnectionString))
            {
                fConnection.Open();
                using (FbTransaction fTrans = fConnection.BeginTransaction())
                {
                    using (FbCommand fCommand = new FbCommand(@"UPDATE da
                            SET da16 = @p1,da3=@da3,da2=@da2,da15=@da15,da14=@dar1 WHERE da1 = @p2", fConnection, fTrans))
                    {
                        fCommand.Parameters.Add("@p1", da16);
                        fCommand.Parameters.Add("@p2", da1);
                        fCommand.Parameters.Add("@da2", da2);
                        fCommand.Parameters.Add("@da3", name);
                        fCommand.Parameters.Add("@dar1",dar1);
                        fCommand.Parameters.Add("@da15", da15);
                        fCommand.ExecuteNonQuery();
                        fTrans.Commit();
                        dbMaster.UpdateOMG<DA>(tableDA, "da2", da2, da1);
                        dbMaster.UpdateOMG<DA>(tableDA, "da14", dar1, da1);
                        dbMaster.UpdateOMG<DA>(tableDA, "da15", da15, da1);
                        dbMaster.UpdateOMG<DA>(tableDA, "da16", da16, da1);              
                        dbMaster.UpdateOMG<DA>(tableDA, "da3", name, da1);
                    }
                }
            }
        }
        public static void UpdateDa(string name, string da16, string da1, string da15, string dar1, string _da2,string dao)
        {
            string da2 = _da2;
            if (string.IsNullOrEmpty(da2))
                SetDa(ref da2, ref name);
            name = name.Length <= 30 ? name : name.Remove(30);
            using (FbConnection fConnection = new FbConnection(ConnectionString))
            {
                fConnection.Open();
                using (FbTransaction fTrans = fConnection.BeginTransaction())
                {
                    using (FbCommand fCommand = new FbCommand(@"UPDATE da
                            SET da16 = @p1,da3=@da3,da2=@da2,da15=@da15,da14=@dar1,da13=@da13 WHERE da1 = @p2", fConnection, fTrans))
                    {
                        fCommand.Parameters.Add("@p1", da16);
                        fCommand.Parameters.Add("@p2", da1);
                        fCommand.Parameters.Add("@da2", da2);
                        fCommand.Parameters.Add("@da3", name);
                        fCommand.Parameters.Add("@dar1", dar1);
                        fCommand.Parameters.Add("@da15", da15);
                        fCommand.Parameters.Add("@da13", dao);
                        fCommand.ExecuteNonQuery();
                        fTrans.Commit();
                        dbMaster.UpdateOMG<DA>(tableDA, "da2", da2, da1);
                        dbMaster.UpdateOMG<DA>(tableDA, "da13", dao, da1);
                        dbMaster.UpdateOMG<DA>(tableDA, "da14", dar1, da1);
                        dbMaster.UpdateOMG<DA>(tableDA, "da15", da15, da1);
                        dbMaster.UpdateOMG<DA>(tableDA, "da16", da16, da1);
                        dbMaster.UpdateOMG<DA>(tableDA, "da3", name, da1);
                    }
                }
            }
        }
        private static void SetDa(ref string da2, ref string name)
        {
            if (name.Contains("г."))
            {
                da2 = "г.";
                name = name.Replace(da2, string.Empty);
            }
            else
               if (name.Contains("м."))
            {
                da2 = "м.";
                name = name.Replace(da2, string.Empty);
            }
            else
            if (name.Contains("смт."))
            {
                da2 = "смт.";
                name = name.Replace(da2, string.Empty);
            }
            else
                 if (name.Contains("с."))
            {
                da2 = "с.";
                name = name.Replace(da2, string.Empty);
            }
            else
                 if (name.Contains("г."))
            {
                da2 = "г.";
                name = name.Replace(da2, string.Empty);
            }
            name = name.Trim();
        }
        /// <summary>
        /// Обновление информации о Области
        /// </summary>
        /// <param name="oblid"></param>
        /// <param name="name"></param>
        /// <param name="dao1"></param>
        public static void UpdateDAO(string oblid, string name, string dao1)
        {
            name = name.Length <= 50 ? name : name.Remove(50);
            using (FbConnection fConnection = new FbConnection(Data.ConnectionString))
            {
                fConnection.Open();
                using (FbTransaction fTrans = fConnection.BeginTransaction())
                {
                    using (FbCommand fCommand = new FbCommand(@"UPDATE dao
                            SET dao3 = @p1,dao7=@p2 WHERE dao1 = @p3", fConnection, fTrans))
                    {
                        fCommand.Parameters.Add("@p1", name);
                        fCommand.Parameters.Add("@p2", oblid);
                        fCommand.Parameters.Add("@p3", dao1);
                        fCommand.ExecuteNonQuery();
                        fTrans.Commit();
                        dbMaster.UpdateOMG<DAO>(tableDAO,"dao3",name,dao1);
                        dbMaster.UpdateOMG<DAO>(tableDAO, "dao7", oblid, dao1);
                    }
                }
            }
        }
        /// <summary>
        /// Получение айди
        /// </summary>
        /// <param name="table"></param>
        /// <returns></returns>
        public static int GenId(string table)
        {
            string field = table + "1", rez;
            FbConnection fConnection = new FbConnection(Data.ConnectionString);

            string sqlQuery = "";

            if (table == "u" || table == "uo" || table == "us" || table == "nr" || table == "gen_vvmp" || table == "st" || table == "std" || table == "sk" || table == "stpr" || table == "stpr2" || table == "gen_nkrs")
                sqlQuery = "select GEN_ID(" + table + ",1) from rdb$database";
            else
                sqlQuery = "SELECT first 1 id1 FROM pr1('" + field + "','" + table + "') left join " + table + " on (" + field + "=id1) where " + field + " is null";
            fConnection.Open();
            FbCommand myCommand = new FbCommand(sqlQuery, fConnection);
            FbDataReader dat = myCommand.ExecuteReader();
            if (dat.Read())
                rez = dat.GetString(0);
            else
                rez = "1";
            fConnection.Close();
            if (rez == string.Empty)
                return 0;
            else
                return int.Parse(rez);
        }
    }
}
