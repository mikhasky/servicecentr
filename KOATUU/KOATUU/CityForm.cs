﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace KOATUU
{
    public partial class CityForm : Form
    {        
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="con">строка подключения</param>
        /// <param name="da1">код города</param>
        public CityForm(string con,int da1=-1)
        {
            InitializeComponent();
            City.MainLoad(con, da1);
            City.CitySelected += SelectCity;
        }

        private void SelectCity(object sender, BaseKOATUU.CCitySelectedEventArgs e)
        {
            this.Close();
        }
        #region data
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.City = new KOATUU.BaseKOATUU();
            this.SuspendLayout();
            // 
            // City
            // 
            this.City.City = -1;
            this.City.Dock = System.Windows.Forms.DockStyle.Fill;
            this.City.Location = new System.Drawing.Point(0, 0);
            this.City.Name = "City";
            this.City.ReadOnly = false;
            this.City.Size = new System.Drawing.Size(801, 540);
            this.City.TabIndex = 0;
            // 
            // CityForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(801, 540);
            this.Controls.Add(this.City);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "CityForm";
            this.ShowIcon = false;
            this.Text = "CityForm";           
            this.ResumeLayout(false);

        }

        #endregion

        public BaseKOATUU City;
        #endregion

        private void CityForm_KeyUp(object sender, KeyEventArgs e)
        {
          
        }
    }
}
