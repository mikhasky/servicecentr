﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using KOATUU.LoadMainData;
using FirebirdSql.Data.FirebirdClient;
using Serializable;
using System.Threading;

namespace KOATUU
{
    public partial class Country : Subject
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public Country()
        {
            MainLoad();
        }
        public override void MainLoad(Subject p=null)
        {
            if (this.InvokeRequired)
            {
                this.Invoke((ThreadStart)delegate ()
                {
                    _mainLoad();
                });
            }
            else
            {
                _mainLoad();
            }           
        }

        private void _mainLoad()
        {
            dataGridView2.Columns[1].HeaderText = "Страны";
            dataGridView2.Columns[2].Visible = false;
            dataGridView2.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }

        /// <summary>
        /// Загрузка стран
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public override void LoadData(object sender, EventArgs e)
        {
            if (this.InvokeRequired)
            {
                this.Invoke((ThreadStart)delegate ()
                {
                    _loadData();
                });
            }
            else
            {
                _loadData();
            }          
        }

        private void _loadData()
        {
            if (Data.tableDAS == null)
                return;
            dataGridView2.Rows.Clear();
            for (int i = 0; i < Data.tableDAS.Count; i++)
            {
                dataGridView2.Rows.Add(Data.tableDAS[i].DAS1, Data.tableDAS[i].DAS2);
            }
        }

        /// <summary>
        /// Обновление данных при редактировании пользователем
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public override void UpdateData(object sender, EventArgs e)
        {
            if (dataGridView2.CurrentRow == null)
                return;
            Data.SetEmptyRow(dataGridView2.CurrentRow);
            using (FbConnection fConnection = new FbConnection(Data.ConnectionString))
            {
                fConnection.Open();
                using (FbTransaction fTrans = fConnection.BeginTransaction())
                {
                    using (FbCommand fCommand = new FbCommand(@"UPDATE das
                            SET das2 = @p1 WHERE das1 = @p3", fConnection, fTrans))
                    {
                        fCommand.Parameters.Add("@p1", dataGridView2.CurrentRow.Cells[1].Value.ToString());
                        fCommand.Parameters.Add("@p3", dataGridView2.CurrentRow.Cells[0].Value.ToString());
                        fCommand.ExecuteNonQuery();
                        fTrans.Commit();
                        dbMaster.UpdateOMG<DAS>(Data.tableDAS, "das2", dataGridView2.CurrentRow.Cells[1].Value.ToString(), dataGridView2.CurrentRow.Cells[0].Value.ToString());
                    }
                }
            }
        }
        /// <summary>
        /// Добавление новой страны
        /// </summary>
        public override void Add(object sender, EventArgs e)
        {
            string id = Data.GenId("das").ToString();
            using (FbConnection fConnection = new FbConnection(Data.ConnectionString))
            {
                fConnection.Open();
                using (FbTransaction fTrans = fConnection.BeginTransaction())
                {
                    using (FbCommand fCommand = new FbCommand(@"INSERT INTO das(das1,das4)
                        VALUES(@das1,1)", fConnection, fTrans))
                    {
                        fCommand.Parameters.Add("@das1", id);
                        fCommand.ExecuteNonQuery();
                        fTrans.Commit();
                        dbMaster.FirstInsertOMG<DAS>(Data.tableDAS, new DAS() { DAS1 = Convert.ToInt32(id), DAS2 = string.Empty, DAS3 = 0, DAS4 =1, DAS5 = string.Empty });
                    }
                }
                dataGridView2.Rows.Add(id, string.Empty, string.Empty);
                dataGridView2.CurrentCell = dataGridView2.Rows[dataGridView2.RowCount - 1].Cells[1];
                dataGridView2.CurrentCell.Selected = true;
            }
        }
        /// <summary>
        /// Удаление текущей страны
        /// </summary>
        public override void Remove(object sender, EventArgs e)
        {
            if (dataGridView2.CurrentRow == null)
                return;
            if (MessageBox.Show("Удалить выбранную страну?", "Предупреждение", MessageBoxButtons.OKCancel, MessageBoxIcon.Stop) != DialogResult.OK)
            {
                return;
            }
            if (MessageBox.Show("Вы уверены?", "Предупреждение", MessageBoxButtons.OKCancel, MessageBoxIcon.Stop) != DialogResult.OK)
            {
                return;
            }
            using (FbConnection fConnection = new FbConnection(Data.ConnectionString))
            {
                fConnection.Open();
                using (FbTransaction fTrans = fConnection.BeginTransaction())
                {
                    using (FbCommand fCommand = new FbCommand(@"DELETE FROM das
                            WHERE das1 = @p1", fConnection, fTrans))
                    {
                        fCommand.Parameters.Add("@p1", dataGridView2.CurrentRow.Cells[0].Value.ToString());
                        fCommand.ExecuteNonQuery();
                        fTrans.Commit();
                        dbMaster.RemoveOMG<DAS>(Data.tableDAS, Convert.ToInt32(dataGridView2.CurrentRow.Cells[0].Value.ToString()));
                        dataGridView2.Rows.Remove(dataGridView2.CurrentRow);
                    }
                }
            }
            if (dataGridView2.CurrentRow != null)
                CellClick(dataGridView2, e);
        }
    }
}
