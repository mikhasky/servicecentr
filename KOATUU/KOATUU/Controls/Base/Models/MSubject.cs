﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
namespace KOATUU.Models
{
    /// <summary>
    /// Базовый класс для таблицы городов, районов, областей и стран
    /// </summary>
   public class MSubject:UserControl
    {
        /// <summary>
        /// Индекс выбранной строки таблицы
        /// </summary>
        public int curentRow=-1;
        public Subject parrent;
        public Subject child;
        /// <summary>
        /// Основная загрука, подписка на события и т.д.
        /// </summary>
        public virtual void MainLoad(Subject parrent)
        {
            throw new Exception();
        }
        /// <summary>
        /// Загрузка данных по айди, если айди -1, то загрузить всё с таблицы
        /// </summary>
        /// <param name="id"></param>
        public virtual void LoadData(object sender, EventArgs e)
        { throw new Exception(); }
        /// <summary>
        /// Обновление данных таблицы пользователем
        /// </summary>
        public virtual void UpdateData(object sender, EventArgs e)
        {
            throw new Exception();
        }
        /// <summary>
        /// Добавление новых данных в таблицу
        /// </summary>
        public virtual void Add(object sender, EventArgs e)
        {
            throw new Exception();
        }
        /// <summary>
        /// Удаление данных из таблицы
        /// </summary>
        public virtual void Remove(object sender, EventArgs e)
        {
            throw new Exception();
        }
    }
}
