﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using KOATUU.Models;
using System.Collections.ObjectModel;
namespace KOATUU
{
    /// <summary>
    /// Класс-посредник
    /// </summary>
    public partial class Subject :MSubject
    {        
        #region Constructor
        /// <summary>
        /// Базовый конструктор для элемента
        /// </summary>
        public Subject()
        {
            InitializeComponent();
            dataGridView2.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
            dataGridView2.ReadOnly = false;
            dataGridView2.Columns[0].ReadOnly = false;
            dataGridView2.Columns[1].ReadOnly = false;
            dataGridView2.Columns[2].ReadOnly = false;            
            bUsers_add_.Click += Add;
            bUser_del_.Click += Remove;
            dataGridView2.CellEndEdit += UpdateData;
            if (parrent != null)                
                parrent.dataGridView2.Rows.CollectionChanged += Collection;            
        }
        #endregion
        #region Открытые функцит
        /// <summary>
        /// Получение высоты шапки
        /// </summary>
        /// <returns></returns>
        public int GetHeadersHeight()
        {
            return dataGridView2.ColumnHeadersHeight;
        }
        /// <summary>
        /// Установка высоты шапки
        /// </summary>
        /// <param name="height"></param>
        public void SetHeightColumns(int height)
        {
            dataGridView2.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridView2.ColumnHeadersHeight = height;
        }
        /// <summary>
        /// Видимость колонки КОАТУУ
        /// </summary>
        /// <param name="vis">true - видимая колонка</param>
        public void SetKoatuuVisible(bool vis)
        {
            dataGridView2.Columns[2].Visible = vis;
            if (vis == false && dataGridView2.Columns[1].Visible)
                dataGridView2.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            else
                dataGridView2.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.NotSet;

        }
        /// <summary>
        /// Доступ на редактирование колонки названия
        /// </summary>
        /// <param name="ReadOnly">true - доступ запрещён</param>
        public void SetAccesNameUpdate(bool ReadOnly)
        {
            dataGridView2.Columns[1].ReadOnly = ReadOnly;
        }
        /// <summary>
        /// Доступ на редактирование колонки редактирования
        /// </summary>
        /// <param name="ReadOnly">true - доступ запрещён</param>
        public void SetAccesKoatuuUpdate(bool ReadOnly)
        {
            dataGridView2.Columns[2].ReadOnly = ReadOnly;
        }
        /// <summary>
        /// Доступно ли пользователю добавлять и удалять данные
        /// </summary>
        /// <param name="acc">true - есть доступ</param>
        public void SetAddRemoveAccess(bool acc)
        {
            bUsers_add_.Visible = acc;
            bUser_del_.Visible = acc;
            if (acc)            
                dataGridView2.Size = new Size(dataGridView2.Size.Width, dataGridView2.Size.Height - bUser_del_.Height);
            else
                dataGridView2.Size = new Size(dataGridView2.Size.Width, dataGridView2.Size.Height + bUser_del_.Height);
        }
        /// <summary>
        /// Событие изменения коллекции родителя
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Collection(object sender, CollectionChangeEventArgs e)
        {
            if (parrent.dataGridView2.Rows.Count == 0)
                dataGridView2.Rows.Clear();
        }
        /// <summary>
        /// Событие клика по ячейке, проверка на то, выбрана ли на данный момент текущая строка
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void CellClick(object sender, EventArgs e)
        {            
            if (parrent == null)
                return;
            if (sender == null || parrent.dataGridView2.CurrentRow == null)
            {
                ClearChilds(this.child);
                return;
            }
            if (!GetParrentRowChanged(parrent))
            {
                ClearChilds(this);                          
                LoadData(sender, e);
            }           
        }
        /// <summary>
        /// Установка цвета строки в последнюю строку текущей таблицы
        /// </summary>
        /// <param name="col"></param>
        public void SetEndRowColor(Color col,int row=-1)
        {
            if (row==-1)
            this.dataGridView2.Rows[this.dataGridView2.RowCount - 1].DefaultCellStyle.BackColor = col;
            else
                this.dataGridView2.Rows[row].DefaultCellStyle.BackColor = col;
        }     
        /// <summary>
        /// Рекурсивная функция определения изменения выделенной строки родителем
        /// </summary>
        /// <param name="parrent"></param>
        /// <returns></returns>
        private bool GetParrentRowChanged(Subject parrent)
        {
            if (parrent == null||parrent.child==null)
                return true;
            if (parrent.dataGridView2.CurrentRow == null)
                return true;
            if (parrent.curentRow != parrent.dataGridView2.CurrentRow.Index)
            {
                parrent.curentRow = parrent.dataGridView2.CurrentRow.Index;                
                return false;
            }
            else return GetParrentRowChanged(parrent.parrent);
        }
        /// <summary>
        /// Рекурсивная функция очищения дочерних таблиц
        /// </summary>
        /// <param name="child"></param>
        private void ClearChilds(Subject child)
        {
            if (child == null)
                return;       
            if  (child.child!=null)  
            child.dataGridView2.SelectionChanged -= child.child.CellClick;
            child.curentRow = -1;
            child.dataGridView2.Rows.Clear();
            if (child.child != null)
                child.dataGridView2.SelectionChanged += child.child.CellClick;
            ClearChilds(child.child);
        }
        #endregion
        #region Нереализованные ф-ции
        public override void MainLoad(Subject parrent)
        {
            throw new NotImplementedException();
        }
        public override void Add(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }
        public override void LoadData(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }
        public override void UpdateData(object sender,EventArgs e)
        {
            throw new NotImplementedException();
        }
        public override void Remove(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }
        #endregion
        #region Инициализация компонентов, дизайнер
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.bUser_del_ = new System.Windows.Forms.Button();
            this.bUsers_add_ = new System.Windows.Forms.Button();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // bUser_del_
            // 
            this.bUser_del_.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bUser_del_.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.bUser_del_.Location = new System.Drawing.Point(122, 405);
            this.bUser_del_.Name = "bUser_del_";
            this.bUser_del_.Size = new System.Drawing.Size(75, 23);
            this.bUser_del_.TabIndex = 19;
            this.bUser_del_.Text = "Удалить";
            this.bUser_del_.UseVisualStyleBackColor = true;
            // 
            // bUsers_add_
            // 
            this.bUsers_add_.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bUsers_add_.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.bUsers_add_.Location = new System.Drawing.Point(0, 405);
            this.bUsers_add_.Name = "bUsers_add_";
            this.bUsers_add_.Size = new System.Drawing.Size(75, 23);
            this.bUsers_add_.TabIndex = 18;
            this.bUsers_add_.Text = "Добавить";
            this.bUsers_add_.UseVisualStyleBackColor = true;
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.AllowUserToResizeRows = false;
            this.dataGridView2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView2.BackgroundColor = System.Drawing.SystemColors.Menu;
            this.dataGridView2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView2.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView2.GridColor = System.Drawing.SystemColors.AppWorkspace;
            this.dataGridView2.Location = new System.Drawing.Point(-1, 1);
            this.dataGridView2.MultiSelect = false;
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.RowHeadersVisible = false;
            this.dataGridView2.RowTemplate.Height = 17;
            this.dataGridView2.ShowCellToolTips = false;
            this.dataGridView2.Size = new System.Drawing.Size(198, 402);
            this.dataGridView2.TabIndex = 17;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "id";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Название";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 122;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "КОАТУУ";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Width = 75;
            // 
            // Subject
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.bUser_del_);
            this.Controls.Add(this.bUsers_add_);
            this.Controls.Add(this.dataGridView2);
            this.Name = "Subject";
            this.Size = new System.Drawing.Size(197, 430);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.ResumeLayout(false);

        }

     

        public System.Windows.Forms.Button bUser_del_;
        public System.Windows.Forms.Button bUsers_add_;
        public System.Windows.Forms.DataGridView dataGridView2;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
    }
    #endregion
}

