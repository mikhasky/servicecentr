﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using KOATUU.LoadMainData;
using FirebirdSql.Data.FirebirdClient;
using Serializable;
using System.Threading;

namespace KOATUU
{
    public partial class Region : Subject
    {
        public Region()
        {
            dataGridView2.Columns[1].HeaderText = "Районы";
        }
        /// <summary>
        /// Конструктор
        /// </summary>
        public Region(Subject p=null)
        {
            //InitializeComponent();
            MainLoad(p);
        }
        /// <summary>
        /// Инициализация контрола
        /// </summary>        
        public override void MainLoad(Subject p)
        {
            if (this.InvokeRequired)
            {
                this.Invoke((ThreadStart)delegate ()
                {
                    _mainLoad(p);
                });
            }
            else
            {
                _mainLoad(p);
            }
           
        }

        private void _mainLoad(Subject p)
        {
            if (p == null)
                return;
            parrent = p;
            parrent.child = this;
            dataGridView2.Columns[1].HeaderText = "Районы";
            parrent.dataGridView2.SelectionChanged += CellClick;
        }

        /// <summary>
        /// Загрузка данных при выборе области
        /// </summary>
        /// <param name="p"></param>
        public override void LoadData(object sender, EventArgs e)
        {
            if (this.InvokeRequired)
            {
                this.Invoke((ThreadStart)delegate ()
                {
                    _loadData();
                });
            }
            else
            {
                _loadData();
            }
           
        }

        private void _loadData()
        {
            int id = Convert.ToInt32(parrent.dataGridView2.CurrentRow.Cells[0].Value);
            dataGridView2.Rows.Clear();
            foreach (var item in Data.tableDAR.Where(i => i.DAR2 == id).OrderBy(i=>i.DAR3))
                dataGridView2.Rows.Add(item.DAR1, item.DAR3, item.DAR6);
        }

        /// <summary>
        /// Обновление данных при редактировании пользователем
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public override void UpdateData(object sender, EventArgs e)
        {
            if (dataGridView2.CurrentRow == null)
                return;
            Data.SetEmptyRow(dataGridView2.CurrentRow);
            Data.UpdateDAR(dataGridView2.CurrentRow.Cells[2].Value.ToString(), dataGridView2.CurrentRow.Cells[1].Value.ToString(), dataGridView2.CurrentRow.Cells[0].Value.ToString());
        }
        /// <summary>
        /// Добавление нового района
        /// </summary>
        public override void Add(object sender, EventArgs e)
        {
            string id = Data.GenId("dar").ToString();           
            using (FbConnection fConnection = new FbConnection(Data.ConnectionString))
            {
                fConnection.Open();
                using (FbTransaction fTrans = fConnection.BeginTransaction())
                {
                    using (FbCommand fCommand = new FbCommand(@"INSERT INTO dar(dar1,dar2)
                        VALUES(@dar1,@dar2)", fConnection, fTrans))
                    {
                        fCommand.Parameters.Add("@dar1", id);
                        fCommand.Parameters.Add("@dar2", parrent.dataGridView2.CurrentRow.Cells[0].Value.ToString());
                        fCommand.ExecuteNonQuery();
                        fTrans.Commit();
                        dbMaster.FirstInsertOMG<DAR>(Data.tableDAR, new DAR() { DAR1 = Convert.ToInt32(id), DAR2 = Convert.ToInt32(parrent.dataGridView2.CurrentRow.Cells[0].Value.ToString()), DAR3 = string.Empty, DAR4 = 0, DAR5 = string.Empty, DAR6 = string.Empty });
                    }
                }                
                dataGridView2.Rows.Add(id, string.Empty, string.Empty);
                dataGridView2.CurrentCell = dataGridView2.Rows[dataGridView2.RowCount - 1].Cells[1];
                dataGridView2.CurrentCell.Selected = true;
            }
        }
        /// <summary>
        /// Удаление текущего района
        /// </summary>
        public override void Remove(object sender, EventArgs e)
        {
            if (dataGridView2.CurrentRow == null)
                return;
            if (MessageBox.Show("Удалить выбранный район?", "Предупреждение", MessageBoxButtons.OKCancel, MessageBoxIcon.Stop) != DialogResult.OK)
            {
                return;
            }
            if (MessageBox.Show("Вы уверены?", "Предупреждение", MessageBoxButtons.OKCancel, MessageBoxIcon.Stop) != DialogResult.OK)
            {
                return;
            }
            using (FbConnection fConnection = new FbConnection(Data.ConnectionString))
            {
                fConnection.Open();
                using (FbTransaction fTrans = fConnection.BeginTransaction())
                {
                    using (FbCommand fCommand = new FbCommand(@"DELETE FROM dar
                            WHERE dar1 = @p1", fConnection, fTrans))
                    {
                        fCommand.Parameters.Add("@p1", dataGridView2.CurrentRow.Cells[0].Value.ToString());
                        fCommand.ExecuteNonQuery();
                        fTrans.Commit();
                        dbMaster.RemoveOMG<DAR>(Data.tableDAR, Convert.ToInt32(dataGridView2.CurrentRow.Cells[0].Value.ToString()));
                        dataGridView2.Rows.Remove(dataGridView2.CurrentRow);
                    }
                }
            }
            if (dataGridView2.CurrentRow != null)
                CellClick(dataGridView2, e);
        }
    }
}
