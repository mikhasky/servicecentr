﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using KOATUU.LoadMainData;
using FirebirdSql.Data.FirebirdClient;
using Serializable;
using System.Threading;

namespace KOATUU
{
    public partial class District : Subject
    {
        public District()
        {
            dataGridView2.Columns[1].HeaderText = "Области";
            dataGridView2.KeyUp += Start;
        }

        private void Start(object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.F5)
                return;
            KOATUU.KOATUU_DATA.Start(Data.ConnectionString);
            MessageBox.Show("Выполнено");
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        public District(Subject p=null)
        {
            //InitializeComponent();
            MainLoad(p);
        }
        /// <summary>
        /// Инициализация контрола
        /// </summary>        
        public override void MainLoad(Subject p)
        {
            if (this.InvokeRequired)
            {
                this.Invoke((ThreadStart)delegate ()
                {
                    _mainLoad(p);
                });
            }
            else
            {
                _mainLoad(p);
            }           
        }

        private void _mainLoad(Subject p)
        {
            if (p == null)
                return;
            parrent = p;
            parrent.child = this;
            dataGridView2.Columns[1].HeaderText = "Области";
            parrent.dataGridView2.SelectionChanged += CellClick;//
        }

        /// <summary>
        /// Загрузка данных при выборе области
        /// </summary>
        /// <param name="p"></param>
        public override void LoadData(object sender, EventArgs e)
        {
            if (this.InvokeRequired)
            {
                this.Invoke((ThreadStart)delegate ()
                {
                    _loadData();
                });
            }
            else
            {
                _loadData();
            }                 
        }

        private void _loadData()
        {
            int id = Convert.ToInt32(parrent.dataGridView2.CurrentRow.Cells[0].Value);
            dataGridView2.Rows.Clear();
            foreach (var item in Data.tableDAO.Where(i => i.DAO2 == id).OrderBy(i=>i.DAO3))
                dataGridView2.Rows.Add(item.DAO1, item.DAO3, item.DAO7);
        }

        /// <summary>
        /// Обновление данных при редактировании пользователем
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public override void UpdateData(object sender, EventArgs e)
        {
            if (dataGridView2.CurrentRow == null)
                return;
            Data.SetEmptyRow(dataGridView2.CurrentRow);
            Data.UpdateDAO(dataGridView2.CurrentRow.Cells[2].Value.ToString(), dataGridView2.CurrentRow.Cells[1].Value.ToString(), dataGridView2.CurrentRow.Cells[0].Value.ToString());
        }
        /// <summary>
        /// Добавление нового района
        /// </summary>
        public override void Add(object sender, EventArgs e)
        {
            string id = Data.GenId("dao").ToString();
            using (FbConnection fConnection = new FbConnection(Data.ConnectionString))
            {
                fConnection.Open();
                using (FbTransaction fTrans = fConnection.BeginTransaction())
                {
                    using (FbCommand fCommand = new FbCommand(@"INSERT INTO dao(dao1,dao2,dao5)
                        VALUES(@dao1,@dao2,1)", fConnection, fTrans))
                    {
                        fCommand.Parameters.Add("@dao1", id);
                        fCommand.Parameters.Add("@dao2", parrent.dataGridView2.CurrentRow.Cells[0].Value.ToString());
                        fCommand.ExecuteNonQuery();
                        fTrans.Commit();
                        dbMaster.FirstInsertOMG<DAO>(Data.tableDAO, new DAO() { DAO1 = Convert.ToInt32(id), DAO2 = Convert.ToInt32(parrent.dataGridView2.CurrentRow.Cells[0].Value.ToString()), DAO3 = string.Empty, DAO4 = string.Empty, DAO5 = 1, DAO6 = string.Empty, DAO7 = string.Empty });
                    }
                }
                dataGridView2.Rows.Add(id, string.Empty, string.Empty);
                dataGridView2.CurrentCell = dataGridView2.Rows[dataGridView2.RowCount - 1].Cells[1];
                dataGridView2.CurrentCell.Selected = true;
            }
        }
        /// <summary>
        /// Удаление текущего района
        /// </summary>
        public override void Remove(object sender, EventArgs e)
        {
            if (dataGridView2.CurrentRow == null)
                return;
            if (MessageBox.Show("Удалить выбранную область?", "Предупреждение", MessageBoxButtons.OKCancel, MessageBoxIcon.Stop) != DialogResult.OK)
            {
                return;
            }
            if (MessageBox.Show("Вы уверены?", "Предупреждение", MessageBoxButtons.OKCancel, MessageBoxIcon.Stop) != DialogResult.OK)
            {
                return;
            }
            using (FbConnection fConnection = new FbConnection(Data.ConnectionString))
            {
                fConnection.Open();
                using (FbTransaction fTrans = fConnection.BeginTransaction())
                {
                    using (FbCommand fCommand = new FbCommand(@"DELETE FROM dao
                            WHERE dao1 = @p1", fConnection, fTrans))
                    {
                        fCommand.Parameters.Add("@p1", dataGridView2.CurrentRow.Cells[0].Value.ToString());
                        fCommand.ExecuteNonQuery();
                        fTrans.Commit();
                        dbMaster.RemoveOMG<DAO>(Data.tableDAO, Convert.ToInt32(dataGridView2.CurrentRow.Cells[0].Value.ToString()));
                        dataGridView2.Rows.Remove(dataGridView2.CurrentRow);
                    }
                }
            }
            if (dataGridView2.CurrentRow != null)
                CellClick(dataGridView2, e);
        }
    }
}
