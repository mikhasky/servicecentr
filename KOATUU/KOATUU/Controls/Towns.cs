﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using KOATUU.LoadMainData;
using FirebirdSql.Data.FirebirdClient;
using Serializable;
using KOATUU.Models;
using System.Threading;

namespace KOATUU
{  
    public partial class Towns:Subject
    {

        public delegate void SelectedCountryHandler(int da1);
        /// <summary>
        /// Выбор города
        /// </summary>
        public event SelectedCountryHandler SelectedCountry;
        public Towns()
        {
            InitializeComponent();
            dataGridView2.Columns[1].HeaderText = "Города";
            dataGridView2.Columns[2].Width = dataGridView2.Columns[2].Width+5;
            dataGridView2.SelectionChanged += CountryChanged;      
        }

        private void CountryChanged(object sender, EventArgs e)
        {
            if (dataGridView2.CurrentRow == null)            
                SelectedCountry?.Invoke(-1);
            else
                SelectedCountry?.Invoke(Convert.ToInt32(dataGridView2.CurrentRow.Cells[0].Value));
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        public Towns(Subject p=null)
        {
           // InitializeComponent();
            MainLoad(p);
        }
        /// <summary>
        /// Инициализация контрола
        /// </summary>        
        public override void MainLoad(Subject p)
        {
            if (this.InvokeRequired)
            {
                this.Invoke((ThreadStart)delegate ()
                {
                    _mainLoad(p);
                });
            }
            else
            {
                _mainLoad(p);
            }
        }

        private void _mainLoad(Subject p)
        {
            if (p == null)
                return;
            parrent = p;
            parrent.child = this;
            dataGridView2.Columns[1].HeaderText = "Города";
            parrent.dataGridView2.SelectionChanged += CellClick;            
        }
        /// <summary>
        /// Включить горячие клавиши F5,F6,F7,F8
        /// </summary>
        /// <param name="acc"></param>
        public void SetAccessFuncs(bool acc)
        {
            bInfo.Visible = acc;
            dataGridView2.KeyUp -= KeyFuncs;
            if (acc)
            dataGridView2.KeyUp += KeyFuncs;
        }

        private void KeyFuncs(object sender, KeyEventArgs e)
        {
            if (dataGridView2.CurrentRow == null|| parrent.dataGridView2.CurrentRow==null)
                return;
            //F5 - сделать районным центром(dar14-код района, da15=1)
            //F6 - сделать областным центром(da14=0,da15=0)
            //F7 - сделать селом(da14=dar1,da15=2)
            //F8 - без привязки(da14=0,da15=3)
            switch (e.KeyData)
            {
                case Keys.F5:
                    if (MessageBox.Show("Сделать районным центром?", "Подтверждение", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) != DialogResult.OK)
                        return;
                    UpdDa(parrent.dataGridView2.CurrentRow.Cells[0].Value.ToString(),"1");
                    SetEndRowColor(Color.FromArgb(223, 237, 253), dataGridView2.CurrentRow.Index);
                    break;
                case Keys.F6:
                    if (MessageBox.Show("Сделать областным центром?", "Подтверждение", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) != DialogResult.OK)
                        return;
                    UpdDa("0", "0");
                    SetEndRowColor(Color.FromArgb(231, 245, 233), dataGridView2.CurrentRow.Index);
                    break;
                case Keys.F7:
                    if (MessageBox.Show("Сделать населённым пунктом?", "Подтверждение", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) != DialogResult.OK)
                        return;
                    UpdDa(parrent.dataGridView2.CurrentRow.Cells[0].Value.ToString(), "2");
                    SetEndRowColor(Color.White, dataGridView2.CurrentRow.Index);
                    break;
                case Keys.F8:
                    if (MessageBox.Show("Убрать привязку?", "Подтверждение", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) != DialogResult.OK)
                        return;
                    UpdDa("0", "3");
                    SetEndRowColor(Color.FromArgb(254, 224, 253), dataGridView2.CurrentRow.Index);
                    break;
            }
        }

        private void UpdDa(string da14, string da15)
        {
            using (FbConnection fConnection = new FbConnection(Data.ConnectionString))
            {
                fConnection.Open();
                using (FbTransaction fTrans = fConnection.BeginTransaction())
                {
                    using (FbCommand fCommand = new FbCommand(@"UPDATE da
                            SET da14=@da14,da15=@da15 WHERE da1 = @p1", fConnection, fTrans))
                    {
                        fCommand.Parameters.Add("@p1", dataGridView2.CurrentRow.Cells[0].Value);
                        fCommand.Parameters.Add("@da14", da14);
                        fCommand.Parameters.Add("@da15", da15);
                        fCommand.ExecuteNonQuery();
                        fTrans.Commit();
                        dbMaster.UpdateOMG<DA>(Data.tableDA, "da14", da14, dataGridView2.CurrentRow.Cells[0].Value);
                        dbMaster.UpdateOMG<DA>(Data.tableDA, "da15", da15, dataGridView2.CurrentRow.Cells[0].Value);
                    }
                }
            }
        }

        private void _loadData()
        {                  
            int dar1= Convert.ToInt32(parrent.dataGridView2.CurrentRow.Cells[0].Value);
            int dao1= Convert.ToInt32(parrent.parrent.dataGridView2.CurrentRow.Cells[0].Value);
            dataGridView2.Rows.Clear();  
           
            foreach (var item in Data.tableDA.Where(i => i.DA13 == dao1 &&i.DA14==0 && i.DA15 ==0 ).OrderBy(i=>i.DA3))
            {
                dataGridView2.Rows.Add(item.DA1, item.DA2 + item.DA3, item.DA16);
                SetEndRowColor(Color.FromArgb(231, 245, 233));
            }
            foreach (var item in Data.tableDA.Where(i => i.DA14 == dar1&&(i.DA15==1)).OrderBy(i => i.DA3))
            {
                dataGridView2.Rows.Add(item.DA1, item.DA2 + item.DA3, item.DA16);
                SetEndRowColor(Color.FromArgb(223, 237, 253));
            }
            foreach (var item in Data.tableDA.Where(i=>i.DA13 == dao1 && i.DA14 == 0 && i.DA15 == 3).OrderBy(i=>i.DA3))
            {
                dataGridView2.Rows.Add(item.DA1, item.DA2 + item.DA3, item.DA16);
                SetEndRowColor(Color.FromArgb(254, 224, 253));
            }
            foreach (var item in Data.tableDA.Where(i => i.DA14 == dar1 && (i.DA15 == 3)).OrderBy(i => i.DA3))
            {
                dataGridView2.Rows.Add(item.DA1, item.DA2 + item.DA3, item.DA16);
                SetEndRowColor(Color.FromArgb(254, 224, 253));
            }
            foreach (var item in Data.tableDA.Where(i => i.DA14 == dar1 && (i.DA15 == 2)).OrderBy(i => i.DA3))
            {
                dataGridView2.Rows.Add(item.DA1, item.DA2 + item.DA3, item.DA16);
            }
            
            /*int id = Convert.ToInt32(parrent.dataGridView2.CurrentRow.Cells[0].Value);
             foreach (var item in Data.tableDA.Where(i => i.DA14 == id))
                 dataGridView2.Rows.Add(item.DA1, item.DA2 + item.DA3, item.DA16);*/

        }
        private void RowColor(int da15)
        {
            switch (da15)
            {
                case 0: SetEndRowColor(Color.FromArgb(91, 217, 145)); break;
                case 1: SetEndRowColor(Color.FromArgb(99, 176, 209)); break;
            }
        }
        /// <summary>
        /// Загрузка данных при выборе района
        /// </summary>
        /// <param name="p"></param>
        public override void LoadData(object sender, EventArgs e)
        {
            if (this.InvokeRequired)
            {
                this.Invoke((ThreadStart)delegate ()
                {
                    _loadData();
                });
            }
            else
            {
                _loadData();
            }          
        }
        /// <summary>
        /// Обновление данных при редактировании пользователем
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public override void UpdateData(object sender,EventArgs e)
        {
            if (dataGridView2.CurrentRow == null)
                return;
            Data.SetEmptyRow(dataGridView2.CurrentRow);
            Data.UpdateDa(dataGridView2.CurrentRow.Cells[1].Value.ToString(), dataGridView2.CurrentRow.Cells[2].Value.ToString(), dataGridView2.CurrentRow.Cells[0].Value.ToString());
        }
        /// <summary>
        /// Добавление нового города
        /// </summary>
        public override void Add(object sender, EventArgs e)
        {
            string id = Data.GenId("da").ToString();
            string da14 = parrent.dataGridView2.CurrentRow.Cells[0].Value.ToString();
            using (FbConnection fConnection = new FbConnection(Data.ConnectionString))
            {
                fConnection.Open();
                using (FbTransaction fTrans = fConnection.BeginTransaction())
                {
                    using (FbCommand fCommand = new FbCommand(@"INSERT INTO da(da1,da2,da3,da14,da16)
                        VALUES(@da1,@da2,@da3,@da14,@da16)", fConnection, fTrans))
                    {
                        fCommand.Parameters.Add("@da1", id);
                        fCommand.Parameters.Add("@da2", string.Empty);
                        fCommand.Parameters.Add("@da3", string.Empty);
                        fCommand.Parameters.Add("@da14", da14);
                        fCommand.Parameters.Add("@da16", string.Empty);
                        fCommand.ExecuteNonQuery();
                        fTrans.Commit();
                        dbMaster.FirstInsertOMG<DA>(Data.tableDA, new DA() { DA1 = Convert.ToInt32(id), DA2 = string.Empty, DA3 = string.Empty, DA9 = DateTime.Now, DA10 = 0, DA12 = 0, DA13 = 0, DA14 = Convert.ToInt32(da14), DA15 = 2, DA16 = string.Empty });
                    }
                }               
                dataGridView2.Rows.Add(id, string.Empty, string.Empty);
                dataGridView2.CurrentCell = dataGridView2.Rows[dataGridView2.RowCount - 1].Cells[1];
                dataGridView2.CurrentCell.Selected = true;
            }
        }
        /// <summary>
        /// Удаление текущего города
        /// </summary>
        public override void Remove(object sender, EventArgs e)
        {
            if (dataGridView2.CurrentRow == null)
                return;
            if (MessageBox.Show("Удалить выбранный город?", "Предупреждение", MessageBoxButtons.OKCancel, MessageBoxIcon.Stop) != DialogResult.OK)
            {
                return;
            }
            if (MessageBox.Show("Вы уверены?", "Предупреждение", MessageBoxButtons.OKCancel, MessageBoxIcon.Stop) != DialogResult.OK)
            {
                return;
            }
            using (FbConnection fConnection = new FbConnection(Data.ConnectionString))
            {
                fConnection.Open();
                using (FbTransaction fTrans = fConnection.BeginTransaction())
                {
                    using (FbCommand fCommand = new FbCommand(@"DELETE FROM da
                            WHERE da1 = @p1", fConnection, fTrans))
                    {
                        fCommand.Parameters.Add("@p1", dataGridView2.CurrentRow.Cells[0].Value.ToString());
                        fCommand.ExecuteNonQuery();
                        fTrans.Commit();
                        dbMaster.RemoveOMG<DA>(Data.tableDA, Convert.ToInt32(dataGridView2.CurrentRow.Cells[0].Value.ToString()));
                        dataGridView2.Rows.Remove(dataGridView2.CurrentRow);
                    }
                }
            }
        }

        private void bInfo_Click(object sender, EventArgs e)
        {
            //F5 - сделать районным центром(dar14-код района, da15=1)
            //F6 - сделать областным центром(da14=0,da15=0)
            //F7 - сделать селом(da14=dar1,da15=2)
            //F8 - без привязки(da14=0,da15=3)
            MessageBox.Show("F5 - сделать районным центром"+Environment.NewLine+ "F6 - сделать областным центром" + Environment.NewLine + "F7 - сделать населённым пунктом" + Environment.NewLine + "F8 - отвязать");
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
    }
    
}
