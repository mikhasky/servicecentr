﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using KOATUU.LoadMainData;
using System.Threading;

namespace KOATUU
{
    /// <summary>
    /// Готовый контрол Адресов
    /// </summary>
    public partial class BaseKOATUU : UserControl
    {
        #region Deligates
        /// <summary>
        /// событие об выборе города
        /// </summary>
        /// <param name="sender">обьект</param>
        /// <param name="e">Параметры</param>
        public delegate void CitySelectEventHandler(object sender, BaseKOATUU.CCitySelectedEventArgs e);
        #endregion

        #region Event
        /// <summary>
        /// событие  об выборе города
        /// </summary>
        public event CitySelectEventHandler CitySelected;
        #endregion

        #region Properties
        /// <summary>
        /// Есть ли возможнтьс редактирования
        /// </summary>
        public bool ReadOnly
        {
            get {
                return _readOnly;
            }
            set {
                if (_readOnly != value)
                {
                    _readOnly = value;
                    SetReadOnly(_readOnly);
                }
            }
        }
        /// <summary>
        /// Выбранный город
        /// </summary>
        public int City
        {
            get {
                return da1;
            }
            set
            {
                SelectAddress(value);
            }
        }
        #endregion

        #region переменные
        /// <summary>
        /// Есть ли возможнтьс редактирования
        /// </summary>
        private bool _readOnly = false;
        /// <summary>
        /// Код выбранного города
        /// </summary>
        private static int da1=-1;
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        /// <summary>
        /// Страны
        /// </summary>
        private Country country1;
        /// <summary>
        /// Округи
        /// </summary>
        private District district1;
        /// <summary>
        /// Регионы
        /// </summary>
        private Region region1;
        /// <summary>
        /// Города
        /// </summary>
        private Towns towns1;
        #endregion

        #region конструктор
        /// <summary>
        /// Конструктор
        /// </summary>
        public BaseKOATUU()
        {
            if (this.InvokeRequired)
            {
                this.Invoke((ThreadStart)delegate ()
                {
                    InitializeComponent();
                    towns1.SelectedCountry += SelectedCountry;
                });
            }
            else
            {
                InitializeComponent();
                towns1.SelectedCountry += SelectedCountry;
            }
            //загружаем основные данные            
                       
        }
/// <summary>
/// Выбор города по щелчку, изменяем da1
/// </summary>
/// <param name="da1"></param>
        private void SelectedCountry(int da1)
        {
            BaseKOATUU.da1 = da1;
        }

        #endregion

        #region Инициализатор
        /// <summary>
        /// Главная загрузка
        /// </summary>
        /// <param name="conn">строка подключения</param>
        /// <param name="da1">можно указать айди города для позиционирования</param>
        public void MainLoad(string conn,int da1=-1)
        {           
            Data.ConnectionString = conn;
            if (Data.ConnectionString == null || country1 == null || district1 == null || region1 == null || towns1 == null||string.IsNullOrEmpty(conn))
                return;           
            if (this.InvokeRequired)
            {
                this.Invoke((ThreadStart)delegate ()
                {
                    SetSettings();
                    SelectAddress(da1);
                    _SetCurrentAdress();
                });
            }
            else
            {
                SetSettings();
                SelectAddress(da1);
                _SetCurrentAdress();
            }           
        }
        private void SetSettings()
        {
            Data.Loader();          
            this.country1.LoadData(null, null);
            district1.MainLoad(country1);
            region1.MainLoad(district1);
            towns1.MainLoad(region1);
            district1.SetKoatuuVisible(false);
            region1.SetKoatuuVisible(false);
            towns1.button1.Click += CloseForm;
            towns1.button2.Click += CloseFormNull;
        }
        private void _SetCurrentAdress()
        {
            if (da1 == -1)
                for (int i = 0; i < country1.dataGridView2.RowCount; i++)
                {
                    if (country1.dataGridView2.Rows[i].Cells[1].Value != null)
                        if (country1.dataGridView2.Rows[i].Cells[1].Value.ToString().ToLower().Contains("укра"))
                        {
                            country1.dataGridView2.CurrentCell = country1.dataGridView2[1, i];
                            district1.CellClick(country1, null);
                            break;
                        }
                }
        }
        private void CloseFormNull(object sender, EventArgs e)
        {
            da1 = 0;
            CitySelected?.Invoke(towns1, new CCitySelectedEventArgs() { City = da1 });
            // this.FindForm().Close();
        }
        #endregion

        #region Open functions
        /// <summary>
        /// Включение горячих клавиш городам
        /// </summary>
        /// <param name="set"></param>
        public void SetHotKeys(bool set)
        {
            if (this.InvokeRequired)
            {
                this.Invoke((ThreadStart)delegate ()
                {
                    towns1.SetAccessFuncs(set);
                });
            }
            else
            {
                towns1.SetAccessFuncs(set);
            }            
        }
        /// <summary>
        /// Переход по адресу, позиционирование на нужном городе
        /// </summary>
        /// <param name="da1">айди города da1</param>        
        public void SelectAddress(int da1)
        {
            if (da1 <= 0)
            {
                SelectRowById(country1, da1.ToString());
                SelectRowById(district1, da1.ToString());
                SelectRowById(region1, da1.ToString());
                SelectRowById(towns1, da1.ToString());
                return;
            }                         
            var material = (from da in Data.tableDA
                            join dao in Data.tableDAO on da.DA13 equals dao.DAO1
                            join dar in Data.tableDAR on da.DA14 equals dar.DAR1
                            join das in Data.tableDAS on dao.DAO2 equals das.DAS1
                            where da.DA1 == da1
                            select new { dao1 = dao.DAO1.ToString(), das1 = das.DAS1.ToString(), dar1 = dar.DAR1.ToString() }).FirstOrDefault();
            SelectRowById(country1, material.das1);
            SelectRowById(district1, material.dao1);
            SelectRowById(region1, material.dar1);
            SelectRowById(towns1, da1.ToString());
            BaseKOATUU.da1 = da1;
        }
        /// <summary>
        /// Получение текущего da1 кода города
        /// </summary>
        /// <returns></returns>
        public int GetDa1()
        {
            return da1;
        }
        /// <summary>
        /// Позиционирование на строке по айди
        /// </summary>
        /// <param name="ctrl"></param>
        /// <param name="id"></param>
        public void SelectRowById(Subject ctrl,string id)
        {
            if (id == "0"||id=="-1")
            {
                if (ctrl.dataGridView2.RowCount > 0)
                    if (ctrl.dataGridView2.CurrentCell != ctrl.dataGridView2.Rows[0].Cells[0])
                    {
                        ctrl.dataGridView2.CurrentCell = ctrl.dataGridView2.Rows[0].Cells[1];
                        ctrl.dataGridView2.CurrentCell.Selected = true;
                    }
                return;
            }
            for (int i = 0; i < ctrl.dataGridView2.RowCount; i++)
            {
                if (ctrl.dataGridView2.Rows[i].Cells[0].Value.ToString() == id)
                {
                    if (ctrl.dataGridView2.CurrentCell != ctrl.dataGridView2.Rows[i].Cells[0])
                    {
                        ctrl.dataGridView2.CurrentCell = ctrl.dataGridView2.Rows[i].Cells[1];
                        ctrl.dataGridView2.CurrentCell.Selected = true;
                    }
                    break;
                }
            }            
        }
        /// <summary>
        /// Видимость колонки КОАТУУ
        /// </summary>
        /// <param name="vis">true - видимая колонка</param>
        public void SetKoatuuVisible(bool vis)
        {
            SetVisibleKOATUU(country1, vis);
            SetVisibleKOATUU(region1, vis);
            SetVisibleKOATUU(district1, vis);
            SetVisibleKOATUU(towns1, vis);
        }       
        /// <summary>
        /// Доступ на редактирование колонки названия
        /// </summary>
        /// <param name="ReadOnly">true - доступ запрещён</param>
        public void SetAccesNameUpdate(bool ReadOnly)
        {
            SetAccessDgColumn(country1, ReadOnly, 1);
            SetAccessDgColumn(region1, ReadOnly, 1);
            SetAccessDgColumn(district1, ReadOnly, 1);
            SetAccessDgColumn(towns1, ReadOnly, 1);
        }
        /// <summary>
        /// Доступ на редактирование колонки редактирования
        /// </summary>
        /// <param name="ReadOnly">true - доступ запрещён</param>
        public void SetAccesKoatuuUpdate(bool ReadOnly)
        {
            SetAccessDgColumn(country1, ReadOnly, 2);
            SetAccessDgColumn(region1, ReadOnly, 2);
            SetAccessDgColumn(district1, ReadOnly, 2);
            SetAccessDgColumn(towns1, ReadOnly, 2);
        }
        /// <summary>
        /// Доступно ли пользователю добавлять и удалять данные
        /// </summary>
        /// <param name="acc">true - нету доступа</param>
        public void SetAddRemoveAccess(bool acc)
        {
            SetIndividualAccess(country1, acc);
            SetIndividualAccess(region1, acc);
            SetIndividualAccess(district1, acc);
            SetIndividualAccess(towns1, acc);
        }
        /// <summary>
        /// Видимость таблицы со странами
        /// </summary>
        /// <param name="vis"></param>
        public void SetCountryVisible(bool vis)
        {
            country1.Visible = vis;
            district1.Visible = vis;
            region1.Visible = vis;
            towns1.Visible = vis;
            RelocateControl(district1, country1.Location, vis);
            RelocateControl(region1, country1.Location, vis);
            RelocateControl(towns1, country1.Location, vis);
        }

        /// <summary>
        /// Задать есть ли возможность редактирования
        /// </summary>
        /// <param name="_readOnly">только чтение - true:readonly=true;нет доступа</param>
        private void SetReadOnly(bool _readOnly)
        {
            if (this.InvokeRequired)
            {
                this.Invoke((ThreadStart)delegate ()
                {
                    SetAddRemoveAccess(_readOnly);
                    SetAccesKoatuuUpdate(_readOnly);
                    SetAccesNameUpdate(_readOnly);
                    SetHotKeys(!_readOnly);
                    CityReposition(_readOnly);
                });
            }
            else
            {
                SetAddRemoveAccess(_readOnly);
                SetAccesKoatuuUpdate(_readOnly);
                SetAccesNameUpdate(_readOnly);
                SetHotKeys(!_readOnly);
                CityReposition(_readOnly);
            }
            
        }
        #endregion
        #region Hidden functions

        /// <summary>
        /// Событие закрытия формы
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseForm(object sender, EventArgs e)
        {
            da1 = towns1.dataGridView2.CurrentRow == null ? -1 : Convert.ToInt32(towns1.dataGridView2.CurrentRow.Cells[0].Value);
           // this.FindForm().Close();

            CitySelected?.Invoke(towns1, new CCitySelectedEventArgs() { City = da1 });
        }
        /// <summary>
        /// Устанавливает видимость колонки таблица указанного контрола
        /// </summary>
        /// <param name="ctrl"></param>
        /// <param name="vis"></param>
        private void SetVisibleKOATUU(Subject ctrl, bool vis)
        {
            ctrl.SetKoatuuVisible(vis);
           // ctrl.dataGridView2.Columns[2].Visible = vis;
        }
        private void SetAccessDgColumn(Subject ctrl, bool ReadOnly,int columnIndex)
        {
            ctrl.dataGridView2.Columns[columnIndex].ReadOnly = ReadOnly;
        }
        /// <summary>
        /// Выбрать город, убрать город
        /// </summary>
        /// <param name="repo">true - пользователь может выбрать город кнопкой "Выбрать"</param>
        public void CityReposition(bool repo)
        {
            towns1.button1.Visible = repo;
            towns1.button2.Visible = repo;
            if (repo)
            {
                towns1.dataGridView2.Height -= 50;                
               /* towns1.bUsers_add_.Location = new Point(towns1.bUsers_add_.Location.X, towns1.bUsers_add_.Location.Y - towns1.button1.Height-5);
                towns1.bUser_del_.Location = new Point(towns1.bUser_del_.Location.X, towns1.bUser_del_.Location.Y - towns1.button1.Height-5);*/
            }
            else
            {
                towns1.dataGridView2.Height += 50;
                /*towns1.bUsers_add_.Location = new Point(towns1.bUsers_add_.Location.X, towns1.bUsers_add_.Location.Y + towns1.button1.Height+5);
                towns1.bUser_del_.Location = new Point(towns1.bUser_del_.Location.X, towns1.bUser_del_.Location.Y + towns1.button1.Height+5);*/
            }
        }
        /// <summary>
        /// Изменение размеры контролов, убирая кнопки из виду
        /// </summary>
        /// <param name="ctrl"></param>
        /// <param name="acc"></param>
        private void SetIndividualAccess(Subject ctrl,bool acc)
        {
            ctrl.bUsers_add_.Visible = !acc;
            ctrl.bUser_del_.Visible = !acc;
            if (acc)
                ctrl.dataGridView2.Size = new Size(ctrl.dataGridView2.Width, ctrl.dataGridView2.Height + ctrl.bUsers_add_.Height);
            else
                ctrl.dataGridView2.Size = new Size(ctrl.dataGridView2.Width, ctrl.dataGridView2.Height - ctrl.bUsers_add_.Height);
        }      
        /// <summary>
        /// Функция смещения контрола
        /// </summary>
        /// <param name="ctrl">Контрол, которые смещаем</param>
        /// <param name="change">на какое расстояние смещаем</param>
        private void RelocateControl(Subject ctrl, Point change, bool plus)
        {
            if (ctrl.Visible == plus)
                return;
            if (plus)
            ctrl.Location = new Point(ctrl.Location.X + change.X, ctrl.Location.Y);
            else
                ctrl.Location = new Point(ctrl.Location.X - change.X, ctrl.Location.Y);
        }   
        /// <summary>
        /// Инициализация контрола
        /// </summary>
        private void InitializeComponent()
        {
            this.towns1 = new KOATUU.Towns();
            this.region1 = new KOATUU.Region();
            this.district1 = new KOATUU.District();
            this.country1 = new KOATUU.Country();
            this.SuspendLayout();
            // 
            // towns1
            // 
            this.towns1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.towns1.Location = new System.Drawing.Point(606, 0);
            this.towns1.Name = "towns1";
            this.towns1.Size = new System.Drawing.Size(209, 386);
            this.towns1.TabIndex = 3;
            // 
            // region1
            // 
            this.region1.Dock = System.Windows.Forms.DockStyle.Left;
            this.region1.Location = new System.Drawing.Point(388, 0);
            this.region1.Name = "region1";
            this.region1.Padding = new System.Windows.Forms.Padding(0, 0, 20, 0);
            this.region1.Size = new System.Drawing.Size(218, 386);
            this.region1.TabIndex = 2;
            // 
            // district1
            // 
            this.district1.Dock = System.Windows.Forms.DockStyle.Left;
            this.district1.Location = new System.Drawing.Point(167, 0);
            this.district1.Name = "district1";
            this.district1.Padding = new System.Windows.Forms.Padding(0, 0, 20, 0);
            this.district1.Size = new System.Drawing.Size(221, 386);
            this.district1.TabIndex = 1;
            // 
            // country1
            // 
            this.country1.Dock = System.Windows.Forms.DockStyle.Left;
            this.country1.Location = new System.Drawing.Point(0, 0);
            this.country1.Name = "country1";
            this.country1.Padding = new System.Windows.Forms.Padding(0, 0, 20, 0);
            this.country1.Size = new System.Drawing.Size(167, 386);
            this.country1.TabIndex = 0;
            // 
            // BaseKOATUU
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.towns1);
            this.Controls.Add(this.region1);
            this.Controls.Add(this.district1);
            this.Controls.Add(this.country1);
            this.Name = "BaseKOATUU";
            this.Size = new System.Drawing.Size(815, 386);
            this.ResumeLayout(false);

        }
        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
        #endregion

        private void district1_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Класс параметров Для события об выборе города
        /// </summary>
        public class CCitySelectedEventArgs : EventArgs
        {
            /// <summary>
            /// Город
            /// </summary>
            public int City
            {
                get; set;
            }
        }
    }
}
