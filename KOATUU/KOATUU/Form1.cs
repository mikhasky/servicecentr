﻿using FirebirdSql.Data.FirebirdClient;
using KOATUU.LoadMainData;
using Serializable;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace KOATUU
{
    public partial class Form1 : Form
    {
     
        public Form1()
        {    
            if (this.InvokeRequired)
            {
                this.Invoke((ThreadStart)delegate ()
                {
                    InitializeComponent();
                    baseKOATUU1.MainLoad(Data.ConnectionString);
                    baseKOATUU1.SetHotKeys(true);
                });
            }
            else
            {
                InitializeComponent();
                baseKOATUU1.MainLoad(Data.ConnectionString);
                baseKOATUU1.SetHotKeys(true);
            }
          
           // Start();
        }
       
        
        /// <summary>
        /// Заполнение таблиц необработанным данными и данными с базы
        /// </summary>
        public void FillMerger()
        {
            dataGridView1.Rows.Clear();
            dataGridView2.Rows.Clear();
            dataGridView3.Rows.Clear();
            dataGridView4.Rows.Clear();
            dataGridView5.Rows.Clear();
            dataGridView6.Rows.Clear();
            //заполнение таблицы с необработанными областями
            if (KOATUU_DATA.regions != null)
                foreach (var item in KOATUU_DATA.regions)
                    dataGridView1.Rows.Add(-1, item.name, item.idRegion);
            //заполнение таблицы с областями из базы
            foreach (var item in Data.tableDAO.OrderBy(i => i.DAO3))
                dataGridView2.Rows.Add(item.DAO1, item.DAO3, item.DAO7);
            //заполнение таблицы с необработанными городами
            if (KOATUU_DATA.towns != null)
                foreach (var item in KOATUU_DATA.towns)
                    dataGridView3.Rows.Add(item.type, item.name, item.idRegion);
            //заполнение таблицы с городами из базы
            foreach (var item in Data.tableDA.OrderBy(i => i.DA3))
                dataGridView4.Rows.Add(item.DA1, item.DA3, item.DA16);
            //заполнение таблицы с необработанными районами
            if (KOATUU_DATA.districts != null)
                foreach (var item in KOATUU_DATA.districts)
                    dataGridView5.Rows.Add(-1,item.name,item.idRegion);
            //заполнение таблицы с районами из базы
            foreach (var item in Data.tableDAR.OrderBy(i=>i.DAR3))
                dataGridView6.Rows.Add(item.DAR1, item.DAR3, item.DAR6);
        }
        /// <summary>
        /// Изменение названия или КОАТУУ области пользователем
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView2_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            Data.UpdateDAO(dataGridView2.CurrentRow.Cells[2].Value.ToString(), dataGridView2.CurrentRow.Cells[1].Value.ToString(), dataGridView2.CurrentRow.Cells[0].Value.ToString());
        }
        /// <summary>
        /// Изменение названия или КОАТУУ города пользователем
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView4_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            Data.UpdateDa(dataGridView4.CurrentRow.Cells[1].Value.ToString(), dataGridView4.CurrentRow.Cells[2].Value.ToString(), dataGridView4.CurrentRow.Cells[0].Value.ToString());
        }
        /// <summary>
        /// Перенос города из файла в базу пользователем
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            dataGridView4.CurrentRow.Cells[1].Value = dataGridView3.CurrentRow.Cells[1].Value;
            dataGridView4.CurrentRow.Cells[2].Value = dataGridView3.CurrentRow.Cells[2].Value;
            dataGridView3.Rows.Remove(dataGridView3.CurrentRow);
            dataGridView4_CellEndEdit(null, null);
        }
        /// <summary>
        ///  Перенос области из файла в базу пользователем
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            dataGridView2.CurrentRow.Cells[1].Value = dataGridView1.CurrentRow.Cells[1].Value;
            dataGridView2.CurrentRow.Cells[2].Value = dataGridView1.CurrentRow.Cells[2].Value;
            dataGridView1.Rows.Remove(dataGridView1.CurrentRow);
            dataGridView2_CellEndEdit(null, null);
        }
        /// <summary>
        /// вставляем новую область в базу из текущей строки необработанных областей
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            if (dataGridView1.CurrentRow == null)
                return;
            string id = Data.GenId("dao").ToString();
            using (FbConnection fConnection = new FbConnection(Data.ConnectionString))
            {
                fConnection.Open();
                using (FbTransaction fTrans = fConnection.BeginTransaction())
                {
                    using (FbCommand fCommand = new FbCommand(@"INSERT INTO dao(dao1,dao2,dao3,dao5,dao7)
                        VALUES(@dao1,0,@dao3,0,@dao7)", fConnection, fTrans))
                    {
                        fCommand.Parameters.Add("@dao1", id);
                        fCommand.Parameters.Add("@dao3", dataGridView1.CurrentRow.Cells[1].Value);
                        fCommand.Parameters.Add("@dao7", dataGridView1.CurrentRow.Cells[2].Value);
                        fCommand.ExecuteNonQuery();
                        fTrans.Commit();
                        dbMaster.FirstInsertOMG<DAO>(Data.tableDAO, new DAO() { DAO1 = Convert.ToInt32(id), DAO2 = 0, DAO3 = dataGridView1.CurrentRow.Cells[1].Value.ToString(), DAO4 = string.Empty, DAO5 = 0, DAO6 = string.Empty, DAO7 = dataGridView1.CurrentRow.Cells[2].Value.ToString() });
                    }
                }
                dataGridView1.CurrentRow.Cells[0].Value = id;
                dataGridView2.Rows.Add(dataGridView1.CurrentRow.Cells[0].Value, dataGridView1.CurrentRow.Cells[1].Value, dataGridView1.CurrentRow.Cells[2].Value);
                dataGridView1.Rows.Remove(dataGridView1.CurrentRow);
            }
        }
       
        /// <summary>
        /// вставляем новый город в базу из текущей строки необработанных областей
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button4_Click(object sender, EventArgs e)
        {
            if (dataGridView3.CurrentRow == null)
                return;
            string id = Data.GenId("da").ToString();
            DAR dar=null;
            string dar6 = dataGridView3.CurrentRow.Cells[2].Value.ToString();
            if (dataGridView3.CurrentRow.Cells[2].Value.ToString().Length > 5)            
                dar6 = dar6.Remove(5);            
                dar = Data.tableDAR.Where(i => i.DAR6 == dar6).FirstOrDefault();            
            string da14 = "0";
            if (dar != null)
                da14 = dar.DAR1.ToString();
            else
                MessageBox.Show("Не определён район этого города, район будет установлен по умолчанию");           
            using (FbConnection fConnection = new FbConnection(Data.ConnectionString))
            {
                fConnection.Open();
                using (FbTransaction fTrans = fConnection.BeginTransaction())
                {
                    using (FbCommand fCommand = new FbCommand(@"INSERT INTO da(da1,da2,da3,da14,da16)
                        VALUES(@da1,@da2,@da3,@da14,@da16)", fConnection, fTrans))
                    {
                        fCommand.Parameters.Add("@da1", id);
                        fCommand.Parameters.Add("@da2", KOATUU_DATA.getType(dataGridView3.CurrentRow.Cells[0].Value.ToString()));
                        fCommand.Parameters.Add("@da3", dataGridView3.CurrentRow.Cells[1].Value);
                        fCommand.Parameters.Add("@da14", da14);
                        fCommand.Parameters.Add("@da16", dataGridView3.CurrentRow.Cells[2].Value);
                        fCommand.ExecuteNonQuery();
                        fTrans.Commit();
                        dbMaster.FirstInsertOMG<DA>(Data.tableDA, new DA() { DA1 = Convert.ToInt32(id), DA2 = KOATUU_DATA.getType(dataGridView3.CurrentRow.Cells[0].Value.ToString()), DA3 = dataGridView3.CurrentRow.Cells[1].Value.ToString(), DA9 = DateTime.Now, DA10 = 0, DA12 = 0, DA13 = 0, DA14 = Convert.ToInt32(da14), DA15 = 2, DA16 = dataGridView3.CurrentRow.Cells[2].Value.ToString() });
                    }
                }
                dataGridView3.CurrentRow.Cells[0].Value = id;
                dataGridView4.Rows.Add(dataGridView3.CurrentRow.Cells[0].Value, dataGridView3.CurrentRow.Cells[1].Value, dataGridView3.CurrentRow.Cells[2].Value);
                dataGridView3.Rows.Remove(dataGridView3.CurrentRow);
            }
        }
        /// <summary>
        /// Повторное сканирование данных
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button6_Click(object sender, EventArgs e)
        {
            KOATUU_DATA.Start(Data.ConnectionString,this);
        }
        /// <summary>
        /// Повторное сканирование данных
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button5_Click(object sender, EventArgs e)
        {
            KOATUU_DATA.Start(Data.ConnectionString,this);
        }
        /// <summary>
        /// Повторное сканирование данных
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button7_Click(object sender, EventArgs e)
        {
            KOATUU_DATA.Start(Data.ConnectionString,this);
        }
        /// <summary>
        /// Перенос района из файла в базу пользователем
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button8_Click(object sender, EventArgs e)
        {
            dataGridView6.CurrentRow.Cells[1].Value = dataGridView5.CurrentRow.Cells[1].Value;
            dataGridView6.CurrentRow.Cells[2].Value = dataGridView5.CurrentRow.Cells[2].Value;
            dataGridView5.Rows.Remove(dataGridView5.CurrentRow);
            dataGridView6_CellEndEdit(null, null);
        }
        /// <summary>
        /// вставляем новый район в базу из текущей строки необработанных областей
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button9_Click(object sender, EventArgs e)
        {
            if (dataGridView5.CurrentRow == null)
                return;
            string id = Data.GenId("dar").ToString();
            string dao7 = dataGridView5.CurrentRow.Cells[2].Value.ToString();            
            string dar2 = "0";
            if (dao7.Length > 2)
                dao7 = dao7.Remove(2);
            DAO dao = Data.tableDAO.Where(i => i.DAO7 == dao7).FirstOrDefault();
            if (dao != null)
                dar2 = dao.DAO1.ToString();
            else
                MessageBox.Show("Не найдена область для указанного района, область будет установлена по умолчанию");
            using (FbConnection fConnection = new FbConnection(Data.ConnectionString))
            {
                fConnection.Open();
                using (FbTransaction fTrans = fConnection.BeginTransaction())
                {
                    using (FbCommand fCommand = new FbCommand(@"INSERT INTO dar(dar1,dar2,dar3,dar6)
                        VALUES(@dar1,@dar2,@dar3,@dar6)", fConnection, fTrans))
                    {
                        fCommand.Parameters.Add("@dar1", id);
                        fCommand.Parameters.Add("@dar2", dar2);
                        fCommand.Parameters.Add("@dar3", dataGridView5.CurrentRow.Cells[1].Value);
                        fCommand.Parameters.Add("@dar6", dataGridView5.CurrentRow.Cells[2].Value);
                        fCommand.ExecuteNonQuery();
                        fTrans.Commit();
                        dbMaster.FirstInsertOMG<DAR>(Data.tableDAR, new DAR() { DAR1 = Convert.ToInt32(id), DAR2 =Convert.ToInt32(dar2), DAR3 = dataGridView5.CurrentRow.Cells[1].Value.ToString(),DAR4=0,DAR5=string.Empty,DAR6= dataGridView5.CurrentRow.Cells[2].Value.ToString()});
                    }
                }
                dataGridView5.CurrentRow.Cells[0].Value = id;
                dataGridView6.Rows.Add(dataGridView5.CurrentRow.Cells[0].Value, dataGridView5.CurrentRow.Cells[1].Value, dataGridView5.CurrentRow.Cells[2].Value);
                dataGridView5.Rows.Remove(dataGridView5.CurrentRow);
            }
        }
        /// <summary>
        /// Изменение названия или КОАТУУ района пользователем
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView6_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            Data.UpdateDAR(dataGridView6.CurrentRow.Cells[2].Value.ToString(), dataGridView6.CurrentRow.Cells[1].Value.ToString(), dataGridView6.CurrentRow.Cells[0].Value.ToString());
        }
        /// <summary>
        /// поиск области, принадлежащей району
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView5_SelectionChanged(object sender, EventArgs e)
        {
            string oblid = dataGridView5.CurrentRow.Cells[2].Value.ToString();
            if (oblid.Length > 2)
                oblid = oblid.Remove(2);
            DAO dao = Data.tableDAO.Where(i => i.DAO7 == oblid).FirstOrDefault();
            if (dao != null)
                label1.Text = dao.DAO3;
            else
                label1.Text = "не найдено";
        }
        /// <summary>
        /// поиск района, принадлежащему городу
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView3_SelectionChanged(object sender, EventArgs e)
        {
            DAR dar = null;
            string dar6 = dataGridView3.CurrentRow.Cells[2].Value.ToString();
            if (dataGridView3.CurrentRow.Cells[2].Value.ToString().Length > 5)            
                dar6 = dar6.Remove(5);            
            dar = Data.tableDAR.Where(i => i.DAR6 == dar6).FirstOrDefault();

            string da14 = "0";
            if (dar != null)
                label2.Text = dar.DAR3;
            else
                label2.Text = "не найдено";
        }
    }
}
