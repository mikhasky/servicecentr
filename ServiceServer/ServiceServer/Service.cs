﻿using LinqToDB;
using MKP.Controls.Extension;
using MKP.DataModels;
using MyContracts;
using MyContracts.Contrals;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ServiceServer
{   
        class Service : iConstract
        {
        public int AddDirectoryItem(byte[] item, int idDirectory)
        {            
            try
            {
                //MyContracts.Contrals.Admin
                switch (idDirectory)
                {
                    //if (obj is Brand)
                    case 0:
                        var brand = Encoding.UTF8.GetString(item).Deserialize<Brand>();
                      
                        using (var db = new MKP.DataModels.BDETALONDB(Program.conBD))
                        {
                            db.BeginTransaction();
                            var brnd = new MKP.DataModels.BRAND()
                            {
                                BRAND1 = db.Gen_ID<MKP.DataModels.BRAND>(),
                                BRAND2 = brand.brand
                            };
                            db.Insert(brnd);
                        db.CommitTransaction();
                            return brnd.BRAND1;
                        }                     
                            //MKP.DataModels.BRAND
                            break;
                    //  if (obj is Clients)
                    case 1:
                        var client = Encoding.UTF8.GetString(item).Deserialize<Clients>();
                        using (var db = new MKP.DataModels.BDETALONDB(Program.conBD))
                        {
                            db.BeginTransaction();
                            var clnt = new MKP.DataModels.CLIENT()
                            {
                                CLIENT1 = db.Gen_ID<MKP.DataModels.CLIENT>(),
                                CLIENT2 = client.FIO,
                                CLIENT3 = client.INN,
                                CLIENT5 = client.adress,
                                CLIENT6 = client.phone,
                                CLIENT7 = client.email,
                                CLIENT8 = client.bblack_list?"1":"0"
                            };
                            db.Insert(clnt);
                            db.CommitTransaction();
                            return clnt.CLIENT1;
                        }
                        break;
                    // if (obj is Complect)
                    case 2:
                        var complect = Encoding.UTF8.GetString(item).Deserialize<Complect>();
                        using (var db = new MKP.DataModels.BDETALONDB(Program.conBD))
                        {
                            db.BeginTransaction();
                            var cmplt = new MKP.DataModels.COMPLECT()
                            {
                                COMPLECT1 = db.Gen_ID<MKP.DataModels.COMPLECT>(),
                                COMPLECT2 = complect.detail
                            };
                            db.Insert(cmplt);
                            db.CommitTransaction();
                            return cmplt.COMPLECT1;
                        }
                        break;

                    //  if (obj is Complete)
                    //case 3:
                    //    var complete = Encoding.UTF8.GetString(item).Deserialize<Complete>();
                    //    break;
                    // if (obj is Damag)
                    case 4:
                        var damag = Encoding.UTF8.GetString(item).Deserialize<Damag>();
                        break;
                    //  if (obj is mainhistory)
                    case 5:
                        var mainhistory = Encoding.UTF8.GetString(item).Deserialize<mainhistory>();
                        break;
                    //  if (obj is DeviceTP)
                    case 6:
                        var devicetp = Encoding.UTF8.GetString(item).Deserialize<DeviceTP>();
                        break;
                    //if (obj is Metki)
                    case 7:
                        var metki = Encoding.UTF8.GetString(item).Deserialize<Metki>();
                        break;
                    // if (obj is Statusy)
                    case 8:
                        var statusy = Encoding.UTF8.GetString(item).Deserialize<Statusy>();
                        break;
                    //if (obj is work)
                    case 9:
                        var cwork = Encoding.UTF8.GetString(item).Deserialize<work>();
                        break;
                    //  if (obj is workEnum)
                    case 10:
                        var cworkenum = Encoding.UTF8.GetString(item).Deserialize<workEnum>();
                        break;
                    //  if (obj is Admin)
                    case 99:
                        var admin = Encoding.UTF8.GetString(item).Deserialize<Admin>();
                        break;
                    default:
                        Console.WriteLine("Error adding new record; Invalid data key(attack?)"); return -1;
                }
            }
            catch(Exception ex) {
                Console.WriteLine("Error adding new record - "+ex.ToString());
                return -1;
            }
            
        }

        public int getAccess(string login, string pass)
        {
            using (var db = new MKP.DataModels.BDETALONDB(Program.conBD))
            {
                var user = db.UUSER.Where(i => i.UUSER2 == login && i.UUSER3 == pass).FirstOrDefault();
                if (user == null)
                    return -1;
                return user.UUSER5;
            }
        }

        public byte[] LoadDirectoryById(int idDirectory)
        {
            throw new NotImplementedException();
        }

        public byte[] LoadDirectoryByItemId(int idDirectory, int idItem)
        {
            throw new NotImplementedException();
        }

        public byte[] LoadListDirectoryChanges(int idUser)
        {
            throw new NotImplementedException();
        }

        public byte[] LoadNewDirectoryItems(int idDirectory)
        {
            throw new NotImplementedException();
        }

        public int Login(string login, string pass)
        {
            using (var db = new MKP.DataModels.BDETALONDB(Program.conBD))
            {
                var user = db.UUSER.Where(i => i.UUSER2 == login && i.UUSER3 == pass).FirstOrDefault();
                if (user == null)
                    return -1;
                return user.UUSER1;
            }
        }

        public int Register(string login, string pass,string name)
        {
            using (var db = new MKP.DataModels.BDETALONDB(Program.conBD))
            {
                db.BeginTransaction();
                var user = new UUSER()
                {
                    UUSER1 = db.Gen_ID<UUSER>(),
                    UUSER2 = login,
                    UUSER3 = pass,
                    UUSER4 = name,
                    UUSER5 = 0,
                    UUSER6 = null,
                    UUSER7 = 0
                };
                db.Insert(user);               
                db.CommitTransaction();
                return user.UUSER1;
            }
        }

        public bool RemoveDirectoryItemById(int idDirectory, int id)
        {
            throw new NotImplementedException();
        }

        public string Say(string text)
            {
                Console.WriteLine($"Not bad man : {text}");
                return "OK!";
            }
        }    
}
