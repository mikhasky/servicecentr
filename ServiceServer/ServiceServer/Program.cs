﻿using LinqToDB;
using MKP.DataModels;
using MyContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace ServiceServer
{
    class Program
    {
        public static string conBD;
        static void Main(string[] args)
        {
            Console.WriteLine("Подключение к БД...");
            try
            {
                conBD = dblib.Encryption.DecryptToString(@"bin\dbset.dll", "5635655899633252");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Ошибка при подключении к БД: " + ex.ToString());
                Console.ReadLine();
                Environment.Exit(0);
            }
            Console.WriteLine("Загрузка БД...");
            try
            {
                LoadDatabase();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Ошибка во время загрузки БД: " + ex.ToString());
                Console.ReadLine();
                Environment.Exit(0);
            }
            Console.WriteLine("Преобразование данных...");
            try
            {
                LoadItems(); 
            }
            catch (Exception ex)
            {
                Console.WriteLine("Ошибка во время приобразования данных: " + ex.ToString());
                Console.ReadLine();
                Environment.Exit(0);
            }
            Console.Title = "SERVICE SERVER";
            Uri address = new Uri("http://localhost:1515/iConstract"); //address
            ServiceHost host = new ServiceHost(typeof(Service));
            host.AddServiceEndpoint(typeof(iConstract), new BasicHttpBinding(), address);
            host.Open();
            Console.WriteLine("Ready for work...");
            Console.ReadKey();
            host.Close();
        }
        public static MyContracts.Contrals.Common.ALLItems Items;
        public static List<BRAND> lstBrand;
        public static List<CLIENT> lstClient;
        public static List<COMPLECT> lstComplect;
        public static List<DAMAG> lstDamag;
        public static List<DEVICE> lstDevice;
        public static List<DEVICE_STATUS> lstDeviceStat;
        public static List<DEVICE_STSHST> lstDeviceStatHistory;
        public static List<DEVICE_TP> lstDeviceType;
        public static List<LABEL> lstLabel;
        public static List<MESSAGES> lstMessages;
        public static List<SPRAV_WORK> lstSprawWork;
        public static List<UUSER> lstUser;
        public static List<WWORK> lstWork;
        static void LoadItems()
        {
            Items = new MyContracts.Contrals.Common.ALLItems();

            Items.brands = new List<MyContracts.Contrals.Brand>();
            foreach (var item in lstBrand)
            {
                Items.brands.Add(new MyContracts.Contrals.Brand()
                {
                    id=item.BRAND1,
                    brand=item.BRAND2
                });
            }
        }
        static void LoadDatabase()
        {
            using (var db = new MKP.DataModels.BDETALONDB(conBD))
            {
                lstBrand = db.BRAND.ToList();
                lstClient = db.CLIENT.ToList();
                lstComplect = db.COMPLECT.ToList();
                lstDamag = db.DAMAG.ToList();
                lstDevice = db.DEVICE.ToList();
                lstDeviceStat = db.DEVICE_STATUS.ToList();
                lstDeviceStatHistory = db.DEVICE_STSHST.ToList();
                lstDeviceType = db.DEVICE_TP.ToList();
                lstLabel = db.LABEL.ToList();
                lstMessages = db.MESSAGES.ToList();
                lstSprawWork = db.SPRAV_WORK.ToList();
                lstUser = db.UUSER.ToList();
                lstWork = db.WWORK.ToList();
            }
        }
        static void UpdateBrand(BRAND obj)
        {
            using (var db = new MKP.DataModels.BDETALONDB(conBD))
            {
                db.BeginTransaction();
                db.BRAND.Where(i => i.BRAND1 == obj.BRAND1).Set(i => i.BRAND2, obj.BRAND2).Update();
                db.CommitTransaction();
            }
        }
        static void UpdateClient(CLIENT obj)
        {
            using (var db = new MKP.DataModels.BDETALONDB(conBD))
            {
                db.BeginTransaction();
                db.CLIENT.Where(i => i.CLIENT1 == obj.CLIENT1).Set(i => i.CLIENT2, obj.CLIENT2).Set(i => i.CLIENT3, obj.CLIENT3).Set(i => i.CLIENT5, obj.CLIENT5).Set(i => i.CLIENT6,obj.CLIENT6).Set(i=>i.CLIENT7,obj.CLIENT7).Set(i=>i.CLIENT8,obj.CLIENT8).Update();
                db.CommitTransaction();
            }
        }
        static void UpdateComplect(COMPLECT obj)
        {
            using (var db = new MKP.DataModels.BDETALONDB(conBD))
            {
                db.BeginTransaction();
                db.COMPLECT.Where(i => i.COMPLECT1 == obj.COMPLECT1).Set(i => i.COMPLECT2, obj.COMPLECT2).Update();
                db.CommitTransaction();
            }
        }
        static void UpdateDamag(DAMAG obj)
        {
            using (var db = new MKP.DataModels.BDETALONDB(conBD))
            {
                db.BeginTransaction();
                db.DAMAG.Where(i => i.DAMAG1 == obj.DAMAG1).Set(i => i.DAMAG2, obj.DAMAG2).Update();
                db.CommitTransaction();
            }
        }
        static void UpdateDevice(DEVICE obj)
        {
            using (var db = new MKP.DataModels.BDETALONDB(conBD))
            {
                db.BeginTransaction();
                db.DEVICE.Where(i => i.DEVICE1 == obj.DEVICE1).Set(i => i.DEVICE2, obj.DEVICE2).Set(i => i.DEVICE3,obj.DEVICE3).Set(i => i.DEVICE4,obj.DEVICE4).Set(i => i.DEVICE5,obj.DEVICE5).Set(i => i.DEVICE6,obj.DEVICE6).Set(i => i.DEVICE7,obj.DEVICE7).Set(i => i.DEVICE8,obj.DEVICE8).Update();
                db.CommitTransaction();
            }
        }
        static void UpdateDeviceStat(DEVICE_STATUS obj)
        {
            using (var db = new MKP.DataModels.BDETALONDB(conBD))
            {
                db.BeginTransaction();
                db.DEVICE_STATUS.Where(i => i.DEVICE_STATUS1 == obj.DEVICE_STATUS1).Set(i => i.DEVICE_STATUS2, obj.DEVICE_STATUS2).Set(i=>i.DEVICE_STATUS3,obj.DEVICE_STATUS3).Update();
                db.CommitTransaction();
            }
        }
        static void UpdateDeviceStatHistory(DEVICE_STSHST obj)
        {
            using (var db = new MKP.DataModels.BDETALONDB(conBD))
            {
                db.BeginTransaction();
                db.DEVICE_STSHST.Where(i => i.DEVICE_STSHST1 == obj.DEVICE_STSHST1).Set(i => i.DEVICE_STSHST2, obj.DEVICE_STSHST2).Set(i => i.DEVICE_STSHST3, obj.DEVICE_STSHST3).Set(i => i.DEVICE_STSHST4, obj.DEVICE_STSHST4).Set(i => i.DEVICE_STSHST5, obj.DEVICE_STSHST5).Set(i => i.DEVICE_STSHST6, obj.DEVICE_STSHST6).Update();
                db.CommitTransaction();
            }
        }
        static void UpdateDeviceType(DEVICE_TP obj)
        {
            using (var db = new MKP.DataModels.BDETALONDB(conBD))
            {
                db.BeginTransaction();
                db.DEVICE_TP.Where(i => i.DEVICE_TP1 == obj.DEVICE_TP1).Set(i => i.DEVICE_TP2, obj.DEVICE_TP2).Update();
                db.CommitTransaction();
            }
        }
        static void UpdateLabel(LABEL obj)
        {
            using (var db = new MKP.DataModels.BDETALONDB(conBD))
            {
                db.BeginTransaction();
                db.LABEL.Where(i => i.LABEL1 == obj.LABEL1).Set(i => i.LABEL2, obj.LABEL2).Update();
                db.CommitTransaction();
            }
        }
        static void UpdateMessages(MESSAGES obj)
        {
            using (var db = new MKP.DataModels.BDETALONDB(conBD))
            {
                db.BeginTransaction();
                db.MESSAGES.Where(i => i.MESSAGES1 == obj.MESSAGES1).Set(i => i.MESSAGES2, obj.MESSAGES2).Set(i => i.MESSAGES3, obj.MESSAGES3).Set(i => i.MESSAGES4, obj.MESSAGES4).Set(i => i.MESSAGES5, obj.MESSAGES5).Update();
                db.CommitTransaction();
            }
        }
        static void UpdateSprawWork(SPRAV_WORK obj)
        {
            using (var db = new MKP.DataModels.BDETALONDB(conBD))
            {
                db.BeginTransaction();
                db.SPRAV_WORK.Where(i => i.SPRAV_WORK1 == obj.SPRAV_WORK1).Set(i => i.SPRAV_WORK2, obj.SPRAV_WORK2).Set(i => i.SPRAV_WORK3, obj.SPRAV_WORK3).Set(i => i.SPRAV_WORK4, obj.SPRAV_WORK4).Update();
                db.CommitTransaction();
            }
        }
        static void UpdateUser(UUSER obj)
        {
            using (var db = new MKP.DataModels.BDETALONDB(conBD))
            {
                db.BeginTransaction();
                db.UUSER.Where(i => i.UUSER1 == obj.UUSER1).Set(i => i.UUSER2, obj.UUSER2).Set(i => i.UUSER3, obj.UUSER3).Set(i => i.UUSER4, obj.UUSER4).Set(i => i.UUSER5, obj.UUSER5).Set(i => i.UUSER6, obj.UUSER6).Set(i => i.UUSER7, obj.UUSER7).Update();
                db.CommitTransaction();
            }
        }
        static void UpdateWork(WWORK obj)
        {
            using (var db = new MKP.DataModels.BDETALONDB(conBD))
            {
                db.BeginTransaction();
                db.WWORK.Where(i => i.WWORK1 == obj.WWORK1).Set(i => i.WWORK2, obj.WWORK2).Set(i => i.WWORK3, obj.WWORK3).Set(i => i.WWORK4, obj.WWORK4).Set(i => i.WWORK5, obj.WWORK5).Set(i => i.WWORK6, obj.WWORK6).Set(i => i.WWORK7, obj.WWORK7).Update();
                db.CommitTransaction();
            }
        }
        static void InsertBrand(BRAND obj)
        {
            using (var db = new MKP.DataModels.BDETALONDB(conBD))
            {
                db.BeginTransaction();
                obj.BRAND1 = db.Gen_ID<BRAND>();
                db.Insert(obj);
                db.CommitTransaction();
            }
        }
        static void InsertClient(CLIENT obj)
        {
            using (var db = new MKP.DataModels.BDETALONDB(conBD))
            {
                db.BeginTransaction();
                obj.CLIENT1 = db.Gen_ID<CLIENT>();
                db.Insert(obj);
                db.CommitTransaction();
            }
        }
        static void InsertComplect(COMPLECT obj)
        {
            using (var db = new MKP.DataModels.BDETALONDB(conBD))
            {
                db.BeginTransaction();
                obj.COMPLECT1 = db.Gen_ID<COMPLECT>();
                db.Insert(obj);
                db.CommitTransaction();
            }
        }
        static void InsertDamag(DAMAG obj)
        {
            using (var db = new MKP.DataModels.BDETALONDB(conBD))
            {
                db.BeginTransaction();
                obj.DAMAG1 = db.Gen_ID<DAMAG>();
                db.Insert(obj);
                db.CommitTransaction();
            }
        }
        static void InsertDevice(DEVICE obj)
        {
            using (var db = new MKP.DataModels.BDETALONDB(conBD))
            {
                db.BeginTransaction();
                obj.DEVICE1 = db.Gen_ID<DEVICE>();
                db.Insert(obj);
                db.CommitTransaction();
            }
        }
        static void InsertDeviceStat(DEVICE_STATUS obj)
        {
            using (var db = new MKP.DataModels.BDETALONDB(conBD))
            {
                db.BeginTransaction();
                obj.DEVICE_STATUS1 = db.Gen_ID<DEVICE_STATUS>();
                db.Insert(obj);
                db.CommitTransaction();
            }
        }
        static void InsertDeviceStatHistory(DEVICE_STSHST obj)
        {
            using (var db = new MKP.DataModels.BDETALONDB(conBD))
            {
                db.BeginTransaction();
                obj.DEVICE_STSHST1 = db.Gen_ID<DEVICE_STSHST>();
                db.Insert(obj);
                db.CommitTransaction();
            }
        }
        static void InsertDeviceType(DEVICE_TP obj)
        {
            using (var db = new MKP.DataModels.BDETALONDB(conBD))
            {
                db.BeginTransaction();
                obj.DEVICE_TP1 = db.Gen_ID<DEVICE_TP>();
                db.Insert(obj);
                db.CommitTransaction();
            }
        }
        static void InsertLabel(LABEL obj)
        {
            using (var db = new MKP.DataModels.BDETALONDB(conBD))
            {
                db.BeginTransaction();
                obj.LABEL1 = db.Gen_ID<LABEL>();
                db.Insert(obj);
                db.CommitTransaction();
            }
        }
        static void InsertMessages(MESSAGES obj)
        {
            using (var db = new MKP.DataModels.BDETALONDB(conBD))
            {
                db.BeginTransaction();
                obj.MESSAGES1 = db.Gen_ID<MESSAGES>();
                db.Insert(obj);
                db.CommitTransaction();
            }
        }
        static void InsertSprawWork(SPRAV_WORK obj)
        {
            using (var db = new MKP.DataModels.BDETALONDB(conBD))
            {
                db.BeginTransaction();
                obj.SPRAV_WORK1 = db.Gen_ID<SPRAV_WORK>();
                db.Insert(obj);
                db.CommitTransaction();
            }
        }
        static void InsertUser(UUSER obj)
        {
            using (var db = new MKP.DataModels.BDETALONDB(conBD))
            {
                db.BeginTransaction();
                obj.UUSER1 = db.Gen_ID<UUSER>();
                db.Insert(obj);
                db.CommitTransaction();
            }
        }
        static void InsertWork(WWORK obj)
        {
            using (var db = new MKP.DataModels.BDETALONDB(conBD))
            {
                db.BeginTransaction();
                obj.WWORK1 = db.Gen_ID<WWORK>();
                db.Insert(obj);
                db.CommitTransaction();
            }
        }
    }

}
