﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace MyContracts
{
    [ServiceContract]
   public interface iConstract
    {
        [OperationContract]
        string Say(string text);
        [OperationContract]
        int getAccess(string login, string pass);
        /// <summary>
        /// Login,send login and pass
        /// </summary>
        /// <param name="login">Login</param>
        /// <param name="pass">Password</param>
        /// <returns>idUser(-1 if login failed)</returns>
        [OperationContract]
        int Login(string login,string pass);
        /// <summary>
        /// Login,send login and pass
        /// </summary>
        /// <param name="login">Login</param>
        /// <param name="pass">Password</param>
        /// <returns>idUser(-1 if register failed)</returns>
        [OperationContract]
        int Register(string login, string pass,string name);
        [OperationContract]
        byte[] LoadDirectoryById(int idDirectory);
        [OperationContract]
        byte[] LoadDirectoryByItemId(int idDirectory, int idItem);
        /// <summary>
        /// Load List id directories whic have changes
        /// </summary>
        /// <param name="idUser"></param>
        /// <returns></returns>
        [OperationContract]
        byte[] LoadListDirectoryChanges(int idUser);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="idDirectory"></param>
        /// <returns></returns>
        [OperationContract]
        byte[] LoadNewDirectoryItems(int idDirectory);
        /// <summary>
        /// AddDirectoryItemById
        /// </summary>
        /// <param name="item"></param>
        /// <returns>id new item(-1 if failed)</returns>
        [OperationContract]
        int AddDirectoryItem(byte[] item, int idDirectory);
        /// <summary>
        /// RemoveDirectoryItemById
        /// </summary>
        /// <param name="item"></param>
        /// <returns>true if success</returns>
        [OperationContract]
        bool RemoveDirectoryItemById(int idDirectory, int id);
    }
}
