﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace MyContracts.Contrals
{
    [DataContract]
    public class work
    {
        [DataMember]
        public int id;
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public string master { get; set; }
        [DataMember]
        public double pprice;
        public string price { get; set; }
        [DataMember]
        public int ccnt;
        public string cnt { get; set; }
        [DataMember]
        public DateTime ddata;
        public string data { get; set; }
    }
}
