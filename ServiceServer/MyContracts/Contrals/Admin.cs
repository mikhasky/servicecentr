﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace MyContracts.Contrals
{
    [DataContract]
    public class Admin
    {
        [DataMember]
        public int id;
        [DataMember]
        public string loginUser { get; set; }
        [DataMember]
        public string passUser { get; set; }
        [DataMember]
        public string NameUser { get; set; }
        [DataMember]
        public int idAccess;
        [DataMember]
        public byte[] Options;
        public int idLastMessage;

    }
}
