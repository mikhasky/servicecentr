﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace MyContracts.Contrals
{
    [DataContract]
    public class Brand
    {
        [DataMember]
        public int id;
        [DataMember]
        public string brand { get; set; }
    }
}
