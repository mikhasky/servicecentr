﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace MyContracts.Contrals
{
    [DataContract]
    public class Clients
    {
        [DataMember]
        public int id;
        [DataMember]
        public string FIO { get; set; }
        [DataMember]
        public string phone { get; set; }
        [DataMember]
        public string email { get; set; }
        [DataMember]
        public string adress { get; set; }
        [DataMember]
        public string INN { get; set; }
        [DataMember]
        public bool bblack_list { get; set; }       
    }
}
