﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace MyContracts.Contrals.Common
{
    [DataContract]
    public class ALLItems
    {
        [DataMember]
        public List<Brand> brands;
        [DataMember]
        public List<Clients> clients;
        [DataMember]
        public List<Complect> complect;
        [DataMember]
        public List<Damag> damag;
        [DataMember]
        public List<DeviceTP> dvcTP;
        [DataMember]
        public List<Metki> metki;
        [DataMember]
        public List<Statusy> stats;
        [DataMember]
        public List<work> Wrk;
        [DataMember]
        public List<workEnum> WrkEnum;
        [DataMember]
        public List<Admin> admin;
    }
}
