﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace MyContracts.Contrals
{
    [DataContract]
    public class DeviceTP
    {
        [DataMember]
        public int id;
        [DataMember]
        public string tip { get; set; }
    }
}
