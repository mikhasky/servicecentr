﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace MyContracts.Contrals
{
    [DataContract]
    public class mainhistory
    {
        [DataMember]
        public int id;
        [DataMember]
        public DateTime? ddata; 
        public string data { get; set; }
        [DataMember]
        public string number { get; set; }
        [DataMember]
        public string tip { get; set; }
        [DataMember]
        public string brand { get; set; }
        [DataMember]
        public string model { get; set; }
        [DataMember]
        public string serial_namber { get; set; }
        [DataMember]
        public string client { get; set; }
        [DataMember]
        public string adress { get; set; }
        [DataMember]
        public string phone { get; set; }
        [DataMember]
        public string master { get; set; }
        [DataMember]
        public string status { get; set; }
        [DataMember]
        public string metki { get; set; }

    }

}
