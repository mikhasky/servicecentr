﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace MyContracts.Contrals
{
    public class workEnum
    {
        public int id;
        public Image img { get; set; }
        public string name;
        public List<workEnum> Next { get; set; }
        public workEnum Prev;       
        public override string ToString()
        {
            return this.name;
        }
        public static List<workEnum> GetData(List<workData> lst)
        {
            List<workEnum> works = new List<workEnum>();
            for (int i = 0,j=-1; i < lst.Count; i++)
            {
                if (lst[i].refId != 0)
                    continue;
                j++;
                works.Add(new workEnum());
                if (lst[i].isFolder == 1)
                {
                    works[j].Next.AddRange(ScanFolder(lst, lst[i].id, works[j]));
                }
                else
                    works[j].Next.Add(new workEnum() { id = lst[i].id, name = lst[i].name, Prev = null, Next = null });
            }
            return works;
        }

        private static List<workEnum> ScanFolder(List<workData> lst, int id,workEnum curr)
        {
            curr.Next = new List<workEnum>();
            for (int i = 0; i < lst.Count; i++)
            {
                if (lst[i].refId != id)
                {
                    continue;
                } 
                if (lst[i].refId == 0)
                    curr.Next.Add(new workEnum() { id = lst[i].id, name = lst[i].name, Prev = curr, Next = null });
                else
                {
                    if (lst[i].isFolder == 1)
                    {
                        curr.Next.Add(new workEnum() { id = lst[i].id, name = lst[i].name, Prev = curr, Next = null });
                        curr.Next[i].Next = ScanFolder(lst, lst[i].id, curr.Next[i]);
                    }
                    else
                        curr.Next.Add(new workEnum() { id = lst[i].id, name = lst[i].name, Prev = curr, Next = null });
                }
            }
            return curr.Next;
        }
    }
    [DataContract]
    public class workData
    {
        [DataMember]
        public int id;//айди
        [DataMember]
        public string name;//имя
        [DataMember]
        public int refId;//айди внешней папки, если айди 0 - снаружи
        [DataMember]
        public short isFolder; //1 -  папка

    }
}
