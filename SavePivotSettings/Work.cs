﻿using FirebirdSql.Data.FirebirdClient;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace SavePivotSettings
{
    public class Work
    {
        public void updateSetting(byte[] setting,int id, string ConnBDD)
        {
            using (FbCommand fbCommand = new FbCommand("update spg set spg4 = @Setting where spg1 = @ID"))
            {
                fbCommand.Parameters.Add("@Setting", FbDbType.Binary, setting.Length).Value = setting;
                fbCommand.Parameters.Add("@ID", id);
                MKP.DBImplementation.WorkWhithDB.ExecutNonQuery(fbCommand,ConnBDD,true);
            }
        }
        public void updateSetting(byte[] setting, string ConnBDD, int moduleNUM, int enNumber, int i1)
        {
            var id = MKP.DBImplementation.WorkWhithDB.GetData<int>($"select spg1 from spg where spg2={moduleNUM} and spg3={enNumber} and spg6={i1}", ConnBDD);
            updateSetting(setting, id.Single(), ConnBDD);
        }
        public byte[] getSettingFromDB(string ConnBDD, int moduleNUM, int enNumber, int i1)
        {
            var id = MKP.DBImplementation.WorkWhithDB.GetData<int>($"select spg1 from spg where spg2={moduleNUM} and spg3={enNumber} and spg6={i1}", ConnBDD);
            if (id.Count == 0)
                return getSettingFromDB(ConnBDD, insertNewSetting(ConnBDD, moduleNUM, enNumber, i1).ID);
            if (id.Count ==1)
                return getSettingFromDB(ConnBDD, id.Single());
            return new byte[0];
        }
        public byte[] getSettingFromDB(string ConnBDD, int id)
        {
            byte[] result;
            if (id != -1)
                using (FbConnection fbConnection = new FbConnection(ConnBDD))
                {
                    using (FbCommand fbCommand = new FbCommand("SELECT spg4 from spg where spg1=@ID", fbConnection))
                    {
                        fbCommand.Parameters.AddWithValue("@ID", id);
                        fbConnection.Open();
                        using (FbDataReader fbDataReader = fbCommand.ExecuteReader())
                        {
                            bool flag = fbDataReader != null;
                            if (flag && fbDataReader.Read())
                            {
                                result = new byte[fbDataReader.GetBytes(0, 0L, null, 0, 2147483647)];
                                if (result.Length > 0)
                                {
                                    var rez = fbDataReader[0].ToString();
                                    fbDataReader.GetBytes(0, 0L, result, 0, result.Length);
                                    return result;
                                }
                            }
                        }
                    }
                }
            result = null;
            return result;
        }
        public string getSetting(string ConnBDD, int id)
        {
            
            if (id != -1)
                using (FbConnection fbConnection = new FbConnection(ConnBDD))
                {
                    using (FbCommand fbCommand = new FbCommand("SELECT spg4 from spg where spg1=@ID", fbConnection))
                    {
                        fbCommand.Parameters.AddWithValue("@ID", id);
                        fbConnection.Open();
                        using (FbDataReader fbDataReader = fbCommand.ExecuteReader())
                        {
                            bool flag = fbDataReader != null;
                            if (flag && fbDataReader.Read())
                            {
                                return fbDataReader[0].ToString();
                            }                            
                        }
                    }
                }
            return "";
        }
        public bool deleteSetting(string connBDD,int id)
        {
            using (FbCommand fbCommand = new FbCommand("delete from spg where spg1 = @ID"))
            {                
                fbCommand.Parameters.Add("@ID", id);
                return MKP.DBImplementation.WorkWhithDB.ExecutNonQuery(fbCommand, connBDD, true);                 
            }
        }
        public List<Setting> LoadSettingList(string connBDD,int Module,int TabPageNum,int i1)
        {
            List<Setting> rez = new List<Setting>();
            try
            {
                using (FbConnection fbConnection = new FbConnection(connBDD))
                {
                    fbConnection.Open();
                    using (FbCommand fbCommand = new FbCommand("select spg1,spg5,spg6 from spg where spg2=@Module and spg3=@tbPage and (spg6 =@i1 or spg6=0)", fbConnection))
                    {
                        fbCommand.Parameters.Add("@Module", Module);
                        fbCommand.Parameters.Add("@tbPage", TabPageNum);
                        fbCommand.Parameters.Add("@i1", i1);
                        using (FbDataReader fbDataReader = fbCommand.ExecuteReader())
                        {
                            while (fbDataReader.Read())
                            {
                                rez.Add(new Setting
                                {
                                    ID = Convert.ToInt32(fbDataReader[0]),
                                    Name = ((fbDataReader[2].ToString() != "0") ? "! " : "") + fbDataReader[1].ToString()
                                });
                            }
                        }
                    }
                }                
            }
            catch (FbException)
            {
                MessageBox.Show("Возникла ошибка при загрузке настроек таблицы из базы данных. Обратитесь к разработчикам.");
            }
            return rez;
        }

        public void updateName(string connBDD, string name, int iD)
        {
            using (FbCommand fbCommand = new FbCommand("update spg set spg5 = @Name where spg1=@ID "))
            {
                fbCommand.Parameters.Add("@Name", name);
                fbCommand.Parameters.Add("@ID", iD);
                MKP.DBImplementation.WorkWhithDB.ExecutNonQuery(fbCommand, connBDD, true);
            }
        }
        public Setting insertNewSetting(string connBDD, int module, int tabPageNum, int i1)
        {
            Setting rez = null;
            int num = -1;
            using (FbConnection fbConnection = new FbConnection(connBDD))
            {
                fbConnection.Open();
                using (FbCommand fbCommand = new FbCommand("SELECT NEXT VALUE FOR GEN_SPG_ID FROM RDB$DATABASE", fbConnection))
                {
                    using (FbDataReader fbDataReader = fbCommand.ExecuteReader())
                    {
                        bool flag = fbDataReader.Read();
                        if (flag)
                        {
                            int.TryParse(fbDataReader[0].ToString(), out num);
                        }
                    }
                }
            }
            bool flag2 = num != -1;
            if (flag2)
            {
                using (FbCommand fbCommand2 = new FbCommand("insert into spg (spg1,spg2,spg3,spg5,spg6) VALUES(@ID,@Module,@tbPage,@ID,@i1)"))
                {
                    fbCommand2.Parameters.Add("@Module", module);
                    fbCommand2.Parameters.Add("@tbPage", tabPageNum);
                    fbCommand2.Parameters.Add("@ID", num);
                    fbCommand2.Parameters.Add("@i1", i1);
                    bool flag3 = !MKP.DBImplementation.WorkWhithDB.ExecutNonQuery(fbCommand2, connBDD, true);
                    if (flag3)
                    {
                        MessageBox.Show("Возникла ошибка. Обратитесь к разработчикам.");
                    }
                    else
                    {
                        rez = new Setting
                        {
                            ID = num,
                            Name = num.ToString()
                        };
                    }
                }
            }
            else
            {
                MessageBox.Show("Возникла ошибка. Обратитесь к разработчикам.");
            }
            if (rez == null)
                throw new ArgumentNullException();
            return rez;
        }
        public string GetXMLSetting(string connBDD,int ID)
        {
            return getSetting(connBDD, ID);
        }
        string GetString(byte[] arr)
        {
            string rez = "";
            using (var memoryStream = new MemoryStream())
            {
                memoryStream.Write(arr, 0, arr.Length);
                memoryStream.Seek(0, SeekOrigin.Begin);
                memoryStream.Position = 0;
                var sr = new StreamReader(memoryStream);
                rez = sr.ReadToEnd();
            }
            return rez;
        }
    }
}
