﻿using FirebirdSql.Data.FirebirdClient;
using System;

namespace MKP
{
    public class UpdateBDD
    {
        private string connString
        {
            get;
            set;
        }

        public UpdateBDD(string ConnString)
        {
            this.connString = ConnString;
            this.SPG();
        }

        private void SPG()
        {
            try
            {
                using (FbConnection fbConnection = new FbConnection(this.connString))
                {
                    fbConnection.Open();
                    using (FbCommand fbCommand = new FbCommand("select first 1 spg1 from spg", fbConnection))
                    {
                        using (FbDataReader fbDataReader = fbCommand.ExecuteReader())
                        {
                            bool flag = fbDataReader.Read();
                            if (flag)
                            {
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                string cmdText = @"CREATE TABLE SPG ( 
                                    SPG1  INTEGER NOT NULL,
                                    SPG2  SMALLINT NOT NULL,
                                    SPG3  SMALLINT NOT NULL,
                                    SPG4  BLOB SUB_TYPE 1 SEGMENT SIZE 80,
                                    SPG5  VARCHAR(25) NOT NULL,
                                    SPG6  INTEGER DEFAULT 0 NOT NULL);";
                FbCommand fbCommand2 = new FbCommand(cmdText);
                DBImplementation.WorkWhithDB.ExecutNonQuery(fbCommand2, connString, true);
                cmdText = "ALTER TABLE SPG ADD CONSTRAINT PK_SPG PRIMARY KEY (SPG1, SPG2, SPG3);";
                fbCommand2 = new FbCommand(cmdText);
                DBImplementation.WorkWhithDB.ExecutNonQuery(fbCommand2, connString, true);
                cmdText = "CREATE GENERATOR GEN_SPG_ID;";
                fbCommand2 = new FbCommand(cmdText);
                DBImplementation.WorkWhithDB.ExecutNonQuery(fbCommand2, connString, true);
                cmdText = "SET GENERATOR GEN_SPG_ID TO 11;";
                fbCommand2 = new FbCommand(cmdText);
                DBImplementation.WorkWhithDB.ExecutNonQuery(fbCommand2, connString,true);
            }
        }
    }
}
