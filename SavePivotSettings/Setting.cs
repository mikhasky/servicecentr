﻿namespace SavePivotSettings
{
    public class Setting
    {
        public int ID
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public override string ToString()
        {
            return this.Name;
        }
    }
}
