﻿using DevExpress.XtraPivotGrid;
using FirebirdSql.Data.FirebirdClient;
using MKP;
using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace SavePivotSettings
{
    public class ComboBoxPivotSetings : UserControl
    {
        public enum DisplayStyle
        {
            CbLeftBtnLeft,
            CbLeftBtnRight,
            CbFillBtnRight,
            CbUpBtnDown,
            CbDownBtnUp
        }
        public DisplayStyle _DisplayStyle
        {
            get { return _displayStyle; }
            set { _displayStyle = value; SetStyle(); }
        }

        private DisplayStyle _displayStyle = DisplayStyle.CbLeftBtnLeft;
        private void SetStyle()
        {
            this.Controls.Clear();
            this.Controls.Add(this.btnSettings);
            this.Controls.Add(this.cbSettings);
            switch (_DisplayStyle)
            {
                case DisplayStyle.CbLeftBtnLeft:
                    this.Size = new System.Drawing.Size(265, 21);
                    this.cbSettings.Dock = System.Windows.Forms.DockStyle.Left;
                    this.btnSettings.Dock = System.Windows.Forms.DockStyle.Left;
                    break;
                case DisplayStyle.CbLeftBtnRight:
                    this.Size = new System.Drawing.Size(265, 21);
                    this.cbSettings.Dock = System.Windows.Forms.DockStyle.Left;
                    this.btnSettings.Dock = System.Windows.Forms.DockStyle.Right;
                    break;
                case DisplayStyle.CbFillBtnRight:
                    this.Size = new System.Drawing.Size(265, 21);
                    this.Controls.Add(this.cbSettings);
                    this.Controls.Add(this.btnSettings);
                    this.cbSettings.Dock = System.Windows.Forms.DockStyle.Fill;
                    this.btnSettings.Dock = System.Windows.Forms.DockStyle.Right;
                    break;
                case DisplayStyle.CbUpBtnDown:
                    this.Size = new System.Drawing.Size(265, 42);
                    this.cbSettings.Dock = System.Windows.Forms.DockStyle.Top;
                    this.btnSettings.Dock = System.Windows.Forms.DockStyle.Bottom;
                    break;
                case DisplayStyle.CbDownBtnUp:
                    this.Size = new System.Drawing.Size(265, 42);
                    this.cbSettings.Dock = System.Windows.Forms.DockStyle.Bottom;
                    this.btnSettings.Dock = System.Windows.Forms.DockStyle.Top;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
        Work work = new Work();
        private ContextMenuStrip _contextMenuStrip;

        private PivotGridControl PivotGrid;

        private string ConnBDD;

        private string ConnBD;

        private int Module;

        private int TabPageNum;

        private int cbIndex = -1;

        private int i1;

        private bool admin;

        private IContainer components = null;

        private ComboBox cbSettings;

        private Button btnSettings;

        public ComboBoxPivotSetings()
        {
            InitializeComponent();
        }

        public void Initialize(PivotGridControl pivotGrid, string connectionStringBDD, string connectionStringBD, int module, int tabPageNum, int i1)
        {
            PivotGrid = pivotGrid;
            ConnBDD = connectionStringBDD;
            ConnBD = connectionStringBD;
            Module = module;
            TabPageNum = tabPageNum;
            this.i1 = i1;
            GetUser();
            UpdateBDD updateBDD = new UpdateBDD(connectionStringBDD);
            LoadSettings();           
            object selectedItem = cbSettings.SelectedItem;
        }

        private void GetUser()
        {
            if (i1 == 0)
            {
                admin = true;
                return;
            }
            using (FbConnection fbConnection = new FbConnection(ConnBD))
            {
                fbConnection.Open();
                using (FbCommand fbCommand = new FbCommand(@"select gp.gp3 from i 
                    inner join gpi on (i.i1 = gpi.gpi1)           
                    inner join gp on (gpi.gpi2 = gp.gp1)     
                    where i1 = @i1 and gp3 = 1", fbConnection))
                {
                    fbCommand.Parameters.Add("@i1", i1);
                    using (FbDataReader fbDataReader = fbCommand.ExecuteReader())
                    {
                        bool flag = fbDataReader != null;
                        if (flag)
                        {
                            if (fbDataReader.Read())
                            {
                                admin = (fbDataReader[0] != null);
                                i1 = 0;                                
                            }
                                
                            else
                                admin = false;
                        }
                    }
                }
            }
        }

        private void LoadSettings()
        {
            cbSettings.Items.AddRange(work.LoadSettingList(ConnBDD, Module, TabPageNum, i1).ToArray());            
            cbSettings.SelectedIndex = ((cbSettings.Items.Count > 0) ? 0 : -1);          
        }

        private void btnSettings_Click(object sender, EventArgs e)
        {
            AddStripMenu((Button)sender);
        }

        public void AddStripMenu(Button btn)
        {
            _contextMenuStrip = new ContextMenuStrip();
            _contextMenuStrip.ItemClicked += new ToolStripItemClickedEventHandler(contextMenuStrip_ItemClicked);
            AddItems();
            _contextMenuStrip.Show(btn, 110, 0);
        }

        private void AddItems()
        {
            _contextMenuStrip.Items.Clear();
            _contextMenuStrip.Items.Add("Сохранить шаблон");
            _contextMenuStrip.Items.Add("Загрузить шаблон");
            _contextMenuStrip.Items.Add("Удалить шаблон");
        }

        private void contextMenuStrip_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            if (cbSettings.Items.Count == 0)
                return;
            switch (e.ClickedItem.Text)
            {
                case "Сохранить шаблон":
                    updateSetting();
                    break;
                case "Загрузить шаблон":
                    loadSetting();
                    break;
                case "Удалить шаблон":
                    deleteSetting();
                    break;

            }          
        }
        void deleteSetting()
        {
            work.deleteSetting(ConnBDD, (cbSettings.Items[cbIndex] as Setting).ID);
            cbSettings.Items.RemoveAt(cbIndex);
            if(cbSettings.Items.Count > 0)
            {
                cbSettings.SelectedIndex = 0;
                loadSetting();
            }
        }
        private void loadSetting()
        {
            byte[] settingFromDB = work.getSettingFromDB(ConnBDD, (cbSettings.Items[cbIndex] as Setting).ID);
            bool flag = settingFromDB != null&& settingFromDB.Length>0;
            if (flag)
            {
                using (MemoryStream memoryStream = new MemoryStream())
                {                   
                    memoryStream.Write(settingFromDB, 0, settingFromDB.Length);                    
                    
                    memoryStream.Seek(0, SeekOrigin.Begin);
                    PivotGrid.RestoreLayoutFromStream(memoryStream);
                }
            }
            else
            {
                MessageBox.Show("Настройка пуста.");
            }
        }

        private void cbSettings_KeyUp(object sender, KeyEventArgs e)
        {
            ComboBox comboBox = (ComboBox)sender;
            bool flag = comboBox.Text.Length > 25;
            if (flag)
            {
                comboBox.Text.Remove(24);
                MessageBox.Show("Максимальная длинна названия шаблона 25 символов");
            }
            bool flag2 = e.KeyCode == Keys.Return;
            if (flag2)
            {
                cbSettings.Items[cbIndex] = new Setting
                {
                    ID = (cbSettings.Items[cbIndex] as Setting).ID,
                    Name = comboBox.Text
                };
                updateName();
            }
            else
            {
                bool flag3 = e.KeyCode == Keys.Insert;
                if (flag3)
                {                   
                    cbSettings.Items.Add(work.insertNewSetting(ConnBDD, Module, TabPageNum, i1));
                    cbSettings.SelectedIndex = cbSettings.Items.Count - 1;
                    comboBox.Text = (cbSettings.Items[cbIndex] as Setting).Name;
                }
            }
        }

        private void updateSetting()
        {
            byte[] setting;
            using (MemoryStream memoryStream = new MemoryStream())
            {
                PivotGrid.SaveLayoutToStream(memoryStream);
                long num = memoryStream.Seek(0L, SeekOrigin.Begin);
                using (BinaryReader binaryReader = new BinaryReader(memoryStream))
                {
                    setting = binaryReader.ReadBytes((int)memoryStream.Length);
                }
            }
            work.updateSetting(setting, (cbSettings.Items[cbIndex] as Setting).ID,ConnBDD);
        }

        private void updateName()
        {
            work.updateName(ConnBDD, (cbSettings.Items[cbIndex] as Setting).Name, (cbSettings.Items[cbIndex] as Setting).ID);            
        }
        
        private void cbSettings_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbIndex = cbSettings.SelectedIndex;
        }

        protected override void Dispose(bool disposing)
        {
            bool flag = disposing && components != null;
            if (flag)
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            cbSettings = new ComboBox();
            btnSettings = new Button();
            SuspendLayout();
            cbSettings.Anchor = (AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right);
            cbSettings.FormattingEnabled = true;
            cbSettings.Location = new Point(0, 0);
            cbSettings.Name = "cbSettings";
            cbSettings.Size = new Size(149, 21);
            cbSettings.TabIndex = 0;
            cbSettings.SelectedIndexChanged += new EventHandler(cbSettings_SelectedIndexChanged);
            cbSettings.KeyUp += new KeyEventHandler(cbSettings_KeyUp);
            btnSettings.Anchor = (AnchorStyles.Top | AnchorStyles.Right);
            btnSettings.ForeColor = SystemColors.HotTrack;
            btnSettings.Location = new Point(155, 0);
            btnSettings.Name = "btnSettings";
            btnSettings.Size = new Size(110, 21);
            btnSettings.TabIndex = 1;
            btnSettings.Text = "Шаблон таблицы";
            btnSettings.UseVisualStyleBackColor = true;
            btnSettings.Click += new EventHandler(btnSettings_Click);
            AutoScaleDimensions = new SizeF(6f, 13f);
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(btnSettings);
            Controls.Add(cbSettings);
            Name = "ComboBoxPivotSetings";
            Size = new Size(265, 21);
            ResumeLayout(false);
        }
    }
}