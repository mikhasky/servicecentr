﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using FirebirdSql.Data.FirebirdClient;
using FirebirdSql.Data.Isql;

/*MKP dependies */
using dblib; // bin\dblib.dll


// skey 5635655899633252
namespace DBCONFIGURATORold
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }


        public string connectionBD = "";
        public string connectionBDD = "";
        public string connectionCont = "";

        private void Form1_Load(object sender, EventArgs e)
        {
            string inputBD = "";
            string inputBDD = "";
            try
            {
                inputBD = Encryption.DecryptToString(@"bin\dbset.dll", "5635655899633252");
                inputBDD = Encryption.DecryptToString(@"bin\dbbset.dll", "5635655899633252");

                if(!string.IsNullOrEmpty(inputBD))
                {
                    tbAsuIp.Text = inputBD.Substring(inputBD.IndexOf("DataSource = ") + 13, inputBD.IndexOf("Port = ") - inputBD.IndexOf("DataSource = ") - 14);
                    tbAsuPath.Text = inputBD.Substring(inputBD.IndexOf("Database = ") + 11, inputBD.IndexOf("DataSource = ") - inputBD.IndexOf("Database = ") - 12);
                    tbAsuPathPort.Text = inputBD.Substring(inputBD.IndexOf("Port = ") + 7, inputBD.IndexOf("Dialect = 3") - inputBD.IndexOf("Port = ") - 8);

                    CheckConnectionBD();
                }

                if(!string.IsNullOrEmpty(inputBDD))
                {
                    tbBddIp.Text = inputBDD.Substring(inputBDD.IndexOf("DataSource = ") + 13, inputBDD.IndexOf("Port = ") - inputBDD.IndexOf("DataSource = ") - 14);
                    tbBddPath.Text = inputBDD.Substring(inputBDD.IndexOf("Database = ") + 11, inputBDD.IndexOf("DataSource = ") - inputBDD.IndexOf("Database = ") - 12);
                    tbBddPort.Text = inputBDD.Substring(inputBDD.IndexOf("Port = ") + 7, inputBDD.IndexOf("Dialect = 3") - inputBDD.IndexOf("Port = ") - 8);

                    CheckConnectionBDD();
                }
            }
            catch
            {

            }
        }

        private void CheckConnectionBD()
        {
            connectionBD =
                "User = MKP;" +
                "Password = 17051945;" +
                "Database = " + @tbAsuPath.Text + ';' +
                "DataSource = " + tbAsuIp.Text + ';' +
                "Port = " + tbAsuPathPort.Text + ';' +
                "Dialect = 3;" +
                "Charset = WIN1251;" +
                "Role =;" +
                "Connection lifetime = 15;" +
                "Pooling = true;" +
                "MinPoolSize = 0;" +
                "MaxPoolSize = 50;" +
                "Packet Size = 8192;" +
                "ServerType = 0";

            try
            {
                databaseConnection ADAD = new databaseConnection(connectionBD);
                ADAD.OpenConnection();

                if (ADAD.IsWorking())
                {
                    lbBDValid.BackColor = Color.PaleGreen;
                    lbBDValid.Text = "Подключено";
                    ADAD.CloseConnection();
                }
                else
                {
                    lbBDValid.BackColor = Color.Tomato;
                    lbBDValid.Text = "Не подключено";
                }
            }
            catch(FbException)
            {
                lbBDValid.BackColor = Color.Tomato;
                lbBDValid.Text = "Не подключено";
                //MessageBox.Show(e + " " + connectionBD);
            }
        }

        private void CheckConnectionBDD()
        {
            connectionBDD =
                "User = MKP;" +
                "Password = 17051945;" +
                "Database = " + @tbBddPath.Text + ';' +
                "DataSource = " + tbBddIp.Text + ';' +
                "Port = " + tbBddPort.Text + ';' +
                "Dialect = 3;" +
                "Charset = WIN1251;" +
                "Role =;" +
                "Connection lifetime = 15;" +
                "Pooling = true;" +
                "MinPoolSize = 0;" +
                "MaxPoolSize = 50;" +
                "Packet Size = 8192;" +
                "ServerType = 0";

            try
            {
                databaseConnection BDD = new databaseConnection(connectionBDD);
                BDD.OpenConnection();

                if (BDD.IsWorking())
                {
                    lbBddValid.BackColor = Color.PaleGreen;
                    lbBddValid.Text = "Подключено";
                    BDD.CloseConnection();
                }
                else
                {
                    lbBddValid.BackColor = Color.Tomato;
                    lbBddValid.Text = "Не подключено";
                }
            }
            catch
            {
                lbBddValid.BackColor = Color.Tomato;
                lbBddValid.Text = "Не подключено";
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            CheckConnectionBD();
            CheckConnectionBDD();
            Encryption.EncryptString(connectionBD, @"bin\dbset.dll", "5635655899633252");
            Encryption.EncryptString(connectionBDD, @"bin\dbbset.dll", "5635655899633252");
            DialogResult result = MessageBox.Show("Успешно! Скопируйте содержимое папки bin,"
                                + " в папку bin Единого центра запуска",
            "Готово",
            MessageBoxButtons.OK,
            MessageBoxIcon.Information,
            MessageBoxDefaultButton.Button1);
            switch (result)
            {
                case DialogResult.OK:
                    Application.Exit();
                    break;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bTestConnectionBD_Click(object sender, EventArgs e)
        {
            CheckConnectionBD();
        }

        private void bTestConnectionBDD_Click(object sender, EventArgs e)
        {
            CheckConnectionBDD();
        }

        private void btViewFolderBD_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            if (!string.IsNullOrEmpty(tbAsuPath.Text))
            {
                openFileDialog1.FileName = tbAsuPath.Text;
            }
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                System.IO.FileInfo fInfo = new System.IO.FileInfo(openFileDialog1.FileName);
                tbAsuPath.Text = fInfo.FullName;
                if (tbAsuPath.TextLength > 150)
                {
                    return;
                }
            }
        }

        private void btViewFolderBDD_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            if (!string.IsNullOrEmpty(tbBddPath.Text))
            {
                openFileDialog1.FileName = tbBddPath.Text;
            }
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                System.IO.FileInfo fInfo = new System.IO.FileInfo(openFileDialog1.FileName);
                tbBddPath.Text = fInfo.FullName;
                if (tbBddPath.TextLength > 150)
                {
                    return;
                }
            }
        }
    }
}
