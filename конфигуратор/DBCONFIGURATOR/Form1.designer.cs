﻿namespace DBCONFIGURATORold
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tbAsuPath = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tbAsuPathPort = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbBddPath = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbBddPort = new System.Windows.Forms.TextBox();
            this.tbAsuIp = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.tbBddIp = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.lbBDValid = new System.Windows.Forms.Label();
            this.lbBddValid = new System.Windows.Forms.Label();
            this.btDone = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btViewFolderBDD = new System.Windows.Forms.Button();
            this.btViewFolderBD = new System.Windows.Forms.Button();
            this.bTestConnectionBDD = new System.Windows.Forms.Button();
            this.bTestConnectionBD = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbAsuPath
            // 
            this.tbAsuPath.Location = new System.Drawing.Point(177, 37);
            this.tbAsuPath.Name = "tbAsuPath";
            this.tbAsuPath.Size = new System.Drawing.Size(189, 20);
            this.tbAsuPath.TabIndex = 1;
            this.tbAsuPath.Text = "C:\\BD";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(202, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Укажите путь к базе данных АСУ (BD)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(425, 40);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Порт";
            // 
            // tbAsuPathPort
            // 
            this.tbAsuPathPort.Location = new System.Drawing.Point(463, 37);
            this.tbAsuPathPort.Name = "tbAsuPathPort";
            this.tbAsuPathPort.Size = new System.Drawing.Size(38, 20);
            this.tbAsuPathPort.TabIndex = 2;
            this.tbAsuPathPort.Text = "3050";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(20, 106);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(210, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Укажите путь к базе данных АСУ (BDD)";
            // 
            // tbBddPath
            // 
            this.tbBddPath.Location = new System.Drawing.Point(177, 122);
            this.tbBddPath.Name = "tbBddPath";
            this.tbBddPath.Size = new System.Drawing.Size(189, 20);
            this.tbBddPath.TabIndex = 4;
            this.tbBddPath.Text = "C:\\BDD";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(425, 125);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(32, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Порт";
            // 
            // tbBddPort
            // 
            this.tbBddPort.Location = new System.Drawing.Point(463, 122);
            this.tbBddPort.Name = "tbBddPort";
            this.tbBddPort.Size = new System.Drawing.Size(38, 20);
            this.tbBddPort.TabIndex = 5;
            this.tbBddPort.Text = "3050";
            // 
            // tbAsuIp
            // 
            this.tbAsuIp.Location = new System.Drawing.Point(43, 37);
            this.tbAsuIp.Name = "tbAsuIp";
            this.tbAsuIp.Size = new System.Drawing.Size(91, 20);
            this.tbAsuIp.TabIndex = 0;
            this.tbAsuIp.Text = "127.0.0.1";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(20, 40);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(17, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "IP";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(140, 40);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(31, 13);
            this.label8.TabIndex = 16;
            this.label8.Text = "Путь";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(20, 125);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(17, 13);
            this.label10.TabIndex = 15;
            this.label10.Text = "IP";
            // 
            // tbBddIp
            // 
            this.tbBddIp.Location = new System.Drawing.Point(43, 122);
            this.tbBddIp.Name = "tbBddIp";
            this.tbBddIp.Size = new System.Drawing.Size(91, 20);
            this.tbBddIp.TabIndex = 3;
            this.tbBddIp.Text = "127.0.0.1";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(140, 125);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(31, 13);
            this.label11.TabIndex = 16;
            this.label11.Text = "Путь";
            // 
            // lbBDValid
            // 
            this.lbBDValid.AutoSize = true;
            this.lbBDValid.BackColor = System.Drawing.Color.Tomato;
            this.lbBDValid.Location = new System.Drawing.Point(239, 21);
            this.lbBDValid.Name = "lbBDValid";
            this.lbBDValid.Size = new System.Drawing.Size(85, 13);
            this.lbBDValid.TabIndex = 17;
            this.lbBDValid.Text = "Не подключено";
            // 
            // lbBddValid
            // 
            this.lbBddValid.AutoSize = true;
            this.lbBddValid.BackColor = System.Drawing.Color.Tomato;
            this.lbBddValid.Location = new System.Drawing.Point(239, 106);
            this.lbBddValid.Name = "lbBddValid";
            this.lbBddValid.Size = new System.Drawing.Size(85, 13);
            this.lbBddValid.TabIndex = 17;
            this.lbBddValid.Text = "Не подключено";
            // 
            // btDone
            // 
            this.btDone.Location = new System.Drawing.Point(16, 265);
            this.btDone.Name = "btDone";
            this.btDone.Size = new System.Drawing.Size(178, 23);
            this.btDone.TabIndex = 9;
            this.btDone.Text = "Создать файл";
            this.btDone.UseVisualStyleBackColor = true;
            this.btDone.Click += new System.EventHandler(this.button2_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Menu;
            this.panel1.Controls.Add(this.btViewFolderBDD);
            this.panel1.Controls.Add(this.btViewFolderBD);
            this.panel1.Controls.Add(this.bTestConnectionBDD);
            this.panel1.Controls.Add(this.bTestConnectionBD);
            this.panel1.Controls.Add(this.lbBddValid);
            this.panel1.Controls.Add(this.lbBDValid);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.tbBddIp);
            this.panel1.Controls.Add(this.tbAsuIp);
            this.panel1.Controls.Add(this.tbBddPort);
            this.panel1.Controls.Add(this.tbBddPath);
            this.panel1.Controls.Add(this.tbAsuPathPort);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.tbAsuPath);
            this.panel1.Location = new System.Drawing.Point(-3, 75);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(675, 175);
            this.panel1.TabIndex = 18;
            // 
            // btViewFolderBDD
            // 
            this.btViewFolderBDD.Location = new System.Drawing.Point(372, 120);
            this.btViewFolderBDD.Name = "btViewFolderBDD";
            this.btViewFolderBDD.Size = new System.Drawing.Size(47, 23);
            this.btViewFolderBDD.TabIndex = 19;
            this.btViewFolderBDD.Text = "Обзор";
            this.btViewFolderBDD.UseVisualStyleBackColor = true;
            this.btViewFolderBDD.Click += new System.EventHandler(this.btViewFolderBDD_Click);
            // 
            // btViewFolderBD
            // 
            this.btViewFolderBD.Location = new System.Drawing.Point(372, 35);
            this.btViewFolderBD.Name = "btViewFolderBD";
            this.btViewFolderBD.Size = new System.Drawing.Size(47, 23);
            this.btViewFolderBD.TabIndex = 19;
            this.btViewFolderBD.Text = "Обзор";
            this.btViewFolderBD.UseVisualStyleBackColor = true;
            this.btViewFolderBD.Click += new System.EventHandler(this.btViewFolderBD_Click);
            // 
            // bTestConnectionBDD
            // 
            this.bTestConnectionBDD.Location = new System.Drawing.Point(507, 120);
            this.bTestConnectionBDD.Name = "bTestConnectionBDD";
            this.bTestConnectionBDD.Size = new System.Drawing.Size(155, 23);
            this.bTestConnectionBDD.TabIndex = 18;
            this.bTestConnectionBDD.Text = "Проверить подключение";
            this.bTestConnectionBDD.UseVisualStyleBackColor = true;
            this.bTestConnectionBDD.Click += new System.EventHandler(this.bTestConnectionBDD_Click);
            // 
            // bTestConnectionBD
            // 
            this.bTestConnectionBD.Location = new System.Drawing.Point(507, 35);
            this.bTestConnectionBD.Name = "bTestConnectionBD";
            this.bTestConnectionBD.Size = new System.Drawing.Size(155, 23);
            this.bTestConnectionBD.TabIndex = 18;
            this.bTestConnectionBD.Text = "Проверить подключение";
            this.bTestConnectionBD.UseVisualStyleBackColor = true;
            this.bTestConnectionBD.Click += new System.EventHandler(this.bTestConnectionBD_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(12, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(213, 24);
            this.label2.TabIndex = 19;
            this.label2.Text = "Настройка баз данных";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(215, 265);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(178, 23);
            this.button1.TabIndex = 20;
            this.button1.Text = "Закрыть";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(671, 331);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btDone);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Text = "Конфигуратор БД";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbAsuPath;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbAsuPathPort;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbBddPath;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbBddPort;
        private System.Windows.Forms.TextBox tbAsuIp;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbBddIp;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lbBDValid;
        private System.Windows.Forms.Label lbBddValid;
        private System.Windows.Forms.Button btDone;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button bTestConnectionBD;
        private System.Windows.Forms.Button bTestConnectionBDD;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btViewFolderBDD;
        private System.Windows.Forms.Button btViewFolderBD;
    }
}

