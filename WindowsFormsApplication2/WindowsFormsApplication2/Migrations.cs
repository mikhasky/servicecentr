﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LinqToDB;
using System.Runtime.Serialization;
using System.Data.OleDb;
using MKP.PrintDevForms.Options.Json;

namespace WindowsFormsApplication2
{
    public partial class Migrations : UserControl
    {
        public Migrations()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog()
            {
                Multiselect = false,
                Filter = "Excel files (*.xls)|*.xls"
            };

            if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {                
                textBox1.Text = ofd.FileName;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textBox1.Text))
                return;
            migrate();
        }
        public void migrate()
        {
            progressBar1.Value = 0;
            string connstr = $@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source={textBox1.Text};Persist Security Info=True;Jet OLEDB:Database Password=+/-eNg&0705";
            var Brnd = GetResultFromQuerry("select * from Brnd", 2);
            using (var db = new MKP.DataModels.BDETALONDB(Connection.conbd))
            {
                db.BeginTransaction();
                foreach (var item in Brnd)
                {
                    var brnd = new MKP.DataModels.BRAND()
                    {
                        BRAND1 = Convert.ToInt32(item[0]),
                        BRAND2 = item[1]
                    };
                    db.Insert(brnd);
                }
                db.CommitTransaction();
            }
            progressBar1.Value = 5;
            this.Refresh();
            var Clnt = GetResultFromQuerry("select * from Clnt", 11);
            using (var db = new MKP.DataModels.BDETALONDB(Connection.conbd))
            {
                db.BeginTransaction();
                foreach (var item in Clnt)
                {
                    var clnt = new MKP.DataModels.CLIENT()
                    {
                        CLIENT1 = Convert.ToInt32(item[0]),
                        CLIENT2 = item[2],
                        CLIENT3 = item[3],
                        CLIENT5 = item[5],
                        CLIENT6 = item[6],
                        CLIENT7 = item[7],
                        CLIENT8 = item[10]
                    };
                    db.Insert(Clnt);
                }
                db.CommitTransaction();
            }
            progressBar1.Value = 20;
            this.Refresh();
            var Cmpl = GetResultFromQuerry("select * from Cmpl", 2);
            using (var db = new MKP.DataModels.BDETALONDB(Connection.conbd))
            {
                db.BeginTransaction();
                foreach (var item in Cmpl)
                {
                    var cmpl = new MKP.DataModels.COMPLECT()
                    {
                        COMPLECT1 = Convert.ToInt32(item[0]),
                        COMPLECT2 = item[1]
                    };
                    db.Insert(Cmpl);
                }
                db.CommitTransaction();
            }
            progressBar1.Value = 30;
            this.Refresh();
            var Dmg = GetResultFromQuerry("select * from Dmg", 2);
            using (var db = new MKP.DataModels.BDETALONDB(Connection.conbd))
            {
                db.BeginTransaction();
                foreach (var item in Dmg)
                {
                    var dmg = new MKP.DataModels.DAMAG()
                    {
                        DAMAG1 = Convert.ToInt32(item[0]),
                        DAMAG2 = item[1]
                    };
                    db.Insert(Dmg);
                }
                db.CommitTransaction();
            }
            progressBar1.Value = 40;
            this.Refresh();
            var Dvc = GetResultFromQuerry("select * from Dvc", 25);
            using (var db = new MKP.DataModels.BDETALONDB(Connection.conbd))
            {
                db.BeginTransaction();
                foreach (var item in Dvc)
                {
                    CurrentSettings set = new CurrentSettings();
                    set.lst = new List<int>();
                    foreach (var _dmg in Dmg)
                    {
                        if (item[5].Contains(_dmg[1]))
                            set.lst.Add(Convert.ToInt32(_dmg[0]));
                    }
                    var obj = Encoding.UTF8.GetBytes(Json.Serialize<CurrentSettings>(set));

                    CurrentSettings set_cmpl = new CurrentSettings();
                    set_cmpl.lst = new List<int>();
                    foreach (var _cmpl in Cmpl)
                    {
                        if (item[12].Contains(_cmpl[1]))
                            set_cmpl.lst.Add(Convert.ToInt32(_cmpl[0]));
                    }
                    var obj_cmpl = Encoding.UTF8.GetBytes(Json.Serialize<CurrentSettings>(set_cmpl));

                    var dvc = new MKP.DataModels.DEVICE()
                    {
                        DEVICE1 = Convert.ToInt32(item[0]),
                        DEVICE2 = Convert.ToDateTime(item[1]),
                        DEVICE3 = item[2],
                        DEVICE4 = item[3],
                        DEVICE5 = item[4],
                        DEVICE6 = obj,
                        DEVICE7 = item[6],
                        DEVICE8 = obj_cmpl,
                        

                    };
                    db.Insert(Dvc);
                }
                db.CommitTransaction();
            }
            progressBar1.Value = 50;
            this.Refresh();
            var Lbl = GetResultFromQuerry("select * from Lbl", 2);
            using (var db = new MKP.DataModels.BDETALONDB(Connection.conbd))
            {
                db.BeginTransaction();
                foreach (var item in Lbl)
                {
                    var lbl = new MKP.DataModels.LABEL()
                    {
                        LABEL1 = Convert.ToInt32(item[0]),
                        LABEL2 = item[1]
                    };
                    db.Insert(Lbl);
                }
                db.CommitTransaction();
            }
            progressBar1.Value = 60;
            this.Refresh();
            var DvcWrk = GetResultFromQuerry("select * from DvcWrk", 9);
            using (var db = new MKP.DataModels.BDETALONDB(Connection.conbd))
            {
                db.BeginTransaction();
                foreach (var item in DvcWrk)
                {
                    var dvcwrk = new MKP.DataModels.WWORK()
                    {
                        WWORK1 = Convert.ToInt32(item[0]),
                        WWORK2 = Convert.ToInt32(item[1]),
                        WWORK3 = item[3].GetDouble(),
                        WWORK4 = Convert.ToInt32(item[4]),
                        WWORK5 = Convert.ToInt32(item[2]),
                        WWORK6 = item[6],
                        WWORK7 = Convert.ToDateTime(item[7])
                    };
                    db.Insert(DvcWrk);
                }
                db.CommitTransaction();
            }
            progressBar1.Value = 70;
            this.Refresh();
            var Wrk = GetResultFromQuerry("select * from Wrk", 5);
            using (var db = new MKP.DataModels.BDETALONDB(Connection.conbd))
            {
                db.BeginTransaction();
                foreach (var item in Wrk)
                {
                    var wrk = new MKP.DataModels.SPRAV_WORK()
                    {
                        SPRAV_WORK1 = Convert.ToInt32(item[0]),
                        SPRAV_WORK2 = item[1],
                        SPRAV_WORK3 = Convert.ToInt32(item[3]),
                        SPRAV_WORK4 = Convert.ToInt16(item[4])
                    };
                    db.Insert(Wrk);
                }
                db.CommitTransaction();
            }
            progressBar1.Value = 80;
            this.Refresh();
            var DvcTp = GetResultFromQuerry("select * from DvcTp", 2);
            using (var db = new MKP.DataModels.BDETALONDB(Connection.conbd))
            {
                db.BeginTransaction();
                foreach (var item in DvcTp)
                {
                    var dvctp = new MKP.DataModels.DEVICE_TP()
                    {
                        DEVICE_TP1 = Convert.ToInt32(item[0]),
                        DEVICE_TP2 = item[1]
                    };
                    db.Insert(DvcTp);
                }
                db.CommitTransaction();
            }
            progressBar1.Value = 90;
            this.Refresh();
            var DvcStsHst = GetResultFromQuerry("select * from DvcStsHst", 6);
            using (var db = new MKP.DataModels.BDETALONDB(Connection.conbd))
            {
                db.BeginTransaction();
                foreach (var item in DvcStsHst)
                {
                    var dvcstshst = new MKP.DataModels.DEVICE_STSHST()
                    {
                        DEVICE_STSHST1 = Convert.ToInt32(item[0]),
                        DEVICE_STSHST2 = Convert.ToInt32(item[1]),
                        DEVICE_STSHST3 = Convert.ToDateTime(item[2]),
                        DEVICE_STSHST4 = Convert.ToInt32(item[3]),
                        DEVICE_STSHST5 = (item[4]),
                        DEVICE_STSHST6 = Convert.ToDateTime(item[5])
                    };
                    db.Insert(DvcStsHst);
                }
                db.CommitTransaction();
            }
            progressBar1.Value = 95;
            this.Refresh();
            var DvcSts = GetResultFromQuerry("select * from DvcSts", 5);
            using (var db = new MKP.DataModels.BDETALONDB(Connection.conbd))
            {
                db.BeginTransaction();
                foreach (var item in DvcSts)
                {
                    var dvcsts = new MKP.DataModels.DEVICE_STATUS()
                    {
                        DEVICE_STATUS1 = Convert.ToInt32(item[0]),
                        DEVICE_STATUS2 = item[1],
                        DEVICE_STATUS3 = item[2]
                    };
                    db.Insert(DvcSts);
                }
                db.CommitTransaction();
            }
            progressBar1.Value = 100;
            this.Refresh();
            MessageBox.Show("Виконано!");
        }

        [DataContract]
        private class CurrentSettings
        {
            [DataMember]
            public List<int> lst;
        }
        public List<string[]> GetResultFromQuerry(string queryString, int columns, string connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=E:\MasterTool\mstrt.mdb;Persist Security Info=True;Jet OLEDB:Database Password=+/-eNg&0705")
        {
            List<string[]> lst = new List<string[]>();
            using (OleDbConnection connection = new OleDbConnection(connectionString))
            using (OleDbCommand command = new OleDbCommand(queryString, connection))
            {
                try
                {
                    connection.Open();
                    OleDbDataReader reader = command.ExecuteReader();
                    int index = -1;
                    while (reader.Read())
                    {
                        index++;
                        lst.Add(new string[columns]);
                        for (int i = 0; i < columns; i++)
                        {
                            lst[index][i] = reader[0].ToString();
                        }
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            return lst;
        }
    }
}
