using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;
using LinqToDB;
using System.Runtime.Serialization;
using MKP.PrintDevForms.Options.Json;
using MKP.Controls.Helper;
using MyContracts;

namespace WindowsFormsApplication2
{
    public partial class Main : UserControl
    {
        public bool isLoaded = false;
        public List<MyContracts.Contrals.mainhistory> mainHistory = new List<MyContracts.Contrals.mainhistory>();
        private FilteredBindingList<MyContracts.Contrals.mainhistory> source;
        private FilteredBindingList<MyContracts.Contrals.Statusy> source1;
        private FilteredBindingList<MyContracts.Contrals.Complect> source2;
        private FilteredBindingList<MyContracts.Contrals.work> source3;
        string conbd = string.Empty;
        public Main()
        {
            InitializeComponent();
        }
        public void Init()
        {
            if (isLoaded)
                return;
            isLoaded = true;
            source = new FilteredBindingList<MyContracts.Contrals.mainhistory>();
            dataGridView1.DataSource = source;
            source1 = new FilteredBindingList<MyContracts.Contrals.Statusy>();
            dgStud.DataSource = source1;
            source2 = new FilteredBindingList<MyContracts.Contrals.Complect>();
            dataGridView2.DataSource = source2;
            source3 = new FilteredBindingList<MyContracts.Contrals.work>();
            dataGridView3.DataSource = source3;
        }

        

        private void dataGridView1_KeyPress(object sender, KeyPressEventArgs e)
        {
            textBox1.Focus();
            textBox1.Text += e.KeyChar;
            textBox1.SelectionStart = textBox1.Text.Length;
        }
        string prevstr = "";
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (textBox1.Text == prevstr)
                return;
            prevstr = textBox1.Text;
            source.Clear();
            if (string.IsNullOrWhiteSpace(textBox1.Text))
                source.AddRange(mainHistory);
            else
            {
                if (char.IsDigit(prevstr[0]))
                {
                    source.AddRange(mainHistory.Where(i => i.number.StartsWith(prevstr)).ToList());
                }else
                    source.AddRange(mainHistory.Where(i => i.client.StartsWith(prevstr)).ToList());
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            isAdded = true;
            tabControl1.SelectedIndex = 1;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (dataGridView1.CurrentRow == null)
                return;
            if (Connection.channel.RemoveDirectoryItemById(MyContracts.FormDirectory.GetidDirectory<MyContracts.Contrals.mainhistory>(source[dataGridView1.CurrentRow.Index]), source[dataGridView1.CurrentRow.Index].id))
                source.Remove(source[dataGridView1.CurrentRow.Index]);
        }

        private void button10_Click(object sender, EventArgs e)
        {
            MyContracts.Contrals.Statusy brnd = new MyContracts.Contrals.Statusy();
            brnd.data = string.Empty;
            brnd.data_master = string.Empty;
            brnd.status = string.Empty;

            int id = Connection.channel.AddDirectoryItem(Encoding.UTF8.GetBytes(Json.Serialize<MyContracts.Contrals.Statusy>(brnd)), FormDirectory.GetidDirectory(brnd));
            if (id != -1)
            {
                brnd.id = id;
                source1.Insert(0, brnd);
            }
        }

        private void button12_Click(object sender, EventArgs e)
        {
            if (dgStud.CurrentRow == null)
                return;
            if (Connection.channel.RemoveDirectoryItemById(MyContracts.FormDirectory.GetidDirectory<MyContracts.Contrals.Statusy>(source1[dgStud.CurrentRow.Index]), source1[dgStud.CurrentRow.Index].id))
                source1.Remove(source1[dgStud.CurrentRow.Index]);
        }

        private void button15_Click(object sender, EventArgs e)
        {
            MyContracts.Contrals.Complect brnd = new MyContracts.Contrals.Complect();
            brnd.detail = string.Empty;

            int id = Connection.channel.AddDirectoryItem(Encoding.UTF8.GetBytes(Json.Serialize<MyContracts.Contrals.Complect>(brnd)), FormDirectory.GetidDirectory(brnd));
            if (id != -1)
            {
                brnd.id = id;
                source2.Insert(0, brnd);
            }
        }

        private void button13_Click(object sender, EventArgs e)
        {
            if (dataGridView2.CurrentRow == null)
                return;
            if (Connection.channel.RemoveDirectoryItemById(MyContracts.FormDirectory.GetidDirectory<MyContracts.Contrals.Complect>(source2[dataGridView2.CurrentRow.Index]), source2[dataGridView2.CurrentRow.Index].id))
                source2.Remove(source2[dataGridView2.CurrentRow.Index]);
        }

        private void button18_Click(object sender, EventArgs e)
        {
            MyContracts.Contrals.work brnd = new MyContracts.Contrals.work();
            brnd.cnt = string.Empty;
            brnd.data = string.Empty;
            brnd.master = string.Empty;
            brnd.name = string.Empty;
            brnd.price = string.Empty;

            int id = Connection.channel.AddDirectoryItem(Encoding.UTF8.GetBytes(Json.Serialize<MyContracts.Contrals.work>(brnd)), FormDirectory.GetidDirectory(brnd));
            if (id != -1)
            {
                brnd.id = id;
                source3.Insert(0, brnd);
            }
        }

        private void button16_Click(object sender, EventArgs e)
        {
            if (dataGridView3.CurrentRow == null)
                return;
            if (Connection.channel.RemoveDirectoryItemById(MyContracts.FormDirectory.GetidDirectory<MyContracts.Contrals.work>(source3[dataGridView3.CurrentRow.Index]), source3[dataGridView3.CurrentRow.Index].id))
                source3.Remove(source3[dataGridView3.CurrentRow.Index]);
        }
        public bool isAdded;
        MyContracts.Contrals.mainhistory brnd;
        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (isAdded)
            {
                brnd = new MyContracts.Contrals.mainhistory();
                brnd.adress = string.Empty;
                brnd.brand = string.Empty;
                brnd.client = string.Empty;
                brnd.data = string.Empty;
                brnd.master = string.Empty;
                brnd.metki = string.Empty;
                brnd.number = string.Empty;
                brnd.phone = string.Empty;
                brnd.serial_namber = string.Empty;
                brnd.status = string.Empty;
                brnd.tip = string.Empty;
               
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int id = Connection.channel.AddDirectoryItem(Encoding.UTF8.GetBytes(Json.Serialize<MyContracts.Contrals.mainhistory>(brnd)), FormDirectory.GetidDirectory(brnd));
            if (id != -1)
            {
                brnd.id = id;
                source.Insert(0, brnd);
            }
        }

        private void dataGridView1_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
                if (e.RowIndex == -1)
                return;
            isAdded = false;
            tabControl1.SelectedIndex = 1;
        }
    }
    public static class Addons
    {
        private const string dot = ".";
        private const string comma = ",";       
        public static double GetDouble(this string str)
        {
            double output = 0.00;
            str = str.Replace(dot, System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator).Replace(comma, System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator);
            if (str.Contains(System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator))
            {
                if (!double.TryParse(str, out output))
                {
                    throw new FormatException("������ �� ����� �������������� � ����� � ��������� �������, ��������� ���� ����������� � ������� � ������������� ���������");                    
                }
            }
            return .0;
        }
    }
}
