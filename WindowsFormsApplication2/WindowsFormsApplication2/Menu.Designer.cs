﻿namespace WindowsFormsApplication2
{
    partial class Menu
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Menu));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.файлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.клиентыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.изготовительToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.типУстройствToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.деталиКомплектаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.меткиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.типовыеНеисправностиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.работыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.файлToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(38, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // файлToolStripMenuItem
            // 
            this.файлToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.клиентыToolStripMenuItem,
            this.изготовительToolStripMenuItem,
            this.типУстройствToolStripMenuItem,
            this.деталиКомплектаToolStripMenuItem,
            this.меткиToolStripMenuItem,
            this.типовыеНеисправностиToolStripMenuItem,
            this.работыToolStripMenuItem});
            this.файлToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("файлToolStripMenuItem.Image")));
            this.файлToolStripMenuItem.Name = "файлToolStripMenuItem";
            this.файлToolStripMenuItem.Size = new System.Drawing.Size(28, 20);
            // 
            // клиентыToolStripMenuItem
            // 
            this.клиентыToolStripMenuItem.Name = "клиентыToolStripMenuItem";
            this.клиентыToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.клиентыToolStripMenuItem.Text = "Клiенти";
            this.клиентыToolStripMenuItem.Click += new System.EventHandler(this.клиентыToolStripMenuItem_Click);
            // 
            // изготовительToolStripMenuItem
            // 
            this.изготовительToolStripMenuItem.Name = "изготовительToolStripMenuItem";
            this.изготовительToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.изготовительToolStripMenuItem.Text = "Виробник";
            this.изготовительToolStripMenuItem.Click += new System.EventHandler(this.изготовительToolStripMenuItem_Click);
            // 
            // типУстройствToolStripMenuItem
            // 
            this.типУстройствToolStripMenuItem.Name = "типУстройствToolStripMenuItem";
            this.типУстройствToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.типУстройствToolStripMenuItem.Text = "Тип пристроїв";
            this.типУстройствToolStripMenuItem.Click += new System.EventHandler(this.типУстройствToolStripMenuItem_Click);
            // 
            // деталиКомплектаToolStripMenuItem
            // 
            this.деталиКомплектаToolStripMenuItem.Name = "деталиКомплектаToolStripMenuItem";
            this.деталиКомплектаToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.деталиКомплектаToolStripMenuItem.Text = "Деталі комплекту";
            this.деталиКомплектаToolStripMenuItem.Click += new System.EventHandler(this.деталиКомплектаToolStripMenuItem_Click);
            // 
            // меткиToolStripMenuItem
            // 
            this.меткиToolStripMenuItem.Name = "меткиToolStripMenuItem";
            this.меткиToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.меткиToolStripMenuItem.Text = "Мітки";
            this.меткиToolStripMenuItem.Click += new System.EventHandler(this.меткиToolStripMenuItem_Click);
            // 
            // типовыеНеисправностиToolStripMenuItem
            // 
            this.типовыеНеисправностиToolStripMenuItem.Name = "типовыеНеисправностиToolStripMenuItem";
            this.типовыеНеисправностиToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.типовыеНеисправностиToolStripMenuItem.Text = "Типові несправності";
            this.типовыеНеисправностиToolStripMenuItem.Click += new System.EventHandler(this.типовыеНеисправностиToolStripMenuItem_Click);
            // 
            // работыToolStripMenuItem
            // 
            this.работыToolStripMenuItem.Name = "работыToolStripMenuItem";
            this.работыToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.работыToolStripMenuItem.Text = "Роботи";
            this.работыToolStripMenuItem.Click += new System.EventHandler(this.работыToolStripMenuItem_Click);
            // 
            // Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Menu;
            this.Controls.Add(this.menuStrip1);
            this.Name = "Menu";
            this.Size = new System.Drawing.Size(38, 28);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem файлToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem клиентыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem изготовительToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem типУстройствToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem деталиКомплектаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem меткиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem типовыеНеисправностиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem работыToolStripMenuItem;
    }
}
