﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MKP.Controls.Helper;
using MKP.PrintDevForms.Options.Json;
using MyContracts;

namespace WindowsFormsApplication2
{
    public partial class Damag : UserControl
    {
        public bool isLoaded = false;
        private FilteredBindingList<MyContracts.Contrals.Damag> source;
        public Damag()
        {
            InitializeComponent();
        }
        public void Init()
        {
            if (isLoaded)
                return;
            isLoaded = true;
            source = new FilteredBindingList<MyContracts.Contrals.Damag>();
            dgStud.DataSource = source;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MyContracts.Contrals.Damag brnd = new MyContracts.Contrals.Damag();
            brnd.damag = string.Empty;
            int id = Connection.channel.AddDirectoryItem(Encoding.UTF8.GetBytes(Json.Serialize<MyContracts.Contrals.Damag>(brnd)), FormDirectory.GetidDirectory(brnd));
            if (id != -1)
            {
                brnd.id = id;
                source.Insert(0, brnd);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (dgStud.CurrentRow == null)
                return;
            if (Connection.channel.RemoveDirectoryItemById(MyContracts.FormDirectory.GetidDirectory<MyContracts.Contrals.Damag>(source[dgStud.CurrentRow.Index]), source[dgStud.CurrentRow.Index].id))
                source.Remove(source[dgStud.CurrentRow.Index]);
        }
    }
}
