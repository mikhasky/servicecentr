﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    public partial class FormSprav : Form
    {

        public FormSprav(Complect complect)
        {
            InitializeComponent();
            this.Controls.Add(complect);
            complect.Dock = DockStyle.Fill;
        }

        public FormSprav(Metki metki)
        {
            InitializeComponent();
            this.Controls.Add(metki);
            metki.Dock = DockStyle.Fill;
        }

        public FormSprav(Works works)
        {
            InitializeComponent();
            this.Controls.Add(works);
            works.Dock = DockStyle.Fill;
        }

        public FormSprav(Damag damag)
        {
            InitializeComponent();
            this.Controls.Add(damag);
            damag.Dock = DockStyle.Fill;
        }

        public FormSprav(DeviceTP deviceTP)
        {
            InitializeComponent();
            this.Controls.Add(deviceTP);
            deviceTP.Dock = DockStyle.Fill;
        }

        public FormSprav(Brand brand)
        {
            InitializeComponent();
            this.Controls.Add(brand);
            brand.Dock = DockStyle.Fill;
        }

        public FormSprav(Clients_contr clients_contr)
        {
            InitializeComponent();
            this.Controls.Add(clients_contr);
            clients_contr.Dock = DockStyle.Fill;
        }
            
        private void FormSprav_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Visible = false;
            e.Cancel = true;
        }
    }
}
