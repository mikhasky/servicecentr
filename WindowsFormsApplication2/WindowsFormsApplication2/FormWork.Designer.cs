﻿namespace WindowsFormsApplication2
{
    partial class FormWork
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.main1 = new WindowsFormsApplication2.Main();
            this.SuspendLayout();
            // 
            // main1
            // 
            this.main1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main1.Location = new System.Drawing.Point(0, 0);
            this.main1.Name = "main1";
            this.main1.Size = new System.Drawing.Size(1108, 606);
            this.main1.TabIndex = 0;
            // 
            // FormWork
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1108, 606);
            this.Controls.Add(this.main1);
            this.Name = "FormWork";
            this.Text = "FormWork";
            this.ResumeLayout(false);

        }

        #endregion

        private Main main1;
    }
}