﻿using MKP.Controls.Extension;
using MyContracts;
using MyContracts.Contrals;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }        
       
    }
    public static class Connection
    {
        public static string conbd=string.Empty;
        static Uri address;
        static EndpointAddress EndPoint;
        static ChannelFactory<iConstract> factory;
        public static iConstract channel;
        public static void Connect()
        {
            address = new Uri("http://localhost:1515/iConstract");

            EndPoint = new EndpointAddress(address);

            factory = new ChannelFactory<iConstract>(new BasicHttpBinding(), EndPoint);

            channel = factory.CreateChannel();

           /* while (true)
            {
                string text = Console.ReadLine();
                if (text == "quit")
                {
                    break;
                }
                channel.Say(text);
            }*/
        }
    }
    public static class Database
    {
        public static bool brandChanged = false;
        public static void LoadBrand()
        {            
           var item=Connection.channel.LoadDirectoryById(0);
            Encoding.UTF8.GetString(item).Deserialize<Brand>();
            brandChanged = true;
        }
        public static void LoadClient()
        {
            Connection.channel.LoadDirectoryById(1);
            brandChanged = true;
        }
        public static void LoadComplect()
        {
            Connection.channel.LoadDirectoryById(2);
            brandChanged = true;
        }        
        public static void LoadDamag()
        {
            Connection.channel.LoadDirectoryById(4);
            brandChanged = true;
        }
        public static void LoadDeviceTP()
        {
            Connection.channel.LoadDirectoryById(6);
            brandChanged = true;
        }
        public static void LoadMetki()
        {
            Connection.channel.LoadDirectoryById(7);
            brandChanged = true;
        }
        public static void LoadStatusy()
        {
            Connection.channel.LoadDirectoryById(8);
            brandChanged = true;
        }
        public static void Loadwork()
        {
            Connection.channel.LoadDirectoryById(9);
            brandChanged = true;
        }
        public static void LoadworkEnum()
        {
            Connection.channel.LoadDirectoryById(10);
            brandChanged = true;
        }
        public static void LoadAdmin()
        {
            Connection.channel.LoadDirectoryById(99);
            brandChanged = true;
        }    
        public static List<Brand> brands;
        public static List<Clients> clients;
        public static List<Complect> complect;        
        public static List<Damag> damag;
        public static List<DeviceTP> dvcTP;
        public static List<Metki> metki;
        public static List<Statusy> stats;
        public static List<work> Wrk;
        public static List<workEnum> WrkEnum;
        public static List<Admin> admin;
    }
}
