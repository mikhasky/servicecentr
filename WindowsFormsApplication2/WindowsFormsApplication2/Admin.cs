﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MKP.PrintDevForms.Options.Json;
using MKP.Controls.Helper;
using MyContracts;

namespace WindowsFormsApplication2
{
    public partial class Admin : UserControl
    {
        public bool isLoaded = false;
        private FilteredBindingList<MyContracts.Contrals.Admin> source;
        public Admin()
        {
            InitializeComponent();
        }
        public void Init()
        {
            if (isLoaded)
                return;
            isLoaded = true;
            source = new FilteredBindingList<MyContracts.Contrals.Admin>();
            dgStud.DataSource = source;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            MyContracts.Contrals.Admin brnd = new MyContracts.Contrals.Admin();
            brnd.loginUser = string.Empty;
            brnd.NameUser = string.Empty;
            brnd.passUser = string.Empty;
            int id = Connection.channel.AddDirectoryItem(Encoding.UTF8.GetBytes(Json.Serialize<MyContracts.Contrals.Admin>(brnd)), FormDirectory.GetidDirectory(brnd));
            if (id != -1)
            {
                brnd.id = id;
                source.Insert(0, brnd);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (dgStud.CurrentRow == null)
                return;
            if (Connection.channel.RemoveDirectoryItemById(MyContracts.FormDirectory.GetidDirectory<MyContracts.Contrals.Admin>(source[dgStud.CurrentRow.Index]), source[dgStud.CurrentRow.Index].id))
                source.Remove(source[dgStud.CurrentRow.Index]);
        }
    }
}
