﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    public partial class Menu : UserControl
    {
        FormSprav spravClient;
        FormSprav spravBrand;
        FormSprav spravDeviceTP;
        FormSprav spravComplect;
        FormSprav spravMetki;
        FormSprav spravDamag;
        FormSprav spravWorks;
        public Menu()
        {
            InitializeComponent();
        }

        private void клиентыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (spravClient == null)
                spravClient = new FormSprav(new Clients_contr());
            spravClient.Show();
            spravClient.TopMost = true;
        }

        private void изготовительToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (spravBrand == null)
                spravBrand = new FormSprav(new Brand());
            spravBrand.Show();
            spravBrand.TopMost = true;
        }

        private void типУстройствToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (spravDeviceTP == null)
                spravDeviceTP = new FormSprav(new DeviceTP());
            spravDeviceTP.Show();
            spravDeviceTP.TopMost = true;
        }

        private void деталиКомплектаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (spravComplect == null)
                spravComplect = new FormSprav(new Complect());
            spravComplect.Show();
            spravComplect.TopMost = true;
        }

        private void меткиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (spravMetki == null)
                spravMetki = new FormSprav(new Metki());
            spravMetki.Show();
            spravMetki.TopMost = true;

        }

        private void типовыеНеисправностиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (spravDamag == null)
                spravDamag = new FormSprav(new Damag());
            spravDamag.Show();
            spravDamag.TopMost = true;
        }

        private void работыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (spravWorks == null)
                spravWorks = new FormSprav(new Works());
            spravWorks.Show();
            spravWorks.TopMost = true;
        }
    }
}
