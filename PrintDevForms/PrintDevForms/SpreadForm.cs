﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MKP.PrintDevForms.Options;
using DevExpress.XtraRichEdit;
using DevExpress.XtraRichEdit.API.Layout;
using DevExpress.XtraRichEdit.API.Native;
using DevExpress.Spreadsheet;
using System.Text.RegularExpressions;

namespace MKP.PrintDevForms
{
    public partial class SpreadForm : UserControl
    {
        public Color? MainBaseColor = Color.Black;
        public Color? ResultBaseColor = Color.Black;
        public Color ResultMacroColor = Color.FromArgb(204, 51, 0);
        RichEditControl rtf = new RichEditControl();
        DevExpress.Spreadsheet.SearchOptions options;
        public SpreadForm()
        {
            InitializeComponent();
            InitSearchOptions();
            splitContainer1.SplitterDistance = 152;
            Macroses[] test = new Macroses[3];
            string[,] tabl = new string[10, 8];
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    if (i == 0 && j < 3)
                        tabl[i, j] = "Общая строка";
                    else if ((i == 1 && j == 0) || (i == 2 && j == 0))
                        tabl[i, j] = "Общая колонка";
                    else
                        tabl[i, j] = "Значениеi,j => i=" + i + ", j=" + j;
                }
            }
            test[0] = new Macroses() { macro = "ololo1", result = "test1" };
            test[1] = new Macroses() { macro = "ololo2", result = "test2" };
            test[2] = new Macroses(MacroType.ArrayString) { macro = "табличко", result = tabl, rowStart = 1 };
            List<Macroses[]> macro = new List<Macroses[]>();
            macro.Add(test);
            //macro.Add(test2);
            // macro.Add(test2);
            // macro.Add(test3);
            Option.macs = Dev.macroInit(macro);
            Option.IdScript = "-1"; Option.macs = macro;
            List<string> mas = new List<string>();
            _StaticFuntion.InitStaticFunction();
            for (int i = 0; i < _StaticFuntion.lst.Count; i++)
                dgPForms.Rows.Add(_StaticFuntion.lst[i], _StaticFuntion.lst[i].macro, _StaticFuntion.lst[i].result);
            for (int i = 0; i < macro[0].Length; i++)
            {
                dgPForms.Rows.Add((int)macro[0][i].type, macro[0][i].macro, macro[0][i].result);
                mas.Add(macro[0][i].macro);
            }
            spread1.SetMacroses(mas, _StaticFuntion.lst.Select(i => i.macro).ToList());
        }
        public SpreadForm(object macro, string idScript, string ConBD, string ConBDD, ModeStart mode, int UserID, string fastSaveFullPath = "")
        {
            InitializeComponent();
            InitSearchOptions();
            splitContainer1.SplitterDistance = 152;
        }
        /// <summary>
        /// Generate Search Option
        /// </summary>
        private void InitSearchOptions()
        {
            options = new DevExpress.Spreadsheet.SearchOptions();
            options.MatchCase = false;
            options.SearchIn = SearchIn.ValuesAndFormulas;
            options.SearchBy = SearchBy.Columns | SearchBy.Rows;
            options.MatchEntireCellContents = false;
        }
        private void dgPForms_SelectionChanged(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            dataGridView1.Columns.Clear();
            if (dgPForms.CurrentRow == null)
                return;
            bool Yet = false;
            for (int i = 0; i < Option.macs.Count; i++)
            {
                for (int j = 0; j < Option.macs[i].Length; j++)
                {
                    if (Option.macs[i][j].macro == dgPForms.CurrentRow.Cells[1].Value.ToString())
                        switch ((MacroType)Convert.ToInt32(dgPForms.CurrentRow.Cells[0].Value))
                        {
                            case MacroType.String:
                                if (!Yet)
                                {
                                    dataGridView1.Columns.Add(dgPForms.CurrentRow.Cells[1].Value.ToString(), dgPForms.CurrentRow.Cells[1].Value.ToString());
                                    Yet = true;
                                }
                                dataGridView1.Rows.Add(Option.macs[i][j].result);
                                break;
                            case MacroType.Rtf:
                                if (!Yet)
                                {
                                    dataGridView1.Columns.Add(dgPForms.CurrentRow.Cells[1].Value.ToString(), dgPForms.CurrentRow.Cells[1].Value.ToString());
                                    Yet = true;
                                }
                                rtf.RtfText = Option.macs[i][j].result.ToString();
                                dataGridView1.Rows.Add(rtf.Text);
                                break;
                            case MacroType.ArrayString:
                                if (i == 1)
                                    continue;
                                string[,] arr = Option.macs[i][j].result as string[,];
                                if (arr == null)
                                    continue;
                                if (!Yet)
                                {
                                    for (int k = 0; k < arr.GetUpperBound(1) + 1; k++)
                                        dataGridView1.Columns.Add(k.ToString(), k.ToString());
                                    Yet = true;
                                }
                                int rowstart = 0;

                                for (int k = 0; k < arr.GetUpperBound(0) + 1; k++)
                                {
                                    dataGridView1.Rows.Add();
                                    if (k >= Option.macs[i][j].rowStart)
                                    {
                                        dataGridView1.Rows[k].HeaderCell.Value = (rowstart++).ToString();
                                    }
                                    for (int l = 0; l < arr.GetUpperBound(1) + 1; l++)
                                        dataGridView1.Rows[k].Cells[l].Value = arr[k, l];
                                    dataGridView1.Rows[k].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
                                }
                                return;
                                // break;
                        }
                }
            }
        }

        private void dgPForms_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.C)
                return;
            if (dgPForms.CurrentRow == null)
                return;
            if (dgPForms.CurrentCell == null)
                return;
            try
            {
                Clipboard.Clear();
            }
            catch { }
            try
            {
                Clipboard.SetText(dgPForms.CurrentCell.Value.ToString());
            }
            catch { }
        }
        private void button2_Click(object sender, EventArgs e)
        {
            DocPrint(2);
        }
        private void PreInitializeWorkSpace(ResultSpreadSheet result)
        {
            for (int i = 0; i < spread1.spreadsheetControl.Document.Worksheets.Count; i++)
            {
                if (i != 0)
                {
                    result.spread1.spreadsheetControl.Document.Worksheets.Add(spread1.spreadsheetControl.Document.Worksheets[i].Name);
                }
                else
                    result.spread1.spreadsheetControl.Document.Worksheets[i].Name = spread1.spreadsheetControl.Document.Worksheets[i].Name;
                result.spread1.spreadsheetControl.Document.Worksheets[i].CopyFrom(spread1.spreadsheetControl.Document.Worksheets[i]);
                result.spread1.spreadsheetControl.Document.Worksheets[i].Cells[1, 1].Style.Font.Color = Color.Black;
                //result.spread1.spreadsheetControl.Document.Worksheets[i].Cells.Style.Font.Color = Color.Black;
            }
        }
        public void DocPrint(int type)
        {
            GC.Collect();
            List<ResultSpreadSheet> results = new List<ResultSpreadSheet>(); ;
            results.Add(new ResultSpreadSheet());
            PreInitializeWorkSpace(results[0]);            
            if (Option.macs != null && Option.macs.Count > 0)
            {                  
                //on save or open few documents??? 
                for (int j = 0; j < Option.macs.Count; j++)
                {
                    if (j != 0)
                    {
                        results.Add(new ResultSpreadSheet());
                        PreInitializeWorkSpace(results[j]);
                    }
                    //recognition macros
                    for (int i = 0; i < Option.macs[j].Length; i++)
                    {
                        if (Option.macs[j][i].result == null)
                            continue;
                        foreach (Worksheet sheet in results[j].spread1.spreadsheetControl.Document.Worksheets)
                        {
                            CollectionInfo.RowInserted.Clear();
                            switch (Option.macs[j][i].type)
                            {
                                case MacroType.String:
                                    //goto on all worksheet

                                    //find all needed cell
                                    foreach (DevExpress.Spreadsheet.Cell cell in sheet.Search(Option.macs[j][i].macro, options))
                                    {
                                        //switch this cell.value type???(maybe convert value or ???)
                                        //TODO
                                        cell.Value = cell.DisplayText.Replace(Option.macs[j][i].macro, Option.macs[j][i].result.ToString());
                                        cell.Font.Color = Color.Black;
                                    }
                                    break;
                                case MacroType.Rtf:
                                    //MacroRTF(result, j, i);
                                    break;
                                case MacroType.ArrayString:
                                    if (!(Option.macs[j][i].result is string[,]))
                                        continue;
                                   string[,] arr = Option.macs[j][i].result as string[,];
                                    if (Option.macs[j][i].rowStart != 0)
                                    {
                                        string[,] t2 = new string[arr.GetLength(0) - Option.macs[j][i].rowStart, arr.GetLength(1)];
                                        Array.Copy(arr, Option.macs[j][i].rowStart * t2.GetLength(1), t2, 0, t2.Length);
                                        arr = t2;
                                    }
                                    MacroArrString(sheet, results[j], j, i, arr);
                                    break;
                            }
                        }
                    }
                }
            }
            foreach (var item in results)
            {
                item.Show();
            }
        }

        private void MacroArrString(Worksheet sheet, ResultSpreadSheet resultSpreadSheet, int j, int i,string[,] arr)
        {
            
            Regex OneItemArray = new Regex(Option.macs[j][i].macro + Generate.pattern1, RegexOptions.CultureInvariant);
            Regex ColumnGenerator = new Regex(Option.macs[j][i].macro + Generate.pattern2, RegexOptions.CultureInvariant);
            foreach (DevExpress.Spreadsheet.Cell cell in sheet.Search(Option.macs[j][i].macro,options))
            {
                OneOfArray(arr, OneItemArray, cell);
                int a1, a2;
                foreach (System.Text.RegularExpressions.Match item in ColumnGenerator.Matches(cell.DisplayText))
                {
                    int sk = arr.GetLength(0) - 1;
                    Generate.TextOption opt = new Generate.TextOption(item.Value, true);
                    for (int k = cell.RowIndex; sk > 0; k++, sk--)
                    {
                        if (!CollectionInfo.RowInserted.Contains(k + 1))
                        {
                            sheet.Rows.Insert(k);
                            CollectionInfo.RowInserted.Add(k + 1);
                            sheet.Cells[k + 1, cell.ColumnIndex].Value = cell.Value;
                        }
                    }
                    int arrIndex = opt.values[0];
                    if (arr.GetLength(0) == 0)
                    {
                        cell.Value = cell.DisplayText.Replace(opt.textOriginnal, string.Empty); 
                        continue;
                    }
                    if (opt.Minus)
                        arrIndex = arr.GetLength(1) - 1 - arrIndex;
                    if (!opt.Merge)
                        for (int k = cell.RowIndex, l = 0; k < arr.GetLength(0) + cell.RowIndex; k++, l++)
                        {
                            sheet.Cells[k, cell.ColumnIndex].Value = sheet.Cells[k, cell.ColumnIndex].DisplayText.Replace(opt.textOriginnal, arr[l, arrIndex]);
                        }
                    else
                    {
                        string prev = string.Empty;
                        for (int k = cell.RowIndex, l = 0; k < arr.GetLength(0) + cell.RowIndex; k++, l++)
                        {
                            if (l != 0 && prev == arr[l, arrIndex])
                            {
                                sheet.Cells[k, cell.ColumnIndex].Value = sheet.Cells[k, cell.ColumnIndex].DisplayText.Replace(opt.textOriginnal, arr[l, arrIndex]);
                               // sheet.MergeCells
                               // cell.Table.MergeCells(cell.Table[k - 1, cell.Index], cell.Table[k, cell.Index]);
                            }
                            else
                               // result.editorControl1.richEditControl1.Document.ReplaceAll(opt.textOriginnal, arr[l, arrIndex], SearchOptions.CaseSensitive, cell.Table[k, cell.Index].ContentRange);
                            prev = arr[l, arrIndex];
                        }
                    }

                }
            }
        }
        private static void SetResultReplaceArr(Result result, string[,] arr, TableCell cell, Generate.TextOption opt, int arrIndex)
        {
            if (!opt.Merge)
                for (int k = cell.Row.Index, l = 0; k < arr.GetLength(0) + cell.Row.Index; k++, l++)
                {
                  //  result.editorControl1.richEditControl1.Document.ReplaceAll(opt.textOriginnal, arr[l, arrIndex], SearchOptions.CaseSensitive, cell.Table[k, cell.Index].ContentRange);
                }
            else
            {
                string prev = string.Empty;
                for (int k = cell.Row.Index, l = 0; k < arr.GetLength(0) + cell.Row.Index; k++, l++)
                {
                    if (l != 0 && prev == arr[l, arrIndex])
                    {
                       // result.editorControl1.richEditControl1.Document.ReplaceAll(opt.textOriginnal, string.Empty, SearchOptions.CaseSensitive, cell.Table[k, cell.Index].ContentRange);
                        cell.Table.MergeCells(cell.Table[k - 1, cell.Index], cell.Table[k, cell.Index]);
                    }
                    else
                       // result.editorControl1.richEditControl1.Document.ReplaceAll(opt.textOriginnal, arr[l, arrIndex], SearchOptions.CaseSensitive, cell.Table[k, cell.Index].ContentRange);
                    prev = arr[l, arrIndex];
                }
            }
        }
        private static void OneOfArray(string[,] arr, Regex OneItemArray, DevExpress.Spreadsheet.Cell cell)
        {
            int a1, a2;
            foreach (System.Text.RegularExpressions.Match item in OneItemArray.Matches(cell.DisplayText))
            {
                string[] indexes = item.Value.Split(new char[] { '[', ']', ',' }, StringSplitOptions.RemoveEmptyEntries);
                if (!int.TryParse(indexes[1], out a1) || !int.TryParse(indexes[2], out a2))
                    continue;
                cell.Value = cell.DisplayText.Replace(item.Value, arr[a2, a1]);
            }
        }
    }
}
