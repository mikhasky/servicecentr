﻿using DevExpress.Spreadsheet;
using System.Collections.Generic;
using System.Drawing;

namespace MKP.PrintDevForms
{
    public static class SpreadCell
    {
        public class MergedCellData
        {
            public bool isMerged = false;
            int ColumnStart = -1;
            int ColumnEnd = -1;
            public MergedCellData(IList<Range> range)
            {
                if (range.Count > 0)
                {
                    isMerged = true;
                    ColumnStart = range[0].LeftColumnIndex;
                    ColumnEnd = range[0].RightColumnIndex;                    
                }
            }
            public void CopyMergedData(DevExpress.Spreadsheet.Cell setup)
            {
                if (!isMerged)
                    return;
                setup.Worksheet.MergeCells(setup.Worksheet.Range.FromLTRB(ColumnStart,setup.RowIndex,ColumnEnd,setup.RowIndex));
            }
        }
        public class AlignmentCellData
        {
            private SpreadsheetHorizontalAlignment Horizontal;
            private int Indent;
            private int RotationAngle;
            private bool ShrinkToFit;
            private SpreadsheetVerticalAlignment Vertical;
            private bool WrapText;
            public AlignmentCellData(Alignment original)
            {
                Horizontal = original.Horizontal;
                Indent = original.Indent;
                RotationAngle = original.RotationAngle;
                ShrinkToFit = original.ShrinkToFit;
                Vertical = original.Vertical;
                WrapText = original.WrapText;
            }
            public void CopyAlignmentStyle(Alignment setup)
            {
                setup.Horizontal= Horizontal;
                setup.Indent= Indent;
                setup.RotationAngle= RotationAngle;
                setup.ShrinkToFit= ShrinkToFit;
                setup.Vertical= Vertical;
                setup.WrapText= WrapText;
            }
        }
        public class BorderCellData
        {
            private MergedCellData mcd = null;
            private Color BottomBorderColor;
            private BorderLineStyle BottomStyle;

            private Color LeftBorderColor;
            private BorderLineStyle LeftStyle;

            private Color TopBorderColor;
            private BorderLineStyle TopStyle;

            private Color RightBorderColor;
            private BorderLineStyle RightStyle;

            private DiagonalBorderType DiagonalType;
            private Color DiagonalColor;
            private BorderLineStyle DiagonalStyle;
            public BorderCellData(Borders original,MergedCellData merger=null)
            {
                mcd=merger;
                BottomBorderColor = original.BottomBorder.Color;
                BottomStyle= original.BottomBorder.LineStyle;

                LeftBorderColor = original.LeftBorder.Color;
                LeftStyle = original.LeftBorder.LineStyle;

                RightBorderColor = original.RightBorder.Color;
                RightStyle = original.RightBorder.LineStyle;

                TopBorderColor = original.TopBorder.Color;
                TopStyle = original.TopBorder.LineStyle;

                DiagonalType = original.DiagonalBorderType;
                DiagonalColor = original.DiagonalBorderColor;
                DiagonalStyle = original.DiagonalBorderLineStyle;
            }
            public bool CheckIdenticBorders(Borders setup)
            {
                if (setup.BottomBorder.Color == BottomBorderColor &&
                setup.BottomBorder.LineStyle == BottomStyle &&

                setup.LeftBorder.Color == LeftBorderColor &&
                setup.LeftBorder.LineStyle == LeftStyle &&

                setup.TopBorder.Color == TopBorderColor &&
                setup.TopBorder.LineStyle == TopStyle &&

                setup.RightBorder.Color == RightBorderColor &&
                setup.RightBorder.LineStyle == RightStyle &&

                setup.DiagonalBorderType == DiagonalType &&
                setup.DiagonalBorderColor == DiagonalColor &&
                setup.DiagonalBorderLineStyle == DiagonalStyle)
                    return true;
                return false;
            }
            private void SetUp(Borders setup)
            {
                setup.BottomBorder.Color = BottomBorderColor;
                setup.BottomBorder.LineStyle = BottomStyle;

                setup.LeftBorder.Color = LeftBorderColor;
                setup.LeftBorder.LineStyle = LeftStyle;

                setup.TopBorder.Color = TopBorderColor;
                setup.TopBorder.LineStyle = TopStyle;

                setup.RightBorder.Color = RightBorderColor;
                setup.RightBorder.LineStyle = RightStyle;

                setup.DiagonalBorderType = DiagonalType;
                setup.DiagonalBorderColor = DiagonalColor;
                setup.DiagonalBorderLineStyle = DiagonalStyle;
            }
            public void CopyBorderStyle(DevExpress.Spreadsheet.Cell cell)
            {                
                if (mcd == null||!mcd.isMerged)
                    SetUp(cell.Borders);
                else
                {
                    mcd.CopyMergedData(cell);
                    foreach (var item in cell.GetMergedRanges())
                        SetUp(item.Borders);
                }
                
            }            
        }
    }
}
