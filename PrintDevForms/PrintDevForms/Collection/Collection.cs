﻿using DevExpress.XtraRichEdit.API.Native;
using System.Collections.Generic;
namespace MKP.PrintDevForms
{
    public static class Collection
    {
        public static RowCollection rowCollection;
        public static List<TableCell> cells;
    }
    public static class CollectionInfo
    {
        public static void AddRow(int row)
        {
            RowInserted.Add(row);
            for (int i = 0; i < RowInserted.Count; i++)
            {
                if (RowInserted[i] > row)
                    RowInserted[i]++;
            }
        }
        public static List<int> RowInserted = new List<int>();
        public static Table currTable;
        public static string[] Cells;
        public static string currentMacro;
    }
    public class TableInfo
    {
        Table tab;
        List<TableRow> Rows = new List<TableRow>();
        public TableInfo(Table t)
        {
            tab = t;
        }
        public void Clear()
        {
            Rows.Clear();
        }
        /// <summary>
        /// Эту строку уже добавили, её индекс !=-1, а значит не добавляем её, а просто пишем сюда же
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        public int IndexOf(TableRow row)
        {
            return Rows.IndexOf(row);
        }
        public void AddRow(TableRow row)
        {
            Rows.Add(row);
        }
    }
    public class RowCollection
    {
        /// <summary>
        /// Заголовочные строки
        /// </summary>
        public List<Row> Rows=new List<Row>();
        public Document doc;
        public Table tab;
        public TableInfo info;
        public RowCollection(Document doc,Table tab)
        {
            this.tab = tab;
            info = new TableInfo(tab);
            this.doc=doc;
            Collection.rowCollection = this;
        }
        public void Add(TableRow Row)
        {
            Rows.Add(new Row(Row, doc));
        }
        public void Clear()
        {
            Rows.Clear();
        }
      
        public void InsertRowCollection(int RowIndex,InsertedOption option)
        {
            switch (option)
            {
                case InsertedOption.After:
                 
                    break;
                case InsertedOption.Before:
                    Rows.Reverse();
                    foreach (Row row in Rows)
                        {
                            tab.Rows.InsertBefore(RowIndex);
                        for (int i = 0; i < row.Cells.Count; i++)
                            {                            
                            doc.InsertRtfText(tab.Rows[RowIndex].Cells[i].ContentRange.Start, row.Cells[i].contentRangeRTF);
                            tab.Rows[RowIndex].Cells[i].VerticalAlignment = row.Cells[i].current.VerticalAlignment;
                            ParagraphProperties props = doc.BeginUpdateParagraphs(tab.Rows[RowIndex].Cells[i].Range);
                            props.Alignment = row.Cells[i].palign;
                            props.Style.Alignment = row.Cells[i].palignStyle;                           
                            doc.EndUpdateParagraphs(props);
                           //var cp= doc.BeginUpdateCharacters(tab.Rows[RowIndex].Cells[i].Range);
                            
                           // tab.Rows[RowIndex].Cells[i].Style.VerticalAlignment = row.Cells[i].current.Style.VerticalAlignment;
                           // tab.Rows[RowIndex].Cells[i].Style.Alignment = row.Cells[i].current.Style.Alignment;
                          //  tab.Rows[RowIndex].Cells[i].Style = row.Cells[i].current.Style;
                            //tab.Rows[RowIndex].Cells[i].VerticalAlignment = row.Cells[i].current.VerticalAlignment;
                        }                      
                        }
                        Rows.Reverse();
                    break;
            }
            
        }
        public enum InsertedOption
        {
            Before,
            After
        }
    }
    
    public class Row
    {
        public List<Cell> Cells = new List<Cell>();
        public TableRow current;
        public string RangeRTF;
        public string Text;
        public Row(TableRow Row, Document doc)
        {
            current = Row;
            RangeRTF = doc.GetRtfText(Row.Range);
            Text = doc.GetText(Row.Range);
            for (int i = 0; i < Row.Cells.Count; i++)
                Cells.Add(new Cell(Row.Cells[i],doc));
        }
    }
    public class Cell
    {
        public Cell(TableCell cell, Document doc)
        {
            current = cell;
            RangeRTF=doc.GetRtfText(cell.Range);
            contentRangeRTF = doc.GetRtfText(cell.ContentRange);
            Text = doc.GetText(cell.Range);
            ContentText= doc.GetText(cell.ContentRange);
            var props = doc.BeginUpdateParagraphs(cell.Range);
            palign = props.Alignment;
            palignStyle = props.Style.Alignment;
            doc.EndUpdateParagraphs(props);
            //cp=doc.BeginUpdateCharacters(cell.Range);
           // doc.EndUpdateCharacters(cp);
        }
        public ParagraphAlignment? palign;
        public ParagraphAlignment? palignStyle;
        //public CharacterProperties cp;
        public TableCell current;
        public string RangeRTF;
        public string contentRangeRTF;
        public string Text;
        public string ContentText;
    }
}
