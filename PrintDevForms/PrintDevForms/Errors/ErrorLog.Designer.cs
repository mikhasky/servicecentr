﻿namespace MKP.PrintDevForms.logs
{
    partial class ErrorLog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgPForms = new System.Windows.Forms.DataGridView();
            this.formi7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.resultMacro = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgPForms)).BeginInit();
            this.SuspendLayout();
            // 
            // dgPForms
            // 
            this.dgPForms.AllowUserToAddRows = false;
            this.dgPForms.AllowUserToDeleteRows = false;
            this.dgPForms.AllowUserToResizeRows = false;
            this.dgPForms.BackgroundColor = System.Drawing.SystemColors.Menu;
            this.dgPForms.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgPForms.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgPForms.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.formi7,
            this.resultMacro});
            this.dgPForms.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgPForms.GridColor = System.Drawing.SystemColors.AppWorkspace;
            this.dgPForms.Location = new System.Drawing.Point(0, 0);
            this.dgPForms.MultiSelect = false;
            this.dgPForms.Name = "dgPForms";
            this.dgPForms.ReadOnly = true;
            this.dgPForms.RowHeadersVisible = false;
            this.dgPForms.RowTemplate.Height = 19;
            this.dgPForms.ShowCellToolTips = false;
            this.dgPForms.Size = new System.Drawing.Size(204, 262);
            this.dgPForms.TabIndex = 2;
            // 
            // formi7
            // 
            this.formi7.HeaderText = "Макрос";
            this.formi7.Name = "formi7";
            this.formi7.ReadOnly = true;
            this.formi7.Width = 98;
            // 
            // resultMacro
            // 
            this.resultMacro.HeaderText = "Ошибка";
            this.resultMacro.Name = "resultMacro";
            this.resultMacro.ReadOnly = true;
            this.resultMacro.Width = 102;
            // 
            // ErrorLog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(204, 262);
            this.Controls.Add(this.dgPForms);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ErrorLog";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Ошибки";
            this.TopMost = true;
            ((System.ComponentModel.ISupportInitialize)(this.dgPForms)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgPForms;
        private System.Windows.Forms.DataGridViewTextBoxColumn formi7;
        private System.Windows.Forms.DataGridViewTextBoxColumn resultMacro;
    }
}