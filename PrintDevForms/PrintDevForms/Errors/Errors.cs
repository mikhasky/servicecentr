﻿using System.Collections.Generic;

namespace MKP.PrintDevForms.logs
{
    public class Errors
    {
        public static Errors err;
        public Errors()
        {
            lst = new List<Error>();
        }
        public void Add(string macro, string err)
        {
            lst.Add(new Error(macro,err));
        }
        public List<Error> lst;
    }
    public class Error
    {
        public Error(string macro, string err)
        {
            Macro = macro;
            Err = err;
        }
        public string Macro;
        public string Err;
    }
    public enum Err
    {
        Index=0,
        FilePath=1,
        FileAccess=2,
        OtherError=99
    }
}
