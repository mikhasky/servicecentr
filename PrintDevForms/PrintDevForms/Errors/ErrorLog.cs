﻿using System.Windows.Forms;

namespace MKP.PrintDevForms.logs
{
    public partial class ErrorLog : Form
    {
        public ErrorLog(Errors err)
        {
            InitializeComponent();
            foreach (var item in err.lst)
            {
                dgPForms.Rows.Add(item.Macro, item.Err);
            }
        }
    }
}
