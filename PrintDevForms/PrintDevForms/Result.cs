﻿using DevExpress.XtraRichEdit;
using System;
using System.Windows.Forms;

namespace MKP.PrintDevForms
{
    public partial class Result : Form
    {
       public int CurrentPageIndex { get; set; }
       public int PageCount { get; set; }
        public Result()
        {
            InitializeComponent();
            editorControl1.richEditControl1.UpdateUI += new EventHandler(UI);
        }
        public void UI(object sender, EventArgs e)
        {
            /*PageBasedRichEditView currentView = editorControl1.richEditControl1.ActiveView as PageBasedRichEditView;
            CurrentPageIndex = currentView.CurrentPageIndex;
            PageCount = currentView.PageCount;*/
        }
        public void View(bool a)
        {
            this.editorControl1.printItem1.Down = a;
        }
    }
}
