﻿using MKP.PrintDevForms.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MKP.PrintDevForms
{
    public class Form1 : IDisposable
    {
        public static MainForm instance;
        public static event RecognizedDelegate RecognozeFinished;
        public static event EditorClose CurrentEditorClose;
        public event RecognizedDelegate RecognizeFinished;
        bool loaded = false;
        Loader _loader;
        public Form1(object macro, string idScript, string ConBD, string ConBDD, ModeStart mode, int UserID, string fastSaveFullPath = "", EditorType edt = EditorType.Word)
        {
            if (instance == null)
            {
                instance = new MainForm();                
            }
            instance.RecognozeFinished -= RecognozeFinished;
            instance.CurrentEditorClose -= CloseEvent;
            if (mode == ModeStart.Моментальный_результат_после_загрузки)
            {
                instance.CurrentEditorClose += CloseEvent;
                instance.RecognozeFinished += RecognozeFinished;
                instance.Load(macro, idScript, ConBD, ConBDD, mode, UserID, fastSaveFullPath, edt);
            }
            else if (mode == ModeStart.Вызов_результата_распознавания_без_показа)
            {                
                _loader = new Loader(macro, idScript, ConBD, ConBDD, mode, UserID, edt,this);
            }else
            instance.Load(macro, idScript, ConBD, ConBDD, mode, UserID, fastSaveFullPath, edt);
        }
        private class Loader
        {
            object macro; string idScript; string ConBD; string ConBDD; ModeStart mode; int UserID; string fastSaveFullPath = ""; EditorType edt;Form1 frm;
            public Loader(object macro, string idScript, string ConBD, string ConBDD, ModeStart mode, int UserID, EditorType edt,Form1 frm)
            {
                this.macro = macro;
                this.idScript = idScript;
                this.ConBD = ConBD;
                this.ConBDD=ConBDD;
                this.mode = mode;
                this.UserID = UserID;
                this.fastSaveFullPath = "";
                this.edt = edt;
                this.frm = frm;
            }
            public void LoadInstance()
            {
                instance.RecognozeFinished += frm.RecognizeFinished;
                instance.Load(macro, idScript, ConBD, ConBDD, mode, UserID, fastSaveFullPath, edt);
                instance.RecognozeFinished -= frm.RecognizeFinished;
            }
        }
        public void StartScan()
        {
            _loader.LoadInstance();
        }
        private void CloseEvent()
        {
            instance.CurrentEditorClose -= CloseEvent;
            CurrentEditorClose?.Invoke();
        }
        public void Close()
        {
            instance.EditorClose();
        }
        public void ShowDialog()
        {
            Show();
        }
        public void Show()
        {
            if (!loaded)
            {
                instance.Show();
                loaded = true;
            }
            else
                instance.Visible = true;
        }
        public void Dispose()
        { 
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }
    }
}
