﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraRichEdit;
using MKP.PrintDevForms.Options;

namespace MKP.PrintDevForms
{
    public enum EditorType
    {
        Word=0,
        Spread=1,
        Unknown=2
    }
    public delegate void RecognizedDelegate(EditorType type,object editor);
    public delegate void EditorClose();
    public partial class MainForm : Form
    {
        public static WordForm Word;
        public static SpreadForm Spread;
        private static bool instanceLoaded;
        public EditorType CurrentEditor;
        public static object mCollection = null; 
        Result result = new Result();  
        public event RecognizedDelegate RecognozeFinished;
        public event EditorClose CurrentEditorClose;
        public ModeStart _mode=ModeStart.Стандартный_режим;
        public MainForm()
        {
            InitializeComponent();
            /*Word = new WordForm();
            this.Controls.Add(Word);
            Word.Dock = DockStyle.Fill;*/
            /*Spread = new SpreadForm();
            this.Controls.Add(Spread);
            Spread.Dock = DockStyle.Fill;*/
            //DocPrint(2);
        }
        public void Load(object macro, string idScript, string ConBD, string ConBDD, ModeStart mode, int UserID, string fastSaveFullPath = "", EditorType edt = EditorType.Word)
        {
            instanceLoaded = true;
            _mode = mode;
            CurrentEditor = edt;
            switch (edt)
            {
                case EditorType.Word:
                    if (Word == null)
                        Word = new WordForm(true);
                    Word.RecognozeFinished -= RecoFinished;
                    Word.RecognozeFinished += RecoFinished;
                    Word.LoadWordForm(macro, idScript, ConBD, ConBDD, mode, UserID, fastSaveFullPath);
                  
                    this.Controls.Add(Word);
                    Word.Dock = DockStyle.Fill;
                  
                    break;
                case EditorType.Spread:
                    if (Spread == null)
                        Spread = new SpreadForm(true);
                    Spread.RecognozeFinished -= RecoFinished;
                    Spread.RecognozeFinished += RecoFinished;
                    Spread.LoadSpreadForm(macro, idScript, ConBD, ConBDD, mode, UserID, fastSaveFullPath);
                    this.Controls.Add(Spread);
                    Spread.Dock = DockStyle.Fill;
                    break;
            }
            this.VisibleChanged -= CurrentVisibleChanged;
            this.VisibleChanged += CurrentVisibleChanged;
            switch (mode)
            {
                case ModeStart.Моментальный_результат_после_загрузки: this.Visible=false;break;
                case ModeStart.Вызов_результата_распознавания_без_показа: this.Visible = false; break;
            }
        }

        private void CurrentVisibleChanged(object sender, EventArgs e)
        {
            //if (!this.Visible)
            {                
              //  Word.RecognozeFinished -= RecoFinished;
            }
        }

        private void RecoFinished(EditorType type, object result)
        {            
          RecognozeFinished?.Invoke(type, result); 
        }

        public MainForm(object macro,string idScript,string ConBD,string ConBDD,ModeStart mode,int UserID,string fastSaveFullPath="",EditorType edt=EditorType.Word)
        {
            instanceLoaded = true;
            mCollection = macro;                  
                InitializeComponent();           
            CurrentEditor = edt;
            this.Controls.Clear();          
            switch (edt)
            {
                case EditorType.Word:
                    if (Word==null)
                    Word = new WordForm(true);
                 
                    Word.LoadWordForm(macro,idScript,ConBD,ConBDD,mode,UserID,fastSaveFullPath);                                   
                  
                    this.Controls.Add(Word);
                    Word.Dock = DockStyle.Fill;
                 
                    break;
                case EditorType.Spread:
                    if (Spread==null)
                    Spread = new SpreadForm(true);
                    Spread.LoadSpreadForm(macro, idScript, ConBD, ConBDD, mode, UserID, fastSaveFullPath);
                    this.Controls.Add(Spread);
                    Spread.Dock = DockStyle.Fill;
                    break;
            }
        }
        public void EditorClose()
        {
            if (CurrentEditor == EditorType.Word)
            {
                CurrentEditorClose?.Invoke();
                /*if (Option.memory != Word.editorControl1.richEditControl1.Document.RtfText)
                {
                    switch (MessageBox.Show("Сохранить перед выходом?", "Сохранение", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
                    {
                        case System.Windows.Forms.DialogResult.Yes:
                            Word.button1_Click(null, null);
                            break;
                        case System.Windows.Forms.DialogResult.Ignore:
                            break;
                        case System.Windows.Forms.DialogResult.Cancel:
                            e.Cancel = true;
                            break;
                    }
                }*/
                this.Controls.Remove(Word);
                //  Word.editorControl1.richEditControl1.Dispose();
                // Word.editorControl1.ribbonControl1.Dispose();
                // MessageBox.Show(Word.editorControl1.richEditControl1.Disposing + Word.editorControl1.richEditControl1.IsDisposed.ToString());
            }
            else if (CurrentEditor == EditorType.Spread)
            {
                CurrentEditorClose?.Invoke();
                this.Controls.Remove(Spread);
                //Spread.spread1.spreadsheetControl.Dispose();
                // Spread.spread1.ribbonControl.Dispose();
                // MessageBox.Show(Spread.spread1.spreadsheetControl.Disposing + Spread.spread1.spreadsheetControl.IsDisposed.ToString());
            }
            this.Visible = false;
        }
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {          
            try
            {
                EditorClose();
            }
            finally
            {   
                mCollection = null;
                Option.Clear();
                if (!instanceLoaded)//!this.Visible 
                {
                    this.Close();
                }                
                else
                {
                    instanceLoaded = false;
                    this.Visible = false;
                    e.Cancel = true;                    
                }
            }
        }

      
        /// <summary>
        /// Temp
        /// </summary>
        /// <returns></returns>
        public string GetResult()
        {
            /*Result result = new Result();
            Temp.richEditControl1.Document.Delete(Temp.richEditControl1.Document.Range);
            Temp.richEditControl1.Document.RtfText = editorControl1.richEditControl1.Document.RtfText;
            result.editorControl1.SetBaseColor(ResultBaseColor.Value);
            Temp.SetBlackColor();
            result.editorControl1.richEditControl1.RtfText = Temp.richEditControl1.Document.RtfText;
                result.editorControl1.richEditControl1.Document.ReplaceAll(macs[0][i].macro, macs[0][i].result, SearchOptions.CaseSensitive);
            return
                result.editorControl1.richEditControl1.Document.RtfText;*/
            return string.Empty;
        }
        public Color? GetMainColor()
        {
            if (Word!=null)
            return Word.MainBaseColor;
            else return Spread.MainBaseColor;
        }
        public Color? GetResultColor()
        {
            if (Word != null)
                return Word.MainBaseColor;
            else
                return Spread.MainBaseColor;
        }
        public void SetMainColor(Color col)
        {
            if (Word != null)
                Word.MainBaseColor = col;
            else
                Spread.MainBaseColor = col;
        }
        public void SetResultColor(Color col)
        {
            if (Word != null)
                Word.ResultBaseColor = col;
            else
                Spread.ResultBaseColor = col;
        }
        //TODO
        public void SetBaseMacroColor(Color col)
        {
            if (Word!=null)
            Word.editorControl1.SetNewMacroColor(col);
           // else
              //  Spread.
        }
        public void SetResultMacroColor(Color col)
        {
            if (Word != null)
                Word.ResultMacroColor = col;
            else
                Spread.ResultMacroColor = col;
        }       
    }
   /// <summary>
   /// Тип макроса, который посылается в документ
   /// </summary>
    public enum MacroType
    { 
        SaveDocument=0,

        CellBorderCopy = 2,
        CellCopy = 3,
        TableFunctions = 6,
        TableHeaderDub = 7,
        TableMergeCellFuncs = 89,       
        TernarLogic = 88,
        Floor = 90,
        Image = 91,
        LowerFirsteCharacterString = 92,
        UpperFirsteCharacterString = 93,
        String = 94,
        Rtf = 95,        
        CountPages = 96,
        PageNumber = 97,
        Request = 98,
        ArrContinue= 99,
        ArrayString = 100,//вначале создаём таблицу, потом всё остальное 
        ArrayWithRTF = 101,//доп аттрибуты - в массиве может находиться rtf
        ArrayWithImage = 102,// в массиве может находиться картинка
        ArrayMacro = 103,//массив макросов      
        NotOpen = 104,
        OneFile = 1,
        NotUsed = 999
    }
   
    public static class _StaticFuntion
    {
        public static List<Macroses> lst = new List<Macroses>();
        public static int ArrContinueIndex=2;
        public static void InitStaticFunction()
        {
            if (lst.Count != 0)
                return;
            lst.Clear();           
            lst.Add(new Macroses(MacroType.TableHeaderDub) { macro = "{Заголовок страницы}", result = "Заголовок" });//0                      
            lst.Add(new Macroses(MacroType.ArrContinue) { macro = "{Продолжение}", result = "Продолжение массива" });//2
            lst.Add(new Macroses(MacroType.LowerFirsteCharacterString) { macro = "{строка({макрос})}", result = "строка с маленькой буквы" });//3
            lst.Add(new Macroses(MacroType.UpperFirsteCharacterString) { macro = "{Строка({макрос})}", result = "строка с большой буквы" });//4
            lst.Add(new Macroses(MacroType.CellBorderCopy) { macro = "{Линии[номер]}", result = "какой столбец копируется" });//6
            lst.Add(new Macroses(MacroType.CellCopy) { macro = "{Дубль[номер]}", result = "дубляж одновремено с указанной колонкой" });//7
            lst.Add(new Macroses(MacroType.NotUsed) { macro = "{Разрыв страницы}", result = "вставка следующего файла" });//8
          
            lst.Add(new Macroses(MacroType.TernarLogic) { macro = "^ЕСЛИ(значение1=значение2)?'результат(истина)':'результат(ложь)'^", result = "тернарный оператор, первый результат вернёт, если истина" });//9           
            lst.Add(new Macroses(MacroType.TableMergeCellFuncs) { macro = "{Обьединить ячейки}", result = "Обьединение ячеек" });//1  
            lst.Add(new Macroses(MacroType.Floor) { macro = "{Округлить(значение;знак)}", result = "округление до знака после запятой" });//10
            lst.Add(new Macroses(MacroType.NotUsed) { macro = "{Следующая страница}", result = "вставка следующего файла с колонтитулами без привязки с предыдущим" });//11           
            lst.Add(new Macroses(MacroType.CountPages) { macro = "{Количество страниц}", result = "количество страниц" });//96
            lst.Add(new Macroses(MacroType.PageNumber) { macro = "{Номер страницы}", result = "Номер страницы" });//97           
            lst.Add(new Macroses(MacroType.NotOpen) { macro = "{Не открывать}", result = "не открывать результат после распознавания" });//98
            lst.Add(new Macroses(MacroType.SaveDocument) { macro = "{Сохранить как(\"папка\",\"Имя файла\")}", result = "куда сохранить файл" });//99     
            lst.Add(new Macroses(MacroType.SaveDocument) { macro = "{Сохранить pdf(\"папка\",\"Имя файла\")}", result = "куда сохранить файл" });//100
            lst.Add(new Macroses(MacroType.OneFile) { macro = "{Одним файлом}", result = "Принудительное формирование одного файла, макрос должен находиться в правом нижнем углу документа" });//101
        }
    }
    public class Macroses:ICloneable
    {
        public Macroses Header(Macroses item)
        {
            item.macro=item.macro.Insert(1, "Ш ");
            if (item.rowStart != 0)
            {
                string[,] arr = item.result as string[,];
                string[,] t2 = new string[item.rowStart, arr.GetLength(1)];
                Array.Copy(arr, 0, t2, 0, t2.GetLength(0)* t2.GetLength(1));
                arr = t2;
                item.result = arr;
                item.rowStart = 0;
                return item;
            }
            return null;
        }
        public object Clone()
        {
            return this.MemberwiseClone();
        }
        public Macroses Copy()
        {
            Macroses obj = new Macroses();
            obj.result = this.result;
            obj.macro = this.macro;
            obj.type = this.type;
            obj.rowStart = this.rowStart;
            return obj;
        }
        public Macroses()
        {
            type = MacroType.String;
        }
        public Macroses(bool isRtf)
        {
            type = MacroType.Rtf;
        }
        public Macroses(MacroType type)
        {
            this.type = type;
        }
        public string macro;
        public object result;      
        public MacroType type;
        public int rowStart = 0;
        public bool isRtf
        {
            set
            {
                if (value)
                    type = MacroType.Rtf;
            }
        }
        private static RichEditControl temp = new RichEditControl();
    }
}
