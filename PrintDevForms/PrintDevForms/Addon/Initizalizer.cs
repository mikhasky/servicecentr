﻿using DevExpress.XtraBars.Docking.Helpers;
using DevExpress.XtraEditors;
using DevExpress.XtraRichEdit;
using DevExpress.XtraSpreadsheet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;

namespace MKP.PrintDevForms
{
   public class Initializer
    {
        static void RunTypeInitializers(Assembly a)
        {    
            Type[] types = a.GetExportedTypes();

            for (int i = 0; i < types.Length; i++)
            {
                RuntimeHelpers.RunClassConstructor(types[i].TypeHandle);
            }
        }
        private static bool isOptimized = false;
        public static void StartOptimization()
        {
            if (isOptimized)
                return;
            isOptimized = true;
            // RunTypeInitializers(Assembly.GetAssembly(typeof(DevExpress.Wpf.Core.Native.LayoutHelper))); //Core
            try
            {
                RunTypeInitializers(Assembly.GetAssembly(typeof(SpreadDocument))); //SpreadDocument

                RunTypeInitializers(Assembly.GetAssembly(typeof(TextEdit))); //Editors

                RunTypeInitializers(Assembly.GetAssembly(typeof(DockLayoutManager))); //Docking

                RunTypeInitializers(Assembly.GetAssembly(typeof(RichTextEdit))); //RichTextEdit

                RunTypeInitializers(Assembly.GetAssembly(typeof(SpreadsheetControl))); //SpreadsheetControl

                RunTypeInitializers(Assembly.GetAssembly(typeof(RichEditControl))); //RichEditControl
            }
            catch { }

            
        }
    }
}
