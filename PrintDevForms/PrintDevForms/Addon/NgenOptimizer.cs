﻿using System;
using System.Collections.Generic;
using System.Deployment.Application;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace MKP.PrintDevForms.Addon
{
   public static class NgenOptimizer
    {
        public static string GetOSBit()
        {
            bool is64bit = Is64Bit();
            if (is64bit)
                return "x64";
            else
                return "x32";
        }

        [DllImport("kernel32.dll", SetLastError = true, CallingConvention = CallingConvention.Winapi)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool IsWow64Process([In] IntPtr hProcess, [Out] out bool lpSystemInfo);

        public static bool Is64Bit()
        {
            bool retVal;
            IsWow64Process(Process.GetCurrentProcess().Handle, out retVal);
            return retVal;
        }
        public static void Optimize()
        {            
           // if (ApplicationDeployment.IsNetworkDeployed && ApplicationDeployment.CurrentDeployment.IsFirstRun)
            {
                string appPath = Application.StartupPath;
                string winPath = Environment.GetEnvironmentVariable("WINDIR");
                MessageBox.Show(appPath);
                MessageBox.Show(Application.ProductName);
                Process proc = new Process();
                System.IO.Directory.SetCurrentDirectory(appPath);

                proc.EnableRaisingEvents = false;
                proc.StartInfo.CreateNoWindow = false;
                proc.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                string bits = Is64Bit()?"64":"";
                proc.StartInfo.FileName = winPath + @"\Microsoft.NET\Framework"+ bits + "\v4.0.30319\ngen.exe";
                proc.StartInfo.Arguments = "uninstall " + Application.ProductName + " /nologo /silent";

                proc.Start();
                proc.WaitForExit();

                proc.StartInfo.FileName = winPath + @"\Microsoft.NET\Framework"+ bits + "\v4.0.30319\ngen.exe";
                proc.StartInfo.Arguments = "install " + Application.ProductName + " /nologo /silent";

                proc.Start();
                proc.WaitForExit();

            }
        }
    }
}
