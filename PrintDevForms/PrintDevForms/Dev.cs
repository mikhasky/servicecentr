﻿using DevExpress.XtraRichEdit.API.Native;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MKP.PrintDevForms
{
    static class Dev
    {
        public static StringBuilder sb = new StringBuilder();
        public static int GetTableIndex(TableCollection TC,Table T)
        {
            return (TC.IndexOf(T));
        }
        public static List<Macroses[]> macroInit(object macro)
        {
            List<Macroses[]> lst = new List<Macroses[]>();
            if (macro is List<Macroses>)
            {
                macro = (macro as List<Macroses>).OrderByDescending(i => (int)i.type).ToList();                
                for (int i = 0; i < (macro as List<Macroses>).Count - 0; i++)
                {
                    sb.Clear();
                    (macro as List<Macroses>)[i].macro = sb.Append("{").Append((macro as List<Macroses>)[i].macro).Append("}").ToString();
                    if ((macro as List<Macroses>)[i].type == MacroType.ArrayString && (macro as List<Macroses>)[i].rowStart > 0)
                    {
                        Macroses item = (macro as List<Macroses>)[i].Header((macro as List<Macroses>)[i].Copy());
                        if (item != null)
                        {
                            (macro as List<Macroses>).Insert(i, item); i += 1;
                        }
                    }
                }
                lst.Add((macro as List<Macroses>).ToArray());
            }
            else if (macro is List<Macroses[]>)
            {
                lst = macro as List<Macroses[]>;
                for (int j = 0; j < lst.Count; j++)
                {
                    lst[j] = lst[j].OrderByDescending(i => (int)i.type).ToArray();
                    int n = 0;
                    if (j != 0)
                        for (int i = 0; i < lst[j].Length - n; i++)
                        {
                            sb.Clear();
                            lst[j][i].macro = sb.Append("{").Append(lst[j][i].macro).Append("}").ToString();
                            if (lst[j][i].type == MacroType.ArrayString && lst[j][i].rowStart > 0)
                            {
                                Macroses item = lst[j][i].Header(lst[j][i].Copy());
                                if (item != null)
                                {
                                    List<Macroses> macs = lst[j].ToList();
                                    macs.Insert(i, item);
                                    lst[j] = macs.ToArray();
                                    i += 1;
                                }
                            }
                        }
                    else
                    {
                        for (int i = 0; i < lst[j].Length - n; i++)
                        {
                            sb.Clear();
                            lst[j][i].macro = sb.Append("{").Append(lst[j][i].macro).Append("}").ToString();
                            if (lst[j][i].type == MacroType.ArrayString && lst[j][i].rowStart > 0)
                            {
                                Macroses item = lst[j][i].Header(lst[j][i].Copy());
                                if (item != null)
                                {
                                    List<Macroses> macs = lst[j].ToList();
                                    macs.Insert(i, item);
                                    lst[j] = macs.ToArray();
                                    i += 1;
                                }
                            }
                        }
                    }
                }
            }
            else if (macro is Macroses[])
            {
                lst.Add((macro as Macroses[]));
                for (int j = 0; j < lst.Count; j++)
                {
                    lst[j] = lst[j].OrderByDescending(i => (int)i.type).ToArray();
                    int n = 0;
                    for (int i = 0; i < lst[j].Length - n; i++)
                    {
                        sb.Clear();
                        lst[j][i].macro = sb.Append("{").Append(lst[j][i].macro).Append("}").ToString();
                        if (lst[j][i].type == MacroType.ArrayString)
                        {
                            Macroses item = lst[j][i].Header(lst[j][i].Copy());
                            if (item != null)
                            {
                                List<Macroses> macs = lst[j].ToList();
                                macs.Insert(i, item);
                                lst[j] = macs.ToArray();
                                i += 1;
                                // Array.Resize<Macroses>(ref macs, macs.Length + 1);
                                //macs[macs.Length - 1] = item;
                                // n++;
                            }
                        }
                    }
                }
            }
            else if (macro is List<List<Macroses>>)
            {
                var sm = (macro as List<List<Macroses>>);
                for (int i = 0; i < sm.Count; i++)
                    lst.Add(sm[i].ToArray());
                for (int j = 0; j < lst.Count; j++)
                {
                    lst[j] = lst[j].OrderByDescending(i => (int)i.type).ToArray();
                    int n = 0;
                    if (j!=0)
                    for (int i = 0; i < lst[j].Length - n; i++)
                    {
                        sb.Clear();
                        lst[j][i].macro = lst[0][i].macro;
                        if (lst[j][i].type == MacroType.ArrayString)
                        {
                            Macroses item = lst[j][i].Header(lst[j][i].Copy());
                            if (item != null)
                            {
                                List<Macroses> macs = lst[j].ToList();
                                macs.Insert(i, item);
                                lst[j] = macs.ToArray();
                                i += 1;
                            }
                        }
                    }
                    else
                        for (int i = 0; i < lst[j].Length - n; i++)
                        {
                            sb.Clear();
                            lst[j][i].macro = sb.Append("{").Append(lst[j][i].macro).Append("}").ToString();
                            if (lst[j][i].type == MacroType.ArrayString)
                            {
                                Macroses item = lst[j][i].Header(lst[j][i].Copy());
                                if (item != null)
                                {
                                    List<Macroses> macs = lst[j].ToList();
                                    macs.Insert(i, item);
                                    lst[j] = macs.ToArray();
                                    i += 1;
                                }
                            }
                        }
                }
            }
            return lst;
        }
    }
}
