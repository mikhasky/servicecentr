﻿using System.Collections.Generic;
using System.Runtime.Serialization;
namespace MKP.PrintDevForms.Options
{
    public static class Option
    {
        public enum ScanDocumentType
        {
            Document=0,SubDocument=1
        }
        public static void Clear()
        {
            SavePath = null;
            FileName = null;
            IdScript = null;
            memory = null;
            macs = new List<Macroses[]>();
        }
        public static string separator = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
        public static int UserId { get; set; }
        public static string SavePath { get; set; }
        public static string FileName { get; set; }
        public static string IdScript { get; set; }
        public static string ConBD { get; set; }= @"User = MKP;Password = 17051945;Database = E:\BD\BD;DataSource = 127.0.0.1;Port = 3050;Dialect = 3;Charset = WIN1251;Role =;Connection lifetime = 15;Pooling = true;MinPoolSize = 0;MaxPoolSize = 50;Packet Size = 8192;ServerType = 0";
        public static string ConBDD { get; set; }= @"User = MKP;Password = 17051945;Database = E:\BD\BDD;DataSource = 127.0.0.1;Port = 3050;Dialect = 3;Charset = WIN1251;Role =;Connection lifetime = 15;Pooling = true;MinPoolSize = 0;MaxPoolSize = 50;Packet Size = 8192;ServerType = 0";
        public static string memory { get; set; }
        public static byte[] SpreadMemory { get; set; }
        public static List<Macroses[]> macs { get; set; } = new List<Macroses[]>();
        public static DocumentOptions doc=new DocumentOptions();
    }
    [DataContract]
    public class DocumentOptions
    {
        [DataMember]
        public int formi9;//1 f11,2-f12
        internal List<int> idMacroRun=new List<int>();
        internal List<int> idMacroSubDocumentRun=new List<int>();
       
    }
    public enum ModeStart
    {
        Печать=0,
        Вызвать_диалог_печати=1,
        Вызвать_сразу_предпросмотр=2,
        Сохранить_результат_в_файл=3,
        Сохранить_как=4,
        Стандартный_режим=5,
        Моментальный_результат_после_загрузки=6,
        Вызов_результата_распознавания_без_показа=7
    }
}
