﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace MKP.PrintDevForms
{
    public static class Generate
    {
        public const string patterCommon = @"\{(\s?[a-zA-Z0-9А-ЯЁа-яё. ()\/';:.,*&^%$#@!=+_?№-])*\s?}";
        public static Regex CommonRegex { get; private set; } = new Regex(patterCommon, RegexOptions.CultureInvariant | RegexOptions.Compiled);
        public const string patterCommonShort = @"\{(.*)\}";
        public const string pattern1 = @"\[\d+\,\d+\]";
        public const string pattern2 = @"\[\*?(\-)?\*?\*?\d+\*?(Об)?\d*\*?\]";//\[((\d*)|(\-\d*))\];
        //\[((\О\б)?(\-)?(\О\б)?\d+(\О\б)?)\]
        public const string pattern3 = @"(\[.+\-.+\])"; //@"(\[((\W*?\w*)?.*)\-(\W*?\w*?)?.*\])";
        public const string patternMergedColumn = @"Столб\(\d*\)";
        public const string patternTernarLogic = @"\^ЕСЛИ\(.+\^"; //@"\^ЕСЛИ\(((\W*?\w*)?.*)\=((\W*?\w*)?.*)\)\?\'((\W*?\w*)?.*)\'\:\'((\W*?\w*)?.*)\'\^";
        public const string patternAlternateTernarLogic = @"\^ЕСЛИ(\W*?\w*?)*[\n\r]*\^";
        public static char[] splitLogic = new char[] { '=','<','>' };
        private const string garbage1 = "\r\n\r\n";
        private const string garbage2 = "\r\n";
        private const string space = " ";
        private const string star = "*";
        private const string merge="Об";
        private const string minus = "-";
        private const string ObText = "ОбТекста";
        public readonly static Regex Ternar=new Regex(Generate.patternTernarLogic, RegexOptions.CultureInvariant|RegexOptions.Multiline|RegexOptions.Compiled);
        private readonly static char[] splited = new char[] { '[', ']', '-', ',' };
        private readonly static Regex getDigit=new Regex(@"(\d.*)"); //([0-9].)
        public static string GetResultLogic(string logic, string[] Comparsion,string[] Values)
        {
            double a, b;
            switch (logic)
            {
                case "=": return Comparsion[0] == Comparsion[1] ? Values[0] : Values[1];
                case "<":
                    Comparsion[0] = RemoveErrorValue(Comparsion[0]);
                    Comparsion[1] = RemoveErrorValue(Comparsion[1]);                    
                    if (double.TryParse(Comparsion[0], out a) && double.TryParse(Comparsion[1], out b))
                        return a < b ? Values[0] : Values[1];
                    else return Values[1];
                case ">":
                    Comparsion[0] = RemoveErrorValue(Comparsion[0]);
                    Comparsion[1] = RemoveErrorValue(Comparsion[1]);                   
                    if (double.TryParse(Comparsion[0], out a) && double.TryParse(Comparsion[1], out b))
                        return a > b ? Values[0] : Values[1];
                    else return Values[1];
                default: return "Оператор не найден";
            }
        }
        private static string RemoveErrorValue(string value)
        {
            return !string.IsNullOrEmpty(value)?value.Replace(" ", string.Empty):value;
        }
        public static string GetLogic(string range)
        {
            for (int i = 0; i < splitLogic.Length; i++)
            {
                if (range.Contains(splitLogic[i]))
                    return splitLogic[i].ToString();
            }
            return string.Empty;
        }
        public static void ClearText(ref string txt)
        {
            if (txt.Contains(garbage1))
            {
                txt = txt.Remove(txt.IndexOf(garbage2));
            }
        }        
        /// <summary>
        /// Обьект, хранящий в себе текст
        /// </summary>
        public class TextOption
        {
            public TextOption(string txt,bool OneValue=false)
            {
                ClearText(ref txt);
                textOriginnal = txt;
                workedText = textOriginnal.Replace(space, string.Empty).Replace(" ",string.Empty);
                if (workedText.Contains(star))
                {
                    columnInsert = false;
                    workedText = workedText.Replace(star, string.Empty);
                }
                if (workedText.Contains(ObText))
                {
                    ObTextIs = true;
                    bool min = false;
                    string t = workedText.Remove(0, workedText.IndexOf(ObText) + ObText.Length);
                    string replace = ObText;
                    if (t[0] == '-')
                    {
                        min = true;
                        replace += '-';
                        t = t.Remove(0, 1);                      
                    }
                    string num = string.Empty;
                    for (int i = 0; i < t.Length; i++)
                    {
                        if (!char.IsDigit(t[i]))
                        {
                            workedText = workedText.Replace(replace, string.Empty);
                            ObRowIndex = min?-Convert.ToInt32(num):Convert.ToInt32(num);
                            break;
                        }
                        num += t[i];
                        replace += t[i];
                    }
                }
                if (workedText.Contains(merge))
                {
                    Merge = true;
                    for (int m = workedText.IndexOf(merge) + 2; m < workedText.Length; m++)
                    {
                        if (!char.IsDigit(workedText[m]))
                            break;
                        mData += workedText[m];
                    }
                    if (mData.Length != 0)
                    {
                        workedText = workedText.Replace(merge+mData, string.Empty);
                        AnalizeCells = true;
                        mDataCount = int.Parse(mData);
                       mData = "Столб(" + mData + ")";                       
                    } else
                    workedText = workedText.Replace(merge, string.Empty);
                }
                if (OneValue)
                if (workedText.Contains(minus))
                {
                    Minus = true;
                    workedText = workedText.Replace(minus, string.Empty);
                }
                workedText = workedText.Remove(0, workedText.IndexOf('['));               
                indexes = workedText.Split(splited, StringSplitOptions.RemoveEmptyEntries);              
                values = new int[indexes.Length];
                for (int i = 0; i < values.Length; i++)
                {
                    MatchCollection match = getDigit.Matches(indexes[i]);
                    if (match.Count > 0)
                        values[i] =Convert.ToInt32(match[0].Value);
                }
                if (values.Length == 3)
                    OneRow = true;
                txt = workedText;
            }
            public bool AnalizeCells = false;
            public string mData = string.Empty;
            public int mDataCount = 0;
            /// <summary>
            /// Оригинальный чистый текст для замены
            /// </summary>
            public string textOriginnal;
            /// <summary>
            /// Разрешить ли вставку новых колонок
            /// </summary>
            public bool columnInsert = true;
            public bool OneRow = false;
            /// <summary>
            /// рабочий текст
            /// </summary>
            private string workedText;
            public string[] indexes;
            public int[] values;
            public bool Merge = false;
            public bool Minus = false;
            public bool ObTextIs = false;
            public int ObRowIndex = -1;
        }        
    }
}
