﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraRichEdit;
using DevExpress.XtraSpreadsheet;

namespace MKP.PrintDevForms.Forms
{
    public partial class DynControl : UserControl
    {
        public SpreadEditor.Spread spread = new SpreadEditor.Spread();
        public EditorControl word = new EditorControl();
        public EditorType edt;
        public DynControl()
        {
            InitializeComponent();
            spread.Visible = false;
            word.Visible = false;
            spread.Dock = DockStyle.Fill;
            word.Dock = DockStyle.Fill;
            this.Controls.Add(spread);
            this.Controls.Add(word);
        }        
        public void LoadDocument(object file, EditorType edt)
        {
            this.edt = edt;
            switch (edt)
            {
                case EditorType.Spread:
                    word.Visible = false;
                    spread.Visible = true;                    
                    spread.spreadsheetControl.Document.LoadDocument((file as SpreadsheetControl).Document.SaveDocument(DevExpress.Spreadsheet.DocumentFormat.Xls), DevExpress.Spreadsheet.DocumentFormat.Xls);
                    break;
                case EditorType.Word:
                    spread.Visible = false;
                    word.Visible = true;
                    word.richEditControl1.RtfText = (file as RichEditControl).RtfText;
                    break;
            }

        }
       
    }
}
