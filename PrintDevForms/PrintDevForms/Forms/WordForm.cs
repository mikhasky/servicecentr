﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MKP.PrintDevForms.Options;
using DevExpress.XtraRichEdit;
using DevExpress.XtraRichEdit.API.Native;
using FirebirdSql.Data.FirebirdClient;
using DevExpress.XtraRichEdit.Commands;
using System.IO;
using System.Text.RegularExpressions;
using DevExpress.XtraRichEdit.API.Layout;
using MKP.PrintDevForms.logs;
using DevExpress.XtraRichEdit.API.Native.Implementation;
using DevExpress.XtraRichEdit.Export;

namespace MKP.PrintDevForms
{
    public delegate void RecognizedWordDelegate(EditorType type, object result);
    public partial class WordForm : UserControl
    {
        public string FileName = string.Empty;
        public Color? MainBaseColor = Color.Black;
        public Color? ResultBaseColor = Color.Black;
        public Color ResultMacroColor = Color.FromArgb(213, 213, 255);
        EditorControl Temp = new EditorControl();
        RichEditControl rtf = new RichEditControl();
        public bool isPrintable;
        public event RecognizedWordDelegate RecognozeFinished;
        public WordForm()
        {
            InitializeComponent();           
            
            Macroses[] test = new Macroses[1];
           
             string[,] tabl = new string[250, 3];
            for (int i = 0; i < 250; i++)
            {
                for (int j = 0; j < 3; j++)
                {                    
                        tabl[i, j] = "i="+ i + ", j=" + j;
                }
            }
           
            test[0] = new Macroses(MacroType.ArrayString) { macro = "табличко", result = tabl, rowStart = 1 };
         
            List<Macroses[]> macro = new List<Macroses[]>();
            macro.Add(test);
         
            Option.macs = Dev.macroInit(macro);
            isPrintable = true;
            Option.IdScript = "-1"; Option.macs = macro;
            List<string> mas = new List<string>();
            _StaticFuntion.InitStaticFunction();
            for (int i = 0; i < _StaticFuntion.lst.Count; i++)
                dgPForms.Rows.Add(_StaticFuntion.lst[i], _StaticFuntion.lst[i].macro, _StaticFuntion.lst[i].result);
            for (int i = 0; i < macro[0].Length; i++)
            {
                dgPForms.Rows.Add((int)macro[0][i].type, macro[0][i].macro, macro[0][i].result);
                mas.Add(macro[0][i].macro);
            }
           
            editorControl1.ribbonControl1.Minimized = false;
            editorControl1.DrawMacroses = true;
        
        }
        public ModeStart mode=ModeStart.Стандартный_режим;
        public void LoadWordForm(object macro, string idScript, string ConBD, string ConBDD, ModeStart mode, int UserID, string fastSaveFullPath = "")
        {
            Option.IdScript = idScript;
            Option.ConBD = ConBD;
            Option.ConBDD = ConBDD;
            Option.SavePath = fastSaveFullPath;
            Option.UserId = UserID;
            Option.macs = Dev.macroInit(macro);
            this.mode = mode;
            using (FbConnection fbCon = new FbConnection(Option.ConBDD))
            {
                fbCon.Open();
                using (FbCommand fbCom = new FbCommand("SELECT formi7,formi8,formi14 FROM formi WHERE formi1 = @p1", fbCon))
                {
                    fbCom.Parameters.Add("@p1", Option.IdScript);
                    using (FbDataReader fbDr = fbCom.ExecuteReader())
                    {
                        if (fbDr.Read())
                        {
                            if (fbDr["formi14"] != DBNull.Value) Option.doc = MKP.PrintDevForms.Options.Json.Json.Deserialize<Options.DocumentOptions>(Encoding.UTF8.GetString((byte[])fbDr["formi14"]));
                            else
                                scanned = false;
                            if (!scanned)
                                ScanMacro();
                            if (fbDr["formi8"] != DBNull.Value)
                            {
                                editorControl1.richEditControl1.Document.RtfText = System.Text.Encoding.Default.GetString((byte[])fbDr["formi8"]);
                            }
                            Option.memory = editorControl1.richEditControl1.Document.RtfText;
                            button1.BackColor = Color.FromArgb(81, 206, 109);
                        }
                    }
                }
            }
            dgPForms.Rows.Clear();
            for (int i = 0; i < _StaticFuntion.lst.Count; i++)
                dgPForms.Rows.Add(_StaticFuntion.lst[i], _StaticFuntion.lst[i].macro, _StaticFuntion.lst[i].result);
            if (Option.macs.Count > 0)
                for (int i = 0; i < Option.macs[0].Length; i++)
                    dgPForms.Rows.Add((int)Option.macs[0][i].type, Option.macs[0][i].macro, Option.macs[0][i].result);
            StopChange = true;
            StopChange = false;
            if (mode != ModeStart.Стандартный_режим)
                DocPrint((int)mode);
            else
                editorControl1.ribbonControl1.Minimized = false;
        }
        public WordForm(bool logged)
        {
            InitializeComponent();
            //initThreads();
            _StaticFuntion.InitStaticFunction();   
            editorControl1.DrawMacroses = true;
        }
        bool scanned = true;
        private void change(object sender, EventArgs e)
        {
            PACKPACKA.Stop();
            timer1.Stop();
            if (!StopChange)
            {
                PACKPACKA.Start();
            }
            else
                timer1.Start();
            ScanMacro();
        }
        private void ScanMacro()
        {
            reScanMacro = true;
        }
        System.Threading.Thread trMacroLive;
        private volatile bool reScanMacro = false;
        private volatile bool isScanned = false;
        //базовая инициализация потоков
        private void initThreads()
        {
            trMacroLive = (new System.Threading.Thread(() =>
            {
                while (true)
                {
                    if (!reScanMacro)
                    {
                        System.Threading.Thread.Sleep(1000);
                        continue;
                    }
                    //как только меняем занчение на false - каждый раз перезапуск сканирования
                    reScanMacro = false;
                    Option.doc.idMacroRun = FindWorkMacro();
                }
            }
                )
                );
            trMacroLive.Start();
        }
        private List<int> FindWorkMacro()
        {
            isScanned = true;
            List<int> idMacroRun = new List<int>();
            if (Option.macs.Count() == 0)
                return idMacroRun;
            int leng = Option.macs[0].Count();
                idMacroRun.AddRange(FindParallel(leng, editorControl1.richEditControl1));
            isScanned = false;
            return idMacroRun;
        }
        //TODO
       /* private List<int> FindCurrentMacroses()
        {
            List<int> idMacroRun = new List<int>();
            if (Option.macs.Count() == 0)
                return idMacroRun;
            foreach (var item in editorControl1.richEditControl1.Document.FindAll(new Regex(Generate.patterCommon)))
            {

            }
            throw new Exception();
        }*/

        private List<int> FindParallel(int leng, RichEditControl REC)
        {
            List<int> idMacroRun = new List<int>();
            try
            {
                System.Threading.Tasks.Parallel.For(0, leng, i =>
                {

                    if (REC.Document.FindAll(Option.macs[0][i].macro, SearchOptions.CaseSensitive).Count() > 0)
                        idMacroRun.Add(i);
                });
            }
            catch { }
            return idMacroRun;

        }
        private void Packa(Color MacroColor, Color Base,bool isOriginal)
        {            
            CharacterProperties cp = null;
            if (!isOriginal)
            {
                cp = result.editorControl1.richEditControl1.Document.BeginUpdateCharacters(result.editorControl1.richEditControl1.Document.Range);
                cp.ForeColor = Base;
                result.editorControl1.richEditControl1.Document.EndUpdateCharacters(cp);
            }
            else
            {
                cp = editorControl1.richEditControl1.Document.BeginUpdateCharacters(editorControl1.richEditControl1.Document.Range);
                cp.ForeColor = Base;
                editorControl1.richEditControl1.Document.EndUpdateCharacters(cp);
            }
            /*
            if (!isOriginal)
                for (int i = 0; i < result.editorControl1.richEditControl1.Document.Length; i++)
                {
                    cp = result.editorControl1.richEditControl1.Document.BeginUpdateCharacters(result.editorControl1.richEditControl1.Document.CreateRange(i, 1));
                    if (cp.ForeColor == MacroColor)
                        if (Base != MacroColor)
                            cp.ForeColor = Base;
                    result.editorControl1.richEditControl1.Document.EndUpdateCharacters(cp);
                }
            else
            {               
                    for (int i = 0; i < editorControl1.richEditControl1.Document.Length; i++)
                    {
                        cp = editorControl1.richEditControl1.Document.BeginUpdateCharacters(editorControl1.richEditControl1.Document.CreateRange(i, 1));
                        if (cp.ForeColor == MacroColor)
                            if (Base != MacroColor)
                                cp.ForeColor = Base;
                        editorControl1.richEditControl1.Document.EndUpdateCharacters(cp);
                    }                          
            }*/
        }       
        public void button1_Click(object sender, EventArgs e)
        {
            Option.memory = editorControl1.richEditControl1.Document.RtfText;
            byte[] array = Encoding.Default.GetBytes(Option.memory);
            button1.BackColor = Color.FromArgb(81, 206, 109);
            using (FbConnection fConnection = new FbConnection(Option.ConBDD))
            {
                fConnection.Open();
                using (FbTransaction fTrans = fConnection.BeginTransaction())
                {
                    using (FbCommand fCommand = new FbCommand(@"UPDATE formi 
                        SET formi8 = @p2, formi9=1,formi14=@formi14 WHERE formi1 = @p1", fConnection, fTrans))
                    {
                        fCommand.Parameters.Add("@p2", array);
                        fCommand.Parameters.Add("@p1", Option.IdScript);
                        fCommand.Parameters.Add("@formi14", Encoding.UTF8.GetBytes(MKP.PrintDevForms.Options.Json.Json.Serialize<Options.DocumentOptions>(Option.doc)));
                        fCommand.ExecuteNonQuery();
                        fTrans.Commit();
                    }
                }
            }
        }
        public Result result;
        int DocumentPageSumm { get; set; } = 0;
        int DocumentPageCount { get; set; } = 0;
        int PageDecrement { get; set; } = 0;
        bool DocumentColontitulesSetOriginal { get; set; } = false;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="type">0-сразу на печать, 1 - диалог печати, 2 - предпросмотр</param>
        public void DocPrint(int type)
        {
            DocumentColontitulesSetOriginal = false;
            DocumentPageCount = 0;
            DocumentPageSumm = 0;
            PageDecrement = 0;
            Option.doc.idMacroRun = new List<int>();
            Option.doc.idMacroSubDocumentRun = new List<int>();
          //  while (isScanned || reScanMacro)
            //    System.Threading.Thread.Sleep(200);
            bool open = true;
            bool rtfRestartMode = false;
            bool nextPage = false;             
            //GC.Collect();
            Errors.err = new logs.Errors();
            Collection.cells = new List<TableCell>();
            CollectionInfo.RowInserted.Clear();
            result = new Result();
            Temp.richEditControl1.Document.Delete(Temp.richEditControl1.Document.Range);
            Temp.richEditControl1.Document.RtfText = editorControl1.richEditControl1.Document.RtfText;
            if (Temp.richEditControl1.Document.Text.Contains("{Следующая страница}"))
            {
                nextPage = true;
                Temp.richEditControl1.Document.ReplaceAll("{Следующая страница}", string.Empty, SearchOptions.CaseSensitive);
            }
            
           /* CharacterProperties cp = Temp.richEditControl1.Document.BeginUpdateCharacters(Temp.richEditControl1.Document.Range);
            cp.ForeColor = Color.Black;
            Temp.richEditControl1.Document.EndUpdateCharacters(cp);
            foreach (var item in Temp.richEditControl1.Document.Sections)
            {
                var header = item.BeginUpdateHeader();
                cp =header.BeginUpdateCharacters(header.Range);
                cp.ForeColor = Color.Black;
                header.EndUpdateCharacters(cp);
                item.EndUpdateHeader(header);

                var footer = item.BeginUpdateFooter();
                cp = footer.BeginUpdateCharacters(footer.Range);
                cp.ForeColor = Color.Black;
                footer.EndUpdateCharacters(cp);
                item.EndUpdateFooter(footer);
            }*/
            
            // Temp.SetBlackColor();
            result.editorControl1.richEditControl1.RtfText = Temp.richEditControl1.Document.RtfText;
           // Packa(ResultMacroColor, ResultBaseColor.Value,false);
            RichEditControl addDocument = new RichEditControl();
            result.editorControl1.richEditControl1.BeginUpdate();            
            int len = 0;
            //DELETE?
            Option.doc.idMacroRun.Clear();
            //END
            if (Option.macs != null)
            {
                if (Option.macs.Count > 0)
                {
                    len = Option.macs[0].Length;
                    addDocument.Document.RtfText = result.editorControl1.richEditControl1.Document.RtfText;
                    ScanActiveMacro();
                    //Document.FindAll(Option.macs[0][i].macro, SearchOptions.CaseSensitive)
                }
               // int EndIndex = 0;
                //if (Option.doc.idMacroRun.Count > 0)
                  //  EndIndex = Option.doc.idMacroRun.Max();               
                for (int j = 0; j < Option.macs.Count; j++)
                {
                    DocumentColontitulesSetOriginal = false;
                    if (j != 0)
                    {
                        if (editorControl1.richEditControl1.Document.Text.Contains("{Новый файл}"))
                        {
                            result.editorControl1.richEditControl1.Document.RtfText=addDocument.Document.RtfText;
                            result.editorControl1.richEditControl1.Document.ReplaceAll("{Новый файл}", string.Empty, SearchOptions.CaseSensitive);
                        }
                        else
                            if (!result.editorControl1.richEditControl1.Document.Text.Contains("{Разрыв страницы}"))
                        {
                             InsertPageBreakCommand command = new InsertPageBreakCommand(result.editorControl1.richEditControl1);
                              command.Execute();
                            {
                                if (nextPage)
                                {
                                    SendNextPage(addDocument);
                                }else
                                result.editorControl1.richEditControl1.Document.InsertRtfText(result.editorControl1.richEditControl1.Document.Range.End, addDocument.Document.RtfText);
                            }
                        }                        
                        else 
                        {
                            DocumentPosition pos = result.editorControl1.richEditControl1.Document.FindAll("{Разрыв страницы}", SearchOptions.CaseSensitive)[0].Start;
                            result.editorControl1.richEditControl1.Document.ReplaceAll("{Разрыв страницы}", string.Empty, SearchOptions.CaseSensitive);
                           
                          var text=  result.editorControl1.richEditControl1.Document.Text;
                            if (nextPage)
                            {
                                SendNextPage(addDocument);
                            }else
                            result.editorControl1.richEditControl1.Document.InsertRtfText(pos, addDocument.Document.RtfText);
                        }
                        CollectionInfo.RowInserted.Clear();
                        CollectionInfo.currTable = null;
                    }

                    do
                    {
                        rtfRestartMode = false;
                        int CommonLength = Option.macs[j].Length;
                        for (int i = 0; i < CommonLength; i++)
                        {
                            if (!Option.doc.idMacroRun.Contains(i)&&!Option.doc.idMacroSubDocumentRun.Contains(i))
                                continue;
                            if (Option.macs[j][i].result == null)
                                continue;
                            switch (Option.macs[j][i].type)
                            {
                                case MacroType.String:
                                    MacroText(j, i);
                                    break;
                                case MacroType.Rtf:
                                    rtfRestartMode = !MacroRTF(result, j, i);
                                    ScanActiveMacro();
                                    break;
                                case MacroType.Image:
                                    if (Option.macs[j][i].result is Image)
                                    {
                                        if (Option.doc.idMacroRun.Contains(i))
                                        {
                                            _ImageReplace(result.editorControl1.richEditControl1.Document, j, i);
                                        }
                                        if (Option.doc.idMacroSubDocumentRun.Contains(i))
                                        {
                                            foreach (var section in result.editorControl1.richEditControl1.Document.Sections)
                                            {
                                                var header = section.BeginUpdateHeader();
                                                _ImageReplace(header, j, i);
                                                section.EndUpdateHeader(header);

                                                var footer = section.BeginUpdateFooter();
                                                _ImageReplace(footer, j, i);
                                                section.EndUpdateFooter(footer);
                                            }
                                        }
                                    }
                                    break;
                                case MacroType.ArrayString:
                                    if (!(Option.macs[j][i].result is string[,]))
                                        continue;
                                    ArrayFunc(j, i, Option.macs[j][i].type);
                                    break;
                                case MacroType.ArrayWithRTF:
                                    if (!(Option.macs[j][i].result is string[,]))
                                        continue;
                                    ArrayFunc(j, i, Option.macs[j][i].type);
                                    break;
                            }
                            result.editorControl1.richEditControl1.Document.CaretPosition = result.editorControl1.richEditControl1.Document.Range.End;
                        }
                    } while (rtfRestartMode);
                        
                    // result.editorControl1.richEditControl1.EndUpdate();
                    open= SpecialMacro(result);
                    EndAnalizeResult(result);                   
                }
                result.editorControl1.richEditControl1.Document.ReplaceAll("{Разрыв страницы}", string
                       .Empty, SearchOptions.CaseSensitive);
                if (type == 2)
                {
                    result.editorControl1.richEditControl1.EndUpdate();
                    if (open)
                        result.Show();
                    else
                    {
                        result.Close();
                        MessageBox.Show("Выполнено!");
                    }
                    RecognozeFinished?.Invoke(EditorType.Word, result.editorControl1.richEditControl1);
                    // if (!isPrintable)
                    //   this.Close();
                }
                else
                    if (type == 1)
                {
                    result.editorControl1.richEditControl1.EndUpdate();
                    result.editorControl1.richEditControl1.ShowPrintDialog();
                }
                else if (type == 0)
                {
                    result.editorControl1.richEditControl1.EndUpdate();
                    result.editorControl1.richEditControl1.Print();
                }
                else if (type == 3)
                {
                    result.editorControl1.richEditControl1.EndUpdate();
                    result.editorControl1.richEditControl1.SaveDocument(FileName, DocumentFormat.Doc);
                }
                else
                    if (type == 4)
                {
                    result.editorControl1.richEditControl1.EndUpdate();
                    result.editorControl1.richEditControl1.SaveDocumentAs();
                }
                else
                    if (type == 6)
                {
                    result.editorControl1.richEditControl1.EndUpdate();
                    RecognozeFinished?.Invoke(EditorType.Word, result.editorControl1.richEditControl1);
                    result.Close();
                }
                else if (type == 7)
                {
                    result.editorControl1.richEditControl1.EndUpdate();
                    RecognozeFinished?.Invoke(EditorType.Word, result.editorControl1.richEditControl1);
                    result.Close();
                }
                else
                    result.editorControl1.richEditControl1.EndUpdate();
            }
            else
                result.editorControl1.richEditControl1.EndUpdate();
            if (Errors.err.lst.Count > 0)
                new ErrorLog(Errors.err).Show();
        }
        private void ArrayFunc(int j,int i,MacroType typeArray)
        {            
            if (Option.doc.idMacroRun.Contains(i))
                MacroArrString(result.editorControl1.richEditControl1.Document, j, i,typeArray);

            if (Option.doc.idMacroSubDocumentRun.Contains(i))
                foreach (var section in result.editorControl1.richEditControl1.Document.Sections)
                {
                    var header = section.BeginUpdateHeader();
                    MacroArrString(header, j, i, typeArray);
                    section.EndUpdateHeader(header);

                    var footer = section.BeginUpdateFooter();
                    MacroArrString(footer, j, i, typeArray);
                    section.EndUpdateFooter(footer);
                }
        }
        private void _ImageReplace(SubDocument sub,int j, int i)
        {
            foreach (DocumentRange range in sub.FindAll(Option.macs[j][i].macro, SearchOptions.CaseSensitive))
            {
                sub.Images.Insert(range.Start, Option.macs[j][i].result as Image);
            }
            sub.ReplaceAll(Option.macs[j][i].macro, string.Empty, SearchOptions.CaseSensitive);
        }

        private void SendNextPage(RichEditControl addDocument)
        {
            //result.editorControl1.richEditControl1.Document.InsertRtfText(result.editorControl1.richEditControl1.Document.Range.End, addDocument.Document.RtfText);
            for (int i = 0; i < addDocument.Document.Sections.Count; i++)
            {               
                result.editorControl1.richEditControl1.Document.AppendSection();
                Section _section = result.editorControl1.richEditControl1.Document.Sections[result.editorControl1.richEditControl1.Document.Sections.Count - 1];
                if (i==0)
                    result.editorControl1.richEditControl1.Document.InsertRtfText(result.editorControl1.richEditControl1.Document.Range.End, addDocument.Document.RtfText);
                if (i == 0)
                    _section.UnlinkHeaderFromPrevious();
                var addHeader = addDocument.Document.Sections[i].BeginUpdateHeader();
                var header = _section.BeginUpdateHeader();
                header.Delete(header.Range);
               // header.Replace(header.Range, string.Empty);
                header.InsertRtfText(header.Range.Start, addHeader.GetRtfText(addHeader.Range));
                addDocument.Document.Sections[i].EndUpdateHeader(addHeader);
                _section.EndUpdateHeader(header);
                if (i == 0)
                    _section.UnlinkFooterFromPrevious();
                var addFooter = addDocument.Document.Sections[i].BeginUpdateFooter();
                var footer = _section.BeginUpdateFooter();
                footer.Delete(footer.Range);
               // footer.Replace(footer.Range, string.Empty);
                footer.InsertRtfText(footer.Range.Start, addFooter.GetRtfText(addFooter.Range));
                addDocument.Document.Sections[i].EndUpdateFooter(addFooter);
                _section.EndUpdateFooter(footer);
            }
        }

        private void ScanSubDocumentActiveMacro(SubDocument sub,int len)
        {
            foreach (var item in sub.FindAll(Generate.CommonRegex))
            {
                string ItemText = sub.GetText(item);
                if (string.IsNullOrWhiteSpace(ItemText))
                    continue;
                SetUpItemMacro(ItemText, len, Option.ScanDocumentType.SubDocument);
            }
        }
        private void ScanActiveMacro()
        {
            int len = Option.macs[0].Length;
            //Option.doc.idMacroRun.Clear();
            foreach (var section in result.editorControl1.richEditControl1.Document.Sections)
            {
                var header = section.BeginUpdateHeader();
                ScanSubDocumentActiveMacro(header,len);
                section.EndUpdateHeader(header);

                var footer = section.BeginUpdateFooter();
                ScanSubDocumentActiveMacro(footer, len);
                section.EndUpdateFooter(footer);
            }
            foreach (var item in result.editorControl1.richEditControl1.Document.FindAll(Generate.CommonRegex))
            {
                string ItemText = result.editorControl1.richEditControl1.Document.GetText(item);
                if (string.IsNullOrWhiteSpace(ItemText))
                    continue;
                SetUpItemMacro(ItemText, len,Option.ScanDocumentType.Document);
            }
            Option.doc.idMacroRun = Option.doc.idMacroRun.OrderBy(i => i).ToList();
        }
        private void SetUpItemMacro(string ItemText, int len, Option.ScanDocumentType scantype)
        {
            switch (scantype)
            {
                case Option.ScanDocumentType.Document:
            for (int i = 0; i < len; i++)
            {
                if (Option.macs[0][i].macro == ItemText)
                {
                    if (!Option.doc.idMacroRun.Contains(i))
                        Option.doc.idMacroRun.Add(i);
                    break;
                }
            }
                    break;
                case Option.ScanDocumentType.SubDocument:
                    for (int i = 0; i < len; i++)
                    {
                        if (Option.macs[0][i].macro == ItemText)
                        {
                            if (!Option.doc.idMacroSubDocumentRun.Contains(i))
                                Option.doc.idMacroSubDocumentRun.Add(i);
                            break;
                        }
                    }
                    break;
        }
        }

        private void MacroText(int j, int i)
        {
            if (Option.doc.idMacroRun.Contains(i))
            result.editorControl1.richEditControl1.Document.ReplaceAll(Option.macs[j][i].macro, Option.macs[j][i].result.ToString(), SearchOptions.CaseSensitive);
            if (Option.doc.idMacroSubDocumentRun.Contains(i))
            {
                foreach (var section in result.editorControl1.richEditControl1.Document.Sections)
                {                   
                    var header = section.BeginUpdateHeader();
                    header.ReplaceAll(Option.macs[j][i].macro, Option.macs[j][i].result.ToString(), SearchOptions.CaseSensitive);
                    section.EndUpdateHeader(header);

                    var footer = section.BeginUpdateFooter();
                    footer.ReplaceAll(Option.macs[j][i].macro, Option.macs[j][i].result.ToString(), SearchOptions.CaseSensitive);
                    section.EndUpdateFooter(footer);
                }
            }
        }

        private void EndAnalizeResult(Result result)
        {
            var Stolb = result.editorControl1.richEditControl1.Document.FindAll(new Regex(Generate.patternMergedColumn, RegexOptions.CultureInvariant));
            while(Stolb.Length>0)
            {
                DocumentRange range = Stolb[0];
                TableCell cell = result.editorControl1.richEditControl1.Document.Tables.GetTableCell(range.Start);
                if (cell == null)
                {
                    Errors.err.Add(result.editorControl1.richEditControl1.Document.GetText(range), "указанный макрос должен использоваться только в пределах таблицы");
                    break;
                }         
                //count rows by merged in columns     
                int rowRangeMerged = 0;
                for (int i = cell.Row.Index; i < cell.Table.Rows.Count-cell.Row.Index; i++)
                {
                    if (!string.IsNullOrEmpty(result.editorControl1.richEditControl1.Document.GetText(cell.Table[i+1, cell.Index].ContentRange)))
                        break;
                    rowRangeMerged++;
                }
                if (rowRangeMerged == 0)
                    break;
                string worked = result.editorControl1.richEditControl1.Document.GetText(range);
                if (!string.IsNullOrEmpty(worked))
                result.editorControl1.richEditControl1.Document.ReplaceAll(worked, string.Empty, SearchOptions.CaseSensitive, range);
                //Count columns by merge in right
                int Count = Convert.ToInt32(worked.Replace(")",string.Empty).Replace("Столб(",string.Empty));
                if (Count <= 0)
                    continue;
                for (int i = cell.Index+1; i <= cell.Index+Count; i++)
                {
                    for (int j = 0; j < rowRangeMerged; j++)
                    {                        
                        result.editorControl1.richEditControl1.Document.ReplaceAll(result.editorControl1.richEditControl1.Document.GetText(cell.Table[cell.Row.Index + j + 1, i].ContentRange), string.Empty, SearchOptions.CaseSensitive, cell.Table[cell.Row.Index + j + 1, i].ContentRange);
                    }
                        
                    cell.Table.MergeCells(cell.Table[cell.Row.Index,i], cell.Table[cell.Row.Index+rowRangeMerged, i]);
                }
                Stolb = result.editorControl1.richEditControl1.Document.FindAll(new Regex(Generate.patternMergedColumn, RegexOptions.CultureInvariant));
            }
           /* foreach (var section in result.editorControl1.richEditControl1.Document.Sections)
            {
                var header = section.BeginUpdateHeader();
                foreach (DocumentRange range in header.FindAll("{Номер страницы}", SearchOptions.CaseSensitive))
                {
                    result.editorControl1.richEditControl1.Views.PrintLayoutView.CurrentPageIndex
                    header.Replace(range, "");
                }
                section.EndUpdateHeader(header);

                var footer = section.BeginUpdateFooter();
                
                section.EndUpdateFooter(footer);
               
            } */
           
        }
        private bool NotOpen()
        {
            foreach (DocumentRange range in result.editorControl1.richEditControl1.Document.FindAll(@"{Не открывать}",SearchOptions.CaseSensitive))
            {
                string general = result.editorControl1.richEditControl1.Document.GetText(range);
                result.editorControl1.richEditControl1.Document.ReplaceAll(general, string.Empty, SearchOptions.CaseSensitive, range);
                return false;
            }
            return true;
        }
        private void Ternar()
        {
            foreach (DocumentRange range in result.editorControl1.richEditControl1.Document.FindAll(Generate.Ternar))
            {
                string general = result.editorControl1.richEditControl1.Document.GetText(range);
                string text = general.Replace(Environment.NewLine, string.Empty);
                text = text.Replace("^ЕСЛИ(", string.Empty);
                int idx = text.IndexOf(")?'");
                string tempValues = text.Remove(idx);
                string logic = string.Empty;
                string[] Comparsion = tempValues.Split(Generate.splitLogic, StringSplitOptions.None);
                if (Comparsion.Length != 2)
                    return;
                text = text.Remove(0, idx + 3);
                text = text.Remove(text.Length - 2, 2);
                string[] Values = text.Split(new string[] { "':'" }, StringSplitOptions.None);
                if (Values.Length != 2)
                    return;
                result.editorControl1.richEditControl1.Document.ReplaceAll(general, Generate.GetResultLogic(Generate.GetLogic(tempValues), Comparsion, Values), SearchOptions.CaseSensitive, range);
            }
        }
        private bool SpecialMacro(Result result)
        {
            DocumentPosition pos = null;
            bool open = true;
            for (int i = 0; i < _StaticFuntion.lst.Count; i++)
            {
                switch (_StaticFuntion.lst[i].type)
                {
                    case MacroType.Floor:
                        FloorFunc();break;
                    case MacroType.TernarLogic:Ternar();
                        break;
                    case MacroType.TableHeaderDub:
                        if (!result.editorControl1.richEditControl1.Document.Text.Contains(_StaticFuntion.lst[i].macro))
                            continue;                        
                        result.UI(null, null);
                        int pagecount = 0;
                        pagecount = result.editorControl1.richEditControl1.DocumentLayout.GetPageCount();//.PageCount;
                        if (pagecount <= 1)
                        {
                            result.editorControl1.richEditControl1.Document.ReplaceAll(_StaticFuntion.lst[i].macro, string.Empty, SearchOptions.CaseSensitive);
                            break;
                        }
                        //предыдущая таблица
                        int prevTableindex = -1;
                        int pageindex = -1;
                        // restart:
                        var headers = result.editorControl1.richEditControl1.Document.FindAll(_StaticFuntion.lst[i].macro, SearchOptions.CaseSensitive);
                        if (headers.Length == 0)
                            break;
                        foreach (DocumentRange range in headers)
                        {
                            TableCell cell = result.editorControl1.richEditControl1.Document.Tables.GetTableCell(range.Start);
                            if (cell == null)
                                continue;
                            Table table=cell.Table;
                            //var element = result.editorControl1.richEditControl1.DocumentLayout.GetElement(range.Start, DevExpress.XtraRichEdit.API.Layout.LayoutType.TableCell);
                            //получить текущую страницу данной ячейки, получить все "буфферные" строки до этой cтроки включая её. Номер строки?
                            int currTableindex = Dev.GetTableIndex(result.editorControl1.richEditControl1.Document.Tables, table); //result.editorControl1.richEditControl1.DocumentLayout.GetPageIndex(element);
                                                                                                                                        
                            if (prevTableindex == -1 || prevTableindex != currTableindex)
                            {
                                prevTableindex = currTableindex;
                                Collection.rowCollection = new RowCollection(result.editorControl1.richEditControl1.Document, table);
                                for (int e = 0; e < cell.Row.Index + 1; e++)
                                {
                                    result.editorControl1.richEditControl1.Document.ReplaceAll(_StaticFuntion.lst[i].macro, string.Empty, SearchOptions.CaseSensitive, table.Rows[e].Range);
                                    Collection.rowCollection.Add(table.Rows[e]);
                                    if (e == cell.Row.Index)
                                    {
                                        var elem = result.editorControl1.richEditControl1.DocumentLayout.GetElement(table.Rows[i].Cells[cell.Index].Range.Start, LayoutType.TableCell);
                                        if (elem == null)
                                            elem = result.editorControl1.richEditControl1.DocumentLayout.GetElement(table.Rows[i].Cells[cell.Index].Range.End, LayoutType.TableCell);
                                        pageindex = result.editorControl1.richEditControl1.DocumentLayout.GetPageIndex(elem);
                                    }
                                }
                            }
                            //Теперь у нас есть в буфере нужные строки
                            for (int m = 0; m < table.Rows.Count; m++)
                            {
                                var elem = GetRangedElement(result, cell, table, m);
                                int newPageindex = result.editorControl1.richEditControl1.DocumentLayout.GetPageIndex(elem);
                                if (newPageindex > pageindex)
                                {                                  
                                    //pageindex = newPageindex;
                                    result.editorControl1.richEditControl1.Document.CaretPosition = table.Rows[m].Cells[cell.Index].Range.Start;
                                    var splitCommand = new SplitTableCommand(result.editorControl1.richEditControl1);
                                    var _pos = table.Rows[m].Cells[0].Range.Start;
                                    //
                                    splitCommand.Execute();
                                    table = result.editorControl1.richEditControl1.Document.Tables[result.editorControl1.richEditControl1.Document.Tables.Count - 1];
                                    //result.editorControl1.richEditControl1.Document.Tables[result.editorControl1.richEditControl1.Document.Tables.IndexOf(table) + 1];
                                    m = 0;
                                    Collection.rowCollection.tab = table;
                                    Collection.rowCollection.InsertRowCollection(table.Rows[m].Index, RowCollection.InsertedOption.Before);
                                    result.editorControl1.richEditControl1.Document.CaretPosition = result.editorControl1.richEditControl1.Document.CreatePosition(table[0, 0].Range.Start.ToInt() - 1);
                                    var pageBreak = new InsertPageBreakCommand(result.editorControl1.richEditControl1);
                                    pageBreak.Execute();
                                    m += Collection.rowCollection.Rows.Count;
                                    elem = GetRangedElement(result, cell, table, m);
                                    pageindex = result.editorControl1.richEditControl1.DocumentLayout.GetPageIndex(elem);
                                    //Тут нужно обновить слой элементов
                                    // m += Collection.rowCollection.Rows.Count;


                                }
                            }
                            //мы должны вернуться на предыдущую обработку ренжей т.к. данные уже сдвинулись
                            //goto restart;
                        }
                        break;
                    case MacroType.TableMergeCellFuncs:
                        var merges = result.editorControl1.richEditControl1.Document.FindAll(new Regex(_StaticFuntion.lst[i].macro + @"\[\d*\]", RegexOptions.CultureInvariant));
                        foreach (DocumentRange range in merges)
                        {
                            TableCell cell = result.editorControl1.richEditControl1.Document.Tables.GetTableCell(range.Start);
                            if (cell == null)
                                continue;
                            string text = result.editorControl1.richEditControl1.Document.GetText(range);
                            text = text.Remove(0, text.IndexOf("[") + 1);
                            text = text.Remove(text.IndexOf("]"));
                            int indexOf = -1;
                            if (!int.TryParse(text, out indexOf))
                                continue;
                            result.editorControl1.richEditControl1.Document.ReplaceAll(result.editorControl1.richEditControl1.Document.GetText(range), string.Empty, SearchOptions.CaseSensitive, range);
                            //сколько?
                            cell.Table.MergeCells(cell, cell.Row[cell.Index + indexOf - 1]);
                        }

                        merges = result.editorControl1.richEditControl1.Document.FindAll(_StaticFuntion.lst[i].macro, SearchOptions.CaseSensitive);
                        foreach (DocumentRange range in merges)
                        {
                            TableCell cell = result.editorControl1.richEditControl1.Document.Tables.GetTableCell(range.Start);
                            if (cell == null)
                                continue;
                            result.editorControl1.richEditControl1.Document.ReplaceAll(result.editorControl1.richEditControl1.Document.GetText(range), string.Empty, SearchOptions.CaseSensitive, range);
                            cell.Table.MergeCells(cell, cell.Row.LastCell);
                        }
                        break;
                    case MacroType.ArrContinue:
                        if (!result.editorControl1.richEditControl1.Document.Text.Contains(_StaticFuntion.lst[i].macro))
                            continue;
                        result.editorControl1.richEditControl1.Document.ReplaceAll(_StaticFuntion.lst[i].macro, string.Empty, SearchOptions.CaseSensitive);
                        break;
                    case MacroType.LowerFirsteCharacterString:
                        var lowers = result.editorControl1.richEditControl1.Document.FindAll(new Regex(@"{строка\(.+\)}", RegexOptions.CultureInvariant));
                        if (lowers.Length == 0)
                            continue;
                        StringBuilder Lower;
                        foreach (DocumentRange range in lowers)
                        {
                            string str = result.editorControl1.richEditControl1.Document.GetText(range);
                            Lower = new StringBuilder(str.Remove(0, 8));
                            Lower= Lower.Remove(Lower.Length - 2, 2);
                            Lower[0] = char.ToLower(Lower[0]);
                            result.editorControl1.richEditControl1.Document.ReplaceAll(str, Lower.ToString(), SearchOptions.CaseSensitive, range);
                        }
                        break;
                    case MacroType.UpperFirsteCharacterString:
                        var uppers = result.editorControl1.richEditControl1.Document.FindAll(new Regex(@"{Строка\(.+\)}", RegexOptions.CultureInvariant));
                        if (uppers.Length == 0)
                            continue;
                        StringBuilder Upper;
                        foreach (DocumentRange range in uppers)
                        {
                            string str = result.editorControl1.richEditControl1.Document.GetText(range);
                            Upper = new StringBuilder(str.Remove(0,8));
                            Upper = Upper.Remove(Upper.Length - 2, 2);
                            Upper[0] = char.ToUpper(Upper[0]);
                            result.editorControl1.richEditControl1.Document.ReplaceAll(str, Upper.ToString(), SearchOptions.CaseSensitive, range);
                        }
                        break;
                    case MacroType.PageNumber:
                        pos=null;
                        if (!DocumentColontitulesSetOriginal)
                        {
                            DocumentColontitulesSetOriginal = true;
                            result.editorControl1.SetOriginalColontitules(result.editorControl1.richEditControl1.Document);
                        }

                        //PageDecrement = result.editorControl1.richEditControl1.Views.PrintLayoutView.PageCount - DocumentPageCount;
                        pos = PageNumberReplace(result, result.editorControl1.richEditControl1.Document,LayoutType.Page, PageDecrement,pos);
                        foreach (var section in result.editorControl1.richEditControl1.Document.Sections)
                        {
                            var header = section.BeginUpdateHeader();
                            pos = PageNumberReplace(result, header,LayoutType.Header, PageDecrement,pos);
                            section.EndUpdateHeader(header);

                            var footer = section.BeginUpdateFooter();
                            pos = PageNumberReplace(result, footer, LayoutType.Footer, PageDecrement,pos);
                            section.EndUpdateFooter(footer);
                        }
                        if (pos != null)
                        {
                            var s = result.editorControl1.richEditControl1.Document.GetSection(pos);
                            s.PageNumbering.ContinueNumbering = false;
                            s.PageNumbering.FirstPageNumber = 1;
                            pos = null;
                        }
                        break;
                    case MacroType.CountPages:
                        if (!DocumentColontitulesSetOriginal)
                        {
                            DocumentColontitulesSetOriginal = true;
                            result.editorControl1.SetOriginalColontitules(result.editorControl1.richEditControl1.Document);
                        }                    
                        DocumentPageCount = result.editorControl1.richEditControl1.Views.PrintLayoutView.PageCount - DocumentPageSumm - PageDecrement++;
                        DocumentPageSumm += DocumentPageCount;
                          CountPagesReplace(result, result.editorControl1.richEditControl1.Document, LayoutType.Page);
                        foreach (var section in result.editorControl1.richEditControl1.Document.Sections)
                        {
                            var header = section.BeginUpdateHeader();
                            CountPagesReplace(result, header, LayoutType.Header);
                            section.EndUpdateHeader(header);

                            var footer = section.BeginUpdateFooter();
                            CountPagesReplace(result, footer, LayoutType.Footer);
                            section.EndUpdateFooter(footer);
                        }
                        break;
                    case MacroType.SaveDocument:
                        SaveRTF(result);
                        SavePDF(result);
                        break;
                    case MacroType.NotOpen:
                        open = NotOpen();
                        break;
                    //Запрос
                    case MacroType.Request:

                        break;
                }
            }
            if (result.editorControl1.richEditControl1.Document.Tables.Count > 0)
                result.editorControl1.barButtonItem1_ItemClick(null, null);
            return open;
        }
        
        private static DocumentPosition PageNumberReplace(Result result,SubDocument doc, LayoutType type,int decrement, DocumentPosition pos=null)
        {           
            foreach (DocumentRange range in doc.FindAll(new Regex(@"{Номер страницы}", RegexOptions.CultureInvariant)))
            {                
                pos = range.Start;
                doc.ReplaceAll("{Номер страницы}", string.Empty, SearchOptions.CaseSensitive, range);
               /*  doc.ReplaceAll(doc.GetText(range), (result.editorControl1.richEditControl1.DocumentLayout.GetPageIndex(result.editorControl1.richEditControl1.DocumentLayout.GetElement(range.Start, type))-decrement+1).ToString(), SearchOptions.CaseSensitive, range);    */           
            }
            return pos;
        }

        private DocumentPosition CountPagesReplace(Result result, SubDocument doc,LayoutType type,DocumentPosition pos=null)
        {
            foreach (DocumentRange range in doc.FindAll(new Regex(@"{Количество страниц}", RegexOptions.CultureInvariant)))
            {
                if (pos != null)
                    pos = range.Start;
                //doc.ReplaceAll(doc.GetText(range), string.Empty, SearchOptions.CaseSensitive, range);
                result.editorControl1.richEditControl1.Document.CaretPosition = result.editorControl1.richEditControl1.Document.Range.End;
                result.editorControl1.richEditControl1.ScrollToCaret();
                doc.ReplaceAll(doc.GetText(range), DocumentPageCount.ToString(), SearchOptions.CaseSensitive, range);
            }
            return pos;
        }

        private static RangedLayoutElement GetRangedElement(Result result, TableCell cell, Table table, int m)
        {
            RangedLayoutElement elem = result.editorControl1.richEditControl1.DocumentLayout.GetElement(table.Rows[m].Cells[cell.Index].Range.Start, LayoutType.TableCell);
            if (elem == null)
                elem = result.editorControl1.richEditControl1.DocumentLayout.GetElement(table.Rows[m].Cells[cell.Index].Range.End, LayoutType.TableCell);
            return elem;
        }

        private void FloorFunc()
        {
            foreach (DocumentRange range in result.editorControl1.richEditControl1.Document.FindAll(new Regex(@"{Округлить\(.+\)}", RegexOptions.Multiline)))
            {
                string general = result.editorControl1.richEditControl1.Document.GetText(range);
                string text = general.Replace(Environment.NewLine, string.Empty).Replace(",",Option.separator).Replace(".",Option.separator).Replace("{Округлить(",string.Empty).Replace(")}",string.Empty);
                double value; int znak;
                string[] Save = text.Split(new char[] { ';' }, StringSplitOptions.None);
                switch (Save.Length)
                {
                    case 0:
                        if (double.TryParse(text, out value))
                        {
                            result.editorControl1.richEditControl1.Document.ReplaceAll(general, Math.Round(value).ToString(), SearchOptions.CaseSensitive, range);
                            continue;
                        }
                        //TODO
                        result.editorControl1.richEditControl1.Document.ReplaceAll(general, string.Empty, SearchOptions.CaseSensitive, range);
                        break;
                    case 2:
                        if (double.TryParse(Save[0], out value) && int.TryParse(Save[1], out znak))
                        {
                            result.editorControl1.richEditControl1.Document.ReplaceAll(general, Math.Round(value,znak,MidpointRounding.AwayFromZero).ToString(), SearchOptions.CaseSensitive, range);
                            continue;
                        }
                        result.editorControl1.richEditControl1.Document.ReplaceAll(general, string.Empty, SearchOptions.CaseSensitive, range);
                        break;
                    default:
                        result.editorControl1.richEditControl1.Document.ReplaceAll(general, string.Empty, SearchOptions.CaseSensitive, range); break;
                }

               
            }
        }

        private void SavePDF(Result result)
        {
            if (string.IsNullOrEmpty(Option.FileName))
            {
                var Saves = result.editorControl1.richEditControl1.Document.FindAll(new Regex(@"{Сохранить pdf\(.+\,.+\)}", RegexOptions.CultureInvariant));
                // MessageBox.Show("Не определён путь для сохранения файла, сохранение не будет произведено");
                if (Saves.Length == 0)
                    return;
                using (FbConnection fConnection = new FbConnection(Option.ConBD))
                {
                    fConnection.Open();
                    using (FbCommand fCommand = new FbCommand(@"select i5 from i where i1=" + Option.UserId, fConnection))
                    {
                        using (FbDataReader fReader = fCommand.ExecuteReader())
                        {
                            if (fReader.Read())
                            {
                                Option.FileName = fReader[0]?.ToString();
                                if (string.IsNullOrEmpty(Option.FileName))
                                    return;
                            }
                            else
                                return;
                        }
                    }
                }
                foreach (DocumentRange range in Saves)
                {
                    string text = result.editorControl1.richEditControl1.Document.GetText(range);
                    string[] Save = text.Split(new char[] { '(', ',', ')' }, StringSplitOptions.RemoveEmptyEntries);
                    if (Save.Length < 4)
                        continue;
                    result.editorControl1.richEditControl1.Document.ReplaceAll(text, string.Empty, SearchOptions.CaseSensitive, range);
                    string path = (Option.FileName + @"\" + Save[1]).Replace("\"", string.Empty);
                    if (!Directory.Exists(path))
                        Directory.CreateDirectory(path);

                    RichEditDocumentServer server = new RichEditDocumentServer();
                    MemoryStream stream = new MemoryStream();
                    result.editorControl1.richEditControl1.SaveDocument(stream, DevExpress.XtraRichEdit.DocumentFormat.Rtf);
                    stream.Seek(0, SeekOrigin.Begin);
                    server.LoadDocument(stream, DevExpress.XtraRichEdit.DocumentFormat.Rtf);
                    string outFileName = (Option.FileName + @"\" + Save[1] + @"\" + Save[2] + ".pdf").Replace("\"", string.Empty);
                    FileStream fsOut = File.Open(outFileName, FileMode.Create);
                    server.ExportToPdf(fsOut);
                    fsOut.Close();
                    Option.FileName = string.Empty;
                }
            }
            else
            {
                if (!Directory.Exists(Option.FileName))
                    Directory.CreateDirectory(Option.FileName);
                RichEditDocumentServer server = new RichEditDocumentServer();
                MemoryStream stream = new MemoryStream();
                result.editorControl1.richEditControl1.SaveDocument(stream, DevExpress.XtraRichEdit.DocumentFormat.Rtf);
                stream.Seek(0, SeekOrigin.Begin);
                server.LoadDocument(stream, DevExpress.XtraRichEdit.DocumentFormat.Rtf);
                string outFileName = Option.FileName;
                FileStream fsOut = File.Open(outFileName, FileMode.Create);
                server.ExportToPdf(fsOut);
                fsOut.Close();               
                Option.FileName = string.Empty;
            }
        }

        private static void SaveRTF(Result result)
        {
            result.editorControl1.richEditControl1.Document.ReplaceAll("{Новый файл}", string.Empty, SearchOptions.CaseSensitive);
            if (string.IsNullOrEmpty(Option.FileName))
            {
                var Saves = result.editorControl1.richEditControl1.Document.FindAll(new Regex(@"{Сохранить как\(.+\,.+\)}", RegexOptions.CultureInvariant));
                // MessageBox.Show("Не определён путь для сохранения файла, сохранение не будет произведено");
                if (Saves.Length == 0)
                    return;
                using (FbConnection fConnection = new FbConnection(Option.ConBD))
                {
                    fConnection.Open();
                    using (FbCommand fCommand = new FbCommand(@"select i5 from i where i1=" + Option.UserId, fConnection))
                    {
                        using (FbDataReader fReader = fCommand.ExecuteReader())
                        {
                            if (fReader.Read())
                            {
                                Option.FileName = fReader[0]?.ToString();
                                if (string.IsNullOrEmpty(Option.FileName))
                                    return;
                            }
                            else
                                return;
                        }
                    }
                }
                foreach (DocumentRange range in Saves)
                {
                    string text = result.editorControl1.richEditControl1.Document.GetText(range);
                    string[] Save = text.Split(new char[] { '(', ',', ')' }, StringSplitOptions.RemoveEmptyEntries);
                    if (Save.Length < 4)
                        continue;
                    result.editorControl1.richEditControl1.Document.ReplaceAll(text, string.Empty, SearchOptions.CaseSensitive, range);
                    string path = (Option.FileName + @"\" + Save[1]).Replace("\"", string.Empty);
                    if (!Directory.Exists(path))
                        Directory.CreateDirectory(path);
                    result.editorControl1.richEditControl1.SaveDocument((Option.FileName + @"\" + Save[1] + @"\" + Save[2] + ".doc").Replace("\"", string.Empty), DocumentFormat.Rtf);
                    Option.FileName = string.Empty;
                }
            }
            else
            {
                if (!Directory.Exists(Option.FileName))
                    Directory.CreateDirectory(Option.FileName);
                result.editorControl1.richEditControl1.SaveDocument(Option.FileName, DocumentFormat.Rtf);
                Option.FileName = string.Empty;
            }
        }
        private static void _rtfReplacer(SubDocument document,string rtf,string macro,DocumentRange docRange=null)
        {
            var find = document.FindAll(macro, SearchOptions.CaseSensitive);
            if (docRange == null)
            {
                foreach (DocumentRange range in find)
                    document.InsertRtfText(range.Start, rtf, InsertOptions.KeepSourceFormatting);
                document.ReplaceAll(macro, string.Empty, SearchOptions.CaseSensitive);
            }
            else
            {
                document.InsertRtfText(docRange.Start, rtf, InsertOptions.KeepSourceFormatting);
                document.ReplaceAll(macro, string.Empty, SearchOptions.CaseSensitive, docRange);
            }
               
        }
        private static void MacroArrString(SubDocument document, int j, int i, MacroType typeArray)
        {
            string[,] arr = null;
            bool full = false;
           // CollectionInfo.currTable = null;
            var parameters = document.FindAll(new Regex(Option.macs[j][i].macro + Generate.pattern1, RegexOptions.CultureInvariant));
            if (parameters.Length != 0 && !full)
            {
                arr = Option.macs[j][i].result as string[,];
                if (Option.macs[j][i].rowStart != 0)
                {
                    string[,] t2 = new string[arr.GetLength(0) - Option.macs[j][i].rowStart, arr.GetLength(1)];
                    Array.Copy(arr, Option.macs[j][i].rowStart * t2.GetLength(1), t2, 0, t2.Length);
                    arr = t2;
                }
                full = true;
            }
            int a1, a2;
            foreach (DocumentRange range in parameters)
            {
                string text = document.GetText(range);
                string[] indexes = text.Split(new char[] { '[', ']', ',' }, StringSplitOptions.RemoveEmptyEntries);
                if (indexes.Length < 3)
                {
                    Errors.err.Add(text, "отсутствуют квадратные скобки, запятая, или значения в скобках");
                    continue;
                }
                if (!int.TryParse(indexes[1], out a1) || !int.TryParse(indexes[2], out a2))
                {
                    Errors.err.Add(text, "ошибка внутри квадратных скобок, одно из значений не является числом");
                    continue;
                }
                if (a2 >= arr.GetLength(0) || a1 >= arr.GetLength(1))
                {
                    Errors.err.Add(text, "указан индекс, который выходит за пределы массива макроса");
                    document.ReplaceAll(text, string.Empty, SearchOptions.CaseSensitive, range);
                } else
                    switch (typeArray)
                    {
                        case MacroType.ArrayString:
                            document.ReplaceAll(text, arr[a2, a1], SearchOptions.CaseSensitive, range);break;
                        case MacroType.ArrayWithRTF:
                            _rtfReplacer(document, arr[a2, a1], text);
                            break;
                    }
               
            }
            var TableContinue = document.FindAll(_StaticFuntion.lst[_StaticFuntion.ArrContinueIndex].macro, SearchOptions.CaseSensitive);
            var columns = document.FindAll(new Regex(Option.macs[j][i].macro + Generate.pattern2, RegexOptions.CultureInvariant));
            if (columns.Length != 0 && !full)
            {
                arr = Option.macs[j][i].result as string[,];
                if (Option.macs[j][i].rowStart != 0)
                {
                    string[,] t2 = new string[arr.GetLength(0) - Option.macs[j][i].rowStart, arr.GetLength(1)];
                    Array.Copy(arr, Option.macs[j][i].rowStart * t2.GetLength(1), t2, 0, t2.Length);
                    arr = t2;
                }
                full = true;
            }
            while(columns.Length>0)          
            {
                DocumentRange range = columns[0];
                TableCell cell = document.Tables.GetTableCell(range.Start);
                if (cell == null)
                {
                    Errors.err.Add(document.GetText(range), "для данного макроса отсутствует таблица в документе");
                    break;
                }
                int sk = arr.GetLength(0) - 1;                
                //если длина одного измерения больше 0
                if (sk > 0)
                {
                    if (TableContinue.Length == 0)
                    {
                        Generate.TextOption opt = new Generate.TextOption(document.GetText(range), true);                       
                        for (int k = cell.Row.Index; sk > 0; k++, sk--)
                        {
                            if (CollectionInfo.currTable == null || !CollectionInfo.currTable.Equals(cell.Table))
                            {
                                CollectionInfo.currentMacro = Option.macs[j][i].macro;
                                CollectCellsInfo(document, cell);
                                CollectionInfo.RowInserted.Clear();
                            } else if (CollectionInfo.currentMacro!= Option.macs[j][i].macro)
                            {
                                CollectionInfo.currentMacro = Option.macs[j][i].macro;
                                CollectCellsInfo(document, cell);
                            }
                            if (!CollectionInfo.RowInserted.Contains(k+1))//k+1
                            {                               
                                cell.Table.Rows.InsertAfter(k);
                                CollectionInfo.AddRow(k + 1);
                                //CollectionInfo.RowInserted.Add(k+1);//k+1
                                for (int n = 0; n < cell.Row.Cells.Count; n++)
                                {
                                    document.InsertRtfText(cell.Table[k + 1, n].Range.Start, CollectionInfo.Cells[n],InsertOptions.KeepSourceFormatting);
                                }
                            }
                        }                        
                        int arrIndex = opt.values[0];
                        if (arr.GetLength(0) == 0)
                        {
                            document.ReplaceAll(opt.textOriginnal, string.Empty, SearchOptions.CaseSensitive, cell.Table.Range);
                            continue;
                        }
                        if (opt.Minus)
                            arrIndex = arr.GetLength(1) - 1 - arrIndex;
                        //медленное место...
                        SetResultReplaceArr(document, arr, cell, opt, arrIndex);

                    }
                    else//Если мы имеем продолжение - никаких строк не добавляем, только в последнюю таблицу
                    {                        
                        Generate.TextOption opt = new Generate.TextOption(document.GetText(range), true);
                        bool start = true;
                        string rtfText = document.GetRtfText(range);
                        int arrIndex = opt.values[0];
                        for (int k = cell.Row.Index, cellIndex = 0, rowIndex = 0; sk > 0; k++, sk--, rowIndex++)
                        {
                            if (k > cell.Table.Rows.Count - 1)
                            {
                                cell = document.Tables.GetTableCell(TableContinue[cellIndex].Start).Row.Cells[cell.Index];
                                k = cell.Row.Index;
                                cellIndex++;
                            }
                            if (!start)
                            {
                                document.InsertRtfText(cell.Table.Rows[k].Cells[cell.Index].ContentRange.Start, rtfText);
                            }
                            start = false;
                            document.ReplaceAll(opt.textOriginnal, arr[rowIndex, arrIndex], SearchOptions.CaseSensitive, cell.Table.Rows[k].Cells[cell.Index].ContentRange);
                        }
                    }
                }
                else
                {
                    if (sk == 0)
                    {
                        Generate.TextOption opt = new Generate.TextOption(document.GetText(range), true);                       
                        int arrIndex = opt.values[0];                       
                        if (opt.Minus)
                            arrIndex = arr.GetLength(1) - arrIndex - 1;
                        SetResultReplaceArr(document, arr, cell, opt, arrIndex);
                    }
                }
                if (sk == -1)
                    break;
                columns = document.FindAll(new Regex(Option.macs[j][i].macro + Generate.pattern2, RegexOptions.CultureInvariant));
            }
            {
                //макрос[c какого столбца-отминусовать с конца]
                var cNew = document.FindAll(new Regex(Option.macs[j][i].macro + Generate.pattern3, RegexOptions.CultureInvariant));               
                if (cNew.Length != 0 && !full)
                {
                    arr = Option.macs[j][i].result as string[,];
                    if (Option.macs[j][i].rowStart != 0)
                    {
                        string[,] t2 = new string[arr.GetLength(0) - Option.macs[j][i].rowStart, arr.GetLength(1)];
                        Array.Copy(arr, Option.macs[j][i].rowStart * t2.GetLength(1), t2, 0, t2.Length);
                        arr = t2;
                    }
                    full = true;
                }
                int PrevIndex = -1; int prevRowIndex = -1;             
                while (cNew.Length > 0)
                {
                    DocumentRange range = cNew[0];                   
                    TableCell cell = document.Tables.GetTableCell(range.Start);
                    if (cell == null)
                        continue;
                    if (PrevIndex == cell.Index && prevRowIndex == cell.Row.Index)
                        continue;
                    PrevIndex = cell.Index;
                    prevRowIndex = cell.Row.Index;
                    int sk = arr.GetLength(0) - 1;
                    //если длина одного измерения больше 0
                    if (sk >= 0)
                    {
                        Generate.TextOption opt = new Generate.TextOption(document.GetText(range)); 
                        if (opt.values.Length < 2)
                            break;                       
                        string CellText = document.GetRtfText(cell.ContentRange);
                        int num = arr.GetLength(1) - opt.values[1] - 3;
                        string prev = string.Empty;
                        for (int k = cell.Index, currArr = opt.values[0], interval = 0; interval < num; k++, currArr++, interval++)
                        {
                            //первая текущая ячейка уже есть, на все последующих мы добавляем столбец и вставляем текст макроса
                            if (interval != 0)
                            {
                                if (opt.columnInsert)
                                {
                                    //Новый столбец
                                    //cell.Row.Cells[k-1].PreferredWidthType = WidthType.None; 
                                    cell.Row.Cells.InsertAfter(k - 1);                                    
                                }
                                document.InsertRtfText(cell.Table[cell.Row.Index, k].ContentRange.Start, CellText);
                            }                            
                            if (!opt.OneRow)
                            {
                                for (int c = cell.Row.Index, p = 0; c < cell.Table.Rows.Count; c++, p++)
                                {
                                    if (p != 0)
                                    {
                                        if (!document.GetText(cell.Table[c, k].ContentRange).Contains(opt.textOriginnal))
                                            document.InsertRtfText(cell.Table[c, k].ContentRange.Start, CellText);
                                    }
                                    document.ReplaceAll(opt.textOriginnal, arr[p, currArr], SearchOptions.CaseSensitive, cell.Table[c, k].ContentRange);
                                    if (c == cell.Table.Rows.Count - 1 && sk > c)
                                        cell.Table.Rows.InsertAfter(cell.Table.Rows[c].Index);
                                    if (c == cell.Table.Rows.Count - 1)
                                    {document.ReplaceAll(opt.textOriginnal, arr[p, currArr], SearchOptions.CaseSensitive, cell.Table[c, k].ContentRange);
                                    }
                                }
                            }
                            else
                            {
                                document.ReplaceAll(opt.textOriginnal, arr[opt.values[2], currArr], SearchOptions.CaseSensitive, cell.Table[cell.Row.Index, k].ContentRange);
                                if (opt.Merge)
                                {
                                    if (interval != 0)
                                    {
                                        if (prev == arr[opt.values[2], currArr])
                                        {
                                            if (!string.IsNullOrEmpty(prev))
                                                document.ReplaceAll(arr[opt.values[2], currArr], string.Empty, SearchOptions.CaseSensitive, cell.Table[cell.Row.Index, k].ContentRange);
                                            cell.Table.MergeCells(cell.Table[cell.Row.Index, k - 1], cell.Table[cell.Row.Index, k]);                                           
                                            k--;
                                        }
                                    }                                   
                                }
                                prev = arr[opt.values[2], currArr];
                            }
                        }
                    }
                    cNew = document.FindAll(new Regex(Option.macs[j][i].macro + Generate.pattern3, RegexOptions.CultureInvariant));
                }

            }
        }

        private static void CollectCellsInfo(SubDocument document, TableCell cell)
        {
            CollectionInfo.currTable = cell.Table;
            CollectionInfo.Cells = new string[cell.Row.Cells.Count];
            for (int n = 0; n < CollectionInfo.Cells.Length; n++)
            {
                CollectionInfo.Cells[n] = document.GetRtfText(cell.Row.Cells[n].ContentRange);
            }
        }

        //TODO
        private static void SetResultReplaceArr(SubDocument document, string[,] arr, TableCell cell, Generate.TextOption opt, int arrIndex,MacroType typeArray=MacroType.ArrayString)
        {
            if (!opt.Merge)
                for (int k = cell.Row.Index, l = 0; k < arr.GetLength(0) + cell.Row.Index; k++, l++)
                {
                    if (l > arr.GetLength(0) || arrIndex > arr.GetLength(1))
                    {                       
                        Errors.err.Add(opt.textOriginnal, "указан индекс, который выходит за пределы массива макроса");
                        if (!string.IsNullOrEmpty(opt.textOriginnal))
                            document.ReplaceAll(opt.textOriginnal, string.Empty, SearchOptions.CaseSensitive, cell.Table[k, cell.Index].ContentRange);
                    }else
                    if (!string.IsNullOrEmpty(opt.textOriginnal))
                        switch (typeArray)
                        {
                            case MacroType.ArrayString:
                                document.ReplaceAll(opt.textOriginnal, arr[l, arrIndex], SearchOptions.CaseSensitive, cell.Table[k, cell.Index].ContentRange); break;
                            case MacroType.ArrayWithRTF:
                                _rtfReplacer(document, arr[l, arrIndex], opt.textOriginnal, cell.Table[k, cell.Index].ContentRange);
                                break;
                        }
                   
                }
            else
            {
                int start = 0;
                string prev = string.Empty;
                for (int k = cell.Row.Index, l = 0; k < arr.GetLength(0) + cell.Row.Index; k++, l++)
                {
                    if (l > arr.GetLength(0) || arrIndex > arr.GetLength(1))
                    {
                        Errors.err.Add(opt.textOriginnal, "указан индекс, который выходит за пределы массива макроса");
                        return;
                    }
                    if (l != 0 && prev == arr[l, arrIndex])
                    {
                        if (start == 0)                       
                            start = k-1;
                        if (!string.IsNullOrEmpty(opt.textOriginnal))  
                            document.ReplaceAll(opt.textOriginnal, string.Empty, SearchOptions.CaseSensitive, cell.Table[k, cell.Index].ContentRange);
                            cell.Table.MergeCells(cell.Table[getMergedPrevCellIndex(arr, arrIndex, l) + cell.Row.Index, cell.Index], cell.Table[k, cell.Index]);
                        
                        // cell.Table.MergeCells(cell.Table[k - 1, cell.Index], cell.Table[k, cell.Index]);
                    }
                    else
                    {
                        if (start != 0)
                        {
                            string txt = document.GetText(cell.Table[start, cell.Index].ContentRange);
                            if (!string.IsNullOrEmpty(txt))
                                document.ReplaceAll(txt, txt + opt.mData, SearchOptions.CaseSensitive, cell.Table[start, cell.Index].ContentRange);
                            else
                                document.InsertText(cell.Table[start, cell.Index].ContentRange.Start, opt.mData);
                        }
                        if (!string.IsNullOrEmpty(opt.textOriginnal))
                            document.ReplaceAll(opt.textOriginnal, arr[l, arrIndex], SearchOptions.CaseSensitive, cell.Table[k, cell.Index].ContentRange);
                        start = 0;
                    }                 
                    prev = arr[l, arrIndex];
                }
            }
        }
       private static int getMergedPrevCellIndex(string[,] arr, int arrIndex, int endRowIndex)
        {
            int output = endRowIndex - 1;
            for (int i = endRowIndex-1; i >= 0; i--)
            {
                if (arr[i, arrIndex] != arr[endRowIndex, arrIndex])
                    break;
                output = i;
            }
            return output;
        }
        private static bool MacroRTF(Result result, int j, int i)
        {
            bool notfind = false;
            if (Option.doc.idMacroRun.Contains(i))
            {
                var find = result.editorControl1.richEditControl1.Document.FindAll(Option.macs[j][i].macro, SearchOptions.CaseSensitive);
                if (find.Length > 0)
                    notfind = true;
                foreach (DocumentRange range in find)
                {
                    result.editorControl1.richEditControl1.Document.InsertRtfText(range.Start, Option.macs[j][i].result.ToString(), InsertOptions.KeepSourceFormatting);
                }
                result.editorControl1.richEditControl1.Document.ReplaceAll(Option.macs[j][i].macro, string.Empty, SearchOptions.CaseSensitive);
            }
            if (Option.doc.idMacroSubDocumentRun.Contains(i))
            {
                foreach (var section in result.editorControl1.richEditControl1.Document.Sections)
                {
                    var header = section.BeginUpdateHeader();
                    var find = header.FindAll(Option.macs[j][i].macro, SearchOptions.CaseSensitive);
                    if (find.Length > 0)
                        notfind = true;
                    foreach (DocumentRange range in find)                    
                        header.InsertRtfText(range.Start, Option.macs[j][i].result.ToString(), InsertOptions.KeepSourceFormatting);
                    header.ReplaceAll(Option.macs[j][i].macro, string.Empty, SearchOptions.CaseSensitive);
                    section.EndUpdateHeader(header);


                    var footer = section.BeginUpdateFooter();
                    find = footer.FindAll(Option.macs[j][i].macro, SearchOptions.CaseSensitive);
                    if (find.Length > 0)
                        notfind = true;
                    foreach (DocumentRange range in find)
                        footer.InsertRtfText(range.Start, Option.macs[j][i].result.ToString(), InsertOptions.KeepSourceFormatting);
                    footer.ReplaceAll(Option.macs[j][i].macro, string.Empty, SearchOptions.CaseSensitive);
                    section.EndUpdateFooter(footer);
                }
            }
            return notfind;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DocPrint(2);
        }     
        private void SetPacka()
        {
            if (!editorControl1.barCheckItem1.Checked)
                return;
            StopChange = true;
           // editorControl1.richEditControl1.Document.ContentChanged -= change;
            PACKPACKA.Stop();
            //try
            {
                //editorControl1.richEditControl1.Document.BeginUpdate();
               // editorControl1.SetAllMacroColor();
               // PACKPACKA.Stop();
                //editorControl1.SetMacroColor();
                //Packa(ResultMacroColor, ResultMacroColor, true);
            }
            //catch { editorControl1.richEditControl1.Document.EndUpdate(); }           
          //  editorControl1.richEditControl1.Document.ContentChanged += change;
            PACKPACKA.Stop();
            timer1.Start();
        }
        private bool StopChange = false;
        private void PACKPACKA_Tick(object sender, EventArgs e)
        {
            PACKPACKA.Stop();
            if (!StopChange)
            SetPacka();           
            
        }

        private void timer1_Tick_1(object sender, EventArgs e)
        {
            StopChange = false;
        }

        private void dgPForms_SelectionChanged(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            dataGridView1.Columns.Clear();
            pictureBox1.Visible = false;
            string[,] arr; int rowstart;
            if (dgPForms.CurrentRow == null)
                return;
            bool Yet = false;
            for (int i = 0; i < Option.macs.Count; i++)
            {
                for (int j = 0; j < Option.macs[i].Length; j++)
                {
                    if (Option.macs[i][j].macro == dgPForms.CurrentRow.Cells[1].Value.ToString())
                        switch ((MacroType)Convert.ToInt32(dgPForms.CurrentRow.Cells[0].Value))
                        {
                            case MacroType.String:
                                if (!Yet)
                                {
                                    dataGridView1.Columns.Add(dgPForms.CurrentRow.Cells[1].Value.ToString(), dgPForms.CurrentRow.Cells[1].Value.ToString());
                                    Yet = true;
                                }
                                dataGridView1.Rows.Add(Option.macs[i][j].result);
                                break;
                            case MacroType.Rtf:
                                if (!Yet)
                                {
                                    dataGridView1.Columns.Add(dgPForms.CurrentRow.Cells[1].Value.ToString(), dgPForms.CurrentRow.Cells[1].Value.ToString());
                                    Yet = true;
                                }
                                rtf.RtfText = Option.macs[i][j].result!=null? Option.macs[i][j].result.ToString():string.Empty;
                                dataGridView1.Rows.Add(rtf.Text);
                                break;
                            case MacroType.Image:
                                if (Option.macs[i][j].result is Image)
                                    pictureBox1.Image = Option.macs[i][j].result as Image;
                                else
                                    pictureBox1.Image = null;
                                pictureBox1.Visible = true;
                                    break;                               
                            case MacroType.ArrayString:
                                if (i == 1)
                                    continue;
                                arr = Option.macs[i][j].result as string[,];
                                if (arr == null)
                                    continue;
                                if (!Yet)
                                {
                                    for (int k = 0; k < arr.GetUpperBound(1) + 1; k++)
                                        dataGridView1.Columns.Add(k.ToString(), k.ToString());
                                    Yet = true;
                                }
                                rowstart = 0;

                                for (int k = 0; k < arr.GetUpperBound(0) + 1; k++)
                                {
                                    dataGridView1.Rows.Add();
                                    if (k >= Option.macs[i][j].rowStart)
                                    {
                                        dataGridView1.Rows[k].HeaderCell.Value = (rowstart++).ToString();
                                    }
                                    for (int l = 0; l < arr.GetUpperBound(1) + 1; l++)
                                        dataGridView1.Rows[k].Cells[l].Value = arr[k, l];
                                    dataGridView1.Rows[k].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
                                }
                                return;
                            case MacroType.ArrayWithRTF:
                                if (i == 1)
                                    continue;
                                arr = Option.macs[i][j].result as string[,];
                                if (arr == null)
                                    continue;
                                if (!Yet)
                                {
                                    for (int k = 0; k < arr.GetUpperBound(1) + 1; k++)
                                        dataGridView1.Columns.Add(k.ToString(), k.ToString());
                                    Yet = true;
                                }
                                rowstart = 0;

                                for (int k = 0; k < arr.GetUpperBound(0) + 1; k++)
                                {
                                    dataGridView1.Rows.Add();
                                    if (k >= Option.macs[i][j].rowStart)
                                    {
                                        dataGridView1.Rows[k].HeaderCell.Value = (rowstart++).ToString();
                                    }
                                    for (int l = 0; l < arr.GetUpperBound(1) + 1; l++)
                                        dataGridView1.Rows[k].Cells[l].Value = arr[k, l];
                                    dataGridView1.Rows[k].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
                                }
                                break;
                        }
                }
            }
        }

        private void dgPForms_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.C)
                return;
            if (dgPForms.CurrentRow == null)
                return;
            if (dgPForms.CurrentCell == null)
                return;
            try
            {
                Clipboard.Clear();
            }
            catch { }
            try
            {
                Clipboard.SetText(dgPForms.CurrentCell.Value.ToString());
            }
            catch { }
        }

        private void button3_Click(object sender, EventArgs e)
        {

        }
    }
}
