﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MKP.PrintDevForms.Options;
using DevExpress.XtraRichEdit;
using DevExpress.XtraSpreadsheet.Services;
using DevExpress.XtraSpreadsheet.Services.Implementation;
using DevExpress.Spreadsheet;
using System.Text.RegularExpressions;
using FirebirdSql.Data.FirebirdClient;
using System.IO;
using MKP.PrintDevForms.logs;

namespace MKP.PrintDevForms
{
    public partial class SpreadForm : UserControl
    {
        public Color? MainBaseColor = Color.Black;
        public Color? ResultBaseColor = Color.Black;
        public Color ResultMacroColor = Color.FromArgb(213, 213, 255);
        RichEditControl rtf = new RichEditControl();
        public event RecognizedWordDelegate RecognozeFinished;
        //FuncSpreadForm FSF = new FuncSpreadForm();
        public SpreadForm()
        {
            InitializeComponent();
           // initThreads();
            SpreadDocument.InitSearchOptions();
            splitContainer1.SplitterDistance = 152;
            Macroses[] test = new Macroses[6];
            /* string[,] tabl = new string[10, 8];
             for (int i = 0; i < 10; i++)
             {
                 for (int j = 0; j < 8; j++)
                 {
                         tabl[i, j] = "i:"+ i + " j:" + j;
                 }
             }
             tabl[0, 0] = "Об";
             tabl[0, 1] = "Об";
             tabl[0, 2] = "Об";
             tabl[0, 3] = "Об";
             tabl[1, 0] = "Об";
             tabl[1, 1] = "Об";
             tabl[1, 2] = "Об";
             tabl[1, 3] = "Об";
             tabl[2, 0] = "Об";
             tabl[2, 1] = "Об";


            Macroses[] test2 = new Macroses[1];
            string[,] tabl2 = new string[10, 8];
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    tabl2[i, j] = "test";
                }
            }
            tabl2[0, 0] = "Об";
            tabl2[0, 1] = "Об";
            tabl2[0, 2] = "Об";
            tabl2[0, 3] = "Об";
            tabl2[1, 0] = "Об";
            tabl2[1, 1] = "Об";
            tabl2[1, 2] = "Об";
            tabl2[1, 3] = "Об";
            tabl2[2, 0] = "Об";
            tabl2[2, 1] = "Об";*/


            test[0] = new Macroses() { macro = "ololo1", result = "test1" };
            test[1] = new Macroses() { macro = "ololo2", result = "test2" };
            test[2] = new Macroses() { macro = "ololo3", result = "test3" };
            test[3] = new Macroses() { macro = "ololo4", result = "test4" };
            test[4] = new Macroses() { macro = "ololo5", result = "test5" };
            test[5] = new Macroses() { macro = "ololo6", result = "test6" };
            //test[2] = new Macroses(MacroType.ArrayString) { macro = "Таблица", result = tabl, rowStart = 2 };
            // test[3] = new Macroses(MacroType.ArrayString) { macro = "Таблица2", result = tabl, rowStart = 2 };*/
            List<Macroses[]> macro = new List<Macroses[]>();
           // test[0] = new Macroses() { macro = "ololo1", result = "test1" };
           // test[0]= new Macroses(MacroType.ArrayString) { macro = "Таблица", result = tabl, rowStart = 2 };
           // test2[0]= new Macroses(MacroType.ArrayString) { macro = "Таблица", result = tabl2, rowStart = 2 };
            macro.Add(test);
           // macro.Add(test2);
            // test2[0] = new Macroses() { macro = "ololo1", result = "test234234" };
            // macro.Add(test2);
            //macro.Add(test2);
            // macro.Add(test2);
            // macro.Add(test3);
            Option.macs = Dev.macroInit(macro);
            Option.IdScript = "-1"; Option.macs = macro;
            List<string> mas = new List<string>();
            _StaticFuntion.InitStaticFunction();
            dgPForms.Rows.Clear();
            for (int i = 0; i < _StaticFuntion.lst.Count; i++)
                dgPForms.Rows.Add(_StaticFuntion.lst[i], _StaticFuntion.lst[i].macro, _StaticFuntion.lst[i].result);
            for (int i = 0; i < macro[0].Length; i++)
            {
                dgPForms.Rows.Add((int)macro[0][i].type, macro[0][i].macro, macro[0][i].result);
                mas.Add(macro[0][i].macro);
            }
            spread1.MacroVision(true);
            //spread1.SetMacroses(mas, _StaticFuntion.lst.Select(i => i.macro).ToList());
            spread1.spreadsheetControl.ContentChanged -= changed;
            spread1.spreadsheetControl.ContentChanged += changed;        
        }

        private void selection(object sender, EventArgs e)
        {
            //FSF.Controls.Clear();
           // if (!FSF.Visible)
           //     FSF.Visible = true;
            AnalizeCurrentSection();
        }
        //TODO
        private void AnalizeCurrentSection()
        {
           // Range ran = spread1.spreadsheetControl.Document.Worksheets.ActiveWorksheet.SelectedCell;
            //if (ran == null)
               // return;

        }
        ModeStart mode = ModeStart.Стандартный_режим;
        public void LoadSpreadForm(object macro, string idScript, string ConBD, string ConBDD, ModeStart mode, int UserID, string fastSaveFullPath = "")
        {
            spread1.spreadsheetControl.ContentChanged -= changed;
          //  spread1.spreadsheetControl.SelectionChanged -= selection;
            Option.IdScript = idScript;
            Option.ConBD = ConBD;
            Option.ConBDD = ConBDD;
            Option.SavePath = fastSaveFullPath;
            Option.UserId = UserID;
            Option.macs = Dev.macroInit(macro);
            this.mode = mode;
            dgPForms.Rows.Clear();
            for (int i = 0; i < _StaticFuntion.lst.Count; i++)
                dgPForms.Rows.Add(_StaticFuntion.lst[i], _StaticFuntion.lst[i].macro, _StaticFuntion.lst[i].result);
            if (Option.macs.Count > 0)
                for (int i = 0; i < Option.macs[0].Length; i++)
                {
                    dgPForms.Rows.Add((int)Option.macs[0][i].type, Option.macs[0][i].macro, Option.macs[0][i].result);
                }
            using (FbConnection fbCon = new FbConnection(Option.ConBDD))
            {
                fbCon.Open();
                using (FbCommand fbCom = new FbCommand("SELECT formi7,formi8,formi14 FROM formi WHERE formi1 = @p1", fbCon))
                {
                    fbCom.Parameters.Add("@p1", Option.IdScript);
                    using (FbDataReader fbDr = fbCom.ExecuteReader())
                    {
                        if (fbDr.Read())
                        {
                            if (fbDr["formi14"] != DBNull.Value) Option.doc = MKP.PrintDevForms.Options.Json.Json.Deserialize<Options.DocumentOptions>(Encoding.UTF8.GetString((byte[])fbDr["formi14"]));
                            else
                                Scanned = false;
                            if (fbDr["formi8"] != DBNull.Value)
                                spread1.spreadsheetControl.LoadDocument((byte[])fbDr["formi8"], DevExpress.Spreadsheet.DocumentFormat.Xls);
                            Option.SpreadMemory = spread1.spreadsheetControl.SaveDocument(DevExpress.Spreadsheet.DocumentFormat.Xls);
                            button1.BackColor = Color.FromArgb(81, 206, 109);
                            if (!Scanned)
                                ScanMacro();
                        }
                    }
                }
            }
            if (mode != ModeStart.Стандартный_режим)
                DocPrint((int)mode);
            spread1.spreadsheetControl.ContentChanged -= changed;
            spread1.spreadsheetControl.ContentChanged += changed;
        }
        public SpreadForm(bool logged)
        {
            InitializeComponent();           
            SpreadDocument.InitSearchOptions();
            splitContainer1.SplitterDistance = 152;            
            _StaticFuntion.InitStaticFunction();           
            spread1.MacroVision(true);                  
        }
        bool Scanned = true;
        private void changed(object sender, EventArgs e)
        {
            byte[] b=spread1.spreadsheetControl.SaveDocument(DevExpress.Spreadsheet.DocumentFormat.Xls);
            if (Option.SpreadMemory != b)
            {
                Option.SpreadMemory = b;
                button1.BackColor = Color.Red;
            }
            ScanMacro();
        }
        private void ScanMacro()
        {
            reScanMacro = true;
        }
        System.Threading.Thread trMacroLive;
        private bool reScanMacro=false;
        private bool isScanned = false;
        //базовая инициализация потоков
            
        private void dgPForms_SelectionChanged(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            dataGridView1.Columns.Clear();
            pictureBox1.Visible = false;
            if (dgPForms.CurrentRow == null)
                return;
            bool Yet = false;
            for (int i = 0; i < Option.macs.Count; i++)
            {
                for (int j = 0; j < Option.macs[i].Length; j++)
                {
                    if (Option.macs[i][j].macro == dgPForms.CurrentRow.Cells[1].Value.ToString())
                        switch ((MacroType)Convert.ToInt32(dgPForms.CurrentRow.Cells[0].Value))
                        {
                            case MacroType.String:
                                if (!Yet)
                                {
                                    dataGridView1.Columns.Add(dgPForms.CurrentRow.Cells[1].Value.ToString(), dgPForms.CurrentRow.Cells[1].Value.ToString());
                                    Yet = true;
                                }
                                dataGridView1.Rows.Add(Option.macs[i][j].result);
                                break;
                            case MacroType.Rtf:
                                if (!Yet)
                                {
                                    dataGridView1.Columns.Add(dgPForms.CurrentRow.Cells[1].Value.ToString(), dgPForms.CurrentRow.Cells[1].Value.ToString());
                                    Yet = true;
                                }
                                rtf.RtfText = Option.macs[i][j].result.ToString();
                                dataGridView1.Rows.Add(rtf.Text);
                                break;
                            case MacroType.Image:
                                if (Option.macs[i][j].result is Image)
                                    pictureBox1.Image = Option.macs[i][j].result as Image;
                                else
                                    pictureBox1.Image = null;
                                pictureBox1.Visible = true;
                                break;
                            case MacroType.ArrayString:
                                if (i == 1)
                                    continue;
                                string[,] arr = Option.macs[i][j].result as string[,];
                                if (arr == null)
                                    continue;
                                if (!Yet)
                                {
                                    for (int k = 0; k < arr.GetUpperBound(1) + 1; k++)
                                        dataGridView1.Columns.Add(k.ToString(), k.ToString());
                                    Yet = true;
                                }
                                int rowstart = 0;

                                for (int k = 0; k < arr.GetUpperBound(0) + 1; k++)
                                {
                                    dataGridView1.Rows.Add();
                                    if (k >= Option.macs[i][j].rowStart)
                                    {
                                        dataGridView1.Rows[k].HeaderCell.Value = (rowstart++).ToString();
                                    }
                                    for (int l = 0; l < arr.GetUpperBound(1) + 1; l++)
                                        dataGridView1.Rows[k].Cells[l].Value = arr[k, l];
                                    dataGridView1.Rows[k].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
                                }
                                return;
                                // break;
                        }
                }
            }
        }

        private void dgPForms_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.C)
                return;
            if (dgPForms.CurrentRow == null)
                return;
            if (dgPForms.CurrentCell == null)
                return;
            try
            {
                Clipboard.Clear();
            }
            catch { }
            try
            {
                Clipboard.SetText(dgPForms.CurrentCell.Value.ToString());
            }
            catch { }
        }
        private void button2_Click(object sender, EventArgs e)
        {
            DocPrint(2);
        }        
        private void PreInitializeWorkSpace(ResultSpreadSheet result)
        {
            result.spread1.spreadsheetControl.Document.LoadDocument(spread1.spreadsheetControl.Document.SaveDocument(DevExpress.Spreadsheet.DocumentFormat.Xls), DevExpress.Spreadsheet.DocumentFormat.Xls);
            /*for (int i = 0; i < spread1.spreadsheetControl.Document.Worksheets.Count; i++)
            {
                if (i != 0)
                {
                    result.spread1.spreadsheetControl.Document.Worksheets.Add(spread1.spreadsheetControl.Document.Worksheets[i].Name);
                }
                else
                    result.spread1.spreadsheetControl.Document.Worksheets[i].Name = spread1.spreadsheetControl.Document.Worksheets[i].Name;
                result.spread1.spreadsheetControl.Document.Worksheets[i].CopyFrom(spread1.spreadsheetControl.Document.Worksheets[i]);
                result.spread1.spreadsheetControl.Document.Worksheets[i].Cells[1, 1].Style.Font.Color = Color.Black;
                //result.spread1.spreadsheetControl.Document.Worksheets[i].Cells.Style.Font.Color = Color.Black;
            }*/
        }
        
        private List<int> FindWorkMacro()
        {
            isScanned = true;
            List<int> idMacroRun = new List<int>();
            int leng = Option.macs[0].Count();
            var sheets = spread1.spreadsheetControl.Document.Worksheets;
            foreach (var sheet in sheets)           
                idMacroRun.AddRange(FindParallel(leng, sheet));
            isScanned = false;
            return idMacroRun;
        }

        private List<int> FindParallel(int leng, Worksheet sheet)
        {
            List<int> idMacroRun = new List<int>();
            sheet.Calculate();
            //loop:
            //try
            //{    
            for (int i = 0; i < leng; i++)
            {
                if (sheet.Search(Option.macs[0][i].macro, SpreadDocument.options).Count() > 0)
                    idMacroRun.Add(i);
            }
                /*System.Threading.Tasks.Parallel.For(0, leng, i =>
                {
                    if (sheet.Search(Option.macs[0][i].macro, SpreadDocument.options).Count() > 0)
                        idMacroRun.Add(i);
                });*/
           // }
          //  catch
         //   {
           //     System.Threading.Thread.Sleep(200);
            //    goto loop;
           // }
            return idMacroRun;
        }
        
        public void DocPrint(int type)
        {
            // while (isScanned|| reScanMacro)            
            //      System.Threading.Thread.Sleep(200);
            //GC.Collect();        
            bool onefile = false;   
            Option.doc.idMacroRun = new List<int>();
            Option.doc.idMacroSubDocumentRun = new List<int>();
            Errors.err = new logs.Errors();
            List<ResultSpreadSheet> results = new List<ResultSpreadSheet>();
            results.Add(new ResultSpreadSheet());
            if (spread1.spreadsheetControl.Document.Worksheets.Count>0)
            foreach (var sled in spread1.spreadsheetControl.Document.Worksheets[0].Search("{Одним файлом}"))
            {
                    if (onefile)
                    {
                        Errors.err.Add("{Одним файлом}", "Данный макрос должен находиться в документе лишь в одном экземпляре");
                        break;
                    }
                    onefile = true;                    
            }
            PreInitializeWorkSpace(results[0]);  
            bool open = true;
            bool nextLetter = false;
            int len=0;
            if (Option.macs != null && Option.macs.Count > 0)
            {
                len = Option.macs[0].Length;
                foreach (Worksheet sheet in results[0].spread1.spreadsheetControl.Document.Worksheets)
                    foreach (DevExpress.Spreadsheet.Cell cell in sheet.Search("{", SpreadDocument.options))
                    {
                        string value = cell.HasFormula ? cell.Formula : cell.DisplayText;
                        foreach (Match item in Generate.CommonRegex.Matches(value))//cell.Value.ToString()
                        {                            
                            for (int i = 0; i < len; i++)
                            {
                                if (Option.macs[0][i].macro == item.Value)
                                {
                                    if (!Option.doc.idMacroRun.Contains(i))
                                        Option.doc.idMacroRun.Add(i);
                                    break;
                                }
                            }
                        }  
                      
                       /* if (!text.Contains("}"))
                            continue;
                        if (!text.StartsWith("{"))
                            text = text.Remove(0, text.IndexOf("{"));
                        if (!text.EndsWith("}"))
                            text = text.Remove(text.IndexOf("}")+1);*/                        
                    }                                 
                // idMacroRun = FindWorkMacro();
                int EndIndex = 0;
                if (Option.doc.idMacroRun.Count>0)
                EndIndex = Option.doc.idMacroRun.Max();
                ResultSpreadSheet original = new ResultSpreadSheet();
                //on save or open few documents??? 
                for (int j = 0,h=0; j < Option.macs.Count; j++)
                {
                    if (j != 0)
                    {
                        if (!nextLetter)
                        {
                            h++;
                            results.Add(new ResultSpreadSheet());
                            PreInitializeWorkSpace(results[h]); 
                        } else
                        {
                            results[h].spread1.spreadsheetControl.Document.Worksheets.Add();
                            results[h].spread1.spreadsheetControl.Document.Worksheets[results[h].spread1.spreadsheetControl.Document.Worksheets.Count - 1].CopyFrom(original.spread1.spreadsheetControl.Document.Worksheets[0]);
                        }
                    }
                    if (j == 0&& results[0].spread1.spreadsheetControl.Document.Worksheets.Count>0)
                    {
                        foreach (var sled in spread1.spreadsheetControl.Document.Worksheets[0].Search("{Следующий лист}"))
                        {
                            original.spread1.spreadsheetControl.Document.Worksheets[0].CopyFrom(spread1.spreadsheetControl.Document.Worksheets[0]);
                            nextLetter = true;
                            break;
                        }
                        foreach (var sled in original.spread1.spreadsheetControl.Document.Worksheets[0].Search("{Следующий лист}"))
                        {
                            sled.Value = sled.Value.ToString().Replace("{Следующий лист}",string.Empty);
                        }
                    }
                    if (!nextLetter)
                        foreach (Worksheet sheet in results[j].spread1.spreadsheetControl.Document.Worksheets)
                        {
                            //recognition macros 
                            open = Recognition(results, open, EndIndex, j, sheet);
                        }
                    else
                    {
                        open = Recognition(results, open, EndIndex, j, results[h].spread1.spreadsheetControl.Document.Worksheets[results[h].spread1.spreadsheetControl.Document.Worksheets.Count - 1]);
                    }
                }
                foreach (var item in results)
                    item.spread1.spreadsheetControl.Document.CalculateFull();
            }
            if (nextLetter)
                foreach (var sled in results[0].spread1.spreadsheetControl.Document.Worksheets[0].Search("{Следующий лист}"))
                {    
                    sled.Value = sled.Value.ToString().Replace("{Следующий лист}", string.Empty);
                }
            if (onefile)
            {
                SetOneResult(results);
            }
            foreach (var item in results)
            {
                if (item.spread1.spreadsheetControl.Document.Worksheets.Count>0)
                SaveDocument(item, item.spread1.spreadsheetControl.Document.Worksheets[0]);
            }
            if (type == 6)
            {               
                RecognozeFinished?.Invoke(EditorType.Spread, results[0].spread1.spreadsheetControl);
                foreach (var item in results)
                    item.Close();
            }else
            if (open)
                foreach (var item in results)
                    item.Show();
            else
            {
                foreach (var item in results)
                    item.Close();
                MessageBox.Show("Выполнено!");
            }

        }

        private void SetOneResult(List<ResultSpreadSheet> results)
        {
            DevExpress.Spreadsheet.Cell head = null;
            if (results.Count <= 1)
            {
                for (int i = 0; i < results.Count; i++)
                {
                    head = spread1.spreadsheetControl.Document.Worksheets[0].Search("{Одним файлом}").ToList()[0];
                    head.Value=head.Value.ToString().Replace("{Одним файлом}", string.Empty);
                }
                return;
            }
            head = spread1.spreadsheetControl.Document.Worksheets[0].Search("{Одним файлом}").ToList()[0];
            ResultSpreadSheet spread = new ResultSpreadSheet();
            spread.spread1.spreadsheetControl.Document.LoadDocument(results[0].spread1.spreadsheetControl.Document.SaveDocument(DevExpress.Spreadsheet.DocumentFormat.Xls), DevExpress.Spreadsheet.DocumentFormat.Xls);            
            for (int i = 1; i < results.Count; i++)
            {             
                var end = spread.spread1.spreadsheetControl.Document.Worksheets[0].Search("{Одним файлом}").ToList()[0];

                end.Value = end.Value.ToString().Replace("{Одним файлом}",string.Empty);

                var start = results[i].spread1.spreadsheetControl.Document.Worksheets[0].Search("{Одним файлом}").ToList()[0];

                //start.Value = start.Value.ToString().Replace("{Одним файлом}", string.Empty);

                var paste = results[i].spread1.spreadsheetControl.Document.Worksheets[0].Range.FromLTRB(0, 0, end.ColumnIndex+1, end.RowIndex+1);

                spread.spread1.spreadsheetControl.Document.Worksheets[0].Range.FromLTRB(0, end.RowIndex, start.ColumnIndex, end.RowIndex+start.RowIndex).CopyFrom(paste,PasteSpecial.All);
            }
            foreach (var item in spread.spread1.spreadsheetControl.Document.Worksheets[0].Search("{Одним файлом}"))
            {
                item.Value=item.Value.ToString().Replace("{Одним файлом}", string.Empty);
            }
            results.Clear();
            results.Add(spread);
        }

        private bool Recognition(List<ResultSpreadSheet> results, bool open, int EndIndex, int j, Worksheet sheet)
        {
            int rez = results.Count - 1;
            for (int i = 0; i < Option.macs[j].Length; i++)
            {
                if (!Option.doc.idMacroRun.Contains(i))
                    continue;
                if (Option.macs[j][i].result == null)
                    continue;
                {
                    CollectionInfo.RowInserted.Clear();
                    switch (Option.macs[j][i].type)
                    {
                        case MacroType.String:
                            //goto on all worksheet

                            //find all needed cell
                            foreach (DevExpress.Spreadsheet.Cell cell in sheet.Search(Option.macs[j][i].macro, SpreadDocument.options))
                            {
                                //switch this cell.value type???(maybe convert value or ???)
                                //TODO
                                if (cell.HasFormula)
                                cell.Formula = cell.Formula.Replace(Option.macs[j][i].macro, Option.macs[j][i].result.ToString());
                                else
                                    cell.Value= cell.DisplayText.Replace(Option.macs[j][i].macro, Option.macs[j][i].result.ToString());
                                cell.Font.Color = Color.Black;
                            }
                            break;
                        case MacroType.Rtf:
                            //TODO
                            //MacroRTF(result, j, i);
                            break;
                        case MacroType.ArrayString:
                            if (!(Option.macs[j][i].result is string[,]))
                                continue;
                            string[,] arr = Option.macs[j][i].result as string[,];
                            if (Option.macs[j][i].rowStart != 0)
                            {
                                string[,] t2 = new string[arr.GetLength(0) - Option.macs[j][i].rowStart, arr.GetLength(1)];
                                Array.Copy(arr, Option.macs[j][i].rowStart * t2.GetLength(1), t2, 0, t2.Length);
                                arr = t2;
                            }
                            MacroArrString(sheet, results[rez], j, i, arr);
                            break;
                        case MacroType.Image:
                            if (!(Option.macs[j][i].result is Image))
                                continue;
                            foreach (DevExpress.Spreadsheet.Cell cell in sheet.Search(Option.macs[j][i].macro, SpreadDocument.options))
                            {
                                if (cell.HasFormula)
                                cell.Formula = cell.Formula.Replace(Option.macs[j][i].macro, Option.macs[j][i].result.ToString());
                                else
                                    cell.Value= cell.DisplayText.Replace(Option.macs[j][i].macro, Option.macs[j][i].result.ToString());
                                cell.Font.Color = Color.Black;
                                sheet.Pictures.AddPicture(Option.macs[j][i].result as Image, cell);
                            }
                            break;
                    }
                    if (!currs.Contains(sheet) && (EndIndex == i))
                    {
                        currs.Add(sheet);
                        open = SpecialMacro(results[rez], sheet, i, EndIndex);
                    }
                }

            }

            return open;
        }

        List<Worksheet> currs = new List<Worksheet>(); 
        private bool SpecialMacro(ResultSpreadSheet result,Worksheet sheet,int j,int EndIndex)
        {
            bool Open = true;
            for (int i = 0; i < _StaticFuntion.lst.Count; i++)
            {
                switch (_StaticFuntion.lst[i].type)
                {
                    case MacroType.TableHeaderDub:                      
                        break;
                    case MacroType.TableMergeCellFuncs:
                        TableMergeCellFuncs(sheet, i);
                        break;
                    case MacroType.Floor:
                        FloorFunc(sheet, i);
                        break;
                    case MacroType.TernarLogic:
                        TernarFunc(sheet, i);
                        break;
                    case MacroType.LowerFirsteCharacterString:
                        if (EndIndex == j)
                            LowerFirsteCharacterString(sheet);
                        break;
                    case MacroType.UpperFirsteCharacterString:
                        if (EndIndex == j)
                            UpperFirsteCharacterString(sheet);
                        break;
                   /* case MacroType.SaveDocument:
                        if (EndIndex == j)
                            SaveDocument(result, sheet);
                        break;*/
                    case MacroType.CellCopy:
                        if (EndIndex == j)
                            CellBorderCopy(result, sheet,true);
                        break;
                    case MacroType.CellBorderCopy:
                        if (EndIndex == j)
                        CellBorderCopy(result, sheet,false);
                        break;
                    case MacroType.NotOpen:
                        Open = NotOpen(sheet);
                        break;                   
                    //Запрос
                    case MacroType.Request:
                        break;
                }
            }
            return Open;
        }

        private void FloorFunc(Worksheet sheet, int i)
        {
            foreach (DevExpress.Spreadsheet.Cell cell in sheet.Search(@"Округлить", SpreadDocument.options))
            {
                if (cell.HasFormula)
                foreach (System.Text.RegularExpressions.Match match in (new Regex(@"{Округлить\(.+\)}", RegexOptions.CultureInvariant)).Matches(cell.Formula))
                {
                    double value;int znak;
                    string text = match.Value.Replace(Environment.NewLine, string.Empty).Replace(",", Option.separator).Replace(".", Option.separator).Replace("{Округлить(", string.Empty).Replace(")}", string.Empty);
                    string[] Save = text.Split(new char[] { ';' }, StringSplitOptions.None);
                    switch (Save.Length)
                    {
                        case 0:
                            if (double.TryParse(text, out value))
                            {
                                cell.Formula = cell.Formula.Replace(match.Value, Math.Round(value).ToString());
                                cell.Font.Color = Color.Black;
                                continue;
                            }
                            //TODO
                            cell.Formula = cell.Formula.Replace(match.Value, string.Empty);
                            break;
                        case 2:
                            if (double.TryParse(Save[0], out value)&& int.TryParse(Save[1], out znak))
                            {
                                cell.Formula = cell.Formula.Replace(match.Value, Math.Round(value,znak,MidpointRounding.AwayFromZero).ToString());
                                cell.Font.Color = Color.Black;
                                continue;
                            }
                            cell.Formula = cell.Formula.Replace(match.Value, string.Empty);
                            break;
                        default:
                            cell.Formula = cell.Formula.Replace(match.Value, string.Empty);break;
                    }
                   
                }
                else
                    foreach (System.Text.RegularExpressions.Match match in (new Regex(@"{Округлить\(.+\)}", RegexOptions.CultureInvariant)).Matches(cell.DisplayText))
                    {
                        double value; int znak;
                        string text = match.Value.Replace(Environment.NewLine, string.Empty).Replace(",", Option.separator).Replace(".", Option.separator).Replace("{Округлить(", string.Empty).Replace(")}", string.Empty);
                        string[] Save = text.Split(new char[] { ';' }, StringSplitOptions.None);
                        switch (Save.Length)
                        {
                            case 0:
                                if (double.TryParse(text, out value))
                                {
                                    cell.Value = cell.DisplayText.Replace(match.Value, Math.Round(value).ToString());
                                    cell.Font.Color = Color.Black;
                                    continue;
                                }
                                //TODO
                                cell.Value = cell.DisplayText.Replace(match.Value, string.Empty);
                                break;
                            case 2:
                                if (double.TryParse(Save[0], out value) && int.TryParse(Save[1], out znak))
                                {
                                    cell.Value = cell.DisplayText.Replace(match.Value, Math.Round(value, znak, MidpointRounding.AwayFromZero).ToString());
                                    cell.Font.Color = Color.Black;
                                    continue;
                                }
                                cell.Value = cell.DisplayText.Replace(match.Value, string.Empty);
                                break;
                            default:
                                cell.Value = cell.DisplayText.Replace(match.Value, string.Empty); break;
                        }

                    }

            }
        }

        private bool NotOpen(Worksheet sheet)
        {
            foreach (DevExpress.Spreadsheet.Cell cell in sheet.Search(@"{Не открывать}", SpreadDocument.options))
            {
                      cell.Value = cell.Formula.Replace(@"{Не открывать}", string.Empty);
                    cell.Font.Color = Color.Black;
                return false;
             }
            return true;
        }
        private void TernarFunc(Worksheet sheet, int i)
        {
            foreach (DevExpress.Spreadsheet.Cell cell in sheet.Search("^ЕСЛИ(", SpreadDocument.options))
            {
                string find = cell.HasFormula ? cell.Formula : cell.DisplayText;
                foreach (System.Text.RegularExpressions.Match match in Generate.Ternar.Matches(find))
                {
                    string text = match.Value;
                    text = text.Replace("^ЕСЛИ(",string.Empty);
                    string tempValues = text.Remove(text.IndexOf(")?'"));
                    string[] Comparsion = tempValues.Split(Generate.splitLogic, StringSplitOptions.None);
                    if (Comparsion.Length != 2)
                        return;
                    text = text.Remove(0, text.IndexOf(")?'")+3);
                    text = text.Remove(text.Length - 2, 2);
                    string[] Values = text.Split(new string[] { "':'" }, StringSplitOptions.None);
                    if (Values.Length != 2)
                        return;        
                    if (cell.HasFormula)            
                    cell.Formula= find.Replace(match.Value, Generate.GetResultLogic(Generate.GetLogic(tempValues), Comparsion, Values));
                    else
                        cell.Value = find.Replace(match.Value, Generate.GetResultLogic(Generate.GetLogic(tempValues), Comparsion, Values));
                    cell.Font.Color = Color.Black;
                }
            }
        }

        private void CellBorderCopy(ResultSpreadSheet result, Worksheet sheet,bool copyAll=false)
        {
            string type = "Линии";
            if (copyAll)
                type = "Дубль";
            foreach (DevExpress.Spreadsheet.Cell cell in sheet.Search(type, SpreadDocument.options))
            {
                if (cell.HasFormula)
                {
                    foreach (System.Text.RegularExpressions.Match match in (new Regex(@"{" + type + @"\[\d*\]}", RegexOptions.CultureInvariant)).Matches(cell.Formula))
                    {
                        string text = match.Value;
                        text = text.Remove(0, text.IndexOf("[") + 1);
                        text = text.Remove(text.IndexOf("]"));
                        int indexOf = -1;
                        cell.Formula = cell.Formula.Replace(match.Value, string.Empty);
                        cell.Font.Color = Color.Black;
                        if (!int.TryParse(text, out indexOf))
                            continue;
                        SpreadCell.BorderCellData borderOptions = new SpreadCell.BorderCellData(sheet.Cells[cell.RowIndex, indexOf].Borders, new SpreadCell.MergedCellData(cell.GetMergedRanges()));
                        int row = cell.RowIndex + 1;
                        while (borderOptions.CheckIdenticBorders(sheet.Cells[row, indexOf].Borders))
                        {
                            borderOptions.CopyBorderStyle(sheet.Cells[row, cell.ColumnIndex]);
                            if (copyAll)
                            {
                                sheet.Cells[row, cell.ColumnIndex].Value = cell.Value;
                                sheet.Cells[row, cell.ColumnIndex].Formula = cell.Formula;
                            }
                            row++;
                        }
                    }
                }
                else
                foreach (System.Text.RegularExpressions.Match match in (new Regex(@"{"+ type + @"\[\d*\]}", RegexOptions.CultureInvariant)).Matches(cell.DisplayText))
                {
                    string text = match.Value;
                    text = text.Remove(0, text.IndexOf("[") + 1);
                    text = text.Remove(text.IndexOf("]"));
                    int indexOf = -1;
                    cell.Value = cell.DisplayText.Replace(match.Value, string.Empty);
                    cell.Font.Color = Color.Black;
                    if (!int.TryParse(text, out indexOf))
                        continue;
                    SpreadCell.BorderCellData borderOptions = new SpreadCell.BorderCellData(sheet.Cells[cell.RowIndex,indexOf].Borders,new SpreadCell.MergedCellData(cell.GetMergedRanges()));
                    int row = cell.RowIndex+1;
                    while (borderOptions.CheckIdenticBorders(sheet.Cells[row, indexOf].Borders))
                    {
                        borderOptions.CopyBorderStyle(sheet.Cells[row, cell.ColumnIndex]);
                        if (copyAll)
                        sheet.Cells[row, cell.ColumnIndex].Value = cell.Value;
                        row++;
                    }
                }
            }
        }
        private void SaveDocument(ResultSpreadSheet result, Worksheet sheet)
        {
            SaveXLS(result, sheet);
            SavePDF(result, sheet);
        }

        private void SavePDF(ResultSpreadSheet result, Worksheet sheet)
        {
            sheet.Workbook.AddService(typeof(IChartControllerFactoryService), new ChartControllerFactoryService());
            sheet.Workbook.AddService(typeof(IChartImageService), new ChartImageService());
            foreach (DevExpress.Spreadsheet.Cell cell in sheet.Search(@"Сохранить как", SpreadDocument.options))
            {
                foreach (System.Text.RegularExpressions.Match match in (new Regex(@"{Сохранить pdf\(.+\,.+\)}", RegexOptions.CultureInvariant)).Matches(cell.HasFormula?cell.Formula:cell.DisplayText))
                {
                    if (string.IsNullOrEmpty(Option.FileName))
                    {
                        using (FbConnection fConnection = new FbConnection(Option.ConBD))
                        {
                            fConnection.Open();
                            using (FbCommand fCommand = new FbCommand(@"select i5 from i where i1=" + Option.UserId, fConnection))
                            {
                                using (FbDataReader fReader = fCommand.ExecuteReader())
                                {
                                    if (fReader.Read())
                                    {
                                        Option.FileName = fReader[0]?.ToString();
                                        if (string.IsNullOrEmpty(Option.FileName))
                                            continue;
                                        string text = match.Value;
                                        string[] Save = text.Split(new char[] { '(', ',', ')' }, StringSplitOptions.RemoveEmptyEntries);
                                        if (Save.Length < 4)
                                            continue;
                                        StringBuilder Lower = new StringBuilder(match.Value);
                                        Lower[0] = char.ToLower(Lower[0]);
                                        cell.Value = string.Empty;
                                        cell.Formula = string.Empty;
                                        string path = (Option.FileName + @"\" + Save[1]).Replace("\"", string.Empty);
                                        if (!Directory.Exists(path))
                                            Directory.CreateDirectory(path);
                                        sheet.Workbook.ExportToPdf((Option.FileName + @"\" + Save[1] + @"\" + Save[2] + ".pdf").Replace("\"", string.Empty));
                                       // result.spread1.spreadsheetControl.SaveDocument((Option.FileName + @"\" + Save[1] + @"\" + Save[2] + ".xls").Replace("\"", string.Empty), DevExpress.Spreadsheet.DocumentFormat.Xls);
                                    }
                                    else
                                        continue;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (!Directory.Exists(Option.FileName))
                            Directory.CreateDirectory(Option.FileName);
                        sheet.Workbook.ExportToPdf(Option.FileName);
                        //result.spread1.spreadsheetControl.SaveDocument(Option.FileName, DevExpress.Spreadsheet.DocumentFormat.Xls);
                    }
                    Option.FileName = string.Empty;
                }
            }
        }

        private static void SaveXLS(ResultSpreadSheet result, Worksheet sheet)
        {
            foreach (DevExpress.Spreadsheet.Cell cell in sheet.Search(@"Сохранить как", SpreadDocument.options))
            {
                foreach (System.Text.RegularExpressions.Match match in (new Regex(@"{Сохранить как\(.+\,.+\)}", RegexOptions.CultureInvariant)).Matches(cell.HasFormula?cell.Formula:cell.DisplayText))
                {
                    if (string.IsNullOrEmpty(Option.FileName))
                    {
                        using (FbConnection fConnection = new FbConnection(Option.ConBD))
                        {
                            fConnection.Open();
                            using (FbCommand fCommand = new FbCommand(@"select i5 from i where i1=" + Option.UserId, fConnection))
                            {
                                using (FbDataReader fReader = fCommand.ExecuteReader())
                                {
                                    if (fReader.Read())
                                    {
                                        Option.FileName = fReader[0]?.ToString();
                                        if (string.IsNullOrEmpty(Option.FileName))
                                            continue;
                                        string text = match.Value;
                                        string[] Save = text.Split(new char[] { '(', ',', ')' }, StringSplitOptions.RemoveEmptyEntries);
                                        if (Save.Length < 4)
                                            continue;
                                        StringBuilder Lower = new StringBuilder(match.Value);
                                        Lower[0] = char.ToLower(Lower[0]);
                                        cell.Value = string.Empty;
                                        cell.Formula = string.Empty;
                                        string path = (Option.FileName + @"\" + Save[1]).Replace("\"", string.Empty);
                                        if (!Directory.Exists(path))
                                            Directory.CreateDirectory(path);
                                        result.spread1.spreadsheetControl.SaveDocument((Option.FileName + @"\" + Save[1] + @"\" + Save[2] + ".xls").Replace("\"", string.Empty), DevExpress.Spreadsheet.DocumentFormat.Xls);
                                    }
                                    else
                                        continue;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (!Directory.Exists(Option.FileName))
                            Directory.CreateDirectory(Option.FileName);
                        result.spread1.spreadsheetControl.SaveDocument(Option.FileName, DevExpress.Spreadsheet.DocumentFormat.Xls);
                    }
                    Option.FileName = string.Empty;
                }
            }
        }

        private void UpperFirsteCharacterString(Worksheet sheet)
        {
            foreach (DevExpress.Spreadsheet.Cell cell in sheet.Search(@"Строка", SpreadDocument.options))
            {
                foreach (System.Text.RegularExpressions.Match match in (new Regex(@"{Строка\(.+\)}", RegexOptions.CultureInvariant)).Matches(cell.HasFormula?cell.Formula:cell.DisplayText))
                {
                    StringBuilder Lower = new StringBuilder(match.Value);
                    Lower[0] = char.ToLower(Lower[0]);
                    if (cell.HasFormula)
                    cell.Formula = cell.Formula.Replace(match.Value, Lower.Replace("{Строка(", string.Empty).Replace(")}", string.Empty).ToString());
                    else
                        cell.Value = cell.DisplayText.Replace(match.Value, Lower.Replace("{Строка(", string.Empty).Replace(")}", string.Empty).ToString());
                    cell.Font.Color = Color.Black;
                }
            }
        }

        private void LowerFirsteCharacterString(Worksheet sheet)
        {
            foreach (DevExpress.Spreadsheet.Cell cell in sheet.Search(@"строка", SpreadDocument.options))
            {
                foreach (System.Text.RegularExpressions.Match match in (new Regex(@"{строка\(.+\)}", RegexOptions.CultureInvariant)).Matches(cell.HasFormula ? cell.Formula : cell.DisplayText))
                {
                    StringBuilder Lower = new StringBuilder(match.Value);
                    Lower[0] = char.ToLower(Lower[0]);
                    if (cell.HasFormula)
                    cell.Value = cell.DisplayText.Replace(match.Value, Lower.Replace("{строка(", string.Empty).Replace(")}", string.Empty).ToString());
                    else
                        cell.Formula = cell.Formula.Replace(match.Value, Lower.Replace("{строка(", string.Empty).Replace(")}", string.Empty).ToString());
                    cell.Font.Color = Color.Black;
                }
            }
        }
        //\[\d*\]
        private void TableMergeCellFuncs(Worksheet sheet, int i)
        {
            foreach (DevExpress.Spreadsheet.Cell cell in sheet.Search(_StaticFuntion.lst[i].macro, SpreadDocument.options))
            {
                bool minus = false;
                bool horizontal = false;
                foreach (System.Text.RegularExpressions.Match match in (new Regex(_StaticFuntion.lst[i].macro + @"\[.+\]", RegexOptions.CultureInvariant)).Matches(cell.HasFormula ? cell.Formula : cell.DisplayText))
                {
                    string text = match.Value;
                    if (text.Contains("-"))
                    {
                        text= text.Replace("-", "");
                        minus = true;
                    }
                    if (text.Contains("*"))
                    {
                        text = text.Replace("*", "");
                        horizontal = true;
                    }
                    text = text.Remove(0, text.IndexOf("[") + 1);
                    text = text.Remove(text.IndexOf("]"));
                    int indexOf = -1;
                    if (cell.HasFormula)
                    cell.Formula = cell.Formula.Replace(match.Value, string.Empty);
                    else
                        cell.Value = cell.DisplayText.Replace(match.Value, string.Empty);
                    cell.Font.Color = Color.Black;
                    if (!int.TryParse(text, out indexOf))
                        continue;
                    if (minus)
                        indexOf = -indexOf;
                    if (!horizontal)                    
                        sheet.MergeCells(sheet.Range.FromLTRB(cell.ColumnIndex, cell.RowIndex, cell.ColumnIndex, cell.RowIndex + indexOf - 1));
                     else
                        sheet.MergeCells(sheet.Range.FromLTRB(cell.ColumnIndex, cell.RowIndex, cell.ColumnIndex + indexOf - 1, cell.RowIndex));
                }
            }
        }

        private void MacroArrString(Worksheet sheet, ResultSpreadSheet resultSpreadSheet, int j, int i,string[,] arr)
        {
            Regex OneItemArray = new Regex(Option.macs[j][i].macro + Generate.pattern1, RegexOptions.CultureInvariant);
            Regex ColumnGenerator = new Regex(Option.macs[j][i].macro + Generate.pattern2, RegexOptions.CultureInvariant);
            Regex TableGenerator = new Regex(Option.macs[j][i].macro + Generate.pattern3, RegexOptions.CultureInvariant);
            foreach (DevExpress.Spreadsheet.Cell cell in sheet.Search(Option.macs[j][i].macro, SpreadDocument.options))
            {               
               SpreadDocument.OneOfArray(arr, OneItemArray, cell);
               SpreadDocument.ColumnsGen(sheet, arr, ColumnGenerator, cell);
            }
            SpreadDocument.TableGen(sheet, j, i, arr, TableGenerator);
        }
        

        private void button1_Click(object sender, EventArgs e)
        {
            button1.BackColor = Color.FromArgb(81, 206, 109);
            using (FbConnection fConnection = new FbConnection(Option.ConBDD))
            {
                fConnection.Open();
                using (FbTransaction fTrans = fConnection.BeginTransaction())
                {
                    using (FbCommand fCommand = new FbCommand(@"UPDATE formi 
                        SET formi8 = @p2, formi9=2,formi14=@formi14 WHERE formi1 = @p1", fConnection, fTrans))
                    {
                        fCommand.Parameters.Add("@p2", spread1.spreadsheetControl.SaveDocument(DevExpress.Spreadsheet.DocumentFormat.Xls));
                        fCommand.Parameters.Add("@formi14",Encoding.UTF8.GetBytes(MKP.PrintDevForms.Options.Json.Json.Serialize<Options.DocumentOptions>(Option.doc)));
                        fCommand.Parameters.Add("@p1", Option.IdScript);
                        fCommand.ExecuteNonQuery();
                        fTrans.Commit();
                    }
                }
            }
        }
    }
}
