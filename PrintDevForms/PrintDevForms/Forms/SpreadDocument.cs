﻿using DevExpress.Spreadsheet;
using MKP.PrintDevForms.Options;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;

namespace MKP.PrintDevForms
{
    public static class SpreadDocument
    {
        public static DevExpress.Spreadsheet.SearchOptions options;
        /// <summary>
        /// Generate Search Option
        /// </summary>
        public static void InitSearchOptions()
        {
            options = new DevExpress.Spreadsheet.SearchOptions();
            options.MatchCase = true;
            options.SearchIn = SearchIn.ValuesAndFormulas;
            options.SearchBy = SearchBy.Columns | SearchBy.Rows;
            options.MatchEntireCellContents = false;
        }
        public static void YetGenerator(ref bool yet, bool value)
        {
            if (!yet)
                yet = value;
        }
                
        public static bool WorkTableGeneration(Worksheet sheet, string[,] arr, MatchCollection cellMathes, DevExpress.Spreadsheet.Cell cell)
        {
            if (cellMathes.Count == 0)
                return false;
            foreach (System.Text.RegularExpressions.Match match in cellMathes)
            {
                Generate.TextOption opt = new Generate.TextOption(match.Value);
                if (opt.values.Length < 2)
                    return false;
                cell.Font.Color = Color.Black;
                SpreadCell.AlignmentCellData AlignmentOptions = new SpreadCell.AlignmentCellData(cell.Alignment);              
                cell.GetMergedRanges();
                CellValue val = cell.Value;
                Style style = cell.Style;
                SpreadCell.BorderCellData borderOptions = new SpreadCell.BorderCellData(cell.Borders);               
                int rowIndex = cell.RowIndex;
                double wid = cell.ColumnWidth;
                string text = cell.HasFormula?cell.Formula:cell.DisplayText;
               // var c = cell.Style;
                int num = arr.GetLength(1) - opt.values[1]- opt.values[0];
                string prev = string.Empty; //bool Inserted = false;
               // if (!Inserted)
                {
                    if (opt.columnInsert)
                    {
                        sheet.Cells.BeginUpdate();
                       // sheet.Cells[cell.RowIndex, cell.ColumnIndex].Value = val;
                       if (num-1>0)
                        sheet.Columns.Insert(cell.ColumnIndex+1, num-1); 
                       // sheet.Cells[cell.RowIndex, cell.ColumnIndex].Value = val;
                        sheet.Cells[cell.RowIndex, cell.ColumnIndex].Style = style;
                        sheet.Cells[cell.RowIndex, cell.ColumnIndex].ColumnWidth = wid;
                        for (int i = 0; i < num; i++)
                        {
                            if (i != 0)
                            {                                
                                if (opt.ObTextIs)
                                {                                   
                                    sheet.MergeCells(sheet.Range.FromLTRB(cell.ColumnIndex, cell.RowIndex+opt.ObRowIndex, cell.ColumnIndex+i, cell.RowIndex + opt.ObRowIndex), MergeCellsMode.Default); 
                                }
                                sheet.Cells[cell.RowIndex, cell.ColumnIndex + i].Formula = cell.Formula;
                                sheet.Cells[cell.RowIndex, cell.ColumnIndex + i].Value = val;
                                sheet.Cells[cell.RowIndex, cell.ColumnIndex + i].Style = style;
                                sheet.Cells[cell.RowIndex, cell.ColumnIndex + i].ColumnWidth = wid;
                                borderOptions.CopyBorderStyle(sheet.Cells[cell.RowIndex, cell.ColumnIndex + i]);
                                AlignmentOptions.CopyAlignmentStyle(sheet.Cells[cell.RowIndex, cell.ColumnIndex + i].Alignment);
                            }
                            else
                            {
                                sheet.Cells[cell.RowIndex, cell.ColumnIndex + i].Value = val;
                            }
                        }
                        if (opt.ObTextIs)
                            for (int i = 0; i < num; i++)
                        {     
                                borderOptions.CopyBorderStyle(sheet.Cells[cell.RowIndex + opt.ObRowIndex, cell.ColumnIndex+i]);
                                AlignmentOptions.CopyAlignmentStyle(sheet.Cells[cell.RowIndex + opt.ObRowIndex, cell.ColumnIndex+i].Alignment);
                           
                        }
                    }                 
                    //Inserted = true;
                }
                for (int k = cell.ColumnIndex, currArr = opt.values[0], interval = 0; interval < num; k++, currArr++, interval++)
                {
                    //первая текущая ячейка уже есть, на все последующих мы добавляем столбец и вставляем текст макроса                   
                    if (interval != 0)
                    {      
                        sheet.Cells[cell.RowIndex, k].Value = val;
                    }
                    if (!opt.OneRow)
                    {
                        for (int c = cell.RowIndex, p = 0; c < arr.GetLength(0) + cell.RowIndex; c++, p++)
                        {
                            if (p != 0)
                            {
                                if (sheet.Cells[c, k].HasFormula)
                                {
                                    if (!sheet.Cells[c, k].DisplayText.Contains(opt.textOriginnal))
                                    {
                                        sheet.Cells[c, k].Formula = cell.Formula;
                                    }
                                }
                                else
                                if (!sheet.Cells[c, k].DisplayText.Contains(opt.textOriginnal))
                                {
                                    sheet.Cells[c, k].Value = val;                                 
                                }
                            }
                            if (sheet.Cells[c,k].HasFormula)
                            sheet.Cells[c, k].Formula = sheet.Cells[c, k].Formula.Replace(opt.textOriginnal, arr[p, currArr]);
                            else
                                sheet.Cells[c, k].Value = sheet.Cells[c, k].DisplayText.Replace(opt.textOriginnal, arr[p, currArr]);
                            if (c == arr.GetLength(0) - 1)
                            {
                                if (sheet.Cells[c, k].HasFormula)
                                {
                                    sheet.Cells[c, k].Formula = sheet.Cells[c, k].Formula.Replace(opt.textOriginnal, arr[p, currArr]);
                                }
                                else
                                    sheet.Cells[c, k].Value = sheet.Cells[c, k].DisplayText.Replace(opt.textOriginnal, arr[p, currArr]);
                            }
                            borderOptions.CopyBorderStyle(sheet.Cells[c, k]);
                            AlignmentOptions.CopyAlignmentStyle(sheet.Cells[c, k].Alignment);
                        }
                    }
                    else
                    {         
                        if (sheet.Cells[cell.RowIndex, k].HasFormula)              
                        sheet.Cells[cell.RowIndex, k].Formula = sheet.Cells[cell.RowIndex, k].Formula.Replace(opt.textOriginnal, arr[opt.values[2], currArr]);
                        else
                            sheet.Cells[cell.RowIndex, k].Value = sheet.Cells[cell.RowIndex, k].DisplayText.Replace(opt.textOriginnal, arr[opt.values[2], currArr]);
                        if (opt.Merge)
                        {
                            if (interval != 0)
                            {
                                if (prev == arr[opt.values[2], currArr])
                                {
                                    if ((sheet.Cells[cell.RowIndex, k].DisplayText!=string.Empty||!sheet.Cells[cell.RowIndex, k].HasFormula) && arr[opt.values[2], currArr]!=string.Empty)
                                    sheet.MergeCells(sheet.Range.FromLTRB(k-1,cell.RowIndex,k, cell.RowIndex), MergeCellsMode.Default);
                                   // k--;
                                }
                            }
                        }
                        borderOptions.CopyBorderStyle(sheet.Cells[cell.RowIndex, k]);
                        AlignmentOptions.CopyAlignmentStyle(sheet.Cells[cell.RowIndex, k].Alignment);
                        prev = arr[opt.values[2], currArr];
                    }                    
                }

            }
            sheet.Cells.EndUpdate();
            return true;
        }
        
        public static void TableGen(Worksheet sheet, int j, int i, string[,] arr, Regex TableGenerator)
        {
            List<DevExpress.Spreadsheet.Cell> cells = sheet.Search(Option.macs[j][i].macro, options).ToList();
            bool Yet = true;
            while (cells.Count > 0 && Yet)
            {
                if (cells[0].HasFormula)
             YetGenerator(ref Yet, WorkTableGeneration(sheet, arr, TableGenerator.Matches(cells[0].Formula), cells[0]));
                else
                    YetGenerator(ref Yet, WorkTableGeneration(sheet, arr, TableGenerator.Matches(cells[0].DisplayText), cells[0]));
                cells = sheet.Search(Option.macs[j][i].macro, options).ToList();
            }
        }
        public static void OneOfArray(string[,] arr, Regex OneItemArray, DevExpress.Spreadsheet.Cell cell)
        {
            int a1, a2;
            if (cell.HasFormula)
            foreach (System.Text.RegularExpressions.Match item in OneItemArray.Matches(cell.Formula))
            {
                string[] indexes = item.Value.Split(new char[] { '[', ']', ',' }, StringSplitOptions.RemoveEmptyEntries);
                if (!int.TryParse(indexes[1], out a1) || !int.TryParse(indexes[2], out a2))
                    continue;
                cell.Formula = cell.Formula.Replace(item.Value, arr[a2, a1]);
                cell.Font.Color = Color.Black;
            }
            else
                foreach (System.Text.RegularExpressions.Match item in OneItemArray.Matches(cell.DisplayText))
                {
                    string[] indexes = item.Value.Split(new char[] { '[', ']', ',' }, StringSplitOptions.RemoveEmptyEntries);
                    if (!int.TryParse(indexes[1], out a1) || !int.TryParse(indexes[2], out a2))
                        continue;
                    cell.Value = cell.DisplayText.Replace(item.Value, arr[a2, a1]);
                    cell.Font.Color = Color.Black;
                }

        }
        public static void ColumnsGen(Worksheet sheet, string[,] arr, Regex ColumnGenerator, DevExpress.Spreadsheet.Cell cell)
        {
            bool inserted = false;
            string find = cell.HasFormula ? cell.Formula : cell.DisplayText;
            foreach (System.Text.RegularExpressions.Match item in ColumnGenerator.Matches(find))
            {
                int sk = arr.GetLength(0) - 1;
                Generate.TextOption opt = new Generate.TextOption(item.Value, true);
                SpreadCell.MergedCellData MergedOptions = new SpreadCell.MergedCellData(cell.GetMergedRanges());
                SpreadCell.BorderCellData borderOptions = new SpreadCell.BorderCellData(cell.Borders);
                for (int k = cell.RowIndex; sk > 0; k++, sk--)
                {
                    if (!CollectionInfo.RowInserted.Contains(k + 1))
                    {
                        sheet.Rows.Insert(k + 1);
                        CollectionInfo.RowInserted.Add(k + 1);
                        sheet.Cells[k + 1, cell.ColumnIndex].CopyFrom(cell);
                        MergedOptions.CopyMergedData(sheet.Cells[k + 1, cell.ColumnIndex]);
                        borderOptions.CopyBorderStyle(sheet.Cells[k + 1, cell.ColumnIndex]);
                    }
                    else if (!inserted)
                    {
                        sheet.Cells[k + 1, cell.ColumnIndex].CopyFrom(cell);
                        MergedOptions.CopyMergedData(sheet.Cells[k + 1, cell.ColumnIndex]);
                        borderOptions.CopyBorderStyle(sheet.Cells[k + 1, cell.ColumnIndex]);
                    }
                }
                inserted = true;          
                int arrIndex = opt.values[0];
                if (arr.GetLength(0) == 0)
                {
                    cell.Value = cell.Formula.Replace(opt.textOriginnal, string.Empty);
                    continue;
                }
                if (opt.Minus)
                    arrIndex = arr.GetLength(1) - 1 - arrIndex;
                if (!opt.Merge)
                    for (int k = cell.RowIndex, l = 0; k < arr.GetLength(0) + cell.RowIndex; k++, l++)
                    {
                       // if (!sheet.Cells[k, cell.ColumnIndex].Formula.Contains())
                       if (cell.HasFormula)
                        sheet.Cells[k, cell.ColumnIndex].Formula = sheet.Cells[k, cell.ColumnIndex].Formula.Replace(opt.textOriginnal, arr[l, arrIndex]);                        
                        else
                            sheet.Cells[k, cell.ColumnIndex].Value = sheet.Cells[k, cell.ColumnIndex].DisplayText.Replace(opt.textOriginnal, arr[l, arrIndex]);
                        sheet.Cells[k, cell.ColumnIndex].Font.Color = Color.Black;

                    }
                else
                {
                    string prev = string.Empty;
                    for (int k = cell.RowIndex, l = 0; k < arr.GetLength(0) + cell.RowIndex; k++, l++)
                    {
                        if (l != 0 && prev == arr[l, arrIndex])
                        {
                            //  sheet.Cells[k, cell.ColumnIndex].Value = string.Empty;
                            var range = sheet.Range.FromLTRB(cell.ColumnIndex, k - 1, cell.ColumnIndex, k);
                            if (cell.ColumnIndex != 0)
                                sheet.MergeCells(range, MergeCellsMode.IgnoreIntersections);
                            else
                                sheet.MergeCells(range);

                        }
                        else
                        {
                            if (cell.HasFormula)
                            sheet.Cells[k, cell.ColumnIndex].Formula = sheet.Cells[k, cell.ColumnIndex].Formula.Replace(opt.textOriginnal, arr[l, arrIndex]);
                            else
                                sheet.Cells[k, cell.ColumnIndex].Value = sheet.Cells[k, cell.ColumnIndex].DisplayText.Replace(opt.textOriginnal, arr[l, arrIndex]);
                        }
                        sheet.Cells[k, cell.ColumnIndex].Font.Color = Color.Black;
                        prev = arr[l, arrIndex];
                    }
                }

            }
        }
    }
}
