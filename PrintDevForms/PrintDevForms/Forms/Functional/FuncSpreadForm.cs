﻿using System.Windows.Forms;

namespace MKP.PrintDevForms
{
    public partial class FuncSpreadForm : Form
    {
        public FuncSpreadForm()
        {
            InitializeComponent();
        }

        private void FuncSpreadForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Visible = false;
            e.Cancel = true;
        }
    }
}
