﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MKP.PrintDevForms.Options;
using DevExpress.XtraRichEdit;
using DevExpress.XtraRichEdit.API.Native;
using FirebirdSql.Data.FirebirdClient;
using DevExpress.XtraRichEdit.Commands;
using System.IO;
using System.Text.RegularExpressions;
using DevExpress.XtraRichEdit.API.Layout;

namespace MKP.PrintDevForms
{
    public partial class WordForm : UserControl
    {
        public string FileName = string.Empty;
        public Color? MainBaseColor = Color.Black;
        public Color? ResultBaseColor = Color.Black;
        public Color ResultMacroColor = Color.FromArgb(204, 51, 0);
        EditorControl Temp = new EditorControl();
        RichEditControl rtf = new RichEditControl();
        public bool isPrintable;
        public WordForm()
        {
            InitializeComponent();            
            Macroses[] test = new Macroses[3];
            string[,] tabl = new string[10, 8];
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    if (i == 0 && j < 3)
                        tabl[i, j] = "Общая строка";
                    else if ((i == 1 && j == 0) || (i == 2 && j == 0))
                        tabl[i, j] = "Общая колонка";
                    else
                        tabl[i, j] = "Значениеi,j => i=" + i + ", j=" + j;
                }
            }
            test[0] = new Macroses() { macro = "ololo1", result = "test1" };
            test[1] = new Macroses() { macro = "ololo2", result = "test2" };
            test[2] = new Macroses(MacroType.ArrayString) { macro = "табличко", result = tabl, rowStart = 1 };
            //==================
            /*Macroses[] test2 = new Macroses[3];
            string[,] tabl2 = new string[10, 8];
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    tabl2[i, j] = "sfksdf";
                }
            }
            test2[0] = new Macroses() { macro = "ololo1", result = "test1" };
            test2[1] = new Macroses() { macro = "ololo2", result = "test2" };
            test2[2] = new Macroses(MacroType.ArrayString) { macro = "табличко", result = tabl2,rowStart=1 };*/

            List<Macroses[]> macro = new List<Macroses[]>();
            macro.Add(test);
            //macro.Add(test2);
            // macro.Add(test2);
            // macro.Add(test3);
            Option.macs = Dev.macroInit(macro);
            isPrintable = true;
            Option.IdScript = "-1"; Option.macs = macro;
            List<string> mas = new List<string>();
            _StaticFuntion.InitStaticFunction();
            for (int i = 0; i < _StaticFuntion.lst.Count; i++)
                dgPForms.Rows.Add(_StaticFuntion.lst[i], _StaticFuntion.lst[i].macro, _StaticFuntion.lst[i].result);
            for (int i = 0; i < macro[0].Length; i++)
            {
                dgPForms.Rows.Add((int)macro[0][i].type, macro[0][i].macro, macro[0][i].result);
                mas.Add(macro[0][i].macro);
            }
            editorControl1.SetMacroses(mas, _StaticFuntion.lst.Select(i => i.macro).ToList());
            editorControl1.richEditControl1.Document.ContentChanged += new EventHandler(change);
            editorControl1.ribbonControl1.Minimized = false;
        }
        public WordForm(object macro, string idScript, string ConBD, string ConBDD, ModeStart mode, int UserID, string fastSaveFullPath = "")
        {
            InitializeComponent();
            Option.IdScript = idScript;
            Option.ConBD = ConBD;
            Option.ConBDD = ConBDD;
            Option.SavePath = fastSaveFullPath;
            Option.UserId = UserID;
            Option.macs = Dev.macroInit(macro);
            _StaticFuntion.InitStaticFunction();
            for (int i = 0; i < _StaticFuntion.lst.Count; i++)
                dgPForms.Rows.Add(_StaticFuntion.lst[i], _StaticFuntion.lst[i].macro, _StaticFuntion.lst[i].result);
            List<string> mas = new List<string>();
            if (Option.macs.Count > 0)
                for (int i = 0; i < Option.macs[0].Length; i++)
                {
                    dgPForms.Rows.Add((int)Option.macs[0][i].type, Option.macs[0][i].macro, Option.macs[0][i].result);
                    mas.Add(Option.macs[0][i].macro);
                }
            editorControl1.SetMacroses(mas, _StaticFuntion.lst.Select(i => i.macro).ToList());
            using (FbConnection fbCon = new FbConnection(Option.ConBDD))
            {
                fbCon.Open();
                using (FbCommand fbCom = new FbCommand("SELECT formi7,formi8 FROM formi WHERE formi1 = @p1", fbCon))
                {
                    fbCom.Parameters.Add("@p1", Option.IdScript);
                    using (FbDataReader fbDr = fbCom.ExecuteReader())
                    {
                        if (fbDr.Read())
                        {
                            if (fbDr["formi8"] != DBNull.Value)
                                editorControl1.richEditControl1.Document.RtfText = System.Text.Encoding.Default.GetString((byte[])fbDr["formi8"]);
                            Option.memory = editorControl1.richEditControl1.Document.RtfText;
                            button1.BackColor = Color.FromArgb(81, 206, 109);
                        }
                    }
                }
            }
            editorControl1.richEditControl1.Document.ContentChanged += new EventHandler(change);
            if (mode != ModeStart.Стандартный_режим)
                DocPrint((int)mode);
            else
                editorControl1.ribbonControl1.Minimized = false;
        }
        private void change(object sender, EventArgs e)
        {
            PACKPACKA.Stop();
            timer1.Stop();
            timer1.Start();
            PACKPACKA.Start();
        }
        private void Packa(ref EditorControl ec, Color MacroColor, Color Base)
        {
            CharacterProperties cp = null;
            for (int i = 0; i < ec.richEditControl1.Document.Length; i++)
            {
                cp = ec.richEditControl1.Document.BeginUpdateCharacters(ec.richEditControl1.Document.CreateRange(i, 1));
                if (cp.ForeColor == MacroColor)
                    if (Base != MacroColor)
                        cp.ForeColor = Base;
                ec.richEditControl1.Document.EndUpdateCharacters(cp);
            }
        }
        private void timer1_Tick(object sender, EventArgs e)
        {

        }
        public void button1_Click(object sender, EventArgs e)
        {
            Option.memory = editorControl1.richEditControl1.Document.RtfText;
            byte[] array = Encoding.Default.GetBytes(Option.memory);
            button1.BackColor = Color.FromArgb(81, 206, 109);
            using (FbConnection fConnection = new FbConnection(Option.ConBDD))
            {
                fConnection.Open();
                using (FbTransaction fTrans = fConnection.BeginTransaction())
                {
                    using (FbCommand fCommand = new FbCommand(@"UPDATE formi 
                        SET formi8 = @p2, formi9=1 WHERE formi1 = @p1", fConnection, fTrans))
                    {
                        fCommand.Parameters.Add("@p2", array);
                        fCommand.Parameters.Add("@p1", Option.IdScript);
                        fCommand.ExecuteNonQuery();
                        fTrans.Commit();
                    }
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="type">0-сразу на печать, 1 - диалог печати, 2 - предпросмотр</param>
        public void DocPrint(int type)
        {
            GC.Collect(); Collection.cells = new List<TableCell>();
            CollectionInfo.RowInserted.Clear();
            Result result = new Result();
            Temp.richEditControl1.Document.Delete(Temp.richEditControl1.Document.Range);
            Temp.richEditControl1.Document.RtfText = editorControl1.richEditControl1.Document.RtfText;
            // Temp.SetBlackColor();
            result.editorControl1.richEditControl1.RtfText = Temp.richEditControl1.Document.RtfText;
            Packa(ref result.editorControl1, ResultMacroColor, ResultBaseColor.Value);
            RichEditControl addDocument = new RichEditControl();
            result.editorControl1.richEditControl1.BeginUpdate();
            if (Option.macs != null)
            {
                if (Option.macs.Count > 0)
                    addDocument.Document.RtfText = result.editorControl1.richEditControl1.Document.RtfText;
                InsertPageBreakCommand command = new InsertPageBreakCommand(result.editorControl1.richEditControl1);
                for (int j = 0; j < Option.macs.Count; j++)
                {
                    if (j != 0)
                    {
                        command.Execute();
                        result.editorControl1.richEditControl1.Document.InsertRtfText(result.editorControl1.richEditControl1.Document.CaretPosition, addDocument.Document.RtfText);
                        CollectionInfo.RowInserted.Clear();
                    }
                    for (int i = 0; i < Option.macs[j].Length; i++)
                    {
                        if (Option.macs[j][i].result == null)
                            continue;
                        switch (Option.macs[j][i].type)
                        {
                            case MacroType.String: result.editorControl1.richEditControl1.Document.ReplaceAll(Option.macs[j][i].macro, Option.macs[j][i].result.ToString(), SearchOptions.CaseSensitive); break;
                            case MacroType.Rtf:
                                MacroRTF(result, j, i);
                                break;
                            case MacroType.ArrayString:
                                if (!(Option.macs[j][i].result is string[,]))
                                    continue;
                                MacroArrString(result, j, i);
                                break;
                        }
                        result.editorControl1.richEditControl1.Document.CaretPosition = result.editorControl1.richEditControl1.Document.Range.End;
                    }
                    // result.editorControl1.richEditControl1.EndUpdate();
                    SpecialMacro(result);
                }
                if (type == 2)
                {
                    result.editorControl1.richEditControl1.EndUpdate();
                    result.ShowDialog();
                    // if (!isPrintable)
                    //   this.Close();
                }
                else
                    if (type == 1)
                {
                    result.editorControl1.richEditControl1.EndUpdate();
                    result.editorControl1.richEditControl1.ShowPrintDialog();
                }
                else if (type == 0)
                {
                    result.editorControl1.richEditControl1.EndUpdate();
                    result.editorControl1.richEditControl1.Print();
                }
                else if (type == 3)
                {
                    result.editorControl1.richEditControl1.EndUpdate();
                    result.editorControl1.richEditControl1.SaveDocument(FileName, DocumentFormat.Doc);
                }
                else
                    if (type == 4)
                {
                    result.editorControl1.richEditControl1.EndUpdate();
                    result.editorControl1.richEditControl1.SaveDocumentAs();
                }
                else
                    result.editorControl1.richEditControl1.EndUpdate();
            }
            else
                result.editorControl1.richEditControl1.EndUpdate();
        }
        private void SpecialMacro(Result result)
        {
            for (int i = 0; i < _StaticFuntion.lst.Count; i++)
            {
                switch (_StaticFuntion.lst[i].type)
                {
                    case MacroType.TableHeaderDub:
                        if (!result.editorControl1.richEditControl1.Document.Text.Contains(_StaticFuntion.lst[i].macro))
                            continue;
                        result.UI(null, null);
                        int pagecount = 0;
                        pagecount = result.PageCount;
                        if (pagecount <= 1)
                        {
                            result.editorControl1.richEditControl1.Document.ReplaceAll(_StaticFuntion.lst[i].macro, string.Empty, SearchOptions.CaseSensitive);
                            break;
                        }
                        //предыдущая таблица
                        int prevTableindex = -1;
                        int pageindex = -1;
                        // restart:
                        var headers = result.editorControl1.richEditControl1.Document.FindAll(_StaticFuntion.lst[i].macro, SearchOptions.CaseSensitive);
                        if (headers.Length == 0)
                            break;
                        foreach (DocumentRange range in headers)
                        {
                            TableCell cell = result.editorControl1.richEditControl1.Document.GetTableCell(range.Start);
                            if (cell == null)
                                continue;
                            //получить текущую страницу данной ячейки, получить все "буфферные" строки до этой cтроки включая её. Номер строки?
                            int currTableindex = Dev.GetTableIndex(result.editorControl1.richEditControl1.Document.Tables, cell.Table);
                            if (prevTableindex == -1 || prevTableindex != currTableindex)
                            {
                                prevTableindex = currTableindex;
                                Collection.rowCollection = new RowCollection(result.editorControl1.richEditControl1.Document, cell.Table);
                                for (int e = 0; e < cell.Row.Index + 1; e++)
                                {
                                    result.editorControl1.richEditControl1.Document.ReplaceAll(_StaticFuntion.lst[i].macro, string.Empty, SearchOptions.CaseSensitive, cell.Table.Rows[e].Range);
                                    Collection.rowCollection.Add(cell.Table.Rows[e]);
                                    if (e == cell.Row.Index)
                                    {
                                        var elem = result.editorControl1.richEditControl1.DocumentLayout.GetElement(cell.Table.Rows[i].Cells[cell.Index].Range.Start, LayoutType.TableCell);
                                        if (elem == null)
                                            elem = result.editorControl1.richEditControl1.DocumentLayout.GetElement(cell.Table.Rows[i].Cells[cell.Index].Range.End, LayoutType.TableCell);
                                        pageindex = result.editorControl1.richEditControl1.DocumentLayout.GetPageIndex(elem);
                                    }
                                }
                            }
                            //Теперь у нас есть в буфере нужные строки
                            for (int m = 0; m < cell.Table.Rows.Count; m++)
                            {
                                var elem = result.editorControl1.richEditControl1.DocumentLayout.GetElement(cell.Table.Rows[m].Cells[cell.Index].Range.Start, LayoutType.TableCell);
                                if (elem == null)
                                    elem = result.editorControl1.richEditControl1.DocumentLayout.GetElement(cell.Table.Rows[m].Cells[cell.Index].Range.End, LayoutType.TableCell);
                                int newPageindex = result.editorControl1.richEditControl1.DocumentLayout.GetPageIndex(elem);
                                if (newPageindex > pageindex)
                                {
                                    pageindex = newPageindex;
                                    Collection.rowCollection.InsertRowCollection(cell.Table.Rows[m].Index, RowCollection.InsertedOption.Before);
                                }
                            }
                            //мы должны вернуться на предыдущую обработку ренжей т.к. данные уже сдвинулись
                            //goto restart;
                        }
                        break;
                    case MacroType.TableMergeCellFuncs:
                        var merges = result.editorControl1.richEditControl1.Document.FindAll(new Regex(_StaticFuntion.lst[i].macro + @"\[\d*\]", RegexOptions.CultureInvariant));
                        foreach (DocumentRange range in merges)
                        {
                            TableCell cell = result.editorControl1.richEditControl1.Document.GetTableCell(range.Start);
                            if (cell == null)
                                continue;
                            string text = result.editorControl1.richEditControl1.Document.GetText(range);
                            text = text.Remove(0, text.IndexOf("[") + 1);
                            text = text.Remove(text.IndexOf("]"));
                            int indexOf = -1;
                            if (!int.TryParse(text, out indexOf))
                                continue;
                            result.editorControl1.richEditControl1.Document.ReplaceAll(result.editorControl1.richEditControl1.Document.GetText(range), string.Empty, SearchOptions.CaseSensitive, range);
                            //сколько?
                            cell.Table.MergeCells(cell, cell.Row[cell.Index + indexOf - 1]);
                        }

                        merges = result.editorControl1.richEditControl1.Document.FindAll(_StaticFuntion.lst[i].macro, SearchOptions.CaseSensitive);
                        foreach (DocumentRange range in merges)
                        {
                            TableCell cell = result.editorControl1.richEditControl1.Document.GetTableCell(range.Start);
                            if (cell == null)
                                continue;
                            result.editorControl1.richEditControl1.Document.ReplaceAll(result.editorControl1.richEditControl1.Document.GetText(range), string.Empty, SearchOptions.CaseSensitive, range);
                            cell.Table.MergeCells(cell, cell.Row.LastCell);
                        }
                        break;
                    case MacroType.ArrContinue:
                        if (!result.editorControl1.richEditControl1.Document.Text.Contains(_StaticFuntion.lst[i].macro))
                            continue;
                        result.editorControl1.richEditControl1.Document.ReplaceAll(_StaticFuntion.lst[i].macro, string.Empty, SearchOptions.CaseSensitive);
                        break;
                    case MacroType.LowerFirsteCharacterString:
                        var lowers = result.editorControl1.richEditControl1.Document.FindAll(new Regex(@"{строка\(.+\)}", RegexOptions.CultureInvariant));
                        if (lowers.Length == 0)
                            continue;
                        StringBuilder Lower;
                        foreach (DocumentRange range in lowers)
                        {
                            string str = result.editorControl1.richEditControl1.Document.GetText(range);
                            Lower = new StringBuilder(str);
                            Lower[0] = char.ToLower(Lower[0]);
                            result.editorControl1.richEditControl1.Document.ReplaceAll(str, Lower.ToString(), SearchOptions.CaseSensitive, range);
                        }
                        break;
                    case MacroType.UpperFirsteCharacterString:
                        var uppers = result.editorControl1.richEditControl1.Document.FindAll(new Regex(@"{Строка\(.+\)}", RegexOptions.CultureInvariant));
                        if (uppers.Length == 0)
                            continue;
                        StringBuilder Upper;
                        foreach (DocumentRange range in uppers)
                        {
                            string str = result.editorControl1.richEditControl1.Document.GetText(range);
                            Upper = new StringBuilder(str);
                            Upper[0] = char.ToUpper(Upper[0]);
                            result.editorControl1.richEditControl1.Document.ReplaceAll(str, Upper.ToString(), SearchOptions.CaseSensitive, range);
                        }
                        break;
                    case MacroType.SaveDocument:
                        if (string.IsNullOrEmpty(Option.FileName))
                        {
                            var Saves = result.editorControl1.richEditControl1.Document.FindAll(new Regex(@"{Сохранить как\(.+\,.+\)}", RegexOptions.CultureInvariant));
                            // MessageBox.Show("Не определён путь для сохранения файла, сохранение не будет произведено");
                            if (Saves.Length == 0)
                                continue;
                            using (FbConnection fConnection = new FbConnection(Option.ConBD))
                            {
                                fConnection.Open();
                                using (FbCommand fCommand = new FbCommand(@"select i5 from i where i1=" + Option.UserId, fConnection))
                                {
                                    using (FbDataReader fReader = fCommand.ExecuteReader())
                                    {
                                        if (fReader.Read())
                                        {
                                            Option.FileName = fReader[0]?.ToString();
                                            if (string.IsNullOrEmpty(Option.FileName))
                                                continue;
                                        }
                                        else
                                            continue;
                                    }
                                }
                            }
                            foreach (DocumentRange range in Saves)
                            {
                                string text = result.editorControl1.richEditControl1.Document.GetText(range);
                                string[] Save = text.Split(new char[] { '(', ',', ')' }, StringSplitOptions.RemoveEmptyEntries);
                                if (Save.Length < 4)
                                    continue;
                                result.editorControl1.richEditControl1.Document.ReplaceAll(text, string.Empty, SearchOptions.CaseSensitive, range);
                                string path = (Option.FileName + @"\" + Save[1]).Replace("\"", string.Empty);
                                if (!Directory.Exists(path))
                                    Directory.CreateDirectory(path);
                                result.editorControl1.richEditControl1.SaveDocument((Option.FileName + @"\" + Save[1] + @"\" + Save[2] + ".doc").Replace("\"", string.Empty), DocumentFormat.Doc);
                                Option.FileName = string.Empty;
                            }
                        }
                        else
                        {
                            if (!Directory.Exists(Option.FileName))
                                Directory.CreateDirectory(Option.FileName);
                            result.editorControl1.richEditControl1.SaveDocument(Option.FileName, DocumentFormat.Doc);
                            Option.FileName = string.Empty;
                        }
                        break;
                    //Запрос
                    case MacroType.Request:

                        break;
                }
            }
            if (result.editorControl1.richEditControl1.Document.Tables.Count > 0)
                result.editorControl1.barButtonItem1_ItemClick(null, null);
        }
        private static void MacroArrString(Result result, int j, int i)
        {
            string[,] arr = null;
            bool full = false;
            CollectionInfo.currTable = null;
            var parameters = result.editorControl1.richEditControl1.Document.FindAll(new Regex(Option.macs[j][i].macro + Generate.pattern1, RegexOptions.CultureInvariant));
            if (parameters.Length != 0 && !full)
            {
                arr = Option.macs[j][i].result as string[,];
                if (Option.macs[j][i].rowStart != 0)
                {
                    string[,] t2 = new string[arr.GetLength(0) - Option.macs[j][i].rowStart, arr.GetLength(1)];
                    Array.Copy(arr, Option.macs[j][i].rowStart * t2.GetLength(1), t2, 0, t2.Length);
                    arr = t2;
                }
                full = true;
            }
            int a1, a2, a3 = -1, a4, a5;
            foreach (DocumentRange range in parameters)
            {
                string text = result.editorControl1.richEditControl1.Document.GetText(range);
                string[] indexes = text.Split(new char[] { '[', ']', ',' }, StringSplitOptions.RemoveEmptyEntries);
                if (indexes.Length < 3)
                    continue;
                if (!int.TryParse(indexes[1], out a1) || !int.TryParse(indexes[2], out a2))
                    continue;
                result.editorControl1.richEditControl1.Document.ReplaceAll(text, arr[a2, a1], SearchOptions.CaseSensitive, range);
            }
            var TableContinue = result.editorControl1.richEditControl1.Document.FindAll(_StaticFuntion.lst[_StaticFuntion.ArrContinueIndex].macro, SearchOptions.CaseSensitive);
            var columns = result.editorControl1.richEditControl1.Document.FindAll(new Regex(Option.macs[j][i].macro + Generate.pattern2, RegexOptions.CultureInvariant));
            if (columns.Length != 0 && !full)
            {
                arr = Option.macs[j][i].result as string[,];
                if (Option.macs[j][i].rowStart != 0)
                {
                    string[,] t2 = new string[arr.GetLength(0) - Option.macs[j][i].rowStart, arr.GetLength(1)];
                    Array.Copy(arr, Option.macs[j][i].rowStart * t2.GetLength(1), t2, 0, t2.Length);
                    arr = t2;
                }
                full = true;
            }
            foreach (DocumentRange range in columns)
            {
                TableCell cell = result.editorControl1.richEditControl1.Document.GetTableCell(range.Start);
                if (cell == null)
                    continue;
                int sk = arr.GetLength(0) - 1;
                //если длина одного измерения больше 0
                if (sk > 0)
                {
                    if (TableContinue.Length == 0)
                    {
                        Generate.TextOption opt = new Generate.TextOption(result.editorControl1.richEditControl1.Document.GetText(range), true);
                        for (int k = cell.Row.Index; sk > 0; k++, sk--)
                        {
                            if (!CollectionInfo.RowInserted.Contains(k + 1))
                            {
                                if (CollectionInfo.currTable == null || !CollectionInfo.currTable.Equals(cell.Table))
                                {
                                    CollectionInfo.currTable = cell.Table;
                                    CollectionInfo.Cells = new string[cell.Row.Cells.Count];
                                    for (int n = 0; n < CollectionInfo.Cells.Length; n++)
                                    {
                                        CollectionInfo.Cells[n] = result.editorControl1.richEditControl1.Document.GetRtfText(cell.Row.Cells[n].ContentRange);
                                    }
                                }
                                cell.Table.Rows.InsertAfter(k);
                                CollectionInfo.RowInserted.Add(k + 1);
                                for (int n = 0; n < cell.Row.Cells.Count; n++)
                                {
                                    result.editorControl1.richEditControl1.Document.InsertRtfText(cell.Table[k + 1, n].Range.Start, CollectionInfo.Cells[n]);
                                }
                            }
                        }                        
                        int arrIndex = opt.values[0];
                        if (arr.GetLength(0) == 0)
                        {
                            result.editorControl1.richEditControl1.Document.ReplaceAll(opt.textOriginnal, string.Empty, SearchOptions.CaseSensitive, cell.Table.Range);
                            continue;
                        }
                        if (opt.Minus)
                            arrIndex = arr.GetLength(1) - 1 - arrIndex;
                        //медленное место...
                        SetResultReplaceArr(result, arr, cell, opt, arrIndex);

                    }
                    else//Если мы имеем продолжение - никаких строк не добавляем, только в последнюю таблицу
                    {
                        //string currMacro = result.editorControl1.richEditControl1.Document.GetText(range);
                        Generate.TextOption opt = new Generate.TextOption(result.editorControl1.richEditControl1.Document.GetText(range), true);
                        bool start = true;
                        string rtfText = result.editorControl1.richEditControl1.Document.GetRtfText(range);
                        //  int ix = currMacro.IndexOf("]");
                        //if (ix < currMacro.Length - 1)
                        //     currMacro = currMacro.Remove(ix + 1);
                        // string text = currMacro;
                        // text = text.Remove(0, text.IndexOf("[") + 1);
                        // text = text.Remove(text.IndexOf("]"));
                        int arrIndex = opt.values[0];
                        // int arrIndex = -1;
                        // if (!int.TryParse(text, out arrIndex))
                        //     continue;
                        for (int k = cell.Row.Index, cellIndex = 0, rowIndex = 0; sk > 0; k++, sk--, rowIndex++)
                        {
                            if (k > cell.Table.Rows.Count - 1)
                            {
                                cell = result.editorControl1.richEditControl1.Document.GetTableCell(TableContinue[cellIndex].Start).Row.Cells[cell.Index];
                                k = cell.Row.Index;
                                cellIndex++;
                            }
                            if (!start)
                            {
                                result.editorControl1.richEditControl1.Document.InsertRtfText(cell.Table.Rows[k].Cells[cell.Index].ContentRange.Start, rtfText);
                            }
                            start = false;
                            result.editorControl1.richEditControl1.Document.ReplaceAll(opt.textOriginnal, arr[rowIndex, arrIndex], SearchOptions.CaseSensitive, cell.Table.Rows[k].Cells[cell.Index].ContentRange);
                        }
                    }
                }
                else
                {
                    if (sk == 0)
                    {
                        Generate.TextOption opt = new Generate.TextOption(result.editorControl1.richEditControl1.Document.GetText(range), true);
                        // string currMacro = result.editorControl1.richEditControl1.Document.GetText(range);
                        //  int ix = currMacro.IndexOf("]");
                        // if (ix < currMacro.Length - 1)
                        //    currMacro = currMacro.Remove(ix + 1);
                        // string text = currMacro; bool minus = false;
                        // text = text.Remove(0, text.IndexOf("[") + 1);
                        //  text = text.Remove(text.IndexOf("]"));
                        // int arrIndex = -1;
                        int arrIndex = opt.values[0];
                        //  if (text.Contains("-"))
                        // {
                        //      minus = true; text = text.Replace("-", string.Empty);
                        //  }
                        //if (!int.TryParse(text, out arrIndex))
                        //     continue;
                        if (opt.Minus)
                            arrIndex = arr.GetLength(1) - arrIndex - 1;
                        SetResultReplaceArr(result, arr, cell, opt, arrIndex);
                    }
                }
            }
            {
                //макрос[c какого столбца-отминусовать с конца]
                var cNew = result.editorControl1.richEditControl1.Document.FindAll(new Regex(Option.macs[j][i].macro + Generate.pattern3, RegexOptions.CultureInvariant));
                //((\[\d*\-\d*\,\d*\])|(\[\d*\-\d*\])|(\[\*\d*\-\d*\,\d*\])|(\[\*\d*\-\d*\]))
                //((\[([^,(\W\s)]*)\-([^,(\W\s)]*)\,([^,(\W\s)]*)\])|(\[([^,(\W\s)]*)\-([^,(\W\s)]*)\]))
                if (cNew.Length != 0 && !full)
                {
                    arr = Option.macs[j][i].result as string[,];
                    if (Option.macs[j][i].rowStart != 0)
                    {
                        string[,] t2 = new string[arr.GetLength(0) - Option.macs[j][i].rowStart, arr.GetLength(1)];
                        Array.Copy(arr, Option.macs[j][i].rowStart * t2.GetLength(1), t2, 0, t2.Length);
                        arr = t2;
                    }
                    full = true;
                }
                int PrevIndex = -1; int prevRowIndex = -1;
                bool columnInsert = true;
                int width = 0;
                //----------------------[какой столбец массива-сколько отминусовать столбцов с конца]
                // for (int h = 0; h < cNew.Length; h++)
                while (cNew.Length > 0)
                {
                    DocumentRange range = cNew[0];
                    // }
                    // foreach (DocumentRange range in cNew)
                    //  {
                    TableCell cell = result.editorControl1.richEditControl1.Document.GetTableCell(range.Start);
                    if (cell == null)
                        continue;
                    if (PrevIndex == cell.Index && prevRowIndex == cell.Row.Index)
                        continue;
                    PrevIndex = cell.Index;
                    prevRowIndex = cell.Row.Index;
                    int sk = arr.GetLength(0) - 1;
                    //если длина одного измерения больше 0
                    if (sk >= 0)
                    {
                        Generate.TextOption opt = new Generate.TextOption(result.editorControl1.richEditControl1.Document.GetText(range));
                        //string text = result.editorControl1.richEditControl1.Document.GetText(range);
                        // if (text.Contains("\r\n\r\n\r\n"))
                        // {
                        //     text = text.Replace("\r\n", string.Empty);
                        // }
                        // string[] indexes = text.Split(new char[] { '[', ']', '-',',' }, StringSplitOptions.RemoveEmptyEntries);
                        if (opt.values.Length < 2)
                            break;
                        //  continue;
                        // if (indexes.Length < 3)
                        //    continue;
                        // if (indexes[1].Contains("*"))
                        //  {
                        //    columnInsert = false;
                        //     indexes[1] = indexes[1].Replace("*",string.Empty);
                        // }
                        //a1 - это колонка с которой заполняем
                        //a2 - это 
                        //   if (!int.TryParse(indexes[1], out a1) || !int.TryParse(indexes[2], out a2))
                        //  continue;
                        // if (indexes.Length == 4)
                        //{                           
                        //       if (!int.TryParse(indexes[3], out a3))
                        //          continue;
                        //   }
                        string CellText = result.editorControl1.richEditControl1.Document.GetRtfText(cell.ContentRange);
                        int num = arr.GetLength(1) - opt.values[1] - 3;
                        string prev = string.Empty;
                        for (int k = cell.Index, currArr = opt.values[0], interval = 0; interval < num; k++, currArr++, interval++)
                        {
                            //первая текущая ячейка уже есть, на все последующих мы добавляем столбец и вставляем текст макроса
                            if (interval != 0)
                            {
                                if (opt.columnInsert)
                                {
                                    //Новый столбец
                                    //cell.Row.Cells[k-1].PreferredWidthType = WidthType.None; 
                                    cell.Row.Cells.InsertAfter(k - 1);
                                    //    Rectangle rect = result.editorControl1.richEditControl1.DocumentLayout.GetElement(cell.Row.Cells[k].Range.Start, LayoutType.TableCell).Bounds;
                                    //  rect.Width +=rect.Width<width?(width-rect.Width): -(rect.Width-width);
                                    //  result.editorControl1.richEditControl1.DocumentLayout.GetElement(cell.Row.Cells[k].Range.Start, LayoutType.TableCell).Bounds.Inflate(rect.Width,rect.Height);
                                    //  cell.Row.Cells[k]

                                    //  cell.Row.Cells[k].PreferredWidthType = WidthType.Auto;
                                    //  cell.Row.Cells[k].PreferredWidth = cell.Row.Cells[k - 1].PreferredWidth;
                                    //cell.Row.Cells[k].PreferredWidth = cell.Row.Cells[k-1].PreferredWidth;
                                }
                                result.editorControl1.richEditControl1.Document.InsertRtfText(cell.Table[cell.Row.Index, k].ContentRange.Start, CellText);
                            }
                            // else
                            //    width = result.editorControl1.richEditControl1.DocumentLayout.GetElement(cell.Row.Cells[k].Range.Start, LayoutType.TableCell).Bounds.Width;
                            if (!opt.OneRow)
                            {
                                for (int c = cell.Row.Index, p = 0; c < cell.Table.Rows.Count; c++, p++)
                                {
                                    if (p != 0)
                                    {
                                        if (!result.editorControl1.richEditControl1.Document.GetText(cell.Table[c, k].ContentRange).Contains(opt.textOriginnal))
                                            result.editorControl1.richEditControl1.Document.InsertRtfText(cell.Table[c, k].ContentRange.Start, CellText);
                                    }
                                    result.editorControl1.richEditControl1.Document.ReplaceAll(opt.textOriginnal, arr[p, currArr], SearchOptions.CaseSensitive, cell.Table[c, k].ContentRange);
                                    if (c == cell.Table.Rows.Count - 1 && sk > c)
                                        cell.Table.Rows.InsertAfter(cell.Table.Rows[c].Index);
                                    if (c == cell.Table.Rows.Count - 1)
                                    {
                                        // result.editorControl1.richEditControl1.Document.InsertRtfText(cell.Table[c, k].ContentRange.Start, CellText);
                                        result.editorControl1.richEditControl1.Document.ReplaceAll(opt.textOriginnal, arr[p, currArr], SearchOptions.CaseSensitive, cell.Table[c, k].ContentRange);
                                    }
                                }
                            }
                            else
                            {
                                result.editorControl1.richEditControl1.Document.ReplaceAll(opt.textOriginnal, arr[opt.values[2], currArr], SearchOptions.CaseSensitive, cell.Table[cell.Row.Index, k].ContentRange);
                                if (opt.Merge)
                                {
                                    if (interval != 0)
                                    {
                                        if (prev == arr[opt.values[2], currArr])
                                        {
                                            if (!string.IsNullOrEmpty(prev))
                                                result.editorControl1.richEditControl1.Document.ReplaceAll(arr[opt.values[2], currArr], string.Empty, SearchOptions.CaseSensitive, cell.Table[cell.Row.Index, k].ContentRange);
                                            cell.Table.MergeCells(cell.Table[cell.Row.Index, k - 1], cell.Table[cell.Row.Index, k]);
                                            // Rectangle rect = result.editorControl1.richEditControl1.DocumentLayout.GetElement(cell.Row.Cells[k - 1].Range.Start, LayoutType.TableCell).Bounds;
                                            //  width += width;
                                            //  rect.Width += rect.Width < width ? (width - rect.Width) : -(rect.Width - width);
                                            // result.editorControl1.richEditControl1.DocumentLayout.GetElement(cell.Row.Cells[k].Range.Start, LayoutType.TableCell).Bounds.Inflate(rect.Width, rect.Height);
                                            k--;
                                        }
                                    }
                                    // else
                                    //   width = result.editorControl1.richEditControl1.DocumentLayout.GetElement(cell.Row.Cells[k].Range.Start, LayoutType.TableCell).Bounds.Width;
                                }
                                prev = arr[opt.values[2], currArr];
                            }
                        }
                    }
                    cNew = result.editorControl1.richEditControl1.Document.FindAll(new Regex(Option.macs[j][i].macro + Generate.pattern3, RegexOptions.CultureInvariant));
                }

            }
        }

        //TODO
        private static void SetResultReplaceArr(Result result, string[,] arr, TableCell cell, Generate.TextOption opt, int arrIndex)
        {
            if (!opt.Merge)
                for (int k = cell.Row.Index, l = 0; k < arr.GetLength(0) + cell.Row.Index; k++, l++)
                {
                    result.editorControl1.richEditControl1.Document.ReplaceAll(opt.textOriginnal, arr[l, arrIndex], SearchOptions.CaseSensitive, cell.Table[k, cell.Index].ContentRange);
                }
            else
            {
                string prev = string.Empty;
                for (int k = cell.Row.Index, l = 0; k < arr.GetLength(0) + cell.Row.Index; k++, l++)
                {
                    if (l != 0 && prev == arr[l, arrIndex])
                    {
                        result.editorControl1.richEditControl1.Document.ReplaceAll(opt.textOriginnal, string.Empty, SearchOptions.CaseSensitive, cell.Table[k, cell.Index].ContentRange);
                        cell.Table.MergeCells(cell.Table[k - 1, cell.Index], cell.Table[k, cell.Index]);
                    }
                    else
                        result.editorControl1.richEditControl1.Document.ReplaceAll(opt.textOriginnal, arr[l, arrIndex], SearchOptions.CaseSensitive, cell.Table[k, cell.Index].ContentRange);
                    prev = arr[l, arrIndex];
                }
            }
        }

        private static void MacroRTF(Result result, int j, int i)
        {
            var find = result.editorControl1.richEditControl1.Document.FindAll(Option.macs[j][i].macro, SearchOptions.CaseSensitive);
            foreach (DocumentRange range in find)
            {
                result.editorControl1.richEditControl1.Document.InsertRtfText(range.Start, Option.macs[j][i].result.ToString(), InsertOptions.KeepSourceFormatting);
            }
            result.editorControl1.richEditControl1.Document.ReplaceAll(Option.macs[j][i].macro, string.Empty, SearchOptions.CaseSensitive);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DocPrint(2);
        }

        private void PACKPACKA_Tick(object sender, EventArgs e)
        {
            editorControl1.richEditControl1.Document.ContentChanged -= new EventHandler(change);
            editorControl1.SetMacroColor();
            Packa(ref editorControl1, ResultMacroColor, ResultMacroColor);
            editorControl1.richEditControl1.Document.ContentChanged += new EventHandler(change);
            PACKPACKA.Stop();
        }

        private void timer1_Tick_1(object sender, EventArgs e)
        {

        }

        private void dgPForms_SelectionChanged(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            dataGridView1.Columns.Clear();
            if (dgPForms.CurrentRow == null)
                return;
            bool Yet = false;
            for (int i = 0; i < Option.macs.Count; i++)
            {
                for (int j = 0; j < Option.macs[i].Length; j++)
                {
                    if (Option.macs[i][j].macro == dgPForms.CurrentRow.Cells[1].Value.ToString())
                        switch ((MacroType)Convert.ToInt32(dgPForms.CurrentRow.Cells[0].Value))
                        {
                            case MacroType.String:
                                if (!Yet)
                                {
                                    dataGridView1.Columns.Add(dgPForms.CurrentRow.Cells[1].Value.ToString(), dgPForms.CurrentRow.Cells[1].Value.ToString());
                                    Yet = true;
                                }
                                dataGridView1.Rows.Add(Option.macs[i][j].result);
                                break;
                            case MacroType.Rtf:
                                if (!Yet)
                                {
                                    dataGridView1.Columns.Add(dgPForms.CurrentRow.Cells[1].Value.ToString(), dgPForms.CurrentRow.Cells[1].Value.ToString());
                                    Yet = true;
                                }
                                rtf.RtfText = Option.macs[i][j].result.ToString();
                                dataGridView1.Rows.Add(rtf.Text);
                                break;
                            case MacroType.ArrayString:
                                if (i == 1)
                                    continue;
                                string[,] arr = Option.macs[i][j].result as string[,];
                                if (arr == null)
                                    continue;
                                if (!Yet)
                                {
                                    for (int k = 0; k < arr.GetUpperBound(1) + 1; k++)
                                        dataGridView1.Columns.Add(k.ToString(), k.ToString());
                                    Yet = true;
                                }
                                int rowstart = 0;

                                for (int k = 0; k < arr.GetUpperBound(0) + 1; k++)
                                {
                                    dataGridView1.Rows.Add();
                                    if (k >= Option.macs[i][j].rowStart)
                                    {
                                        dataGridView1.Rows[k].HeaderCell.Value = (rowstart++).ToString();
                                    }
                                    for (int l = 0; l < arr.GetUpperBound(1) + 1; l++)
                                        dataGridView1.Rows[k].Cells[l].Value = arr[k, l];
                                    dataGridView1.Rows[k].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
                                }
                                return;
                                // break;
                        }
                }
            }
        }

        private void dgPForms_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.C)
                return;
            if (dgPForms.CurrentRow == null)
                return;
            if (dgPForms.CurrentCell == null)
                return;
            try
            {
                Clipboard.Clear();
            }
            catch { }
            try
            {
                Clipboard.SetText(dgPForms.CurrentCell.Value.ToString());
            }
            catch { }
        }
    }
}
