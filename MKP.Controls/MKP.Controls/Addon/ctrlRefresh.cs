﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MKP.Addon
{
    public static class ctrlRefresh
    {
        public static List<Control> lst;
        public static bool isInit = false;
        public static Timer tmr;
        public static void AddRefreshList(Control ctrl)
        {
            Init();
            tmr.Stop();
            lst.Add(ctrl);
            tmr.Start();
        }

        private static void Init()
        {
            if (isInit)
                return;
            isInit = true;
            tmr = new Timer();
            lst = new List<Control>();
            tmr.Interval = 100;
            tmr.Tick -= Tick;
            tmr.Tick += Tick;
        }

        private static void Tick(object sender, EventArgs e)
        {
            tmr.Stop();
            if (lst.Count == 0)
                return;
            while (lst.Count > 0)
            {
                lst.Last().Dock = DockStyle.None;
                lst.Last().Dock = DockStyle.Fill;
                lst.Last().ResumeLayout();
                lst.Remove(lst.Last());
            }
        }
        public static void SetDoubleBuffered(System.Windows.Forms.Control c)
        {
            c.SuspendLayout();
            //Taxes: Remote Desktop Connection and painting
            //http://blogs.msdn.com/oldnewthing/archive/2006/01/03/508694.aspx
            if (System.Windows.Forms.SystemInformation.TerminalServerSession)
                return;

            System.Reflection.PropertyInfo aProp =
                  typeof(System.Windows.Forms.Control).GetProperty(
                        "DoubleBuffered",
                        System.Reflection.BindingFlags.NonPublic |
                        System.Reflection.BindingFlags.Instance);

            aProp.SetValue(c, true, null);
        }
    }
}
