﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MKP.Controls.Addon
{
    public static class ExtComboBox
    {
        public static void SetComboBoxWidth<T>(this System.Windows.Forms.ComboBox cb)
        {
            if (cb.Items.Count == 0)
                return;
            System.Drawing.Graphics graphics = cb.CreateGraphics();
            var max = cb.Items.Cast<T>().Max(i => Convert.ToInt32(graphics.MeasureString(i.ToString(), cb.Font).Width));
            cb.DropDownWidth = max + 10;
        }
    }
}
