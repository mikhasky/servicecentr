﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MKP.Controls.Addon
{
    public static class Ext
    {
        public static string GetCountOrEmpty(this int count)
        {
            if (count != 0)
                return count.ToString();
            else
                return string.Empty;
        }
        public static IList<T> Clone<T>(this IList<T> listToClone) where T : ICloneable
        {
            return listToClone.Select(item => (T)item.Clone()).ToList();
        }
        public static IEnumerable<T> Clone<T>(this IEnumerable<T> listToClone) where T : ICloneable
        {
            return listToClone.Select(item => (T)item.Clone());
        }
        public static double getDoubleOr(this string val, double _default)
        {
            if (string.IsNullOrWhiteSpace(val))
                return _default;
            double d;
            if (double.TryParse(val, out d))
                return d;
            MessageBox.Show("Ошибка конвертации значения: " + val);
            return _default;
        }
        public static double getIntOr(this string val, int _default)
        {
            if (string.IsNullOrWhiteSpace(val))
                return _default;
            int d;
            if (int.TryParse(val, out d))
                return d;
            MessageBox.Show("Ошибка конвертации значения: " + val);
            return _default;
        }
        public static double getDoubleOr(this object val, double _default)
        {
            if (val == null)
                return _default;
            string str = val.ToString();
            if (string.IsNullOrWhiteSpace(str))
                return _default;
            double d;
            if (double.TryParse(str, out d))
                return d;
            MessageBox.Show("Ошибка конвертации значения: " + str);
            return _default;
        }
        public static double getIntOr(this object val, int _default)
        {
            if (val == null)
                return _default;
            string str = val.ToString();
            if (string.IsNullOrWhiteSpace(str))
                return _default;
            int d;
            if (int.TryParse(str, out d))
                return d;
            MessageBox.Show("Ошибка конвертации значения: " + str);
            return _default;
        }
    }
}
