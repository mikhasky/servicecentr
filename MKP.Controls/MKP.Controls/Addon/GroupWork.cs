﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MKP.Controls.Addon
{
    public static class GroupWork
    {
        private static Dictionary<int, dWorks> dict = new Dictionary<int, dWorks>();
        public static string GetGroupName(int gr1, int course)
        {
            if (!dict.ContainsKey(gr1))
            {
                using (var db = new DataModels.BDETALONDB(Configurations.instance.ConnBD))
                {
                    dict.Add(gr1, db.GR.Where(i => i.GR1 == gr1).Select(i => new dWorks() { gr19 = i.GR19, gr20 = i.GR20, gr21 = i.GR21, gr22 = i.GR22, gr23 = i.GR23, gr24 = i.GR24, gr3 = i.GR3 }).First());
                    dict[gr1].gr19Added = !string.IsNullOrEmpty(dict[gr1].gr19) ? (dict[gr1].gr19 + $"({dict[gr1].gr3})") : dict[gr1].gr3;
                    dict[gr1].gr20Added = !string.IsNullOrEmpty(dict[gr1].gr20) ? (dict[gr1].gr20 + $"({dict[gr1].gr3})") : dict[gr1].gr3;
                    dict[gr1].gr21Added = !string.IsNullOrEmpty(dict[gr1].gr21) ? (dict[gr1].gr21 + $"({dict[gr1].gr3})") : dict[gr1].gr3;
                    dict[gr1].gr22Added = !string.IsNullOrEmpty(dict[gr1].gr22) ? (dict[gr1].gr22 + $"({dict[gr1].gr3})") : dict[gr1].gr3;
                    dict[gr1].gr23Added = !string.IsNullOrEmpty(dict[gr1].gr23) ? (dict[gr1].gr23 + $"({dict[gr1].gr3})") : dict[gr1].gr3;
                    dict[gr1].gr24Added = !string.IsNullOrEmpty(dict[gr1].gr24) ? (dict[gr1].gr24 + $"({dict[gr1].gr3})") : dict[gr1].gr3;
                }
            }
            string name = GetName(gr1, course);
            return !string.IsNullOrEmpty(name) ? name : dict[gr1].gr3;
        }
        public static string GetGroupName(int gr1, int course, string gr3)
        {
            if (!dict.ContainsKey(gr1))
            {
                using (var db = new DataModels.BDETALONDB(Configurations.instance.ConnBD))
                {
                    dict.Add(gr1, db.GR.Where(i => i.GR1 == gr1).Select(i => new dWorks() { gr19 = i.GR19, gr20 = i.GR20, gr21 = i.GR21, gr22 = i.GR22, gr23 = i.GR23, gr24 = i.GR24, gr3 = i.GR3 }).First());
                    dict[gr1].gr19Added = !string.IsNullOrEmpty(dict[gr1].gr19) ? (dict[gr1].gr19 + $"({dict[gr1].gr3})") : dict[gr1].gr3;
                    dict[gr1].gr20Added = !string.IsNullOrEmpty(dict[gr1].gr20) ? (dict[gr1].gr20 + $"({dict[gr1].gr3})") : dict[gr1].gr3;
                    dict[gr1].gr21Added = !string.IsNullOrEmpty(dict[gr1].gr21) ? (dict[gr1].gr21 + $"({dict[gr1].gr3})") : dict[gr1].gr3;
                    dict[gr1].gr22Added = !string.IsNullOrEmpty(dict[gr1].gr22) ? (dict[gr1].gr22 + $"({dict[gr1].gr3})") : dict[gr1].gr3;
                    dict[gr1].gr23Added = !string.IsNullOrEmpty(dict[gr1].gr23) ? (dict[gr1].gr23 + $"({dict[gr1].gr3})") : dict[gr1].gr3;
                    dict[gr1].gr24Added = !string.IsNullOrEmpty(dict[gr1].gr24) ? (dict[gr1].gr24 + $"({dict[gr1].gr3})") : dict[gr1].gr3;
                }
            }
            string name = GetName(gr1, course, gr3);
            return !string.IsNullOrEmpty(name) ? name : dict[gr1].gr3;
        }
        private static string GetName(int gr1, int course, string gr3)
        {
            switch (course)
            {
                case 1:
                    return dict[gr1].gr19Added;
                case 2:
                    return dict[gr1].gr20Added;
                case 3:
                    return dict[gr1].gr21Added;
                case 4:
                    return dict[gr1].gr22Added;
                case 5:
                    return dict[gr1].gr23Added;
                case 6:
                    return dict[gr1].gr24Added;
            }
            return dict[gr1].gr3;
        }
        private static string GetName(int gr1, int course)
        {
            switch (course)
            {
                case 1:
                    return dict[gr1].gr19;
                case 2:
                    return dict[gr1].gr20;
                case 3:
                    return dict[gr1].gr21;
                case 4:
                    return dict[gr1].gr22;
                case 5:
                    return dict[gr1].gr23;
                case 6:
                    return dict[gr1].gr24;
            }
            return dict[gr1].gr3;
        }
    }
    public class dWorks
    {
        public string gr19;
        public string gr20;
        public string gr21;
        public string gr22;
        public string gr23;
        public string gr24;
        public string gr3;
        public string gr19Added;
        public string gr20Added;
        public string gr21Added;
        public string gr22Added;
        public string gr23Added;
        public string gr24Added;
    }
}
