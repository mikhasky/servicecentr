﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MKP.Controls.Addon
{
    public static class FioExp
    {
        public static string GetFio(string _FirstName, string _LastName, string _PatronymicName, bool fiofull = false)
        {
            if (!fiofull)
                return $"{_FirstName} {GetInitials(_LastName)} {GetInitials(_PatronymicName)}";
            else
                return $"{_FirstName} {_LastName} {_PatronymicName}";
        }
        public static string GetInitials(string val)
        {
            return string.IsNullOrWhiteSpace(val) ? "" : val.Length > 1 ? val.Remove(1) + "." : val.Length == 1 ? val : "";
        }
    }
}
