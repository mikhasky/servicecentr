﻿using System.Resources;
using System.Reflection;
using System.Runtime.InteropServices;

//Общиесведенияобэтойсборкепредоставляютсяследующимнабором
//атрибутов.Отредактируйтезначенияэтихатрибутов,чтобыизменить
//общиесведенияобэтойсборке.
[assembly: AssemblyTitle("MKP.Controls")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("MKP")]
[assembly: AssemblyProduct("MKP.Controls")]
[assembly: AssemblyCopyright("Copyright©MKP2016")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

//УстановказначенияFalseвпараметреComVisibleделаеттипывэтойсборкеневидимыми
//дляCOM-компонентов.Еслинеобходимдоступктипувэтойсборкеиз
//COM,следуетустановитьатрибутComVisibleвTRUEдляэтоготипа.
[assembly: ComVisible(false)]

//СледующийGUIDслужитдляидентификациибиблиотекитипов,еслиэтотпроектбудетвидимымдляCOM
[assembly: Guid("dcde31a5-96a0-4707-80c0-ab52aa4c1e03")]

//Сведенияоверсиисборкисостоятизследующихчетырехзначений:
//
//Основнойномерверсии
//Дополнительныйномерверсии
//Номерсборки
//Редакция
//
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: NeutralResourcesLanguage("ru")]

