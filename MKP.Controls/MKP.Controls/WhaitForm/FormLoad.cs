﻿using DevExpress.XtraSplashScreen;
using DevExpress.XtraWaitForm;
using System;

namespace MKP.Controls.WhaitForm
{
    public partial class FormLoad : WaitForm
    {
        public FormLoad()
        {
            InitializeComponent();
            this.progressPanel1.AutoHeight = true;
            SetCaption(_caption);
            SetDescription(_description);
        }

        #region Overrides

        public override void SetCaption(string caption)
        {
            if (caption == null)
                return;
                caption = _caption;
            base.SetCaption(caption);
            this.progressPanel1.Caption = caption;
        }
        public override void SetDescription(string description)
        {
            if (_description != null)
                description = _description;
            base.SetDescription(description);
            this.progressPanel1.Description = description;
          
        }
        public override void ProcessCommand(Enum cmd, object arg)
        {
            base.ProcessCommand(cmd, arg);
        }
        static string _caption;
        static string _description;
        #endregion
        public static void ShowFormLoad(object userControlOrForm, string caption = null, string description = null)
        {
            _caption = caption;
            _description = description;
           
            try
            {
                if (userControlOrForm is System.Windows.Forms.Form)
                    SplashScreenManager.ShowForm((System.Windows.Forms.Form)userControlOrForm, typeof(WhaitForm.FormLoad), false, false);
                else
                    SplashScreenManager.ShowForm((System.Windows.Forms.UserControl)userControlOrForm, typeof(WhaitForm.FormLoad), false, false);
            }
            catch 
            {

            }
         
        }
        public static void CloseFormLoad()
        {
            SplashScreenManager.CloseForm(false);
        }
        public enum WaitFormCommand
        {

        }
    }
}