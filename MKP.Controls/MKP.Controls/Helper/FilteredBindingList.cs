﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Reflection;
using System.Collections.Specialized;
using System.Runtime.CompilerServices;
using System.Linq;
using System.Linq.Expressions;

namespace MKP.Controls.Helper
{
    public delegate void CollectionItemChangedEventHandler<in T>(T item);
    public class FilteredBindingList<T> : BindingList<T>, IBindingListView, INotifyPropertyChanged, INotifyCollectionChanged
    {
        [TypeForwardedFrom("WindowsBase, Version=3.0.0.0, Culture=Neutral, PublicKeyToken=31bf3856ad364e35")]
        [Serializable]
        private class SimpleMonitor : IDisposable
        {
            private int _busyCount;

            public bool Busy
            {
                get
                {
                    return this._busyCount > 0;
                }
            }

            public void Enter()
            {
                this._busyCount++;
            }

            public void Dispose()
            {
                this._busyCount--;
            }
        }

        private const string CountString = "Count";

        private const string IndexerName = "Item[]";

        private SimpleMonitor _monitor = new SimpleMonitor();

        /// <summary>Возникает при изменениях значения свойства.</summary>

        event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged
        {

            add
            {
                this.PropertyChanged += value;
            }

            remove
            {
                this.PropertyChanged -= value;
            }
        }

        /// <summary>Происходит, когда элемент добавляется, удаляется, изменяется или перемещается, а также при обновлении всего списка.</summary>		
        [CompilerGenerated]
        [field: NonSerialized]
        public virtual event NotifyCollectionChangedEventHandler CollectionChanged;
        /// <summary>Возникает при изменениях значения свойства.</summary>	
        [CompilerGenerated]
        [field: NonSerialized]
        protected virtual event PropertyChangedEventHandler PropertyChanged;
        /// <summary>Возникает при изменениях значения свойства.</summary>	
        [CompilerGenerated]
        [field: NonSerialized]
        public virtual event CollectionItemChangedEventHandler<T> CollectionItemChanged;
        /// <summary>Возникает перед изменениях значения свойства.</summary>	
        [CompilerGenerated]
        [field: NonSerialized]
        public virtual event CollectionItemChangedEventHandler<T> CollectionItemChanging;
        /// <summary>Инициализирует новый экземпляр класса <see cref="T:ObservableCollection`1" />.</summary>	
        public FilteredBindingList()
        { }


        /// <summary>Инициализирует новый экземпляр <see cref="T:System.Collections.ObjectModel.ObservableCollection`1" />, который содержит элементы, скопированные из указанного списка.</summary>
        /// <param name="list">Список, из которого копируются элементы.</param>
        /// <exception cref="T:System.ArgumentNullException">Параметр <paramref name="list" /> не может иметь значение null.</exception>
        public FilteredBindingList(List<T> list) : base((list != null) ? new List<T>(list.Count) : list)
        {
            this.CopyFrom(list);
        }

        /// <summary>Инициализирует новый экземпляр <see cref="T:System.Collections.ObjectModel.ObservableCollection`1" />, который содержит элементы, скопированные из указанного коллекции.</summary>
        /// <param name="collection">Коллекция, из которой копируются элементы.</param>
        /// <exception cref="T:System.ArgumentNullException">Параметр <paramref name="collection" /> не может иметь значение null.</exception>

        public FilteredBindingList(IEnumerable<T> collection)
        {
            if (collection == null)
            {
                throw new ArgumentNullException("collection");
            }
            this.CopyFrom(collection);
        }

        private void CopyFrom(IEnumerable<T> collection)
        {
            IList<T> items = base.Items;
            if (collection != null && items != null)
            {
                using (IEnumerator<T> enumerator = collection.GetEnumerator())
                {
                    while (enumerator.MoveNext())
                    {
                        items.Add(enumerator.Current);
                    }
                }
            }
        }

        /// <summary>Перемещает элемент по указанному индексу на новое местоположение в коллекции.</summary>
        /// <param name="oldIndex">Начинающийся с нуля индекс, указывающий местоположение элемента, который требуется переместить.</param>
        /// <param name="newIndex">Начинающийся с нуля индекс, указывающий новое местоположение элемента.</param>

        public void Move(int oldIndex, int newIndex)
        {
            this.MoveItem(oldIndex, newIndex);
        }

        /// <summary>Удаляет все элементы из коллекции.</summary>

        protected override void ClearItems()
        {
            this.CheckReentrancy();
            base.ClearItems();
            this.OnPropertyChanged("Count");
            this.OnPropertyChanged("Item[]");
            this.OnCollectionReset();
        }

        /// <summary>Удаляет элемент по указанному индексу коллекции.</summary>
        /// <param name="index">Начинающийся с нуля индекс элемента, который требуется удалить.</param>

        protected override void RemoveItem(int index)
        {
            this.CheckReentrancy();
            T t = base[index];
            base.RemoveItem(index);
            this.OnPropertyChanged("Count");
            this.OnPropertyChanged("Item[]");
            this.OnCollectionChanged(NotifyCollectionChangedAction.Remove, t, index);
        }

        /// <summary>Вставляет элемент в коллекцию по указанному индексу.</summary>
        /// <param name="index">Начинающийся с нуля индекс, по которому вставляется <paramref name="item" />.</param>
        /// <param name="item">Объект, который нужно вставить.</param>

        protected override void InsertItem(int index, T item)
        {
            this.CheckReentrancy();
            base.InsertItem(index, item);
            this.OnPropertyChanged("Count");
            this.OnPropertyChanged("Item[]");
            this.OnCollectionChanged(NotifyCollectionChangedAction.Add, item, index);
        }

        /// <summary>Заменяет элемент по указанному индексу.</summary>
        /// <param name="index">Отсчитываемый от нуля индекс заменяемого элемента.</param>
        /// <param name="item">Новое значение элемента по указанному индексу.</param>

        protected override void SetItem(int index, T item)
        {
            this.CheckReentrancy();
            T t = base[index];
            base.SetItem(index, item);
            this.OnPropertyChanged("Item[]");
            this.OnCollectionChanged(NotifyCollectionChangedAction.Replace, t, item, index);
        }

        /// <summary>
        /// Список елементов по вибранным индексам
        /// </summary>
        /// <param name="indexes">индексы</param>
        /// <returns></returns>
        public IEnumerable<T> GetItems(IEnumerable<int> indexes)
        {
            foreach (var index in indexes)
            {
                yield return this[index];
            }
        }

        /// <summary>Перемещает элемент по указанному индексу на новое местоположение в коллекции.</summary>
        /// <param name="oldIndex">Начинающийся с нуля индекс, указывающий местоположение элемента, который требуется переместить.</param>
        /// <param name="newIndex">Начинающийся с нуля индекс указывает новое местоположение элемента.</param>

        protected virtual void MoveItem(int oldIndex, int newIndex)
        {
            this.CheckReentrancy();
            T t = base[oldIndex];
            base.RemoveItem(oldIndex);
            base.InsertItem(newIndex, t);
            this.OnPropertyChanged("Item[]");
            this.OnCollectionChanged(NotifyCollectionChangedAction.Move, t, newIndex, oldIndex);
        }

        /// <summary>Вызывает событие <see cref="E:System.Collections.ObjectModel.ObservableCollection`1.PropertyChanged" /> с предоставленными аргументами.</summary>
        /// <param name="e">Аргументы вызываемого события.</param>

        protected virtual void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, e);
            }
        }

        /// <summary>Вызывает событие <see cref="E:System.Collections.ObjectModel.ObservableCollection`1.CollectionChanged" /> с предоставленными аргументами.</summary>
        /// <param name="e">Аргументы вызываемого события.</param>

        protected virtual void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            if (this.CollectionChanged != null)
            {
                using (this.BlockReentrancy())
                {
                    this.CollectionChanged(this, e);
                }
            }
        }

        /// <summary>Запрещает реентерабельные попытки изменения этой коллекции.</summary>
        /// <returns>Объект <see cref="T:System.IDisposable" />, который может быть использован для уничтожения объекта.</returns>

        protected IDisposable BlockReentrancy()
        {
            this._monitor.Enter();
            return this._monitor;
        }

        /// <summary>Проведите проверку на реентерабельные попытки изменения этой коллекции.</summary>
        /// <exception cref="T:System.InvalidOperationException">Если был выполнен вызов метода <see cref="M:System.Collections.ObjectModel.ObservableCollection`1.BlockReentrancy" />, возвращаемое значение <see cref="T:System.IDisposable" /> которого ещё не было уничтожено.Как правило, это означает проведение дополнительных попыток изменения этой коллекции во время события <see cref="E:System.Collections.ObjectModel.ObservableCollection`1.CollectionChanged" />.Однако это зависит от того, когда производные классы осуществляют вызов метода <see cref="M:System.Collections.ObjectModel.ObservableCollection`1.BlockReentrancy" />.</exception>

        protected void CheckReentrancy()
        {
            if (this._monitor.Busy && this.CollectionChanged != null && this.CollectionChanged.GetInvocationList().Length > 1)
            {
                throw new InvalidOperationException("ObservableCollectionReentrancyNotAllowed");
            }
        }

        private void OnPropertyChanged(string propertyName)
        {
            this.OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
        }

        private void OnCollectionChanged(NotifyCollectionChangedAction action, object item, int index)
        {
            this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(action, item, index));
        }

        private void OnCollectionChanged(NotifyCollectionChangedAction action, object item, int index, int oldIndex)
        {
            this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(action, item, index, oldIndex));
        }

        private void OnCollectionChanged(NotifyCollectionChangedAction action, object oldItem, object newItem, int index)
        {
            this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(action, newItem, oldItem, index));
        }

        private void OnCollectionReset()
        {
            this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        private List<T> originalListValue = new List<T>();
        public List<T> OriginalList
        {
            get
            { return originalListValue; }
        }

        public void AddRange(IEnumerable<T> collection)
        {
            InsertRange(this, this.Count, collection);

        }

        private void InsertRange(BindingList<T> sourse, int index, IEnumerable<T> collection)
        {
            MKP.Ecxeption.Throw<ArgumentNullException>.If(collection == null);
            using (IEnumerator<T> enumerator = collection.GetEnumerator())
            {
                while (enumerator.MoveNext())
                {
                    if (enumerator.Current is INotifyPropertyChanged)
                    {
                        (enumerator.Current as INotifyPropertyChanged).PropertyChanged += FilteredBindingList_PropertyChanged;
                        if(enumerator.Current is INotifyPropertyChanging)
                            (enumerator.Current as INotifyPropertyChanging).PropertyChanging += FilteredBindingList_PropertyChanging; ;                        
                    }
                    sourse.Insert(index++, enumerator.Current);
                }
            }
        }

        private void FilteredBindingList_PropertyChanging(object sender, PropertyChangingEventArgs e)
        {
            if (sender is T)
                CollectionItemChanging?.Invoke((T)sender);
        }

        private void FilteredBindingList_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (sender is T)
                CollectionItemChanged?.Invoke((T)sender);
        }

        #region Searching

        protected override bool SupportsSearchingCore
        {
            get
            {
                return true;
            }
        }

        protected override int FindCore(PropertyDescriptor prop, object key)
        {
            // Get the property info for the specified property.
            PropertyInfo propInfo = typeof(T).GetProperty(prop.Name);
            T item;

            if (key != null)
            {
                // Loop through the items to see if the key
                // value matches the property value.
                for (int i = 0; i < Count; ++i)
                {
                    item = (T)Items[i];
                    if (propInfo.GetValue(item, null)?.Equals(key) ?? false)
                        return i;
                }
            }
            return -1;
        }

        public int Find(string property, object key)
        {
            // Check the properties for a property with the specified name.
            PropertyDescriptorCollection properties =
                TypeDescriptor.GetProperties(typeof(T));
            PropertyDescriptor prop = properties.Find(property, true);

            // If there is not a match, return -1 otherwise pass search to
            // FindCore method.
            if (prop == null)
                return -1;
            else
                return FindCore(prop, key);
        }

        #endregion Searching

        #region Sorting
        ArrayList sortedList;
        FilteredBindingList<T> unsortedItems;
        bool isSortedValue;
        ListSortDirection sortDirectionValue;
        PropertyDescriptor sortPropertyValue;

        protected override bool SupportsSortingCore
        {
            get { return true; }
        }

        protected override bool IsSortedCore
        {
            get { return isSortedValue; }
        }

        protected override PropertyDescriptor SortPropertyCore
        {
            get { return sortPropertyValue; }
        }

        protected override ListSortDirection SortDirectionCore
        {
            get { return sortDirectionValue; }
        }
        public void ApplySort(ListSortDirection direction, params Func<T, object>[] orderedProp)
        {
            if (this.Count == 0 || orderedProp == null || orderedProp.Count() == 0)
                return;
            unsortedItems = new FilteredBindingList<T>();
            unsortedItems.AddRange(this.Items);
           
            IOrderedEnumerable<T> sortedItemsTmp = null;
            for (int i = 0; i < orderedProp.Count(); i++)
            {
                if (i == 0)
                {
                    if (direction == ListSortDirection.Ascending)
                        sortedItemsTmp = Items.OrderBy(orderedProp[i]);
                    else
                        sortedItemsTmp = Items.OrderByDescending(orderedProp[i]);
                }
                else
                {
                    if (direction == ListSortDirection.Ascending)
                        sortedItemsTmp = sortedItemsTmp.ThenBy(orderedProp[i]);
                    else
                        sortedItemsTmp = sortedItemsTmp.ThenByDescending(orderedProp[i]);
                }
            }
            var sortedItems = sortedItemsTmp.ToList();
            this.Items.Clear();
            foreach (var item in sortedItems)
            {
                this.Items.Add(item);
            }
            sortedItems.Clear();
            sortedItems = null;
        }

        public void ApplySort(string propertyName, ListSortDirection direction)
        {
            // Check the properties for a property with the specified name.
            PropertyDescriptor prop = TypeDescriptor.GetProperties(typeof(T))[propertyName];

            // If there is not a match, return -1 otherwise pass search to
            // FindCore method.
            if (prop == null)
                throw new ArgumentException(propertyName +
                    " is not a valid property for type:" + typeof(T).Name);
            else
                ApplySortCore(prop, direction);
        }
        private static Func<T, object> CreatePropertyAccessor(string propertyName)
        {
            var propertyInfo = typeof(T).GetProperty(propertyName);

            // create a parameter (object obj)
            var obj = Expression.Parameter(typeof(object), "obj");
           
            // cast obj to runtimeType
            var objT = Expression.TypeAs(obj, typeof(T));

            // property accessor
            var property = Expression.Property(objT, propertyInfo);

            var convert = Expression.TypeAs(property, typeof(object));
            return (Func<T, object>)Expression.Lambda(convert, obj).Compile();            
        }

        protected override void ApplySortCore(PropertyDescriptor prop,ListSortDirection direction)
        {
            if (this.Count == 0)
                return;
            unsortedItems = new FilteredBindingList<T>();
            unsortedItems.AddRange(this.Items);
            var sortedItems = new List<T>();
            if (direction == ListSortDirection.Ascending)
                sortedItems = Items.OrderBy(CreatePropertyAccessor(prop.Name)).ToList();
            else
                sortedItems = Items.OrderByDescending(CreatePropertyAccessor(prop.Name)).ToList();
            this.Items.Clear();
          
            foreach (var item in sortedItems)
            {
                this.Items.Add(item);
            }
            sortedItems.Clear();
            sortedItems = null;
        }

        protected override void RemoveSortCore()
        {
            this.RaiseListChangedEvents = false;
            // Ensure the list has been sorted.
            if (unsortedItems != null && originalListValue.Count > 0)
            {
                this.Clear();
                if (Filter != null)
                {
                    unsortedItems.Filter = this.Filter;
                    foreach (T item in unsortedItems)
                        this.Add(item);
                }
                else
                {
                    foreach (T item in originalListValue)
                        this.Add(item);
                }
                isSortedValue = false;
                this.RaiseListChangedEvents = true;
                // Raise the list changed event, indicating a reset, and index
                // of -1.
                OnListChanged(new ListChangedEventArgs(ListChangedType.Reset,
                    -1));
            }
        }

        public void RemoveSort()
        {
            RemoveSortCore();
        }


        public override void EndNew(int itemIndex)
        {
            // Check to see if the item is added to the end of the list,
            // and if so, re-sort the list.
            if (IsSortedCore && itemIndex > 0
                && itemIndex == this.Count - 1)
            {
                ApplySortCore(this.sortPropertyValue,
                    this.sortDirectionValue);
                base.EndNew(itemIndex);
            }
        }

        #endregion Sorting

        #region AdvancedSorting
        public bool SupportsAdvancedSorting
        {
            get { return false; }
        }
        public ListSortDescriptionCollection SortDescriptions
        {
            get { return null; }
        }

        public void ApplySort(ListSortDescriptionCollection sorts)
        {
            throw new NotSupportedException();
        }

        #endregion AdvancedSorting

        #region Filtering

        public bool SupportsFiltering
        {
            get { return true; }
        }

        public void RemoveFilter()
        {
            if (Filter != null) Filter = null;
        }

        private string filterValue = null;

        public string Filter
        {
            get
            {
                return filterValue;
            }
            set
            {
                if (filterValue == value) return;

                // If the value is not null or empty, but doesn't
                // match expected format, throw an exception.
                if (!string.IsNullOrEmpty(value) &&
                    !Regex.IsMatch(value,
                    BuildRegExForFilterFormat(), RegexOptions.Singleline))
                    throw new ArgumentException("Filter is not in " +
                          "the format: propName[<>=]'value'.");

                //Turn off list-changed events.
                RaiseListChangedEvents = false;

                // If the value is null or empty, reset list.
                if (string.IsNullOrEmpty(value))
                    ResetList();
                else
                {
                    int count = 0;
                    string[] matches = value.Split(new string[] { " AND " },
                        StringSplitOptions.RemoveEmptyEntries);

                    while (count < matches.Length)
                    {
                        string filterPart = matches[count].ToString();

                        // Check to see if the filter was set previously.
                        // Also, check if current filter is a subset of 
                        // the previous filter.
                        if (!String.IsNullOrEmpty(filterValue)
                                && !value.Contains(filterValue))
                            ResetList();

                        // Parse and apply the filter.
                        SingleFilterInfo filterInfo = ParseFilter(filterPart);
                        ApplyFilter(filterInfo);
                        count++;
                    }
                }
                // Set the filter value and turn on list changed events.
                filterValue = value;
                RaiseListChangedEvents = true;
                OnListChanged(new ListChangedEventArgs(ListChangedType.Reset, -1));
            }
        }


        // Build a regular expression to determine if 
        // filter is in correct format.
        public static string BuildRegExForFilterFormat()
        {
            StringBuilder regex = new StringBuilder();

            // Look for optional literal brackets, 
            // followed by word characters or space.
            regex.Append(@"\[?[\w\s]+\]?\s?");

            // Add the operators: > < or =.
            regex.Append(@"[><=]");

            //Add optional space followed by optional quote and
            // any character followed by the optional quote.
            regex.Append(@"\s?'?.+'?");

            return regex.ToString();
        }

        private void ResetList()
        {
            this.ClearItems();
            var rez = originalListValue.ToList();
            foreach (T t in rez)
                this.Items.Add(t);
            if (IsSortedCore)
                ApplySortCore(SortPropertyCore, SortDirectionCore);
        }


        protected override void OnListChanged(ListChangedEventArgs e)
        {
            // If the list is reset, check for a filter. If a filter 
            // is applied don't allow items to be added to the list.
            if (e.ListChangedType == ListChangedType.Reset)
            {
                if (Filter == null || Filter == "")
                    AllowNew = true;
                else
                    AllowNew = false;
            }
            // Add the new item to the original list.
            if (e.ListChangedType == ListChangedType.ItemAdded)
            {
                OriginalList.Add(this[e.NewIndex]);
                if (!String.IsNullOrEmpty(Filter))
                //if (Filter == null || Filter == "")
                {
                    string cachedFilter = this.Filter;
                    this.Filter = "";
                    this.Filter = cachedFilter;
                }
            }
            // Remove the new item from the original list.
            if (e.ListChangedType == ListChangedType.ItemDeleted)
                OriginalList.RemoveAt(e.NewIndex);
            if (e.ListChangedType == ListChangedType.ItemChanged)
                this.CollectionItemChanged?.Invoke(this[e.NewIndex]);
            base.OnListChanged(e);
        }
              
        internal void ApplyFilter(SingleFilterInfo filterParts)
        {
            List<T> results;

            // Check to see if the property type we are filtering by implements
            // the IComparable interface.
            Type interfaceType =
                TypeDescriptor.GetProperties(typeof(T))[filterParts.PropName]
                .PropertyType.GetInterface("IComparable");

            if (interfaceType == null)
                throw new InvalidOperationException("Filtered property" +
                " must implement IComparable.");

            results = new List<T>();

            // Check each value and add to the results list.
            foreach (T item in this)
            {
                if (filterParts.PropDesc.GetValue(item) != null)
                {
                    IComparable compareValue =
                        filterParts.PropDesc.GetValue(item) as IComparable;
                    int result = -1;
                    if (filterParts.PropDesc.PropertyType != typeof(string))
                    {
                        result = compareValue.CompareTo(filterParts.CompareValue);
                    }
                    else
                    {
                        result = compareValue.ToString().ToLower().StartsWith(filterParts.CompareValue.ToString().ToLower()) ? 0 : -1;
                    }
                    if (filterParts.OperatorValue ==
                        FilterOperator.EqualTo && result == 0)
                        results.Add(item);
                    if (filterParts.OperatorValue ==
                        FilterOperator.GreaterThan && result > 0)
                        results.Add(item);
                    if (filterParts.OperatorValue ==
                        FilterOperator.LessThan && result < 0)
                        results.Add(item);
                }
            }
            this.ClearItems();
            foreach (T itemFound in results)
                this.Add(itemFound);
        }

        internal SingleFilterInfo ParseFilter(string filterPart)
        {
            SingleFilterInfo filterInfo = new SingleFilterInfo();
            filterInfo.OperatorValue = DetermineFilterOperator(filterPart);

            string[] filterStringParts =
                filterPart.Split(new char[] { (char)filterInfo.OperatorValue });

            filterInfo.PropName =
                filterStringParts[0].Replace("[", "").
                Replace("]", "").Replace(" AND ", "").Trim();

            // Get the property descriptor for the filter property name.
            PropertyDescriptor filterPropDesc =
                TypeDescriptor.GetProperties(typeof(T))[filterInfo.PropName];

            // Convert the filter compare value to the property type.
            if (filterPropDesc == null)
                throw new InvalidOperationException("Specified property to " +
                    "filter " + filterInfo.PropName +
                    " on does not exist on type: " + typeof(T).Name);

            filterInfo.PropDesc = filterPropDesc;

            string comparePartNoQuotes = StripOffQuotes(filterStringParts[1]);
            try
            {
                TypeConverter converter =
                    TypeDescriptor.GetConverter(filterPropDesc.PropertyType);
                filterInfo.CompareValue =
                    converter.ConvertFromString(comparePartNoQuotes);
            }
            catch (NotSupportedException)
            {
                throw new InvalidOperationException("Specified filter" +
                    "value " + comparePartNoQuotes + " can not be converted" +
                    "from string. Implement a type converter for " +
                    filterPropDesc.PropertyType.ToString());
            }
            return filterInfo;
        }

        internal FilterOperator DetermineFilterOperator(string filterPart)
        {
            // Determine the filter's operator.
            if (Regex.IsMatch(filterPart, "[^>^<]="))
                return FilterOperator.EqualTo;
            else if (Regex.IsMatch(filterPart, "<[^>^=]"))
                return FilterOperator.LessThan;
            else if (Regex.IsMatch(filterPart, "[^<]>[^=]"))
                return FilterOperator.GreaterThan;
            else
                return FilterOperator.None;
        }

        internal static string StripOffQuotes(string filterPart)
        {
            // Strip off quotes in compare value if they are present.
            if (Regex.IsMatch(filterPart, "'.+'"))
            {
                int quote = filterPart.IndexOf('\'');
                filterPart = filterPart.Remove(quote, 1);
                quote = filterPart.LastIndexOf('\'');
                filterPart = filterPart.Remove(quote, 1);
                filterPart = filterPart.Trim();
            }
            return filterPart;
        }

        #endregion Filtering
    }
    public struct SingleFilterInfo
    {
        internal string PropName;
        internal PropertyDescriptor PropDesc;
        internal Object CompareValue;
        internal FilterOperator OperatorValue;
    }

    // Enum to hold filter operators. The chars 
    // are converted to their integer values.
    public enum FilterOperator
    {
        EqualTo = '=',
        LessThan = '<',
        GreaterThan = '>',     
        None = ' '
    }
}
