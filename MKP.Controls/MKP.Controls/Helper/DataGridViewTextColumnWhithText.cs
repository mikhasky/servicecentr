﻿using System.Drawing;
using System.Windows.Forms;

namespace MKP.Controls.Helper
{
    public class DataGridViewCheckBoxColumnWhithText : DataGridViewTextBoxColumn
    {
        public bool ShowCheckBox { get; set; } = false;
        public DataGridViewCheckBoxColumnWhithText()
        {
            DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        }
        public override DataGridViewCell CellTemplate
        {
            get
            {
                if (ShowCheckBox)
                    return new DataGridViewCheckBoxCellWhithText();
                return new DataGridViewTextBoxCell();
            }
        }
    }
    public delegate void CellCheckedChengedHandler(DataGridViewCheckBoxCellWhithText sender);
    public class DataGridViewCheckBoxCellWhithText : DataGridViewTextBoxCell
    {
        public event CellCheckedChengedHandler CellCheckedChenged;
        Point checkBoxLocation;
        Size checkBoxSize;
        public bool Checked { get; set; } = false;
        public bool ShowCheckBox = true;
        Point _cellLocation = new Point();
        System.Windows.Forms.VisualStyles.CheckBoxState _cbState =
        System.Windows.Forms.VisualStyles.CheckBoxState.UncheckedNormal;

        protected override void Paint(System.Drawing.Graphics graphics, System.Drawing.Rectangle clipBounds,
        System.Drawing.Rectangle cellBounds, int rowIndex, DataGridViewElementStates dataGridViewElementState,
        object value, object formattedValue, string errorText, DataGridViewCellStyle cellStyle,
        DataGridViewAdvancedBorderStyle advancedBorderStyle, DataGridViewPaintParts paintParts)
        {
            base.Paint(graphics, clipBounds, cellBounds, rowIndex, dataGridViewElementState, value,
            formattedValue, errorText, cellStyle, advancedBorderStyle, paintParts);
            Point p = new Point();
            Size s = CheckBoxRenderer.GetGlyphSize(graphics, System.Windows.Forms.VisualStyles.CheckBoxState.UncheckedNormal);
            p.X = cellBounds.Location.X + (s.Width / 2);
            p.Y = cellBounds.Location.Y + (s.Height / 2) - 3;
            _cellLocation = cellBounds.Location;
            checkBoxLocation = p;
            checkBoxSize = s;
            if (Checked)
                _cbState = System.Windows.Forms.VisualStyles.CheckBoxState.CheckedNormal;
            else
                _cbState = System.Windows.Forms.VisualStyles.CheckBoxState.UncheckedNormal;
            if (ShowCheckBox)
                CheckBoxRenderer.DrawCheckBox(graphics, checkBoxLocation, _cbState);
            //  System.Windows.Forms.ComboBoxRenderer.DrawDropDownButton
        }

        protected override void OnMouseClick(DataGridViewCellMouseEventArgs e)
        {
            Point p = new Point(e.X + _cellLocation.X, e.Y + _cellLocation.Y);
            if (p.X >= checkBoxLocation.X && p.X <= checkBoxLocation.X + checkBoxSize.Width
            && p.Y >= checkBoxLocation.Y && p.Y <= checkBoxLocation.Y + checkBoxSize.Height)
            {
                Checked = !Checked;
                CellCheckedChenged?.Invoke(this);
            }
            base.OnMouseClick(e);
            OwningColumn.DataGridView.Refresh();
        }
    }
}
