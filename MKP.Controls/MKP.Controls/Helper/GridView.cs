﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using MKP.PrintDevForms;
using System.Collections;

namespace MKP.Controls.Helper
{
    public class GridView : DataGridView
    {
        public int iNoOfLevels => _stackedHeaderDecorator?.iNoOfLevels ?? 1;
        private int[] editedCell = new int[2];
        internal Header HeaderTree;
        StackedHeaderDecorator _stackedHeaderDecorator;
        public GridView() : base()
        {
            this.BackgroundColor = SystemColors.Menu;           
            this.DoubleBuffered = true;
            _stackedHeaderDecorator = new StackedHeaderDecorator(this);           
        }
        
        public new object DataSource
        {
            get
            {
                return base.DataSource;
            }
            set
            {
                base.DataSource = value;
                if (this.Columns.Count > 0 && value != null && value is BindingSource && (value as BindingSource).DataSource !=null)
                {                   
                    if ((value as BindingSource).DataSource is Type && this.Columns.Count>0)
                    {
                        var prop = ((value as BindingSource).DataSource as Type).GetProperties();                        
                        foreach (var item in prop)
                        {
                            var atr = item.GetCustomAttributes(typeof(System.ComponentModel.DisplayNameAttribute),true);
                            if (atr.Length > 0 && this.Columns.Cast<DataGridViewColumn>().Where(i => i.DataPropertyName == item.Name).Count() > 0)
                                this.Columns.Cast<DataGridViewColumn>().Where(i => i.DataPropertyName == item.Name).ToList().ForEach(i => i.HeaderText = (atr[0] as System.ComponentModel.DisplayNameAttribute).DisplayName);
                                
                        }                       
                    }
                }

            }
        }
       
        protected override void OnKeyDown(KeyEventArgs e)
        {
            editedCell = new int[] { this.CurrentCell.ColumnIndex, this.CurrentCell.RowIndex };
            base.OnKeyDown(e);
        }

        protected override void OnKeyUp(KeyEventArgs e)
        {
            base.OnKeyUp(e);
            SellectCell();
        }
        
        protected override void OnRowsAdded(DataGridViewRowsAddedEventArgs e)
        {
            if (this.Columns.Count > 0 && DataSource != null && DataSource is BindingSource && (DataSource as BindingSource).DataSource != null)
            {              
                if ((DataSource as BindingSource).DataSource is IList && ((DataSource as BindingSource).DataSource as IList).Count>0)
                {
                    var instanceItem = ((DataSource as BindingSource).DataSource as IList)[0];                  
                    var prop = instanceItem.GetType().GetProperties();
                    foreach (var item in prop)
                    {
                        var atr = item.GetCustomAttributes(typeof(System.ComponentModel.DisplayNameAttribute), true);
                        if (atr.Length > 0 && this.Columns.Cast<DataGridViewColumn>().Where(i => i.DataPropertyName == item.Name).Count() > 0)
                            this.Columns.Cast<DataGridViewColumn>().Where(i => i.DataPropertyName == item.Name).ToList().ForEach(i => i.HeaderText = (atr[0] as System.ComponentModel.DisplayNameAttribute).DisplayName);
                    }
                    GenerateStackedHeader();
                }
            }
            base.OnRowsAdded(e);
            SellectCell();
        }

        private void SellectCell()
        {
            if (!(editedCell != null && editedCell.Length > 0 && this.Rows.Count >= editedCell[1]))
                return;
            try
            {
                if (this.CurrentCell == null)
                    return;
                if (this.CurrentCell.ColumnIndex != editedCell[0] && this.CurrentCell.RowIndex != editedCell[1])
                {
                    if (editedCell[1] - 1 < 0)
                        return;
                    this.CurrentCell = this[editedCell[0], editedCell[1] - 1];
                    //editedCell = new int[0];
                }

            }
            catch (Exception)
            {


            }
        }

        private bool _multiColumnHeader;
        /// <summary>
        /// сложный заголов в таблице
        /// </summary>
        public bool MultiColumnHeader
        {
            get
            {
                return _multiColumnHeader;
            }
            set
            {
                _multiColumnHeader = value;
                GenerateStackedHeader();
                RefreshPrivate();
            }
        }
              
        /// <summary>
        /// разделитель
        /// </summary>
        public string ColumnHeaderSpliter { get; set; } = string.Empty;
      
        protected override void OnScroll(ScrollEventArgs e)
        {
            base.OnScroll(e);
            RefreshPrivate();
        }
        public PrintDevForms.Macroses GetMacros(string macrosName,bool onlyVisibleColumns)
        {
            if (!_multiColumnHeader)
                return new Macroses(type: MacroType.ArrayString) { macro = macrosName, result = MKP.Controls.Extension.DataGridViewExtension.ToStringArray(this, onlyVisibleColumns), rowStart = 1 };
            var columns = onlyVisibleColumns ? this.Columns.Cast<DataGridViewColumn>().Where(i => i.Visible) : this.Columns.Cast<DataGridViewColumn>();
            int levelNum = _stackedHeaderDecorator.NoOfLevels(this.HeaderTree) - 1;
            var array = new string[this.RowCount + levelNum, columns.Count()];
           
            int k = 0;
            foreach (var column in columns)
            {
                int j = 0;
                foreach (var item in column.HeaderText.Split(new string[] { this.ColumnHeaderSpliter }, options: StringSplitOptions.None))
                {
                    array[j, k] = item ?? "";
                    j++;
                }
                k++;
            }
            for (var l = 0; l < this.Rows.Count; l++)
            {
                int j = 0;
                foreach (DataGridViewColumn col in columns)
                {
                    array[l+ levelNum, j] = this.Rows[l].Cells[col.Name].Value?.ToString() ?? "";
                    j++;
                }
            }
            return new Macroses(type: MacroType.ArrayString) { macro = macrosName, result = array, rowStart = levelNum };
        }
        protected override void OnColumnAdded(DataGridViewColumnEventArgs e)
        {
            base.OnColumnAdded(e);
            if (MultiColumnHeader)
                e.Column.SortMode = DataGridViewColumnSortMode.NotSortable;
            GenerateStackedHeader();
            RefreshPrivate();
        }
        protected override void OnColumnRemoved(DataGridViewColumnEventArgs e)
        {
            base.OnColumnRemoved(e);
            GenerateStackedHeader();
            RefreshPrivate();
        }
        protected override void OnColumnWidthChanged(DataGridViewColumnEventArgs e)
        {
            base.OnColumnWidthChanged(e);
            RefreshPrivate();
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            if(MultiColumnHeader)
                _stackedHeaderDecorator.RenderColumnHeaders(e.Graphics);
        }
        protected override void OnColumnHeaderCellChanged(DataGridViewColumnEventArgs e)
        {
            base.OnColumnHeaderCellChanged(e);
            GenerateStackedHeader();
            RefreshPrivate();
         
        }

        protected override void OnCellEndEdit(DataGridViewCellEventArgs e)
        {
            base.OnCellEndEdit(e);
        }

        protected override void OnCellBeginEdit(DataGridViewCellCancelEventArgs e)
        {
            if (CurrentCell is DataGridViewTextBoxCell)
            {
                if (CurrentCell.ValueType == typeof(DateTime) || CurrentCell.ValueType == typeof(DateTime?))
                {
                    CalendarCell cell = new CalendarCell();
                    cell.Value = CurrentCell.Value;
                    this[CurrentCell.ColumnIndex, CurrentCell.RowIndex] = cell;
                }
            }
           
            base.OnCellBeginEdit(e);
            
        }
        public void RegenerateStackedHeader()
        {
            GenerateStackedHeader();
            RefreshPrivate();
        }
       
        private void GenerateStackedHeader()
        {
            if (!MultiColumnHeader || this.Columns.Count == 0)
            {
                HeaderTree = null;
                return;
            }

            Header objParentHeader = new Header();
            Dictionary<string, Header> objHeaderTree = new Dictionary<string, Header>();
            int iX = 0;
            foreach (DataGridViewColumn objColumn in this.Columns)
            {

                string[] segments = objColumn.HeaderText.Split(new string[] { this.ColumnHeaderSpliter }, options: StringSplitOptions.None);
                if (segments.Length > 0)
                {
                    string segment = segments[0];
                    Header tempHeader, lastTempHeader = null;
                    if (objHeaderTree.ContainsKey(segment))
                    {
                        tempHeader = objHeaderTree[segment];
                    }
                    else
                    {
                        tempHeader = new Header { Name = segment, X = iX };
                        objParentHeader.Children.Add(tempHeader);
                        objHeaderTree[segment] = tempHeader;
                        tempHeader.ColumnId = objColumn.Index;
                    }
                    for (int i = 1; i < segments.Length; ++i)
                    {
                        segment = segments[i];
                        bool found = false;
                        foreach (Header child in tempHeader.Children)
                        {
                            if (0 == string.Compare(child.Name, segment, StringComparison.InvariantCultureIgnoreCase))
                            {
                                found = true;
                                lastTempHeader = tempHeader;
                                tempHeader = child;
                                break;
                            }
                        }
                        if (!found || i == segments.Length - 1)
                        {
                            Header temp = new Header { Name = segment, X = iX };
                            temp.ColumnId = objColumn.Index;
                            if (found && i == segments.Length - 1 && null != lastTempHeader)
                            {
                                lastTempHeader.Children.Add(temp);
                            }
                            else
                            {
                                tempHeader.Children.Add(temp);
                            }
                            tempHeader = temp;
                        }
                    }
                }
                iX += objColumn.Width;
            }
            HeaderTree = objParentHeader;

        }
        private void RefreshPrivate()
        {
            Rectangle rtHeader = this.DisplayRectangle;
            this.Invalidate(rtHeader);
        }
        internal class StackedHeaderDecorator
        {
            private readonly GridView DataGrid;
            internal int iNoOfLevels { get; private set; }
            private readonly StringFormat objFormat;
            private Graphics objGraphics;

            public StackedHeaderDecorator(GridView dataGrid)
            {
                this.DataGrid = dataGrid;
                objFormat = new StringFormat()
                {
                    Alignment = StringAlignment.Center,
                    LineAlignment = StringAlignment.Center
                };
            }

            internal void RenderColumnHeaders(Graphics graphics)
            {
                if (this.DataGrid.HeaderTree == null)
                    return;
                objGraphics = graphics;
                iNoOfLevels = NoOfLevels(DataGrid.HeaderTree) - 1;
                DataGrid.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
                DataGrid.ColumnHeadersHeight = iNoOfLevels * 20;
                RenderColumnHeaders();
            }

            private void RenderColumnHeaders()
            {
                objGraphics.FillRectangle(new SolidBrush(DataGrid.ColumnHeadersDefaultCellStyle.BackColor),
                                          new Rectangle(DataGrid.DisplayRectangle.X + 1, DataGrid.DisplayRectangle.Y + 1,
                                                        DataGrid.DisplayRectangle.Width - 2, DataGrid.ColumnHeadersHeight - 2));
              
                foreach (Header objChild in DataGrid.HeaderTree.Children)
                {
                    objChild.Measure(DataGrid, 0, DataGrid.ColumnHeadersHeight / iNoOfLevels);
                    objChild.AcceptRenderer(this);
                }
            }

            public void Render(Header objHeader)
            {
                if (objHeader.Children.Count == 0)
                {
                    Rectangle r1 = DataGrid.GetColumnDisplayRectangle(objHeader.ColumnId, true);
                    if (r1.Width == 0)
                    {
                        return;
                    }
                    r1.Y = objHeader.Y + 1;
                    r1.Width += 1;
                    r1.X -= 1;
                    r1.Height = objHeader.Height;
                    objGraphics.SetClip(r1);

                    if (r1.X + DataGrid.Columns[objHeader.ColumnId].Width < DataGrid.DisplayRectangle.Width)
                    {
                        r1.X -= (DataGrid.Columns[objHeader.ColumnId].Width - r1.Width);
                    }
                    r1.X -= 1;
                    r1.Width = DataGrid.Columns[objHeader.ColumnId].Width;
                    objGraphics.DrawRectangle(Pens.Gray, r1);
                    objGraphics.DrawString(objHeader.Name,
                                           DataGrid.ColumnHeadersDefaultCellStyle.Font,
                                           new SolidBrush(DataGrid.ColumnHeadersDefaultCellStyle.ForeColor),
                                           r1,
                                           objFormat);
                    
                    objGraphics.ResetClip();
                }
                else
                {
                    Rectangle r1 = DataGrid.GetCellDisplayRectangle(objHeader.ColumnId, -1, true);
                    if (r1.Width == 0)
                    {
                        return;
                    }
                    int rowHeaderWidth = DataGrid.RowHeadersVisible ? DataGrid.RowHeadersWidth : 1;
                    int x = rowHeaderWidth;
                    for (int i = 0; i < objHeader.Children[0].ColumnId; ++i)
                    {
                        if (DataGrid.Columns[i].Visible)
                        {
                             x += DataGrid.Columns[i].Width;                           
                        }
                    }
                    if (x > (DataGrid.HorizontalScrollingOffset + DataGrid.DisplayRectangle.Width - 5))
                    {
                        return;
                    }
                    
                    r1.Y = objHeader.Y + 1;
                    r1.Height = objHeader.Height;
                    r1.Width = objHeader.Width + 1;
                    if (r1.X < rowHeaderWidth)
                    {
                        r1.X = rowHeaderWidth;
                    }
                    r1.X -= 1;
                    objGraphics.SetClip(r1);
                    if (!DataGrid.Columns[objHeader.ColumnId].Frozen)
                        r1.X = x - DataGrid.HorizontalScrollingOffset;
                    r1.Width -= 1;
                    objGraphics.DrawRectangle(Pens.Gray, r1);
                    r1.X -= 1;
                    objGraphics.DrawString(objHeader.Name, DataGrid.ColumnHeadersDefaultCellStyle.Font,
                                           new SolidBrush(DataGrid.ColumnHeadersDefaultCellStyle.ForeColor),
                                           r1, objFormat);                    
                    objGraphics.ResetClip();
                }
            }

            internal int NoOfLevels(Header header)
            {
                int level = 0;
                foreach (Header child in header.Children)
                {
                    int temp = NoOfLevels(child);
                    level = temp > level ? temp : level;
                }
                return level + 1;
            }
        }
        internal class Header
        {
            public List<Header> Children { get; set; }

            public string Name { get; set; }

            public int X { get; set; }

            public int Y { get; set; }

            public int Width { get; set; }

            public int Height { get; set; }

            public int ColumnId { get; set; }

            public Header()
            {
                Name = string.Empty;
                Children = new List<Header>();
                ColumnId = -1;
            }

            public void Measure(GridView objGrid, int iY, int iHeight)
            {
                Width = 0;
                if (Children.Count > 0)
                {
                    int tempY = string.IsNullOrEmpty(Name.Trim()) ? iY : iY + iHeight;
                    bool columnWidthSet = false;
                    foreach (Header child in Children)
                    {
                        child.Measure(objGrid, tempY, iHeight);
                        Width += child.Width;
                        if (!columnWidthSet && Width > 0)
                        {
                            ColumnId = child.ColumnId;
                            columnWidthSet = true;
                        }
                    }
                }
                else if (-1 != ColumnId && objGrid.Columns[ColumnId].Visible)
                {
                    Width = objGrid.Columns[ColumnId].Width;
                }
                Y = iY;
                if ((Children.Count == 0))
                {
                    Height = objGrid.ColumnHeadersHeight - iY;
                }
                else
                {
                    Height = iHeight;
                }
            }

            internal void AcceptRenderer(StackedHeaderDecorator objRenderer)
            {
                foreach (Header objChild in Children)
                {
                    objChild.AcceptRenderer(objRenderer);
                }
                if (-1 != ColumnId && Name != null /*!string.IsNullOrEmpty(Name.Trim())*/)
                {
                    objRenderer.Render(this);
                }

            }
        }
    }
}
