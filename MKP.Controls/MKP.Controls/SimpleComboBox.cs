﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MKP.Controls
{
    public class SimpleComboBox : IComparer<SimpleComboBox>, IComparable<SimpleComboBox>
    {
        public string Name { get; set; }
        public int Id { get; set; }
        public SimpleComboBox Value
        {
            get { return this; }
        }
        public object[] buffer { get; set; }
        public object Data;
        public SimpleComboBox(string Name, int Id)
        {
            this.Name = Name;
            this.Id = Id;
        }
        public SimpleComboBox(int Id, string Name)
        {
            this.Name = Name;
            this.Id = Id;
        }
        public override string ToString()
        {
            return Name;
        }
        public string getId()
        {
            return Id.ToString();
        }
        public int GetIntId()
        {
            return Id;
        }
        public void Rename(string newName)
        {
            this.Name = newName;
        }
        public override bool Equals(object obj)
        {
            SimpleComboBox t = obj as SimpleComboBox;
            if (t == null)
                return false;
            return t.Id == this.Id;
        }
        public int Compare(SimpleComboBox x, SimpleComboBox y)
        {
            if (x.Id < y.Id)
                return -1;
            if (x.Id > y.Id)
                return 1;
            return 0;

        }

        public int CompareTo(SimpleComboBox other)
        {
            if (other == null)
                return 1;
            return this.Id.CompareTo(other.Id);
        }
    }
    public class Combo3
    {
        public int gr1;
        public int sg1;
        public string gr3;
        public short sem4;
        public int sg4;
        public int? gr7;
        public short sem7;
        public string sp2;
        public int sem3;
        public override string ToString()
        {
            return gr3;
        }
    }
}
