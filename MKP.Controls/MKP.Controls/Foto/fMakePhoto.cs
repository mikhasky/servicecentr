﻿using System;
using System.Drawing;
using System.Windows.Forms;
using AForge.Video.DirectShow;

namespace MKP.Controls.Foto
{
    public delegate void PhotoHandler(Bitmap image);
    public partial class fMakePhoto : Form
    {
        public event PhotoHandler PhotoCreated;
        FilterInfoCollection videodevices;
        VideoCaptureDevice videoSource;
        public fMakePhoto()
        {
            InitializeComponent();
            this.FormClosed += FMakePhoto_FormClosed;
        }

        private void FMakePhoto_FormClosed(object sender, FormClosedEventArgs e)
        {
            videodevices = null;
            videoSource = null;
        }

        private void fMakeFoto_Shown(object sender, EventArgs e)
        {
            videodevices = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            if (videodevices.Count == 0)
            {
                MessageBox.Show("Не подключена ни одна веб камера!");
                CLoseThread(null);
                return;
            }
            videoSource = new VideoCaptureDevice(videodevices[0].MonikerString);        
            videoSourcePlayer1.VideoSource = videoSource;
            videoSource.Start();
        }

        private void CLoseThread(Bitmap image)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(() =>{
                  
                    if (image != null)
                        PhotoCreated?.Invoke(image);
                    videoSource.SignalToStop();
                    videoSource.WaitForStop();
                    this.Close();
                }));
            }
            else
            {
                this.Close();
                if(image!=null)
                    PhotoCreated?.Invoke(image);
                videoSource.SignalToStop();
                videoSource.WaitForStop();
    
            }
        }

        private void btn_MakeFoto_Click(object sender, EventArgs e)
        {
            CLoseThread(videoSourcePlayer1.GetCurrentVideoFrame());           
        }

        private void btn_Close_Click(object sender, EventArgs e)
        {
            CLoseThread(null);
        }
    }
}
