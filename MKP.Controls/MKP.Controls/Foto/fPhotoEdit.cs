﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.Windows.Forms;

namespace MKP.Controls.Foto
{
    public delegate void EndPhotoEditEventHandler(Bitmap image, bool edited);
    public partial class fPhotoEdit : Form
    {
        #region Конструктор, событие редактирования, стек изменений
        public event EndPhotoEditEventHandler EndPhotoEdit;
        Stack<Image> UChanges = new Stack<Image>(10);
        Stack<Image> RChanges = new Stack<Image>(10);
        private Image startImage;
        private string connectionStringToBD;
        private fPhotoEdit()
        {
            InitializeComponent();
        }
        public fPhotoEdit(Bitmap imageForEdit, string ConnectionStringToBD) : this()
        {
            System.Reflection.PropertyInfo pi = typeof(Control).GetProperty("DoubleBuffered", System.Reflection.BindingFlags.SetProperty
               | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
            pi.SetValue(pictureBox1, true, null);
            startImage = imageForEdit;
            connectionStringToBD = ConnectionStringToBD;
            LoadFoto(imageForEdit);
        }
        #endregion

        #region открытие фото или снимок с камеры
        private void btn_MakeFoto_Click(object sender, EventArgs e)
        {
            if (pictureBox1.Image != null && UChanges.Count > 0)
            {
                if (MessageBox.Show("Есть не сохранённые изменения. Сохарнить?", "", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    btn_Save_Click(this, null);
                else if (MessageBox.Show("Не сохранённые изменения. Будут потеряны!", "", MessageBoxButtons.OKCancel) != DialogResult.OK)
                    return;
            }
            using (var photo = new fMakePhoto())
            {
                photo.PhotoCreated += Photo_PhotoCreated;
                photo.ShowDialog();
            }
        }

        private void Photo_PhotoCreated(Bitmap image)
        {
            if (image == null)
                return;
            if (this.InvokeRequired)
                this.Invoke(new MethodInvoker(() => LoadFoto(image)));
            else
                LoadFoto(image);
        }

        private void btn_Open_Click(object sender, EventArgs e)
        {
            using (var ofd = new OpenFileDialog())
            {

                string fileName;
                ofd.InitialDirectory = @"C:\";
                ofd.Title = "Выбирите изображение";
                ofd.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    fileName = ofd.FileName;
                    Image img = System.Drawing.Image.FromFile(fileName);
                    LoadFoto(img);
                }
            }
        }
        #endregion

        #region Private Methods 
        private void LoadFoto(Image image)
        {
            var param = MKP.DBImplementation.WorkWhithDB.GetData<int>(@"SELECT per2 FROM per WHERE per1 in (73,74) ORDER BY per1", connectionStringToBD);
            if (param.Count == 2)
            {
                tbWidth.Text = param[0].ToString();
                tbHeight.Text = param[1].ToString();
            }
            else
            {
                tbWidth.Text = "100";
                tbHeight.Text = "100";
            }
            

            if (image == null)
                return;    
            adjustWindow(image.Width, image.Height);
            pictureBox1.Image = image;          
            btn_Undo.Enabled = false;
            btn_Redo.Enabled = false;
            RChanges.Clear();
            UChanges.Clear();
            trackBar_Size.Value = 100;
        }

        public void adjustWindow(int width, int height)
        {
            //trackBar_Size.Value = 100;
           
            pictureBox1.Height = height;

            pictureBox1.Width = width;
            clipRect.Location = new Point(pictureBox1.Width / 2 - clipRect.Width / 2, pictureBox1.Height / 2 - clipRect.Height / 2);
        }

       
        #endregion

        #region Поворот
        private Bitmap Rotate(Bitmap bitmap, int degrees)
        {
            AForge.Imaging.Filters.RotateBicubic filter = new AForge.Imaging.Filters.RotateBicubic(degrees);
            return filter.Apply(bitmap);
        }
        private void trackBar_Rotate_Scroll(object sender, EventArgs e)
        {
            if (pictureBox1.Image == null)
            {
                trackBar_Rotate.Value = 100;
                return;
            }
            UCAdd(pictureBox1.Image);
            pictureBox1.Image = Rotate((Bitmap)pictureBox1.Image, trackBar_Rotate.Value);
            adjustWindow(pictureBox1.Image.Width, pictureBox1.Image.Height);
            RChanges.Clear();
            trackBar_Rotate.Value = 0;
        }
        
        #endregion

        #region размера        
        private Image ScaleByPercent(Image imgPhoto, int Percent)
        {
            float nPercent = ((float)Percent / 100);
            int sourceWidth = (int)imgPhoto.Width;
            int sourceHeight = (int)imgPhoto.Height;
            int sourceX = 0;
            int sourceY = 0;
            int destX = 0;
            int destY = 0;
            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);
            if (destWidth == 0 || destHeight == 0)
                return imgPhoto;
            Bitmap bmPhoto = new Bitmap(destWidth, destHeight,
                                     PixelFormat.Format24bppRgb);
            bmPhoto.SetResolution(imgPhoto.HorizontalResolution,
                                    imgPhoto.VerticalResolution);
            Graphics grPhoto = Graphics.FromImage(bmPhoto);
            grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;
            grPhoto.DrawImage(imgPhoto,
                new Rectangle(destX, destY, destWidth, destHeight),
                new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
                GraphicsUnit.Pixel);
            grPhoto.Dispose();
            return bmPhoto;
        }
        private void trackBar_Size_Scroll(object sender, EventArgs e)
        {
            if (pictureBox1.Image == null)
            {
                trackBar_Size.Value = 100;
                return;
            }
            UCAdd(pictureBox1.Image);
            pictureBox1.Image = ScaleByPercent(pictureBox1.Image, trackBar_Size.Value);
            adjustWindow(pictureBox1.Image.Width, pictureBox1.Image.Height);
            RChanges.Clear();

        }
        #endregion

        #region Отмена изменений
        private void UCAdd(Image img)
        {
            UChanges.Push(img);
            btn_Undo.Enabled = true;
            GC.Collect();
        }

        private void RCAdd(Image img)
        {
            RChanges.Push(img);
            btn_Redo.Enabled = true;
            GC.Collect();
        }
        private void btn_Undo_Click(object sender, EventArgs e)
        {
            if (UChanges.Count != 0)
            {
                RCAdd(pictureBox1.Image);
                pictureBox1.Image = UChanges.Pop();
                if (UChanges.Count == 0)
                {
                    btn_Undo.Enabled = false;
                }
                else
                {
                    btn_Undo.Enabled = true;
                }
            }
        }

        private void btn_Redo_Click(object sender, EventArgs e)
        {
            if (RChanges.Count != 0)
            {
                UCAdd(pictureBox1.Image);
                pictureBox1.Image = RChanges.Pop();
                if (RChanges.Count == 0)
                {
                    btn_Redo.Enabled = false;
                }
                else
                {
                    btn_Redo.Enabled = true;
                }
            }
        }
        #endregion

        #region Cохранение
        private void btn_Save_Click(object sender, EventArgs e)
        {
            using (SaveFileDialog Save = new SaveFileDialog())
            {
                Save.Title = "Save";
                Save.Filter = "Please select a file type||Bitmap File (*.bmp)|*.bmp|JPEG file (*.jpg)|*.jpg|PNG file(*.png)|*.png";

                try
                {
                    if (Save.ShowDialog() == System.Windows.Forms.DialogResult.Cancel)
                    {
                        return;
                    }

                    if (Save.FileName == "")
                    {
                        return;
                    }
                    else if (Save.FileName.Contains(".jpg"))
                    {
                        pictureBox1.Image.Save(Save.FileName, System.Drawing.Imaging.ImageFormat.Jpeg);
                    }
                    else if (Save.FileName.Contains(".png"))
                    {
                        pictureBox1.Image.Save(Save.FileName, System.Drawing.Imaging.ImageFormat.Png);
                    }
                    else
                    {
                        pictureBox1.Image.Save(Save.FileName, System.Drawing.Imaging.ImageFormat.Bmp);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Ошибка при сохранении:" + ex.Message);
                    return;
                }
            }
            UChanges.Clear();
            RChanges.Clear();
            btn_Undo.Enabled = false;
            btn_Redo.Enabled = false;
        }
        #endregion

        #region Обрезка изображения       
        private Rectangle clipRect;

        private Point PrevMouse;
        private Brush myBrush = new SolidBrush(Color.FromArgb(170, Color.DimGray));
        private Region mainRegion;

        private const Int32 threshold = 15; // пороговое значение угла растяжки

        private Boolean DragTopLeft = false;
        private Boolean DragTopRight = false;
        private Boolean DragBottomLeft = false;
        private Boolean DragBottomRight = false;
        private Boolean DragCentre = false;       

        private void button1_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.Default;
            if (pictureBox1.Image != null
                && !(pictureBox1.Image.Width == 0 || pictureBox1.Image.Height == 0)
                && !(clipRect.Width == 0 || clipRect.Height == 0))
            {
                UCAdd(pictureBox1.Image);
                Bitmap clippedBmp = new Bitmap(clipRect.Width, clipRect.Height);

                using (Graphics g = Graphics.FromImage(clippedBmp))
                {
                    g.DrawImage(pictureBox1.Image,
                                    new Rectangle(0, 0, clipRect.Width, clipRect.Height),
                                    clipRect,
                                    GraphicsUnit.Pixel);

                }
                pictureBox1.Image = clippedBmp;
                adjustWindow(clippedBmp.Width, clippedBmp.Height);
                clipRect.Location = new Point(pictureBox1.Width / 2 - clipRect.Width / 2, pictureBox1.Height / 2 - clipRect.Height / 2);
            }
        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left ||
                (SystemInformation.MouseButtonsSwapped &&
                 e.Button == MouseButtons.Right))
            {
                Point topLeft = new Point(clipRect.Left, clipRect.Top);
                Point topRight = new Point(clipRect.Right, clipRect.Top);
                Point bottomLeft = new Point(clipRect.Left, clipRect.Bottom);
                Point bottomRight = new Point(clipRect.Right, clipRect.Bottom);

                Point mouse = new Point(e.X, e.Y);

                /*if (GetDistance(mouse, topLeft) <= threshold)
                {
                    DragTopLeft = true;
                    Cursor = Cursors.SizeNWSE;
                }
                else if (GetDistance(mouse, topRight) <= threshold)
                {
                    DragTopRight = true;
                    Cursor = Cursors.SizeNESW;
                }
                else if (GetDistance(mouse, bottomLeft) <= threshold)
                {
                    DragBottomLeft = true;
                    Cursor = Cursors.SizeNESW;
                }
                else if (GetDistance(mouse, bottomRight) <= threshold)
                {
                    DragBottomRight = true;
                    Cursor = Cursors.SizeNWSE;
                }
                else*/
                if (clipRect.Contains(mouse))
                {
                    DragCentre = true;
                    Cursor = Cursors.SizeAll;
                }
            }
        }
        private double GetDistance(Point pnt1, Point pnt2)
        {
            int a = pnt2.X - pnt1.X;
            int b = pnt2.Y - pnt1.Y;

            return Math.Sqrt(a * a + b * b);
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawRectangle(new Pen(Brushes.Lime, 3), clipRect);

            //отрисовка затемненного региона
            mainRegion = new Region(pictureBox1.DisplayRectangle);
            mainRegion.Xor(clipRect);
            e.Graphics.FillRegion(myBrush, mainRegion);
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            Point topLeft = new Point(clipRect.Left, clipRect.Top);
            Point topRight = new Point(clipRect.Right, clipRect.Top);
            Point bottomLeft = new Point(clipRect.Left, clipRect.Bottom);
            Point bottomRight = new Point(clipRect.Right, clipRect.Bottom);

            Point mouse = new Point(e.X, e.Y);

            if (DragTopLeft)
            {
                topLeft = mouse;
                bottomLeft.X = topLeft.X;
                topRight.Y = topLeft.Y;
            }
            else if (DragTopRight)
            {
                topRight = mouse;
                bottomRight.X = topRight.X;
                topLeft.Y = topRight.Y;
            }
            else if (DragBottomLeft)
            {
                bottomLeft = mouse;
                topLeft.X = bottomLeft.X;
                bottomRight.Y = bottomLeft.Y;
            }
            else if (DragBottomRight)
            {
                bottomRight = mouse;
                topRight.X = bottomRight.X;
                bottomLeft.Y = bottomRight.Y;
            }
            else if (DragCentre)
            {
                topLeft.X += (mouse.X - (clipRect.Width / 2)) - (PrevMouse.X - (clipRect.Width / 2));
                topLeft.Y += (mouse.Y - (clipRect.Height / 2)) - (PrevMouse.Y - (clipRect.Height / 2));

                topRight.X += (mouse.X + (clipRect.Width / 2)) - (PrevMouse.X + (clipRect.Width / 2));
                topRight.Y = topLeft.Y;

                bottomLeft.X = topLeft.X;
                bottomLeft.Y += (mouse.Y + (clipRect.Height / 2)) - (PrevMouse.Y + (clipRect.Height / 2));

                bottomRight.X = topRight.X;
                bottomRight.Y = bottomLeft.Y;
            }
            /*else if (GetDistance(mouse, topLeft) <= threshold)
            {
                Cursor = Cursors.SizeNWSE;
            }
            else if (GetDistance(mouse, topRight) <= threshold)
            {
                Cursor = Cursors.SizeNESW;
            }
            else if (GetDistance(mouse, bottomLeft) <= threshold)
            {
                Cursor = Cursors.SizeNESW;
            }
            else if (GetDistance(mouse, bottomRight) <= threshold)
            {
                Cursor = Cursors.SizeNWSE;
            }
            else if (clipRect.Contains(mouse))
            {
                Cursor = Cursors.SizeAll;
            }*/
            else
                Cursor = Cursors.Arrow;


            int width = topRight.X - topLeft.X;
            int height = bottomLeft.Y - topLeft.Y;
            clipRect = new Rectangle(topLeft.X, topLeft.Y, width, height);

            tbWidth.Text = clipRect.Width.ToString();
            tbHeight.Text = clipRect.Height.ToString();
            this.Refresh();
            PrevMouse = new Point(e.X, e.Y);
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            if (DragBottomLeft || DragBottomRight || DragTopLeft || DragTopRight)
            {
                tbWidth_Leave(null, null);
                tbHeight_Leave(null, null);
            }
            DragTopLeft = false;
            DragTopRight = false;
            DragBottomLeft = false;
            DragBottomRight = false;
            DragCentre = false;

            Cursor = Cursors.Arrow;
        }
        #endregion

        #region Печать
        private void btn_Print_Click(object sender, EventArgs e)
        {
            using (PrintDialog pd = new PrintDialog())
            {
                pd.Document = new System.Drawing.Printing.PrintDocument();
                pd.Document.PrintPage += PrintPage;
                if (pd.ShowDialog() == DialogResult.OK)
                    pd.Document.Print();
            }
        }

        private void PrintPage(object sender, PrintPageEventArgs e)
        {
            if (pictureBox1.Image == null)
                return;
            Point loc = new Point(100, 100);
            e.Graphics.DrawImage(pictureBox1.Image, loc);
            (sender as PrintDocument).PrintPage -= PrintPage;
        }
        #endregion

        private void fPhotoEdit_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (pictureBox1.Image != null 
                && !(pictureBox1.Image.Width == 0 || pictureBox1.Image.Height == 0)
                && !(clipRect.Width == 0 || clipRect.Height == 0))
            {
                Bitmap clippedBmp = new Bitmap(clipRect.Width, clipRect.Height);

                using (Graphics g = Graphics.FromImage(clippedBmp))
                {
                    g.DrawImage(pictureBox1.Image,
                                    new Rectangle(0, 0, clipRect.Width, clipRect.Height),
                                    clipRect,
                                    GraphicsUnit.Pixel);

                }
                if ((startImage != null ? !startImage.Equals((Image)clippedBmp) : clippedBmp != null))
                    if (MessageBox.Show("Есть не сохранённые изменения. Сохранить?", "", MessageBoxButtons.OKCancel) != DialogResult.OK)                        
                            return;
                EndPhotoEdit?.Invoke(clippedBmp, (startImage != null ? !startImage.Equals((Image)clippedBmp) : clippedBmp != null));
            }
          
            
        }
        private void button1_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void tbWidth_Leave(object sender, EventArgs e)
        {
            MKP.DBImplementation.WorkWhithDB.ExecutNonQuery(@"UPDATE per SET per2 = @p1 WHERE per1 = 73", connectionStringToBD, parameters: new KeyValuePair<string, object>("@p1", tbWidth.Text));
        }

        private void tbHeight_Leave(object sender, EventArgs e)
        {
            MKP.DBImplementation.WorkWhithDB.ExecutNonQuery(@"UPDATE per SET per2 = @p1 WHERE per1 = 74", connectionStringToBD, parameters: new KeyValuePair<string, object>("@p1", tbHeight.Text));
        }

        private void tbHeight_TextChanged(object sender, EventArgs e)
        {
            clipRect.Height = Convert.ToInt32(tbHeight.Text);
        }

        private void tbWidth_TextChanged(object sender, EventArgs e)
        {
            clipRect.Width = Convert.ToInt32(tbWidth.Text);
        }

        private void tb_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar)
                && !char.IsDigit(e.KeyChar)
                && e.KeyChar != '.')
            {
                e.Handled = true;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (pictureBox1.Image != null
                && !(pictureBox1.Image.Width == 0 || pictureBox1.Image.Height == 0)
                && !(clipRect.Width == 0 || clipRect.Height == 0))
            {
                Bitmap clippedBmp = new Bitmap(clipRect.Width, clipRect.Height);

                using (Graphics g = Graphics.FromImage(clippedBmp))
                {
                    g.DrawImage(pictureBox1.Image,
                                    new Rectangle(0, 0, clipRect.Width, clipRect.Height),
                                    clipRect,
                                    GraphicsUnit.Pixel);

                }               
                EndPhotoEdit?.Invoke(clippedBmp, (startImage != null ? !startImage.Equals((Image)clippedBmp) : clippedBmp != null));
            }
        }
    }
}
