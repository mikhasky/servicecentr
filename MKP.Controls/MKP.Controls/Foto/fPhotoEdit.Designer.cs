﻿namespace MKP.Controls.Foto
{
    partial class fPhotoEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fPhotoEdit));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_Open = new System.Windows.Forms.Button();
            this.trackBar_Size = new System.Windows.Forms.TrackBar();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_Undo = new System.Windows.Forms.Button();
            this.btn_Redo = new System.Windows.Forms.Button();
            this.btn_MakeFoto = new System.Windows.Forms.Button();
            this.btn_Save = new System.Windows.Forms.Button();
            this.btn_Print = new System.Windows.Forms.Button();
            this.trackBar_Rotate = new System.Windows.Forms.TrackBar();
            this.label2 = new System.Windows.Forms.Label();
            this.panel_ImageBox = new System.Windows.Forms.Panel();
            this.pictureBox1 = new AForge.Controls.PictureBox();
            this.btn_Cut = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.tbWidth = new System.Windows.Forms.TextBox();
            this.tbHeight = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar_Size)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar_Rotate)).BeginInit();
            this.panel_ImageBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoScroll = true;
            this.tableLayoutPanel1.ColumnCount = 9;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 57F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 82F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.btn_Open, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.trackBar_Size, 7, 2);
            this.tableLayoutPanel1.Controls.Add(this.label1, 6, 2);
            this.tableLayoutPanel1.Controls.Add(this.btn_Undo, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.btn_Redo, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.btn_MakeFoto, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.btn_Save, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.btn_Print, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.trackBar_Rotate, 7, 4);
            this.tableLayoutPanel1.Controls.Add(this.label2, 6, 4);
            this.tableLayoutPanel1.Controls.Add(this.panel_ImageBox, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.btn_Cut, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.button1, 7, 6);
            this.tableLayoutPanel1.Controls.Add(this.tbWidth, 7, 1);
            this.tableLayoutPanel1.Controls.Add(this.tbHeight, 7, 0);
            this.tableLayoutPanel1.Controls.Add(this.label3, 6, 0);
            this.tableLayoutPanel1.Controls.Add(this.label4, 6, 1);
            this.tableLayoutPanel1.Controls.Add(this.button2, 8, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 7;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 309F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1008, 681);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // btn_Open
            // 
            this.btn_Open.Image = ((System.Drawing.Image)(resources.GetObject("btn_Open.Image")));
            this.btn_Open.Location = new System.Drawing.Point(3, 3);
            this.btn_Open.Name = "btn_Open";
            this.btn_Open.Size = new System.Drawing.Size(23, 23);
            this.btn_Open.TabIndex = 0;
            this.btn_Open.UseVisualStyleBackColor = true;
            this.btn_Open.Click += new System.EventHandler(this.btn_Open_Click);
            // 
            // trackBar_Size
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.trackBar_Size, 2);
            this.trackBar_Size.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trackBar_Size.Location = new System.Drawing.Point(867, 61);
            this.trackBar_Size.Maximum = 200;
            this.trackBar_Size.Minimum = 10;
            this.trackBar_Size.Name = "trackBar_Size";
            this.trackBar_Size.Size = new System.Drawing.Size(138, 17);
            this.trackBar_Size.TabIndex = 13;
            this.trackBar_Size.Value = 100;
            this.trackBar_Size.Scroll += new System.EventHandler(this.trackBar_Size_Scroll);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(810, 58);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 23);
            this.label1.TabIndex = 14;
            this.label1.Text = "Размер";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btn_Undo
            // 
            this.btn_Undo.Image = ((System.Drawing.Image)(resources.GetObject("btn_Undo.Image")));
            this.btn_Undo.Location = new System.Drawing.Point(3, 32);
            this.btn_Undo.Name = "btn_Undo";
            this.btn_Undo.Size = new System.Drawing.Size(23, 23);
            this.btn_Undo.TabIndex = 3;
            this.btn_Undo.UseVisualStyleBackColor = true;
            this.btn_Undo.Click += new System.EventHandler(this.btn_Undo_Click);
            // 
            // btn_Redo
            // 
            this.btn_Redo.Image = ((System.Drawing.Image)(resources.GetObject("btn_Redo.Image")));
            this.btn_Redo.Location = new System.Drawing.Point(32, 32);
            this.btn_Redo.Name = "btn_Redo";
            this.btn_Redo.Size = new System.Drawing.Size(23, 23);
            this.btn_Redo.TabIndex = 4;
            this.btn_Redo.UseVisualStyleBackColor = true;
            this.btn_Redo.Click += new System.EventHandler(this.btn_Redo_Click);
            // 
            // btn_MakeFoto
            // 
            this.btn_MakeFoto.Image = ((System.Drawing.Image)(resources.GetObject("btn_MakeFoto.Image")));
            this.btn_MakeFoto.Location = new System.Drawing.Point(32, 3);
            this.btn_MakeFoto.Name = "btn_MakeFoto";
            this.btn_MakeFoto.Size = new System.Drawing.Size(23, 23);
            this.btn_MakeFoto.TabIndex = 1;
            this.btn_MakeFoto.UseVisualStyleBackColor = true;
            this.btn_MakeFoto.Click += new System.EventHandler(this.btn_MakeFoto_Click);
            // 
            // btn_Save
            // 
            this.btn_Save.Image = ((System.Drawing.Image)(resources.GetObject("btn_Save.Image")));
            this.btn_Save.Location = new System.Drawing.Point(61, 3);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.Size = new System.Drawing.Size(23, 23);
            this.btn_Save.TabIndex = 2;
            this.btn_Save.UseVisualStyleBackColor = true;
            this.btn_Save.Click += new System.EventHandler(this.btn_Save_Click);
            // 
            // btn_Print
            // 
            this.btn_Print.Image = ((System.Drawing.Image)(resources.GetObject("btn_Print.Image")));
            this.btn_Print.Location = new System.Drawing.Point(90, 3);
            this.btn_Print.Name = "btn_Print";
            this.btn_Print.Size = new System.Drawing.Size(23, 23);
            this.btn_Print.TabIndex = 3;
            this.btn_Print.UseVisualStyleBackColor = true;
            this.btn_Print.Click += new System.EventHandler(this.btn_Print_Click);
            // 
            // trackBar_Rotate
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.trackBar_Rotate, 2);
            this.trackBar_Rotate.Location = new System.Drawing.Point(867, 113);
            this.trackBar_Rotate.Maximum = 90;
            this.trackBar_Rotate.Minimum = -90;
            this.trackBar_Rotate.Name = "trackBar_Rotate";
            this.trackBar_Rotate.Size = new System.Drawing.Size(138, 17);
            this.trackBar_Rotate.TabIndex = 15;
            this.trackBar_Rotate.Visible = false;
            this.trackBar_Rotate.Scroll += new System.EventHandler(this.trackBar_Rotate_Scroll);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(810, 110);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 23);
            this.label2.TabIndex = 18;
            this.label2.Text = "Поворот";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label2.Visible = false;
            // 
            // panel_ImageBox
            // 
            this.panel_ImageBox.AutoScroll = true;
            this.tableLayoutPanel1.SetColumnSpan(this.panel_ImageBox, 6);
            this.panel_ImageBox.Controls.Add(this.pictureBox1);
            this.panel_ImageBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_ImageBox.Location = new System.Drawing.Point(3, 61);
            this.panel_ImageBox.Name = "panel_ImageBox";
            this.tableLayoutPanel1.SetRowSpan(this.panel_ImageBox, 5);
            this.panel_ImageBox.Size = new System.Drawing.Size(801, 617);
            this.panel_ImageBox.TabIndex = 19;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = null;
            this.pictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(798, 503);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox1_Paint);
            this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseMove);
            this.pictureBox1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseUp);
            // 
            // btn_Cut
            // 
            this.btn_Cut.Image = ((System.Drawing.Image)(resources.GetObject("btn_Cut.Image")));
            this.btn_Cut.Location = new System.Drawing.Point(90, 32);
            this.btn_Cut.Name = "btn_Cut";
            this.btn_Cut.Size = new System.Drawing.Size(23, 23);
            this.btn_Cut.TabIndex = 20;
            this.btn_Cut.UseVisualStyleBackColor = true;
            this.btn_Cut.Click += new System.EventHandler(this.button1_Click);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button1.Location = new System.Drawing.Point(867, 657);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(76, 21);
            this.button1.TabIndex = 21;
            this.button1.Text = "Сохранить";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // tbWidth
            // 
            this.tbWidth.Location = new System.Drawing.Point(867, 32);
            this.tbWidth.Name = "tbWidth";
            this.tbWidth.Size = new System.Drawing.Size(76, 20);
            this.tbWidth.TabIndex = 23;
            this.tbWidth.Text = "100";
            this.tbWidth.TextChanged += new System.EventHandler(this.tbWidth_TextChanged);
            this.tbWidth.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_KeyPress);
            this.tbWidth.Leave += new System.EventHandler(this.tbWidth_Leave);
            // 
            // tbHeight
            // 
            this.tbHeight.Location = new System.Drawing.Point(867, 3);
            this.tbHeight.Name = "tbHeight";
            this.tbHeight.Size = new System.Drawing.Size(76, 20);
            this.tbHeight.TabIndex = 22;
            this.tbHeight.Text = "100";
            this.tbHeight.TextChanged += new System.EventHandler(this.tbHeight_TextChanged);
            this.tbHeight.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_KeyPress);
            this.tbHeight.Leave += new System.EventHandler(this.tbHeight_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(810, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 29);
            this.label3.TabIndex = 24;
            this.label3.Text = "Высота";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Location = new System.Drawing.Point(810, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 29);
            this.label4.TabIndex = 25;
            this.label4.Text = "Ширина";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(949, 32);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(52, 23);
            this.button2.TabIndex = 26;
            this.button2.Text = "Готово";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // printDialog1
            // 
            this.printDialog1.UseEXDialog = true;
            // 
            // fPhotoEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 681);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "fPhotoEdit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "fFoto";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.fPhotoEdit_FormClosed);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar_Size)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar_Rotate)).EndInit();
            this.panel_ImageBox.ResumeLayout(false);
            this.panel_ImageBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button btn_Print;
        private System.Windows.Forms.Button btn_Save;
        private System.Windows.Forms.Button btn_MakeFoto;
        private System.Windows.Forms.Button btn_Open;
        private System.Windows.Forms.Button btn_Redo;
        private System.Windows.Forms.Button btn_Undo;
        private System.Windows.Forms.TrackBar trackBar_Size;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TrackBar trackBar_Rotate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_Cut;
        private System.Windows.Forms.PrintDialog printDialog1;
        private System.Windows.Forms.Panel panel_ImageBox;
        private AForge.Controls.PictureBox pictureBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox tbWidth;
        private System.Windows.Forms.TextBox tbHeight;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button2;
    }
}