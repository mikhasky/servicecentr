﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MKP.Controls.Spravochnik
{
   public class RenewalSimpleData
    {
        public RenewalSimpleData(int id,string Name)
        {
            this.id = id;
            this.Name = Name;
        }
        public int id;
        public string Name;
        public override string ToString()
        {
            return Name;
        }
    }
}
