﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MKP.Controls.SpravQuestion.Spravochnik
{
   public class Allert
    {
        public static bool Question(string question)
        {
            if (MessageBox.Show(question, "Подтверждение", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) != DialogResult.OK)
                return false;
            return true;
        }
        public static bool QuestionYesNo(string question)
        {
            if (MessageBox.Show(question, "Подтверждение", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                return false;
            return true;
        }
    }
}
