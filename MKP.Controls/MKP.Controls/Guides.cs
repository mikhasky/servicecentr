﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqToDB;

namespace MKP.Controls
{
    public class Guides
    {
        private static object _instance = new object();
        private static Guides instance;
        private int _groupCount = -1;
        private int _ks1 = -1;
        private Guides()
        {

        }
        public static Guides Initialize()
        {
            if (instance == null)
                lock (_instance)
                    if (instance == null)
                        instance = new Guides();
            return instance;
        }
        private int _acadYearForlistGRPForSelectedYear;
        
        public List<Handbook> LoadAllGroup(int year)
        {
            if (!(listGRPForSelectedYear != null && _acadYearForlistGRPForSelectedYear == year && listGRPForSelectedYear.Count > 0))
            {
                _acadYearForlistGRPForSelectedYear = year;
                listGRPForSelectedYear = GetGroups(academicYear: (short)year);
            }
            return listGRPForSelectedYear;
        }

        private List<Handbook> listGRPForSelectedYear;

        private List<Handbook> listGRP;
        private List<Handbook> listDepartments;
        private int acadYearForGR;
        DataModels.BDETALONDB _dB => new DataModels.BDETALONDB(config.conf.ConnBD);
        public DateTime GetCurrentDate()
        {
            return MKP.DBImplementation.WorkWhithDB.GetData<DateTime>("select current_timestamp from rdb$database", config.conf.ConnBD).Single();
        }
        public List<DataModels.SG> GetSG(int gr1 = -1)
        {
             using (var db = _dB)
                return (from sg in db.SG
                        join gr in db.GR.Where(i => gr1 <= -1 || i.GR1 == gr1) on sg.SG1 equals gr.GR2
                        select sg).ToList();
        }
        public List<Handbook> GetDepartments(int ks1)
        {
            if (_ks1 == ks1 && listDepartments.Count > 0)
            {
                return listDepartments;
            }
            _ks1 = ks1;
            using (var db = _dB)
            {
                listDepartments = db.F.Where(i => i.F1 > 0 && i.F12 != 0 && i.F17 == 0 && (i.F19 == null || i.F19 >= DateTime.Now) && i.F14 == ks1).OrderBy(i=>i.F15).ThenBy(i=>i.F2)
                    .Select(i => new Handbook { ID = i.F1, Text = i.F2 }).ToList();
                return listDepartments;
            }
        }

        public string GetDepatmentName(int Id,bool Short)
        {
            using (var db = _dB)
                return Short
                    ? db.F.Where(i => i.F1 == Id).Select(i => i.F2).Single()
                    : db.F.Where(i => i.F1 == Id).Select(i => i.F3).Single();
        }

        public string GetKafedra(int id,bool Short)
        {
            using (var db = _dB)
                if (Short)
                    return db.K.Where(i => i.K1 == id).Select(i => i.K2).SingleOrDefault() ?? "";
                else
                    return db.K.Where(i => i.K1 == id).Select(i => i.K3).SingleOrDefault() ?? "";
        }

        public string GetSpesilytiKode(int sp1 = -1, int sg1 = -1, int gr1 = -1)
        {
            using (var db = _dB)
            {
                if (sp1 > -1 && sg1 > -1 && gr1 > -1)
                    return (from pnsp in db.PNSP
                            join sp in db.SP on pnsp.PNSP1 equals sp.SP11
                            join sg in db.SG on sp.SP1 equals sg.SG2
                            join gr in db.GR on sg.SG1 equals gr.GR2
                            where gr.GR1 == gr1 && sp.SP1 == sp1 && sg.SG1 == sg1
                            select sp.SP4).FirstOrDefault() ?? "";
                if (sp1 > -1)
                    return (from pnsp in db.PNSP
                            join sp in db.SP on pnsp.PNSP1 equals sp.SP11
                            where sp.SP1 == sp1
                            select sp.SP4).FirstOrDefault() ?? "";
                if (sg1 > -1)
                    return (from pnsp in db.PNSP
                            join sp in db.SP on pnsp.PNSP1 equals sp.SP11
                            join sg in db.SG on sp.SP1 equals sg.SG2
                            where sg.SG1 == sg1
                            select sp.SP4).FirstOrDefault() ?? "";
                if (gr1 > -1)
                    return (from pnsp in db.PNSP
                            join sp in db.SP on pnsp.PNSP1 equals sp.SP11
                            join sg in db.SG on sp.SP1 equals sg.SG2
                            join gr in db.GR on sg.SG1 equals gr.GR2
                            where gr.GR1 == gr1
                            select sp.SP4).FirstOrDefault() ?? "";
            }
            return "";
        }

        internal DataModels.SEM GetSem(int _gr1, int academicYear, int academicSemestr)
        {
            using (var db = _dB)
            {
                return (from sem in db.SEM
                        join gr in db.GR on sem.SEM2 equals gr.GR2
                        where gr.GR1 == _gr1 && sem.SEM3 == academicYear && sem.SEM5 == academicSemestr
                        select sem).FirstOrDefault() ?? new DataModels.SEM(); 
            }
        }

        public string GetSpesilytiName(int sp1=-1, int sg1=-1, int gr1=-1)
        {
            using (var db = _dB)
            {
                if (sp1 > -1 && sg1 > -1 && gr1 > -1)
                    return (from pnsp in db.PNSP
                               join sp in db.SP on pnsp.PNSP1 equals sp.SP11
                               join sg in db.SG on sp.SP1 equals sg.SG2
                               join gr in db.GR on sg.SG1 equals gr.GR2
                               where gr.GR1 == gr1 && sp.SP1 == sp1 && sg.SG1 == sg1
                               select pnsp.PNSP2).FirstOrDefault() ?? "";
                if (sp1>-1)
                    return (from pnsp in db.PNSP
                               join sp in db.SP on pnsp.PNSP1 equals sp.SP11
                               where sp.SP1 == sp1
                            select pnsp.PNSP2).FirstOrDefault() ?? "";
                if(sg1>-1)
                        return  (from pnsp in db.PNSP
                                 join sp in db.SP on pnsp.PNSP1 equals sp.SP11
                                 join sg in db.SG on sp.SP1 equals sg.SG2
                                 where sg.SG1 == sg1
                                 select pnsp.PNSP2).FirstOrDefault() ?? "";
                if (gr1 > -1)
                    return (from pnsp in db.PNSP
                            join sp in db.SP on pnsp.PNSP1 equals sp.SP11
                            join sg in db.SG on sp.SP1 equals sg.SG2
                            join gr in db.GR on sg.SG1 equals gr.GR2
                            where gr.GR1 == gr1
                            select pnsp.PNSP2).FirstOrDefault() ?? "";
            }
            return "";
        }
        public string GetEducationalLevel(int sp1 = -1, int sg1 = -1, int gr1 = -1)
        {
            using (var db = _dB)
            {
                if (sp1 > -1 && sg1 > -1 && gr1 > -1)
                    return (from pnsp in db.PNSP
                            join sp in db.SP on pnsp.PNSP1 equals sp.SP11
                            join sg in db.SG on sp.SP1 equals sg.SG2
                            join gr in db.GR on sg.SG1 equals gr.GR2
                            join ku in db.KU on sp.SP14 equals ku.KU1
                            where gr.GR1 == gr1 && sp.SP1 == sp1 && sg.SG1 == sg1
                            select ku.KU2).FirstOrDefault() ?? "";
                if (sp1 > -1)
                    return (from pnsp in db.PNSP
                            join sp in db.SP on pnsp.PNSP1 equals sp.SP11
                            join ku in db.KU on sp.SP14 equals ku.KU1
                            where sp.SP1 == sp1
                            select ku.KU2).FirstOrDefault() ?? "";
                if (sg1 > -1)
                    return (from pnsp in db.PNSP
                            join sp in db.SP on pnsp.PNSP1 equals sp.SP11
                            join sg in db.SG on sp.SP1 equals sg.SG2
                            join ku in db.KU on sp.SP14 equals ku.KU1
                            where sg.SG1 == sg1
                            select ku.KU2).FirstOrDefault() ?? "";
                if (gr1 > -1)
                    return (from pnsp in db.PNSP
                            join sp in db.SP on pnsp.PNSP1 equals sp.SP11
                            join sg in db.SG on sp.SP1 equals sg.SG2
                            join gr in db.GR on sg.SG1 equals gr.GR2
                            join ku in db.KU on sp.SP14 equals ku.KU1
                            where gr.GR1 == gr1
                            select ku.KU2).FirstOrDefault() ?? "";
            }
            return "";
        }

        public object GetSpecialization(int gr1)
        {
            using (var db = _dB)
            {
                return (from gr in db.GR
                        join spc in db.SPC on gr.GR8 equals spc.SPC1
                        where gr.GR1 == gr1
                        select spc.SPC4).FirstOrDefault() ?? "";
            }
        }

        public DataModels.SEM GetSem(int id)
        {
            using (var db = _dB)
                return db.SEM.SingleOrDefault(i => i.SEM1 == id);
        }

        public int GetEducationForm(int gr1 = -1,int sg1 = -1)
        {
            using (var db = _dB)
            {
                if (gr1 > -1 && sg1 > -1) 
                    return (from sg in db.SG
                        join gr in db.GR on sg.SG1 equals gr.GR2
                        where sg.SG1 == sg1 && gr.GR1 == gr1
                        select sg.SG4).FirstOrDefault();
                if (gr1 > -1)
                    return (from sg in db.SG
                            join gr in db.GR on sg.SG1 equals gr.GR2
                            where gr.GR1 == gr1
                            select sg.SG4).FirstOrDefault();
                if (sg1 > -1)
                    return (from sg in db.SG
                            join gr in db.GR on sg.SG1 equals gr.GR2
                            where sg.SG1 == sg1
                            select sg.SG4).FirstOrDefault();
            }
            return -1;
        }

        public List<Handbook> GetYearOfEntranceCampaign()
        {
             using (var db = _dB)
            {
                var rez = db.SG.Select(i => i.SG3).Where(i=>i!=(short)0).GroupBy(i => i).Select(i => new Handbook() { ID = (int)i.Key, Text = i.Key.ToString() }).ToList();
                return rez;
            }
        }
        public List<DataModels.D> GetDisciplins()
        {
             using (var db = _dB)
            {
                return db.D.ToList();
            }
        }
        public List<DataModels.K> GetKafedra()
        {
             using (var db = _dB)
            {
                return db.K.ToList();
            }
        }
        public List<Handbook> GetGroups(short academicYear, int SG1 = -1, int SP1 = -1, int F1 = -1,int SEM4 = -1,int ST1=-1,short yearOfEntanceCampain = -1)
        {
            if (listGRP != null && listGRP.Count== _groupCount && acadYearForGR == academicYear && SG1 == -1 && SP1 ==-1 && F1 == -1 && SEM4 == -1 && ST1 == -1)
            {
                return listGRP;
            }
            acadYearForGR = academicYear;
            using (var db = _dB)
            {
                IQueryable<DataModels.GR> GR = GetGroups(SG1, SP1, F1, SEM4, ST1, yearOfEntanceCampain, db).OrderBy(i => i.GR7);
                var grp = (from gr in GR
                           let sem = db.SEM.Where(i => i.SEM2 == gr.GR2 && i.SEM3 == academicYear).DefaultIfEmpty().FirstOrDefault().SEM4
                           orderby gr.GR7
                           select new
                           {
                               sem,
                               gr.GR1,
                               gr.GR19,
                               gr.GR20,
                               gr.GR21,
                               gr.GR22,
                               gr.GR23,
                               gr.GR24,
                               gr.GR28,
                               gr.GR3,
                               gr.GR7
                           }).GroupBy(i => i).Select(i => i.Key);
                var listTMP = new[] { new { id = 1, text = "", sort = 1 } }.ToList();
                listTMP.Clear();
                foreach (var item in grp)
                {
                    var val = item.sem == 1 ? item.GR19 : item.sem == 2 ? item.GR20 : item.sem == 3 ? item.GR21 : item.sem == 4 ? item.GR22 : item.sem == 5 ? item.GR23 : item.sem == 6 ? item.GR24 : item.GR28;
                    var grName = MKP.Controls.Extension.StringExtension.IsNullOrWhiteSpace(val) ? item.GR3 : val;
                    listTMP.Add(new { id = item.GR1, text = grName, sort = (int)item.GR7 });
                }
                if (_groupCount == -1)
                    _groupCount = db.GR.Count();

                if (listGRP!=null && listTMP.Count< listGRP.Count)
                    return listTMP.OrderBy(i => i.sort).ThenBy(i => i.text).Select(i => new Handbook { ID = i.id, Text = i.text }).ToList();

                listGRP = listTMP.OrderBy(i => i.sort).ThenBy(i => i.text).Select(i => new Handbook { ID = i.id, Text = i.text }).ToList();
              
                return listGRP;
            }
        }
        public int GetKodVuz()
        {
             using (var db = _dB)
                return db.B.Single(i => i.B1 == 0).B15;
        }
        public List<DataModels.GR> GetGroups(int GR1 = -1,int SG1 = -1, int SP1 = -1, int F1 = -1,int SEM4 = -1 ,int ST1 = -1, short yearOfEntanceCampain = -1)
        {
             using (var db = _dB)
            {
                IQueryable<DataModels.GR> GR = GetGroups(SG1, SP1, F1, SEM4, ST1, yearOfEntanceCampain, db).OrderBy(i=>i.GR7);
                return GR1 > -1 ? GR.Where(i => i.GR1 == GR1).GroupBy(i=>i).Select(i=>i.Key).ToList() : GR.GroupBy(i => i).Select(i => i.Key).ToList();
            }
        }
        private static IQueryable<DataModels.GR> GetGroups(int SG1, int SP1, int F1,int SEM4, int ST1, short yearOfEntanceCampain, DataModels.BDETALONDB db)
        {
            var GR = SG1 == -1 ? db.GR : db.GR.Where(i => i.GR2 == SG1);
            var SG = yearOfEntanceCampain == -1 ? db.SG : db.SG.Where(i => i.SG3 == yearOfEntanceCampain);
            if (SP1 > -1)
            {
                SG = SP1 == -1 ? SG : SG.Where(i => i.SG2 == SP1);
                GR = (from gr in GR
                      join sg in SG on gr.GR2 equals sg.SG1
                      select gr);
            }
            if (F1 > -1)
                GR = (from gr in GR
                      join sg in SG on gr.GR2 equals sg.SG1
                      join sp in db.SP.Where(i => i.SP5 == F1) on sg.SG2 equals sp.SP1
                      select gr);
            if (SEM4 > -1)
            {
                GR = (from gr in GR
                      join sg in SG on gr.GR2 equals sg.SG1
                      join sem in db.SEM.Where(i => i.SEM4 == SEM4) on sg.SG1 equals sem.SEM2
                      select gr);
            }
            if (ST1 > -1)
                GR = (from gr in GR
                      join std in db.STD.Where(i => i.STD2 == ST1 && (i.STD7 == null || i.STD7 >= config.conf.Date) && i.STD11 != 1 && i.STD4 <= config.conf.Date) on gr.GR1 equals std.STD3
                      select gr);
          
            return GR;
        }

        public List<Handbook> GetSemestr(int gr1)
        {
             using (var db = _dB)
                return (from sem in db.SEM
                        join gr in db.GR.Where(i => i.GR1 == gr1) on sem.SEM2 equals gr.GR2
                        let acadYears = $"{sem.SEM3}/{sem.SEM3 + 1} {(sem.SEM5 == 0 ? "осен." : "весе.")}, {sem.SEM4} курс, {sem.SEM7} семестр"
                        orderby new { sem.SEM4, sem.SEM5 }
                        select new Handbook
                        {
                            ID = sem.SEM1,
                            Text = acadYears
                        }).ToList();
        }
        public List<short> GetCourseForDepartment(int f1)
        {
            using (var db = _dB)
            {
                return (from sem in db.SEM
                        join sg in db.SG on sem.SEM2 equals sg.SG1
                        join sp in db.SP on sg.SG2 equals sp.SP1
                        where sp.SP5 == f1
                        select sem.SEM4).GroupBy(i => i).Select(i => i.Key).OrderBy(i => i).ToList();
            }
        }
        public List<Handbook> GetMacroDepartmInfo(int fID)
        {
            using (var db = _dB)
            {
                var rez = new List<Handbook>();
                foreach (var item in db.DMF.Where(i => i.DMF4 == fID).OrderBy(i => i.DMF2).ThenBy(i => i.DMF3))
                {
                    rez.Add(new Handbook { ID = item.DMF2, Text = item.DMF3 });
                }
                return rez;
            }
              //  return db.DMF.Where(i => i.DMF4 == fID).OrderBy(i => i.DMF2).ThenBy(i => i.DMF3).Select(i => new Handbook { ID = i.DMF2, Text = i.DMF3 }).ToList();
        }
               

        public List<Handbook> GetMacroByzInfo()
        {
            using (var db = _dB)
                return db.DM.OrderBy(i => i.DM2).ThenBy(i => i.DM3).Select(i => new Handbook { ID = i.DM2, Text = i.DM3 }).ToList();
        }
        public int GetEmploymentType(int US4, int US6)
        {
            switch (US4)
            {
                case 5:
                    return 5;
                case 6:
                    if (US6 == 1 || US6 == 0)
                        return 6;
                    else if (US6 == 2)
                        return 7;
                    else
                        throw new NotImplementedException("US6 = " + US6);
                case 8:
                    return 8;
                case 20:
                    if (US6 == 1 || US6 == 0)
                        return 9;
                    else if (US6 == 2)
                        return 10;
                    else if (US6 == 3)
                        return 11;
                    else
                        throw new NotImplementedException("US6 = " + US6);
                case 21:
                    return 12;
                case 0:
                default:
                    return -1;                 
            }
        }
    }
}
