﻿using System;
using System.Windows.Forms;
using System.Diagnostics;

namespace MKP
{
    public partial class ButtonExport : UserControl
    {
        public ButtonExport()
        {
            InitializeComponent();
        }

        private void btnExport_MouseHover(object sender, EventArgs e)
        {
            CreateToolTip(btnExport, "Экспорт в эксель");
        }
        private static void CreateToolTip(Control controlForToolTip, string toolTipText)
        {
            ToolTip toolTip = new ToolTip { Active = true };
            toolTip.SetToolTip(controlForToolTip, toolTipText);
            toolTip.IsBalloon = true;
        }
        DevExpress.XtraPivotGrid.PivotGridControl PivGrid;

        public void Initialize(DevExpress.XtraPivotGrid.PivotGridControl PivotGrid)
        {
            PivGrid = PivotGrid; 
        }
        private void btnExport_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();            
            saveFileDialog1.Filter = "Microsoft Excel 2007-2010|*.xlsx";
            saveFileDialog1.Title = "Сохранить в файл";
            saveFileDialog1.ShowDialog();
            if (saveFileDialog1.FileName != "")
            {
             
                PivGrid.ExportToXlsx(saveFileDialog1.FileName,DevExpress.XtraPrinting.TextExportMode.Text);
                if (MessageBox.Show("Открыть сохраненный файл?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    Process.Start(saveFileDialog1.FileName);
            }
        }
    }
}
