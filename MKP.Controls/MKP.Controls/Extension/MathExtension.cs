﻿using System;

namespace MKP.Controls.Extension.Math
{
    public static class MathExtension
    {
        public static decimal Round(this decimal value)
        {
            return System.Math.Round(value, MidpointRounding.AwayFromZero);
        }
        public static decimal Round(this decimal value,int digits)
        {
            return System.Math.Round(value, digits, MidpointRounding.AwayFromZero);
        }
        public static double Round(this double value)
        {

            return System.Math.Round(value, MidpointRounding.AwayFromZero);
        }
        public static double Round(this double value, int digits)
        {
            return System.Math.Round(value, digits, MidpointRounding.AwayFromZero);
        }
    }
}
