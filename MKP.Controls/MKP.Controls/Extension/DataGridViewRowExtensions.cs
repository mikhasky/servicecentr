﻿using System.Collections.Generic;
using System.Linq;

namespace System.Windows.Forms
{
    public static class DataGridViewRowExtensions
    {
        public static int[] GetRowsIndex(this DataGridViewRowCollection dataGridViewSelectedRowCollection)
        {
            return dataGridViewSelectedRowCollection.Cast<DataGridViewRow>().GetRowsIndex();
        }
        public static int[] GetRowsIndex(this DataGridViewSelectedRowCollection dataGridViewSelectedRowCollection)
        {
            return dataGridViewSelectedRowCollection.Cast<DataGridViewRow>().GetRowsIndex();
        }
        public static int[] GetRowsIndex(this IEnumerable<DataGridViewRow> dataGridViewSelectedRowCollection)
        {
            return dataGridViewSelectedRowCollection.Select(i => i.Index).ToArray();
        }
    }
}
