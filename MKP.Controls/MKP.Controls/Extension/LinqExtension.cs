﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MKP.Controls.Extension
{
    public class GroupResult<T>
    {
        public object Key { get; set; }
        public string stringKey => Key.ToString();
        public int Count { get; set; }
        public List<T> Items { get; set; }
        public List<GroupResult<T>> SubGroups { get; set; }

    }
    public static class Ext
    {
        public static IEnumerable<GroupResult<T>> GroupByMany<T>(this IEnumerable<T> elements, params Func<T, object>[] groupSelectors)
        {
            if (groupSelectors.Length > 0)
            {
                var selector = groupSelectors.First();
                var nextSelectors = groupSelectors.Skip(1).ToArray();
                return elements.GroupBy(selector).Select(
                    g => new GroupResult<T>
                    {
                        Key = g.Key,
                        Count = g.Count(),
                        Items = g.ToList(),
                        SubGroups = nextSelectors.Length > 0 ? g.GroupByMany(nextSelectors).ToList() : null
                    });
            }
            return null;
        }
    }
}
