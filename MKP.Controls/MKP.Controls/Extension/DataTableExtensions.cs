﻿using System.Data;

namespace MKP.Controls.Extension
{
    public static class DataTableExtensions
    {
        public static string[,] ToStringArray(this DataTable dt)
        {
            var array = new string[dt.Rows.Count + 1, dt.Columns.Count];
            for (var l = 0; l <= dt.Rows.Count; l++)
            {
                int j = 0;
                foreach (DataColumn col in dt.Columns)
                {
                    if (l == 0)
                        array[l, j] = col.ColumnName;
                    else
                        array[l, j] = dt.Rows[l - 1][col.ColumnName]?.ToString() ?? "";
                    j++;
                }
            }
            return array;
        }
    }
}
