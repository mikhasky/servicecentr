﻿using System.Data;
using System.Linq;
using System.Windows.Forms;
namespace MKP.Controls.Extension
{
    public static class DataGridViewExtension
    {
        public static DataTable ToDataTable(this DataGridView dg, bool onlyVisibleColumn)
        {
            var dt = new DataTable();
            dt.Columns.AddRange(
                dg.Columns.Cast<DataGridViewColumn>()
                    .Where(i => onlyVisibleColumn ? i.Visible : true)
                    .Select(i => new DataColumn(i.HeaderText + "_" + i.Index, typeof(object)))
                    .ToArray());
            foreach (DataGridViewRow row in dg.Rows)
            {
                var dr = dt.NewRow();
                foreach (DataColumn col in dt.Columns)
                {
                    var index =
                        dg.Columns.Cast<DataGridViewColumn>()
                            .Single(i => (i.HeaderText + "_" + i.Index) == col.ColumnName)
                            .Index;
                    dr.SetField(col, row.Cells[index].Value ?? "");
                }
                dt.Rows.Add(dr);
            }
            return dt;
        }
        public static string[,] ToStringArrayOnlyCheck(this DataGridView dg,int checkColumn, bool onlyVisibleColumn)
        {
            var columnCount = onlyVisibleColumn ? dg.Columns.Cast<DataGridViewColumn>().Where(i => i.Visible).Count() : dg.ColumnCount;
            var rows = dg.Rows.Cast<DataGridViewRow>().Where(i => i.Cells[checkColumn].Value.ToString() == "True").ToList();         
            var array = new string[rows.Count + 1, columnCount];

            for (var l = 0; l <= rows.Count; l++)
            {
                int j = 0;
                foreach (DataGridViewColumn col in dg.Columns)
                {
                    if (onlyVisibleColumn && !col.Visible)
                        continue;
                    if (l == 0)
                    {
                        array[l, j] = col.HeaderText;
                    }
                    else
                        array[l, j] = rows[l - 1].Cells[col.Name].Value?.ToString() ?? "";
                    j++;
                }
            }
            return array;
        }
        public static string[,] ToStringArray(this DataGridView dg, bool onlyVisibleColumn,bool RemoveHeaderSplitter=false)
        {
            var columnCount = onlyVisibleColumn ? dg.Columns.Cast<DataGridViewColumn>().Where(i => i.Visible).Count() : dg.ColumnCount;

            var array = new string[dg.RowCount + 1, columnCount];
           
            string separator = string.Empty;
            if (RemoveHeaderSplitter && dg is MKP.Controls.Helper.GridView)
            {
                array = new string[dg.RowCount + (dg as MKP.Controls.Helper.GridView).iNoOfLevels, columnCount];
                separator = (dg as MKP.Controls.Helper.GridView).ColumnHeaderSpliter;
            }



            for (var l = 0; l <= dg.Rows.Count; l++)
            {
                int j = 0;
                foreach (DataGridViewColumn col in dg.Columns)
                {
                    if (onlyVisibleColumn && !col.Visible)
                        continue;
                    if (l == 0)
                    {
                        array[l, j] = col.HeaderText;
                        if (!separator.IsNullOrWhiteSpace())
                        {
                            var p = array[l, j].IndexOf(separator);
                            if (p > -1)
                                array[l, j] = array[l, j].Remove(0, p + 1);
                        }
                    }
                    else
                        array[l, j] = dg.Rows[l - 1].Cells[col.Name].Value?.ToString() ?? "";
                    j++;
                }
            }
            return array;
        }
        public static object[,] ToObjectArray(this DataGridView dg, bool onlyVisibleColumn, bool RemoveHeaderSplitter = false)
        {
            var columnCount = onlyVisibleColumn ? dg.Columns.Cast<DataGridViewColumn>().Where(i => i.Visible).Count() : dg.ColumnCount;
            var array = new object[dg.RowCount + 1, columnCount];
            string separator = string.Empty;
            if (RemoveHeaderSplitter && dg is MKP.Controls.Helper.GridView)
                separator = (dg as MKP.Controls.Helper.GridView).ColumnHeaderSpliter;
            for (var l = 0; l <= dg.Rows.Count; l++)
            {
                int j = 0;
                foreach (DataGridViewColumn col in dg.Columns)
                {
                    if (onlyVisibleColumn && !col.Visible)
                        continue;
                    if (l == 0)
                    {
                        array[l, j] = col.HeaderText;
                        if (!separator.IsNullOrWhiteSpace())
                        {
                            var p = array[l, j].ToString().IndexOf(separator);
                            if (p > -1)
                                array[l, j] = array[l, j].ToString().Remove(0, p + 1);
                        }
                    }
                    else
                        array[l, j] = dg.Rows[l - 1].Cells[col.Name].Value;
                    j++;
                }
            }
            return array;
        }
    }
}
