﻿using DevExpress.XtraPivotGrid;
using System.Data;
using System.Reflection;

namespace MKP.Controls.Extension
{
    public static class PivotGridExtension
    {
        private static bool dataRefresh = false;

        public static DataTable MakeDataTable(this PivotGridControl pivotGrid, string tableName)
        {
            dataRefresh = true;
            pivotGrid.RefreshData();
            dataRefresh = false;          
            PivotSummaryDataSource ds = pivotGrid.CreateSummaryDataSource();
            PropertyInfo pi = typeof(PivotSummaryDataSource).GetProperty("PropertyDescriptors", BindingFlags.Instance | BindingFlags.NonPublic);
            PivotSummaryPropertyDescriptorCollection pdc = pi.GetValue(ds, null) as PivotSummaryPropertyDescriptorCollection;
            DataTable dt = new DataTable(tableName);
            if (ds.RowCount == 0)
                return dt;

            System.Globalization.NumberFormatInfo numberFormatInfo = System.Globalization.CultureInfo.CurrentCulture.NumberFormat;
            for (int c = 0; c < pdc.Count; c++)
            {
                if (pdc[c].Field.Visible)
                {
                    dt.Columns.Add(pdc[c].Name);
                    dt.Columns[dt.Columns.Count - 1].Caption = pdc[c].DisplayName;
                    dt.Columns[dt.Columns.Count - 1].Prefix = pdc[c].Field.Area.ToString();
                    if (pdc[c].Field.DataType.Name != "String")
                    {
                        dt.Columns[dt.Columns.Count - 1].DataType = ds.GetValue(0, pdc[c].Name).ToString().Contains(numberFormatInfo.NumberDecimalSeparator) ? (typeof(double)) : (typeof(int));
                    }
                    else
                        dt.Columns[dt.Columns.Count - 1].DataType = pdc[c].Field.DataType;
                }
            }

            for (int r = 0; r < ds.RowCount; r++)
            {
                object[] v = new object[dt.Columns.Count];
                for (int c = 0; c < dt.Columns.Count; c++)
                    v[c] = ds.GetValue(r, dt.Columns[c].ColumnName);
                dt.Rows.Add(v);
            }
            return dt;
        }
    }
}
