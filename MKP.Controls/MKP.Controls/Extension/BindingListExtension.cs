﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace MKP.Controls.Extension
{
    public static class BindingListExtension
    {
        public static void AddRange<T>(this BindingList<T> sourse, IEnumerable<T> collection)
        {
            InsertRange(sourse, sourse.Count, collection);
        }
        private static void InsertRange<T>(BindingList<T> sourse, int index, IEnumerable<T> collection)
        {
            MKP.Ecxeption.Throw<ArgumentNullException>.If(collection == null);
            using (IEnumerator<T> enumerator = collection.GetEnumerator())
            {
                while (enumerator.MoveNext())
                {
                    sourse.Insert(index++, enumerator.Current);
                }
            }
        }
    }
}
