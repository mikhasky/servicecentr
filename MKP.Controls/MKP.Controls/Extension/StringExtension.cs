﻿using System;
using System.IO;

namespace MKP.Controls.Extension
{
    public static class StringExtension
    {
        public static bool IsNullOrWhiteSpace(this string value)
        {
            return string.IsNullOrWhiteSpace(value);
        }
        public static byte[] GetBytes(this string value)
        {
            byte[] bytes = null;
            using (MemoryStream fs = new MemoryStream())
            {
                using (TextWriter tx = new StreamWriter(fs))
                {
                    tx.WriteLine(value);
                    tx.Flush();
                    fs.Flush();
                    bytes = fs.ToArray();
                }
            }
            return bytes;
        }
        public static string GetString(this byte[] arr)
        {
            if (arr == null)
                throw new NullReferenceException("arr");

            using (var memoryStream = new MemoryStream())
            {
                memoryStream.Write(arr, 0, arr.Length);
                memoryStream.Seek(0, SeekOrigin.Begin);
                memoryStream.Position = 0;
                var sr = new StreamReader(memoryStream);
                return sr.ReadToEnd();
            }
        }
    }
}
