﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MKP.Controls.TabDbFixes
{
    public partial class AllFixes : Form
    {
        public AllFixes()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            new DisciplineFixes().ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            new ShablonFixes().ShowDialog();
        }
    }
}
