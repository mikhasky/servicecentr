﻿using LinqToDB;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MKP.Controls.TabDbFixes
{
    public partial class ShablonFixes : Form
    {    
        public ShablonFixes()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            new Task(() => Execute()).Start();
            button1.Enabled = false;          
        }

        private void Execute()
        {
            RichTextBox converter = new RichTextBox();
            using (var db = new DataModels.BDETALONDB(Configurations.instance.ConnBD))
            {
                var lst = db.VPRV.Where(i => i.VPRV1 != 0).ToList();
                int count = lst.Count; bool next = true;
                int progress = 0;
                for (int j = 0; j < count; j++)
                {
                    next = true;
                    if ((j * 100 / count) != progress)
                    {
                        progress = j * 100 / count;
                        progressBar1.Invoke(new MethodInvoker(() => progressBar1.Value = progress));
                    }
                    try
                    {
                        db.BeginTransaction();
                        string VPRV11 = lst[j].VPRV11;
                        if (lst[j].VPRV11!=null)
                        if (!lst[j].VPRV11.ToLower().Contains("rtf"))
                        {
                            try {
                                converter.Text = VPRV11;
                                VPRV11 = converter.Rtf;
                                next = false;
                            } catch { }
                        }
                       
                            string VPRV12 = lst[j].VPRV12;
                        if (lst[j].VPRV12 != null)
                            if (!lst[j].VPRV12.ToLower().Contains("rtf"))
                        {
                            try
                            {
                                converter.Text = VPRV12;
                                VPRV12 = converter.Rtf;
                                next = false;
                            }
                            catch { }
                        }
                       /* string VPRV13 = lst[j].VPRV13;
                        if (!lst[j].VPRV13.ToLower().Contains("rtf"))
                        {
                            try
                            {
                                converter.Text = VPRV13;
                                VPRV13 = converter.Rtf;
                                next = false;
                            }
                            catch { }
                        }*/
                        if (!next)
                        db.VPRV.Where(i => i.VPRV1 == lst[j].VPRV1).Set(i=>i.VPRV11,VPRV11).Set(i => i.VPRV12, VPRV12).Update();//.Set(i => i.VPRV13, VPRV13)
                        db.CommitTransaction();
                    }
                    catch (Exception)
                    {
                        db.RollbackTransaction();
                        throw;
                    }
                }
                var _stprk = db.STPRK.Where(i => i.STPRK1 != 0).ToList();
                count = _stprk.Count;
                progress = 0;
                for (int j = 0; j < count; j++)
                {
                    next = true;
                    if ((j * 100 / count) != progress)
                    {
                        progress = j * 100 / count;
                        progressBar1.Invoke(new MethodInvoker(() => progressBar1.Value = progress));
                    }
                    try
                    {
                        db.BeginTransaction();
                        string stprk2 = _stprk[j].STPRK2;
                        if (!_stprk[j].STPRK2.ToLower().Contains("rtf"))
                        {
                            try
                            {
                                converter.Text = stprk2;
                                stprk2 = converter.Rtf;
                                next = false;
                            }
                            catch { }
                        }                        
                        if (!next)
                        db.STPRK.Where(i => i.STPRK1 == _stprk[j].STPRK1).Set(i => i.STPRK2, stprk2).Update();
                        db.CommitTransaction();
                    }
                    catch (Exception)
                    {
                        db.RollbackTransaction();
                        throw;
                    }
                }
                var _osps = db.OSPS.Where(i => i.OSPS1 != 0).ToList();
                count = _osps.Count;
                progress = 0;
                for (int j = 0; j < count; j++)
                {
                    next = true;
                    if ((j * 100 / count) != progress)
                    {
                        progress = j * 100 / count;
                        progressBar1.Invoke(new MethodInvoker(() => progressBar1.Value = progress));
                    }
                    try
                    {
                        db.BeginTransaction();
                        string _osps5 = _osps[j].OSPS5;
                        if (!_osps[j].OSPS5.ToLower().Contains("rtf"))
                        {
                            try
                            {
                                converter.Text = _osps5;
                                _osps5 = converter.Rtf;
                                next = false;
                            }
                            catch { }
                        }
                        if (!next)
                            db.OSPS.Where(i => i.OSPS1 == _osps[j].OSPS1).Set(i => i.OSPS5, _osps5).Update();
                        db.CommitTransaction();
                    }
                    catch (Exception)
                    {
                        db.RollbackTransaction();
                        throw;
                    }
                }
                MessageBox.Show("Выполнено!");
            }
        }
    }
}
