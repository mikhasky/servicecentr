﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading.Tasks;
using System.Threading;
using LinqToDB;

namespace MKP.Controls.TabDbFixes
{
    public partial class DisciplineFixes : Form
    {
        public DisciplineFixes()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            new Task(()=>Execute()).Start();
            button1.Enabled = false;
            checkBox1.Enabled = false;        
        }

        private void Execute()
        {
            using (var db = new DataModels.BDETALONDB(Configurations.instance.ConnBD))
            {
                var lst = db.D.Where(i=>i.D1!=0).Select(i => new FxDiscipline(i.D1,i.D2,i.D3,i.D4,i.D27,i.D28)).ToList();
                int count = lst.Count;
                int progress = 0;
                for (int j = 0; j < count; j++)
                {
                    if ((j * 100 / count) != progress)
                    {
                        progress = j * 100 / count;
                        progressBar1.Invoke(new MethodInvoker(() => progressBar1.Value = progress));
                    }
                    try
                    {
                        db.BeginTransaction();
                        db.D.Where(i => i.D1 == lst[j].d1).Set(i => i.D2, lst[j].d2).Set(i => i.D3, lst[j].d3).Set(i => i.D4, lst[j].d4).Set(i => i.D27, lst[j].d27).Set(i => i.D28, lst[j].d28).Update();
                        db.CommitTransaction();
                    }
                    catch (Exception)
                    {
                        db.RollbackTransaction();
                        throw;
                    }
                }
                MessageBox.Show("Выполнено!");              
            }
        }
        public class FxDiscipline
        {
            public int d1;
            public string d2, d3, d4, d27,d28;
            public FxDiscipline(int d1, string d2, string d3, string d4, string d27,string d28)
            {
                this.d1 = d1;
                this.d2=d2.Replace(Environment.NewLine, string.Empty);
                this.d3= d3.Replace(Environment.NewLine, string.Empty);
                this.d4 = d4.Replace(Environment.NewLine, string.Empty);
                this.d27 = d27.Replace(Environment.NewLine, string.Empty);
                this.d28 = d28.Replace(Environment.NewLine, string.Empty);
            }
        }
    }
}
