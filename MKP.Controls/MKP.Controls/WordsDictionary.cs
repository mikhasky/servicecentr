﻿using System;
using System.Collections.Generic;

namespace MKP.Controls
{
    /// <summary>
    /// Словарь для перевода оценок, форм контроля и т.д.
    /// </summary>
    public class WordsDictionary
    {
        private static WordsDictionary instance;
        /// <summary>
        ///  0 - русский, 1-украинский
        /// </summary>
        public int Language { get; set; } = 1;
        WordsDictionary(int language)
        {
            Language = language;
        }
        /// <summary>
        /// Инициализаци 
        /// </summary>
        /// <param name="language"> 0 - русский, 1-украинский</param>
        /// <returns></returns>
        public static WordsDictionary Initialize(int language)
        {
            var obj = new object();
            if (instance == null)
            {
                lock (obj)
                    if (instance == null)
                        instance = new WordsDictionary(language);
                    else
                        instance.Language = language;
            }
            else
            {
                instance.Language = language;
            }
            return instance;
        }
        public static WordsDictionary Instance()
        {
            if (instance == null)
                throw new TypeInitializationException("WordsDictionary", new Exception("WordsDictionary not initialized"));
            return instance;
        }
        ///* 1-зав.каф 2-проф. 3-доцент 4-ст.преп. 5-ассист. */
        public string[] Dolgnosti
        {
            get
            {
                if (Language == 0)
                    return new string[]
                        {
                            "","зав.каф","проф.","доцент","ст.преп.","ассист."
                        };
                else
                    return new string[]
                             {
                                 "","зав.каф","проф.","доцент","ст.преп.","ассист."
                             };
            }
        }
        /// <summary>
        /// список семестров 0-осенний, 1-весенний
        /// </summary>
        public string[] Semestr
        {
            get
            {
                if (Language == 0)
                    return new string[]
                    {
                    "осенний","весенний"
                    };
                return new string[]
                {
                "осінній", "весняний"
                };
            }
        }
        public string[] SemestrShort
        {
            get
            {
                if (Language == 0)
                    return new string[]
                    {
                    "осен.","весн."
                    };
                return new string[]
                {
                "осін.", "весн."
                };
            }
        }
        /// <summary>
        /// форма обучения 0 - дневная 1 - заочная 2 - вечерняя 3 - экстернат 4 - второе высшее
        /// </summary>      
        public string[] EducationsFom
        {
            get
            {
                if (Language == 0)
                    return new string[] { "дневная", "заочная", "вечерняя", "экстернат", "второе высшее" };
                return new string[] { "Денна", "заочна", "вечірня", "екстернат", "друга вища" };
            }
        }
        public string[] EducationsFomEnglish
        {
            get
            {
                return new string[] { "full-time", "Extramural studies", "Evening classes", "Externship", "Second degree" };
            }
        }
        /// <summary>
        ///  Оценка словами
        /// </summary>
        public Dictionary<string, string> MarksToWords
        {
            get
            {
                if (Language == 0)
                    return new Dictionary<string, string>()
                    {
                        [""] = "0",
                        ["0"] = "0",
                        ["1"] = "1",
                        ["2"] = "Неудовлетворительно",
                        ["3"] = "Удовлетворительно",
                        ["4"] = "Хорошо",
                        ["5"] = "Отлично",
                        ["не зачёт"] = "Незачет",
                        ["зачёт"] = "Зачёт",
                        ["-1"] = "Зачёт"
                    };
                return new Dictionary<string, string>()
                {
                    [""] = "0",
                    ["0"] = "0",
                    ["1"] = "1",
                    ["2"] = "Незадовільно",
                    ["3"] = "Задовільно",
                    ["4"] = "Добре",
                    ["5"] = "Відмінно",
                    ["не зачёт"] = "Не зараховано",
                    ["зачёт"] = "Зараховано",
                    ["-1"] = "Зараховано",
                    ["зарах."] = "Зараховано",
                };
            }
        }

        public Dictionary<string, string> MarksToWordsEng
        {
            get
            {
                return new Dictionary<string, string>()
                {
                    { "0", "" },
                    { "1", "1" },
                    { "2", "Fail" },
                    { "3", "Satisfactory" },
                    { "4", "Good" },
                    { "5", "Excellent" },
                    { "не зачёт", "Fail" },
                    { "-1",  "Passed" },
                    { "зарах.",  "Passed" },
                    { "зачёт", "Passed" },
                    { "-2", "" },
                    { "-3", "" },
                    { "-4", "" }
                };

            }
        }
        /// <summary>
        /// Типы контроля  0 - НФАУ  5 - экз. 6 - зач. 8 - Курсов. 7 - ДифЗач
        /// </summary>
        public Dictionary<int, string> EmploymentType
        {
            get
            {
                if (Language == 0)
                    return new Dictionary<int, string>()
                    {
                        [0] = "",
                        [5] = "Экзамен",
                        [6] = "Зачёт",
                        [8] = "Курсовая работа",
                        [7] = "дифференцированный зачет",
                        [9] = "Практика",
                        [10] = "Практика",
                        [11] = "Практика",
                        [12] = "Гос. экзамен"
                    };
                // Ирпень
                if (config.conf.KodVuz == 24)
                    return new Dictionary<int, string>
                    {
                        [0] = "",
                        [5] = "Екзамен",
                        [6] = "Залік",
                        [8] = "Курсова робота",
                        [7] = "Диференційований залік",
                        [9] = "Виробнича практика",
                        [10] = "Виробнича практика",
                        [11] = "Виробнича практика",
                        [12] = "Держ. екзамен"
                    };
                else
                    return new Dictionary<int, string>
                    {
                        [0] = "",
                        [5] = "Іспит",
                        [6] = "Залік",
                        [8] = "Курсова робота",
                        [7] = "Диференційований залік",
                        [9] = "Практика",
                        [10] = "Практика",
                        [11] = "Практика",
                        [12] = "Держ. іспит"
                    };
            }
        }
        public Dictionary<int, string> D8DiscTypes
        {
            get
            {
                if (Language == 0)
                    return new Dictionary<int, string>()
                    {
                        [0] = "",
                        [2] = "Гос. iспит",
                        [6] = "Практика",
                        [7] = "Дипломиривание"
                    };
                    return new Dictionary<int, string>
                    {
                        [0] = "",
                        [2] = "Держ.екзамен",
                        [6] = "Практика",
                        [7] = "Дипломування"
                    };
            }
        }
        public Dictionary<int, string> EmploymentsTypeShort
        {
            get
            {
                if (Language == 0)
                    return new Dictionary<int, string>()
                    {
                        [0] = "",
                        [5] = "Экз",
                        [6] = "Зач",
                        [7] = "ДифЗач",
                        [8] = "Курс",
                        [9] = "Практ",
                        [10] = "Практ",
                        [11] = "Практ",
                        [12] = "Гос"
                    };

                // Ирпень
                if (config.conf.KodVuz == 24)
                    return new Dictionary<int, string>
                    {
                        [0] = "",
                        [5] = "Екз",
                        [6] = "Зал",
                        [8] = "К.Р.",
                        [7] = "ДифЗал",
                        [9] = "В.П.",
                        [10] = "В.П.",
                        [11] = "В.П.",
                        [12] = "Держ. Ек"
                    };
                else
                    return new Dictionary<int, string>
                    {
                        [0] = "",
                        [5] = "Ісп",
                        [6] = "Зал",
                        [8] = "Курс",
                        [7] = "ДифЗал",
                        [9] = "Практ",
                        [10] = "Практ",
                        [11] = "Практ",
                        [12] = "Держ"
                    };
            }
        }

        /// <summary>
        /// перевод оценок в STUSV
        /// </summary>
        public Dictionary<int, string> MarksForSTUSVST
        {
            get
            {
                if (Language == 0)
                    return new Dictionary<int, string>
                    {
                        [-4] = "Ув.",
                        [-3] = "НД",
                        [-2] = "Н/Я",
                        [-1] = "зачёт",
                        [0] = "",
                        [1] = "",
                        [2] = "2",
                        [3] = "3",
                        [4] = "4",
                        [5] = "5",
                    };
                return new Dictionary<int, string>
                {
                    [-4] = "Пов.",
                    [-3] = "НД",
                    [-2] = "Н/Я",
                    [-1] = "зарах.",
                    [0] = "",
                    [1] = "",
                    [2] = "2",
                    [3] = "3",
                    [4] = "4",
                    [5] = "5",
                };
            }
        }
        /// <summary>
        /// статусы студенда
        /// </summary>
        public Dictionary<int, string> STD11
        {
            get
            {
                if (Language == 0)
                    return new Dictionary<int, string> {
                        { 0,"Учится"},
                        { 1,"Свободный"},
                        { 2,"Отчислен"},
                        { 3,"В академ.отпуске"},
                        { 4,"Закончил ВУЗ"},
                        { 5,"Не зачислен"},
                        { 6,"Допуск для ликвидации разницы в учебных планах"},
                        { 7,"Повторное обучение"},
                        { 8,"Зачислен"}
                    };
                return new Dictionary<int, string>
                {
                    { 0, "Вчиться"},
                    { 1, "Вільний"},
                    { 2, "Відрахований"},
                    { 3, "В академ.отпуске"},
                    { 4, "Закінчив ВНЗ"},
                    { 5, "Не зарахований"},
                    { 6, "Допуск для ліквідації різниці в навчальних планах"},
                    { 7, "Повторне навчання"},
                    { 8, "Зарахований"}
                };
            }
        }

        public Dictionary<int, string> MonthDictionary
        {
            get
            {
                if (Language == 0)
                    return new Dictionary<int, string>
                    {
                        [1] = "январь",
                        [2] = "февраль",
                        [3] = "март",
                        [4] = "апрель",
                        [5] = "май",
                        [6] = "июнь",
                        [7] = "июль",
                        [8] = "август",
                        [9] = "сентябрь",
                        [10] = "октябрь",
                        [11] = "ноябрь",
                        [12] = "декабрь"
                    };
                return new Dictionary<int, string>
                {
                    [1] = "січень",
                    [2] = "лютий",
                    [3] = "березень",
                    [4] = "квітень",
                    [5] = "травень",
                    [6] = "червень",
                    [7] = "липень",
                    [8] = "серпень",
                    [9] = "вересень",
                    [10] = "жовтень",
                    [11] = "листопад",
                    [12] = "грудень"
                };
            }
        }
        public Dictionary<int, string> MonthDictionaryWhom
        {
            get
            {
                if (Language == 0)
                    return new Dictionary<int, string>
                    {
                        [1] = "января",
                        [2] = "февраля",
                        [3] = "марта",
                        [4] = "апреля",
                        [5] = "мая",
                        [6] = "июня",
                        [7] = "июля",
                        [8] = "августа",
                        [9] = "сентября",
                        [10] = "октября",
                        [11] = "ноября",
                        [12] = "декабря"
                    };
                return new Dictionary<int, string>
                {
                    [1] = "січня",
                    [2] = "лютого",
                    [3] = "березня",
                    [4] = "квітня",
                    [5] = "травня",
                    [6] = "червня",
                    [7] = "липня",
                    [8] = "серпня",
                    [9] = "вересня",
                    [10] = "жовтня",
                    [11] = "листопада",
                    [12] = "грудня"
                };
            }
        }

        public Dictionary<int, string> MonthDictionaryOnEnglish
        {
            get
            {
                return new Dictionary<int, string>
                {
                    [1] = "January",
                    [2] = "February",
                    [3] = "March",
                    [4] = "April",
                    [5] = "May",
                    [6] = "June",
                    [7] = "July",
                    [8] = "August",
                    [9] = "September",
                    [10] = "October",
                    [11] = "November",
                    [12] = "December"
                };
            }
        }
        public Dictionary<int, string> Fond
        {
            get
            {
                return new Dictionary<int, string>
                {
                    [0] = "Бюджет",
                    [1] = "Спец.фонд"
                };
            }
        }
        public Dictionary<int, string> FondStud
        {
            get
            {
                return new Dictionary<int, string>
                {
                    [0] = "Бюджет",
                    [1] = "Контракт"
                };
            }
        }
        public Dictionary<int, string> QualLevel
        {
            get
            {
                if (Language == 0)
                    return new Dictionary<int, string>
                    {
                        [0] = "мл.спец.",
                        [1] = "бакал.",
                        [2] = "специал.",
                        [3] = "магистр"
                    };
                return new Dictionary<int, string>
                {
                    [0] = "мл.спец.",
                    [1] = "бакал.",
                    [2] = "спеціал.",
                    [3] = "магістр"
                };
            }
        }
        public Dictionary<int, string> QualLevelFull
        {
            get
            {
                if (Language == 0)
                    return new Dictionary<int, string>
                    {
                        [0] = "младший специалист",
                        [1] = "бакалавр",
                        [2] = "специалист",
                        [3] = "магистр"
                    };
                return new Dictionary<int, string>
                {
                    [0] = "молодший спеціаліст",
                    [1] = "бакалавр",
                    [2] = "спеціаліст",
                    [3] = "магістр"
                };
            }
        }
        public Dictionary<int, string> Inostran
        {
            get
            {
                if (Language == 0)
                    return new Dictionary<int, string>
                    {
                        [0] = "Не иностранцы",
                        [1] = "Иностранцы"
                    };
                return new Dictionary<int, string>
                {
                    [0] = "Не іноземці",
                    [1] = "Iноземці"
                };
            }
        }
        public Dictionary<char, string> SK3
        {
            get
            {
                return new Dictionary<char, string>
                {
                    ['\0'] = "Бюджет",
                    ['0'] = "Бюджет",
                    ['1'] = "Контракт"
                };
            }
        }        
        public Dictionary<int, string> STD17
        {
            get
            {
                if (Language == 0)
                    return new Dictionary<int, string>
                    {
                        [0] = "Приказом",
                        [1] = "Редакт."
                    };
                return new Dictionary<int, string>
                {
                    [0] = "Приказом",
                    [1] = "Редаг."
                };
            }
        }
        public Dictionary<int, string> PE50
        {
            get
            {
                if (Language == 0)
                    return new Dictionary<int, string>
                    {
                        [0] = "Нет",
                        [1] = "Приписное свидетельство",
                        [2] = "Военный билет"
                    };
                return new Dictionary<int, string>
                {
                    [0] = "Нет",
                    [1] = "Приписне свідоцтво",
                    [2] = "Военний білет"
                };
            }
        }
        public Dictionary<int, string> DocType
        {
            get
            {
                if (Language == 0)
                    return new Dictionary<int, string>
                    {
                        [0] = "Паспорт",
                        [1] = "Свидетельство о рождении",
                        [2] = "Военный билет",
                        [3] = "Свидетельство на постоянное проживание в стране",
                        [4] = "Временное удостоверение на проживание",
                        [5] = "Паспорт электронный"
                    };
                return new Dictionary<int, string>
                {
                    [0] = "Паспорт",
                    [1] = "Свідотство про народження",
                    [2] = "Военный білет",
                    [3] = "Свідотство на постійне проживання в країні",
                    [4] = "Тимчасове посвідчення на проживання",
                    [5] = "Паспорт електронний"
                };
            }
        }
    }
}