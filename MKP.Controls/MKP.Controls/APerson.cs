﻿using MKP.Controls.Extension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.Runtime.Serialization;
namespace MKP.Controls
{
    public class APerson: AModel
    {
        public APerson() { }
        public APerson(int id,string firstName,string lastName,string patronymicName)
        {
            ID = id;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.PatronymicName = patronymicName;
        }
        [IgnoreDataMember]
        [DisplayName("ИД")]
        public int ID { get; set; }
        private string _FirstName;

        [DisplayName("Фамилия")]
        public string FirstName { get { return _FirstName; } set { _FirstName = value; } }

        private string _LastName;
        [DisplayName("Имя")]
        public string LastName { get { return _LastName; } set { _LastName = value; } }
        private string _PatronymicName;
        [DisplayName("Отчество")]
        public string PatronymicName { get { return _PatronymicName; } set { _PatronymicName = value; } }
        private string _Post = "";
        public string Post { get { return _Post; } set { _Post = value; } }
        public virtual string GetSex(short value, bool Short = false)
        {           
            if (Short)
                return value == 0 ? "М" : "Ж";
            return value == 0 ? "М" : "Ж";
        }

        string GetFioWhom(IEnumerable<DataModels.OK> dict, string value, char type, short sex)
        {
            if (string.IsNullOrWhiteSpace(value))
                return "";
            var filtr = dict.Where(i => i.OK9 == type && value.EndsWith(i.OK2, StringComparison.Ordinal)).FirstOrDefault();
            if (filtr != null)
            {
                var okonchanie = sex == 0 ? filtr.OK5 : filtr.OK6;
                return value.Remove(value.Length - filtr.OK2.Length) + okonchanie;
            }
            return value;
        }

        public virtual string GetFio(bool Short = false)
        {
            if (Short)
                return $"{(string.IsNullOrWhiteSpace(Post)? "" : Post + " ")}{_FirstName} {GetInitials(_LastName)} {GetInitials(_PatronymicName)}";
            return $"{(string.IsNullOrWhiteSpace(Post) ? "" : Post + " ")}{_FirstName} {_LastName} {_PatronymicName}";
        }
        private string GetInitials(string val)
        {
            return val.IsNullOrWhiteSpace() ? "" : val.Length > 1 ? val.Remove(1) + "." : val.Length == 1 ? val : "";
        }
        public virtual string GetFioInitialsFirst(bool Short = false)
        {
            if (Short)
                return $"{(string.IsNullOrWhiteSpace(Post) ? "" : Post + " ")}{GetInitials(_LastName)} {GetInitials(_PatronymicName)} {_FirstName}";
            return $"{(string.IsNullOrWhiteSpace(Post) ? "" : Post + " ")}{_LastName} {_PatronymicName} {_FirstName}";
        }
        public override string ToString()
        {
            return GetFio(true);
        }
    }
}
