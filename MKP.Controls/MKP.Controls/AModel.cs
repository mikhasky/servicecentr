﻿using System.ComponentModel;
using System.Diagnostics;

namespace MKP.Controls
{

    public abstract class AModel:INotifyPropertyChanged
    {
        protected virtual void OnPropertyChanged(string propertyName = null)
        {
            StackFrame frame = new StackFrame(1);
            var method = frame.GetMethod();
            if(PropertyChanged!=null)
                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        protected virtual void OnPropertyChanged(object sender)
        {
            StackFrame frame = new StackFrame(1);
            var method = frame.GetMethod();
            var propName = method.Name.Substring("set_".Length);
            PropertyChanged?.Invoke(sender, new PropertyChangedEventArgs(propName));
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
