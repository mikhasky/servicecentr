﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MKP.Controls.PrinDevMacroses
{
   public static class GeneralMacroses
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="module">dm4, 0-ОК  1-деканат  2-аспирантура  3-аббитуриент</param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static MKP.PrintDevForms.Macroses GetDmMacroses(int module,string name)
        {
            MKP.PrintDevForms.Macroses macro = new PrintDevForms.Macroses(PrintDevForms.MacroType.ArrayString) {macro=name };
            using (var db = new DataModels.BDETALONDB(Configurations.instance.ConnBD))
            { 
                macro.result = db.DM.Where(i => i.DM4 == module && i.DM5 == 0 && i.DM6 == Configurations.instance.KS1).Select(i => new {i.DM2, i.DM3 }).ToArray();
            }
            return macro;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="module">dmp5, 0-ОК  1-деканат  2-аспирантура  3-аббитуриент</param>
        /// <param name="tipe">dmp6,0-по полу  1-по оплате учится 2-по обучению учится 3-по уровню подготовки</param>
        /// <returns></returns>
        public static MKP.PrintDevForms.Macroses GetDmpMacroses(int module,int tipe,string name)
        {
            MKP.PrintDevForms.Macroses macro = new PrintDevForms.Macroses(PrintDevForms.MacroType.ArrayString) { macro = name };
            using (var db = new DataModels.BDETALONDB(Configurations.instance.ConnBD))
            {
                macro.result = db.DMP.Where(i => i.DMP5 == module && i.DMP6 == tipe).Select(i => new { i.DMP2, i.DMP3}).ToArray();
            }
            return macro;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="module">dmp5, 0-ОК  1-деканат  2-аспирантура  3-аббитуриент</param>        
        /// <param name="f1">ид факультета</param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static MKP.PrintDevForms.Macroses GetDmindMacroses(int module, int f1, string name)
        {
            MKP.PrintDevForms.Macroses macro = new PrintDevForms.Macroses(PrintDevForms.MacroType.ArrayString) { macro = name };
            using (var db = new DataModels.BDETALONDB(Configurations.instance.ConnBD))
            {
                var data = (from dm in db.DM.Where(i => i.DM4 == module && i.DM5 == 1 && i.DM6 == Configurations.instance.KS1 && i.DM1 == 0)
                                 join dmind in db.DMIND.Where(i=>i.DMIND4==f1) on dm.DM1 equals dmind.DMIND2
                                 //join f in db.F on dmind.DMIND4 equals f.F1
                                // join sp in db.SP.OrderBy(i => i.SP8) on dmind.DMIND6 equals sp.SP1
                                 select new { dm.DM2,dmind.DMIND3 }).ToArray();
                macro.result = data;
            }
            return macro;
        }
    }
}
