﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MKP.Controls.Helper;
using MKP.DBImplementation;
using System.Threading.Tasks;

namespace MKP.Controls.Controls.EDBOMigration
{
    public partial class EDBOMigrationControl : UserControl
    {        
        private bool FileLoaded = false;
        private bool isLoaded = false;
        private FilteredBindingList<MigrateOptions> source;
        public EDBOMigrationControl()
        {
            InitializeComponent();
            source = new FilteredBindingList<MigrateOptions>();
            dgOptions.DataSource = source;
        }
        public Dictionary<string, string> Assoc = new Dictionary<string, string>()
        {
            {"LastName","st2" },
            {"FirstName","st3" },
            {"MiddleName","st4" },
            {"SexId","st6" },//1 man, 2 woman
            {"Birthday","st7" },
            {"PaymentTypeId","st7" },//1 бюджет, 2 контракт
            {"PersonDocumentTypeId","st7" },//3 passport, 17 Посвідка на постійне проживання в Україні, 36 Паспорт громадянина України з безконтактним електронним носієм
            {"PersonDocumentSeries","st7" },
            {"PersonDocumentNumber","st7" },
            {"DocumentTypeId","st7" },//6 Диплом бакалавра державного зразка
            {"DocumentTypeName","st7" },
            {"DocumentSeries","st7" },
            {"DocumentNumber","st7" },
            {"INN","st7" },
            {"LastNameEn","st7" },
            {"FirstNameEn","st7" },
            {"SpecialityName","st7" },
            {"SpecialityNameEn","st7" },
            {"FacultyName","st7" },
            {"EducationFormId","st7" },//2 заочна
        };
        /// <summary>
        /// First Load
        /// </summary>
        public void Init()
        {
            if (isLoaded)
                return;
            source.Clear();
            var lst = Assoc.Select(i =>new MigrateOptions(i.Key)).ToList();
            source.AddRange(lst);
            isLoaded = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }
    }
}
