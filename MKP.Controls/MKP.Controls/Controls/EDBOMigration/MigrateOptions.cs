﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MKP.Controls.Controls.EDBOMigration
{
   internal class MigrateOptions
    {
        public MigrateOptions(string Column)
        {
            this.Column = Column;
        }
        public bool Checked { get; set; }
        public string Column { get; set; }       
    }    
}
