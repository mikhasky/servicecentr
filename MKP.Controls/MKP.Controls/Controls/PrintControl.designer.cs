﻿namespace MKP.Controls.Controls
{
    partial class PrintControl
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonPrint_print = new System.Windows.Forms.Button();
            this.comboBoxPrint = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // buttonPrint_print
            // 
            this.buttonPrint_print.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonPrint_print.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.buttonPrint_print.Location = new System.Drawing.Point(189, 0);
            this.buttonPrint_print.Name = "buttonPrint_print";
            this.buttonPrint_print.Size = new System.Drawing.Size(75, 21);
            this.buttonPrint_print.TabIndex = 3;
            this.buttonPrint_print.Text = "Печать";
            this.buttonPrint_print.UseVisualStyleBackColor = true;
            this.buttonPrint_print.Click += new System.EventHandler(this.buttonPrint_Click);
            // 
            // comboBoxPrint
            // 
            this.comboBoxPrint.Dock = System.Windows.Forms.DockStyle.Left;
            this.comboBoxPrint.FormattingEnabled = true;
            this.comboBoxPrint.Location = new System.Drawing.Point(0, 0);
            this.comboBoxPrint.Name = "comboBoxPrint";
            this.comboBoxPrint.Size = new System.Drawing.Size(189, 21);
            this.comboBoxPrint.TabIndex = 2;
            this.comboBoxPrint.SelectedIndexChanged += new System.EventHandler(this.comboBoxPrint_SelectedIndexChanged);
            this.comboBoxPrint.KeyUp += new System.Windows.Forms.KeyEventHandler(this.comboBoxPrint_KeyUp);
            // 
            // PrintControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.buttonPrint_print);
            this.Controls.Add(this.comboBoxPrint);
            this.Name = "PrintControl";
            this.Size = new System.Drawing.Size(265, 21);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonPrint_print;
        private System.Windows.Forms.ComboBox comboBoxPrint;
    }
}
