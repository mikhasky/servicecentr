﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using MKP.Controls.Extension;
using MKP.DBImplementation.TablesBDD;
using MKP.PrintDevForms;
using MKP.Printing;
using MKP.PrintDevForms.Forms;

namespace MKP.Controls.Controls
{
    public partial class PrintControl : UserControl
    {
        #region Fields
        public bool forcedRecognition { get; set; }=false;        
        public DynControl child { get; set; }
        private bool StopForcedRecognition = false;
        #endregion

        #region Delegates
        public delegate void GetMarcosEventHandler();

        public delegate void GetDevMarcosEventHandler();       
        #endregion

        #region Events

        public event GetMarcosEventHandler GetMacros;

        public event GetDevMarcosEventHandler GetDevMacros;

        public event RecognizedDelegate RecognizeFinished;
        #endregion

        /*сами печетніе формі*/
        private BindingList<FORMI> _formi = new BindingList<FORMI>();
      
        /// <summary>
        ///  Для добавления макросов по F11-F12
        ///  </summary>
        public object MacroDevList;
        /// <summary>
        /// Для добавления макросов по F9
        /// </summary>
        public List<SpecialMacro> MacroList;
      
        private DisplayStyle _displayStyle = DisplayStyle.CbLeftBtnLeft;
        private bool Loaded;
        private int? selectedId;
        private string ConnectionStringBD;

        public DisplayStyle _DisplayStyle
        {
            get { return _displayStyle; }
            set { _displayStyle = value; SetStyle(); }
        }

        /// <summary>
        /// Строка подключения к БДД
        /// </summary>
        public string ConnectionString { get; set; }
        /// <summary>
        /// номер модуля
        /// </summary>
        public int IdModule { get; set; } = -1;
        /// <summary>
        /// Номер вкладки
        /// </summary>
        public int IdTab { get; set; } = -1;
        /// <summary>
        /// ИД пользователя
        /// </summary>
        public int IdUser { get; set; } = -1;

        private bool isCanEdit;

        public PrintControl()
        {
            InitializeComponent();
            comboBoxPrint.DropDownStyle = ComboBoxStyle.DropDown;
            comboBoxPrint.Sorted = false;
            SetStyle();
        }
        
        /// <summary>
        /// Инициализация 
        /// </summary>       
        public void Initialize(string сonnectionStringBDD, string сonnectionStringBD, int idModule, int idTab, int idUser,bool portal = false)
        {
            if (Loaded)
                return;
            ConnectionString = сonnectionStringBDD;
            ConnectionStringBD = сonnectionStringBD;
            IdModule = idModule;
            IdTab = idTab;
            IdUser = idUser;
            RefreshPfList();
            Loaded = true;
            if (!portal)
                using (var db = new DataModels.BDETALONDB(ConnectionStringBD))
                {
                    isCanEdit = Convert.ToBoolean(db.I.Single(i => i.I1 == IdUser).I35);
                }
        }
        private void SetStyle()
        {
            this.Controls.Clear();
            this.Controls.Add(this.buttonPrint_print);
            this.Controls.Add(this.comboBoxPrint);
            switch (_DisplayStyle)
            {
                case DisplayStyle.CbLeftBtnLeft:
                    this.Size = new System.Drawing.Size(265, 21);
                    this.comboBoxPrint.Dock = System.Windows.Forms.DockStyle.Left;
                    this.buttonPrint_print.Dock = System.Windows.Forms.DockStyle.Left;
                    break;
                case DisplayStyle.CbLeftBtnRight:
                    this.Size = new System.Drawing.Size(265, 21);
                    this.comboBoxPrint.Dock = System.Windows.Forms.DockStyle.Left;
                    this.buttonPrint_print.Dock = System.Windows.Forms.DockStyle.Right;
                    break;
                case DisplayStyle.CbFillBtnRight:
                    this.Size = new System.Drawing.Size(265, 21);
                    this.Controls.Add(this.comboBoxPrint);
                    this.Controls.Add(this.buttonPrint_print);this.comboBoxPrint.Dock = System.Windows.Forms.DockStyle.Fill;
                    this.buttonPrint_print.Dock = System.Windows.Forms.DockStyle.Right;
                    break;
                case DisplayStyle.CbUpBtnDown:
                    this.Size = new System.Drawing.Size(265, 42);
                    this.comboBoxPrint.Dock = System.Windows.Forms.DockStyle.Top;
                    this.buttonPrint_print.Dock = System.Windows.Forms.DockStyle.Bottom;
                    break;
                case DisplayStyle.CbDownBtnUp:
                    this.Size = new System.Drawing.Size(265, 42);
                    this.comboBoxPrint.Dock = System.Windows.Forms.DockStyle.Bottom;
                    this.buttonPrint_print.Dock = System.Windows.Forms.DockStyle.Top;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public void AddMacroList(List<Macroses> macroses)
        {
            MKP.Ecxeption.Throw<NullReferenceException>.If(macroses == null, nameof(macroses));
            MacroDevList = macroses;
        }
        public void AddMacroList(List<Macroses[]> macroses)
        {            
            MKP.Ecxeption.Throw<NullReferenceException>.If(macroses == null, nameof(macroses));
            MacroDevList = macroses;
        }
        public void AddMacroList(List<SpecialMacro> macroses)
        {
            MKP.Ecxeption.Throw<NullReferenceException>.If(macroses == null, nameof(macroses));
            MacroList = macroses;
        }
        /*обновления списка форм, записуеться в PfList(свойство set вызывает fillPrintComboBox)*/
        private void RefreshPfList()
        {
            if (IdModule == -1 || ConnectionString.IsNullOrWhiteSpace())
                return;
            _formi.Clear();
            _formi.AddRange(
                DBImplementation.WorkWhithDB.GetDataForQuery<FORMI>(
                    "select formi1, formi7 from formi where formi2 = @m0 and formi4 = @m1 and formi5 = 0 order by formi6",
                    ConnectionString,
                    new KeyValuePair<string, object>("m0", IdModule),
                    new KeyValuePair<string, object>("m1", IdTab)));
            comboBoxPrint.DataSource = _formi;
        }
        private int getCurrentId()
        {
            int? form1 = null;
            if (comboBoxPrint.SelectedIndex != -1)
            {
                form1 = (comboBoxPrint.SelectedItem as FORMI).FORMI1;
            }
            else
            {
                if (selectedId == null)
                {
                    form1 = _formi.First().FORMI1;
                }
                else
                {
                    form1 = selectedId;
                }
            }
            selectedId = form1;
            return form1.Value;
        }
        private void comboBoxPrint_KeyUp(object sender, KeyEventArgs e)
        {
#if !DEBUG
            if (!isCanEdit && (e.KeyCode == Keys.F9 || e.KeyCode == Keys.Insert || e.KeyCode == Keys.F8 || e.KeyCode == Keys.F11 || e.KeyCode == Keys.F12 || e.KeyCode == Keys.Enter))
            {
                MessageBox.Show("Нет доступа к редактору форм. Обратитесь к администратору!");
                return;
            }
#endif
            switch (e.KeyCode)
            {
                case Keys.F9: //F9 - запустить

                    StartRedactor(false,true);
                    break;
                case Keys.Insert:
                    int maxId =
                        MKP.DBImplementation.WorkWhithDB.GetData<int>("select MAX(formi1) + 1 FROM formi",
                            ConnectionString).Single();
                    var newFormi = new DBImplementation.TablesBDD.FORMI
                    {
                        FORMI1 = maxId,
                        FORMI2 = IdModule,
                        FORMI4 = IdTab,
                        FORMI7 = maxId.ToString()
                    };
                    DBImplementation.WorkWhithDB.InsertValue(newFormi, ConnectionString);
                    RefreshPfList();
                    comboBoxPrint.SelectedIndex = _formi.IndexOf(_formi.Single(i => i.FORMI1 == newFormi.FORMI1));
                    break;
                case Keys.F8:
                    if (!(MessageBox.Show("Удалить?", "Предупреждение", MessageBoxButtons.OKCancel,
                              MessageBoxIcon.Stop) == DialogResult.OK))
                        return;
                    DBImplementation.WorkWhithDB.DeleteValue(comboBoxPrint.SelectedItem as FORMI, ConnectionString);
                    RefreshPfList();
                    break;
                case Keys.F11:
                    StartRedactor(false,false);
                    break;
                case Keys.F12:
                    StartRedactor(false, false,EditorType.Spread);
                    break;
                case Keys.Enter:
                    var id = getCurrentId();
                    DBImplementation.WorkWhithDB.ExecutNonQuery("UPDATE formi SET formi7 = @m0 WHERE formi1=@m1", ConnectionString, 
                        new KeyValuePair<string,object>("@m1", id), new KeyValuePair<string, object>("@m0", comboBoxPrint.Text));
                    RefreshPfList();
                    comboBoxPrint.SelectedIndex = _formi.IndexOf(_formi.Single(i => i.FORMI1 == id));
                    break;
            }
        }
        /// <summary>
        /// forcedRecognition
        /// </summary>
        public void StartForceRecognition()
        {
            if (_formi.Count == 0)
                return;
            if (child == null)
                throw new NullReferenceException("forcedRecognition is not have a child");
            Form1.RecognozeFinished -= RecognozeFinished;
            Form1.RecognozeFinished += RecognozeFinished;
            EditorType edt = EditorType.Word;           
            isPrintable(getCurrentId(),ref edt);  
                MacroDevList = new List<Macroses>();
                GetDevMacros?.Invoke();
           using (var editor = new PrintDevForms.Form1(MacroDevList,
                    getCurrentId().ToString(),
                    ConnectionStringBD,
                    ConnectionString,
                   PrintDevForms.Options.ModeStart.Моментальный_результат_после_загрузки,
                    IdUser, string.Empty, edt))
            {
                editor.Close();    
                editor.Dispose();
            }
        }

        private void RecognozeFinished(EditorType type, object editor)
        {
            Form1.RecognozeFinished -= RecognozeFinished;
            child.LoadDocument(editor, type);
        }

        private bool isPrintable(int formi1, ref EditorType edt)
        {
            int rez =MKP.DBImplementation.WorkWhithDB.GetDataForQuery<int>("SELECT formi9 from formi where formi1=@p1",
                ConnectionString, new KeyValuePair<string, object>("@p1", formi1)).SingleOrDefault();
            if (rez == 2)
                edt = EditorType.Spread;
            return rez == 0;
        }

        private void buttonPrint_Click(object sender, EventArgs e)
        {
            if (_formi.Count == 0 || comboBoxPrint.SelectedItem == null || selectedId == null)
            {
                MessageBox.Show("Список печатных форм пуст или не выбрана ни одна печатная форма");
                return;
            }               
            StartRedactor(true);
        }

        private void comboBoxPrint_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedId = (comboBoxPrint.SelectedItem as FORMI)?.FORMI1;
            if (selectedId == null)
                return;
            if (forcedRecognition)
                StartForceRecognition();
        }
        public void StartScanDocument()
        {
            MacroDevList = new List<Macroses>();
            GetDevMacros?.Invoke();
            //try
            {
                PrintDevForms.Form1 NewPDF = new PrintDevForms.Form1(MacroDevList,
                    getCurrentId().ToString(),
                    ConnectionStringBD,
                    ConnectionString,
                    (PrintDevForms.Options.ModeStart.Вызов_результата_распознавания_без_показа),
                    IdUser, string.Empty, EditorType.Word);
                
                    NewPDF.RecognizeFinished -= RecognizeFinished;
                    NewPDF.RecognizeFinished += RecognizeFinished;
                    NewPDF.StartScan();
                
            }
            MacroDevList = null;
            //catch (Exception e)
            {
                //MessageBox.Show("Ошибка запуска окна! Ошибка: " + e.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void StartRedactor(bool isPrint, bool printedForm = true, EditorType edt = EditorType.Word)
        {
            if (isPrint)
            {
                printedForm = isPrintable(getCurrentId(), ref edt);
            }

            if (printedForm)
            {
                this.MacroList = new List<SpecialMacro>();
                try
                {
                    GetMacros?.Invoke();
                    FMain printingForm = new FMain(getCurrentId().ToString(), MacroList, ConnectionString,
                        ConnectionStringBD,
                        comboBoxPrint.Text + (!isPrint ? string.Empty : "visible=false"), IdUser.ToString());
                    printingForm.Show();
                }
                catch (SyntaxErrorException err)
                {
                    MessageBox.Show(err.ToString());
                }
                catch (Exception err)
                {
                    MessageBox.Show(err.Message);
                }
            }
            else
            {
                MacroDevList = new List<Macroses>();
                GetDevMacros?.Invoke();
                //try
                {
                    using (PrintDevForms.Form1 NewPDF = new PrintDevForms.Form1(MacroDevList,
                        getCurrentId().ToString(),
                        ConnectionStringBD,
                        ConnectionString,
                        (!isPrint ? PrintDevForms.Options.ModeStart.Стандартный_режим :
                                    PrintDevForms.Options.ModeStart.Вызвать_сразу_предпросмотр),
                        IdUser, string.Empty, edt))
                    {
                        if (!isPrint)
                        {
                            if (forcedRecognition)
                            {
                                Form1.CurrentEditorClose += CurrentEditorClose;
                            }
                            NewPDF.ShowDialog();                           
                            NewPDF.Dispose();
                        }
                    }
                }
                MacroDevList = null;
                //catch (Exception e)
                {
                    //MessageBox.Show("Ошибка запуска окна! Ошибка: " + e.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void CurrentEditorClose()
        {
            if (StopForcedRecognition)
            {
                StopForcedRecognition = !StopForcedRecognition;
                return;
            }
            StopForcedRecognition = !StopForcedRecognition;
            if (forcedRecognition)
                StartForceRecognition();
        }

        public enum DisplayStyle
        {
            CbLeftBtnLeft,
            CbLeftBtnRight,
            CbFillBtnRight,
            CbUpBtnDown,
            CbDownBtnUp
        }
    }
}