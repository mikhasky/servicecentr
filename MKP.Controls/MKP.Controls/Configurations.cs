﻿using System;
using System.Linq;

static class config
{
#if DEBUG
    public static MKP.Controls.Configurations conf = MKP.Controls.Configurations.Init();
#else
    public static MKP.Controls.Configurations conf = MKP.Controls.Configurations.instance;
#endif
}
 
namespace MKP.Controls
{
    public class Configurations
    {
        public delegate void DateValueChangedEventHandler(DateTime date);
        public static event DateValueChangedEventHandler DateChanged;
        static Configurations()
        {

        }
        public PermissionType Permission { get; private set; } = PermissionType.NotAdmin;
        public enum PermissionType
        {
            Admin,
            NotAdmin
        }
        public static Configurations instance { get; private set; }
        public string ConnBD { get; private set; } = string.Empty;
        public string ConnBDD { get; private set; } = string.Empty;
        public int KS1 { get; private set; } = 0;
        public int I1 { get; private set; }
        public int KodVuz { get; private set; }
        public int AcademicYear { get; private set; } = 2016;
        public int AcademicSemestr { get; private set; } = 0;
        public int Language { get; private set; }
        private DateTime _date;
        public DateTime Date
        {
            get { return _date; }
            set
            {
                if (!_date.Date.Equals(value.Date))
                {
                    _date = value;
                    DateChanged?.Invoke(value);
                }

            }
        }
        public bool IsPoratalUser { get; set; } = true;
        private Configurations(string conBD, string conBDD, int ks1, int i1) : this()
        {
            ConnBD = conBD;
            ConnBDD = conBDD;
            KS1 = ks1;

            I1 = i1;
        }

        private Configurations()
        {
            TabPageSettings.SettingControl.SettingChanged += Settings_SettingChanged;
            MKP.Controls.OnDate.DateChenged += OnDate_DateChenged;
        }

        private void OnDate_DateChenged(DateTime date)
        {
            Date = date.Date;
        }

        private void Settings_SettingChanged(TabPageSettings.SettingChangedEventArgs e)
        {
            AcademicYear = e.AcademicYear;
            AcademicSemestr = e.AcademicSemestr;
        }

#if !DEBUG
        public static Configurations Init(string[] parameters)
#else
        public static Configurations Init()
#endif
        {
            if (instance != null)
                return instance;
#if !DEBUG            
            if (string.IsNullOrWhiteSpace(parameters[0])) throw new NullReferenceException("conBD");
            if (string.IsNullOrWhiteSpace(parameters[1])) throw new NullReferenceException("conBDD");
            if (parameters.Length < 5)
                throw new ArgumentException("KS1 not initialized!");
            instance = new Configurations(parameters[0], parameters[1].Replace("UTF8", "WIN1251"), int.Parse(parameters[4]), int.Parse(parameters[2]));
#else
            instance = new Configurations();
            if (Environment.MachineName.ToUpper() == "VALENTIN")
            {
                instance.ConnBD = @"User=MKP;Password=17051945;Database=D:\BD\BD;DataSource=localhost;Port=3050;Dialect=3;Charset=UTF8;Role=;Connectionlifetime=15;Pooling=true;MinPoolSize=0;MaxPoolSize=50;PacketSize=8192;ServerType=0";
                instance.ConnBDD = @"User=MKP;Password=17051945;Database=D:\BD\BDD;DataSource=localhost;Port=3050;Dialect=3;Charset=UTF8;Role=;Connectionlifetime=15;Pooling=true;MinPoolSize=0;MaxPoolSize=50;PacketSize=8192;ServerType=0";
                instance.I1 = 0;
            }
            else if(Environment.MachineName.ToUpper() == "1-ПК" || Environment.MachineName.ToUpper() == "DESKTOP-18JPV7L")
            {                
                instance.ConnBD = @"User=MKP;Password=17051945;Database=E:\dBase\Bogomolec\BD;DataSource = localhost;Port = 3050;Dialect = 3;Charset = UTF8;Role =;Connectionlifetime = 1;Pooling = true;MinPoolSize = 0;MaxPoolSize =50;Packet Size = 8192;ServerType = 0";
                instance.ConnBDD = @"User=MKP;Password=17051945;Database=e:\dBase\BDD;DataSource=localhost;Port=3050;Dialect=3;Charset=UTF8;Role=;Connectionlifetime=15;Pooling=true;MinPoolSize=0;MaxPoolSize=50;PacketSize=8192;ServerType=0";
                instance.I1 = 10;                
            }
            else if(Environment.MachineName.ToUpper() == "DESKTOP-FMBMBJR")
            {
                instance.ConnBD = @"User=MKP;Password=17051945;Database=E:\bd_mkr\bogom\BD;DataSource=127.0.0.1;Port = 3050;Dialect = 3;Charset = UTF8;Role = rdb$admin;Connectionlifetime = 15;Pooling = true;MinPoolSize = 0;MaxPoolSize = 50;Packet Size = 8192;ServerType = 0";
                instance.ConnBDD = @"User=MKP;Password=17051945;Database=E:\bd_mkr\bogom\BDD;DataSource=localhost;Port=3050;Dialect=3;Charset=UTF8;Role=rdb$admin;Connectionlifetime=15;Pooling=true;MinPoolSize=0;MaxPoolSize=50;PacketSize=8192;ServerType=0";
                //instance.IsPoratalUser = true;
                instance.I1 = 10;
            }
#endif

            instance.KodVuz = instance.GetKodVuz();
#if !DEBUG
            if (Convert.ToInt16(parameters[3]) == 0)
#endif
            {
#if DEBUG
                instance.IsPoratalUser = false; 
#else
                instance.IsPoratalUser = false;
#endif
                instance.GetPermission();
                instance.Language = instance.GetLanguage();
            }
#if DEBUG
            //instance.I1 = 746;// 1347;
#endif
           // MKP.Localization.Configuration.SetLanguage(instance.Language);
          //  MKP.Localization.Configuration.SetKodeVuz(instance.KodVuz);
            return instance;
        }

        private int GetKodVuz()
        {
            using (var db = new DataModels.BDETALONDB(ConnBD))
                return db.B.Where(i => i.B1 == 0).Select(i => i.B15).Single();
        }
        private int GetLanguage()
        {
            using (var db = new DataModels.BDETALONDB(ConnBD))
            {
                var lang = int.Parse(db.I.Where(i => i.I1 == I1).Select(i => i.I10).Single().ToString());
                WordsDictionary.Initialize(lang);
                return lang;

            }

        }
        private void GetPermission()
        {
            using (var db = new DataModels.BDETALONDB(ConnBD))
            {
                var perm = (from gpi in db.GPI
                            join gp in db.GP on gpi.GPI2 equals gp.GP1
                            where gpi.GPI1 == I1
                            select gp.GP3).ToList().GroupBy(i => i).Select(i => i.Key).ToList();
                Permission = perm.Contains(1) ? PermissionType.Admin : PermissionType.NotAdmin;
            }
        }
    }
}
