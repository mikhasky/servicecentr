﻿using System;
using System.Windows.Forms;

namespace MKP.Controls
{
    public delegate void DateChangedEventHandler(DateTime date);
    public partial class OnDate : UserControl
    {
        public static event DateChangedEventHandler DateChenged;
        public OnDate()
        {
            InitializeComponent();           
            MKP.Controls.Configurations.DateChanged += Configurations_DateChanged;
        }
        private void Configurations_DateChanged(DateTime date)
        {
            if (checkBox1.Checked)
            {
                if (!dtp_OnDate.Value.Equals(date))
                    config.conf.Date = dtp_OnDate.Value;
                return;
            }

            if (!dtp_OnDate.Value.Equals(date))
                dtp_OnDate.Value = date;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            dtp_OnDate.Value = Guides.Initialize().GetCurrentDate();
        }
        private void dtp_OnDate_ValueChanged(object sender, EventArgs e)
        {
            DateChenged?.Invoke(dtp_OnDate.Value);
        }
    }
}
