﻿namespace MKP.Controls
{
    public class Handbook
    {
       
        public Handbook() { }
        public Handbook(object id, string text)
        {
            this.ID = id;
            this.Text = text;
        }
        public object ID { get; set; }

        public int value { get; set; }
        public string Text { get; set; }
        public override string ToString()
        {
            return Text;
        }
        public static bool IsHanbook(object value)
        {
            return value != null && value.GetType().Equals(typeof(Handbook));
        }
    }
}
