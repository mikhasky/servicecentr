﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MKP.Controls.TabPageSettings
{
    public delegate void SettingChangedEventHandler(SettingChangedEventArgs e);
    public partial class SettingControl : UserControl
    {
        public static event SettingChangedEventHandler SettingChanged;
        static int academicYear;
        internal static int AcademicYear
        {
            set
            {
                if (!academicYear.Equals(value))
                {
                    academicYear = value;
                    SettingChanged?.Invoke(new SettingChangedEventArgs(academicYear, academicSemestr));
                }
            }
        }
        static int academicSemestr;
        private bool Loaded;

        internal static int AcademicSemestr
        {
            set
            {
                if (!academicSemestr.Equals(value))
                {
                    academicSemestr = value;
                    SettingChanged?.Invoke(new SettingChangedEventArgs(academicYear, academicSemestr));
                }
            }
        }
                
        public SettingControl()
        {
            InitializeComponent();
        }
        
        public void Initialize()
        {
            if (Loaded)
                return;
            Loaded = true;
            cb_AcademicYear.SelectedIndexChanged -= cb_AcademicYear_SelectedIndexChanged;
            cb_AcademicYear.DataSource = SettingCTRL.GetAcademicYears();
            AcademicYear = SettingCTRL.GetYearOnUserSetting();
            cb_AcademicYear.SelectedIndex = (cb_AcademicYear.DataSource as List<Handbook>).IndexOf((cb_AcademicYear.DataSource as List<Handbook>).Where(i =>Convert.ToInt32(i.ID) == academicYear).SingleOrDefault());
            cb_AcademicYear.SelectedIndexChanged += cb_AcademicYear_SelectedIndexChanged;
            rb_SemOsen.CheckedChanged -= rb_SemOsen_CheckedChanged;
            AcademicSemestr = SettingCTRL.GetSemestrOnUserSetting();
            rb_SemOsen.Checked = !Convert.ToBoolean(academicSemestr);
            rb_SemVesna.Checked = !rb_SemOsen.Checked;
            rb_SemOsen.CheckedChanged += rb_SemOsen_CheckedChanged;
            bFixes.Visible = config.conf.Permission == Configurations.PermissionType.Admin;
        }
        private void cb_AcademicYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            AcademicYear = Convert.ToInt32((cb_AcademicYear.Items[cb_AcademicYear.SelectedIndex] as Handbook).ID);
            SettingCTRL.SetAcademicYear(Convert.ToInt32((cb_AcademicYear.Items[cb_AcademicYear.SelectedIndex] as Handbook).ID));
        }

        private void rb_SemOsen_CheckedChanged(object sender, EventArgs e)
        {
            AcademicSemestr = rb_SemOsen.Checked ? 0 : 1;
            SettingCTRL.SetAcademicSemestr(academicSemestr);
        }

        private void bFixes_Click(object sender, EventArgs e)
        {
            new TabDbFixes.AllFixes().ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
           
        }
    }
    public class SettingChangedEventArgs : EventArgs
    {
        public SettingChangedEventArgs(int year, int semestr)
        {
            this.AcademicYear = year;
            this.AcademicSemestr = semestr;
        }
        public int AcademicYear { get; set; }
        public int AcademicSemestr { get; set; }
    }
}
