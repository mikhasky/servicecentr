﻿using System.Collections.Generic;
using System.Linq;
using LinqToDB;
namespace MKP.Controls.TabPageSettings
{
    internal static class SettingCTRL
    {        
        internal static List<Handbook> GetAcademicYears()
        {
            using (var db = new DataModels.BDETALONDB(config.conf.ConnBD))            
                return db.SG.Where(i => i.SG3 != 0).Select(i => i.SG3).GroupBy(i => i)
                    .Select(i => i.Key).ToList().OrderByDescending(i=>i)
                    .Select(i => new Handbook { ID = i, Text = $"{i}/{i + 1}" }).ToList();
            
        }
        static IQueryable<DataModels.I> GetCurrentUser(DataModels.BDETALONDB db)
        {
            return db.I.Where(i => i.I1 == config.conf.I1);
        }
        static IQueryable<DataModels.P> GetCurrentPortalUser(DataModels.BDETALONDB db)
        {
            return db.P.Where(i => i.P1 == config.conf.I1);
        }
        internal static short GetYearOnUserSetting()
        {
            using (var db = new DataModels.BDETALONDB(config.conf.ConnBD))
            {
                if(config.conf.IsPoratalUser)
                    return GetCurrentPortalUser(db).Select(i=>i.P123).SingleOrDefault();
                else
                    return GetCurrentUser(db).Select(i => i.I32).SingleOrDefault();
            }
        }
        internal static short GetSemestrOnUserSetting()
        {
            using (var db = new DataModels.BDETALONDB(config.conf.ConnBD))
            {
                if (config.conf.IsPoratalUser)
                    return GetCurrentPortalUser(db).Select(i => i.P122).SingleOrDefault();
                else
                    return GetCurrentUser(db).Select(i => i.I14).SingleOrDefault();
            }
        }

        internal static void SetAcademicYear(int academicYear)
        {
            using (var db = new DataModels.BDETALONDB(config.conf.ConnBD))
            {
                if (config.conf.IsPoratalUser)
                    GetCurrentPortalUser(db).Set(i => i.P123, academicYear).Update();
                else
                    GetCurrentUser(db).Set(i => i.I32, academicYear).Update();
            }
              
        }

        internal static void SetAcademicSemestr(int academicSemestr)
        {
            using (var db = new DataModels.BDETALONDB(config.conf.ConnBD))
            {
                if (config.conf.IsPoratalUser)
                    GetCurrentPortalUser(db).Set(i => i.P122, academicSemestr).Update();
                else
                    GetCurrentUser(db).Set(i => i.I14, academicSemestr).Update();
            }
        }
    }
}
