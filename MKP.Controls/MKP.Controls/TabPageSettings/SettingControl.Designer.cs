﻿namespace MKP.Controls.TabPageSettings
{
    partial class SettingControl
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbSemestr = new System.Windows.Forms.GroupBox();
            this.rb_SemVesna = new System.Windows.Forms.RadioButton();
            this.rb_SemOsen = new System.Windows.Forms.RadioButton();
            this.gbGod = new System.Windows.Forms.GroupBox();
            this.cb_AcademicYear = new System.Windows.Forms.ComboBox();
            this.bFixes = new System.Windows.Forms.Button();
            this.gbSemestr.SuspendLayout();
            this.gbGod.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbSemestr
            // 
            this.gbSemestr.Controls.Add(this.rb_SemVesna);
            this.gbSemestr.Controls.Add(this.rb_SemOsen);
            this.gbSemestr.Location = new System.Drawing.Point(3, 56);
            this.gbSemestr.Name = "gbSemestr";
            this.gbSemestr.Size = new System.Drawing.Size(187, 49);
            this.gbSemestr.TabIndex = 5;
            this.gbSemestr.TabStop = false;
            this.gbSemestr.Text = "Семестр";
            // 
            // rb_SemVesna
            // 
            this.rb_SemVesna.AutoSize = true;
            this.rb_SemVesna.Location = new System.Drawing.Point(107, 19);
            this.rb_SemVesna.Name = "rb_SemVesna";
            this.rb_SemVesna.Size = new System.Drawing.Size(74, 17);
            this.rb_SemVesna.TabIndex = 2;
            this.rb_SemVesna.TabStop = true;
            this.rb_SemVesna.Text = "Весенний";
            this.rb_SemVesna.UseVisualStyleBackColor = true;
            // 
            // rb_SemOsen
            // 
            this.rb_SemOsen.AutoSize = true;
            this.rb_SemOsen.Checked = true;
            this.rb_SemOsen.Location = new System.Drawing.Point(6, 19);
            this.rb_SemOsen.Name = "rb_SemOsen";
            this.rb_SemOsen.Size = new System.Drawing.Size(69, 17);
            this.rb_SemOsen.TabIndex = 1;
            this.rb_SemOsen.TabStop = true;
            this.rb_SemOsen.Text = "Осенний";
            this.rb_SemOsen.UseVisualStyleBackColor = true;
            this.rb_SemOsen.CheckedChanged += new System.EventHandler(this.rb_SemOsen_CheckedChanged);
            // 
            // gbGod
            // 
            this.gbGod.Controls.Add(this.cb_AcademicYear);
            this.gbGod.Location = new System.Drawing.Point(3, 3);
            this.gbGod.Name = "gbGod";
            this.gbGod.Size = new System.Drawing.Size(187, 47);
            this.gbGod.TabIndex = 4;
            this.gbGod.TabStop = false;
            this.gbGod.Text = "Учебный год";
            // 
            // cb_AcademicYear
            // 
            this.cb_AcademicYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_AcademicYear.FormattingEnabled = true;
            this.cb_AcademicYear.Location = new System.Drawing.Point(4, 15);
            this.cb_AcademicYear.Name = "cb_AcademicYear";
            this.cb_AcademicYear.Size = new System.Drawing.Size(177, 21);
            this.cb_AcademicYear.TabIndex = 8;
            this.cb_AcademicYear.SelectedIndexChanged += new System.EventHandler(this.cb_AcademicYear_SelectedIndexChanged);
            // 
            // bFixes
            // 
            this.bFixes.Location = new System.Drawing.Point(3, 111);
            this.bFixes.Name = "bFixes";
            this.bFixes.Size = new System.Drawing.Size(187, 23);
            this.bFixes.TabIndex = 6;
            this.bFixes.Text = "Исправления";
            this.bFixes.UseVisualStyleBackColor = true;
            this.bFixes.Click += new System.EventHandler(this.bFixes_Click);
            // 
            // SettingControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.bFixes);
            this.Controls.Add(this.gbSemestr);
            this.Controls.Add(this.gbGod);
            this.Name = "SettingControl";
            this.Size = new System.Drawing.Size(988, 639);
            this.gbSemestr.ResumeLayout(false);
            this.gbSemestr.PerformLayout();
            this.gbGod.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox gbSemestr;
        private System.Windows.Forms.RadioButton rb_SemVesna;
        private System.Windows.Forms.RadioButton rb_SemOsen;
        private System.Windows.Forms.GroupBox gbGod;
        private System.Windows.Forms.ComboBox cb_AcademicYear;
        private System.Windows.Forms.Button bFixes;
    }
}
