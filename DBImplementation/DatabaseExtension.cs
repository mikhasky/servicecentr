﻿using FirebirdSql.Data.FirebirdClient;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;

namespace MKP.DBImplementation
{
    public static class DatabaseExtension
    {
        class Fields
        {
            public string FIELDNAME { get; set; }
            public string FIELDTYPE { get; set; }
            public string ISNULL { private get; set; }
            public bool _ISNULL() { return ISNULL != "NOTNULL"; }
        }

        public static List<object> GetDynamicFromBD(string Query, string connstring)
        {
            List<object> rezult = new List<object>();
            using (FbConnection fbConnection = new FbConnection(connstring))
            {
                using (DataContext dataContext = new DataContext(fbConnection))
                {
                    try
                    {
                        var rez = dataContext.ExecuteQuery(GetResultType(Query, connstring), Query);
                        foreach (var item in rez)
                        {
                            rezult.Add(item);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }
            return rezult;
        }

        static Type GetResultType(string Query, string connstring)
        {
            TypeBuilder builder = CreateTypeBuilder(("MyDynamicAssembly" + new Random().Next()), "DB", "MyType");
            var rez = Query.Substring(6, (Query.ToLower().IndexOf("from", StringComparison.InvariantCultureIgnoreCase) - 6)).Split(new char[] { ' ', ',', '*' }, StringSplitOptions.RemoveEmptyEntries);
            if (rez.Length == 0)
                throw new ArgumentException("В запросе нет подходяших полей для создания сущности.");
            foreach (var item in (GetFields(rez, connstring)))
            {
                CreateAutoImplementedProperty(builder, item.FIELDNAME, GetFieldType(item));
            }
            return builder.CreateType();
        }

        static Type GetFieldType(Fields field)
        {
            Type type = null;
            if (field.FIELDTYPE.ToLower().StartsWith("var") || field.FIELDTYPE.ToLower().StartsWith("char"))
                type = typeof(string);
            else if (field.FIELDTYPE.ToLower().StartsWith("int") || field.FIELDTYPE.ToLower().StartsWith("smal"))
                type = field._ISNULL() ? typeof(int?) : typeof(int);
            else if (field.FIELDTYPE.ToLower().StartsWith("dat"))
                type = field._ISNULL() ? typeof(DateTime?) : typeof(DateTime);
            else if (field.FIELDTYPE.ToLower().StartsWith("doub"))
                type = field._ISNULL() ? typeof(double?) : typeof(double);
            else
                throw new NotImplementedException();

            return type;
        }
        static List<Fields> GetFields(string[] fields, string connstring)
        {
            string fild = string.Join(",", fields.Select(i => $"'{i.ToUpper()}'").ToArray());
            string query = @"select
trim(rf.rdb$field_name) as fieldname,
rdb$field_source fieldtype,
iif((f.rdb$null_flag=1)or(rf.rdb$null_flag=1),
'notnull','')as isnull
from
rdb$relation_fields rf join rdb$fields f on f.rdb$field_name=rf.rdb$field_source
where
rf.rdb$field_name in".ToLower() + "(" + fild + ")";
            List<Fields> rez = new List<Fields>();
            using (var con = new FirebirdSql.Data.FirebirdClient.FbConnection(connstring))
            {
                con.Open();
                using (var cont = new DataContext(con))
                {
                    rez = cont.ExecuteQuery<Fields>(query).ToList();
                }
            }

            return rez;
        }
        private static TypeBuilder CreateTypeBuilder(string assemblyName, string moduleName, string typeName)
        {
            TypeBuilder typeBuilder = AppDomain
            .CurrentDomain
            .DefineDynamicAssembly(new AssemblyName(assemblyName), AssemblyBuilderAccess.Run)
            .DefineDynamicModule(moduleName)
            .DefineType(typeName, TypeAttributes.Public);
            typeBuilder.DefineDefaultConstructor(MethodAttributes.Public);
            return typeBuilder;
        }
        private static void CreateAutoImplementedProperty(TypeBuilder builder, string propertyName, Type propertyType)
        {
            const string privateFieldPrefix = "m_";
            const string GetterPrefix = "get_";
            const string SetterPrefix = "set_";
            //Сформироватьполе.
            FieldBuilder fieldBuilder = builder.DefineField(string.Concat(privateFieldPrefix, propertyName), propertyType, FieldAttributes.Private);
            //Сформироватьсвойство
            PropertyBuilder propertyBuilder = builder.DefineProperty(propertyName, PropertyAttributes.HasDefault, propertyType, null);

            //GetСвойствоиSetатрибуты.
            MethodAttributes propertyMethodAttributes = MethodAttributes.Public | MethodAttributes.SpecialName | MethodAttributes.HideBySig;

            //ОпределениеметодаGet.
            MethodBuilder getterMethod = builder.DefineMethod(string.Concat(GetterPrefix, propertyName), propertyMethodAttributes, propertyType,
            Type.EmptyTypes);

            //EmitILcode.
            //ldarg.0
            //ldfld,_field
            //ret
            ILGenerator getterILCode = getterMethod.GetILGenerator();
            getterILCode.Emit(OpCodes.Ldarg_0);
            getterILCode.Emit(OpCodes.Ldfld, fieldBuilder);
            getterILCode.Emit(OpCodes.Ret);

            //ОпределениеметодаSet.
            MethodBuilder setterMethod = builder.DefineMethod(string.Concat(SetterPrefix, propertyName)
            , propertyMethodAttributes, null, new Type[]{propertyType
                });

            //EmitILcode.
            //ldarg.0
            //ldarg.1
            //stfld,_field
            //ret
            ILGenerator setterILCode = setterMethod.GetILGenerator();
            setterILCode.Emit(OpCodes.Ldarg_0);
            setterILCode.Emit(OpCodes.Ldarg_1);
            setterILCode.Emit(OpCodes.Stfld, fieldBuilder);
            setterILCode.Emit(OpCodes.Ret);
            propertyBuilder.SetGetMethod(getterMethod);
            propertyBuilder.SetSetMethod(setterMethod);
        }
    }
}
