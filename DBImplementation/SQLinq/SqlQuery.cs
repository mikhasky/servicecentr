﻿using  MKP.DBImplementation.SQLinq.Atributes;

using  System;
using  System.Collections.Generic;
using  System.Linq;
using  System.Linq.Expressions;
using  System.Text;

namespace  MKP.DBImplementation.SQLinq
{   
    class  SqlQuery<T>
    {
        
        private   static  string  GetTableName()
        {
            var  type = typeof(T);
            var  tableAttribute = type.GetCustomAttributes(typeof(SQLinqTableAttribute), false).FirstOrDefault() as SQLinqTableAttribute;
            if (tableAttribute != null)
                return  tableAttribute.Table;
            return  type.Name;
        }

        public  static  SqlQueryResult GetSelectQuery(object Where = null)
        {
            if (Where is string )
            {
                return  new  SqlQueryResult { Parameters = null, SqlQuery = $"select * from {GetTableName()} {Where}" };
            }
            else if (Where is Expression<Func<T, bool >>)
            {
                var  expresResult = new  SQLinq.Compiler.SqlExpressionCompiler().Compile((Expression<Func<T, bool >>)Where);
                return  new  SqlQueryResult()
                {
                    Parameters = expresResult?.Parameters ?? null,
                    SqlQuery = $"select * from {GetTableName()}{(expresResult != null ? $" where {expresResult.SQL}" : string .Empty)}"
                };
            }
            else if (Where == null)
            {
                return  new  SqlQueryResult { Parameters = null, SqlQuery = $"select * from {GetTableName()}" };
            }
            else
                throw new  ArgumentException("Where mast be string  or  Expression<Func<T, bool >>");  
        }

        public  static  SqlQueryResult GetUpdateQuery(T instatce,object where)
        {
            return  CreatstringWhere(instatce, where);
        }
        public  static  SqlQueryResult GetDeleteQuery(T instatce, object where)
        {
            throw new  NotImplementedException();
        }
        private   static  SqlQueryResult CreatstringWhere(T instatce, object where)
        {
            SqlQueryResult result = new  SqlQueryResult();
            if (where == null)
            {
                if (result.Parameters == null)
                    result.Parameters = new  Dictionary<string , object>();
                foreach (var  item in instatce.GetType().GetProperties())
                {
                    var  key = item.GetCustomAttributes(typeof(KeyAttribute), false);
                    if (key.Length > 0)
                    {
                        
                        result.Parameters.Add("@"+item.Name, item.GetValue(instatce, null));
                    }
                }
            }
            else if (where is string )
            {
                result.SqlQuery = (string )where;
            }
            else if (where is Expression<Func<T, bool >>)
            {
                var  expresResult = new  SQLinq.Compiler.SqlExpressionCompiler().Compile((Expression<Func<T, bool >>)where);
                result.SqlQuery = " where " + expresResult.SQL;
                result.Parameters = expresResult.Parameters;
            }
            else
                throw new  ArgumentException("Where mast be string  or  Expression<Func<T, bool >>");
            return  result;
        }
        public  static  SqlQueryResult GetInsertQuery(T instatce)
        {
            var  type = typeof(T);
            var  prop = type.GetProperties();           
            var  dict = prop.Select(i => GetValuesForInsert(instatce, i)).Select(i=>i).ToDictionary(i=>i.Key,j=>j.Value);
            var  fields = string .Join(",", dict.Select(i => i.Key).ToArray());
            string  Values = string .Join(",", dict.Select(i => "@" + i.Key).ToArray());
            return  new  SqlQueryResult()
            {
                Parameters = dict,
                SqlQuery = $"insert int o {GetTableName()} ({fields}) values({Values})"
            };
        }
        private   static  KeyValuePair<string , object> GetValuesForInsert(T instatce, System.Reflection.PropertyInfo item)
        {
            string  prpop = item.Name;
            object value = item.GetValue(instatce, null);
            KeyValuePair<string , object> rez = new  KeyValuePair<string , object>(prpop, value);
            return  rez;
        }
    }
    struct SqlQueryResult
    {
        public  string  SqlQuery { get; set; }
        public  IDictionary<string , object> Parameters { get; set; }
    }
}
