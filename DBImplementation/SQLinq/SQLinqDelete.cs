﻿using SQLinq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SQLinq
{
    class SQLinqDelete<T> : SQLinqUpdate<T>
    {
        public SQLinqDelete(T data) : base(data) { }
    }
}
