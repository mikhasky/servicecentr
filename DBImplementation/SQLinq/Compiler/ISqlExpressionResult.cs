﻿//Copyright (c) Chris Pietschmann 2012 (http://pietschsoft.com)
//Licensed under the GNU Library General public  License (LGPL)
//License can be found here: http://sqlinq.codeplex.com/license

using  System.Collections.Generic;

namespace  MKP.DBImplementation.SQLinq.Compiler
{
    interface ISqlExpressionResult
    {
        IDictionary<string , object> Parameters { get; set; }
    }
}
