﻿//Copyright (c) Chris Pietschmann 2013 (http://pietschsoft.com)
//Licensed under the GNU Library General public  License (LGPL)
//License can be found here: http://sqlinq.codeplex.com/license

using  System;

namespace  MKP.DBImplementation.SQLinq.Atributes
{
    /// <summary>
    /// Используется для явного указания имени столбца базы данных, если она не совпадает с именем свойства объекта.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public  class  SQLinqColumnAttribute : Attribute
    {
        /// <summary>
        /// SQL Linq ColumnAttribute конструктор
        /// </summary>
        /// <param name="columnName">Имя столбца базы данных, чтобы использовать для этого свойства с запросами SQL Linq.</param>
        public  SQLinqColumnAttribute(string  columnName = null, bool  insert = true, bool  update = true)
        {
            this.Column = columnName;
            this.Insert = insert;
            this.Update = update;
        }

        /// <summary>
        /// Имя столбца базы данных, чтобы использовать для этого свойства с запросами SQL Linq.
        /// </summary>
        public  string  Column { get; private   set; }

        /// <summary>
        /// Определяет, используется ли столбец для Insert;
        /// </summary>
        public  bool  Insert { get; set; }

        /// <summary>
        /// Определяет, используется ли столбец для Update;
        /// </summary>
        public  bool  Update { get; set; }
    }
}
