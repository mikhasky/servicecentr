﻿using  System;
using  System.Collections.Generic;
using  System.Linq;
using  System.Text;

namespace  MKP.DBImplementation.SQLinq.Atributes
{
    /// <summary>
    /// Используется для явного указания Ключа(Ключей) таблицы
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    class  SQLinqKeyAttribute: Attribute
    {
    }
}
