﻿//Copyright (c) Chris Pietschmann 2012 (http://pietschsoft.com)
//Licensed under the GNU Library General public  License (LGPL)
//License can be found here: http://sqlinq.codeplex.com/license

using  System;

namespace  MKP.DBImplementation.SQLinq.Atributes
{
    /// <summary>
    ///Используется для явного указания таблицы базы данных или просмотреть имя, если оно не совпадает с именем объекта.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class , AllowMultiple = false)]
    public  class  SQLinqTableAttribute : Attribute
    {
        /// <summary>
        ///SQLinqTableAttribute конструктор
        /// </summary>
        /// <param name="tableName">Имя таблицы / view базы данных, чтобы использовать для этого объекта с запросами SQL Linq.</param>
        public  SQLinqTableAttribute(string  tableName)
        {
            this.Table = tableName;
        }

        /// <summary>
        /// Имя таблицы / view базы данных, чтобы использовать для этого объекта с запросами SQL Linq.
        /// </summary>
        public  string  Table { get; private   set; }
    }
}
