﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SQLinq
{
    class SQLinqDeleteResult : ISQLinqResult
    {
        public string Table { get; set; }

        public IDictionary<string, string> Fields { get; set; }

        public IDictionary<string, object> Parameters { get; set; }

        public string Where { get; set; }

        public string ToQuery()
        {
            if (string.IsNullOrWhiteSpace((this.Table ?? string.Empty).Trim()))
            {
                throw new ArgumentException("SQLinqInsertResult.Table is required to have a value.", "Table");
            }
            if (this.Fields == null || this.Fields.Count == 0)
            {
                throw new ArgumentNullException("Fields");
            }
            if (this.Parameters == null || this.Parameters.Count == 0)
            {
                throw new ArgumentNullException("Parameters");
            }

            var fieldParameterList = new StringBuilder();

            var isFirst = true;
            foreach (var f in this.Fields)
            {
                if (!isFirst)
                {
                    fieldParameterList.Append(", ");
                }
                else
                {
                    isFirst = false;
                }
                fieldParameterList.Append(f.Key).Append(" = ").Append(f.Value);
            }

            if (string.IsNullOrWhiteSpace(this.Where))
            {
                return string.Format("DELETE FROM {0}", this.Table);
            }
            else
            {
                return string.Format("DELETE FROM {0} WHERE {1}", this.Table, this.Where);
            }
        }
    }
}
