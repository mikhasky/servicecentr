﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LinqToDB;
using LinqToDB.Linq;
using LinqToDB.SqlQuery;
using LinqToDB.Data;

namespace LinqToDB
{
    public static class DBExtension
    {
        public static DateTime CurrentTimestamp(this IDataContext dataContext)
        {
            return (dataContext as DataConnection).Query<DateTime>("select current_timestamp from rdb$database").Single();
        }
        public static int Gen_ID<T>(this IDataContext dataContext)
        {          
            return GenID<T>(dataContext);
        }
        public static int Gen_ID<T>(this IDataContext dataContext, T obj)
        {
            return GenID<T>(dataContext);
        }
        static int GenID<T>(IDataContext dataContext)
        {          
            var dataContextInfo = DataContextInfo.Create(dataContext);
            var sqlTable = new SqlTable<T>(dataContextInfo.MappingSchema);
            var keys = sqlTable.GetKeys(true).Cast<SqlField>().ToList();
            if (keys.Count != 1)
                throw new NotSupportedException("Count primary key mast be equals 1 !");
            try
            {
                return MKP.DBImplementation.WorkWhithDB.GetData<int>($"select GEN_ID({sqlTable.Name},1) as ID from rdb$database", (dataContext as DataConnection).Connection.ConnectionString).Single();
            }
            catch (FirebirdSql.Data.FirebirdClient.FbException exF1)
            {
                if (exF1.Message.Contains("invalid request BLR at offset"))
                    try
                    {
                        return MKP.DBImplementation.WorkWhithDB.GetData<int>($"select GEN_ID(GEN_{sqlTable.Name},1) as ID from rdb$database", (dataContext as DataConnection).Connection.ConnectionString).Single();
                    }
                    catch (FirebirdSql.Data.FirebirdClient.FbException exF2)
                    {
                        if (exF2.Message.Contains("invalid request BLR at offset"))
                            try
                            {
                                return MKP.DBImplementation.WorkWhithDB.GetData<int>($"SELECT first 1 id1 as ID FROM pr1('{keys.Single().Name}','{sqlTable.Name}') left join {sqlTable.Name} on ({keys.Single().Name}=id1) where {keys.Single().Name} is null", (dataContext as DataConnection).Connection.ConnectionString).SingleOrDefault();
                            }
                            catch (Exception ex)
                            {
                                throw ex;
                            }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return 0;
        }

    }
}
