﻿using  MKP.DBImplementation.SQLinq.Atributes;
using  System;
using  System.Collections.Generic;
using  System.Linq;
using  System.Text;

namespace  MKP.DBImplementation
{
    [SQLinqTable("ST")]
    public  class  Stud : Table<Stud>
    {             
        [Key]
        public  int  st1 { get; set; }       
        public  string  st2 { get; set; }
    }
}
