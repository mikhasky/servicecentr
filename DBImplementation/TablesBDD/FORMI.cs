﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MKP.DBImplementation.TablesBDD
{
    public class FORMI:ITable
    {
        [Key]
        public int FORMI1 { get; set; }
        /// <summary>
        /// ID модуля
        /// </summary>
        public int FORMI2 { get; set; }
        /// <summary>
        /// код вкладки TabSheeft   
        /// </summary>
        public int FORMI4 { get; set; }     
       
        public string FORMI7 { get; set; }
        public override string ToString()
        {
            return FORMI7;
        }
    }
}
