namespace  MKP.DBImplementation.Tables
{
    using  System;
   
    public  class  DA:Table<DA>
    {        
        public  int  DA1 { get; set; }
        public  string  DA2 { get; set; }
        public  string  DA3 { get; set; }
        public  DateTime  DA9 { get; set; }
        public  int  DA10 { get; set; }
        public  int  DA12 { get; set; }
        public  int  DA13 { get; set; }
        public  int  DA14 { get; set; }
        public  short  DA15 { get; set; }
        public  string  DA16 { get; set; }
        public  string  DA17 { get; set; }
    }
}
