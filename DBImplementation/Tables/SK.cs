namespace  MKP.DBImplementation.Tables
{
    using  System;
    public  class  SK:Table<SK>
    {
        [Key]       
        public  int  SK1 { get; set; }
        public  int  SK2 { get; set; }     
        public  string  SK3 { get; set; }
        public  DateTime  SK4 { get; set; }
        public  DateTime?  SK5 { get; set; }
        public  string  SK6 { get; set; }
        public  string  SK7 { get; set; }
        public  int  SK8 { get; set; }
        public  int  SK9 { get; set; }
        public  DateTime  SK10 { get; set; }
        public  int  SK11 { get; set; }
        public  DateTime?  SK12 { get; set; }
        public  string  SK13 { get; set; }
    }
}
