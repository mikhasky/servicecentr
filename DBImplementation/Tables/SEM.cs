namespace  MKP.DBImplementation.Tables
{
    using  System;   
    public   class  SEM
    {       
        public  int  SEM1 { get; set; }

        public  int  SEM2 { get; set; }

        public  short  SEM3 { get; set; }

        public  short  SEM4 { get; set; }

        public  short  SEM5 { get; set; }

        public  short  SEM7 { get; set; }

        public  short  SEM9 { get; set; }

        public  DateTime?  SEM10 { get; set; }

        public  DateTime?  SEM11 { get; set; }

        public  DateTime?  SEM12 { get; set; }

        public  DateTime?  SEM13 { get; set; }

        public  DateTime?  SEM14 { get; set; }

        public  DateTime?  SEM15 { get; set; }
        
        public  string  SEM17 { get; set; }

        public  DateTime  SEM18 { get; set; }

        public  int  SEM19 { get; set; }

        public  short  SEM20 { get; set; }

        public  short  SEM21 { get; set; }

        public  short  SEM22 { get; set; }

        public  int  SEM24 { get; set; }

        public  short  SEM23 { get; set; }
    }
}
