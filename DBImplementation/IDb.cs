﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using LinqToDB;
using LinqToDB.Mapping;

namespace MKP.DBImplementation
{
    public interface IDb
    {

    }
    [Table("ZZ")]
    public partial class ZZ : INotifyPropertyChanged, INotifyPropertyChanging
    {
        #region ZZ1 : int

        private int _zZ1;
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int ZZ1 // integer
        {
            get { return _zZ1; }
            set
            {
                if (_zZ1 != value)
                {
                    OnZZ1Changing();

                    BeforeZZ1Changed(value);
                    _zZ1 = value;
                    AfterZZ1Changed();

                    OnZZ1Changed();
                }
            }
        }

        #region INotifyPropertyChanged support

        partial void BeforeZZ1Changed(int newValue);
        partial void AfterZZ1Changed();

        public const string NameOfZZ1 = "ZZ1";

        private static readonly PropertyChangedEventArgs _zZ1ChangedEventArgs = new PropertyChangedEventArgs(NameOfZZ1);

        private void OnZZ1Changed()
        {
            OnPropertyChanged(_zZ1ChangedEventArgs);
        }

        #endregion

        #region INotifyPropertyChanging support

        private static readonly PropertyChangingEventArgs _zZ1ChangingEventArgs = new PropertyChangingEventArgs(NameOfZZ1);

        private void OnZZ1Changing()
        {
            OnPropertyChanging(_zZ1ChangingEventArgs);
        }

        #endregion

        #endregion

        #region ZZ2 : DateTime?

        private DateTime? _zZ2;
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? ZZ2 // timestamp
        {
            get { return _zZ2; }
            set
            {
                if (_zZ2 != value)
                {
                    OnZZ2Changing();

                    BeforeZZ2Changed(value);
                    _zZ2 = value;
                    AfterZZ2Changed();

                    OnZZ2Changed();
                }
            }
        }

        #region INotifyPropertyChanged support

        partial void BeforeZZ2Changed(DateTime? newValue);
        partial void AfterZZ2Changed();

        public const string NameOfZZ2 = "ZZ2";

        private static readonly PropertyChangedEventArgs _zZ2ChangedEventArgs = new PropertyChangedEventArgs(NameOfZZ2);

        private void OnZZ2Changed()
        {
            OnPropertyChanged(_zZ2ChangedEventArgs);
        }

        #endregion

        #region INotifyPropertyChanging support

        private static readonly PropertyChangingEventArgs _zZ2ChangingEventArgs = new PropertyChangingEventArgs(NameOfZZ2);

        private void OnZZ2Changing()
        {
            OnPropertyChanging(_zZ2ChangingEventArgs);
        }

        #endregion

        #endregion

        #region ZZ3 : short

        private short _zZ3;
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ZZ3 // smallint
        {
            get { return _zZ3; }
            set
            {
                if (_zZ3 != value)
                {
                    OnZZ3Changing();

                    BeforeZZ3Changed(value);
                    _zZ3 = value;
                    AfterZZ3Changed();

                    OnZZ3Changed();
                }
            }
        }

        #region INotifyPropertyChanged support

        partial void BeforeZZ3Changed(short newValue);
        partial void AfterZZ3Changed();

        public const string NameOfZZ3 = "ZZ3";

        private static readonly PropertyChangedEventArgs _zZ3ChangedEventArgs = new PropertyChangedEventArgs(NameOfZZ3);

        private void OnZZ3Changed()
        {
            OnPropertyChanged(_zZ3ChangedEventArgs);
        }

        #endregion

        #region INotifyPropertyChanging support

        private static readonly PropertyChangingEventArgs _zZ3ChangingEventArgs = new PropertyChangingEventArgs(NameOfZZ3);

        private void OnZZ3Changing()
        {
            OnPropertyChanging(_zZ3ChangingEventArgs);
        }

        #endregion

        #endregion

        #region ZZ4 : int

        private int _zZ4;
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ZZ4 // integer
        {
            get { return _zZ4; }
            set
            {
                if (_zZ4 != value)
                {
                    OnZZ4Changing();

                    BeforeZZ4Changed(value);
                    _zZ4 = value;
                    AfterZZ4Changed();

                    OnZZ4Changed();
                }
            }
        }

        #region INotifyPropertyChanged support

        partial void BeforeZZ4Changed(int newValue);
        partial void AfterZZ4Changed();

        public const string NameOfZZ4 = "ZZ4";

        private static readonly PropertyChangedEventArgs _zZ4ChangedEventArgs = new PropertyChangedEventArgs(NameOfZZ4);

        private void OnZZ4Changed()
        {
            OnPropertyChanged(_zZ4ChangedEventArgs);
        }

        #endregion

        #region INotifyPropertyChanging support

        private static readonly PropertyChangingEventArgs _zZ4ChangingEventArgs = new PropertyChangingEventArgs(NameOfZZ4);

        private void OnZZ4Changing()
        {
            OnPropertyChanging(_zZ4ChangingEventArgs);
        }

        #endregion

        #endregion

        #region ZZ5 : int

        private int _zZ5;
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ZZ5 // integer
        {
            get { return _zZ5; }
            set
            {
                if (_zZ5 != value)
                {
                    OnZZ5Changing();

                    BeforeZZ5Changed(value);
                    _zZ5 = value;
                    AfterZZ5Changed();

                    OnZZ5Changed();
                }
            }
        }

        #region INotifyPropertyChanged support

        partial void BeforeZZ5Changed(int newValue);
        partial void AfterZZ5Changed();

        public const string NameOfZZ5 = "ZZ5";

        private static readonly PropertyChangedEventArgs _zZ5ChangedEventArgs = new PropertyChangedEventArgs(NameOfZZ5);

        private void OnZZ5Changed()
        {
            OnPropertyChanged(_zZ5ChangedEventArgs);
        }

        #endregion

        #region INotifyPropertyChanging support

        private static readonly PropertyChangingEventArgs _zZ5ChangingEventArgs = new PropertyChangingEventArgs(NameOfZZ5);

        private void OnZZ5Changing()
        {
            OnPropertyChanging(_zZ5ChangingEventArgs);
        }

        #endregion

        #endregion

        #region ZZ6 : int

        private int _zZ6;
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ZZ6 // integer
        {
            get { return _zZ6; }
            set
            {
                if (_zZ6 != value)
                {
                    OnZZ6Changing();

                    BeforeZZ6Changed(value);
                    _zZ6 = value;
                    AfterZZ6Changed();

                    OnZZ6Changed();
                }
            }
        }

        #region INotifyPropertyChanged support

        partial void BeforeZZ6Changed(int newValue);
        partial void AfterZZ6Changed();

        public const string NameOfZZ6 = "ZZ6";

        private static readonly PropertyChangedEventArgs _zZ6ChangedEventArgs = new PropertyChangedEventArgs(NameOfZZ6);

        private void OnZZ6Changed()
        {
            OnPropertyChanged(_zZ6ChangedEventArgs);
        }

        #endregion

        #region INotifyPropertyChanging support

        private static readonly PropertyChangingEventArgs _zZ6ChangingEventArgs = new PropertyChangingEventArgs(NameOfZZ6);

        private void OnZZ6Changing()
        {
            OnPropertyChanging(_zZ6ChangingEventArgs);
        }

        #endregion

        #endregion

        #region ZZ7 : string

        private string _zZ7;
        [Column(DataType = DataType.NVarChar, Length = 250), NotNull]
        public string ZZ7 // varchar(250)
        {
            get { return _zZ7; }
            set
            {
                if (_zZ7 != value)
                {
                    OnZZ7Changing();

                    BeforeZZ7Changed(value);
                    _zZ7 = value;
                    AfterZZ7Changed();

                    OnZZ7Changed();
                }
            }
        }

        #region INotifyPropertyChanged support

        partial void BeforeZZ7Changed(string newValue);
        partial void AfterZZ7Changed();

        public const string NameOfZZ7 = "ZZ7";

        private static readonly PropertyChangedEventArgs _zZ7ChangedEventArgs = new PropertyChangedEventArgs(NameOfZZ7);

        private void OnZZ7Changed()
        {
            OnPropertyChanged(_zZ7ChangedEventArgs);
        }

        #endregion

        #region INotifyPropertyChanging support

        private static readonly PropertyChangingEventArgs _zZ7ChangingEventArgs = new PropertyChangingEventArgs(NameOfZZ7);

        private void OnZZ7Changing()
        {
            OnPropertyChanging(_zZ7ChangingEventArgs);
        }

        #endregion

        #endregion

        #region ZZ8 : DateTime?

        private DateTime? _zZ8;
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? ZZ8 // timestamp
        {
            get { return _zZ8; }
            set
            {
                if (_zZ8 != value)
                {
                    OnZZ8Changing();

                    BeforeZZ8Changed(value);
                    _zZ8 = value;
                    AfterZZ8Changed();

                    OnZZ8Changed();
                }
            }
        }

        #region INotifyPropertyChanged support

        partial void BeforeZZ8Changed(DateTime? newValue);
        partial void AfterZZ8Changed();

        public const string NameOfZZ8 = "ZZ8";

        private static readonly PropertyChangedEventArgs _zZ8ChangedEventArgs = new PropertyChangedEventArgs(NameOfZZ8);

        private void OnZZ8Changed()
        {
            OnPropertyChanged(_zZ8ChangedEventArgs);
        }

        #endregion

        #region INotifyPropertyChanging support

        private static readonly PropertyChangingEventArgs _zZ8ChangingEventArgs = new PropertyChangingEventArgs(NameOfZZ8);

        private void OnZZ8Changing()
        {
            OnPropertyChanging(_zZ8ChangingEventArgs);
        }

        #endregion

        #endregion

        #region ZZ9 : int

        private int _zZ9;
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ZZ9 // integer
        {
            get { return _zZ9; }
            set
            {
                if (_zZ9 != value)
                {
                    OnZZ9Changing();

                    BeforeZZ9Changed(value);
                    _zZ9 = value;
                    AfterZZ9Changed();

                    OnZZ9Changed();
                }
            }
        }

        #region INotifyPropertyChanged support

        partial void BeforeZZ9Changed(int newValue);
        partial void AfterZZ9Changed();

        public const string NameOfZZ9 = "ZZ9";

        private static readonly PropertyChangedEventArgs _zZ9ChangedEventArgs = new PropertyChangedEventArgs(NameOfZZ9);

        private void OnZZ9Changed()
        {
            OnPropertyChanged(_zZ9ChangedEventArgs);
        }

        #endregion

        #region INotifyPropertyChanging support

        private static readonly PropertyChangingEventArgs _zZ9ChangingEventArgs = new PropertyChangingEventArgs(NameOfZZ9);

        private void OnZZ9Changing()
        {
            OnPropertyChanging(_zZ9ChangingEventArgs);
        }

        #endregion

        #endregion

        #region ZZ10 : int

        private int _zZ10;
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ZZ10 // integer
        {
            get { return _zZ10; }
            set
            {
                if (_zZ10 != value)
                {
                    OnZZ10Changing();

                    BeforeZZ10Changed(value);
                    _zZ10 = value;
                    AfterZZ10Changed();

                    OnZZ10Changed();
                }
            }
        }

        #region INotifyPropertyChanged support

        partial void BeforeZZ10Changed(int newValue);
        partial void AfterZZ10Changed();

        public const string NameOfZZ10 = "ZZ10";

        private static readonly PropertyChangedEventArgs _zZ10ChangedEventArgs = new PropertyChangedEventArgs(NameOfZZ10);

        private void OnZZ10Changed()
        {
            OnPropertyChanged(_zZ10ChangedEventArgs);
        }

        #endregion

        #region INotifyPropertyChanging support

        private static readonly PropertyChangingEventArgs _zZ10ChangingEventArgs = new PropertyChangingEventArgs(NameOfZZ10);

        private void OnZZ10Changing()
        {
            OnPropertyChanging(_zZ10ChangingEventArgs);
        }

        #endregion

        #endregion

        #region ZZ11 : short

        private short _zZ11;
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ZZ11 // smallint
        {
            get { return _zZ11; }
            set
            {
                if (_zZ11 != value)
                {
                    OnZZ11Changing();

                    BeforeZZ11Changed(value);
                    _zZ11 = value;
                    AfterZZ11Changed();

                    OnZZ11Changed();
                }
            }
        }

        #region INotifyPropertyChanged support

        partial void BeforeZZ11Changed(short newValue);
        partial void AfterZZ11Changed();

        public const string NameOfZZ11 = "ZZ11";

        private static readonly PropertyChangedEventArgs _zZ11ChangedEventArgs = new PropertyChangedEventArgs(NameOfZZ11);

        private void OnZZ11Changed()
        {
            OnPropertyChanged(_zZ11ChangedEventArgs);
        }

        #endregion

        #region INotifyPropertyChanging support

        private static readonly PropertyChangingEventArgs _zZ11ChangingEventArgs = new PropertyChangingEventArgs(NameOfZZ11);

        private void OnZZ11Changing()
        {
            OnPropertyChanging(_zZ11ChangingEventArgs);
        }

        #endregion

        #endregion

        #region INotifyPropertyChanged support

#if !SILVERLIGHT
        [field: NonSerialized]
#endif
        public virtual event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            var propertyChanged = PropertyChanged;

            if (propertyChanged != null)
            {
#if SILVERLIGHT
				if (System.Windows.Deployment.Current.Dispatcher.CheckAccess())
					propertyChanged(this, new PropertyChangedEventArgs(propertyName));
				else
					System.Windows.Deployment.Current.Dispatcher.BeginInvoke(
						() =>
						{
							var pc = PropertyChanged;
							if (pc != null)
								pc(this, new PropertyChangedEventArgs(propertyName));
						});
#else
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
#endif
            }
        }

        protected void OnPropertyChanged(PropertyChangedEventArgs arg)
        {
            var propertyChanged = PropertyChanged;

            if (propertyChanged != null)
            {
#if SILVERLIGHT
				if (System.Windows.Deployment.Current.Dispatcher.CheckAccess())
					propertyChanged(this, arg);
				else
					System.Windows.Deployment.Current.Dispatcher.BeginInvoke(
						() =>
						{
							var pc = PropertyChanged;
							if (pc != null)
								pc(this, arg);
						});
#else
                propertyChanged(this, arg);
#endif
            }
        }

        #endregion

        #region INotifyPropertyChanging support

#if !SILVERLIGHT
        [field: NonSerialized]
#endif
        public virtual event PropertyChangingEventHandler PropertyChanging;

        protected void OnPropertyChanging(string propertyName)
        {
            var propertyChanging = PropertyChanging;

            if (propertyChanging != null)
            {
#if SILVERLIGHT
				if (System.Windows.Deployment.Current.Dispatcher.CheckAccess())
					propertyChanging(this, new PropertyChangingEventArgs(propertyName));
				else
					System.Windows.Deployment.Current.Dispatcher.BeginInvoke(
						() =>
						{
							var pc = PropertyChanging;
							if (pc != null)
								pc(this, new PropertyChangingEventArgs(propertyName));
						});
#else
                propertyChanging(this, new PropertyChangingEventArgs(propertyName));
#endif
            }
        }

        protected void OnPropertyChanging(PropertyChangingEventArgs arg)
        {
            var propertyChanging = PropertyChanging;

            if (propertyChanging != null)
            {
#if SILVERLIGHT
				if (System.Windows.Deployment.Current.Dispatcher.CheckAccess())
					propertyChanging(this, arg);
				else
					System.Windows.Deployment.Current.Dispatcher.BeginInvoke(
						() =>
						{
							var pc = PropertyChanging;
							if (pc != null)
								pc(this, arg);
						});
#else
                propertyChanging(this, arg);
#endif
            }
        }

        #endregion
    }
}
