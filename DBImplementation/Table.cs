﻿using  FirebirdSql.Data.FirebirdClient;
using  MKP.DBImplementation;
using  System;
using  System.Collections.Generic;
using  System.Data.Linq;
using  System.Linq;
using  System.Linq.Expressions;
using  System.Reflection;
using  System.Text;
using  System.Collections;

namespace  MKP.DBImplementation
{   
    public  abstract class  Table<T> where T : Table<T>
    {
        #region  GetData
        /// <summary>
        /// Получить данные с условием
        /// </summary>
        /// <param name="where">Лябда для условия</param>
        /// <param name="connstring ">Строка подключения</param>
        /// <return s></return s>
        public  static  List<T> GetData(Expression<Func<T, bool >> where, string  connstring )
        {
            var  query = SQLinq.SqlQuery<T>.GetSelectQuery(where);
            return  WorkWhithDB.GetDataForQuery<T>(query.SqlQuery, connstring , query.Parameters.ToArray());
        }
        /// <summary>
        /// Получить данные с условием
        /// </summary>
        /// <param name="where">Строка для условия</param>
        /// <param name="connstring ">Строка подключения</param>
        /// <return s></return s>
        public  static  List<T> GetData(string  where,string  connstring )
        {
            var  query = SQLinq.SqlQuery<T>.GetSelectQuery(where);
            return  WorkWhithDB.GetDataForQuery<T>(query.SqlQuery, connstring );
        }
        /// <summary>
        /// Получить данные
        /// </summary>
        /// <param name="connstring ">Строка подключения</param>
        /// <return s></return s>
        public  static  List<T> GetData(string  connstring )
        {
            var  query = SQLinq.SqlQuery<T>.GetSelectQuery();
            return  WorkWhithDB.GetDataForQuery<T>(query.SqlQuery, connstring );
        }
        #endregion

        #region  Insert new  record
        /// <summary>
        /// Добавить новую запись в Таблицу
        /// </summary>
        /// <param name="connstring ">Строка подключения</param>
        /// <return s></return s>
        public  bool  Insert(string  connstring )
        {
            var  query = SQLinq.SqlQuery<T>.GetInsertQuery((T)this);
            return  WorkWhithDB.ExecutNonQuery(query.SqlQuery, connstring , query.Parameters.ToArray());
        }
        #endregion

        #region  Update
        /// <summary>
        /// Обновиление записи в БД
        /// </summary>
        /// <param name="where">Условие</param>
        /// <param name="connstring ">Строка подключения </param>
        /// <return s></return s>
        public  bool  Update(Expression<Func<T, bool >> where,string  connstring )
        {
            var  foo = SQLinq.SqlQuery<T>.GetUpdateQuery((T)this, where);
            return  false;
        }
        /// <summary>
        /// Обновиление записи в БД
        /// </summary>
        /// <param name="where">Условие</param>
        /// <param name="connstring ">Строка подключения </param>
        /// <return s></return s>
        public  bool  Update(string  where, string  connstring )
        {
            var  foo = SQLinq.SqlQuery<T>.GetUpdateQuery((T)this, where);
            return  false;
        }
        /// <summary>
        ///  Обновиление записи в БД
        /// </summary>
        /// <param name="connstring ">Строка подключения</param>
        /// <return s></return s>
        public  bool  Update(string  connstring )
        {
            var  foo = SQLinq.SqlQuery<T>.GetUpdateQuery((T)this, null);
            return  false;
        }
        #endregion

        #region  Delete

        public  bool  Delete(Expression<Func<T, bool >> where, string  connstring )
        {           
            throw new  NotImplementedException();
        }
        public  bool  Delete(string  where, string  connstring )
        {
            throw new  NotImplementedException();
        }
        #endregion
    }
}

