﻿using System;
using System.Collections.Generic;
using System.Data;

using LinqToDB;
using LinqToDB.Common;
using LinqToDB.Data;
using LinqToDB.Mapping;

namespace MKP.DataModels
{
    /// <summary>
    /// Database       : BDETALON
    /// Data Source    : localhost
    /// Server Version : WI-V2.5.6.27020 Firebird 2.5
    /// </summary>
    public partial class BDETALONDB : LinqToDB.Data.DataConnection
    {
        public ITable<BRAND> BRAND { get { return this.GetTable<BRAND>(); } }
        public ITable<CLIENT> CLIENT { get { return this.GetTable<CLIENT>(); } }
        public ITable<COMPLECT> COMPLECT { get { return this.GetTable<COMPLECT>(); } }
        public ITable<DAMAG> DAMAG { get { return this.GetTable<DAMAG>(); } }
        public ITable<DEVICE> DEVICE { get { return this.GetTable<DEVICE>(); } }
        public ITable<DEVICE_STATUS> DEVICE_STATUS { get { return this.GetTable<DEVICE_STATUS>(); } }
        public ITable<DEVICE_STSHST> DEVICE_STSHST { get { return this.GetTable<DEVICE_STSHST>(); } }
        public ITable<DEVICE_TP> DEVICE_TP { get { return this.GetTable<DEVICE_TP>(); } }
        public ITable<LABEL> LABEL { get { return this.GetTable<LABEL>(); } }
        public ITable<MESSAGES> MESSAGES { get { return this.GetTable<MESSAGES>(); } }
        public ITable<SPRAV_WORK> SPRAV_WORK { get { return this.GetTable<SPRAV_WORK>(); } }
        public ITable<UUSER> UUSER { get { return this.GetTable<UUSER>(); } }
        public ITable<WWORK> WWORK { get { return this.GetTable<WWORK>(); } }


        public ITable<A> A { get { return this.GetTable<A>(); } }
        public ITable<AB> AB { get { return this.GetTable<AB>(); } }
        public ITable<ABCD> ABCD { get { return this.GetTable<ABCD>(); } }
        public ITable<ABCDZ> ABCDZ { get { return this.GetTable<ABCDZ>(); } }
        public ITable<ABCL> ABCL { get { return this.GetTable<ABCL>(); } }
        public ITable<ABCT> ABCT { get { return this.GetTable<ABCT>(); } }
        public ITable<ABD> ABD { get { return this.GetTable<ABD>(); } }
        public ITable<ABDE> ABDE { get { return this.GetTable<ABDE>(); } }
        public ITable<ABDPB> ABDPB { get { return this.GetTable<ABDPB>(); } }
        public ITable<ABFI> ABFI { get { return this.GetTable<ABFI>(); } }
        public ITable<ABFIZ> ABFIZ { get { return this.GetTable<ABFIZ>(); } }
        public ITable<ABI> ABI { get { return this.GetTable<ABI>(); } }
        public ITable<ABICB> ABICB { get { return this.GetTable<ABICB>(); } }
        public ITable<ABID> ABID { get { return this.GetTable<ABID>(); } }
        public ITable<ABIDE> ABIDE { get { return this.GetTable<ABIDE>(); } }
        public ITable<ABIDKOF> ABIDKOF { get { return this.GetTable<ABIDKOF>(); } }
        public ITable<ABIKOF> ABIKOF { get { return this.GetTable<ABIKOF>(); } }
        public ITable<ABIKOFS> ABIKOFS { get { return this.GetTable<ABIKOFS>(); } }
        public ITable<ABIN> ABIN { get { return this.GetTable<ABIN>(); } }
        public ITable<ABISET> ABISET { get { return this.GetTable<ABISET>(); } }
        public ITable<ABISH> ABISH { get { return this.GetTable<ABISH>(); } }
        public ITable<ABISS> ABISS { get { return this.GetTable<ABISS>(); } }
        public ITable<ABISSC> ABISSC { get { return this.GetTable<ABISSC>(); } }
        public ITable<ABIST> ABIST { get { return this.GetTable<ABIST>(); } }
        public ITable<ABISTL> ABISTL { get { return this.GetTable<ABISTL>(); } }
        public ITable<ABISZ> ABISZ { get { return this.GetTable<ABISZ>(); } }
        public ITable<ABTMP> ABTMP { get { return this.GetTable<ABTMP>(); } }
        public ITable<ABTMPC> ABTMPC { get { return this.GetTable<ABTMPC>(); } }
        public ITable<ABTMPCI> ABTMPCI { get { return this.GetTable<ABTMPCI>(); } }
        public ITable<ABTMPI> ABTMPI { get { return this.GetTable<ABTMPI>(); } }
        public ITable<AD> AD { get { return this.GetTable<AD>(); } }
        public ITable<ADP> ADP { get { return this.GetTable<ADP>(); } }
        public ITable<ADZ> ADZ { get { return this.GetTable<ADZ>(); } }
        public ITable<AE> AE { get { return this.GetTable<AE>(); } }
        public ITable<AEI> AEI { get { return this.GetTable<AEI>(); } }
        public ITable<AEIEX> AEIEX { get { return this.GetTable<AEIEX>(); } }
        public ITable<AEIZ> AEIZ { get { return this.GetTable<AEIZ>(); } }
        public ITable<AO> AO { get { return this.GetTable<AO>(); } }
        public ITable<ASP> ASP { get { return this.GetTable<ASP>(); } }
        public ITable<ATI> ATI { get { return this.GetTable<ATI>(); } }
        public ITable<B> B { get { return this.GetTable<B>(); } }
        public ITable<BAL> BAL { get { return this.GetTable<BAL>(); } }
        public ITable<BB> BB { get { return this.GetTable<BB>(); } }
        public ITable<BL> BL { get { return this.GetTable<BL>(); } }
        public ITable<BLA> BLA { get { return this.GetTable<BLA>(); } }
        public ITable<BSPOL> BSPOL { get { return this.GetTable<BSPOL>(); } }
        public ITable<C> C { get { return this.GetTable<C>(); } }
        public ITable<CMP> CMP { get { return this.GetTable<CMP>(); } }
        public ITable<CN> CN { get { return this.GetTable<CN>(); } }
        public ITable<CWB> CWB { get { return this.GetTable<CWB>(); } }
        public ITable<CXM> CXM { get { return this.GetTable<CXM>(); } }
        public ITable<CXMB> CXMB { get { return this.GetTable<CXMB>(); } }
        public ITable<D> D { get { return this.GetTable<D>(); } }
        public ITable<DA> DA { get { return this.GetTable<DA>(); } }
        public ITable<DAO> DAO { get { return this.GetTable<DAO>(); } }
        public ITable<DAR> DAR { get { return this.GetTable<DAR>(); } }
        public ITable<DAS> DAS { get { return this.GetTable<DAS>(); } }
        public ITable<DC> DC { get { return this.GetTable<DC>(); } }
        public ITable<DDO> DDO { get { return this.GetTable<DDO>(); } }
        public ITable<DDOI> DDOI { get { return this.GetTable<DDOI>(); } }
        public ITable<DDON> DDON { get { return this.GetTable<DDON>(); } }
        public ITable<DDST> DDST { get { return this.GetTable<DDST>(); } }
        public ITable<DIGP> DIGP { get { return this.GetTable<DIGP>(); } }
        public ITable<DIGPA> DIGPA { get { return this.GetTable<DIGPA>(); } }
        public ITable<DIGPT> DIGPT { get { return this.GetTable<DIGPT>(); } }
        public ITable<DIGPTA> DIGPTA { get { return this.GetTable<DIGPTA>(); } }
        public ITable<DIPN> DIPN { get { return this.GetTable<DIPN>(); } }
        public ITable<DK> DK { get { return this.GetTable<DK>(); } }
        public ITable<DKID> DKID { get { return this.GetTable<DKID>(); } }
        public ITable<DM> DM { get { return this.GetTable<DM>(); } }
        public ITable<DMF> DMF { get { return this.GetTable<DMF>(); } }
        public ITable<DMFT> DMFT { get { return this.GetTable<DMFT>(); } }
        public ITable<DMIND> DMIND { get { return this.GetTable<DMIND>(); } }
        public ITable<DMP> DMP { get { return this.GetTable<DMP>(); } }
        public ITable<DNA> DNA { get { return this.GetTable<DNA>(); } }
        public ITable<DNAR> DNAR { get { return this.GetTable<DNAR>(); } }
        public ITable<DNK> DNK { get { return this.GetTable<DNK>(); } }
        public ITable<DNO> DNO { get { return this.GetTable<DNO>(); } }
        public ITable<DNSH> DNSH { get { return this.GetTable<DNSH>(); } }
        public ITable<DNSHF> DNSHF { get { return this.GetTable<DNSHF>(); } }
        public ITable<DNSP> DNSP { get { return this.GetTable<DNSP>(); } }
        public ITable<DOB> DOB { get { return this.GetTable<DOB>(); } }
        public ITable<DOBU> DOBU { get { return this.GetTable<DOBU>(); } }
        public ITable<DOL> DOL { get { return this.GetTable<DOL>(); } }
        public ITable<DOPB> DOPB { get { return this.GetTable<DOPB>(); } }
        public ITable<DOPST> DOPST { get { return this.GetTable<DOPST>(); } }
        public ITable<DOPBST> DOPBST { get { return this.GetTable<DOPBST>(); } }
        public ITable<DPR> DPR { get { return this.GetTable<DPR>(); } }
        public ITable<DPRM> DPRM { get { return this.GetTable<DPRM>(); } }
        public ITable<DPRO> DPRO { get { return this.GetTable<DPRO>(); } }
        public ITable<DPROV> DPROV { get { return this.GetTable<DPROV>(); } }
        public ITable<DPRV> DPRV { get { return this.GetTable<DPRV>(); } }
        public ITable<DPSR> DPSR { get { return this.GetTable<DPSR>(); } }
        public ITable<DRSG> DRSG { get { return this.GetTable<DRSG>(); } }
        public ITable<DU> DU { get { return this.GetTable<DU>(); } }
        public ITable<E> E { get { return this.GetTable<E>(); } }
        public ITable<EDRP> EDRP { get { return this.GetTable<EDRP>(); } }
        public ITable<EK> EK { get { return this.GetTable<EK>(); } }
        public ITable<ELG> ELG { get { return this.GetTable<ELG>(); } }
        public ITable<ELGD> ELGD { get { return this.GetTable<ELGD>(); } }
        public ITable<ELGDST> ELGDST { get { return this.GetTable<ELGDST>(); } }
        public ITable<ELGNO> ELGNO { get { return this.GetTable<ELGNO>(); } }
        public ITable<ELGOTR> ELGOTR { get { return this.GetTable<ELGOTR>(); } }
        public ITable<ELGP> ELGP { get { return this.GetTable<ELGP>(); } }
        public ITable<ELGPMKST> ELGPMKST { get { return this.GetTable<ELGPMKST>(); } }
        public ITable<ELGR> ELGR { get { return this.GetTable<ELGR>(); } }
        public ITable<ELGSD> ELGSD { get { return this.GetTable<ELGSD>(); } }
        public ITable<ELGVST> ELGVST { get { return this.GetTable<ELGVST>(); } }
        public ITable<ELGZ> ELGZ { get { return this.GetTable<ELGZ>(); } }
        public ITable<ELGZST> ELGZST { get { return this.GetTable<ELGZST>(); } }
        public ITable<ELGZU> ELGZU { get { return this.GetTable<ELGZU>(); } }
        public ITable<EPR> EPR { get { return this.GetTable<EPR>(); } }
        public ITable<EPRT> EPRT { get { return this.GetTable<EPRT>(); } }
        public ITable<F> F { get { return this.GetTable<F>(); } }
        public ITable<FEDEDBO> FEDEDBO { get { return this.GetTable<FEDEDBO>(); } }
        public ITable<FG> FG { get { return this.GetTable<FG>(); } }
        public ITable<FGP> FGP { get { return this.GetTable<FGP>(); } }
        public ITable<FO> FO { get { return this.GetTable<FO>(); } }
        public ITable<FST> FST { get { return this.GetTable<FST>(); } }
        public ITable<FSTG> FSTG { get { return this.GetTable<FSTG>(); } }
        public ITable<GA> GA { get { return this.GetTable<GA>(); } }
        public ITable<GAA> GAA { get { return this.GetTable<GAA>(); } }
        public ITable<GNA> GNA { get { return this.GetTable<GNA>(); } }
        public ITable<GOST> GOST { get { return this.GetTable<GOST>(); } }
        public ITable<GP> GP { get { return this.GetTable<GP>(); } }
        public ITable<GPI> GPI { get { return this.GetTable<GPI>(); } }
        public ITable<GR> GR { get { return this.GetTable<GR>(); } }
        public ITable<GVSPR> GVSPR { get { return this.GetTable<GVSPR>(); } }
        public ITable<GRANTS> GRANTS { get { return this.GetTable<GRANTS>(); } }
        public ITable<GUP> GUP { get { return this.GetTable<GUP>(); } }
        public ITable<I> I { get { return this.GetTable<I>(); } }
        public ITable<IA> IA { get { return this.GetTable<IA>(); } }
        public ITable<IDO> IDO { get { return this.GetTable<IDO>(); } }
        public ITable<IK> IK { get { return this.GetTable<IK>(); } }
        public ITable<IM> IM { get { return this.GetTable<IM>(); } }
        public ITable<INNF> INNF { get { return this.GetTable<INNF>(); } }
        public ITable<INNFP> INNFP { get { return this.GetTable<INNFP>(); } }
        public ITable<JPV> JPV { get { return this.GetTable<JPV>(); } }
        public ITable<JPVD> JPVD { get { return this.GetTable<JPVD>(); } }
        public ITable<JPVP> JPVP { get { return this.GetTable<JPVP>(); } }
        public ITable<K> K { get { return this.GetTable<K>(); } }
        public ITable<KA> KA { get { return this.GetTable<KA>(); } }
        public ITable<KAA> KAA { get { return this.GetTable<KAA>(); } }
        public ITable<KCP> KCP { get { return this.GetTable<KCP>(); } }
        public ITable<KD> KD { get { return this.GetTable<KD>(); } }
        public ITable<KGRP> KGRP { get { return this.GetTable<KGRP>(); } }
        public ITable<KK> KK { get { return this.GetTable<KK>(); } }
        public ITable<KMP> KMP { get { return this.GetTable<KMP>(); } }
        public ITable<KP> KP { get { return this.GetTable<KP>(); } }
        public ITable<KPK> KPK { get { return this.GetTable<KPK>(); } }
        public ITable<KPOD> KPOD { get { return this.GetTable<KPOD>(); } }
        public ITable<KRT> KRT { get { return this.GetTable<KRT>(); } }
        public ITable<KS> KS { get { return this.GetTable<KS>(); } }
        public ITable<KSPR> KSPR { get { return this.GetTable<KSPR>(); } }
        public ITable<KU> KU { get { return this.GetTable<KU>(); } }
        public ITable<KUD> KUD { get { return this.GetTable<KUD>(); } }
        public ITable<LAN> LAN { get { return this.GetTable<LAN>(); } }
        public ITable<LG> LG { get { return this.GetTable<LG>(); } }
        public ITable<LS> LS { get { return this.GetTable<LS>(); } }
        public ITable<MA> MA { get { return this.GetTable<MA>(); } }
        public ITable<MAF> MAF { get { return this.GetTable<MAF>(); } }
        public ITable<MAK> MAK { get { return this.GetTable<MAK>(); } }
        public ITable<MARKB> MARKB { get { return this.GetTable<MARKB>(); } }
        public ITable<MEDR> MEDR { get { return this.GetTable<MEDR>(); } }
        public ITable<MEDS> MEDS { get { return this.GetTable<MEDS>(); } }
        public ITable<MEDT> MEDT { get { return this.GetTable<MEDT>(); } }
        public ITable<MOK> MOK { get { return this.GetTable<MOK>(); } }
        public ITable<MP> MP { get { return this.GetTable<MP>(); } }
        public ITable<MTDM> MTDM { get { return this.GetTable<MTDM>(); } }
        public ITable<MTG> MTG { get { return this.GetTable<MTG>(); } }
        public ITable<MTGZ> MTGZ { get { return this.GetTable<MTGZ>(); } }
        public ITable<MTMO> MTMO { get { return this.GetTable<MTMO>(); } }
        public ITable<MTMOA> MTMOA { get { return this.GetTable<MTMOA>(); } }
        public ITable<MTMOP> MTMOP { get { return this.GetTable<MTMOP>(); } }
        public ITable<MTMOT> MTMOT { get { return this.GetTable<MTMOT>(); } }
        public ITable<MTRP> MTRP { get { return this.GetTable<MTRP>(); } }
        public ITable<MTT> MTT { get { return this.GetTable<MTT>(); } }
        public ITable<MTV> MTV { get { return this.GetTable<MTV>(); } }
        public ITable<MTZ> MTZ { get { return this.GetTable<MTZ>(); } }
        public ITable<MVPF> MVPF { get { return this.GetTable<MVPF>(); } }
        public ITable<NA> NA { get { return this.GetTable<NA>(); } }
        public ITable<NAPR> NAPR { get { return this.GetTable<NAPR>(); } }
        public ITable<NC> NC { get { return this.GetTable<NC>(); } }
        public ITable<NKRS> NKRS { get { return this.GetTable<NKRS>(); } }
        public ITable<NPS> NPS { get { return this.GetTable<NPS>(); } }
        public ITable<NR> NR { get { return this.GetTable<NR>(); } }
        public ITable<NRD> NRD { get { return this.GetTable<NRD>(); } }
        public ITable<NRT> NRT { get { return this.GetTable<NRT>(); } }
        public ITable<NRV> NRV { get { return this.GetTable<NRV>(); } }
        public ITable<OB> OB { get { return this.GetTable<OB>(); } }
        public ITable<OBPO> OBPO { get { return this.GetTable<OBPO>(); } }
        public ITable<OBPOP> OBPOP { get { return this.GetTable<OBPOP>(); } }
        public ITable<OBSH> OBSH { get { return this.GetTable<OBSH>(); } }
        public ITable<OBSHK> OBSHK { get { return this.GetTable<OBSHK>(); } }
        public ITable<OBST> OBST { get { return this.GetTable<OBST>(); } }
        public ITable<OBZ> OBZ { get { return this.GetTable<OBZ>(); } }
        public ITable<ODP> ODP { get { return this.GetTable<ODP>(); } }
        public ITable<OK> OK { get { return this.GetTable<OK>(); } }
        public ITable<OM> OM { get { return this.GetTable<OM>(); } }
        public ITable<ORG> ORG { get { return this.GetTable<ORG>(); } }
        public ITable<OSPS> OSPS { get { return this.GetTable<OSPS>(); } }
        public ITable<OTP> OTP { get { return this.GetTable<OTP>(); } }
        public ITable<OTPV> OTPV { get { return this.GetTable<OTPV>(); } }
        public ITable<P> P { get { return this.GetTable<P>(); } }
        public ITable<PAA> PAA { get { return this.GetTable<PAA>(); } }
        public ITable<PAAN> PAAN { get { return this.GetTable<PAAN>(); } }
        public ITable<PAANP> PAANP { get { return this.GetTable<PAANP>(); } }
        public ITable<PAAP> PAAP { get { return this.GetTable<PAAP>(); } }
        public ITable<PB> PB { get { return this.GetTable<PB>(); } }
        public ITable<PBS> PBS { get { return this.GetTable<PBS>(); } }
        public ITable<PD> PD { get { return this.GetTable<PD>(); } }
        public ITable<PDIS> PDIS { get { return this.GetTable<PDIS>(); } }
        public ITable<PDP> PDP { get { return this.GetTable<PDP>(); } }
        public ITable<PE> PE { get { return this.GetTable<PE>(); } }
        public ITable<PEADR> PEADR { get { return this.GetTable<PEADR>(); } }
        public ITable<PEFIO> PEFIO { get { return this.GetTable<PEFIO>(); } }
        public ITable<PEN> PEN { get { return this.GetTable<PEN>(); } }
        public ITable<PER> PER { get { return this.GetTable<PER>(); } }
        public ITable<PESEM> PESEM { get { return this.GetTable<PESEM>(); } }
        public ITable<PKV> PKV { get { return this.GetTable<PKV>(); } }
        public ITable<PKVD> PKVD { get { return this.GetTable<PKVD>(); } }
        public ITable<PKVE> PKVE { get { return this.GetTable<PKVE>(); } }
        public ITable<PKVG> PKVG { get { return this.GetTable<PKVG>(); } }
        public ITable<PL> PL { get { return this.GetTable<PL>(); } }
        public ITable<PM> PM { get { return this.GetTable<PM>(); } }
        public ITable<PMC> PMC { get { return this.GetTable<PMC>(); } }
        public ITable<PMG> PMG { get { return this.GetTable<PMG>(); } }
        public ITable<PNA> PNA { get { return this.GetTable<PNA>(); } }
        public ITable<PND> PND { get { return this.GetTable<PND>(); } }
        public ITable<PNSP> PNSP { get { return this.GetTable<PNSP>(); } }
        public ITable<PO> PO { get { return this.GetTable<PO>(); } }
        public ITable<POBR> POBR { get { return this.GetTable<POBR>(); } }
        public ITable<POD> POD { get { return this.GetTable<POD>(); } }
        public ITable<PORTAL_SETTINGS> PORTAL_SETTINGS { get { return this.GetTable<PORTAL_SETTINGS>(); } }
        public ITable<PP> PP { get { return this.GetTable<PP>(); } }
        public ITable<PPV> PPV { get { return this.GetTable<PPV>(); } }
        public ITable<PPZV> PPZV { get { return this.GetTable<PPZV>(); } }
        public ITable<PPZVV> PPZVV { get { return this.GetTable<PPZVV>(); } }
        public ITable<PROTZ> PROTZ { get { return this.GetTable<PROTZ>(); } }
        public ITable<PRPD> PRPD { get { return this.GetTable<PRPD>(); } }
        public ITable<PRA> PRA { get { return this.GetTable<PRA>(); } }
        public ITable<PRU> PRU { get { return this.GetTable<PRU>(); } }
        public ITable<PRUN> PRUN { get { return this.GetTable<PRUN>(); } }
        public ITable<PRUST> PRUST { get { return this.GetTable<PRUST>(); } }
        public ITable<PRV> PRV { get { return this.GetTable<PRV>(); } }
        public ITable<PS> PS { get { return this.GetTable<PS>(); } }
        public ITable<PSTO> PSTO { get { return this.GetTable<PSTO>(); } }
        public ITable<PUS> PUS { get { return this.GetTable<PUS>(); } }
        public ITable<PUZ> PUZ { get { return this.GetTable<PUZ>(); } }
        public ITable<PVK> PVK { get { return this.GetTable<PVK>(); } }
        public ITable<PVVD> PVVD { get { return this.GetTable<PVVD>(); } }
        public ITable<PY> PY { get { return this.GetTable<PY>(); } }
        public ITable<QT> QT { get { return this.GetTable<QT>(); } }
        public ITable<QUAL> QUAL { get { return this.GetTable<QUAL>(); } }
        public ITable<R> R { get { return this.GetTable<R>(); } }
        public ITable<RA> RA { get { return this.GetTable<RA>(); } }
        public ITable<ROP> ROP { get { return this.GetTable<ROP>(); } }
        public ITable<RSP> RSP { get { return this.GetTable<RSP>(); } }
        public ITable<RSPSG> RSPSG { get { return this.GetTable<RSPSG>(); } }
        public ITable<RSPST> RSPST { get { return this.GetTable<RSPST>(); } }
        public ITable<RT> RT { get { return this.GetTable<RT>(); } }
        public ITable<RZ> RZ { get { return this.GetTable<RZ>(); } }
        public ITable<SDIGPT> SDIGPT { get { return this.GetTable<SDIGPT>(); } }
        public ITable<SDN> SDN { get { return this.GetTable<SDN>(); } }
        public ITable<SDNB> SDNB { get { return this.GetTable<SDNB>(); } }
        public ITable<SEKA> SEKA { get { return this.GetTable<SEKA>(); } }
        public ITable<SEKAP> SEKAP { get { return this.GetTable<SEKAP>(); } }
        public ITable<SEM> SEM { get { return this.GetTable<SEM>(); } }
        public ITable<SF> SF { get { return this.GetTable<SF>(); } }
        public ITable<SFP> SFP { get { return this.GetTable<SFP>(); } }
        public ITable<SFY> SFY { get { return this.GetTable<SFY>(); } }
        public ITable<SG> SG { get { return this.GetTable<SG>(); } }
        public ITable<SGOS> SGOS { get { return this.GetTable<SGOS>(); } }
        public ITable<SGPR> SGPR { get { return this.GetTable<SGPR>(); } }
        public ITable<SGPRZ> SGPRZ { get { return this.GetTable<SGPRZ>(); } }
        public ITable<SGR> SGR { get { return this.GetTable<SGR>(); } }
        public ITable<SGUP> SGUP { get { return this.GetTable<SGUP>(); } }
        public ITable<SH> SH { get { return this.GetTable<SH>(); } }
        public ITable<SHF> SHF { get { return this.GetTable<SHF>(); } }
        public ITable<SHFN> SHFN { get { return this.GetTable<SHFN>(); } }
        public ITable<SHK> SHK { get { return this.GetTable<SHK>(); } }
        public ITable<SHR> SHR { get { return this.GetTable<SHR>(); } }
        public ITable<SHRD> SHRD { get { return this.GetTable<SHRD>(); } }
        public ITable<SHRP> SHRP { get { return this.GetTable<SHRP>(); } }
        public ITable<STOBR> STOBR { get { return this.GetTable<STOBR>(); } }
        public ITable<SK> SK { get { return this.GetTable<SK>(); } }
        public ITable<SL> SL { get { return this.GetTable<SL>(); } }
        public ITable<SLDOC> SLDOC { get { return this.GetTable<SLDOC>(); } }
        public ITable<SLFL> SLFL { get { return this.GetTable<SLFL>(); } }
        public ITable<SLK> SLK { get { return this.GetTable<SLK>(); } }
        public ITable<SLKG> SLKG { get { return this.GetTable<SLKG>(); } }
        public ITable<SLO> SLO { get { return this.GetTable<SLO>(); } }
        public ITable<SLYE> SLYE { get { return this.GetTable<SLYE>(); } }
        public ITable<SLYEI> SLYEI { get { return this.GetTable<SLYEI>(); } }
        public ITable<SLYEIT> SLYEIT { get { return this.GetTable<SLYEIT>(); } }
        public ITable<SNAC> SNAC { get { return this.GetTable<SNAC>(); } }
        public ITable<SNK> SNK { get { return this.GetTable<SNK>(); } }
        public ITable<SNKD> SNKD { get { return this.GetTable<SNKD>(); } }
        public ITable<SO> SO { get { return this.GetTable<SO>(); } }
        public ITable<SOB> SOB { get { return this.GetTable<SOB>(); } }
        public ITable<SP> SP { get { return this.GetTable<SP>(); } }
        public ITable<SPAB> SPAB { get { return this.GetTable<SPAB>(); } }
        public ITable<SPABC> SPABC { get { return this.GetTable<SPABC>(); } }
        public ITable<SPABD> SPABD { get { return this.GetTable<SPABD>(); } }
        public ITable<SPABE> SPABE { get { return this.GetTable<SPABE>(); } }
        public ITable<SPABK> SPABK { get { return this.GetTable<SPABK>(); } }
        public ITable<SPABP> SPABP { get { return this.GetTable<SPABP>(); } }
        public ITable<SPABS> SPABS { get { return this.GetTable<SPABS>(); } }
        public ITable<SPABV> SPABV { get { return this.GetTable<SPABV>(); } }
        public ITable<SPABVI> SPABVI { get { return this.GetTable<SPABVI>(); } }
        public ITable<SPABVIZ> SPABVIZ { get { return this.GetTable<SPABVIZ>(); } }
        public ITable<SPABZ> SPABZ { get { return this.GetTable<SPABZ>(); } }
        public ITable<SPAN> SPAN { get { return this.GetTable<SPAN>(); } }
        public ITable<SPC> SPC { get { return this.GetTable<SPC>(); } }
        public ITable<SPEK> SPEK { get { return this.GetTable<SPEK>(); } }
        public ITable<SPKR> SPKR { get { return this.GetTable<SPKR>(); } }
        public ITable<SPO> SPO { get { return this.GetTable<SPO>(); } }
        public ITable<SPOB> SPOB { get { return this.GetTable<SPOB>(); } }
        public ITable<SPOL> SPOL { get { return this.GetTable<SPOL>(); } }
        public ITable<SPOLI> SPOLI { get { return this.GetTable<SPOLI>(); } }
        public ITable<SPOM> SPOM { get { return this.GetTable<SPOM>(); } }
        public ITable<SPR> SPR { get { return this.GetTable<SPR>(); } }
        public ITable<SPRA> SPRA { get { return this.GetTable<SPRA>(); } }
        public ITable<SPSP> SPSP { get { return this.GetTable<SPSP>(); } }
        public ITable<SR> SR { get { return this.GetTable<SR>(); } }
        public ITable<SRS> SRS { get { return this.GetTable<SRS>(); } }
        public ITable<SRV> SRV { get { return this.GetTable<SRV>(); } }
        public ITable<SRVD> SRVD { get { return this.GetTable<SRVD>(); } }
        public ITable<SST> SST { get { return this.GetTable<SST>(); } }
        public ITable<ST> ST { get { return this.GetTable<ST>(); } }
        public ITable<STAG> STAG { get { return this.GetTable<STAG>(); } }
        public ITable<STAL> STAL { get { return this.GetTable<STAL>(); } }
        public ITable<STALI> STALI { get { return this.GetTable<STALI>(); } }
        public ITable<STAT> STAT { get { return this.GetTable<STAT>(); } }
        public ITable<STATD> STATD { get { return this.GetTable<STATD>(); } }
        public ITable<STBL> STBL { get { return this.GetTable<STBL>(); } }
        public ITable<STD> STD { get { return this.GetTable<STD>(); } }
        public ITable<STDO> STDO { get { return this.GetTable<STDO>(); } }
        public ITable<STDOCS> STDOCS { get { return this.GetTable<STDOCS>(); } }
        public ITable<STEGND> STEGND { get { return this.GetTable<STEGND>(); } }
        public ITable<STI> STI { get { return this.GetTable<STI>(); } }
        public ITable<STL> STL { get { return this.GetTable<STL>(); } }
        public ITable<STLD> STLD { get { return this.GetTable<STLD>(); } }
        public ITable<STMAK> STMAK { get { return this.GetTable<STMAK>(); } }
        public ITable<STML> STML { get { return this.GetTable<STML>(); } }
        public ITable<STO> STO { get { return this.GetTable<STO>(); } }
        public ITable<STPR> STPR { get { return this.GetTable<STPR>(); } }
        public ITable<STPRK> STPRK { get { return this.GetTable<STPRK>(); } }
        public ITable<STPRZ> STPRZ { get { return this.GetTable<STPRZ>(); } }
        public ITable<STPZ> STPZ { get { return this.GetTable<STPZ>(); } }
        public ITable<STSB> STSB { get { return this.GetTable<STSB>(); } }
        public ITable<STTIP> STTIP { get { return this.GetTable<STTIP>(); } }
        public ITable<STTR> STTR { get { return this.GetTable<STTR>(); } }
        public ITable<STUS> STUS { get { return this.GetTable<STUS>(); } }
        public ITable<STUSA> STUSA { get { return this.GetTable<STUSA>(); } }
        public ITable<STUSP> STUSP { get { return this.GetTable<STUSP>(); } }
        public ITable<STUSV> STUSV { get { return this.GetTable<STUSV>(); } }
        public ITable<STUSVST> STUSVST { get { return this.GetTable<STUSVST>(); } }
        public ITable<STV> STV { get { return this.GetTable<STV>(); } }
        public ITable<STZ> STZ { get { return this.GetTable<STZ>(); } }
        public ITable<SVAK> SVAK { get { return this.GetTable<SVAK>(); } }
        public ITable<SY> SY { get { return this.GetTable<SY>(); } }
        public ITable<SZ> SZ { get { return this.GetTable<SZ>(); } }
        public ITable<TARS> TARS { get { return this.GetTable<TARS>(); } }
        public ITable<TARST> TARST { get { return this.GetTable<TARST>(); } }
        public ITable<TBL_MIGRATION> TBL_MIGRATION { get { return this.GetTable<TBL_MIGRATION>(); } }
        public ITable<TDDO> TDDO { get { return this.GetTable<TDDO>(); } }
        public ITable<TDO> TDO { get { return this.GetTable<TDO>(); } }
        public ITable<TEM> TEM { get { return this.GetTable<TEM>(); } }
        public ITable<TGUP> TGUP { get { return this.GetTable<TGUP>(); } }
        public ITable<TMIGRATION> TMIGRATION { get { return this.GetTable<TMIGRATION>(); } }
        public ITable<TP> TP { get { return this.GetTable<TP>(); } }
        public ITable<TPN> TPN { get { return this.GetTable<TPN>(); } }
        public ITable<TPSZ> TPSZ { get { return this.GetTable<TPSZ>(); } }
        public ITable<TRNT> TRNT { get { return this.GetTable<TRNT>(); } }
        public ITable<TSG> TSG { get { return this.GetTable<TSG>(); } }
        public ITable<TSO> TSO { get { return this.GetTable<TSO>(); } }
        public ITable<U> U { get { return this.GetTable<U>(); } }
        public ITable<UCGN> UCGN { get { return this.GetTable<UCGN>(); } }
        public ITable<UCGNS> UCGNS { get { return this.GetTable<UCGNS>(); } }
        public ITable<UCHZ> UCHZ { get { return this.GetTable<UCHZ>(); } }
        public ITable<UCSN> UCSN { get { return this.GetTable<UCSN>(); } }
        public ITable<UCX> UCX { get { return this.GetTable<UCX>(); } }
        public ITable<UCXG> UCXG { get { return this.GetTable<UCXG>(); } }
        public ITable<UG> UG { get { return this.GetTable<UG>(); } }
        public ITable<UGT> UGT { get { return this.GetTable<UGT>(); } }
        public ITable<UO> UO { get { return this.GetTable<UO>(); } }
        public ITable<UOSV> UOSV { get { return this.GetTable<UOSV>(); } }
        public ITable<UOUP> UOUP { get { return this.GetTable<UOUP>(); } }
        public ITable<UPCM> UPCM { get { return this.GetTable<UPCM>(); } }
        public ITable<UPNR> UPNR { get { return this.GetTable<UPNR>(); } }
        public ITable<UPST> UPST { get { return this.GetTable<UPST>(); } }
        public ITable<UPSTD> UPSTD { get { return this.GetTable<UPSTD>(); } }
        public ITable<UPSTF> UPSTF { get { return this.GetTable<UPSTF>(); } }
        public ITable<UPSTK> UPSTK { get { return this.GetTable<UPSTK>(); } }
        public ITable<UPSTN> UPSTN { get { return this.GetTable<UPSTN>(); } }
        public ITable<UPSTS> UPSTS { get { return this.GetTable<UPSTS>(); } }
        public ITable<UPSTU> UPSTU { get { return this.GetTable<UPSTU>(); } }
        public ITable<UPSTZ> UPSTZ { get { return this.GetTable<UPSTZ>(); } }
        public ITable<UPTUP> UPTUP { get { return this.GetTable<UPTUP>(); } }
        public ITable<US> US { get { return this.GetTable<US>(); } }
        public ITable<USERS> USERS { get { return this.GetTable<USERS>(); } }
        public ITable<USERS_AUTH_FAIL> USERS_AUTH_FAIL { get { return this.GetTable<USERS_AUTH_FAIL>(); } }
        public ITable<USERS_HISTORY> USERS_HISTORY { get { return this.GetTable<USERS_HISTORY>(); } }
        public ITable<USTEM> USTEM { get { return this.GetTable<USTEM>(); } }
        public ITable<USTEMP> USTEMP { get { return this.GetTable<USTEMP>(); } }
        public ITable<USUP> USUP { get { return this.GetTable<USUP>(); } }
        public ITable<UTL> UTL { get { return this.GetTable<UTL>(); } }
        public ITable<UUP> UUP { get { return this.GetTable<UUP>(); } }
        public ITable<VD> VD { get { return this.GetTable<VD>(); } }
        public ITable<VDN> VDN { get { return this.GetTable<VDN>(); } }
        public ITable<VDNS> VDNS { get { return this.GetTable<VDNS>(); } }
        public ITable<VIPZ> VIPZ { get { return this.GetTable<VIPZ>(); } }
        public ITable<VKB> VKB { get { return this.GetTable<VKB>(); } }
        public ITable<VMP> VMP { get { return this.GetTable<VMP>(); } }
        public ITable<VMPM> VMPM { get { return this.GetTable<VMPM>(); } }
        public ITable<VMPP> VMPP { get { return this.GetTable<VMPP>(); } }
        public ITable<VMPV> VMPV { get { return this.GetTable<VMPV>(); } }
        public ITable<VMPVF> VMPVF { get { return this.GetTable<VMPVF>(); } }
        public ITable<VN> VN { get { return this.GetTable<VN>(); } }
        public ITable<VOEN> VOEN { get { return this.GetTable<VOEN>(); } }
        public ITable<VSPR> VSPR { get { return this.GetTable<VSPR>(); } }
        public ITable<VPR> VPR { get { return this.GetTable<VPR>(); } }
        public ITable<VPRV> VPRV { get { return this.GetTable<VPRV>(); } }
        public ITable<VRN> VRN { get { return this.GetTable<VRN>(); } }
        public ITable<VRNA> VRNA { get { return this.GetTable<VRNA>(); } }
        public ITable<VRNAR> VRNAR { get { return this.GetTable<VRNAR>(); } }
        public ITable<VVMP> VVMP { get { return this.GetTable<VVMP>(); } }
        public ITable<VVN> VVN { get { return this.GetTable<VVN>(); } }
        public ITable<VVNPBK> VVNPBK { get { return this.GetTable<VVNPBK>(); } }
        public ITable<VVNPD> VVNPD { get { return this.GetTable<VVNPD>(); } }
        public ITable<VVNPF> VVNPF { get { return this.GetTable<VVNPF>(); } }
        public ITable<VVNPP> VVNPP { get { return this.GetTable<VVNPP>(); } }
        public ITable<VVNPS> VVNPS { get { return this.GetTable<VVNPS>(); } }
        public ITable<VVNPV> VVNPV { get { return this.GetTable<VVNPV>(); } }
        public ITable<VVNV> VVNV { get { return this.GetTable<VVNV>(); } }
        public ITable<VZV> VZV { get { return this.GetTable<VZV>(); } }
        public ITable<W> W { get { return this.GetTable<W>(); } }
        public ITable<WK> WK { get { return this.GetTable<WK>(); } }
        public ITable<XPD> XPD { get { return this.GetTable<XPD>(); } }
        public ITable<XPDG> XPDG { get { return this.GetTable<XPDG>(); } }
        public ITable<XPDS> XPDS { get { return this.GetTable<XPDS>(); } }
        public ITable<Y> Y { get { return this.GetTable<Y>(); } }
        public ITable<YK> YK { get { return this.GetTable<YK>(); } }
        public ITable<ZFST> ZFST { get { return this.GetTable<ZFST>(); } }
        public ITable<ZPD> ZPD { get { return this.GetTable<ZPD>(); } }
        public ITable<ZPER> ZPER { get { return this.GetTable<ZPER>(); } }
        public ITable<ZPM> ZPM { get { return this.GetTable<ZPM>(); } }
        public ITable<ZS> ZS { get { return this.GetTable<ZS>(); } }
        public ITable<ZSG> ZSG { get { return this.GetTable<ZSG>(); } }
        public ITable<ZSP> ZSP { get { return this.GetTable<ZSP>(); } }
        public ITable<ZSV> ZSV { get { return this.GetTable<ZSV>(); } }
        public ITable<ZUZ> ZUZ { get { return this.GetTable<ZUZ>(); } }
        public ITable<ZZ> ZZ { get { return this.GetTable<ZZ>(); } }



        public BDETALONDB(string connBD) : base(GetDataProvider(), GetConnection(connBD)) { InitDataContext(); }

        partial void InitDataContext();
    }

    [Table("BRAND")]
    public partial class BRAND
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int BRAND1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string BRAND2 { get; set; } // varchar(100)
    }

    [Table("CLIENT")]
    public partial class CLIENT
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int CLIENT1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string CLIENT2 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string CLIENT3 { get; set; } // varchar(20)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string CLIENT5 { get; set; } // varchar(200)
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string CLIENT6 { get; set; } // varchar(20)
        [Column(DataType = DataType.NVarChar, Length = 100), Nullable]
        public string CLIENT7 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public string CLIENT8 { get; set; } // varchar(1)
    }

    [Table("COMPLECT")]
    public partial class COMPLECT
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int COMPLECT1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string COMPLECT2 { get; set; } // varchar(100)
    }

    [Table("DAMAG")]
    public partial class DAMAG
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int DAMAG1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 300), NotNull]
        public string DAMAG2 { get; set; } // varchar(300)
    }

    [Table("DEVICE")]
    public partial class DEVICE
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int DEVICE1 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime DEVICE2 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string DEVICE3 { get; set; } // varchar(20)
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string DEVICE4 { get; set; } // varchar(20)
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string DEVICE5 { get; set; } // varchar(50)
        [Column(DataType = DataType.Blob, Length = int.MaxValue), Nullable]
        public byte[] DEVICE6 { get; set; } // blob
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string DEVICE7 { get; set; } // varchar(100)
        [Column(DataType = DataType.Blob, Length = int.MaxValue), Nullable]
        public byte[] DEVICE8 { get; set; } // blob
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int DEVICE9 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int DEVICE10 { get; set; } // integer
    }

    [Table("DEVICE_STATUS")]
    public partial class DEVICE_STATUS
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int DEVICE_STATUS1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string DEVICE_STATUS2 { get; set; } // varchar(20)
        [Column(DataType = DataType.NVarChar, Length = 1000), NotNull]
        public string DEVICE_STATUS3 { get; set; } // varchar(1000)
    }

    [Table("DEVICE_STSHST")]
    public partial class DEVICE_STSHST
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int DEVICE_STSHST1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int DEVICE_STSHST2 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime DEVICE_STSHST3 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int DEVICE_STSHST4 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string DEVICE_STSHST5 { get; set; } // varchar(20)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime DEVICE_STSHST6 { get; set; } // timestamp
    }

    [Table("DEVICE_TP")]
    public partial class DEVICE_TP
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int DEVICE_TP1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string DEVICE_TP2 { get; set; } // varchar(200)
    }

    [Table("LABEL")]
    public partial class LABEL
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int LABEL1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string LABEL2 { get; set; } // varchar(100)
    }

    [Table("MESSAGES")]
    public partial class MESSAGES
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int MESSAGES1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int MESSAGES2 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int MESSAGES3 { get; set; } // integer
        [Column(DataType = DataType.Blob, Length = int.MaxValue), Nullable]
        public byte[] MESSAGES4 { get; set; } // blob
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime MESSAGES5 { get; set; } // timestamp
    }

    [Table("SPRAV_WORK")]
    public partial class SPRAV_WORK
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int SPRAV_WORK1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string SPRAV_WORK2 { get; set; } // varchar(200)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SPRAV_WORK3 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SPRAV_WORK4 { get; set; } // varchar(1)
    }

    [Table("UUSER")]
    public partial class UUSER
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int UUSER1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string UUSER2 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string UUSER3 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string UUSER4 { get; set; } // varchar(200)
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int UUSER5 { get; set; } // varchar(20)
        [Column(DataType = DataType.Blob, Length = int.MaxValue), Nullable]
        public byte[] UUSER6 { get; set; } // blob
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int UUSER7 { get; set; } // integer
    }

    [Table("WWORK")]
    public partial class WWORK
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int WWORK1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int WWORK2 { get; set; } // integer
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double WWORK3 { get; set; } // double precision
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int WWORK4 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int WWORK5 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string WWORK6 { get; set; } // varchar(20)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime WWORK7 { get; set; } // timestamp
    }


    [Table("A")]
    public partial class A
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int A1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string A2 { get; set; } // varchar(15)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short A3 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string A4 { get; set; } // varchar(50)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? A5 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int A6 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short A8 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short A9 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime A11 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int A12 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string A14 { get; set; } // varchar(50)
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string A15 { get; set; } // varchar(10)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short A16 { get; set; } // smallint
    }

    [Table("AB")]
    public partial class AB
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int AB1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 35), NotNull]
        public string AB2 { get; set; } // varchar(35)
        [Column(DataType = DataType.NVarChar, Length = 35), NotNull]
        public string AB3 { get; set; } // varchar(35)
        [Column(DataType = DataType.NVarChar, Length = 35), NotNull]
        public string AB4 { get; set; } // varchar(35)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short AB5 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short AB6 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime AB7 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 75), NotNull]
        public string AB8 { get; set; } // varchar(75)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int AB9 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int AB10 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short AB11 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short AB12 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 6), NotNull]
        public string AB20 { get; set; } // varchar(6)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int AB21 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string AB22 { get; set; } // varchar(150)
        [Column(DataType = DataType.NVarChar, Length = 5), NotNull]
        public string AB24 { get; set; } // varchar(5)
        [Column(DataType = DataType.NVarChar, Length = 5), NotNull]
        public string AB25 { get; set; } // varchar(5)
        [Column(DataType = DataType.NVarChar, Length = 30), NotNull]
        public string AB26 { get; set; } // varchar(30)
        [Column(DataType = DataType.NVarChar, Length = 30), NotNull]
        public string AB27 { get; set; } // varchar(30)
        [Column(DataType = DataType.NVarChar, Length = 35), NotNull]
        public string AB28 { get; set; } // varchar(35)
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string AB35 { get; set; } // varchar(15)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short AB36 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 5), NotNull]
        public string AB37 { get; set; } // varchar(5)
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string AB38 { get; set; } // varchar(25)
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string AB39 { get; set; } // varchar(150)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? AB40 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string AB45 { get; set; } // varchar(50)
        [Column(DataType = DataType.NVarChar, Length = 75), NotNull]
        public string AB46 { get; set; } // varchar(75)
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string AB47 { get; set; } // varchar(25)
        [Column(DataType = DataType.NVarChar, Length = 30), NotNull]
        public string AB48 { get; set; } // varchar(30)
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string AB49 { get; set; } // varchar(50)
        [Column(DataType = DataType.NVarChar, Length = 75), NotNull]
        public string AB50 { get; set; } // varchar(75)
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string AB51 { get; set; } // varchar(25)
        [Column(DataType = DataType.NVarChar, Length = 30), NotNull]
        public string AB52 { get; set; } // varchar(30)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int AB60 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short AB69 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short AB71 { get; set; } // smallint
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double AB72 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double AB74 { get; set; } // double precision
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short AB90 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 75), NotNull]
        public string AB91 { get; set; } // varchar(75)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? AB92 { get; set; } // timestamp
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short AB95 { get; set; } // smallint
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double AB110 { get; set; } // double precision
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short AB111 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short AB112 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short AB113 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short AB114 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime AB116 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int AB117 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short AB130 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short AB136 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short AB140 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short AB141 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short AB142 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short AB143 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short AB144 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short AB145 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short AB146 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short AB147 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short AB148 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string AB149 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string AB150 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string AB151 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string AB152 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 1500), NotNull]
        public string AB153 { get; set; } // varchar(1500)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string AB154 { get; set; } // varchar(100)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int AB155 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int AB157 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short AB158 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short AB161 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short AB162 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short AB164 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short AB165 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short AB166 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? AB167 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? AB168 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? AB169 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? AB170 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? AB171 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int AB172 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 6), NotNull]
        public string AB173 { get; set; } // varchar(6)
        [Column(DataType = DataType.NVarChar, Length = 75), NotNull]
        public string AB174 { get; set; } // varchar(75)
        [Column(DataType = DataType.NVarChar, Length = 5), NotNull]
        public string AB175 { get; set; } // varchar(5)
        [Column(DataType = DataType.NVarChar, Length = 5), NotNull]
        public string AB176 { get; set; } // varchar(5)
        [Column(DataType = DataType.NVarChar, Length = 5), NotNull]
        public string AB177 { get; set; } // varchar(5)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short AB178 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short AB179 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short AB180 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 35), NotNull]
        public string AB181 { get; set; } // varchar(35)
        [Column(DataType = DataType.NVarChar, Length = 250), NotNull]
        public string AB182 { get; set; } // varchar(250)
        [Column(DataType = DataType.NVarChar, Length = 8), NotNull]
        public string AB23 { get; set; } // varchar(8)
    }

    [Table("ABCD")]
    public partial class ABCD
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int ABCD1 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABCD2 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string ABCD3 { get; set; } // varchar(200)
    }

    [Table("ABCDZ")]
    public partial class ABCDZ
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int ABCDZ1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int ABCDZ2 { get; set; } // integer
    }

    [Table("ABCL")]
    public partial class ABCL
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int ABCL1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABCL2 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 350), NotNull]
        public string ABCL3 { get; set; } // varchar(350)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? ABCL4 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABCL5 { get; set; } // integer
    }

    [Table("ABCT")]
    public partial class ABCT
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int ABCT1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABCT2 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? ABCT3 { get; set; } // timestamp
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double ABCT4 { get; set; } // double precision
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABCT5 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? ABCT6 { get; set; } // timestamp
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double ABCT7 { get; set; } // double precision
    }

    [Table("ABD")]
    public partial class ABD
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int ABD1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABD2 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABD3 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABD4 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABD5 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABD6 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABD7 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABD8 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string ABD9 { get; set; } // varchar(25)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABD10 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? ABD11 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? ABD12 { get; set; } // timestamp
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABD13 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string ABD14 { get; set; } // varchar(15)
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string ABD15 { get; set; } // varchar(15)
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime ABD16 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABD17 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABD18 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABD19 { get; set; } // integer
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double ABD20 { get; set; } // double precision
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABD22 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABD23 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABD24 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABD25 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABD26 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABD27 { get; set; } // smallint
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double ABD28 { get; set; } // double precision
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABD30 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABD33 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABD34 { get; set; } // smallint
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double ABD35 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double ABD36 { get; set; } // double precision
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string ABD44 { get; set; } // varchar(15)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? ABD45 { get; set; } // timestamp
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABD46 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string ABD47 { get; set; } // varchar(100)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABD48 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABD51 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABD52 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABD54 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABD55 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABD56 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABD57 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABD58 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABD62 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABD63 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABD64 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string ABD65 { get; set; } // varchar(100)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABD66 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABD67 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 5), NotNull]
        public string ABD68 { get; set; } // varchar(5)
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string ABD69 { get; set; } // varchar(10)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? ABD70 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string ABD71 { get; set; } // varchar(100)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? ABD72 { get; set; } // timestamp
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABD73 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABD74 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABD75 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABD77 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? ABD78 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string ABD79 { get; set; } // varchar(25)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABD80 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABD81 { get; set; } // integer
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double ABD82 { get; set; } // double precision
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABD83 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABD84 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABD85 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABD21 { get; set; } // integer
    }

    [Table("ABDE")]
    public partial class ABDE
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int ABDE1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABDE2 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABDE3 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string ABDE4 { get; set; } // varchar(10)
        [Column(DataType = DataType.NVarChar, Length = 30), NotNull]
        public string ABDE5 { get; set; } // varchar(30)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? ABDE6 { get; set; } // timestamp
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double ABDE7 { get; set; } // double precision
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABDE8 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABDE9 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABDE10 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string ABDE11 { get; set; } // varchar(15)
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double ABDE12 { get; set; } // double precision
    }

    [Table("ABDPB")]
    public partial class ABDPB
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int ABDPB1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int ABDPB2 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string ABDPB3 { get; set; } // varchar(15)
    }

    [Table("ABFI")]
    public partial class ABFI
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int ABFI1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string ABFI2 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string ABFI3 { get; set; } // varchar(15)
    }

    [Table("ABFIZ")]
    public partial class ABFIZ
    {
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABFIZ1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABFIZ2 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABFIZ3 { get; set; } // integer
    }

    [Table("ABI")]
    public partial class ABI
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int ABI1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 35), NotNull]
        public string ABI2 { get; set; } // varchar(35)
        [Column(DataType = DataType.NVarChar, Length = 35), NotNull]
        public string ABI3 { get; set; } // varchar(35)
        [Column(DataType = DataType.NVarChar, Length = 35), NotNull]
        public string ABI4 { get; set; } // varchar(35)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABI5 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABI6 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime ABI7 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 75), NotNull]
        public string ABI8 { get; set; } // varchar(75)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABI9 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABI10 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABI11 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 6), NotNull]
        public string ABI12 { get; set; } // varchar(6)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABI13 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string ABI14 { get; set; } // varchar(150)
        [Column(DataType = DataType.NVarChar, Length = 8), NotNull]
        public string ABI15 { get; set; } // varchar(8)
        [Column(DataType = DataType.NVarChar, Length = 5), NotNull]
        public string ABI16 { get; set; } // varchar(5)
        [Column(DataType = DataType.NVarChar, Length = 5), NotNull]
        public string ABI17 { get; set; } // varchar(5)
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string ABI18 { get; set; } // varchar(50)
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string ABI19 { get; set; } // varchar(50)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string ABI20 { get; set; } // varchar(100)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABI21 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 250), NotNull]
        public string ABI22 { get; set; } // varchar(250)
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string ABI23 { get; set; } // varchar(15)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABI24 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 5), NotNull]
        public string ABI25 { get; set; } // varchar(5)
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string ABI26 { get; set; } // varchar(25)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string ABI27 { get; set; } // varchar(100)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? ABI28 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string ABI29 { get; set; } // varchar(50)
        [Column(DataType = DataType.NVarChar, Length = 75), NotNull]
        public string ABI30 { get; set; } // varchar(75)
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string ABI31 { get; set; } // varchar(25)
        [Column(DataType = DataType.NVarChar, Length = 30), NotNull]
        public string ABI32 { get; set; } // varchar(30)
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string ABI33 { get; set; } // varchar(50)
        [Column(DataType = DataType.NVarChar, Length = 75), NotNull]
        public string ABI34 { get; set; } // varchar(75)
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string ABI35 { get; set; } // varchar(25)
        [Column(DataType = DataType.NVarChar, Length = 30), NotNull]
        public string ABI36 { get; set; } // varchar(30)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABI37 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABI38 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? ABI39 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABI40 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABI41 { get; set; } // integer
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double ABI42 { get; set; } // double precision
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABI43 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABI44 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime ABI45 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABI46 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABI47 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABI48 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABI49 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABI50 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string ABI51 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string ABI52 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string ABI53 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string ABI54 { get; set; } // varchar(100)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? ABI55 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? ABI56 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? ABI57 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? ABI58 { get; set; } // timestamp
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABI59 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABI60 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABI61 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABI62 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABI63 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABI64 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABI65 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABI66 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABI67 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABI68 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? ABI69 { get; set; } // timestamp
    }

    [Table("ABICB")]
    public partial class ABICB
    {
        [Column(DataType = DataType.Double, Length = 8), PrimaryKey(0), NotNull]
        public double ABICB1 { get; set; } // double precision
        [Column(DataType = DataType.Int16, Length = 2), PrimaryKey(1), NotNull]
        public short ABICB2 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABICB3 { get; set; } // integer
    }

    [Table("ABID")]
    public partial class ABID
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int ABID1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABID2 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABID3 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABID4 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABID5 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABID6 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABID7 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABID8 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string ABID9 { get; set; } // varchar(25)
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime ABID10 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? ABID11 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABID12 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABID13 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime ABID14 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABID15 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABID16 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string ABID17 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string ABID18 { get; set; } // varchar(25)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABID19 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? ABID20 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABID21 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABID22 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABID23 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABID24 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABID25 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABID26 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABID27 { get; set; } // integer
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double ABID28 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double ABID29 { get; set; } // double precision
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? ABID30 { get; set; } // timestamp
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABID31 { get; set; } // smallint
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double ABID32 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double ABID33 { get; set; } // double precision
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABID34 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string ABID35 { get; set; } // varchar(15)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? ABID36 { get; set; } // timestamp
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABID37 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string ABID38 { get; set; } // varchar(100)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABID39 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABID40 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABID41 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABID42 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABID43 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABID44 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABID45 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABID46 { get; set; } // integer
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double ABID47 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double ABID48 { get; set; } // double precision
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABID49 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime ABID50 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABID51 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABID52 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABID53 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string ABID54 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string ABID55 { get; set; } // varchar(100)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? ABID56 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? ABID57 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 1000), NotNull]
        public string ABID58 { get; set; } // varchar(1000)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string ABID59 { get; set; } // varchar(100)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? ABID60 { get; set; } // timestamp
    }

    [Table("ABIDE")]
    public partial class ABIDE
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int ABIDE1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABIDE2 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABIDE3 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string ABIDE4 { get; set; } // varchar(20)
        [Column(DataType = DataType.NVarChar, Length = 30), NotNull]
        public string ABIDE5 { get; set; } // varchar(30)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? ABIDE6 { get; set; } // timestamp
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double ABIDE7 { get; set; } // double precision
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABIDE8 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABIDE9 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABIDE10 { get; set; } // smallint
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double ABIDE11 { get; set; } // double precision
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime ABIDE12 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime ABIDE13 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABIDE14 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABIDE15 { get; set; } // integer
    }

    [Table("ABIDKOF")]
    public partial class ABIDKOF
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int ABIDKOF1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int ABIDKOF2 { get; set; } // integer
    }

    [Table("ABIKOF")]
    public partial class ABIKOF
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int ABIKOF1 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABIKOF2 { get; set; } // smallint
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double ABIKOF3 { get; set; } // double precision
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string ABIKOF4 { get; set; } // varchar(50)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABIKOF5 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABIKOF6 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string ABIKOF7 { get; set; } // varchar(20)
    }

    [Table("ABIKOFS")]
    public partial class ABIKOFS
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int ABIKOFS1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int ABIKOFS2 { get; set; } // integer
    }

    [Table("ABIN")]
    public partial class ABIN
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int ABIN1 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABIN2 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABIN3 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABIN4 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABIN5 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABIN6 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABIN7 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABIN8 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABIN9 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string ABIN10 { get; set; } // varchar(50)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABIN11 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABIN12 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABIN13 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABIN14 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABIN15 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string ABIN16 { get; set; } // varchar(50)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABIN17 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABIN18 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABIN19 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABIN20 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABIN21 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABIN22 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABIN23 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABIN24 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABIN25 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string ABIN26 { get; set; } // varchar(50)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABIN27 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABIN28 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABIN29 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABIN30 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABIN31 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABIN32 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string ABIN33 { get; set; } // varchar(50)
    }

    [Table("ABISET")]
    public partial class ABISET
    {
        [Column(DataType = DataType.Int16, Length = 2), PrimaryKey, NotNull]
        public short ABISET1 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABISET2 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABISET3 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 45), NotNull]
        public string ABISET4 { get; set; } // varchar(45)
        [Column(DataType = DataType.NVarChar, Length = 90), NotNull]
        public string ABISET7 { get; set; } // varchar(90)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABISET5 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABISET6 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string ABISET8 { get; set; } // varchar(200)
    }

    [Table("ABISH")]
    public partial class ABISH
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int ABISH1 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), PrimaryKey(1), NotNull]
        public short ABISH2 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABISH3 { get; set; } // smallint
    }

    [Table("ABISS")]
    public partial class ABISS
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int ABISS1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 35), NotNull]
        public string ABISS2 { get; set; } // varchar(35)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABISS3 { get; set; } // integer
    }

    [Table("ABISSC")]
    public partial class ABISSC
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int ABISSC1 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), PrimaryKey(1), NotNull]
        public short ABISSC2 { get; set; } // smallint
    }

    [Table("ABIST")]
    public partial class ABIST
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int ABIST1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string ABIST2 { get; set; } // varchar(150)
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string ABIST3 { get; set; } // varchar(50)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABIST4 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABIST5 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABIST6 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABIST7 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABIST8 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABIST9 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABIST10 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABIST11 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABIST12 { get; set; } // integer
    }

    [Table("ABISTL")]
    public partial class ABISTL
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int ABISTL1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABISTL2 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABISTL3 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABISTL4 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABISTL5 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime ABISTL6 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 300), NotNull]
        public string ABISTL7 { get; set; } // varchar(300)
    }

    [Table("ABISZ")]
    public partial class ABISZ
    {
        [Column(DataType = DataType.Int16, Length = 2), PrimaryKey(0), NotNull]
        public short ABISZ1 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), PrimaryKey(1), NotNull]
        public short ABISZ2 { get; set; } // smallint
    }

    [Table("ABTMP")]
    public partial class ABTMP
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int ABTMP1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string ABTMP2 { get; set; } // varchar(100)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABTMP3 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABTMP4 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? ABTMP5 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? ABTMP6 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABTMP7 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABTMP8 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string ABTMP9 { get; set; } // varchar(10)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABTMP10 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABTMP11 { get; set; } // smallint
    }

    [Table("ABTMPC")]
    public partial class ABTMPC
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int ABTMPC1 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), PrimaryKey(1), NotNull]
        public short ABTMPC2 { get; set; } // smallint
    }

    [Table("ABTMPCI")]
    public partial class ABTMPCI
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int ABTMPCI1 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), PrimaryKey(1), NotNull]
        public short ABTMPCI2 { get; set; } // smallint
    }

    [Table("ABTMPI")]
    public partial class ABTMPI
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int ABTMPI1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 30), NotNull]
        public string ABTMPI2 { get; set; } // varchar(30)
        [Column(DataType = DataType.NVarChar, Length = 30), NotNull]
        public string ABTMPI3 { get; set; } // varchar(30)
        [Column(DataType = DataType.NVarChar, Length = 30), NotNull]
        public string ABTMPI4 { get; set; } // varchar(30)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABTMPI5 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABTMPI6 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? ABTMPI7 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? ABTMPI8 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string ABTMPI9 { get; set; } // varchar(10)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABTMPI10 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ABTMPI11 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ABTMPI12 { get; set; } // integer
    }

    [Table("AD")]
    public partial class AD
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int AD1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int AD2 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string AD3 { get; set; } // varchar(25)
        [Column(DataType = DataType.Text, Length = int.MaxValue), Nullable]
        public string AD4 { get; set; } // blob sub_type 1
    }

    [Table("ADP")]
    public partial class ADP
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int ADP1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string ADP2 { get; set; } // varchar(100)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ADP3 { get; set; } // smallint
    }

    [Table("ADZ")]
    public partial class ADZ
    {
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ADZ1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ADZ2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ADZ3 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime ADZ4 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ADZ5 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 350), NotNull]
        public string ADZ6 { get; set; } // varchar(350)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ADZ7 { get; set; } // integer
    }

    [Table("AE")]
    public partial class AE
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int AE2 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int AE3 { get; set; } // integer
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double AE4 { get; set; } // double precision
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? AE5 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string AE6 { get; set; } // varchar(15)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short AE7 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime AE8 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int AE9 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string AE11 { get; set; } // varchar(15)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? AE12 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string AE13 { get; set; } // varchar(10)
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string AE14 { get; set; } // varchar(10)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short AE15 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 45), NotNull]
        public string AE16 { get; set; } // varchar(45)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int AE17 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short AE18 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int AE19 { get; set; } // integer
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double AE20 { get; set; } // double precision
    }

    [Table("AEI")]
    public partial class AEI
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int AEI2 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int AEI3 { get; set; } // integer
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double AEI4 { get; set; } // double precision
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? AEI5 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string AEI6 { get; set; } // varchar(15)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short AEI7 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime AEI8 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int AEI9 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string AEI11 { get; set; } // varchar(15)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? AEI12 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string AEI13 { get; set; } // varchar(10)
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string AEI14 { get; set; } // varchar(10)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short AEI15 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 45), NotNull]
        public string AEI16 { get; set; } // varchar(45)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int AEI17 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short AEI18 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int AEI19 { get; set; } // integer
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double AEI20 { get; set; } // double precision
    }

    [Table("AEIEX")]
    public partial class AEIEX
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int AEIEX1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int AEIEX2 { get; set; } // integer
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double AEIEX3 { get; set; } // double precision
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short AEIEX4 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime AEIEX5 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int AEIEX6 { get; set; } // integer
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double AEIEX7 { get; set; } // double precision
    }

    [Table("AEIZ")]
    public partial class AEIZ
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int AEIZ1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int AEIZ2 { get; set; } // integer
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double AEIZ3 { get; set; } // double precision
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime AEIZ5 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int AEIZ6 { get; set; } // integer
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double AEIZ7 { get; set; } // double precision
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string AEIZ8 { get; set; } // varchar(10)
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string AEIZ9 { get; set; } // varchar(10)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short AEIZ10 { get; set; } // smallint
    }

    [Table("AO")]
    public partial class AO
    {
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int AO1 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime AO2 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int AO3 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short AO4 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 30), NotNull]
        public string AO5 { get; set; } // varchar(30)
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string AO6 { get; set; } // varchar(150)
    }

    [Table("ASP")]
    public partial class ASP
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int ASP1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 35), NotNull]
        public string ASP2 { get; set; } // varchar(35)
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string ASP3 { get; set; } // varchar(20)
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string ASP4 { get; set; } // varchar(20)
    }

    [Table("ATI")]
    public partial class ATI
    {
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ATI1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ATI2 { get; set; } // integer
    }

    [Table("B")]
    public partial class B
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int B1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string B2 { get; set; } // varchar(150)
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string B3 { get; set; } // varchar(150)
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string B4 { get; set; } // varchar(150)
        [Column(DataType = DataType.NVarChar, Length = 35), NotNull]
        public string B5 { get; set; } // varchar(35)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short B8 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short B9 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short B10 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short B11 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short B12 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short B14 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short B15 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? B16 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int B17 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string B25 { get; set; } // varchar(150)
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime B27 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int B28 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int B29 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short B31 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short B32 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short B33 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short B34 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short B35 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short B36 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short B37 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short B38 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int B39 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string B40 { get; set; } // varchar(150)
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string B41 { get; set; } // varchar(150)
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string B42 { get; set; } // varchar(150)
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string B43 { get; set; } // varchar(150)
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string B44 { get; set; } // varchar(50)
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string B45 { get; set; } // varchar(50)
    }

    [Table("BAL")]
    public partial class BAL
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int BAL0 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short BAL1 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short BAL2 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short BAL3 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short BAL4 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short BAL5 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short BAL6 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short BAL7 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short BAL8 { get; set; } // smallint
    }

    [Table("BB")]
    public partial class BB
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int B30 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), PrimaryKey(1), NotNull]
        public DateTime B31 { get; set; } // timestamp
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double B32 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double B33 { get; set; } // double precision
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short B34 { get; set; } // smallint
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double B35 { get; set; } // double precision
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime B36 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int B37 { get; set; } // integer
    }

    [Table("BL")]
    public partial class BL
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int BL1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string BL2 { get; set; } // varchar(50)
        [Column(DataType = DataType.Text, Length = int.MaxValue), Nullable]
        public string BL3 { get; set; } // blob sub_type 1
        [Column(DataType = DataType.Text, Length = int.MaxValue), Nullable]
        public string BL4 { get; set; } // blob sub_type 1
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int BL5 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short BL6 { get; set; } // smallint
    }
    [Table("BLA")]
    public partial class BLA
    {
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short BLA2 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int BLA3 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int BLA4 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int BLA5 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int BLA6 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string BLA7 { get; set; } // varchar(50)
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string BLA8 { get; set; } // varchar(50)
    }

    [Table("BSPOL")]
    public partial class BSPOL
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int BSPOL1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int BSPOL2 { get; set; } // integer
    }

    [Table("C")]
    public partial class C
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int C1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 250), NotNull]
        public string C2 { get; set; } // varchar(250)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int C3 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int C4 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? C5 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime C6 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int C7 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short C8 { get; set; } // smallint
    }

    [Table("CMP")]
    public partial class CMP
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int CMP1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 350), NotNull]
        public string CMP2 { get; set; } // varchar(350)
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string CMP3 { get; set; } // varchar(10)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short CMP4 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime CMP5 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int CMP6 { get; set; } // integer
    }

    [Table("CN")]
    public partial class CN
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int CN1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string CN2 { get; set; } // varchar(50)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short CN3 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string CN4 { get; set; } // varchar(100)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int CN5 { get; set; } // integer
    }

    [Table("CWB")]
    public partial class CWB
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int CWB1 { get; set; } // integer
    }

    [Table("CXM")]
    public partial class CXM
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int CXM0 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string CXM1 { get; set; } // varchar(25)
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string CXM2 { get; set; } // varchar(25)
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string CXM3 { get; set; } // varchar(25)
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string CXM4 { get; set; } // varchar(25)
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string CXM5 { get; set; } // varchar(25)
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string CXM6 { get; set; } // varchar(25)
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string CXM7 { get; set; } // varchar(25)
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string CXM20 { get; set; } // varchar(50)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short CXM21 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short CXM22 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short CXM23 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short CXM24 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short CXM30 { get; set; } // smallint       
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short CXM31 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short CXM32 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short CXM33 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short CXM34 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short CXM35 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short CXM36 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short CXM37 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short CXM38 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short CXM39 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short CXM40 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short CXM41 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short CXM42 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short CXM43 { get; set; } // smallint
    }

    [Table("CXMB")]
    public partial class CXMB
    {
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short CXMB0 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short CXMB2 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 2), NotNull]
        public string CXMB3 { get; set; } // varchar(2)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short CXMB4 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short CXMB5 { get; set; } // smallint
    }

    [Table("D")]
    public partial class D
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int D1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 250), NotNull]
        public string D2 { get; set; } // varchar(250)
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string D3 { get; set; } // varchar(15)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string D4 { get; set; } // varchar(100)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short D5 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short D6 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char D7 { get; set; } // varchar(1)
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char D8 { get; set; } // varchar(1)
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char D9 { get; set; } // varchar(1)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int D10 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? D11 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime D12 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int D13 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short D14 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short D15 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short D16 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short D17 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short D18 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short D19 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short D20 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short D21 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short D22 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short D23 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short D24 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short D25 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char D26 { get; set; } // varchar(1)
        [Column(DataType = DataType.NVarChar, Length = 250), NotNull]
        public string D27 { get; set; } // varchar(250)
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string D28 { get; set; } // varchar(10)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short D30 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short D31 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 250), NotNull]
        public string D32 { get; set; } // varchar(250)
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string D33 { get; set; } // varchar(10)
        [Column(DataType = DataType.NVarChar, Length = 250), NotNull]
        public string D34 { get; set; } // varchar(250)
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string D35 { get; set; } // varchar(10)
        [Column(DataType = DataType.NVarChar, Length = 250), NotNull]
        public string D36 { get; set; } // varchar(250)
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string D37 { get; set; } // varchar(15)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int D38 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string D39 { get; set; } // varchar(15)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int D40 { get; set; } // integer
    }

    [Table("DA")]
    public partial class DA
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int DA1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string DA2 { get; set; } // varchar(10)
        [Column(DataType = DataType.NVarChar, Length = 30), NotNull]
        public string DA3 { get; set; } // varchar(30)
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime DA9 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int DA10 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int DA12 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int DA13 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int DA14 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DA15 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string DA16 { get; set; } // varchar(10)
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string DA17 { get; set; } // varchar(50)
    }

    [Table("DAO")]
    public partial class DAO
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int DAO1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int DAO2 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string DAO3 { get; set; } // varchar(50)
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string DAO4 { get; set; } // varchar(10)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DAO5 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 5), NotNull]
        public string DAO6 { get; set; } // varchar(5)
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string DAO7 { get; set; } // varchar(10)
    }

    [Table("DAR")]
    public partial class DAR
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int DAR1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int DAR2 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 30), NotNull]
        public string DAR3 { get; set; } // varchar(30)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DAR4 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string DAR5 { get; set; } // varchar(15)
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string DAR6 { get; set; } // varchar(10)
    }

    [Table("DAS")]
    public partial class DAS
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int DAS1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 30), NotNull]
        public string DAS2 { get; set; } // varchar(30)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DAS3 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DAS4 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 5), NotNull]
        public string DAS5 { get; set; } // varchar(5)
    }

    [Table("DC")]
    public partial class DC
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int DC1 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DC2 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DC3 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DC4 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DC5 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DC6 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DC7 { get; set; } // smallint
    }

    [Table("DDO")]
    public partial class DDO
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int DDO1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 45), NotNull]
        public string DDO2 { get; set; } // varchar(45)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DDO3 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DDO4 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DDO5 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DDO6 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DDO7 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DDO8 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DDO9 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DDO10 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DDO11 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DDO12 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DDO13 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DDO14 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DDO15 { get; set; } // smallint
    }

    [Table("DDOI")]
    public partial class DDOI
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int DDOI1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int DDOI2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DDOI3 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DDOI4 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DDOI5 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DDOI6 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DDOI7 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DDOI8 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DDOI9 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DDOI10 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DDOI11 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DDOI12 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DDOI13 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DDOI14 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DDOI15 { get; set; } // smallint
    }

    [Table("DDON")]
    public partial class DDON
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int DDON1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int DDON2 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string DDON3 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string DDON4 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string DDON5 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string DDON6 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string DDON7 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string DDON8 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string DDON9 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string DDON10 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string DDON11 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string DDON12 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string DDON13 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string DDON14 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string DDON15 { get; set; } // varchar(100)
    }

    [Table("DDST")]
    public partial class DDST
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int DDST1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int DDST2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DDST3 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 1000), NotNull]
        public string DDST4 { get; set; } // varchar(1000)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DDST5 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 1000), NotNull]
        public string DDST6 { get; set; } // varchar(1000)
        [Column(DataType = DataType.NVarChar, Length = 1000), NotNull]
        public string DDST7 { get; set; } // varchar(1000)
        [Column(DataType = DataType.NVarChar, Length = 1000), NotNull]
        public string DDST8 { get; set; } // varchar(1000)
        [Column(DataType = DataType.NVarChar, Length = 1000), NotNull]
        public string DDST9 { get; set; } // varchar(1000)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DDST10 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DDST11 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 1000), NotNull]
        public string DDST12 { get; set; } // varchar(1000)
        [Column(DataType = DataType.NVarChar, Length = 1000), NotNull]
        public string DDST13 { get; set; } // varchar(1000)
        [Column(DataType = DataType.NVarChar, Length = 1000), NotNull]
        public string DDST14 { get; set; } // varchar(1000)
        [Column(DataType = DataType.NVarChar, Length = 1000), NotNull]
        public string DDST15 { get; set; } // varchar(1000)
        [Column(DataType = DataType.NVarChar, Length = 1000), NotNull]
        public string DDST16 { get; set; } // varchar(1000)
        [Column(DataType = DataType.NVarChar, Length = 1000), NotNull]
        public string DDST17 { get; set; } // varchar(1000)
        [Column(DataType = DataType.NVarChar, Length = 1000), NotNull]
        public string DDST18 { get; set; } // varchar(1000)
        [Column(DataType = DataType.NVarChar, Length = 1000), NotNull]
        public string DDST19 { get; set; } // varchar(1000)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DDST20 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime DDST21 { get; set; } // timestamp
    }

    [Table("DIGP")]
    public partial class DIGP
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int DIGP1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int DIGP2 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int DIGP3 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DIGP4 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int DIGP5 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? DIGP6 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? DIGP7 { get; set; } // timestamp
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DIGP8 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int DIGP9 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime DIGP10 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int DIGP11 { get; set; } // integer
    }

    [Table("DIGPA")]
    public partial class DIGPA
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int DIGPA1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int DIGPA2 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? DIGPA6 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? DIGPA7 { get; set; } // timestamp
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DIGPA8 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime DIGPA10 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int DIGPA11 { get; set; } // integer
    }

    [Table("DIGPT")]
    public partial class DIGPT
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int DIGPT1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int DIGPT2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DIGPT3 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime DIGPT4 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int DIGPT5 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int DIGPT6 { get; set; } // integer
    }

    [Table("DIGPTA")]
    public partial class DIGPTA
    {
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int DIGPTA1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int DIGPTA2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DIGPTA3 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime DIGPTA4 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int DIGPTA5 { get; set; } // integer
    }

    [Table("DIPN")]
    public partial class DIPN
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int DIPN0 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int DIPN1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int DIPN2 { get; set; } // integer
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double DIPN3 { get; set; } // double precision
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DIPN6 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime DIPN7 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int DIPN8 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DIPN9 { get; set; } // smallint
    }

    [Table("DK")]
    public partial class DK
    {
        [Column(DataType = DataType.Int16, Length = 2), PrimaryKey(0), NotNull]
        public short DK1 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int DK2 { get; set; } // integer
    }

    [Table("DKID")]
    public partial class DKID
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int DKID1 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), PrimaryKey(1), NotNull]
        public DateTime DKID2 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? DKID3 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int DKID4 { get; set; } // integer
    }

    [Table("DM")]
    public partial class DM
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int DM1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 50), Nullable]
        public string DM2 { get; set; } // varchar(50)
        [Column(DataType = DataType.NVarChar, Length = 200), Nullable]
        public string DM3 { get; set; } // varchar(200)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DM4 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DM5 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int DM6 { get; set; } // integer
    }

    [Table("DMF")]
    public partial class DMF
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int DMF1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string DMF2 { get; set; } // varchar(50)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string DMF3 { get; set; } // varchar(200)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int DMF4 { get; set; } // integer
    }

    [Table("DMFT")]
    public partial class DMFT
    {
        [Column(DataType = DataType.Int16, Length = 2), PrimaryKey(0), NotNull]
        public short DMFT1 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int DMFT2 { get; set; } // integer
        [Column(DataType = DataType.Text, Length = int.MaxValue), Nullable]
        public string DMFT3 { get; set; } // blob sub_type 1
    }

    [Table("DMIND")]
    public partial class DMIND
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int DMIND1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int DMIND2 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 100), Nullable]
        public string DMIND3 { get; set; } // varchar(100)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int DMIND4 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DMIND5 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int DMIND6 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DMIND7 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DMIND8 { get; set; } // smallint
    }

    [Table("DMP")]
    public partial class DMP
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int DMP1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string DMP2 { get; set; } // varchar(50)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string DMP3 { get; set; } // varchar(200)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string DMP4 { get; set; } // varchar(200)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DMP5 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DMP6 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string DMP7 { get; set; } // varchar(50)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string DMP8 { get; set; } // varchar(200)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string DMP9 { get; set; } // varchar(200)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string DMP10 { get; set; } // varchar(200)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string DMP11 { get; set; } // varchar(200)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string DMP12 { get; set; } // varchar(200)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string DMP13 { get; set; } // varchar(200)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string DMP14 { get; set; } // varchar(200)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string DMP15 { get; set; } // varchar(200)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string DMP16 { get; set; } // varchar(200)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string DMP17 { get; set; } // varchar(200)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string DMP18 { get; set; } // varchar(200)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string DMP19 { get; set; } // varchar(200)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string DMP20 { get; set; } // varchar(200)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string DMP21 { get; set; } // varchar(200)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string DMP22 { get; set; } // varchar(200)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string DMP23 { get; set; } // varchar(200)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string DMP24 { get; set; } // varchar(200)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string DMP25 { get; set; } // varchar(200)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string DMP26 { get; set; } // varchar(200)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string DMP27 { get; set; } // varchar(200)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string DMP28 { get; set; } // varchar(200)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string DMP29 { get; set; } // varchar(200)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string DMP30 { get; set; } // varchar(200)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string DMP31 { get; set; } // varchar(200)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string DMP32 { get; set; } // varchar(200)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string DMP33 { get; set; } // varchar(200)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string DMP34 { get; set; } // varchar(200)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string DMP35 { get; set; } // varchar(200)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string DMP36 { get; set; } // varchar(200)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string DMP37 { get; set; } // varchar(200)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string DMP38 { get; set; } // varchar(200)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string DMP39 { get; set; } // varchar(200)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string DMP40 { get; set; } // varchar(200)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string DMP41 { get; set; } // varchar(200)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string DMP42 { get; set; } // varchar(200)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string DMP43 { get; set; } // varchar(200)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string DMP44 { get; set; } // varchar(200)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string DMP45 { get; set; } // varchar(200)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string DMP46 { get; set; } // varchar(200)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string DMP47 { get; set; } // varchar(200)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string DMP48 { get; set; } // varchar(200)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string DMP49 { get; set; } // varchar(200)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string DMP50 { get; set; } // varchar(200)
    }

    [Table("DNA")]
    public partial class DNA
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int DNA1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int DNA2 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int DNA3 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DNA4 { get; set; } // smallint
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double DNA5 { get; set; } // double precision
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DNA6 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DNA7 { get; set; } // smallint
    }

    [Table("DNAR")]
    public partial class DNAR
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int DNAR1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int DNAR2 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int DNAR3 { get; set; } // integer
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double DNAR4 { get; set; } // double precision
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime DNAR5 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int DNAR6 { get; set; } // integer
    }

    [Table("DNK")]
    public partial class DNK
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int DNK1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int DNK2 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int DNK3 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int DNK4 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char DNK5 { get; set; } // varchar(1)
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double DNK6 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double DNK7 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double DNK8 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double DNK9 { get; set; } // double precision
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DNK20 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DNK21 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DNK22 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DNK23 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DNK24 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DNK25 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DNK26 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DNK27 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DNK28 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DNK29 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DNK30 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DNK31 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DNK32 { get; set; } // smallint
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double DNK33 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double DNK34 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double DNK35 { get; set; } // double precision
    }

    [Table("DNO")]
    public partial class DNO
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int DNO1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int DNO2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DNO3 { get; set; } // smallint
    }

    [Table("DNSH")]
    public partial class DNSH
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int DNSH1 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DNSH2 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DNSH3 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string DNSH4 { get; set; } // varchar(50)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DNSH5 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int DNSH6 { get; set; } // integer
    }

    [Table("DNSHF")]
    public partial class DNSHF
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int DNSHF1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int DNSHF2 { get; set; } // integer
    }

    [Table("DNSP")]
    public partial class DNSP
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int DNSP1 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DNSP2 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DNSP3 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string DNSP4 { get; set; } // varchar(50)
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string DNSP5 { get; set; } // varchar(50)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int DNSP6 { get; set; } // integer
    }

    [Table("DOB")]
    public partial class DOB
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int DOB1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 75), NotNull]
        public string DOB2 { get; set; } // varchar(75)
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string DOB3 { get; set; } // varchar(25)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DOB4 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DOB5 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 75), NotNull]
        public string DOB6 { get; set; } // varchar(75)
    }

    [Table("DOBU")]
    public partial class DOBU
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int DOBU1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int DOBU2 { get; set; } // integer
    }

    [Table("DOL")]
    public partial class DOL
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int DOL1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string DOL2 { get; set; } // varchar(25)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DOL3 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DOL4 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DOL5 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int DOL6 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? DOL7 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime DOL8 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int DOL9 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DOL10 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DOL11 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DOL12 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string DOL13 { get; set; } // varchar(25)
    }

    [Table("DOPB")]
    public partial class DOPB
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int DOPB1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string DOPB2 { get; set; } // varchar(200)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DOPB3 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DOPB4 { get; set; } // smallint
    }
    [Table("DOPST")]
    public partial class DOPST
    {
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int DOPST1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int DOPST2 { get; set; } // integer       
    }
    [Table("DOPBST")]
    public partial class DOPBST
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int DOPBST1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int DOPBST2 { get; set; } // integer
    }

    [Table("DPR")]
    public partial class DPR
    {
        [Column(DataType = DataType.Int16, Length = 2), PrimaryKey(0), NotNull]
        public short DPR1 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), PrimaryKey(1), NotNull]
        public short DPR2 { get; set; } // smallint
    }

    [Table("DPRM")]
    public partial class DPRM
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int DPRM1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string DPRM2 { get; set; } // varchar(100)
    }

    [Table("DPRO")]
    public partial class DPRO
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int DPRO1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 250), NotNull]
        public string DPRO2 { get; set; } // varchar(250)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int DPRO3 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int DPRO4 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 35), NotNull]
        public string DPRO5 { get; set; } // varchar(35)
        [Column(DataType = DataType.NVarChar, Length = 35), NotNull]
        public string DPRO6 { get; set; } // varchar(35)
        [Column(DataType = DataType.NVarChar, Length = 35), NotNull]
        public string DPRO7 { get; set; } // varchar(35)
        [Column(DataType = DataType.NVarChar, Length = 35), NotNull]
        public string DPRO8 { get; set; } // varchar(35)
        [Column(DataType = DataType.NVarChar, Length = 35), NotNull]
        public string DPRO9 { get; set; } // varchar(35)
        [Column(DataType = DataType.NVarChar, Length = 35), NotNull]
        public string DPRO10 { get; set; } // varchar(35)
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string DPRO11 { get; set; } // varchar(150)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int DPRO12 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 75), NotNull]
        public string DPRO13 { get; set; } // varchar(75)
        [Column(DataType = DataType.NVarChar, Length = 6), NotNull]
        public string DPRO14 { get; set; } // varchar(6)
        [Column(DataType = DataType.NVarChar, Length = 8), NotNull]
        public string DPRO15 { get; set; } // varchar(8)
        [Column(DataType = DataType.NVarChar, Length = 5), NotNull]
        public string DPRO16 { get; set; } // varchar(5)
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string DPRO17 { get; set; } // varchar(10)
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string DPRO18 { get; set; } // varchar(50)
        [Column(DataType = DataType.NVarChar, Length = 30), NotNull]
        public string DPRO19 { get; set; } // varchar(30)
        [Column(DataType = DataType.NVarChar, Length = 30), NotNull]
        public string DPRO20 { get; set; } // varchar(30)
        [Column(DataType = DataType.NVarChar, Length = 30), NotNull]
        public string DPRO21 { get; set; } // varchar(30)
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string DPRO22 { get; set; } // varchar(15)
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string DPRO23 { get; set; } // varchar(15)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string DPRO24 { get; set; } // varchar(200)
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string DPRO25 { get; set; } // varchar(10)
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string DPRO26 { get; set; } // varchar(20)
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string DPRO27 { get; set; } // varchar(20)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int DPRO28 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string DPRO29 { get; set; } // varchar(10)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? DPRO30 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? DPRO31 { get; set; } // timestamp
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DPRO32 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string DPRO33 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string DPRO34 { get; set; } // varchar(20)
    }

    [Table("DPROV")]
    public partial class DPROV
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int DPROV1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string DPROV2 { get; set; } // varchar(50)
    }

    [Table("DPRV")]
    public partial class DPRV
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int DPRV1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string DPRV2 { get; set; } // varchar(100)
    }

    [Table("DPSR")]
    public partial class DPSR
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int DPSR1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string DPSR2 { get; set; } // varchar(150)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int DPSR3 { get; set; } // integer
    }

    [Table("DRSG")]
    public partial class DRSG
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int DRSG1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int DRSG2 { get; set; } // integer
    }

    [Table("DU")]
    public partial class DU
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int DU1 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DU2 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DU3 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char DU4 { get; set; } // varchar(1)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short DU5 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char DU6 { get; set; } // varchar(1)
    }

    [Table("E")]
    public partial class E
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int E1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string E2 { get; set; } // varchar(10)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int E6 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? E7 { get; set; } // timestamp
    }

    [Table("EDRP")]
    public partial class EDRP
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int EDRP1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int EDRP2 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? EDRP3 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 1000), NotNull]
        public string EDRP4 { get; set; } // varchar(1000)
    }

    [Table("EK")]
    public partial class EK
    {
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double E3 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double E4 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double E5 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double E8 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double E9 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double E10 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double E11 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double E12 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double E13 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double E14 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double E15 { get; set; } // double precision
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int E16 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), PrimaryKey(1), NotNull]
        public short E17 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(2), NotNull]
        public int E18 { get; set; } // integer
    }

    [Table("ELG")]
    public partial class ELG
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int ELG1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ELG2 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ELG3 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ELG4 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ELG5 { get; set; } // smallint
    }

    [Table("ELGD")]
    public partial class ELGD
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int ELGD0 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ELGD1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ELGD2 { get; set; } // integer
    }

    [Table("ELGDST")]
    public partial class ELGDST
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int ELGDST0 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ELGDST1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ELGDST2 { get; set; } // integer
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double ELGDST3 { get; set; } // double precision
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime ELGDST4 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ELGDST5 { get; set; } // integer
    }

    [Table("ELGNO")]
    public partial class ELGNO
    {
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ELGNO2 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ELGNO3 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ELGNO4 { get; set; } // smallint
    }

    [Table("ELGOTR")]
    public partial class ELGOTR
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int ELGOTR0 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ELGOTR1 { get; set; } // integer
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double ELGOTR2 { get; set; } // double precision
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime ELGOTR3 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ELGOTR4 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime ELGOTR5 { get; set; } // timestamp
    }

    [Table("ELGP")]
    public partial class ELGP
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int ELGP0 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ELGP1 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ELGP2 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string ELGP3 { get; set; } // varchar(20)
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string ELGP4 { get; set; } // varchar(20)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? ELGP5 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime ELGP6 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ELGP7 { get; set; } // integer
    }

    [Table("ELGPMKST")]
    public partial class ELGPMKST
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int ELGPMKST1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ELGPMKST2 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ELGPMKST3 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ELGPMKST4 { get; set; } // integer
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double ELGPMKST5 { get; set; } // double precision
    }

    [Table("ELGR")]
    public partial class ELGR
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int ELGR1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int ELGR2 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? ELGR3 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string ELGR4 { get; set; } // varchar(50)
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime ELGR5 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ELGR6 { get; set; } // integer
    }

    [Table("ELGSD")]
    public partial class ELGSD
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int ELGSD1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string ELGSD2 { get; set; } // varchar(25)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ELGSD3 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ELGSD4 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ELGSD5 { get; set; } // smallint
    }

    [Table("ELGVST")]
    public partial class ELGVST
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int ELGVST1 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? ELGVST2 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? ELGVST3 { get; set; } // timestamp
    }

    [Table("ELGZ")]
    public partial class ELGZ
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int ELGZ1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ELGZ2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ELGZ3 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ELGZ4 { get; set; } // smallint
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double ELGZ5 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double ELGZ6 { get; set; } // double precision
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ELGZ7 { get; set; } // integer
    }

    [Table("ELGZST")]
    public partial class ELGZST
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int ELGZST0 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ELGZST1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ELGZST2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ELGZST3 { get; set; } // smallint
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double ELGZST4 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double ELGZST5 { get; set; } // double precision
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime ELGZST6 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ELGZST7 { get; set; } // integer
    }

    [Table("ELGZU")]
    public partial class ELGZU
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int ELGZU1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ELGZU2 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ELGZU3 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ELGZU4 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime ELGZU5 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ELGZU6 { get; set; } // integer
    }

    [Table("EPR")]
    public partial class EPR
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int EPR1 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), PrimaryKey(1), NotNull]
        public short EPR2 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int EPR3 { get; set; } // integer
    }

    [Table("EPRT")]
    public partial class EPRT
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int EPRT1 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short EPRT2 { get; set; } // smallint
    }

    [Table("F")]
    public partial class F
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int F1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string F2 { get; set; } // varchar(25)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string F3 { get; set; } // varchar(200)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string F4 { get; set; } // varchar(200)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string F5 { get; set; } // varchar(200)
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char F6 { get; set; } // varchar(1)
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char F7 { get; set; } // varchar(1)
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char F8 { get; set; } // varchar(1)
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char F9 { get; set; } // varchar(1)
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char F10 { get; set; } // varchar(1)
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char F11 { get; set; } // varchar(1)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short F12 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 3), NotNull]
        public string F13 { get; set; } // varchar(3)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short F14 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short F15 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int F16 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short F17 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? F19 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 45), NotNull]
        public string F20 { get; set; } // varchar(45)
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string F21 { get; set; } // varchar(150)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short F22 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime F23 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int F24 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int F25 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string F26 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string F27 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string F28 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char F29 { get; set; } // varchar(1)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string F30 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string F31 { get; set; } // varchar(25)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short F32 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short F33 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short F34 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int F35 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short F36 { get; set; } // smallint
    }

    [Table("FEDEDBO")]
    public partial class FEDEDBO
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int FEDEDBO1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int FEDEDBO2 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int FEDEDBO3 { get; set; } // integer
    }

    [Table("FG")]
    public partial class FG
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int FG1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int FG2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short FG3 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string FG4 { get; set; } // varchar(100)
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime FG5 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime FG6 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int FG7 { get; set; } // integer
    }

    [Table("FGP")]
    public partial class FGP
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int FGP1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int FGP2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short FGP3 { get; set; } // smallint
    }

    [Table("FO")]
    public partial class FO
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int FO1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string FO2 { get; set; } // varchar(25)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short FO3 { get; set; } // smallint
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double FO4 { get; set; } // double precision
    }

    [Table("FST")]
    public partial class FST
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int FST1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int FST2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short FST3 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short FST4 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int FST5 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int FST6 { get; set; } // integer
    }

    [Table("FSTG")]
    public partial class FSTG
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int FSTG1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int FSTG2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short FSTG3 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short FSTG4 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int FSTG5 { get; set; } // integer
    }

    [Table("GA")]
    public partial class GA
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int GA1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string GA2 { get; set; } // varchar(25)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int GA3 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime GA5 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int GA6 { get; set; } // integer
    }

    [Table("GAA")]
    public partial class GAA
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int GAA1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int GAA2 { get; set; } // integer
    }

    [Table("GNA")]
    public partial class GNA
    {
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int GNA1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string GNA2 { get; set; } // varchar(150)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int GNA3 { get; set; } // integer
    }

    [Table("GOST")]
    public partial class GOST
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int GOST1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string GOST2 { get; set; } // varchar(50)
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string GOST3 { get; set; } // varchar(50)
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string GOST4 { get; set; } // varchar(10)
    }

    [Table("GP")]
    public partial class GP
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int GP1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string GP2 { get; set; } // varchar(50)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short GP3 { get; set; } // smallint
    }

    [Table("GPI")]
    public partial class GPI
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int GPI1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int GPI2 { get; set; } // integer
    }

    [Table("GVSPR")]
    public partial class GVSPR
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int GVSPR1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int GVSPR2 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string GVSPR3 { get; set; } // varchar(25)
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime GVSPR4 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string GVSPR5 { get; set; } // varchar(50)
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime GVSPR6 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int GVSPR7 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? GVSPR8 { get; set; } // timestamp
        [Column(DataType = DataType.Blob, Length = int.MaxValue), NotNull]
        public byte[] GVSPR9 { get; set; } // blob
    }

    [Table("GR")]
    public partial class GR
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int GR1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int GR2 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 45), NotNull]
        public string GR3 { get; set; } // varchar(45)
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string GR4 { get; set; } // varchar(25)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? GR6 { get; set; } // timestamp
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short GR7 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int GR8 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int GR10 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char GR13 { get; set; } // varchar(1)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short GR14 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short GR15 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char GR16 { get; set; } // varchar(1)
        [Column(DataType = DataType.NVarChar, Length = 7), NotNull]
        public string GR17 { get; set; } // varchar(7)
        [Column(DataType = DataType.NVarChar, Length = 45), NotNull]
        public string GR19 { get; set; } // varchar(45)
        [Column(DataType = DataType.NVarChar, Length = 45), NotNull]
        public string GR20 { get; set; } // varchar(45)
        [Column(DataType = DataType.NVarChar, Length = 45), NotNull]
        public string GR21 { get; set; } // varchar(45)
        [Column(DataType = DataType.NVarChar, Length = 45), NotNull]
        public string GR22 { get; set; } // varchar(45)
        [Column(DataType = DataType.NVarChar, Length = 45), NotNull]
        public string GR23 { get; set; } // varchar(45)
        [Column(DataType = DataType.NVarChar, Length = 45), NotNull]
        public string GR24 { get; set; } // varchar(45)
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime GR25 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int GR26 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 35), NotNull]
        public string GR27 { get; set; } // varchar(35)
        [Column(DataType = DataType.NVarChar, Length = 45), NotNull]
        public string GR28 { get; set; } // varchar(45)
    }

    [Table("GRANTS")]
    public partial class GRANTS
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int GRANTS1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int GRANTS2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short GRANTS3 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short GRANTS6 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short GRANTS7 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short GRANTS5 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short GRANTS4 { get; set; } // smallint
    }

    [Table("GUP")]
    public partial class GUP
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int GUP1 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), PrimaryKey(1), NotNull]
        public short GUP2 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), PrimaryKey(2), NotNull]
        public short GUP3 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int GUP4 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), PrimaryKey(3), NotNull]
        public short GUP5 { get; set; } // smallint
    }

    [Table("I")]
    public partial class I
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int I1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 90), NotNull]
        public string I2 { get; set; } // varchar(90)
        [Column(DataType = DataType.NVarChar, Length = 90), NotNull]
        public string I3 { get; set; } // varchar(90)
        [Column(DataType = DataType.NVarChar, Length = 180), NotNull]
        public string I4 { get; set; } // varchar(180)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string I5 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char I6 { get; set; } // varchar(1)
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char I9 { get; set; } // varchar(1)
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char I10 { get; set; } // varchar(1)
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char I11 { get; set; } // varchar(1)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short I13 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short I14 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short I15 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short I16 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short I18 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short I19 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short I25 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short I26 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short I32 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short I33 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short I34 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short I35 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short I36 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short I37 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short I38 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short I39 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short I40 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short I50 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short I80 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short I81 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short I82 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short I83 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short I96 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short I97 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short I98 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short I99 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short I100 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string I101 { get; set; } // varchar(10)
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string I102 { get; set; } // varchar(10)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short I103 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 45), NotNull]
        public string I104 { get; set; } // varchar(45)
        [Column(DataType = DataType.NVarChar, Length = 45), NotNull]
        public string I105 { get; set; } // varchar(45)
        [Column(DataType = DataType.NVarChar, Length = 45), NotNull]
        public string I106 { get; set; } // varchar(45)
        [Column(DataType = DataType.NVarChar, Length = 45), NotNull]
        public string I107 { get; set; } // varchar(45)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? I108 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? I109 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int I110 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 35), NotNull]
        public string I111 { get; set; } // varchar(35)
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char I12 { get; set; } // varchar(1)
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char I7 { get; set; } // varchar(1)
        [Column(DataType = DataType.NVarChar, Length = 8), NotNull]
        public string I8 { get; set; } // varchar(8)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string I112 { get; set; } // varchar(100)
    }

    [Table("IA")]
    public partial class IA
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int IA1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int IA2 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime IA3 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 5), NotNull]
        public string IA4 { get; set; } // varchar(5)
        [Column(DataType = DataType.NVarChar, Length = 5), NotNull]
        public string IA5 { get; set; } // varchar(5)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int IA6 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short IA7 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string IA8 { get; set; } // varchar(20)
    }

    [Table("IDO")]
    public partial class IDO
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int IDO0 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int IDO1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int IDO2 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int IDO4 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short IDO5 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? IDO6 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string IDO7 { get; set; } // varchar(100)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int IDO8 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int IDO9 { get; set; } // integer
    }

    [Table("IK")]
    public partial class IK
    {
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int IK1 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short IK2 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short IK3 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short IK4 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short IK5 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short IK6 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short IK7 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short IK8 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short IK9 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short IK10 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short IK11 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short IK12 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short IK13 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short IK14 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short IK15 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short IK16 { get; set; } // smallint
    }

    [Table("IM")]
    public partial class IM
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int IM1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int IM2 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime IM3 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 250), NotNull]
        public string IM4 { get; set; } // varchar(250)
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char IM5 { get; set; } // varchar(1)
    }

    [Table("INNF")]
    public partial class INNF
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int INNF1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 5), NotNull]
        public string INNF2 { get; set; } // varchar(5)
        [Column(DataType = DataType.NVarChar, Length = 250), NotNull]
        public string INNF3 { get; set; } // varchar(250)
        [Column(DataType = DataType.NVarChar, Length = 5), NotNull]
        public string INNF4 { get; set; } // varchar(5)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? INNF5 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int INNF6 { get; set; } // integer
    }

    [Table("INNFP")]
    public partial class INNFP
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int INNFP1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int INNFP2 { get; set; } // integer
    }

    [Table("JPV")]
    public partial class JPV
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int JPV1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int JPV2 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int JPV3 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short JPV4 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int JPV5 { get; set; } // integer
    }

    [Table("JPVD")]
    public partial class JPVD
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int JPVD0 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int JPVD1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int JPVD2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short JPVD3 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime JPVD4 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int JPVD5 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int JPVD6 { get; set; } // integer
    }

    [Table("JPVP")]
    public partial class JPVP
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int JPVP1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int JPVP2 { get; set; } // integer
    }

    [Table("K")]
    public partial class K
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int K1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string K2 { get; set; } // varchar(50)
        [Column(DataType = DataType.NVarChar, Length = 250), NotNull]
        public string K3 { get; set; } // varchar(250)
        [Column(DataType = DataType.NVarChar, Length = 250), NotNull]
        public string K4 { get; set; } // varchar(250)
        [Column(DataType = DataType.NVarChar, Length = 250), NotNull]
        public string K5 { get; set; } // varchar(250)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short K7 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short K8 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? K9 { get; set; } // timestamp
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short K10 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short K11 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short K12 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime K13 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int K14 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 250), NotNull]
        public string K15 { get; set; } // varchar(250)
        [Column(DataType = DataType.NVarChar, Length = 250), NotNull]
        public string K16 { get; set; } // varchar(250)
        [Column(DataType = DataType.NVarChar, Length = 250), NotNull]
        public string K17 { get; set; } // varchar(250)
        [Column(DataType = DataType.NVarChar, Length = 250), NotNull]
        public string K18 { get; set; } // varchar(250)
    }

    [Table("KA")]
    public partial class KA
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int KA1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string KA2 { get; set; } // varchar(25)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int KA3 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short KA4 { get; set; } // smallint
    }

    [Table("KAA")]
    public partial class KAA
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int KAA1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int KAA2 { get; set; } // integer
    }

    [Table("KCP")]
    public partial class KCP
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int KCP1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int KCP2 { get; set; } // integer
    }

    [Table("KD")]
    public partial class KD
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int KD0 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime KD1 { get; set; } // timestamp
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double KD2 { get; set; } // double precision
    }

    [Table("KGRP")]
    public partial class KGRP
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int KGRP1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int KGRP2 { get; set; } // integer
    }

    [Table("KK")]
    public partial class KK
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int KK1 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), PrimaryKey(1), NotNull]
        public short KK2 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(2), NotNull]
        public int KK3 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), PrimaryKey(3), NotNull]
        public short KK4 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), PrimaryKey(4), NotNull]
        public short KK5 { get; set; } // smallint
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double KK6 { get; set; } // double precision
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short KK7 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short KK8 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short KK9 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(5), NotNull]
        public int KK10 { get; set; } // integer
    }

    [Table("KMP")]
    public partial class KMP
    {
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int KMP1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int KMP2 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int KMP3 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 1000), NotNull]
        public string KMP4 { get; set; } // varchar(1000)
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string KMP5 { get; set; } // varchar(15)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? KMP6 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? KMP7 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? KMP8 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string KMP9 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string KMP10 { get; set; } // varchar(100)
    }

    [Table("KP")]
    public partial class KP
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int KP1 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime KP2 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? KP3 { get; set; } // timestamp
    }

    [Table("KPK")]
    public partial class KPK
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int KPK1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int KPK2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), PrimaryKey(2), NotNull]
        public short KPK3 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), PrimaryKey(3), NotNull]
        public short KPK4 { get; set; } // smallint
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double KPK5 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double KPK6 { get; set; } // double precision
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(4), NotNull]
        public int KPK7 { get; set; } // integer
    }

    [Table("KPOD")]
    public partial class KPOD
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int KPOD0 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int KPOD1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int KPOD2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short KPOD3 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short KPOD4 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short KPOD5 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short KPOD6 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short KPOD7 { get; set; } // smallint
    }

    [Table("KRT")]
    public partial class KRT
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int KRT1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string KRT2 { get; set; } // varchar(100)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short KRT3 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short KRT4 { get; set; } // smallint
    }

    [Table("KS")]
    public partial class KS
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int KS1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string KS2 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 30), NotNull]
        public string KS3 { get; set; } // varchar(30)
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char KS4 { get; set; } // varchar(1)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short KS7 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string KS8 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string KS9 { get; set; } // varchar(150)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string KS10 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 30), NotNull]
        public string KS11 { get; set; } // varchar(30)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? KS12 { get; set; } // timestamp
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short KS13 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short KS14 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int KS15 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short KS16 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short KS17 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short KS18 { get; set; } // smallint
    }

    [Table("KSPR")]
    public partial class KSPR
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int KSPR1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int KSPR2 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string KSPR3 { get; set; } // varchar(15)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short KSPR4 { get; set; } // smallint
    }

    [Table("KU")]
    public partial class KU
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int KU1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string KU2 { get; set; } // varchar(100)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short KU3 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int KU4 { get; set; } // integer
    }

    [Table("KUD")]
    public partial class KUD
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int KUD1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string KUD2 { get; set; } // varchar(100)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int KUD3 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short KUD4 { get; set; } // smallint
    }

    [Table("LAN")]
    public partial class LAN
    {
        [Column(DataType = DataType.NVarChar, Length = 250), PrimaryKey, NotNull]
        public string LAN1 { get; set; } // varchar(250)
        [Column(DataType = DataType.NVarChar, Length = 250), NotNull]
        public string LAN2 { get; set; } // varchar(250)
        [Column(DataType = DataType.NVarChar, Length = 250), NotNull]
        public string LAN3 { get; set; } // varchar(250)
        [Column(DataType = DataType.NVarChar, Length = 250), NotNull]
        public string LAN4 { get; set; } // varchar(250)
        [Column(DataType = DataType.NVarChar, Length = 250), NotNull]
        public string LAN5 { get; set; } // varchar(250)
    }

    [Table("LG")]
    public partial class LG
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int LG1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string LG2 { get; set; } // varchar(50)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short LG3 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short LG4 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short LG5 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short LG6 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short LG7 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short LG8 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? LG9 { get; set; } // timestamp
    }

    [Table("LS")]
    public partial class LS
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int LS1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int LS2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short LS3 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short LS4 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string LS5 { get; set; } // varchar(15)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short LS6 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short LS7 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int LS8 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int LS9 { get; set; } // integer
    }

    [Table("MA")]
    public partial class MA
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int MA1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string MA2 { get; set; } // varchar(50)
    }

    [Table("MAF")]
    public partial class MAF
    {
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int MAF1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int MAF2 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string MAF3 { get; set; } // varchar(100)
    }

    [Table("MAK")]
    public partial class MAK
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int MAK1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 75), NotNull]
        public string MAK2 { get; set; } // varchar(75)
        [Column(DataType = DataType.NVarChar, Length = 75), NotNull]
        public string MAK3 { get; set; } // varchar(75)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short MAK4 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string MAK6 { get; set; } // varchar(50)
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string MAK7 { get; set; } // varchar(50)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short MAK10 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char MAK11 { get; set; } // varchar(1)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int MAK12 { get; set; } // integer
    }

    [Table("MARKB")]
    public partial class MARKB
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int MARKB1 { get; set; } // integer
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double MARKB2 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double MARKB3 { get; set; } // double precision
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short MARKB4 { get; set; } // smallint
    }

    [Table("MEDR")]
    public partial class MEDR
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int MEDR1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string MEDR3 { get; set; } // varchar(25)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? MEDR4 { get; set; } // timestamp
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short MEDR6 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int MEDR7 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int MEDR8 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int MEDR9 { get; set; } // integer
    }

    [Table("MEDS")]
    public partial class MEDS
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int MEDS1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int MEDS2 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int MEDS3 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 2), NotNull]
        public string MEDS4 { get; set; } // varchar(2)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short MEDS5 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int MEDS6 { get; set; } // integer
    }

    [Table("MEDT")]
    public partial class MEDT
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int MEDT1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 30), NotNull]
        public string MEDT2 { get; set; } // varchar(30)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short MEDT3 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short MEDT4 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short MEDT5 { get; set; } // smallint
    }

    [Table("MOK")]
    public partial class MOK
    {
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double MOK1 { get; set; } // double precision
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime MOK2 { get; set; } // timestamp
    }

    [Table("MP")]
    public partial class MP
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int MP1 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short MP2 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 30), NotNull]
        public string MP3 { get; set; } // varchar(30)
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double MP4 { get; set; } // double precision
        [Column(DataType = DataType.Blob, Length = int.MaxValue), Nullable]
        public byte[] MP5 { get; set; } // blob
    }

    [Table("MTDM")]
    public partial class MTDM
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int MTDM1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int MTDM2 { get; set; } // integer
    }

    [Table("MTG")]
    public partial class MTG
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int MTG1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string MTG2 { get; set; } // varchar(15)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string MTG3 { get; set; } // varchar(100)
    }

    [Table("MTGZ")]
    public partial class MTGZ
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int MTGZ1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int MTGZ2 { get; set; } // integer
    }

    [Table("MTMO")]
    public partial class MTMO
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int MTMO1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 500), NotNull]
        public string MTMO2 { get; set; } // varchar(500)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int MTMO3 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int MTMO4 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short MTMO5 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short MTMO6 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short MTMO7 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short MTMO8 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short MTMO9 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short MTMO10 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 350), NotNull]
        public string MTMO11 { get; set; } // varchar(350)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short MTMO12 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string MTMO13 { get; set; } // varchar(50)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string MTMO15 { get; set; } // varchar(100)
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double MTMO16 { get; set; } // double precision
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string MTMO17 { get; set; } // varchar(50)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short MTMO18 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short MTMO19 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short MTMO20 { get; set; } // smallint
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double MTMO21 { get; set; } // double precision
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string MTMO22 { get; set; } // varchar(50)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? MTMO23 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? MTMO24 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? MTMO25 { get; set; } // timestamp
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double MTMO26 { get; set; } // double precision
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string MTMO27 { get; set; } // varchar(10)
        [Column(DataType = DataType.NVarChar, Length = 30), NotNull]
        public string MTMO28 { get; set; } // varchar(30)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string MTMO29 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string MTMO30 { get; set; } // varchar(25)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short MTMO31 { get; set; } // smallint
    }

    [Table("MTMOA")]
    public partial class MTMOA
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int MTMOA1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int MTMOA2 { get; set; } // integer
    }

    [Table("MTMOP")]
    public partial class MTMOP
    {
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int MTMOP1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int MTMOP2 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string MTMOP3 { get; set; } // varchar(100)
    }

    [Table("MTMOT")]
    public partial class MTMOT
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int MTMOT1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int MTMOT2 { get; set; } // integer
    }

    [Table("MTRP")]
    public partial class MTRP
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int MTRP1 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short MTRP2 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short MTRP3 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short MTRP4 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short MTRP5 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short MTRP6 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short MTRP7 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int MTRP8 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int MTRP9 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int MTRP10 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short MTRP11 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short MTRP12 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short MTRP13 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int MTRP14 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int MTRP15 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int MTRP16 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string MTRP17 { get; set; } // varchar(25)
    }

    [Table("MTT")]
    public partial class MTT
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int MTT1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string MTT2 { get; set; } // varchar(15)
    }

    [Table("MTV")]
    public partial class MTV
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int MTV1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string MTV2 { get; set; } // varchar(15)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short MTV3 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short MTV4 { get; set; } // smallint
    }

    [Table("MTZ")]
    public partial class MTZ
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int MTZ1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int MTZ2 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(2), NotNull]
        public int MTZ3 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short MTZ4 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(3), NotNull]
        public int MTZ5 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(4), NotNull]
        public int MTZ6 { get; set; } // integer
    }

    [Table("MVPF")]
    public partial class MVPF
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int MVPF1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int MVPF2 { get; set; } // integer
        [Column(DataType = DataType.Text, Length = int.MaxValue), Nullable]
        public string MVPF3 { get; set; } // blob sub_type 1
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short MVPF5 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short MVPF4 { get; set; } // smallint
    }

    [Table("NA")]
    public partial class NA
    {
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int NA1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string NA2 { get; set; } // varchar(150)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int NA3 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int NA4 { get; set; } // integer
    }

    [Table("NAPR")]
    public partial class NAPR
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int NAPR1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string NAPR2 { get; set; } // varchar(100)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int NAPR3 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime NAPR4 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int NAPR5 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? NAPR6 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string NAPR7 { get; set; } // varchar(25)
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string NAPR8 { get; set; } // varchar(25)
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string NAPR9 { get; set; } // varchar(25)
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string NAPR10 { get; set; } // varchar(15)
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string NAPR11 { get; set; } // varchar(10)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int NAPR12 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string NAPR13 { get; set; } // varchar(10)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string NAPR14 { get; set; } // varchar(100)
    }

    [Table("NC")]
    public partial class NC
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int NC1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string NC2 { get; set; } // varchar(25)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string NC3 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string NC4 { get; set; } // varchar(15)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short NC5 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short NC6 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short NC7 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short NC8 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short NC9 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short NC10 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short NC11 { get; set; } // smallint
    }

    [Table("NKRS")]
    public partial class NKRS
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int NKRS1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int NKRS2 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int NKRS3 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 350), NotNull]
        public string NKRS4 { get; set; } // varchar(350)
        [Column(DataType = DataType.NVarChar, Length = 350), NotNull]
        public string NKRS5 { get; set; } // varchar(350)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int NKRS6 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int NKRS7 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 350), NotNull]
        public string NKRS8 { get; set; } // varchar(350)
    }

    [Table("NPS")]
    public partial class NPS
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int NPS1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int NPS2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short NPS3 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short NPS4 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 250), NotNull]
        public string NPS5 { get; set; } // varchar(250)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short NPS6 { get; set; } // smallint
        [Column(DataType = DataType.Text, Length = int.MaxValue), Nullable]
        public string NPS7 { get; set; } // blob sub_type 1
        [Column(DataType = DataType.Text, Length = int.MaxValue), Nullable]
        public string NPS8 { get; set; } // blob sub_type 1
        [Column(DataType = DataType.Text, Length = int.MaxValue), Nullable]
        public string NPS9 { get; set; } // blob sub_type 1
        [Column(DataType = DataType.NVarChar, Length = 75), NotNull]
        public string NPS10 { get; set; } // varchar(75)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short NPS11 { get; set; } // smallint
        [Column(DataType = DataType.Text, Length = int.MaxValue), Nullable]
        public string NPS12 { get; set; } // blob sub_type 1
        [Column(DataType = DataType.Text, Length = int.MaxValue), Nullable]
        public string NPS13 { get; set; } // blob sub_type 1
        [Column(DataType = DataType.Text, Length = int.MaxValue), Nullable]
        public string NPS14 { get; set; } // blob sub_type 1
        [Column(DataType = DataType.Text, Length = int.MaxValue), Nullable]
        public string NPS15 { get; set; } // blob sub_type 1
        [Column(DataType = DataType.Text, Length = int.MaxValue), Nullable]
        public string NPS16 { get; set; } // blob sub_type 1
        [Column(DataType = DataType.Text, Length = int.MaxValue), Nullable]
        public string NPS17 { get; set; } // blob sub_type 1
        [Column(DataType = DataType.Text, Length = int.MaxValue), Nullable]
        public string NPS18 { get; set; } // blob sub_type 1
        [Column(DataType = DataType.Text, Length = int.MaxValue), Nullable]
        public string NPS19 { get; set; } // blob sub_type 1
        [Column(DataType = DataType.Text, Length = int.MaxValue), Nullable]
        public string NPS20 { get; set; } // blob sub_type 1
        [Column(DataType = DataType.Text, Length = int.MaxValue), Nullable]
        public string NPS21 { get; set; } // blob sub_type 1
        [Column(DataType = DataType.Text, Length = int.MaxValue), Nullable]
        public string NPS22 { get; set; } // blob sub_type 1
        [Column(DataType = DataType.Text, Length = int.MaxValue), Nullable]
        public string NPS23 { get; set; } // blob sub_type 1
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int NPS24 { get; set; } // integer
        [Column(DataType = DataType.Text, Length = int.MaxValue), Nullable]
        public string NPS25 { get; set; } // blob sub_type 1
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int NPS26 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short NPS27 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short NPS28 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short NPS29 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short NPS30 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int NPS31 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 250), NotNull]
        public string NPS32 { get; set; } // varchar(250)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? NPS33 { get; set; } // timestamp
    }

    [Table("NR")]
    public partial class NR
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int NR1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int NR2 { get; set; } // integer
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double NR3 { get; set; } // double precision
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short NR4 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short NR5 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int NR6 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int NR7 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int NR8 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int NR9 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int NR10 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int NR11 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int NR12 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int NR13 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short NR15 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short NR16 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short NR17 { get; set; } // smallint
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double NR18 { get; set; } // double precision
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short NR21 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short NR23 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime NR24 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int NR25 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short NR26 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short NR27 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short NR28 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short NR29 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int NR30 { get; set; } // integer
    }

    [Table("NRD")]
    public partial class NRD
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int NRD1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 75), NotNull]
        public string NRD2 { get; set; } // varchar(75)
    }

    [Table("NRT")]
    public partial class NRT
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int NRT1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int NRT2 { get; set; } // integer
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double NRT3 { get; set; } // double precision
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short NRT4 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short NRT5 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int NRT6 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int NRT7 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int NRT8 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int NRT9 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int NRT10 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int NRT11 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int NRT12 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int NRT13 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short NRT15 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short NRT16 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short NRT17 { get; set; } // smallint
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double NRT18 { get; set; } // double precision
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short NRT21 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short NRT23 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime NRT24 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int NRT25 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short NRT26 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short NRT27 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short NRT28 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short NRT29 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int NRT30 { get; set; } // integer
    }

    [Table("NRV")]
    public partial class NRV
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int NRV1 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), PrimaryKey(1), NotNull]
        public short NRV2 { get; set; } // smallint
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double NRV3 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double NRV4 { get; set; } // double precision
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(2), NotNull]
        public int NRV5 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(3), NotNull]
        public int NRV6 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(4), NotNull]
        public int NRV7 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(5), NotNull]
        public int NRV8 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short NRV9 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short NRV10 { get; set; } // smallint
    }

    [Table("OB")]
    public partial class OB
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int OB1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string OB2 { get; set; } // varchar(50)
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string OB3 { get; set; } // varchar(20)
    }

    [Table("OBPO")]
    public partial class OBPO
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int OBPO1 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short OBPO2 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short OBPO3 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int OBPO4 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string OBPO5 { get; set; } // varchar(50)
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double OBPO6 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double OBPO7 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double OBPO8 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double OBPO9 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double OBPO10 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double OBPO11 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double OBPO12 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double OBPO13 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double OBPO14 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double OBPO15 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double OBPO16 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double OBPO26 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double OBPO27 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double OBPO28 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double OBPO29 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double OBPO30 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double OBPO31 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double OBPO32 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double OBPO33 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double OBPO34 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double OBPO35 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double OBPO36 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double OBPO46 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double OBPO47 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double OBPO48 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double OBPO49 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double OBPO50 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double OBPO51 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double OBPO52 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double OBPO53 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double OBPO54 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double OBPO55 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double OBPO56 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double OBPO66 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double OBPO67 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double OBPO68 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double OBPO69 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double OBPO70 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double OBPO71 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double OBPO72 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double OBPO73 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double OBPO74 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double OBPO75 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double OBPO76 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double OBPO86 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double OBPO87 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double OBPO88 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double OBPO89 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double OBPO90 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double OBPO91 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double OBPO92 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double OBPO93 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double OBPO94 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double OBPO95 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double OBPO96 { get; set; } // double precision
    }

    [Table("OBPOP")]
    public partial class OBPOP
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int OBPOP1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int OBPOP2 { get; set; } // integer
    }

    [Table("OBSH")]
    public partial class OBSH
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int OBSH1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int OBSH2 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string OBSH3 { get; set; } // varchar(50)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short OBSH4 { get; set; } // smallint
    }

    [Table("OBSHK")]
    public partial class OBSHK
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int OBSHK1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int OBSHK2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short OBSHK3 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string OBSHK4 { get; set; } // varchar(20)
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string OBSHK5 { get; set; } // varchar(20)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short OBSHK6 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string OBSHK9 { get; set; } // varchar(100)
    }

    [Table("OBST")]
    public partial class OBST
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int OBST1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int OBST2 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int OBST3 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime OBST4 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? OBST5 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string OBST6 { get; set; } // varchar(50)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int OBST7 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short OBST9 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int OBST10 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int OBST11 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime OBST12 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int OBST13 { get; set; } // integer
    }

    [Table("OBZ")]
    public partial class OBZ
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int OBZ1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string OBZ2 { get; set; } // varchar(100)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int OBZ3 { get; set; } // integer
    }

    [Table("ODP")]
    public partial class ODP
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int ODP1 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), PrimaryKey(1), NotNull]
        public DateTime ODP2 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 1), PrimaryKey(2), NotNull]
        public char ODP3 { get; set; } // varchar(1)
        [Column(DataType = DataType.Int16, Length = 2), PrimaryKey(3), NotNull]
        public short ODP4 { get; set; } // smallint
    }

    [Table("OK")]
    public partial class OK
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int OK1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 35), NotNull]
        public string OK2 { get; set; } // varchar(35)
        [Column(DataType = DataType.NVarChar, Length = 35), NotNull]
        public string OK3 { get; set; } // varchar(35)
        [Column(DataType = DataType.NVarChar, Length = 35), NotNull]
        public string OK4 { get; set; } // varchar(35)
        [Column(DataType = DataType.NVarChar, Length = 35), NotNull]
        public string OK5 { get; set; } // varchar(35)
        [Column(DataType = DataType.NVarChar, Length = 35), NotNull]
        public string OK6 { get; set; } // varchar(35)
        [Column(DataType = DataType.NVarChar, Length = 35), NotNull]
        public string OK7 { get; set; } // varchar(35)
        [Column(DataType = DataType.NVarChar, Length = 35), NotNull]
        public string OK8 { get; set; } // varchar(35)
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char OK9 { get; set; } // varchar(1)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short OK10 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char OK11 { get; set; } // varchar(1)
        [Column(DataType = DataType.NVarChar, Length = 35), NotNull]
        public string OK12 { get; set; } // varchar(35)
        [Column(DataType = DataType.NVarChar, Length = 35), NotNull]
        public string OK13 { get; set; } // varchar(35)
    }

    [Table("OM")]
    public partial class OM
    {
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double OM1 { get; set; } // double precision
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime OM2 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? OM3 { get; set; } // timestamp
    }

    [Table("ORG")]
    public partial class ORG
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int ORG1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 250), NotNull]
        public string ORG2 { get; set; } // varchar(250)
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string ORG3 { get; set; } // varchar(50)
        [Column(DataType = DataType.NVarChar, Length = 250), NotNull]
        public string ORG4 { get; set; } // varchar(250)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ORG5 { get; set; } // smallint
    }

    [Table("OSPS")]
    public partial class OSPS
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int OSPS1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int OSPS2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short OSPS3 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short OSPS4 { get; set; } // smallint
        [Column(DataType = DataType.Text, Length = int.MaxValue), Nullable]
        public string OSPS5 { get; set; } // blob sub_type 1
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short OSPS6 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int OSPS7 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short OSPS8 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int OSPS9 { get; set; } // integer
    }

    [Table("OTP")]
    public partial class OTP
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int OTP1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string OTP2 { get; set; } // varchar(100)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short OTP3 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? OTP4 { get; set; } // timestamp
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short OTP5 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int OTP6 { get; set; } // integer
    }

    [Table("OTPV")]
    public partial class OTPV
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int OTPV1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string OTPV2 { get; set; } // varchar(100)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short OTPV3 { get; set; } // smallint
    }

    [Table("P")]
    public partial class P
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int P1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 35), NotNull]
        public string P3 { get; set; } // varchar(35)
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string P4 { get; set; } // varchar(20)
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string P5 { get; set; } // varchar(20)
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string P6 { get; set; } // varchar(20)
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string P7 { get; set; } // varchar(20)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short P8 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime P9 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string P10 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string P11 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string P12 { get; set; } // varchar(15)
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string P13 { get; set; } // varchar(20)
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string P14 { get; set; } // varchar(20)
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string P15 { get; set; } // varchar(10)
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string P16 { get; set; } // varchar(15)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string P17 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string P18 { get; set; } // varchar(10)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short P19 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short P20 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string P21 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string P22 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 30), NotNull]
        public string P23 { get; set; } // varchar(30)
        [Column(DataType = DataType.NVarChar, Length = 300), NotNull]
        public string P24 { get; set; } // varchar(300)
        [Column(DataType = DataType.NVarChar, Length = 35), NotNull]
        public string P27 { get; set; } // varchar(35)
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string P33 { get; set; } // varchar(25)
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string P34 { get; set; } // varchar(25)
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string P35 { get; set; } // varchar(25)
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string P36 { get; set; } // varchar(25)
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string P37 { get; set; } // varchar(25)
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string P38 { get; set; } // varchar(25)
        [Column(DataType = DataType.NVarChar, Length = 75), NotNull]
        public string P39 { get; set; } // varchar(75)
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string P40 { get; set; } // varchar(25)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short P48 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? P49 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 30), NotNull]
        public string P50 { get; set; } // varchar(30)
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string P51 { get; set; } // varchar(150)
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string P52 { get; set; } // varchar(150)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string P53 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 35), NotNull]
        public string P54 { get; set; } // varchar(35)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short P55 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string P56 { get; set; } // varchar(10)
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string P57 { get; set; } // varchar(15)
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char P58 { get; set; } // varchar(1)
        [Column(DataType = DataType.NVarChar, Length = 56), NotNull]
        public string P59 { get; set; } // varchar(56)
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string P60 { get; set; } // varchar(25)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int P61 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime P62 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? P63 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string P64 { get; set; } // varchar(25)
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string P65 { get; set; } // varchar(15)
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string P66 { get; set; } // varchar(10)
        [Column(DataType = DataType.NVarChar, Length = 5), NotNull]
        public string P67 { get; set; } // varchar(5)
        [Column(DataType = DataType.NVarChar, Length = 5), NotNull]
        public string P68 { get; set; } // varchar(5)
        [Column(DataType = DataType.NVarChar, Length = 6), NotNull]
        public string P74 { get; set; } // varchar(6)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int P75 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 35), NotNull]
        public string P76 { get; set; } // varchar(35)
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string P77 { get; set; } // varchar(20)
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string P78 { get; set; } // varchar(20)
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string P79 { get; set; } // varchar(25)
        [Column(DataType = DataType.NVarChar, Length = 45), NotNull]
        public string P80 { get; set; } // varchar(45)
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string P81 { get; set; } // varchar(50)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short P82 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short P83 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short P84 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short P85 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short P86 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short P87 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string P88 { get; set; } // varchar(150)
        [Column(DataType = DataType.NVarChar, Length = 75), NotNull]
        public string P89 { get; set; } // varchar(75)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short P90 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short P91 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string P92 { get; set; } // varchar(150)
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string P93 { get; set; } // varchar(150)
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string P94 { get; set; } // varchar(150)
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string P95 { get; set; } // varchar(150)
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string P96 { get; set; } // varchar(150)
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string P97 { get; set; } // varchar(150)
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string P98 { get; set; } // varchar(150)
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string P99 { get; set; } // varchar(150)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short P100 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 35), NotNull]
        public string P102 { get; set; } // varchar(35)
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string P103 { get; set; } // varchar(20)
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string P104 { get; set; } // varchar(20)
        [Column(DataType = DataType.NVarChar, Length = 35), NotNull]
        public string P105 { get; set; } // varchar(35)
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string P106 { get; set; } // varchar(20)
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string P107 { get; set; } // varchar(20)
        [Column(DataType = DataType.NVarChar, Length = 35), NotNull]
        public string P108 { get; set; } // varchar(35)
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string P109 { get; set; } // varchar(20)
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string P110 { get; set; } // varchar(20)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? P111 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? P112 { get; set; } // timestamp
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short P113 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short P114 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short P115 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short P116 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short P117 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short P118 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short P119 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 35), NotNull]
        public string P121 { get; set; } // varchar(35)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short P122 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short P123 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int P124 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short P125 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short P126 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string P127 { get; set; } // varchar(50)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short P128 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short P129 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? P130 { get; set; } // timestamp
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short P131 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int P132 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string P41 { get; set; } // varchar(200)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string P42 { get; set; } // varchar(200)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string P43 { get; set; } // varchar(200)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string P44 { get; set; } // varchar(200)
        [Column(DataType = DataType.NVarChar, Length = 300), NotNull]
        public string P45 { get; set; } // varchar(300)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string P46 { get; set; } // varchar(200)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string P47 { get; set; } // varchar(200)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short P120 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short P133 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string P134 { get; set; } // varchar(15)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? P135 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string P136 { get; set; } // varchar(150)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? P137 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? P138 { get; set; } // timestamp
    }

    [Table("PAA")]
    public partial class PAA
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int PAA1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string PAA2 { get; set; } // varchar(10)
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string PAA3 { get; set; } // varchar(10)
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char PAA4 { get; set; } // varchar(1)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string PAA5 { get; set; } // varchar(100)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PAA6 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PAA7 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PAA8 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PAA9 { get; set; } // smallint
    }

    [Table("PAAN")]
    public partial class PAAN
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int PAAN1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string PAAN2 { get; set; } // varchar(10)
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string PAAN3 { get; set; } // varchar(10)
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char PAAN4 { get; set; } // varchar(1)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string PAAN5 { get; set; } // varchar(100)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PAAN6 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PAAN7 { get; set; } // smallint
    }

    [Table("PAANP")]
    public partial class PAANP
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int PAANP1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int PAANP2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PAANP3 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PAANP4 { get; set; } // smallint
    }

    [Table("PAAP")]
    public partial class PAAP
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int PAAP1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int PAAP2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PAAP3 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PAAP4 { get; set; } // smallint
    }

    [Table("PB")]
    public partial class PB
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int PB1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PB2 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? PB3 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? PB4 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string PB5 { get; set; } // varchar(20)
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string PB6 { get; set; } // varchar(150)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string PB7 { get; set; } // varchar(200)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PB8 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime PB9 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PB10 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PB11 { get; set; } // integer
    }

    [Table("PBS")]
    public partial class PBS
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int PBS1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string PBS2 { get; set; } // varchar(50)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? PBS3 { get; set; } // timestamp
    }

    [Table("PD")]
    public partial class PD
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int PD1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PD2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PD3 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PD4 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PD5 { get; set; } // integer
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double PD6 { get; set; } // double precision
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PD7 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PD8 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PD9 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string PD10 { get; set; } // varchar(25)
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime PD11 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? PD12 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? PD13 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string PD15 { get; set; } // varchar(50)
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char PD16 { get; set; } // varchar(1)
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char PD17 { get; set; } // varchar(1)
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char PD18 { get; set; } // varchar(1)
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char PD19 { get; set; } // varchar(1)
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char PD20 { get; set; } // varchar(1)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PD21 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PD28 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PD31 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string PD33 { get; set; } // varchar(150)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PD34 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PD38 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PD39 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char PD40 { get; set; } // varchar(1)
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char PD41 { get; set; } // varchar(1)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PD43 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime PD44 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PD45 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PD46 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PD47 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char PD48 { get; set; } // varchar(1)
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char PD49 { get; set; } // varchar(1)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PD50 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PD51 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PD52 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PD53 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string PD54 { get; set; } // varchar(150)
    }

    [Table("PDIS")]
    public partial class PDIS
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int PDIS1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int PDIS2 { get; set; } // integer
    }

    [Table("PDP")]
    public partial class PDP
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int PDP1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PDP2 { get; set; } // integer
        [Column(DataType = DataType.Text, Length = int.MaxValue), Nullable]
        public string PDP3 { get; set; } // blob sub_type 1
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string PDP4 { get; set; } // varchar(50)
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string PDP5 { get; set; } // varchar(15)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? PDP6 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PDP7 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string PDP8 { get; set; } // varchar(25)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PDP9 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime PDP10 { get; set; } // timestamp
    }

    [Table("PE")]
    public partial class PE
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int PE1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 35), NotNull]
        public string PE2 { get; set; } // varchar(35)
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string PE3 { get; set; } // varchar(50)
        [Column(DataType = DataType.NVarChar, Length = 35), NotNull]
        public string PE4 { get; set; } // varchar(35)
        [Column(DataType = DataType.NVarChar, Length = 35), NotNull]
        public string PE5 { get; set; } // varchar(35)
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string PE6 { get; set; } // varchar(20)
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string PE7 { get; set; } // varchar(20)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PE8 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime PE9 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 75), NotNull]
        public string PE10 { get; set; } // varchar(75)
        [Column(DataType = DataType.NVarChar, Length = 75), NotNull]
        public string PE11 { get; set; } // varchar(75)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PE12 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string PE20 { get; set; } // varchar(15)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PE21 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string PE22 { get; set; } // varchar(10)
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string PE23 { get; set; } // varchar(25)
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string PE24 { get; set; } // varchar(150)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? PE25 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PE30 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime PE31 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PE32 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PE33 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string PE34 { get; set; } // varchar(50)
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string PE35 { get; set; } // varchar(50)
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string PE36 { get; set; } // varchar(50)
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string PE37 { get; set; } // varchar(50)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PE50 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? PE51 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? PE52 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string PE53 { get; set; } // varchar(100)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PE54 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PE59 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? PE60 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? PE61 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char PE62 { get; set; } // varchar(1)
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string PE63 { get; set; } // varchar(15)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PE64 { get; set; } // smallint
    }

    [Table("PEADR")]
    public partial class PEADR
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int PEADR1 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), PrimaryKey(1), NotNull]
        public short PEADR2 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string PEADR3 { get; set; } // varchar(15)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PEADR4 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string PEADR5 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 8), NotNull]
        public string PEADR6 { get; set; } // varchar(8)
        [Column(DataType = DataType.NVarChar, Length = 5), NotNull]
        public string PEADR7 { get; set; } // varchar(5)
        [Column(DataType = DataType.NVarChar, Length = 5), NotNull]
        public string PEADR8 { get; set; } // varchar(5)
    }

    [Table("PEFIO")]
    public partial class PEFIO
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int PEFIO1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int PEFIO2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PEFIO3 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? PEFIO4 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 35), NotNull]
        public string PEFIO5 { get; set; } // varchar(35)
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string PEFIO6 { get; set; } // varchar(50)
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string PEFIO7 { get; set; } // varchar(20)
    }

    [Table("PEN")]
    public partial class PEN
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int PEN1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string PEN2 { get; set; } // varchar(50)
    }

    [Table("PER")]
    public partial class PER
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int PER1 { get; set; } // integer
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double PER2 { get; set; } // double precision
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string PER3 { get; set; } // varchar(200)
    }

    [Table("PESEM")]
    public partial class PESEM
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int PESEM1 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), PrimaryKey(1), NotNull]
        public short PESEM2 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string PESEM3 { get; set; } // varchar(50)
        [Column(DataType = DataType.NVarChar, Length = 75), NotNull]
        public string PESEM4 { get; set; } // varchar(75)
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string PESEM5 { get; set; } // varchar(25)
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string PESEM6 { get; set; } // varchar(50)
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string PESEM7 { get; set; } // varchar(10)
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string PESEM8 { get; set; } // varchar(25)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string PESEM9 { get; set; } // varchar(100)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? PESEM10 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? PESEM11 { get; set; } // timestamp
    }

    [Table("PKV")]
    public partial class PKV
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int PKV1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PKV2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PKV3 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string PKV4 { get; set; } // varchar(200)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PKV5 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string PKV6 { get; set; } // varchar(200)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PKV7 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime PKV8 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PKV9 { get; set; } // integer
    }

    [Table("PKVD")]
    public partial class PKVD
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int PKVD1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PKVD2 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string PKVD3 { get; set; } // varchar(200)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? PKVD4 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? PKVD5 { get; set; } // timestamp
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PKVD6 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PKVD7 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string PKVD8 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string PKVD9 { get; set; } // varchar(100)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PKVD10 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime PKVD11 { get; set; } // timestamp
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PKVD12 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PKVD13 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PKVD14 { get; set; } // integer
    }

    [Table("PKVE")]
    public partial class PKVE
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int PKVE1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PKVE2 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string PKVE3 { get; set; } // varchar(200)
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string PKVE4 { get; set; } // varchar(50)
    }

    [Table("PKVG")]
    public partial class PKVG
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int PKVG1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PKVG2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PKVG3 { get; set; } // smallint
    }

    [Table("PL")]
    public partial class PL
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int PL1 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), PrimaryKey(1), NotNull]
        public short PL2 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime PL3 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? PL4 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string PL5 { get; set; } // varchar(50)
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string PL6 { get; set; } // varchar(25)
    }

    [Table("PM")]
    public partial class PM
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int PM1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string PM2 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string PM3 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string PM4 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string PM5 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 250), NotNull]
        public string PM6 { get; set; } // varchar(250)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PM7 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PM8 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PM9 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string PM10 { get; set; } // varchar(20)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PM11 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PM12 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PM13 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PM14 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PM15 { get; set; } // smallint
    }

    [Table("PMC")]
    public partial class PMC
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int PMC0 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PMC1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PMC2 { get; set; } // integer
    }

    [Table("PMG")]
    public partial class PMG
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int PMG1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string PMG2 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string PMG3 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string PMG4 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string PMG5 { get; set; } // varchar(100)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PMG6 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PMG7 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PMG8 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string PMG9 { get; set; } // varchar(100)
    }

    [Table("PNA")]
    public partial class PNA
    {
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PNA1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PNA2 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? PNA3 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string PNA4 { get; set; } // varchar(25)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string PNA5 { get; set; } // varchar(100)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PNA6 { get; set; } // integer
    }

    [Table("PND")]
    public partial class PND
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int PND1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PND2 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PND3 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PND4 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime PND5 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? PND6 { get; set; } // timestamp
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PND7 { get; set; } // smallint
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double PND8 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double PND9 { get; set; } // double precision
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PND10 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PND11 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime PND12 { get; set; } // timestamp
    }

    [Table("PNSP")]
    public partial class PNSP
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int PNSP1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string PNSP2 { get; set; } // varchar(150)
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string PNSP3 { get; set; } // varchar(25)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PNSP4 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PNSP5 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime PNSP7 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PNSP8 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? PNSP9 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string PNSP10 { get; set; } // varchar(25)
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string PNSP11 { get; set; } // varchar(25)
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string PNSP12 { get; set; } // varchar(25)
        [Column(DataType = DataType.NVarChar, Length = 250), NotNull]
        public string PNSP13 { get; set; } // varchar(250)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PNSP14 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PNSP15 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string PNSP17 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string PNSP18 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string PNSP19 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string PNSP20 { get; set; } // varchar(150)
    }

    [Table("PO")]
    public partial class PO
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int PO1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PO2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PO3 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PO4 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PO5 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime PO6 { get; set; } // timestamp
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PO14 { get; set; } // smallint
    }

    [Table("POBR")]
    public partial class POBR
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int POBR1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int POBR2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short POBR3 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short POBR4 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string POBR5 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string POBR6 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 30), NotNull]
        public string POBR7 { get; set; } // varchar(30)
        [Column(DataType = DataType.NVarChar, Length = 300), NotNull]
        public string POBR8 { get; set; } // varchar(300)
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string POBR9 { get; set; } // varchar(10)
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string POBR10 { get; set; } // varchar(15)
    }

    [Table("POD")]
    public partial class POD
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int POD1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int POD2 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? POD3 { get; set; } // timestamp
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short POD4 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? POD5 { get; set; } // timestamp
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short POD6 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short POD7 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string POD8 { get; set; } // varchar(50)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int POD9 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime POD10 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int POD11 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? POD12 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? POD13 { get; set; } // timestamp
    }

    [Table("PORTAL_SETTINGS")]
    public partial class PORTAL_SETTINGS
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int PS1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 1500), NotNull]
        public string PS2 { get; set; } // varchar(1500)
    }

    [Table("PP")]
    public partial class PP
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int PP1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PP2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PP3 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PP4 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime PP5 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PP6 { get; set; } // integer
    }

    [Table("PPV")]
    public partial class PPV
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int PPV1 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), PrimaryKey(1), NotNull]
        public short PPV2 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PPV3 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PPV4 { get; set; } // smallint
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double PPV5 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double PPV6 { get; set; } // double precision
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PPV7 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime PPV8 { get; set; } // timestamp
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PPV9 { get; set; } // smallint
    }

    [Table("PPZV")]
    public partial class PPZV
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int PPZV1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PPZV2 { get; set; } // integer
        [Column(DataType = DataType.Text, Length = int.MaxValue), Nullable]
        public string PPZV3 { get; set; } // blob sub_type 1
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PPZV4 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PPZV5 { get; set; } // integer
    }

    [Table("PPZVV")]
    public partial class PPZVV
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int PPZVV1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string PPZVV2 { get; set; } // varchar(50)
    }

    [Table("PROTZ")]
    public partial class PROTZ
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int PROTZ1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 75), NotNull]
        public string PROTZ2 { get; set; } // varchar(75)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PROTZ3 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PROTZ4 { get; set; } // smallint
    }

    [Table("PRPD")]
    public partial class PRPD
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int PRPD1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string PRPD2 { get; set; } // varchar(50)
    }
    [Table("PRA")]
    public partial class PRA
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int PRA1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PRA2 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PRA3 { get; set; } // integer
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double PRA4 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double PRA5 { get; set; } // double precision
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PRA6 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PRA7 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PRA8 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PRA9 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PRA10 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PRA11 { get; set; } // smallint
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double PRA12 { get; set; } // double precision
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PRA13 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PRA14 { get; set; } // smallint
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double PRA15 { get; set; } // double precision
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PRA16 { get; set; } // smallint
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double PRA17 { get; set; } // double precision
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PRA18 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PRA19 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PRA20 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PRA21 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PRA22 { get; set; } // integer
    }
    [Table("PRU")]
    public partial class PRU
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int PRU1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PRU2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PRU3 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PRU4 { get; set; } // smallint
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double PRU5 { get; set; } // double precision
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PRU6 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PRU7 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PRU8 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PRU9 { get; set; } // smallint
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double PRU10 { get; set; } // double precision
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char PRU11 { get; set; } // varchar(1)
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char PRU12 { get; set; } // varchar(1)
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double PRU13 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double PRU14 { get; set; } // double precision
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PRU15 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? PRU16 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? PRU17 { get; set; } // timestamp
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double PRU18 { get; set; } // double precision
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PRU19 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PRA20 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PRA21 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PRA22 { get; set; } // integer
    }

    [Table("PRUN")]
    public partial class PRUN
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int PRUN0 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PRUN1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PRUN2 { get; set; } // integer
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double PRUN3 { get; set; } // double precision
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime PRUN4 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PRUN5 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PRUN6 { get; set; } // smallint
    }

    [Table("PRUST")]
    public partial class PRUST
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int PRUST0 { get; set; } // integer        
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PRUST2 { get; set; } // integer        
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PRUST4 { get; set; } // integer        
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime PRUST9 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PRUST10 { get; set; } // integer   
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PRUST17 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PRUST18 { get; set; } // integer
    }

    [Table("PRV")]
    public partial class PRV
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int PRV1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PRV2 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PRV3 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PRV4 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime PRV5 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PRV6 { get; set; } // integer
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double PRV7 { get; set; } // double precision
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PRV8 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PRV9 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime PRV10 { get; set; } // timestamp
    }

    [Table("PS")]
    public partial class PS
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int PS1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 150), PrimaryKey(1), NotNull]
        public string PS2 { get; set; } // varchar(150)
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char PS3 { get; set; } // varchar(1)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? PS4 { get; set; } // timestamp
    }

    [Table("PSTO")]
    public partial class PSTO
    {
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PSTO1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PSTO2 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 1500), NotNull]
        public string PSTO3 { get; set; } // varchar(1500)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? PSTO4 { get; set; } // timestamp
    }

    [Table("PUS")]
    public partial class PUS
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int PUS1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PUS2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PUS3 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PUS4 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 35), NotNull]
        public string PUS5 { get; set; } // varchar(35)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? PUS6 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PUS7 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PUS8 { get; set; } // smallint
    }

    [Table("PUZ")]
    public partial class PUZ
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int PUZ1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PUZ2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PUZ3 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PUZ4 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 35), NotNull]
        public string PUZ5 { get; set; } // varchar(35)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? PUZ6 { get; set; } // timestamp
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PUZ7 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string PUZ8 { get; set; } // varchar(100)
    }

    [Table("PVK")]
    public partial class PVK
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int PVK1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PVK2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PVK3 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PVK4 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PVK5 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PVK6 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PVK7 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 35), NotNull]
        public string PVK8 { get; set; } // varchar(35)
    }

    [Table("PVVD")]
    public partial class PVVD
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int PVVD1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PVVD2 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int PVVD3 { get; set; } // integer       
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PVVD6 { get; set; } // varchar(200)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PVVD7 { get; set; } // varchar(50)
        [Column(DataType = DataType.VarChar, Length = 250), NotNull]
        public string PVVD8 { get; set; } // smallint
        [Column(DataType = DataType.VarChar, Length = 250), NotNull]
        public string PVVD9 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PVVD10 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double PVVD11 { get; set; } // smallint
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double PVVD12 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PVVD15 { get; set; } // double precision
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PVVD16 { get; set; } // double precision
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PVVD17 { get; set; } // double precision
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string PVVD18 { get; set; } // varchar(200)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short PVVD19 { get; set; } // double precision
    }

    [Table("PY")]
    public partial class PY
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int PY1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int PY2 { get; set; } // integer
    }

    [Table("QT")]
    public partial class QT
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int QT1 { get; set; } // integer
        [Column(DataType = DataType.Blob, Length = int.MaxValue), NotNull]
        public byte[] QT2 { get; set; } // blob
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short QT3 { get; set; } // smallint
        [Column(DataType = DataType.Blob, Length = int.MaxValue), NotNull]
        public byte[] QT4 { get; set; } // blob
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short QT5 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short QT6 { get; set; } // smallint
    }

    [Table("QUAL")]
    public partial class QUAL
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int QUAL1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 250), NotNull]
        public string QUAL2 { get; set; } // varchar(250)
        [Column(DataType = DataType.NVarChar, Length = 250), NotNull]
        public string QUAL3 { get; set; } // varchar(250)
        [Column(DataType = DataType.NVarChar, Length = 250), NotNull]
        public string QUAL4 { get; set; } // varchar(250)
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string QUAL5 { get; set; } // varchar(150)
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string QUAL6 { get; set; } // varchar(150)
        [Column(DataType = DataType.NVarChar, Length = 350), NotNull]
        public string QUAL7 { get; set; } // varchar(350)
        [Column(DataType = DataType.NVarChar, Length = 350), NotNull]
        public string QUAL8 { get; set; } // varchar(350)
    }

    [Table("R")]
    public partial class R
    {
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int R1 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), PrimaryKey(0), NotNull]
        public DateTime R2 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int R4 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(2), NotNull]
        public int R5 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short R6 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), PrimaryKey(3), NotNull]
        public short R7 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int R9 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int R10 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? R11 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int R8 { get; set; } // integer
    }

    [Table("RA")]
    public partial class RA
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int RA1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int RA2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), PrimaryKey(2), NotNull]
        public short RA3 { get; set; } // smallint
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double RA4 { get; set; } // double precision
    }

    [Table("ROP")]
    public partial class ROP
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int ROP1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string ROP2 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string ROP3 { get; set; } // varchar(10)
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime ROP4 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ROP5 { get; set; } // integer
    }

    [Table("RSP")]
    public partial class RSP
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int RSP1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string RSP2 { get; set; } // varchar(50)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int RSP3 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? RSP4 { get; set; } // timestamp
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short RSP5 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short RSP6 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short RSP7 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short RSP8 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short RSP9 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short RSP10 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short RSP11 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short RSP12 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short RSP13 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short RSP14 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short RSP15 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short RSP16 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short RSP17 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short RSP18 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short RSP19 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short RSP20 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short RSP21 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short RSP22 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int RSP23 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int RSP24 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? RSP25 { get; set; } // timestamp
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short RSP26 { get; set; } // smallint
    }

    [Table("RSPSG")]
    public partial class RSPSG
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int RSPSG1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int RSPSG2 { get; set; } // integer
    }

    [Table("RSPST")]
    public partial class RSPST
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int RSPST1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int RSPST2 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int RSPST3 { get; set; } // integer
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double RSPST4 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double RSPST5 { get; set; } // double precision
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int RSPST6 { get; set; } // integer
    }

    [Table("RT")]
    public partial class RT
    {
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int RT1 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), PrimaryKey(0), NotNull]
        public DateTime RT2 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int RT4 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(2), NotNull]
        public int RT5 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short RT6 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), PrimaryKey(3), NotNull]
        public short RT7 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int RT9 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int RT10 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? RT11 { get; set; } // timestamp
    }

    [Table("RZ")]
    public partial class RZ
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int RZ1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 5), NotNull]
        public string RZ2 { get; set; } // varchar(5)
        [Column(DataType = DataType.NVarChar, Length = 5), NotNull]
        public string RZ3 { get; set; } // varchar(5)
        [Column(DataType = DataType.NVarChar, Length = 5), NotNull]
        public string RZ4 { get; set; } // varchar(5)
        [Column(DataType = DataType.NVarChar, Length = 5), NotNull]
        public string RZ5 { get; set; } // varchar(5)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short RZ6 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short RZ7 { get; set; } // smallint
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double RZ8 { get; set; } // double precision
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short RZ9 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short RZ10 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short RZ11 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short RZ12 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short RZ13 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short RZ14 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short RZ15 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int RZ16 { get; set; } // integer
    }

    [Table("SDIGPT")]
    public partial class SDIGPT
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int SDIGPT1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 35), NotNull]
        public string SDIGPT2 { get; set; } // varchar(35)
        [Column(DataType = DataType.Text, Length = int.MaxValue), Nullable]
        public string SDIGPT3 { get; set; } // blob sub_type 1
        [Column(DataType = DataType.Text, Length = int.MaxValue), Nullable]
        public string SDIGPT4 { get; set; } // blob sub_type 1
        [Column(DataType = DataType.Text, Length = int.MaxValue), Nullable]
        public string SDIGPT5 { get; set; } // blob sub_type 1
        [Column(DataType = DataType.Text, Length = int.MaxValue), Nullable]
        public string SDIGPT6 { get; set; } // blob sub_type 1
        [Column(DataType = DataType.Text, Length = int.MaxValue), Nullable]
        public string SDIGPT7 { get; set; } // blob sub_type 1
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string SDIGPT8 { get; set; } // varchar(10)
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string SDIGPT9 { get; set; } // varchar(10)
    }

    [Table("SDN")]
    public partial class SDN
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int SDN1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string SDN2 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 30), NotNull]
        public string SDN3 { get; set; } // varchar(30)
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double SDN4 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double SDN5 { get; set; } // double precision
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SDN6 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SDN7 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SDN8 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? SDN9 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime SDN10 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SDN11 { get; set; } // integer
    }

    [Table("SDNB")]
    public partial class SDNB
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int SDNB1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int SDNB2 { get; set; } // integer
    }

    [Table("SEKA")]
    public partial class SEKA
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int SEKA1 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), PrimaryKey(1), NotNull]
        public short SEKA2 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SEKA3 { get; set; } // smallint
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double SEKA4 { get; set; } // double precision
    }

    [Table("SEKAP")]
    public partial class SEKAP
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int SEKAP1 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), PrimaryKey(1), NotNull]
        public short SEKAP2 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SEKAP3 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SEKAP4 { get; set; } // smallint
    }

    [Table("SEM")]
    public partial class SEM
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int SEM1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SEM2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SEM3 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SEM4 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SEM5 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SEM7 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SEM9 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? SEM10 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? SEM11 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? SEM12 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? SEM13 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? SEM14 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? SEM15 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 1), Nullable]
        public char? SEM17 { get; set; } // varchar(1)
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime SEM18 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SEM19 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SEM20 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SEM21 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SEM22 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SEM23 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SEM24 { get; set; } // integer
    }

    [Table("SF")]
    public partial class SF
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int SF1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string SF2 { get; set; } // varchar(100)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SF3 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SF4 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SF5 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SF6 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SF7 { get; set; } // smallint
    }

    [Table("SFP")]
    public partial class SFP
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int SFP1 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), PrimaryKey(1), NotNull]
        public short SFP2 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), PrimaryKey(2), NotNull]
        public short SFP3 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SFP4 { get; set; } // smallint
    }

    [Table("SFY")]
    public partial class SFY
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int SFY1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 1000), NotNull]
        public string SFY2 { get; set; } // varchar(1000)
        [Column(DataType = DataType.Int16, Length = 2), PrimaryKey(1), NotNull]
        public short SFY3 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), PrimaryKey(2), NotNull]
        public short SFY4 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SFY5 { get; set; } // smallint
    }

    [Table("SG")]
    public partial class SG
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int SG1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SG2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SG3 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SG4 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SG6 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? SG7 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? SG8 { get; set; } // timestamp
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SG10 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SG11 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string SG12 { get; set; } // varchar(150)
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string SG13 { get; set; } // varchar(150)
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string SG14 { get; set; } // varchar(150)
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string SG15 { get; set; } // varchar(150)
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string SG16 { get; set; } // varchar(150)
        [Column(DataType = DataType.NVarChar, Length = 300), NotNull]
        public string SG17 { get; set; } // varchar(300)
        [Column(DataType = DataType.NVarChar, Length = 300), NotNull]
        public string SG18 { get; set; } // varchar(300)
        [Column(DataType = DataType.NVarChar, Length = 300), NotNull]
        public string SG19 { get; set; } // varchar(300)
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime SG20 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SG21 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime SG22 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime SG23 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime SG24 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime SG25 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime SG26 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime SG27 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string SG28 { get; set; } // varchar(15)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SG29 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SG30 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SG32 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SG33 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime SG34 { get; set; } // timestamp
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SG35 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SG36 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 75), NotNull]
        public string SG37 { get; set; } // varchar(75)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? SG38 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? SG39 { get; set; } // timestamp
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SG40 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SG41 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SG42 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SG43 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SG44 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SG45 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SG46 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SG47 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SG49 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SG50 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 250), NotNull]
        public string SG51 { get; set; } // varchar(250)
        [Column(DataType = DataType.NVarChar, Length = 250), NotNull]
        public string SG52 { get; set; } // varchar(250)
        [Column(DataType = DataType.NVarChar, Length = 250), NotNull]
        public string SG53 { get; set; } // varchar(250)
        [Column(DataType = DataType.NVarChar, Length = 250), NotNull]
        public string SG54 { get; set; } // varchar(250)
        [Column(DataType = DataType.Text, Length = int.MaxValue), Nullable]
        public string SG55 { get; set; } // blob sub_type 1
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string SG56 { get; set; } // varchar(150)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SG57 { get; set; } // smallint
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double SG48 { get; set; } // double precision        
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SG58 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 1000), NotNull]
        public string SG59 { get; set; } // varchar(1000)
        [Column(DataType = DataType.NVarChar, Length = 1000), NotNull]
        public string SG60 { get; set; } // varchar(1000)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SG61 { get; set; } // smallint
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double SG62 { get; set; } // double precision
    }

    [Table("SGOS")]
    public partial class SGOS
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int SGOS1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SGOS2 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string SGOS4 { get; set; } // varchar(25)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SGOS5 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 2), NotNull]
        public string SGOS6 { get; set; } // varchar(2)
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string SGOS7 { get; set; } // varchar(15)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? SGOS8 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string SGOS9 { get; set; } // varchar(150)
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime SGOS10 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SGOS11 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SGOS12 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SGOS13 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SGOS15 { get; set; } // integer
    }

    [Table("SGPR")]
    public partial class SGPR
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int SGPR1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string SGPR2 { get; set; } // varchar(25)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string SGPR3 { get; set; } // varchar(100)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SGPR4 { get; set; } // smallint
    }

    [Table("SGPRZ")]
    public partial class SGPRZ
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int SGPRZ1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int SGPRZ2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SGPRZ3 { get; set; } // smallint
    }

    [Table("SGR")]
    public partial class SGR
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int SGR1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 45), NotNull]
        public string SGR2 { get; set; } // varchar(45)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SGR3 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SGR4 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 45), NotNull]
        public string SGR5 { get; set; } // varchar(45)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string SGR6 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string SGR7 { get; set; } // varchar(100)
    }

    [Table("SGUP")]
    public partial class SGUP
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int SGUP1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string SGUP2 { get; set; } // varchar(50)
        [Column(DataType = DataType.NVarChar, Length = 5), NotNull]
        public string SGUP3 { get; set; } // varchar(5)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SGUP4 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SGUP5 { get; set; } // smallint
    }

    [Table("SH")]
    public partial class SH
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int SH1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string SH2 { get; set; } // varchar(50)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SH3 { get; set; } // integer
        [Column(DataType = DataType.Text, Length = int.MaxValue), Nullable]
        public string SH4 { get; set; } // blob sub_type 1
        [Column(DataType = DataType.Text, Length = int.MaxValue), Nullable]
        public string SH5 { get; set; } // blob sub_type 1
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SH6 { get; set; } // integer
        [Column(DataType = DataType.Text, Length = int.MaxValue), Nullable]
        public string SH7 { get; set; } // blob sub_type 1
        [Column(DataType = DataType.Text, Length = int.MaxValue), Nullable]
        public string SH8 { get; set; } // blob sub_type 1
        [Column(DataType = DataType.Text, Length = int.MaxValue), Nullable]
        public string SH9 { get; set; } // blob sub_type 1
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SH10 { get; set; } // smallint
    }

    [Table("SHF")]
    public partial class SHF
    {
        [Column(DataType = DataType.NVarChar, Length = 100), PrimaryKey, NotNull]
        public string SHF1 { get; set; } // varchar(100)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SHF2 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string SHF3 { get; set; } // varchar(100)
    }

    [Table("SHFN")]
    public partial class SHFN
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int SHFN1 { get; set; } // integer
        [Column(DataType = DataType.Blob, Length = int.MaxValue), NotNull]
        public byte[] SHFN2 { get; set; } // blob
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SHFN3 { get; set; } // integer
    }

    [Table("SHK")]
    public partial class SHK
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int SHK1 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), PrimaryKey(1), NotNull]
        public short SHK2 { get; set; } // smallint
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double SHK3 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double SHK4 { get; set; } // double precision
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SHK5 { get; set; } // smallint
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double SHK6 { get; set; } // double precision
    }

    [Table("SHR")]
    public partial class SHR
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int SHR1 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SHR2 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string SHR3 { get; set; } // varchar(100)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SHR4 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SHR5 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SHR6 { get; set; } // smallint
    }

    [Table("SHRD")]
    public partial class SHRD
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int SHRD1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int SHRD2 { get; set; } // integer
    }

    [Table("SHRP")]
    public partial class SHRP
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int SHRP1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int SHRP2 { get; set; } // integer
    }

    [Table("SK")]
    public partial class SK
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int SK1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char SK3 { get; set; } // varchar(1)
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string SK6 { get; set; } // varchar(20)
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string SK7 { get; set; } // varchar(50)
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime SK10 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SK11 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? SK12 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string SK13 { get; set; } // varchar(20)
    }

    [Table("SL")]
    public partial class SL
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int SL1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 1000), NotNull]
        public string SL2 { get; set; } // varchar(1000)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SL3 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime SL4 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? SL5 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SL7 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SL8 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SL10 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string SL11 { get; set; } // varchar(15)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SL9 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SL12 { get; set; } // integer
    }

    [Table("SLDOC")]
    public partial class SLDOC
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int SLDOC1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SLDOC2 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 30), NotNull]
        public string SLDOC3 { get; set; } // varchar(30)
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string SLDOC4 { get; set; } // varchar(15)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? SLDOC5 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? SLDOC6 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string SLDOC7 { get; set; } // varchar(50)
    }

    [Table("SLFL")]
    public partial class SLFL
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int SLFL1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 250), NotNull]
        public string SLFL2 { get; set; } // varchar(250)
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string SLFL3 { get; set; } // varchar(10)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SLFL4 { get; set; } // smallint
    }

    [Table("SLK")]
    public partial class SLK
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int SLK1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 250), NotNull]
        public string SLK2 { get; set; } // varchar(250)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SLK3 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SLK4 { get; set; } // smallint
    }

    [Table("SLKG")]
    public partial class SLKG
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int SLKG1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string SLKG2 { get; set; } // varchar(150)
    }

    [Table("SLO")]
    public partial class SLO
    {
        [Column(DataType = DataType.NVarChar, Length = 200), PrimaryKey(0), NotNull]
        public string SLO1 { get; set; } // varchar(200)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string SLO2 { get; set; } // varchar(200)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string SLO3 { get; set; } // varchar(200)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string SLO4 { get; set; } // varchar(200)
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int SLO5 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SLO6 { get; set; } // smallint
    }

    [Table("SLYE")]
    public partial class SLYE
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int SLYE1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SLYE2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SLYE3 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SLYE4 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SLYE5 { get; set; } // integer
    }

    [Table("SLYEI")]
    public partial class SLYEI
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int SLYEI1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SLYEI2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SLYEI3 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SLYEI4 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SLYEI5 { get; set; } // integer
    }

    [Table("SLYEIT")]
    public partial class SLYEIT
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int SLYEIT1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string SLYEIT2 { get; set; } // varchar(20)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SLYEIT3 { get; set; } // smallint
    }

    [Table("SNAC")]
    public partial class SNAC
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int SNAC1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 45), NotNull]
        public string SNAC2 { get; set; } // varchar(45)
    }

    [Table("SNK")]
    public partial class SNK
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int SNK1 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? SNK2 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SNK3 { get; set; } // integer
    }

    [Table("SNKD")]
    public partial class SNKD
    {
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SNKD1 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SNKD2 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 75), NotNull]
        public string SNKD3 { get; set; } // varchar(75)
        [Column(DataType = DataType.NVarChar, Length = 350), NotNull]
        public string SNKD4 { get; set; } // varchar(350)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SNKD5 { get; set; } // smallint
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double SNKD6 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double SNKD7 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double SNKD8 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double SNKD9 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double SNKD10 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double SNKD11 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double SNKD12 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double SNKD13 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double SNKD14 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double SNKD15 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double SNKD16 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double SNKD17 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double SNKD18 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double SNKD19 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double SNKD20 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double SNKD21 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double SNKD22 { get; set; } // double precision
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SNKD23 { get; set; } // smallint
    }

    [Table("SO")]
    public partial class SO
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int SO0 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SO1 { get; set; } // integer
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double SO2 { get; set; } // double precision
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? SO3 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string SO4 { get; set; } // varchar(15)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SO5 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime SO6 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SO7 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? SO8 { get; set; } // timestamp
    }

    [Table("SOB")]
    public partial class SOB
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int SOB0 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SOB1 { get; set; } // integer
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double SOB2 { get; set; } // double precision
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? SOB3 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string SOB4 { get; set; } // varchar(25)
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime SOB5 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SOB6 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SOB7 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SOB8 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SOB9 { get; set; } // integer
    }

    [Table("SP")]
    public partial class SP
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int SP1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string SP2 { get; set; } // varchar(25)
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string SP4 { get; set; } // varchar(25)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SP5 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? SP7 { get; set; } // timestamp
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SP8 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 250), NotNull]
        public string SP9 { get; set; } // varchar(250)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SP11 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime SP12 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SP13 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SP14 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SP15 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 45), NotNull]
        public string SP16 { get; set; } // varchar(45)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string SP17 { get; set; } // varchar(200)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SP18 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SP19 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string SP20 { get; set; } // varchar(20)
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string SP21 { get; set; } // varchar(150)
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string SP22 { get; set; } // varchar(150)
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string SP23 { get; set; } // varchar(150)
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string SP24 { get; set; } // varchar(150)
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string SP25 { get; set; } // varchar(150)
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string SP26 { get; set; } // varchar(150)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SP27 { get; set; } // integer
    }

    [Table("SPAB")]
    public partial class SPAB
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int SPAB1 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SPAB2 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string SPAB3 { get; set; } // varchar(100)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SPAB4 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SPAB5 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SPAB6 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SPAB7 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string SPAB8 { get; set; } // varchar(15)
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string SPAB9 { get; set; } // varchar(15)
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string SPAB10 { get; set; } // varchar(15)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SPAB11 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SPAB12 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SPAB13 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 250), NotNull]
        public string SPAB14 { get; set; } // varchar(250)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SPAB15 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SPAB16 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SPAB17 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SPAB18 { get; set; } // smallint
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double SPAB20 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double SPAB21 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double SPAB22 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double SPAB23 { get; set; } // double precision
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SPAB24 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SPAB25 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 45), NotNull]
        public string SPAB26 { get; set; } // varchar(45)
        [Column(DataType = DataType.NVarChar, Length = 250), NotNull]
        public string SPAB27 { get; set; } // varchar(250)
        [Column(DataType = DataType.NVarChar, Length = 45), NotNull]
        public string SPAB28 { get; set; } // varchar(45)
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string SPAB29 { get; set; } // varchar(150)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SPAB31 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SPAB32 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SPAB33 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SPAB34 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SPAB35 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SPAB36 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SPAB37 { get; set; } // integer
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double SPAB38 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double SPAB39 { get; set; } // double precision
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string SPAB40 { get; set; } // varchar(20)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SPAB41 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SPAB42 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SPAB43 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 80), NotNull]
        public string SPAB44 { get; set; } // varchar(80)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SPAB45 { get; set; } // integer
    }

    [Table("SPABC")]
    public partial class SPABC
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int SPABC1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string SPABC2 { get; set; } // varchar(100)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? SPABC3 { get; set; } // timestamp
    }

    [Table("SPABD")]
    public partial class SPABD
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int SPABD1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int SPABD2 { get; set; } // integer
    }

    [Table("SPABE")]
    public partial class SPABE
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int SPABE1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int SPABE2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SPABE3 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? SPABE4 { get; set; } // timestamp
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SPABE5 { get; set; } // smallint
    }

    [Table("SPABK")]
    public partial class SPABK
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int SPABK1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int SPABK2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SPABK3 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SPABK4 { get; set; } // smallint
    }

    [Table("SPABP")]
    public partial class SPABP
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int SPABP1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 30), NotNull]
        public string SPABP2 { get; set; } // varchar(30)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SPABP3 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SPABP4 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string SPABP9 { get; set; } // varchar(100)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SPABP10 { get; set; } // smallint
    }

    [Table("SPABS")]
    public partial class SPABS
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int SPABS1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SPABS2 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string SPABS3 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string SPABS4 { get; set; } // varchar(15)
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string SPABS5 { get; set; } // varchar(15)
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string SPABS6 { get; set; } // varchar(15)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SPABS7 { get; set; } // integer
    }

    [Table("SPABV")]
    public partial class SPABV
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int SPABV1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SPABV2 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SPABV3 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string SPABV4 { get; set; } // varchar(100)
    }

    [Table("SPABVI")]
    public partial class SPABVI
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int SPABVI1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SPABVI2 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SPABVI3 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 30), NotNull]
        public string SPABVI4 { get; set; } // varchar(30)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? SPABVI5 { get; set; } // timestamp
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SPABVI6 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SPABVI7 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 30), NotNull]
        public string SPABVI8 { get; set; } // varchar(30)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string SPABVI9 { get; set; } // varchar(200)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SPABVI10 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime SPABVI11 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? SPABVI12 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? SPABVI13 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? SPABVI14 { get; set; } // timestamp
    }

    [Table("SPABVIZ")]
    public partial class SPABVIZ
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int SPABVIZ1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int SPABVIZ2 { get; set; } // integer
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double SPABVIZ3 { get; set; } // double precision
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string SPABVIZ4 { get; set; } // varchar(10)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SPABVIZ5 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime SPABVIZ6 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SPABVIZ7 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime SPABVIZ8 { get; set; } // timestamp
    }

    [Table("SPABZ")]
    public partial class SPABZ
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int SPABZ1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int SPABZ2 { get; set; } // integer
    }

    [Table("SPAN")]
    public partial class SPAN
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int SPAN1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string SPAN2 { get; set; } // varchar(100)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SPAN3 { get; set; } // integer
    }

    [Table("SPC")]
    public partial class SPC
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int SPC1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string SPC4 { get; set; } // varchar(150)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? SPC7 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 250), NotNull]
        public string SPC9 { get; set; } // varchar(250)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SPC11 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime SPC12 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SPC13 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string SPC14 { get; set; } // varchar(150)
    }

    [Table("SPEK")]
    public partial class SPEK
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int SPEK1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string SPEK2 { get; set; } // varchar(150)
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string SPEK3 { get; set; } // varchar(50)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SPEK4 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SPEK5 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SPEK6 { get; set; } // smallint
    }

    [Table("SPKR")]
    public partial class SPKR
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int SPKR1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 1000), NotNull]
        public string SPKR2 { get; set; } // varchar(1000)
        [Column(DataType = DataType.NVarChar, Length = 1000), NotNull]
        public string SPKR3 { get; set; } // varchar(1000)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SPKR4 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SPKR5 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SPKR6 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? SPKR7 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime SPKR8 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SPKR9 { get; set; } // integer

    }

    [Table("SPO")]
    public partial class SPO
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int SPO0 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SPO1 { get; set; } // integer
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double SPO2 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double SPO3 { get; set; } // double precision
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string SPO4 { get; set; } // varchar(50)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SPO5 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SPO6 { get; set; } // smallint
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double SPO7 { get; set; } // double precision
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime SPO8 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SPO9 { get; set; } // integer
    }

    [Table("SPOB")]
    public partial class SPOB
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int SPOB0 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SPOB1 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SPOB2 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SPOB3 { get; set; } // smallint
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double SPOB4 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double SPOB6 { get; set; } // double precision
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string SPOB7 { get; set; } // varchar(50)
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime SPOB8 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SPOB9 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char SPOB10 { get; set; } // varchar(1)
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double SPOB11 { get; set; } // double precision
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SPOB12 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SPOB13 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SPOB14 { get; set; } // integer
    }

    [Table("SPOL")]
    public partial class SPOL
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int SPOL1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string SPOL2 { get; set; } // varchar(100)
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double SPOL3 { get; set; } // double precision
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SPOL4 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SPOL5 { get; set; } // smallint
    }

    [Table("SPOLI")]
    public partial class SPOLI
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int SPOLI1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string SPOLI2 { get; set; } // varchar(100)
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double SPOLI3 { get; set; } // double precision
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SPOLI4 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SPOLI5 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SPOLI6 { get; set; } // smallint
    }

    [Table("SPOM")]
    public partial class SPOM
    {
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SPOM1 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SPOM2 { get; set; } // smallint
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double SPOM3 { get; set; } // double precision
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string SPOM4 { get; set; } // varchar(50)
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime SPOM5 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SPOM6 { get; set; } // integer
    }

    [Table("SPR")]
    public partial class SPR
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int SPR1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string SPR2 { get; set; } // varchar(150)
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double SPR3 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double SPR4 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double SPR5 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double SPR6 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double SPR7 { get; set; } // double precision
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? SPR8 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SPR9 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SPR10 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SPR11 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string SPR12 { get; set; } // varchar(150)
    }

    [Table("SPRA")]
    public partial class SPRA
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int SPRA1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SPRA2 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? SPRA3 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SPRA4 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string SPRA5 { get; set; } // varchar(25)
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string SPRA6 { get; set; } // varchar(25)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? SPRA7 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string SPRA8 { get; set; } // varchar(50)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SPRA9 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime SPRA10 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SPRA11 { get; set; } // integer
        [Column(DataType = DataType.Blob, Length = int.MaxValue), Nullable]
        public byte[] SPRA12 { get; set; } // blob
        [Column(DataType = DataType.Blob, Length = int.MaxValue), Nullable]
        public byte[] SPRA13 { get; set; } // blob
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? SPRA14 { get; set; } // timestamp
        [Column(DataType = DataType.Blob, Length = int.MaxValue), Nullable]
        public byte[] SPRA15 { get; set; } // blob
        [Column(DataType = DataType.Blob, Length = int.MaxValue), Nullable]
        public byte[] SPRA16 { get; set; } // blob
    }

    [Table("SPSP")]
    public partial class SPSP
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int SPSP1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SPSP2 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string SPSP3 { get; set; } // varchar(100)
    }

    [Table("SR")]
    public partial class SR
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int SR1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int SR2 { get; set; } // integer
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double SR3 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double SR4 { get; set; } // double precision
        [Column(DataType = DataType.DateTime, Length = 8), PrimaryKey(2), NotNull]
        public DateTime SR5 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SR6 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SR7 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SR8 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SR9 { get; set; } // integer
    }

    [Table("SRS")]
    public partial class SRS
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int SRS1 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SRS2 { get; set; } // smallint
    }

    [Table("SRV")]
    public partial class SRV
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int SRV1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string SRV2 { get; set; } // varchar(200)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? SRV3 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SRV4 { get; set; } // integer
    }

    [Table("SRVD")]
    public partial class SRVD
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int SRVD1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string SRVD2 { get; set; } // varchar(200)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SRVD3 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? SRVD4 { get; set; } // timestamp
    }

    [Table("SST")]
    public partial class SST
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int SST1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SST2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SST3 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? SST4 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SST5 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? SST6 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string SST7 { get; set; } // varchar(100)
    }

    [Table("ST")]
    public partial class ST
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int ST1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 35), NotNull]
        public string ST2 { get; set; } // varchar(35)
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string ST3 { get; set; } // varchar(50)
        [Column(DataType = DataType.NVarChar, Length = 35), NotNull]
        public string ST4 { get; set; } // varchar(35)
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string ST5 { get; set; } // varchar(15)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ST6 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime ST7 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 75), NotNull]
        public string ST8 { get; set; } // varchar(75)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ST9 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string ST10 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string ST11 { get; set; } // varchar(50)
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string ST12 { get; set; } // varchar(15)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string ST13 { get; set; } // varchar(100)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ST14 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string ST15 { get; set; } // varchar(15)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ST16 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string ST17 { get; set; } // varchar(10)
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string ST18 { get; set; } // varchar(25)
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string ST19 { get; set; } // varchar(150)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? ST20 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 8), NotNull]
        public string ST21 { get; set; } // varchar(8)
        [Column(DataType = DataType.NVarChar, Length = 5), NotNull]
        public string ST22 { get; set; } // varchar(5)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? ST23 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 35), NotNull]
        public string ST28 { get; set; } // varchar(35)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ST29 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 5), NotNull]
        public string ST30 { get; set; } // varchar(5)
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string ST31 { get; set; } // varchar(50)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ST32 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ST34 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string ST38 { get; set; } // varchar(15)
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string ST39 { get; set; } // varchar(50)
        [Column(DataType = DataType.NVarChar, Length = 75), NotNull]
        public string ST40 { get; set; } // varchar(75)
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string ST41 { get; set; } // varchar(25)
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string ST42 { get; set; } // varchar(50)
        [Column(DataType = DataType.NVarChar, Length = 75), NotNull]
        public string ST43 { get; set; } // varchar(75)
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string ST44 { get; set; } // varchar(25)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ST45 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ST56 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ST57 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ST63 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ST64 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ST65 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ST66 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string ST67 { get; set; } // varchar(50)
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string ST68 { get; set; } // varchar(150)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string ST69 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string ST70 { get; set; } // varchar(100)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ST71 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string ST72 { get; set; } // varchar(25)
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string ST73 { get; set; } // varchar(25)
        [Column(DataType = DataType.NVarChar, Length = 35), NotNull]
        public string ST74 { get; set; } // varchar(35)
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string ST75 { get; set; } // varchar(50)
        [Column(DataType = DataType.NVarChar, Length = 35), NotNull]
        public string ST76 { get; set; } // varchar(35)
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime ST77 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ST78 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ST99 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ST100 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ST101 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string ST102 { get; set; } // varchar(25)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ST103 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ST104 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string ST105 { get; set; } // varchar(25)
        [Column(DataType = DataType.NVarChar, Length = 45), NotNull]
        public string ST106 { get; set; } // varchar(45)
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string ST107 { get; set; } // varchar(50)
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string ST108 { get; set; } // varchar(15)
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string ST109 { get; set; } // varchar(50)
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string ST110 { get; set; } // varchar(50)
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string ST111 { get; set; } // varchar(50)
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string ST112 { get; set; } // varchar(50)
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string ST113 { get; set; } // varchar(50)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ST114 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 35), NotNull]
        public string ST117 { get; set; } // varchar(35)
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string ST118 { get; set; } // varchar(20)
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string ST119 { get; set; } // varchar(20)
        [Column(DataType = DataType.NVarChar, Length = 35), NotNull]
        public string ST120 { get; set; } // varchar(35)
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string ST121 { get; set; } // varchar(20)
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string ST122 { get; set; } // varchar(20)
        [Column(DataType = DataType.NVarChar, Length = 35), NotNull]
        public string ST123 { get; set; } // varchar(35)
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string ST124 { get; set; } // varchar(20)
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string ST125 { get; set; } // varchar(20)
        [Column(DataType = DataType.NVarChar, Length = 6), NotNull]
        public string ST126 { get; set; } // varchar(6)
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string ST127 { get; set; } // varchar(50)
        [Column(DataType = DataType.NVarChar, Length = 75), NotNull]
        public string ST128 { get; set; } // varchar(75)
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string ST129 { get; set; } // varchar(25)
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string ST130 { get; set; } // varchar(50)
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string ST131 { get; set; } // varchar(20)
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string ST132 { get; set; } // varchar(20)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ST133 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string ST134 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string ST135 { get; set; } // varchar(15)
        [Column(DataType = DataType.NVarChar, Length = 5), NotNull]
        public string ST136 { get; set; } // varchar(5)
        [Column(DataType = DataType.NVarChar, Length = 5), NotNull]
        public string ST137 { get; set; } // varchar(5)
        [Column(DataType = DataType.NVarChar, Length = 5), NotNull]
        public string ST138 { get; set; } // varchar(5)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ST139 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string ST140 { get; set; } // varchar(10)
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string ST141 { get; set; } // varchar(25)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string ST142 { get; set; } // varchar(100)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? ST143 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ST144 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 2), NotNull]
        public string ST145 { get; set; } // varchar(2)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? ST146 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? ST147 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string ST148 { get; set; } // varchar(15)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ST149 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string ST150 { get; set; } // varchar(10)
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string ST151 { get; set; } // varchar(25)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string ST152 { get; set; } // varchar(100)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? ST153 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string ST154 { get; set; } // varchar(10)
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string ST155 { get; set; } // varchar(25)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string ST156 { get; set; } // varchar(100)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? ST157 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string ST158 { get; set; } // varchar(10)
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string ST159 { get; set; } // varchar(25)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string ST160 { get; set; } // varchar(100)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? ST161 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ST162 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? ST163 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ST164 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 1000), NotNull]
        public string ST165 { get; set; } // varchar(1000)
        [Column(DataType = DataType.NVarChar, Length = 75), NotNull]
        public string ST166 { get; set; } // varchar(75)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ST167 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ST200 { get; set; } // integer
    }

    [Table("STAG")]
    public partial class STAG
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int STAG1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string STAG2 { get; set; } // varchar(10)
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double STAG3 { get; set; } // double precision
    }

    [Table("STAL")]
    public partial class STAL
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int STAL1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STAL2 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string STAL5 { get; set; } // varchar(50)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? STAL6 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string STAL7 { get; set; } // varchar(100)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STAL8 { get; set; } // integer
    }

    [Table("STALI")]
    public partial class STALI
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int STALI1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STALI2 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string STALI3 { get; set; } // varchar(50)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? STALI4 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string STALI5 { get; set; } // varchar(100)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STALI6 { get; set; } // integer
    }

    [Table("STAT")]
    public partial class STAT
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int STAT1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STAT2 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STAT3 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short STAT4 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short STAT5 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STAT6 { get; set; } // integer
    }

    [Table("STATD")]
    public partial class STATD
    {
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STATD1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STATD2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short STATD3 { get; set; } // smallint
    }

    [Table("STBL")]
    public partial class STBL
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int STBL2 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? STBL3 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STBL4 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? STBL5 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STBL6 { get; set; } // integer
    }

    [Table("STD")]
    public partial class STD
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int STD1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STD2 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STD3 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime STD4 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STD5 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? STD7 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? STD10 { get; set; } // timestamp
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short STD11 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string STD12 { get; set; } // varchar(50)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short STD17 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime STD18 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STD19 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short STD20 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short STD21 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STD22 { get; set; } // integer        
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short STD23 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short STD24 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STD25 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STD26 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? STD27 { get; set; } // timestamp
    }

    [Table("STDO")]
    public partial class STDO
    {
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STDO1 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short STDO2 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short STDO3 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short STDO4 { get; set; } // smallint
    }

    [Table("STDOCS")]
    public partial class STDOCS
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int STDOCS1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STDOCS2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short STDOCS3 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STDOCS4 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string STDOCS5 { get; set; } // varchar(20)
        [Column(DataType = DataType.NVarChar, Length = 30), NotNull]
        public string STDOCS6 { get; set; } // varchar(30)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? STDOCS7 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string STDOCS8 { get; set; } // varchar(15)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STDOCS9 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STDOCS10 { get; set; } // integer
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double STDOCS11 { get; set; } // double precision
    }

    [Table("STEGND")]
    public partial class STEGND
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int STEGND1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int STEGND2 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(2), NotNull]
        public int STEGND3 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(3), NotNull]
        public int STEGND4 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(4), NotNull]
        public int STEGND5 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), PrimaryKey(5), NotNull]
        public short STEGND6 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime STEGND7 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STEGND8 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), PrimaryKey(6), NotNull]
        public short STEGND9 { get; set; } // smallint
    }

    [Table("STI")]
    public partial class STI
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int STI1 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? STI2 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? STI3 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char STI4 { get; set; } // varchar(1)
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string STI5 { get; set; } // varchar(15)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short STI6 { get; set; } // smallint
    }

    [Table("STL")]
    public partial class STL
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int STL1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STL2 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STL3 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char STL4 { get; set; } // varchar(1)
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string STL5 { get; set; } // varchar(50)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? STL6 { get; set; } // timestamp
    }

    [Table("STLD")]
    public partial class STLD
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int STLD1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STLD2 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STLD3 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short STLD4 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short STLD5 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime STLD6 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? STLD7 { get; set; } // timestamp
    }
    [Table("STMAK")]
    public partial class STMAK
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int STMAK1 { get; set; } // integer
        [Column(DataType = DataType.Text, Length = int.MaxValue), Nullable]
        public string STMAK2 { get; set; } // blob sub_type 1
        [Column(DataType = DataType.Text, Length = int.MaxValue), Nullable]
        public string STMAK3 { get; set; } // blob sub_type 1
        [Column(DataType = DataType.Text, Length = int.MaxValue), Nullable]
        public string STMAK4 { get; set; } // blob sub_type 1
        [Column(DataType = DataType.Text, Length = int.MaxValue), Nullable]
        public string STMAK5 { get; set; } // blob sub_type 1
        [Column(DataType = DataType.NVarChar, Length = 1000), NotNull]
        public string STMAK6 { get; set; } // varchar(1000)
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string STMAK7 { get; set; } // varchar(150)
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string STMAK8 { get; set; } // varchar(150)
    }
    [Table("STML")]
    public partial class STML
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int STML0 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STML1 { get; set; } // integer
        [Column(DataType = DataType.Text, Length = int.MaxValue), Nullable]
        public string STML2 { get; set; } // blob sub_type 1
        [Column(DataType = DataType.NVarChar, Length = 350), NotNull]
        public string STML3 { get; set; } // varchar(350)
    }

    [Table("STOBR")]
    public partial class STOBR
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int STOBR1 { get; set; } // integer       
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STOBR3 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string STOBR4 { get; set; } // varchar(20)
        [Column(DataType = DataType.NVarChar, Length = 30), NotNull]
        public string STOBR5 { get; set; } // varchar(30)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? STOBR6 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STOBR7 { get; set; } // integer
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double STOBR8 { get; set; } // double precision
        [Column(DataType = DataType.NVarChar, Length = 350), NotNull]
        public string STOBR9 { get; set; } // varchar(350)
        [Column(DataType = DataType.NVarChar, Length = 350), NotNull]
        public string STOBR10 { get; set; } // varchar(350)
        [Column(DataType = DataType.NVarChar, Length = 350), NotNull]
        public string STOBR11 { get; set; } // varchar(350)
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string STOBR12 { get; set; } // varchar(25)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short STOBR13 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 2), NotNull]
        public string STOBR14 { get; set; } // varchar(2)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short STOBR15 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string STOBR16 { get; set; } // varchar(15)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? STOBR17 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STOBR18 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string STOBR19 { get; set; } // varchar(10)
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string STOBR20 { get; set; } // varchar(15)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? STOBR21 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string STOBR22 { get; set; } // varchar(15)
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double STOBR23 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double STOBR24 { get; set; } // double precision
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string STOBR25 { get; set; } // varchar(15)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? STOBR26 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime STOBR27 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STOBR28 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string STOBR29 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string STOBR30 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string STOBR31 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string STOBR32 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string STOBR33 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string STOBR34 { get; set; } // varchar(100)
    }

    [Table("STO")]
    public partial class STO
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int STO1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STO3 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STO4 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string STO5 { get; set; } // varchar(20)
        [Column(DataType = DataType.NVarChar, Length = 30), NotNull]
        public string STO6 { get; set; } // varchar(30)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? STO8 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string STO12 { get; set; } // varchar(15)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STO14 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STO15 { get; set; } // integer
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double STO16 { get; set; } // double precision
        [Column(DataType = DataType.NVarChar, Length = 300), NotNull]
        public string STO17 { get; set; } // varchar(300)
    }

    [Table("STPR")]
    public partial class STPR
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int STPR1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STPR2 { get; set; } // integer
        [Column(DataType = DataType.Text, Length = int.MaxValue), Nullable]
        public string STPR3 { get; set; } // blob sub_type 1
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string STPR5 { get; set; } // varchar(15)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? STPR6 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STPR7 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string STPR8 { get; set; } // varchar(25)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STPR9 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short STPR10 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STPR11 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STPR12 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STPR13 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime STPR14 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STPR15 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STPR16 { get; set; } // integer       
    }

    [Table("STPRK")]
    public partial class STPRK
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int STPRK1 { get; set; } // integer
        [Column(DataType = DataType.Text, Length = int.MaxValue), Nullable]
        public string STPRK2 { get; set; } // blob sub_type 1
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string STPRK3 { get; set; } // varchar(15)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? STPRK4 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STPRK5 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string STPRK6 { get; set; } // varchar(25)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STPRK7 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short STPRK8 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime STPRK9 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STPRK10 { get; set; } // integer
    }

    [Table("STPRZ")]
    public partial class STPRZ
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int STPRZ0 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STPRZ1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string STPRZ2 { get; set; } // varchar(15)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? STPRZ3 { get; set; } // timestamp
    }

    [Table("STPZ")]
    public partial class STPZ
    {
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STPZ1 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? STPZ2 { get; set; } // timestamp
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short STPZ3 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short STPZ4 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime STPZ5 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STPZ6 { get; set; } // integer
    }

    [Table("STSB")]
    public partial class STSB
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int STSB1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STSB2 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? STSB3 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? STSB4 { get; set; } // timestamp
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double STSB5 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double STSB6 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double STSB7 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double STSB8 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double STSB9 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double STSB10 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double STSB11 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double STSB12 { get; set; } // double precision
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short STSB13 { get; set; } // smallint
    }

    [Table("STTIP")]
    public partial class STTIP
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int STTIP1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string STTIP2 { get; set; } // varchar(20)
    }

    [Table("STTR")]
    public partial class STTR
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int STTR1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STTR2 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 75), NotNull]
        public string STTR3 { get; set; } // varchar(75)
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string STTR4 { get; set; } // varchar(50)
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string STTR5 { get; set; } // varchar(10)
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string STTR6 { get; set; } // varchar(10)
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char STTR7 { get; set; } // varchar(1)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short STTR8 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string STTR9 { get; set; } // varchar(150)
        [Column(DataType = DataType.NVarChar, Length = 75), NotNull]
        public string STTR10 { get; set; } // varchar(75)
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string STTR11 { get; set; } // varchar(20)
        [Column(DataType = DataType.NVarChar, Length = 250), NotNull]
        public string STTR12 { get; set; } // varchar(250)
    }

    [Table("STUS")]
    public partial class STUS
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int STUS0 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STUS1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STUS18 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short STUS19 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short STUS20 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short STUS3 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 2), NotNull]
        public string STUS11 { get; set; } // varchar(2)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short STUS8 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? STUS6 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string STUS7 { get; set; } // varchar(15)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STUS4 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STUS16 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STUS17 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime STUS9 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STUS10 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STUS22 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short STUS99 { get; set; } // smallint
    }

    [Table("STUSA")]
    public partial class STUSA
    {
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STUSA1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STUSA18 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short STUSA19 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short STUSA20 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STUSA22 { get; set; } // integer
    }

    [Table("STUSP")]
    public partial class STUSP
    {
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STUSP0 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STUSP2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short STUSP5 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STUSP3 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 2), NotNull]
        public string STUSP11 { get; set; } // varchar(2)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short STUSP8 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? STUSP6 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string STUSP7 { get; set; } // varchar(15)
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string STUSP12 { get; set; } // varchar(25)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short STUSP99 { get; set; } // smallint
    }

    [Table("STUSV")]
    public partial class STUSV
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int STUSV0 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STUSV1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STUSV2 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? STUSV3 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string STUSV4 { get; set; } // varchar(25)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STUSV6 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STUSV7 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STUSV8 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime STUSV9 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STUSV10 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? STUSV11 { get; set; } // timestamp
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short STUSV12 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STUSV13 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short STUSV14 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? STUSV15 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STUSV16 { get; set; } // integer
    }

    [Table("STUSVST")]
    public partial class STUSVST
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int STUSVST1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int STUSVST3 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short STUSVST4 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 2), NotNull]
        public string STUSVST5 { get; set; } // varchar(2)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short STUSVST6 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime STUSVST7 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STUSVST8 { get; set; } // integer
    }

    [Table("STV")]
    public partial class STV
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int STV1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string STV4 { get; set; } // varchar(25)
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string STV5 { get; set; } // varchar(25)
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string STV6 { get; set; } // varchar(20)
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string STV7 { get; set; } // varchar(20)
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string STV8 { get; set; } // varchar(20)
        [Column(DataType = DataType.NVarChar, Length = 75), NotNull]
        public string STV9 { get; set; } // varchar(75)
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string STV10 { get; set; } // varchar(25)
        [Column(DataType = DataType.NVarChar, Length = 20), NotNull]
        public string STV11 { get; set; } // varchar(20)
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char STV12 { get; set; } // varchar(1)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? STV13 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? STV14 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string STV15 { get; set; } // varchar(100)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STV16 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short STV3 { get; set; } // smallint
    }

    [Table("STZ")]
    public partial class STZ
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int STZ1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STZ2 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STZ3 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime STZ4 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int STZ5 { get; set; } // integer
    }

    [Table("SVAK")]
    public partial class SVAK
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int SVAK1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 15), NotNull]
        public string SVAK2 { get; set; } // varchar(15)
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string SVAK3 { get; set; } // varchar(200)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SVAK4 { get; set; } // integer
    }

    [Table("SY")]
    public partial class SY
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int SY1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 45), NotNull]
        public string SY2 { get; set; } // varchar(45)
    }

    [Table("SZ")]
    public partial class SZ
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int SZ1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string SZ2 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char SZ3 { get; set; } // varchar(1)
        [Column(DataType = DataType.Text, Length = int.MaxValue), Nullable]
        public string SZ4 { get; set; } // blob sub_type 1
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char SZ5 { get; set; } // varchar(1)
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char SZ6 { get; set; } // varchar(1)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SZ7 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char SZ8 { get; set; } // varchar(1)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SZ9 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SZ10 { get; set; } // integer
        [Column(DataType = DataType.Text, Length = int.MaxValue), Nullable]
        public string SZ11 { get; set; } // blob sub_type 1
        [Column(DataType = DataType.Text, Length = int.MaxValue), Nullable]
        public string SZ12 { get; set; } // blob sub_type 1
        [Column(DataType = DataType.Text, Length = int.MaxValue), Nullable]
        public string SZ13 { get; set; } // blob sub_type 1
        [Column(DataType = DataType.Text, Length = int.MaxValue), Nullable]
        public string SZ14 { get; set; } // blob sub_type 1
        [Column(DataType = DataType.Text, Length = int.MaxValue), Nullable]
        public string SZ15 { get; set; } // blob sub_type 1
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SZ16 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SZ17 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SZ18 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SZ19 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SZ20 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SZ21 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SZ22 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SZ23 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SZ24 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SZ25 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short SZ26 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int SZ30 { get; set; } // integer
    }

    [Table("TARS")]
    public partial class TARS
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int TARS1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string TARS2 { get; set; } // varchar(10)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? TARS3 { get; set; } // timestamp
    }

    [Table("TARST")]
    public partial class TARST
    {
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int TARST1 { get; set; } // integer
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double TARST2 { get; set; } // double precision
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime TARST3 { get; set; } // timestamp
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double TARST4 { get; set; } // double precision
    }

    [Table("TBL_MIGRATION")]
    public partial class TBL_MIGRATION
    {
        [Column(DataType = DataType.NVarChar, Length = 250), PrimaryKey, NotNull]
        public string VERSION { get; set; } // varchar(250)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int APPLY_TIME { get; set; } // integer
    }

    [Table("TDDO")]
    public partial class TDDO
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int TDDO1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int TDDO2 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int TDDO3 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? TDDO4 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string TDDO5 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 1000), NotNull]
        public string TDDO6 { get; set; } // varchar(1000)
        [Column(DataType = DataType.NVarChar, Length = 45), NotNull]
        public string TDDO7 { get; set; } // varchar(45)
        [Column(DataType = DataType.NVarChar, Length = 45), NotNull]
        public string TDDO8 { get; set; } // varchar(45)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? TDDO9 { get; set; } // timestamp
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short TDDO10 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short TDDO11 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 45), NotNull]
        public string TDDO12 { get; set; } // varchar(45)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? TDDO13 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 250), NotNull]
        public string TDDO14 { get; set; } // varchar(250)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int TDDO15 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int TDDO16 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int TDDO17 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int TDDO18 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string TDDO19 { get; set; } // varchar(100)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int TDDO20 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? TDDO21 { get; set; } // timestamp
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short TDDO22 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short TDDO23 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short TDDO24 { get; set; } // smallint
    }

    [Table("TDO")]
    public partial class TDO
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int TDO1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string TDO2 { get; set; } // varchar(100)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int TDO3 { get; set; } // integer
    }

    [Table("TEM")]
    public partial class TEM
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int TEM1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 30), NotNull]
        public string TEM2 { get; set; } // varchar(30)
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char TEM3 { get; set; } // varchar(1)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int TEM4 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime TEM6 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? TEM7 { get; set; } // timestamp
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double TEM8 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double TEM9 { get; set; } // double precision
        [Column(DataType = DataType.NVarChar, Length = 300), NotNull]
        public string TEM10 { get; set; } // varchar(300)
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime TEM11 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int TEM12 { get; set; } // integer
    }

    [Table("TGUP")]
    public partial class TGUP
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int TGUP1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string TGUP2 { get; set; } // varchar(50)
    }

    [Table("TMIGRATION")]
    public partial class TMIGRATION
    {
        [Column(DataType = DataType.NVarChar, Length = 250), PrimaryKey, NotNull]
        public string VERSION { get; set; } // varchar(250)
        [Column(DataType = DataType.Int32, Length = 4), Nullable]
        public int? APPLY_TIME { get; set; } // integer
    }

    [Table("TP")]
    public partial class TP
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int TP1 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime TP2 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string TP3 { get; set; } // varchar(25)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int TP4 { get; set; } // integer
    }

    [Table("TPN")]
    public partial class TPN
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int TPN1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string TPN2 { get; set; } // varchar(25)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short TPN3 { get; set; } // smallint
    }

    [Table("TPSZ")]
    public partial class TPSZ
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int TPSZ1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int TPSZ2 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int TPSZ3 { get; set; } // integer
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double TPSZ4 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double TPSZ5 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double TPSZ6 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double TPSZ7 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double TPSZ8 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double TPSZ9 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double TPSZ10 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double TPSZ11 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double TPSZ12 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double TPSZ13 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double TPSZ14 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double TPSZ15 { get; set; } // double precision
    }

    [Table("TRNT")]
    public partial class TRNT
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int TRNT1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string TRNT2 { get; set; } // varchar(25)
    }

    [Table("TSG")]
    public partial class TSG
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int TSG1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string TSG2 { get; set; } // varchar(150)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short TSG3 { get; set; } // smallint
    }

    [Table("TSO")]
    public partial class TSO
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int TSO1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int TSO2 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string TSO3 { get; set; } // varchar(150)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int TSO4 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int TSO5 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int TSO6 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string TSO7 { get; set; } // varchar(50)
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string TSO8 { get; set; } // varchar(50)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short TSO9 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short TSO10 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string TSO11 { get; set; } // varchar(50)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string TSO12 { get; set; } // varchar(100)
    }

    [Table("U")]
    public partial class U
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int U1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int U2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short U8 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int U9 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short U10 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int U15 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string U16 { get; set; } // varchar(10)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int U17 { get; set; } // integer
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double U21 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double U22 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double U23 { get; set; } // double precision
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime U25 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int U26 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short U27 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? U38 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? U39 { get; set; } // timestamp
    }

    [Table("UCGN")]
    public partial class UCGN
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int UCGN1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int UCGN2 { get; set; } // integer
    }

    [Table("UCGNS")]
    public partial class UCGNS
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int UCGNS1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int UCGNS2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UCGNS3 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UCGNS4 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UCGNS5 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UCGNS6 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UCGNS7 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UCGNS8 { get; set; } // smallint
    }

    [Table("UCHZ")]
    public partial class UCHZ
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int UCHZ1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int UCHZ2 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 250), NotNull]
        public string UCHZ3 { get; set; } // varchar(250)
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string UCHZ4 { get; set; } // varchar(10)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int UCHZ5 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 250), NotNull]
        public string UCHZ6 { get; set; } // varchar(250)
    }

    [Table("UCSN")]
    public partial class UCSN
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int UCSN1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int UCSN2 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int UCSN3 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime UCSN4 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int UCSN5 { get; set; } // integer
    }

    [Table("UCX")]
    public partial class UCX
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int UCX1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string UCX2 { get; set; } // varchar(100)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UCX4 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UCX5 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UCX6 { get; set; } // smallint
    }

    [Table("UCXG")]
    public partial class UCXG
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int UCXG1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int UCXG2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), PrimaryKey(2), NotNull]
        public short UCXG3 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), PrimaryKey(3), NotNull]
        public short UCXG5 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), PrimaryKey(4), NotNull]
        public short UCXG6 { get; set; } // smallint
    }

    [Table("UG")]
    public partial class UG
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int UG1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int UG2 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(2), NotNull]
        public int UG3 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int UG4 { get; set; } // integer
    }

    [Table("UGT")]
    public partial class UGT
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int UGT1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int UGT2 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(2), NotNull]
        public int UGT3 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int UGT4 { get; set; } // integer
    }

    [Table("UO")]
    public partial class UO
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int UO1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int UO2 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int UO3 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int UO4 { get; set; } // integer
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double UO5 { get; set; } // double precision
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UO6 { get; set; } // smallint
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double UO10 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double UO11 { get; set; } // double precision
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UO12 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UO13 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UO14 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UO15 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UO16 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int UO17 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int UO18 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int UO19 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime UO20 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int UO21 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int UO22 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UO23 { get; set; } // smallint
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double UO24 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double UO25 { get; set; } // double precision
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UO26 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char UO27 { get; set; } // varchar(1)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UO28 { get; set; } // smallint
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double UO29 { get; set; } // double precision
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int UO30 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int UO33 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UO34 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime UO35 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int UO36 { get; set; } // integer
    }

    [Table("UOSV")]
    public partial class UOSV
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int UOSV1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int UOSV2 { get; set; } // integer
    }

    [Table("UOUP")]
    public partial class UOUP
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int UOUP1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int UOUP2 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int UOUP3 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int UOUP4 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UOUP5 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UOUP6 { get; set; } // smallint
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double UOUP10 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double UOUP11 { get; set; } // double precision
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UOUP12 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UOUP13 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UOUP14 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UOUP15 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UOUP16 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int UOUP17 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int UOUP18 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int UOUP19 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime UOUP20 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int UOUP21 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int UOUP22 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UOUP23 { get; set; } // smallint
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double UOUP24 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double UOUP25 { get; set; } // double precision
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UOUP26 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UOUP27 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UOUP28 { get; set; } // smallint
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double UOUP29 { get; set; } // double precision
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int UOUP30 { get; set; } // integer
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double UOUP31 { get; set; } // double precision
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UOUP32 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int UOUP33 { get; set; } // integer
    }

    [Table("UPCM")]
    public partial class UPCM
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int UPCM1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int UPCM2 { get; set; } // integer
    }

    [Table("UPNR")]
    public partial class UPNR
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int UPNR1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int UPNR2 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(2), NotNull]
        public int UPNR3 { get; set; } // integer
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double UPNR4 { get; set; } // double precision
    }

    [Table("UPST")]
    public partial class UPST
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int UPST1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 250), NotNull]
        public string UPST2 { get; set; } // varchar(250)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UPST3 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UPST4 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UPST5 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UPST6 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UPST7 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UPST8 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UPST9 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UPST10 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UPST11 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UPST12 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UPST13 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UPST14 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UPST15 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UPST16 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UPST17 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UPST18 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UPST19 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UPST20 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UPST21 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UPST22 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UPST23 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UPST24 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UPST25 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UPST26 { get; set; } // smallint
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double UPST30 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double UPST31 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double UPST32 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double UPST33 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double UPST34 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double UPST35 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double UPST36 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double UPST37 { get; set; } // double precision
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UPST38 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UPST39 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UPST40 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UPST41 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UPST42 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UPST43 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UPST44 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UPST45 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UPST46 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UPST47 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UPST48 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UPST49 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UPST50 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UPST51 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UPST52 { get; set; } // smallint
    }

    [Table("UPSTD")]
    public partial class UPSTD
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int UPSTD1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int UPSTD3 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int UPSTD4 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UPSTD5 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int UPSTD22 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UPSTD28 { get; set; } // smallint
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double UPSTD29 { get; set; } // double precision
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int UPSTD30 { get; set; } // integer
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double UPSTD31 { get; set; } // double precision
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UPSTD32 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int UPSTD33 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int UPSTD40 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int UPSTD140 { get; set; } // integer
    }

    [Table("UPSTF")]
    public partial class UPSTF
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int UPSTF1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int UPSTF2 { get; set; } // integer
    }

    [Table("UPSTK")]
    public partial class UPSTK
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int UPSTK1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int UPSTK2 { get; set; } // integer
    }

    [Table("UPSTN")]
    public partial class UPSTN
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int UPSTN1 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), PrimaryKey(1), NotNull]
        public short UPSTN2 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), PrimaryKey(2), NotNull]
        public short UPSTN3 { get; set; } // smallint
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double UPSTN4 { get; set; } // double precision
    }

    [Table("UPSTS")]
    public partial class UPSTS
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int UPSTS1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int UPSTS2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UPSTS3 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UPSTS4 { get; set; } // smallint
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double UPSTS6 { get; set; } // double precision
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int UPSTS15 { get; set; } // integer
    }

    [Table("UPSTU")]
    public partial class UPSTU
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int UPSTU1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int UPSTU2 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(2), NotNull]
        public int UPSTU3 { get; set; } // integer
    }

    [Table("UPSTZ")]
    public partial class UPSTZ
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int UPSTZ1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int UPSTZ2 { get; set; } // integer
    }

    [Table("UPTUP")]
    public partial class UPTUP
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int UPTUP1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int UPTUP2 { get; set; } // integer
    }

    [Table("US")]
    public partial class US
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int US1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int US2 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int US3 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short US4 { get; set; } // smallint
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double US5 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double US6 { get; set; } // double precision
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int US12 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int US13 { get; set; } // integer
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double US14 { get; set; } // double precision
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int US15 { get; set; } // integer
    }

    [Table("USERS")]
    public partial class USERS
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int U1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string U2 { get; set; } // varchar(50)
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string U3 { get; set; } // varchar(50)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string U4 { get; set; } // varchar(100)
        [Column(DataType = DataType.Int16, Length = 2), Nullable]
        public short? U5 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), Nullable]
        public int? U6 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short U7 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short U8 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 45), NotNull]
        public string U9 { get; set; } // varchar(45)
        [Column(DataType = DataType.NVarChar, Length = 45), NotNull]
        public string U10 { get; set; } // varchar(45)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? U11 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 30), NotNull]
        public string U12 { get; set; } // varchar(30)
    }

    [Table("USERS_AUTH_FAIL")]
    public partial class USERS_AUTH_FAIL
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int USER_ID { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), PrimaryKey(1), NotNull]
        public DateTime DATE_FAIL { get; set; } // timestamp
    }

    [Table("USERS_HISTORY")]
    public partial class USERS_HISTORY
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int UH1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int UH2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UH3 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string UH4 { get; set; } // varchar(200)
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime UH5 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 200), NotNull]
        public string UH6 { get; set; } // varchar(200)
    }

    [Table("USTEM")]
    public partial class USTEM
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int USTEM1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int USTEM2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short USTEM3 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short USTEM4 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 700), Nullable]
        public string USTEM5 { get; set; } // varchar(700)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short USTEM6 { get; set; } // smallint
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double USTEM7 { get; set; } // double precision
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime USTEM8 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int USTEM9 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int USTEM10 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int USTEM11 { get; set; } // integer
    }

    [Table("USTEMP")]
    public partial class USTEMP
    {
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int USTEMP1 { get; set; } // integer
    }

    [Table("USUP")]
    public partial class USUP
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int USUP1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int USUP2 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int USUP3 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short USUP4 { get; set; } // smallint
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double USUP5 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double USUP6 { get; set; } // double precision
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int USUP12 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short USUP13 { get; set; } // smallint
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double USUP14 { get; set; } // double precision
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int USUP15 { get; set; } // integer
    }

    [Table("UTL")]
    public partial class UTL
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int UTL1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 75), NotNull]
        public string UTL2 { get; set; } // varchar(75)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UTL3 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 75), NotNull]
        public string UTL4 { get; set; } // varchar(75)
        [Column(DataType = DataType.NVarChar, Length = 75), NotNull]
        public string UTL5 { get; set; } // varchar(75)
        [Column(DataType = DataType.NVarChar, Length = 75), NotNull]
        public string UTL6 { get; set; } // varchar(75)
        [Column(DataType = DataType.NVarChar, Length = 75), NotNull]
        public string UTL7 { get; set; } // varchar(75)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UTL8 { get; set; } // smallint
    }

    [Table("UUP")]
    public partial class UUP
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int UUP1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int UUP2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UUP8 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int UUP9 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short UUP10 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int UUP15 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string UUP16 { get; set; } // varchar(10)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int UUP17 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 75), NotNull]
        public string UUP20 { get; set; } // varchar(75)
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double UUP21 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double UUP22 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double UUP23 { get; set; } // double precision
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime UUP25 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int UUP26 { get; set; } // integer
    }

    [Table("VD")]
    public partial class VD
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int VD1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string VD2 { get; set; } // varchar(25)
        [Column(DataType = DataType.NVarChar, Length = 75), NotNull]
        public string VD3 { get; set; } // varchar(75)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int VD4 { get; set; } // integer
    }

    [Table("VDN")]
    public partial class VDN
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int VDN1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string VDN2 { get; set; } // varchar(50)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short VDN3 { get; set; } // smallint
    }

    [Table("VDNS")]
    public partial class VDNS
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int VDNS1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int VDNS2 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(2), NotNull]
        public int VDNS3 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int VDNS4 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string VDNS5 { get; set; } // varchar(150)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short VDNS6 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string VDNS7 { get; set; } // varchar(150)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short VDNS8 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string VDNS9 { get; set; } // varchar(150)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short VDNS10 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string VDNS11 { get; set; } // varchar(150)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short VDNS12 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string VDNS13 { get; set; } // varchar(150)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short VDNS14 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string VDNS15 { get; set; } // varchar(150)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short VDNS16 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string VDNS17 { get; set; } // varchar(150)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short VDNS18 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 150), NotNull]
        public string VDNS19 { get; set; } // varchar(150)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short VDNS20 { get; set; } // smallint
    }

    [Table("VIPZ")]
    public partial class VIPZ
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int VIPZ1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int VIPZ2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), PrimaryKey(2), NotNull]
        public short VIPZ3 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), PrimaryKey(3), NotNull]
        public short VIPZ4 { get; set; } // smallint
    }

    [Table("VKB")]
    public partial class VKB
    {
        [Column(DataType = DataType.Int16, Length = 2), PrimaryKey, NotNull]
        public short VKB1 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short VKB2 { get; set; } // smallint
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double VKB3 { get; set; } // double precision
        [Column(DataType = DataType.NVarChar, Length = 45), NotNull]
        public string VKB7 { get; set; } // varchar(45)
        [Column(DataType = DataType.NVarChar, Length = 45), NotNull]
        public string VKB8 { get; set; } // varchar(45)
        [Column(DataType = DataType.NVarChar, Length = 45), NotNull]
        public string VKB9 { get; set; } // varchar(45)
        [Column(DataType = DataType.NVarChar, Length = 45), NotNull]
        public string VKB10 { get; set; } // varchar(45)
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double VKB11 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double VKB12 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double VKB13 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double VKB14 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double VKB15 { get; set; } // double precision
    }

    [Table("VMP")]
    public partial class VMP
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int VMP1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int VMP2 { get; set; } // integer
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double VMP4 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double VMP5 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double VMP6 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double VMP7 { get; set; } // double precision
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short VMP9 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime VMP10 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int VMP11 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int VMP12 { get; set; } // integer
    }

    [Table("VMPM")]
    public partial class VMPM
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int VMPM1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 350), NotNull]
        public string VMPM2 { get; set; } // varchar(350)
    }

    [Table("VMPP")]
    public partial class VMPP
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int VMPP1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int VMPP2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short VMPP3 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short VMPP4 { get; set; } // smallint
    }

    [Table("VMPV")]
    public partial class VMPV
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int VMPV1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int VMPV2 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 25), NotNull]
        public string VMPV3 { get; set; } // varchar(25)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? VMPV4 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? VMPV5 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? VMPV6 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int VMPV7 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? VMPV8 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime VMPV9 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int VMPV10 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int VMPV11 { get; set; } // integer
    }

    [Table("VMPVF")]
    public partial class VMPVF
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int VMPVF1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int VMPVF2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short VMPVF3 { get; set; } // smallint
    }

    [Table("VN")]
    public partial class VN
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int VN1 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short VN2 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short VN3 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short VN4 { get; set; } // smallint
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double VN5 { get; set; } // double precision
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int VN6 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime VN7 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int VN8 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int VN9 { get; set; } // integer
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double VN10 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double VN11 { get; set; } // double precision
    }

    [Table("VOEN")]
    public partial class VOEN
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int VOEN1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string VOEN2 { get; set; } // varchar(100)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int VOEN3 { get; set; } // integer
    }
    [Table("VSPR")]
    public partial class VSPR
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int VSPR1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string VSPR2 { get; set; } // varchar(50)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? VSPR3 { get; set; } // timestamp
    }
    [Table("VPR")]
    public partial class VPR
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int VPR1 { get; set; } // integer       
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short VPR3 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short VPR4 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 250), NotNull]
        public string VPR5 { get; set; } // varchar(250)
        [Column(DataType = DataType.Text, Length = int.MaxValue), Nullable]
        public string VPR6 { get; set; } // blob sub_type 1
        [Column(DataType = DataType.Text, Length = int.MaxValue), Nullable]
        public string VPR7 { get; set; } // blob sub_type 1
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short VPR8 { get; set; } // smallint
    }

    [Table("VPRV")]
    public partial class VPRV
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int VPRV1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int VPRV2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short VPRV3 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 250), NotNull]
        public string VPRV4 { get; set; } // varchar(250)
        [Column(DataType = DataType.NVarChar, Length = 250), NotNull]
        public string VPRV5 { get; set; } // varchar(250)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string VPRV6 { get; set; } // varchar(100)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short VPRV7 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short VPRV8 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int VPRV9 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? VPRV10 { get; set; } // timestamp
        [Column(DataType = DataType.Text, Length = int.MaxValue), Nullable]
        public string VPRV11 { get; set; } // blob sub_type 1
        [Column(DataType = DataType.Text, Length = int.MaxValue), Nullable]
        public string VPRV12 { get; set; } // blob sub_type 1
        [Column(DataType = DataType.Text, Length = int.MaxValue), Nullable]
        public string VPRV13 { get; set; } // blob sub_type 1
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int VPRV14 { get; set; } // integer
        [Column(DataType = DataType.Text, Length = int.MaxValue), Nullable]
        public string VPRV15 { get; set; } // blob sub_type 1
    }

    [Table("VRN")]
    public partial class VRN
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int VRN1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string VRN2 { get; set; } // varchar(50)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short VRN3 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short VRN4 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int VRN5 { get; set; } // integer
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double VRN6 { get; set; } // double precision
    }

    [Table("VRNA")]
    public partial class VRNA
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int VRNA1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int VRNA2 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int VRNA3 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short VRNA4 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short VRNA5 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short VRNA6 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int VRNA7 { get; set; } // integer
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double VRNA8 { get; set; } // double precision
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short VRNA9 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short VRNA10 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short VRNA11 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short VRNA12 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short VRNA13 { get; set; } // smallint
    }

    [Table("VRNAR")]
    public partial class VRNAR
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int VRNAR1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int VRNAR2 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int VRNAR3 { get; set; } // integer
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double VRNAR4 { get; set; } // double precision
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime VRNAR5 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int VRNAR6 { get; set; } // integer
    }

    [Table("VVMP")]
    public partial class VVMP
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int VVMP1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int VVMP3 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short VVMP4 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int VVMP5 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short VVMP6 { get; set; } // smallint
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double VVMP8 { get; set; } // double precision
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short VVMP9 { get; set; } // smallint
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double VVMP10 { get; set; } // double precision
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int VVMP24 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int VVMP25 { get; set; } // integer
    }

    [Table("VVN")]
    public partial class VVN
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int VVN1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string VVN2 { get; set; } // varchar(50)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short VVN3 { get; set; } // smallint
        [Column(DataType = DataType.DateTime, Length = 8), NotNull]
        public DateTime VVN4 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int VVN5 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int VVN6 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short VVN7 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short VVN8 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short VVN9 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short VVN10 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short VVN11 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short VVN12 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int VVN15 { get; set; } // integer
    }

    [Table("VVNPBK")]
    public partial class VVNPBK
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int VVNPBK1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int VVNPBK2 { get; set; } // integer
    }

    [Table("VVNPD")]
    public partial class VVNPD
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int VVNPD1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int VVNPD2 { get; set; } // integer
    }

    [Table("VVNPF")]
    public partial class VVNPF
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int VVNPF1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int VVNPF2 { get; set; } // integer
    }

    [Table("VVNPP")]
    public partial class VVNPP
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int VVNPP1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int VVNPP2 { get; set; } // integer
    }

    [Table("VVNPS")]
    public partial class VVNPS
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int VVNPS1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int VVNPS2 { get; set; } // integer
    }

    [Table("VVNPV")]
    public partial class VVNPV
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int VVNPV1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int VVNPV2 { get; set; } // integer
    }

    [Table("VVNV")]
    public partial class VVNV
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int VVNV1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string VVNV2 { get; set; } // varchar(50)
    }

    [Table("VZV")]
    public partial class VZV
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int VZV1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 50), NotNull]
        public string VZV2 { get; set; } // varchar(50)
    }

    [Table("W")]
    public partial class W
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int W1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string W2 { get; set; } // varchar(10)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int W5 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? W6 { get; set; } // timestamp
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short W7 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short W8 { get; set; } // smallint
    }

    [Table("WK")]
    public partial class WK
    {
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double W3 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double W4 { get; set; } // double precision
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int W9 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), PrimaryKey(1), NotNull]
        public short W10 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(2), NotNull]
        public int W11 { get; set; } // integer
    }

    [Table("XPD")]
    public partial class XPD
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int XPD1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int XPD4 { get; set; } // integer
    }

    [Table("XPDG")]
    public partial class XPDG
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int XPDG1 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), PrimaryKey(1), NotNull]
        public short XPDG2 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), PrimaryKey(2), NotNull]
        public short XPDG3 { get; set; } // smallint
    }

    [Table("XPDS")]
    public partial class XPDS
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int XPDS1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int XPDS2 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(2), NotNull]
        public int XPDS3 { get; set; } // integer
    }

    [Table("Y")]
    public partial class Y
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int Y1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 10), NotNull]
        public string Y2 { get; set; } // varchar(10)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int Y5 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? Y6 { get; set; } // timestamp
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short Y7 { get; set; } // smallint
    }

    [Table("YK")]
    public partial class YK
    {
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double Y3 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double Y4 { get; set; } // double precision
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int Y8 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), PrimaryKey(1), NotNull]
        public short Y9 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(2), NotNull]
        public int Y10 { get; set; } // integer
    }

    [Table("ZFST")]
    public partial class ZFST
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int ZFST1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ZFST2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ZFST3 { get; set; } // smallint
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string ZFST4 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string ZFST5 { get; set; } // varchar(100)
    }

    [Table("ZPD")]
    public partial class ZPD
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int ZPD1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ZPD2 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ZPD3 { get; set; } // integer
    }

    [Table("ZPER")]
    public partial class ZPER
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int ZPER1 { get; set; } // integer
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double ZPER2 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double ZPER3 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double ZPER4 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double ZPER5 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double ZPER6 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double ZPER7 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double ZPER8 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double ZPER9 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double ZPER10 { get; set; } // double precision
        [Column(DataType = DataType.Double, Length = 8), NotNull]
        public double ZPER11 { get; set; } // double precision
    }

    [Table("ZPM")]
    public partial class ZPM
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int ZPM1 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), PrimaryKey(1), NotNull]
        public short ZPM2 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(2), NotNull]
        public int ZPM3 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), PrimaryKey(3), NotNull]
        public short ZPM4 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ZPM5 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ZPM6 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ZPM7 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ZPM8 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ZPM9 { get; set; } // smallint
        [Column(DataType = DataType.Blob, Length = int.MaxValue), NotNull]
        public byte[] ZPM10 { get; set; } // blob
    }

    [Table("ZS")]
    public partial class ZS
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int ZS1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 100), NotNull]
        public string ZS2 { get; set; } // varchar(100)
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char ZS4 { get; set; } // varchar(1)
        [Column(DataType = DataType.Text, Length = int.MaxValue), Nullable]
        public string ZS5 { get; set; } // blob sub_type 1
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char ZS6 { get; set; } // varchar(1)
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char ZS7 { get; set; } // varchar(1)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ZS8 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char ZS9 { get; set; } // varchar(1)
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ZS10 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char ZS11 { get; set; } // varchar(1)
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char ZS12 { get; set; } // varchar(1)
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char ZS13 { get; set; } // varchar(1)
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char ZS14 { get; set; } // varchar(1)
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char ZS15 { get; set; } // varchar(1)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? ZS16 { get; set; } // timestamp
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? ZS17 { get; set; } // timestamp
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char ZS18 { get; set; } // varchar(1)
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char ZS19 { get; set; } // varchar(1)
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char ZS20 { get; set; } // varchar(1)
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char ZS21 { get; set; } // varchar(1)
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char ZS22 { get; set; } // varchar(1)
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char ZS23 { get; set; } // varchar(1)
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char ZS24 { get; set; } // varchar(1)
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char ZS25 { get; set; } // varchar(1)
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char ZS26 { get; set; } // varchar(1)
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char ZS27 { get; set; } // varchar(1)
        [Column(DataType = DataType.NVarChar, Length = 1), NotNull]
        public char ZS28 { get; set; } // varchar(1)
        [Column(DataType = DataType.Text, Length = int.MaxValue), Nullable]
        public string ZS29 { get; set; } // blob sub_type 1
    }

    [Table("ZSG")]
    public partial class ZSG
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int ZSG1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int ZSG2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ZSG3 { get; set; } // smallint
    }

    [Table("ZSP")]
    public partial class ZSP
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int ZSP1 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), PrimaryKey(1), NotNull]
        public short ZSP2 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ZSP3 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ZSP4 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 250), NotNull]
        public string ZSP5 { get; set; } // varchar(250)
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ZSP6 { get; set; } // smallint
    }

    [Table("ZSV")]
    public partial class ZSV
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(0), NotNull]
        public int ZSV1 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey(1), NotNull]
        public int ZSV2 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ZSV3 { get; set; } // smallint
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ZSV4 { get; set; } // smallint
    }

    [Table("ZUZ")]
    public partial class ZUZ
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int ZUZ1 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 75), NotNull]
        public string ZUZ2 { get; set; } // varchar(75)
    }

    [Table("ZZ")]
    public partial class ZZ
    {
        [Column(DataType = DataType.Int32, Length = 4), PrimaryKey, NotNull]
        public int ZZ1 { get; set; } // integer
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? ZZ2 { get; set; } // timestamp
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ZZ3 { get; set; } // smallint
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ZZ4 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ZZ5 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ZZ6 { get; set; } // integer
        [Column(DataType = DataType.NVarChar, Length = 250), NotNull]
        public string ZZ7 { get; set; } // varchar(250)
        [Column(DataType = DataType.DateTime, Length = 8), Nullable]
        public DateTime? ZZ8 { get; set; } // timestamp
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ZZ9 { get; set; } // integer
        [Column(DataType = DataType.Int32, Length = 4), NotNull]
        public int ZZ10 { get; set; } // integer
        [Column(DataType = DataType.Int16, Length = 2), NotNull]
        public short ZZ11 { get; set; } // smallint
    }


    public static partial class BDETALONDBStoredProcedures
    {
        #region EL_GURNAL

        public partial class EL_GURNALResult
        {
            public int? D1 { get; set; }
            public string D2 { get; set; }
            public int? DOSTUP { get; set; }
            public int? US1 { get; set; }
            public short? SEM4 { get; set; }
            public string GR3 { get; set; }
            public short? GR7 { get; set; }
            public int? GR1 { get; set; }
            public string GR19 { get; set; }
            public string GR20 { get; set; }
            public string GR21 { get; set; }
            public string GR22 { get; set; }
            public string GR23 { get; set; }
            public string GR24 { get; set; }
            public string GR25 { get; set; }
            public string GR26 { get; set; }
            public string GR28 { get; set; }
            public int? KOD_UO1 { get; set; }
            public string SPEC { get; set; }
            public int? CHASI { get; set; }
            public int? US4 { get; set; }
        }

        public static IEnumerable<EL_GURNALResult> EL_GURNAL(this DataConnection dataConnection, int? P1, int? UCH_GOD, int? SEM, int? KOD_D1, short? PRIZNAK, int? KOD_US1, int? R1, short? KUDA_DOSTUP, int? VID, out int? D1, out string D2, out int? DOSTUP, out int? US1, out short? SEM4, out string GR3, out short? GR7, out int? GR1, out string GR19, out string GR20, out string GR21, out string GR22, out string GR23, out string GR24, out string GR25, out string GR26, out string GR28, out int? KOD_UO1, out string SPEC, out int? CHASI, out int? US4)
        {
            var ret = dataConnection.QueryProc<EL_GURNALResult>("EL_GURNAL",
                new DataParameter("P1", P1, DataType.Int32),
                new DataParameter("UCH_GOD", UCH_GOD, DataType.Int32),
                new DataParameter("SEM", SEM, DataType.Int32),
                new DataParameter("KOD_D1", KOD_D1, DataType.Int32),
                new DataParameter("PRIZNAK", PRIZNAK, DataType.Int16),
                new DataParameter("KOD_US1", KOD_US1, DataType.Int32),
                new DataParameter("R1", R1, DataType.Int32),
                new DataParameter("KUDA_DOSTUP", KUDA_DOSTUP, DataType.Int16),
                new DataParameter("VID", VID, DataType.Int32));

            D1 = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["D1"]).Value);
            D2 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["D2"]).Value);
            DOSTUP = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["DOSTUP"]).Value);
            US1 = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["US1"]).Value);
            SEM4 = Converter.ChangeTypeTo<short?>(((IDbDataParameter)dataConnection.Command.Parameters["SEM4"]).Value);
            GR3 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["GR3"]).Value);
            GR7 = Converter.ChangeTypeTo<short?>(((IDbDataParameter)dataConnection.Command.Parameters["GR7"]).Value);
            GR1 = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["GR1"]).Value);
            GR19 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["GR19"]).Value);
            GR20 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["GR20"]).Value);
            GR21 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["GR21"]).Value);
            GR22 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["GR22"]).Value);
            GR23 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["GR23"]).Value);
            GR24 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["GR24"]).Value);
            GR25 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["GR25"]).Value);
            GR26 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["GR26"]).Value);
            GR28 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["GR28"]).Value);
            KOD_UO1 = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["KOD_UO1"]).Value);
            SPEC = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["SPEC"]).Value);
            CHASI = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["CHASI"]).Value);
            US4 = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["US4"]).Value);

            return ret;
        }

        #endregion

        #region EL_GURNAL_OTR

        public partial class EL_GURNAL_OTRResult
        {
            public string D2 { get; set; }
            public string ST2 { get; set; }
            public string ST3 { get; set; }
            public string ST4 { get; set; }
            public string GR3 { get; set; }
            public string R2 { get; set; }
            public string USTEM5 { get; set; }
            public int? ELGZST0 { get; set; }
            public short? ELGZST3 { get; set; }
            public double? ELGZST4 { get; set; }
            public double? ELGZST5 { get; set; }
            public string ELGP3 { get; set; }
            public short? ELGP2 { get; set; }
            public string ELGP4 { get; set; }
            public int? SEM1 { get; set; }
            public short? ELG4 { get; set; }
            public int? ELG2 { get; set; }
            public int? GR1 { get; set; }
            public int? NOM { get; set; }
            public int? R1 { get; set; }
        }

        public static IEnumerable<EL_GURNAL_OTRResult> EL_GURNAL_OTR(this DataConnection dataConnection, short? VID, int? UO1, int? GOD, int? SEM, out string D2, out string ST2, out string ST3, out string ST4, out string GR3, out string R2, out string USTEM5, out int? ELGZST0, out short? ELGZST3, out double? ELGZST4, out double? ELGZST5, out string ELGP3, out short? ELGP2, out string ELGP4, out int? SEM1, out short? ELG4, out int? ELG2, out int? GR1, out int? NOM, out int? R1)
        {
            var ret = dataConnection.QueryProc<EL_GURNAL_OTRResult>("EL_GURNAL_OTR",
                new DataParameter("VID", VID, DataType.Int16),
                new DataParameter("UO1", UO1, DataType.Int32),
                new DataParameter("GOD", GOD, DataType.Int32),
                new DataParameter("SEM", SEM, DataType.Int32));

            D2 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["D2"]).Value);
            ST2 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["ST2"]).Value);
            ST3 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["ST3"]).Value);
            ST4 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["ST4"]).Value);
            GR3 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["GR3"]).Value);
            R2 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["R2"]).Value);
            USTEM5 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["USTEM5"]).Value);
            ELGZST0 = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["ELGZST0"]).Value);
            ELGZST3 = Converter.ChangeTypeTo<short?>(((IDbDataParameter)dataConnection.Command.Parameters["ELGZST3"]).Value);
            ELGZST4 = Converter.ChangeTypeTo<double?>(((IDbDataParameter)dataConnection.Command.Parameters["ELGZST4"]).Value);
            ELGZST5 = Converter.ChangeTypeTo<double?>(((IDbDataParameter)dataConnection.Command.Parameters["ELGZST5"]).Value);
            ELGP3 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["ELGP3"]).Value);
            ELGP2 = Converter.ChangeTypeTo<short?>(((IDbDataParameter)dataConnection.Command.Parameters["ELGP2"]).Value);
            ELGP4 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["ELGP4"]).Value);
            SEM1 = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["SEM1"]).Value);
            ELG4 = Converter.ChangeTypeTo<short?>(((IDbDataParameter)dataConnection.Command.Parameters["ELG4"]).Value);
            ELG2 = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["ELG2"]).Value);
            GR1 = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["GR1"]).Value);
            NOM = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["NOM"]).Value);
            R1 = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["R1"]).Value);

            return ret;
        }

        #endregion

        #region EL_GURNAL_RASP

        public partial class EL_GURNAL_RASPResult
        {
            public DateTime? R2 { get; set; }
            public int? NOM { get; set; }
            public int? NOM_ZANATIY { get; set; }
            public string TEMA { get; set; }
            public short? PRIZ { get; set; }
            public int? R1 { get; set; }
            public int? US1 { get; set; }
            public int? US4 { get; set; }
        }

        public static IEnumerable<EL_GURNAL_RASPResult> EL_GURNAL_RASP(this DataConnection dataConnection, int? VID, int? UCH_GOD, int? SEM, int? KOD_UO1, int? KOD_GR1, out DateTime? R2, out int? NOM, out int? NOM_ZANATIY, out string TEMA, out short? PRIZ, out int? R1, out int? US1, out int? US4)
        {
            var ret = dataConnection.QueryProc<EL_GURNAL_RASPResult>("EL_GURNAL_RASP",
                new DataParameter("VID", VID, DataType.Int32),
                new DataParameter("UCH_GOD", UCH_GOD, DataType.Int32),
                new DataParameter("SEM", SEM, DataType.Int32),
                new DataParameter("KOD_UO1", KOD_UO1, DataType.Int32),
                new DataParameter("KOD_GR1", KOD_GR1, DataType.Int32));

            R2 = Converter.ChangeTypeTo<DateTime?>(((IDbDataParameter)dataConnection.Command.Parameters["R2"]).Value);
            NOM = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["NOM"]).Value);
            NOM_ZANATIY = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["NOM_ZANATIY"]).Value);
            TEMA = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["TEMA"]).Value);
            PRIZ = Converter.ChangeTypeTo<short?>(((IDbDataParameter)dataConnection.Command.Parameters["PRIZ"]).Value);
            R1 = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["R1"]).Value);
            US1 = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["US1"]).Value);
            US4 = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["US4"]).Value);

            return ret;
        }

        #endregion

        #region EL_GURNAL_ZAN

        public partial class EL_GURNAL_ZANResult
        {
            public DateTime? R2 { get; set; }
            public int? NOM { get; set; }
            public int? US4 { get; set; }
            public int? R1 { get; set; }
            public int? R4 { get; set; }
        }

        public static IEnumerable<EL_GURNAL_ZANResult> EL_GURNAL_ZAN(this DataConnection dataConnection, int? UO1, int? GR1, int? SEM1, short? ELG4)
        {
            var ret = dataConnection.QueryProc<EL_GURNAL_ZANResult>("EL_GURNAL_ZAN",
                new DataParameter("UO1", UO1, DataType.Int32),
                new DataParameter("GR1", GR1, DataType.Int32),
                new DataParameter("SEM1", SEM1, DataType.Int32),
                new DataParameter("ELG4", ELG4, DataType.Int16));
            return ret;
        }

        #endregion

        #region PR1

        public partial class PR1Result
        {
            public int? ID1 { get; set; }
        }

        public static IEnumerable<PR1Result> PR1(this DataConnection dataConnection, string KEYFIELD, string TABLENAME, out int? ID1)
        {
            var ret = dataConnection.QueryProc<PR1Result>("PR1",
                new DataParameter("KEYFIELD", KEYFIELD, DataType.NVarChar),
                new DataParameter("TABLENAME", TABLENAME, DataType.NVarChar));

            ID1 = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["ID1"]).Value);

            return ret;
        }

        #endregion

        #region RAA

        public partial class RAAResult
        {
            public string D3 { get; set; }
            public string D2 { get; set; }
            public string US4 { get; set; }
            public DateTime? R2 { get; set; }
            public string D37 { get; set; }
            public string D36 { get; set; }
            public string D35 { get; set; }
            public string D34 { get; set; }
            public string D33 { get; set; }
            public string D32 { get; set; }
            public string D28 { get; set; }
            public int? SP5 { get; set; }
            public string D27 { get; set; }
            public int? R3 { get; set; }
            public DateTime? R11 { get; set; }
            public string TIP { get; set; }
            public int? NDAY { get; set; }
            public int? NED { get; set; }
            public string FIO { get; set; }
            public string GR3 { get; set; }
            public string GRUP { get; set; }
            public string RZ2 { get; set; }
            public string RZ3 { get; set; }
        }

        public static IEnumerable<RAAResult> RAA(this DataConnection dataConnection, int? LANG, int? A1, DateTime? DAT1, DateTime? DAT2, out string D3, out string D2, out string US4, out DateTime? R2, out string D37, out string D36, out string D35, out string D34, out string D33, out string D32, out string D28, out int? SP5, out string D27, out int? R3, out DateTime? R11, out string TIP, out int? NDAY, out int? NED, out string FIO, out string GR3, out string GRUP, out string RZ2, out string RZ3)
        {
            var ret = dataConnection.QueryProc<RAAResult>("RAA",
                new DataParameter("LANG", LANG, DataType.Int32),
                new DataParameter("A1", A1, DataType.Int32),
                new DataParameter("DAT1", DAT1, DataType.DateTime),
                new DataParameter("DAT2", DAT2, DataType.DateTime));

            D3 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["D3"]).Value);
            D2 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["D2"]).Value);
            US4 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["US4"]).Value);
            R2 = Converter.ChangeTypeTo<DateTime?>(((IDbDataParameter)dataConnection.Command.Parameters["R2"]).Value);
            D37 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["D37"]).Value);
            D36 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["D36"]).Value);
            D35 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["D35"]).Value);
            D34 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["D34"]).Value);
            D33 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["D33"]).Value);
            D32 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["D32"]).Value);
            D28 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["D28"]).Value);
            SP5 = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["SP5"]).Value);
            D27 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["D27"]).Value);
            R3 = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["R3"]).Value);
            R11 = Converter.ChangeTypeTo<DateTime?>(((IDbDataParameter)dataConnection.Command.Parameters["R11"]).Value);
            TIP = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["TIP"]).Value);
            NDAY = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["NDAY"]).Value);
            NED = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["NED"]).Value);
            FIO = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["FIO"]).Value);
            GR3 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["GR3"]).Value);
            GRUP = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["GRUP"]).Value);
            RZ2 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["RZ2"]).Value);
            RZ3 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["RZ3"]).Value);

            return ret;
        }

        #endregion

        #region RAGR

        public partial class RAGRResult
        {
            public short? R7 { get; set; }
            public short? R5 { get; set; }
            public string D3 { get; set; }
            public string D2 { get; set; }
            public string D35 { get; set; }
            public string D34 { get; set; }
            public string D33 { get; set; }
            public string D32 { get; set; }
            public string D28 { get; set; }
            public string D37 { get; set; }
            public string D36 { get; set; }
            public string D27 { get; set; }
            public string US4 { get; set; }
            public string A2 { get; set; }
            public DateTime? R2 { get; set; }
            public int? R3 { get; set; }
            public DateTime? R11 { get; set; }
            public string TIP { get; set; }
            public int? NDAY { get; set; }
            public int? NED { get; set; }
            public string FIO_FULL { get; set; }
            public string FIO { get; set; }
            public string GR13 { get; set; }
            public string GR3 { get; set; }
            public string RZ2 { get; set; }
            public string RZ3 { get; set; }
        }

        public static IEnumerable<RAGRResult> RAGR(this DataConnection dataConnection, int? LANG, int? GR1, DateTime? DAT1, DateTime? DAT2, out short? R7, out short? R5, out string D3, out string D2, out string D35, out string D34, out string D33, out string D32, out string D28, out string D37, out string D36, out string D27, out string US4, out string A2, out DateTime? R2, out int? R3, out DateTime? R11, out string TIP, out int? NDAY, out int? NED, out string FIO_FULL, out string FIO, out char? GR13, out string GR3, out string RZ2, out string RZ3)
        {
            var ret = dataConnection.QueryProc<RAGRResult>("RAGR",
                new DataParameter("LANG", LANG, DataType.Int32),
                new DataParameter("GR1", GR1, DataType.Int32),
                new DataParameter("DAT1", DAT1, DataType.DateTime),
                new DataParameter("DAT2", DAT2, DataType.DateTime));

            R7 = Converter.ChangeTypeTo<short?>(((IDbDataParameter)dataConnection.Command.Parameters["R7"]).Value);
            R5 = Converter.ChangeTypeTo<short?>(((IDbDataParameter)dataConnection.Command.Parameters["R5"]).Value);
            D3 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["D3"]).Value);
            D2 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["D2"]).Value);
            D35 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["D35"]).Value);
            D34 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["D34"]).Value);
            D33 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["D33"]).Value);
            D32 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["D32"]).Value);
            D28 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["D28"]).Value);
            D37 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["D37"]).Value);
            D36 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["D36"]).Value);
            D27 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["D27"]).Value);
            US4 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["US4"]).Value);
            A2 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["A2"]).Value);
            R2 = Converter.ChangeTypeTo<DateTime?>(((IDbDataParameter)dataConnection.Command.Parameters["R2"]).Value);
            R3 = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["R3"]).Value);
            R11 = Converter.ChangeTypeTo<DateTime?>(((IDbDataParameter)dataConnection.Command.Parameters["R11"]).Value);
            TIP = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["TIP"]).Value);
            NDAY = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["NDAY"]).Value);
            NED = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["NED"]).Value);
            FIO_FULL = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["FIO_FULL"]).Value);
            FIO = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["FIO"]).Value);
            GR13 = Converter.ChangeTypeTo<char?>(((IDbDataParameter)dataConnection.Command.Parameters["GR13"]).Value);
            GR3 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["GR3"]).Value);
            RZ2 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["RZ2"]).Value);
            RZ3 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["RZ3"]).Value);

            return ret;
        }

        #endregion

        #region RAPR

        public partial class RAPRResult
        {
            public string D37 { get; set; }
            public string D36 { get; set; }
            public string D2 { get; set; }
            public string D35 { get; set; }
            public string D34 { get; set; }
            public string D33 { get; set; }
            public string D32 { get; set; }
            public string D28 { get; set; }
            public string D27 { get; set; }
            public string D3 { get; set; }
            public string D4 { get; set; }
            public string US4 { get; set; }
            public string GR3 { get; set; }
            public DateTime? R2 { get; set; }
            public int? R3 { get; set; }
            public int? R6 { get; set; }
            public string A2 { get; set; }
            public DateTime? R11 { get; set; }
            public string TIP { get; set; }
            public int? NDAY { get; set; }
            public int? NED { get; set; }
            public int? R1 { get; set; }
            public int? UG2 { get; set; }
            public string GR13_ { get; set; }
            public int? KURS { get; set; }
            public int? NR17 { get; set; }
            public string RZ2 { get; set; }
            public string RZ3 { get; set; }
        }

        public static IEnumerable<RAPRResult> RAPR(this DataConnection dataConnection, int? P1, DateTime? DAT1, DateTime? DAT2, out string D37, out string D36, out string D2, out string D35, out string D34, out string D33, out string D32, out string D28, out string D27, out string D3, out string D4, out string US4, out string GR3, out DateTime? R2, out int? R3, out int? R6, out string A2, out DateTime? R11, out string TIP, out int? NDAY, out int? NED, out int? R1, out int? UG2, out char? GR13_, out int? KURS, out int? NR17, out string RZ2, out string RZ3)
        {
            var ret = dataConnection.QueryProc<RAPRResult>("RAPR",
                new DataParameter("P1", P1, DataType.Int32),
                new DataParameter("DAT1", DAT1, DataType.DateTime),
                new DataParameter("DAT2", DAT2, DataType.DateTime));

            D37 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["D37"]).Value);
            D36 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["D36"]).Value);
            D2 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["D2"]).Value);
            D35 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["D35"]).Value);
            D34 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["D34"]).Value);
            D33 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["D33"]).Value);
            D32 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["D32"]).Value);
            D28 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["D28"]).Value);
            D27 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["D27"]).Value);
            D3 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["D3"]).Value);
            D4 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["D4"]).Value);
            US4 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["US4"]).Value);
            GR3 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["GR3"]).Value);
            R2 = Converter.ChangeTypeTo<DateTime?>(((IDbDataParameter)dataConnection.Command.Parameters["R2"]).Value);
            R3 = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["R3"]).Value);
            R6 = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["R6"]).Value);
            A2 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["A2"]).Value);
            R11 = Converter.ChangeTypeTo<DateTime?>(((IDbDataParameter)dataConnection.Command.Parameters["R11"]).Value);
            TIP = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["TIP"]).Value);
            NDAY = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["NDAY"]).Value);
            NED = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["NED"]).Value);
            R1 = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["R1"]).Value);
            UG2 = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["UG2"]).Value);
            GR13_ = Converter.ChangeTypeTo<char?>(((IDbDataParameter)dataConnection.Command.Parameters["GR13_"]).Value);
            KURS = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["KURS"]).Value);
            NR17 = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["NR17"]).Value);
            RZ2 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["RZ2"]).Value);
            RZ3 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["RZ3"]).Value);

            return ret;
        }

        #endregion

        #region RAPRN

        public partial class RAPRNResult
        {
            public string D3 { get; set; }
            public string D4 { get; set; }
            public string US4 { get; set; }
            public string GR3 { get; set; }
            public DateTime? R2 { get; set; }
            public int? R3 { get; set; }
            public int? R6 { get; set; }
            public string A2 { get; set; }
            public string TIP { get; set; }
            public int? R1 { get; set; }
            public int? R4 { get; set; }
        }

        public static IEnumerable<RAPRNResult> RAPRN(this DataConnection dataConnection, int? P1, DateTime? DAT1, DateTime? DAT2, out string D3, out string D4, out string US4, out string GR3, out DateTime? R2, out int? R3, out int? R6, out string A2, out string TIP, out int? R1, out int? R4)
        {
            var ret = dataConnection.QueryProc<RAPRNResult>("RAPRN",
                new DataParameter("P1", P1, DataType.Int32),
                new DataParameter("DAT1", DAT1, DataType.DateTime),
                new DataParameter("DAT2", DAT2, DataType.DateTime));

            D3 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["D3"]).Value);
            D4 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["D4"]).Value);
            US4 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["US4"]).Value);
            GR3 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["GR3"]).Value);
            R2 = Converter.ChangeTypeTo<DateTime?>(((IDbDataParameter)dataConnection.Command.Parameters["R2"]).Value);
            R3 = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["R3"]).Value);
            R6 = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["R6"]).Value);
            A2 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["A2"]).Value);
            TIP = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["TIP"]).Value);
            R1 = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["R1"]).Value);
            R4 = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["R4"]).Value);

            return ret;
        }

        #endregion

        #region RAPRW

        public partial class RAPRWResult
        {
            public string D3 { get; set; }
            public string D4 { get; set; }
            public string US4 { get; set; }
            public string GR3 { get; set; }
            public DateTime? R2 { get; set; }
            public int? R3 { get; set; }
            public int? R5 { get; set; }
            public int? R6 { get; set; }
            public string A2 { get; set; }
            public DateTime? R11 { get; set; }
            public string TIP { get; set; }
            public int? NDAY { get; set; }
            public int? NED { get; set; }
            public int? R1 { get; set; }
            public int? UG2 { get; set; }
            public int? KURS { get; set; }
            public DateTime? SG7 { get; set; }
            public int? R4 { get; set; }
            public short? RZ14 { get; set; }
            public short? RZ15 { get; set; }
        }

        public static IEnumerable<RAPRWResult> RAPRW(this DataConnection dataConnection, int? P1, DateTime? DAT1, DateTime? DAT2, out string D3, out string D4, out string US4, out string GR3, out DateTime? R2, out int? R3, out int? R5, out int? R6, out string A2, out DateTime? R11, out string TIP, out int? NDAY, out int? NED, out int? R1, out int? UG2, out int? KURS, out DateTime? SG7, out int? R4, out short? RZ14, out short? RZ15)
        {
            var ret = dataConnection.QueryProc<RAPRWResult>("RAPRW",
                new DataParameter("P1", P1, DataType.Int32),
                new DataParameter("DAT1", DAT1, DataType.DateTime),
                new DataParameter("DAT2", DAT2, DataType.DateTime));

            D3 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["D3"]).Value);
            D4 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["D4"]).Value);
            US4 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["US4"]).Value);
            GR3 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["GR3"]).Value);
            R2 = Converter.ChangeTypeTo<DateTime?>(((IDbDataParameter)dataConnection.Command.Parameters["R2"]).Value);
            R3 = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["R3"]).Value);
            R5 = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["R5"]).Value);
            R6 = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["R6"]).Value);
            A2 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["A2"]).Value);
            R11 = Converter.ChangeTypeTo<DateTime?>(((IDbDataParameter)dataConnection.Command.Parameters["R11"]).Value);
            TIP = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["TIP"]).Value);
            NDAY = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["NDAY"]).Value);
            NED = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["NED"]).Value);
            R1 = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["R1"]).Value);
            UG2 = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["UG2"]).Value);
            KURS = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["KURS"]).Value);
            SG7 = Converter.ChangeTypeTo<DateTime?>(((IDbDataParameter)dataConnection.Command.Parameters["SG7"]).Value);
            R4 = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["R4"]).Value);
            RZ14 = Converter.ChangeTypeTo<short?>(((IDbDataParameter)dataConnection.Command.Parameters["RZ14"]).Value);
            RZ15 = Converter.ChangeTypeTo<short?>(((IDbDataParameter)dataConnection.Command.Parameters["RZ15"]).Value);

            return ret;
        }

        #endregion

        #region RAST

        public partial class RASTResult
        {
            public string D37 { get; set; }
            public string D36 { get; set; }
            public string P110 { get; set; }
            public string P109 { get; set; }
            public string P108 { get; set; }
            public string P107 { get; set; }
            public string P106 { get; set; }
            public string P105 { get; set; }
            public string P104 { get; set; }
            public string P103 { get; set; }
            public string P102 { get; set; }
            public string P78 { get; set; }
            public string P77 { get; set; }
            public string P76 { get; set; }
            public string D35 { get; set; }
            public string D34 { get; set; }
            public string D33 { get; set; }
            public string D32 { get; set; }
            public string D28 { get; set; }
            public string D27 { get; set; }
            public string D3 { get; set; }
            public string D2 { get; set; }
            public string US4 { get; set; }
            public string A2 { get; set; }
            public DateTime? R2 { get; set; }
            public int? R3 { get; set; }
            public DateTime? R11 { get; set; }
            public string TIP { get; set; }
            public int? NDAY { get; set; }
            public int? NED { get; set; }
            public string GR3 { get; set; }
            public string GR13 { get; set; }
            public string FIO_FULL { get; set; }
            public string FIO { get; set; }
            public string RZ2 { get; set; }
            public string RZ3 { get; set; }
            public int? R1_ { get; set; }
        }

        public static IEnumerable<RASTResult> RAST(this DataConnection dataConnection, int? LANG, int? ST1, DateTime? DAT1, DateTime? DAT2, out string D37, out string D36, out string P110, out string P109, out string P108, out string P107, out string P106, out string P105, out string P104, out string P103, out string P102, out string P78, out string P77, out string P76, out string D35, out string D34, out string D33, out string D32, out string D28, out string D27, out string D3, out string D2, out string US4, out string A2, out DateTime? R2, out int? R3, out DateTime? R11, out string TIP, out int? NDAY, out int? NED, out string GR3, out char? GR13, out string FIO_FULL, out string FIO, out string RZ2, out string RZ3, out int? R1_)
        {
            var ret = dataConnection.QueryProc<RASTResult>("RAST",
                new DataParameter("LANG", LANG, DataType.Int32),
                new DataParameter("ST1", ST1, DataType.Int32),
                new DataParameter("DAT1", DAT1, DataType.DateTime),
                new DataParameter("DAT2", DAT2, DataType.DateTime));

            D37 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["D37"]).Value);
            D36 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["D36"]).Value);
            P110 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["P110"]).Value);
            P109 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["P109"]).Value);
            P108 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["P108"]).Value);
            P107 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["P107"]).Value);
            P106 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["P106"]).Value);
            P105 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["P105"]).Value);
            P104 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["P104"]).Value);
            P103 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["P103"]).Value);
            P102 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["P102"]).Value);
            P78 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["P78"]).Value);
            P77 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["P77"]).Value);
            P76 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["P76"]).Value);
            D35 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["D35"]).Value);
            D34 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["D34"]).Value);
            D33 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["D33"]).Value);
            D32 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["D32"]).Value);
            D28 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["D28"]).Value);
            D27 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["D27"]).Value);
            D3 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["D3"]).Value);
            D2 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["D2"]).Value);
            US4 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["US4"]).Value);
            A2 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["A2"]).Value);
            R2 = Converter.ChangeTypeTo<DateTime?>(((IDbDataParameter)dataConnection.Command.Parameters["R2"]).Value);
            R3 = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["R3"]).Value);
            R11 = Converter.ChangeTypeTo<DateTime?>(((IDbDataParameter)dataConnection.Command.Parameters["R11"]).Value);
            TIP = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["TIP"]).Value);
            NDAY = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["NDAY"]).Value);
            NED = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["NED"]).Value);
            GR3 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["GR3"]).Value);
            GR13 = Converter.ChangeTypeTo<char?>(((IDbDataParameter)dataConnection.Command.Parameters["GR13"]).Value);
            FIO_FULL = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["FIO_FULL"]).Value);
            FIO = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["FIO"]).Value);
            RZ2 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["RZ2"]).Value);
            RZ3 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["RZ3"]).Value);
            R1_ = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["R1_"]).Value);

            return ret;
        }

        #endregion

        #region STAT_PROP

        public partial class STAT_PROPResult
        {
            public string D2 { get; set; }
            public short? NON_ZAN { get; set; }
            public DateTime? R2 { get; set; }
            public short? PROP { get; set; }
            public short? R4 { get; set; }
            public short? US4 { get; set; }
        }

        public static IEnumerable<STAT_PROPResult> STAT_PROP(this DataConnection dataConnection, int? ST1, DateTime? DATA_NAC, DateTime? DATA_KON, out string D2, out short? NON_ZAN, out DateTime? R2, out short? PROP, out short? R4, out short? US4)
        {
            var ret = dataConnection.QueryProc<STAT_PROPResult>("STAT_PROP",
                new DataParameter("ST1", ST1, DataType.Int32),
                new DataParameter("DATA_NAC", DATA_NAC, DataType.DateTime),
                new DataParameter("DATA_KON", DATA_KON, DataType.DateTime));

            D2 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["D2"]).Value);
            NON_ZAN = Converter.ChangeTypeTo<short?>(((IDbDataParameter)dataConnection.Command.Parameters["NON_ZAN"]).Value);
            R2 = Converter.ChangeTypeTo<DateTime?>(((IDbDataParameter)dataConnection.Command.Parameters["R2"]).Value);
            PROP = Converter.ChangeTypeTo<short?>(((IDbDataParameter)dataConnection.Command.Parameters["PROP"]).Value);
            R4 = Converter.ChangeTypeTo<short?>(((IDbDataParameter)dataConnection.Command.Parameters["R4"]).Value);
            US4 = Converter.ChangeTypeTo<short?>(((IDbDataParameter)dataConnection.Command.Parameters["US4"]).Value);

            return ret;
        }

        #endregion

        #region STUD_REYTING

        public partial class STUD_REYTINGResult
        {
            public string FIO { get; set; }
            public string NAME { get; set; }
            public string OTCH { get; set; }
            public int? GR1 { get; set; }
            public int? KYRS { get; set; }
            public float? CREDNIY_BAL_5 { get; set; }
            public float? CREDNIY_BAL_100 { get; set; }
            public int? NE_SDANO { get; set; }
        }

        public static IEnumerable<STUD_REYTINGResult> STUD_REYTING(this DataConnection dataConnection, int? KOD_SG1, int? KOD_GR1, int? SEM1, int? SEM2, int? INOST, out string FIO, out string NAME, out string OTCH, out int? GR1, out int? KYRS, out float? CREDNIY_BAL_5, out float? CREDNIY_BAL_100, out int? NE_SDANO)
        {
            var ret = dataConnection.QueryProc<STUD_REYTINGResult>("STUD_REYTING",
                new DataParameter("KOD_SG1", KOD_SG1, DataType.Int32),
                new DataParameter("KOD_GR1", KOD_GR1, DataType.Int32),
                new DataParameter("SEM1", SEM1, DataType.Int32),
                new DataParameter("SEM2", SEM2, DataType.Int32),
                new DataParameter("INOST", INOST, DataType.Int32));

            FIO = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["FIO"]).Value);
            NAME = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["NAME"]).Value);
            OTCH = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["OTCH"]).Value);
            GR1 = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["GR1"]).Value);
            KYRS = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["KYRS"]).Value);
            CREDNIY_BAL_5 = Converter.ChangeTypeTo<float?>(((IDbDataParameter)dataConnection.Command.Parameters["CREDNIY_BAL_5"]).Value);
            CREDNIY_BAL_100 = Converter.ChangeTypeTo<float?>(((IDbDataParameter)dataConnection.Command.Parameters["CREDNIY_BAL_100"]).Value);
            NE_SDANO = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["NE_SDANO"]).Value);

            return ret;
        }

        #endregion

        #region STUS_AN

        public partial class STUS_ANResult
        {
            public string ST125 { get; set; }
            public string ST124 { get; set; }
            public string ST123 { get; set; }
            public string D36 { get; set; }
            public string D34 { get; set; }
            public string D32 { get; set; }
            public string D27 { get; set; }
            public string ST122 { get; set; }
            public string ST119 { get; set; }
            public string ST76 { get; set; }
            public string ST121 { get; set; }
            public string ST118 { get; set; }
            public string ST75 { get; set; }
            public string ST120 { get; set; }
            public string ST117 { get; set; }
            public string ST74 { get; set; }
            public int? STUS3 { get; set; }
            public string STUS7 { get; set; }
            public string STUS11 { get; set; }
            public string D2 { get; set; }
            public int? ST1 { get; set; }
            public string ST2 { get; set; }
            public string ST3 { get; set; }
            public string ST4 { get; set; }
            public int? STUS8 { get; set; }
            public short? D30 { get; set; }
            public string GR3 { get; set; }
            public string GR19 { get; set; }
            public string GR20 { get; set; }
            public string GR21 { get; set; }
            public string GR22 { get; set; }
            public string GR23 { get; set; }
            public string GR28 { get; set; }
            public string GR24 { get; set; }
            public int? ST56 { get; set; }
            public DateTime? STUS6 { get; set; }
            public int? STUS20_ { get; set; }
            public short? STUS19 { get; set; }
        }

        public static IEnumerable<STUS_ANResult> STUS_AN(this DataConnection dataConnection, int? GR1, int? KOD_DEYSTV, int? KOD_ST1, short? SEM_NAC, short? SEM_KON, out string ST125, out string ST124, out string ST123, out string D36, out string D34, out string D32, out string D27, out string ST122, out string ST119, out string ST76, out string ST121, out string ST118, out string ST75, out string ST120, out string ST117, out string ST74, out int? STUS3, out string STUS7, out string STUS11, out string D2, out int? ST1, out string ST2, out string ST3, out string ST4, out int? STUS8, out short? D30, out string GR3, out string GR19, out string GR20, out string GR21, out string GR22, out string GR23, out string GR28, out string GR24, out int? ST56, out DateTime? STUS6, out int? STUS20_, out short? STUS19)
        {
            var ret = dataConnection.QueryProc<STUS_ANResult>("STUS_AN",
                new DataParameter("GR1", GR1, DataType.Int32),
                new DataParameter("KOD_DEYSTV", KOD_DEYSTV, DataType.Int32),
                new DataParameter("KOD_ST1", KOD_ST1, DataType.Int32),
                new DataParameter("SEM_NAC", SEM_NAC, DataType.Int16),
                new DataParameter("SEM_KON", SEM_KON, DataType.Int16));

            ST125 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["ST125"]).Value);
            ST124 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["ST124"]).Value);
            ST123 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["ST123"]).Value);
            D36 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["D36"]).Value);
            D34 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["D34"]).Value);
            D32 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["D32"]).Value);
            D27 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["D27"]).Value);
            ST122 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["ST122"]).Value);
            ST119 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["ST119"]).Value);
            ST76 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["ST76"]).Value);
            ST121 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["ST121"]).Value);
            ST118 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["ST118"]).Value);
            ST75 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["ST75"]).Value);
            ST120 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["ST120"]).Value);
            ST117 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["ST117"]).Value);
            ST74 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["ST74"]).Value);
            STUS3 = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["STUS3"]).Value);
            STUS7 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["STUS7"]).Value);
            STUS11 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["STUS11"]).Value);
            D2 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["D2"]).Value);
            ST1 = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["ST1"]).Value);
            ST2 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["ST2"]).Value);
            ST3 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["ST3"]).Value);
            ST4 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["ST4"]).Value);
            STUS8 = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["STUS8"]).Value);
            D30 = Converter.ChangeTypeTo<short?>(((IDbDataParameter)dataConnection.Command.Parameters["D30"]).Value);
            GR3 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["GR3"]).Value);
            GR19 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["GR19"]).Value);
            GR20 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["GR20"]).Value);
            GR21 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["GR21"]).Value);
            GR22 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["GR22"]).Value);
            GR23 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["GR23"]).Value);
            GR28 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["GR28"]).Value);
            GR24 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["GR24"]).Value);
            ST56 = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["ST56"]).Value);
            STUS6 = Converter.ChangeTypeTo<DateTime?>(((IDbDataParameter)dataConnection.Command.Parameters["STUS6"]).Value);
            STUS20_ = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["STUS20_"]).Value);
            STUS19 = Converter.ChangeTypeTo<short?>(((IDbDataParameter)dataConnection.Command.Parameters["STUS19"]).Value);

            return ret;
        }

        #endregion

        #region STUS_GR1

        public partial class STUS_GR1Result
        {
            public int? STUS18 { get; set; }
            public string D2 { get; set; }
            public string D27 { get; set; }
            public short? STUS19 { get; set; }
            public int? ST1 { get; set; }
            public string ST2 { get; set; }
            public string ST3 { get; set; }
            public string ST4 { get; set; }
            public int? STUS20 { get; set; }
            public int? STUS3 { get; set; }
            public short? STUS8 { get; set; }
            public string STUS11 { get; set; }
        }

        public static IEnumerable<STUS_GR1Result> STUS_GR1(this DataConnection dataConnection, int? GR1, int? SEM_NAC, int? SEM_KON, out int? STUS18, out string D2, out string D27, out short? STUS19, out int? ST1, out string ST2, out string ST3, out string ST4, out int? STUS20, out int? STUS3, out short? STUS8, out string STUS11)
        {
            var ret = dataConnection.QueryProc<STUS_GR1Result>("STUS_GR1",
                new DataParameter("GR1", GR1, DataType.Int32),
                new DataParameter("SEM_NAC", SEM_NAC, DataType.Int32),
                new DataParameter("SEM_KON", SEM_KON, DataType.Int32));

            STUS18 = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["STUS18"]).Value);
            D2 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["D2"]).Value);
            D27 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["D27"]).Value);
            STUS19 = Converter.ChangeTypeTo<short?>(((IDbDataParameter)dataConnection.Command.Parameters["STUS19"]).Value);
            ST1 = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["ST1"]).Value);
            ST2 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["ST2"]).Value);
            ST3 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["ST3"]).Value);
            ST4 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["ST4"]).Value);
            STUS20 = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["STUS20"]).Value);
            STUS3 = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["STUS3"]).Value);
            STUS8 = Converter.ChangeTypeTo<short?>(((IDbDataParameter)dataConnection.Command.Parameters["STUS8"]).Value);
            STUS11 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["STUS11"]).Value);

            return ret;
        }

        #endregion

        #region STUS_SEM

        public partial class STUS_SEMResult
        {
            public int? STUS8 { get; set; }
            public int? STUS18 { get; set; }
            public int? STUS4 { get; set; }
        }

        public static IEnumerable<STUS_SEMResult> STUS_SEM(this DataConnection dataConnection, short? UCH_GOD, short? SEM, short? SEM_POD, string SG4_, short? PRIZ, out int? STUS8, out int? STUS18, out int? STUS4)
        {
            var ret = dataConnection.QueryProc<STUS_SEMResult>("STUS_SEM",
                new DataParameter("UCH_GOD", UCH_GOD, DataType.Int16),
                new DataParameter("SEM", SEM, DataType.Int16),
                new DataParameter("SEM_POD", SEM_POD, DataType.Int16),
                new DataParameter("SG4_", SG4_, DataType.NVarChar),
                new DataParameter("PRIZ", PRIZ, DataType.Int16));

            STUS8 = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["STUS8"]).Value);
            STUS18 = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["STUS18"]).Value);
            STUS4 = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["STUS4"]).Value);

            return ret;
        }

        #endregion

        #region STUS_STAT

        public partial class STUS_STATResult
        {
            public string ST125 { get; set; }
            public string ST124 { get; set; }
            public string ST123 { get; set; }
            public string ST122 { get; set; }
            public string ST121 { get; set; }
            public string ST120 { get; set; }
            public string ST119 { get; set; }
            public string ST118 { get; set; }
            public string ST117 { get; set; }
            public string ST76 { get; set; }
            public string ST75 { get; set; }
            public string ST74 { get; set; }
            public int? STUS1 { get; set; }
            public int? STUS4 { get; set; }
            public short? STUS8 { get; set; }
            public int? STUS18 { get; set; }
            public short? STUS19 { get; set; }
            public int? STD3 { get; set; }
            public string ST2 { get; set; }
            public string ST3 { get; set; }
            public string ST4 { get; set; }
            public int? ST63 { get; set; }
            public int? SK3 { get; set; }
        }

        public static IEnumerable<STUS_STATResult> STUS_STAT(this DataConnection dataConnection, int? GR1, int? SEM7, DateTime? DAT, out string ST125, out string ST124, out string ST123, out string ST122, out string ST121, out string ST120, out string ST119, out string ST118, out string ST117, out string ST76, out string ST75, out string ST74, out int? STUS1, out int? STUS4, out short? STUS8, out int? STUS18, out short? STUS19, out int? STD3, out string ST2, out string ST3, out string ST4, out int? ST63, out int? SK3)
        {
            var ret = dataConnection.QueryProc<STUS_STATResult>("STUS_STAT",
                new DataParameter("GR1", GR1, DataType.Int32),
                new DataParameter("SEM7", SEM7, DataType.Int32),
                new DataParameter("DAT", DAT, DataType.DateTime));

            ST125 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["ST125"]).Value);
            ST124 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["ST124"]).Value);
            ST123 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["ST123"]).Value);
            ST122 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["ST122"]).Value);
            ST121 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["ST121"]).Value);
            ST120 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["ST120"]).Value);
            ST119 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["ST119"]).Value);
            ST118 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["ST118"]).Value);
            ST117 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["ST117"]).Value);
            ST76 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["ST76"]).Value);
            ST75 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["ST75"]).Value);
            ST74 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["ST74"]).Value);
            STUS1 = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["STUS1"]).Value);
            STUS4 = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["STUS4"]).Value);
            STUS8 = Converter.ChangeTypeTo<short?>(((IDbDataParameter)dataConnection.Command.Parameters["STUS8"]).Value);
            STUS18 = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["STUS18"]).Value);
            STUS19 = Converter.ChangeTypeTo<short?>(((IDbDataParameter)dataConnection.Command.Parameters["STUS19"]).Value);
            STD3 = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["STD3"]).Value);
            ST2 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["ST2"]).Value);
            ST3 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["ST3"]).Value);
            ST4 = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["ST4"]).Value);
            ST63 = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["ST63"]).Value);
            SK3 = Converter.ChangeTypeTo<int?>(((IDbDataParameter)dataConnection.Command.Parameters["SK3"]).Value);

            return ret;
        }

        #endregion

        #region TEMA_NAME

        public partial class TEMA_NAMEResult
        {
            public short? NOM_TEMI { get; set; }
            public short? NOM_ZAN { get; set; }
            public string NAME_TEMI { get; set; }
        }

        public static IEnumerable<TEMA_NAMEResult> TEMA_NAME(this DataConnection dataConnection, int? R1, DateTime? R2, out short? NOM_TEMI, out short? NOM_ZAN, out string NAME_TEMI)
        {
            var ret = dataConnection.QueryProc<TEMA_NAMEResult>("TEMA_NAME",
                new DataParameter("R1", R1, DataType.Int32),
                new DataParameter("R2", R2, DataType.DateTime));

            NOM_TEMI = Converter.ChangeTypeTo<short?>(((IDbDataParameter)dataConnection.Command.Parameters["NOM_TEMI"]).Value);
            NOM_ZAN = Converter.ChangeTypeTo<short?>(((IDbDataParameter)dataConnection.Command.Parameters["NOM_ZAN"]).Value);
            NAME_TEMI = Converter.ChangeTypeTo<string>(((IDbDataParameter)dataConnection.Command.Parameters["NAME_TEMI"]).Value);

            return ret;
        }

        #endregion
    }
}