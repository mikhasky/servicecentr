﻿using FirebirdSql.Data.FirebirdClient;
using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;

namespace MKP.DBImplementation
{
    public static class WorkWhithDB
    {
        public static List<IDb> GetData<IDb>(string query, string connstring)
        {
            var listData = new List<IDb>();
            using (FbConnection fbConnection = new FbConnection(connstring))
            {
                using (DataContext dataContext = new DataContext(fbConnection))
                {
                    try
                    {
                        listData = dataContext.ExecuteQuery<IDb>(query).ToList();
                    }
                    catch (Exception ex)
                    {
                        if(ex is FbException)
                            throw ex as FbException;
                        throw ex;
                    }
                }
            }
            return listData;
        }

        public static bool ExecutNonQuery(FbCommand fbCommand, string connstring, bool showException)
        {
            bool result;
            using (FbConnection fbConnection = new FbConnection(connstring))
            {
                fbConnection.Open();
                using (FbTransaction fbTransaction = fbConnection.BeginTransaction())
                {
                    try
                    {
                        fbCommand.Transaction = fbTransaction;
                        fbCommand.Connection = fbConnection;
                        try
                        {
                            fbCommand.ExecuteNonQuery();
                            fbTransaction.Commit();
                        }
                        catch (FbException ex)
                        {
                            fbTransaction.Rollback();
                            throw ex;
                        }
                    }
                    finally
                    {
                        if (fbCommand != null)
                        {
                            ((IDisposable)fbCommand).Dispose();
                        }
                    }
                }
            }
            result = true;
            return result;
        }
        public static bool ExecutNonQuery(string query, string connstring,params KeyValuePair<string, object>[] parameters)
        {
            bool result;
            using (FbConnection fbConnection = new FbConnection(connstring))
            {
                fbConnection.Open();
                using (FbTransaction fbTransaction = fbConnection.BeginTransaction())
                {
                    FbCommand fbCommand = new FbCommand(query);
                    try
                    {
                        if (parameters != null)
                        {
                            foreach (var item in parameters)
                            {
                                fbCommand.Parameters.Add($"{item.Key}", item.Value);
                            }
                        }
                        fbCommand.Transaction = fbTransaction;
                        fbCommand.Connection = fbConnection;
                        try
                        {
                            fbCommand.ExecuteNonQuery();
                            fbTransaction.Commit();
                            result = true;
                        }
                        catch (FbException ex)
                        {
                            result = false;
                            throw ex;
                        }
                    }
                    finally
                    {
                        if (fbCommand != null)
                        {
                            ((IDisposable)fbCommand).Dispose();
                        }
                    }
                }
            }
            return result;
        }
        /// <summary>
        /// Получить данные по запросу
        /// </summary>
        /// <param name="query">Строка запроса </param>
        /// <param name="connstring ">Строка подключения</param>
        /// <return s></return s>
        public static List<T> GetDataForQuery<T>(string query, string connstring,params KeyValuePair<string, object>[] parameters)
        {
            var listData = new List<T>();
            using (FbConnection fbConnection = new FbConnection(connstring))
            {
                fbConnection.Open();
                using (FbCommand command = new FbCommand(query, fbConnection))
                {
                    if (parameters != null)
                        foreach (var item in parameters)
                        {
                            command.Parameters.Add($"{item.Key}", item.Value);
                        }
                    using (FbDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            using (DataContext dataContext = new DataContext(fbConnection))
                            {
                                try
                                {

                                    listData = dataContext.Translate<T>(reader).ToList();

                                }
                                catch (Exception ex)
                                {
                                    throw ex;
                                }
                            }
                        }
                    }
                }
            }
            return listData;
        }
        [Obsolete("Старая версия", true)]
        public static bool ExecutNonQuery(string query, string connstring, bool showException, KeyValuePair<string, object>[] parameters = null)
        {
            bool result;
            using (FbConnection fbConnection = new FbConnection(connstring))
            {
                fbConnection.Open();
                using (FbTransaction fbTransaction = fbConnection.BeginTransaction())
                {
                    FbCommand fbCommand = new FbCommand(query);
                    try
                    {
                        if (parameters != null)
                        {
                            foreach (var item in parameters)
                            {
                                fbCommand.Parameters.Add($"@{item.Key}", item.Value);
                            }
                        }
                        fbCommand.Transaction = fbTransaction;
                        fbCommand.Connection = fbConnection;
                        try
                        {
                            fbCommand.ExecuteNonQuery();
                            fbTransaction.Commit();
                            result = true;
                        }
                        catch (FbException ex)
                        {
                            result = false;
                            throw ex;
                        }
                    }
                    finally
                    {
                        if (fbCommand != null)
                        {
                            ((IDisposable)fbCommand).Dispose();
                        }
                    }
                }
            }
            return result;
        }
        public static bool UpdateValue(this ITable instatce, string connstring, string where)
        {
            return UpdateValue(instatce, connstring, false, where);
        }
        public static bool UpdateValue(this ITable instatce, string connstring)
        {
            return UpdateValue(instatce, connstring, false, string.Empty);
        }
        public static bool UpdateValue(this ITable instatce, string connstring, bool showException, string where)
        {
            var type = instatce.GetType();
            string update = type.Name;
            List<KeyValuePair<string, object>> set = new List<KeyValuePair<string, object>>();
            bool neadWhere = string.IsNullOrEmpty(where);
            if (!neadWhere && where.ToLower().StartsWith("where"))
            {
                where = where.Replace("where", "");
            }
            foreach (var item in type.GetProperties())
            {
                if (neadWhere)
                {
                    var key = item.GetCustomAttributes(typeof(KeyAttribute), false);
                    if (key.Length > 0)
                        where += string.Format(" {0}={1} and", item.Name, item.GetValue(instatce, null));
                    else
                        set.Add(CreateSetForUpdate(instatce, item));
                }
                else
                    set.Add(CreateSetForUpdate(instatce, item));
            }
            if (where.EndsWith("and"))
            {
                where = where.Remove(where.Length - 4);
            }
            string query = $"Update {update} set {string.Join(" , ", set.Select(i => $"{i.Key}=@{i.Key}"))} where {where}";
            return ExecutNonQuery(query, connstring, set.ToArray());
        }
        private static KeyValuePair<string, object> CreateSetForUpdate(ITable instatce, System.Reflection.PropertyInfo item)
        {
            return new KeyValuePair<string, object>(item.Name, item.GetValue(instatce, null));
        }

        public static bool InsertValue(this ITable instatce, string connstring)
        {
            var type = instatce.GetType();
            var prop = type.GetProperties();
            string TableName = type.Name;
            var dict = prop.Select(i => GetValuesForInsert(instatce, i)).ToArray();
            var fields = string.Join(",", dict.Select(i => i.Key).ToArray());
            string Values = string.Join(",", dict.Select(i => "@" + i.Key).ToArray());
            //  fCommand.Parameters.Add("@slo1", slo);
            string query = $"insert into {TableName}({fields}) values({Values})";
            return ExecutNonQuery(query, connstring, dict);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="instatce"></param>
        /// <param name="connstring "></param>
        /// <param name="where"> только параметры фильта без слова where </param>
        public static void DeleteValue(this ITable instatce, string connstring, string where = "")
        {
            var type = instatce.GetType();
            var prop = type.GetProperties(bindingAttr: System.Reflection.BindingFlags.Public);
            string TableName = type.Name;

            foreach (var item in type.GetProperties())
            {
                if (string.IsNullOrEmpty(where))
                {
                    var key = item.GetCustomAttributes(typeof(KeyAttribute), false);
                    if (key.Length > 0)
                        where = string.Format(" {0}={1} ", item.Name, item.GetValue(instatce, null));
                }
            }
            if (string.IsNullOrEmpty(where))
                throw new ArgumentException();
            string query = $"delete from {TableName} where {where}";
            ExecutNonQuery(query, connstring);
        }
        private static KeyValuePair<string, object> GetValuesForInsert(ITable instatce, System.Reflection.PropertyInfo item)
        {
            string prpop = item.Name;
            object value = item.GetValue(instatce, null);
            KeyValuePair<string, object> rez = new KeyValuePair<string, object>(prpop, value);
            return rez;
        }
        public static int GenId(string table, string connBD)
        {
            string field = table + "1";
            if (table == "u" || table == "uo" || table == "us" || table == "nr" || table == "gen_vvmp" || table == "st" || table == "std" || table == "sk" || table == "stpr" || table == "stpr2" || table == "gen_nkrs")
                return GetData<singlVal>("select GEN_ID(" + table + ",1) as ID from rdb$database", connBD).Single().ID;
            return GetData<singlVal>("SELECT first 1 id1 as ID FROM pr1('" + field + "','" + table + "') left join " + table + " on (" + field + "=id1) where " + field + " is null", connBD).Single().ID;
        }
        class singlVal
        {
            public int ID;
        }
    }
}
