﻿namespace MKP.Printing
{
    partial class FMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FMain));
            this.MainSplitter = new System.Windows.Forms.SplitContainer();
            this.tbFind = new System.Windows.Forms.TextBox();
            this.cbDebug = new System.Windows.Forms.ComboBox();
            this.TopSplitter = new System.Windows.Forms.SplitContainer();
            this.tbMain = new FastColoredTextBoxNS.FastColoredTextBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menuCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.menuCut = new System.Windows.Forms.ToolStripMenuItem();
            this.menuPaste = new System.Windows.Forms.ToolStripMenuItem();
            this.menuMacroSplit = new System.Windows.Forms.ToolStripSeparator();
            this.menuMacro = new System.Windows.Forms.ToolStripMenuItem();
            this.menuMacroForm = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.menuKomandKonstr = new System.Windows.Forms.ToolStripMenuItem();
            this.пToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.числоToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.символToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.переченьПеременныхToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.числоToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.датаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.датаToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.еслиИначеКонецToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.циклКонецToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.покаКонецToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.очиститьМассивToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.запросSELECTFROMToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.параметрЗапросаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.открытьЗапроскудаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.сортироватьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFunkc = new System.Windows.Forms.ToolStripMenuItem();
            this.строкмассивКоличествоСтрокВМассивеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.столбцовмассивКоличествоСтолбцовВМассивеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.удалениеПробеловСправаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.удалениеПробеловСлеваToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.удалениеПробеловСправаИСлеваToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.округлитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.найтиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.вырезатьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.длиннаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.количествоДнейМеждуДатамиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.строкаВВидеСтрокаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.строкаВВидеСТРОКАToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.строкаВВидестрокаToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.датаToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.датаПрописьюToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.сToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.деньгиПрописьюToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuEXCEL = new System.Windows.Forms.ToolStripMenuItem();
            this.создатьEXCELToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.книгаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.альбомToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.создатьOPENToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.книгаToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.альбомToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.открытьEXCELToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.добавитьЛистовToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.названиеЛистаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.текущийЛистToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.поляЛистаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ориентацияЛистаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.книжнаяToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.альбомнаяToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.сохранитьEXCELToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.работаEXCELToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.форматA3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.форматА4ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.форматА5ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.заголовокСтраницToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.верхнийКолонтитулToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.слеваToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.поЦентруToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.справаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.нижнийКолонтитутToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.слеваToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.поЦентруToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.справаToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.центрироватьНаСтраницеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.горизонтальноToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.вертикальноToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.разрывСтраницыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.добавитьСтрокуEXCELToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.скрытьСтолбецToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.скрытьСтрокуToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.процедураОбьединенияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.пToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.столбцыДляСравненияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.выполнитьОбьединениеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuShirina = new System.Windows.Forms.ToolStripMenuItem();
            this.колонкиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.колонокToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuVisota = new System.Windows.Forms.ToolStripMenuItem();
            this.строкиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.строкToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.автоподборСтрокToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuTextToCell = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFont = new System.Windows.Forms.ToolStripMenuItem();
            this.размерШрифтаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.строкиToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.колонкиToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.ячейкиToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.названиеШрифтаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.строкиToolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.колонкиToolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.ячейкиToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.жирностьШрифтаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.строкиToolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.жирныйToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.отменаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.колонкиToolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.жирныйToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.отменаToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ячейкиToolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.жирныйToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.отменаToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.подчеркиваниеШрифтаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.строкиToolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.тонкойЛиниейToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.толстойЛиниейToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.отменаToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.колонкиToolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.тонкойЛиниейToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.толстойЛиниейToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.отменаToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.ячейкиToolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.тонкойЛиниейToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.толстойЛиниейToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.отменаToolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.наклонностьШрифтаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.строкиToolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.наклонныйToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.отменаToolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.колонкиToolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.наклонныйToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.отменаToolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.ячейкиToolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.наклонныйToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.отменаToolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuMerge = new System.Windows.Forms.ToolStripMenuItem();
            this.menuInCell = new System.Windows.Forms.ToolStripMenuItem();
            this.размерСимволовToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.жирностьСимволовToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.жирныйToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.отменаToolStripMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
            this.наклонностьСимволовToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.наклонныйToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.отменаToolStripMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
            this.подчеркиваниеСимволовToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.тонкойЛиниейToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.толстойЛиниейToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.отменаToolStripMenuItem11 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuShrink = new System.Windows.Forms.ToolStripMenuItem();
            this.поГоризонталиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.строкиToolStripMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
            this.поЗначениюToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.поЛевомуКраюToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.поЦентруToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.поПравомуКраюToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.поШиринеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.колонкиToolStripMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
            this.поЗначениюToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.ячейкиToolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.поЗначениюToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
            this.поВертикалиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.строкиToolStripMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
            this.поВерхнемуКраюToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.поЦентруToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.поНижнемуКраюToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.поВысотеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.колонкиToolStripMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
            this.поВерхнемуКраюToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem11 = new System.Windows.Forms.ToolStripMenuItem();
            this.ячейкиToolStripMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
            this.поВерхнемуКраюToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem12 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem13 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem14 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuLine = new System.Windows.Forms.ToolStripMenuItem();
            this.видЛинииToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.всейЯчейкиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.нетToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.толстаяToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.тонкаяToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.двойнаяToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.пунктирнаяToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.точкамиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.вокругЯчейкиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.нетToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem21 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem22 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem23 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem24 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem25 = new System.Windows.Forms.ToolStripMenuItem();
            this.сверхуЯчейкиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.нетToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem26 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem27 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem28 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem29 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem30 = new System.Windows.Forms.ToolStripMenuItem();
            this.слеваЯчейкиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.нетToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem31 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem32 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem33 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem34 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem35 = new System.Windows.Forms.ToolStripMenuItem();
            this.снизуЯчейкиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.нетToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem36 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem37 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem38 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem39 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem40 = new System.Windows.Forms.ToolStripMenuItem();
            this.справаЯчейкиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.нетToolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem41 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem42 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem43 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem44 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem45 = new System.Windows.Forms.ToolStripMenuItem();
            this.толщинаЛинииToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem46 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem48 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem49 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem50 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem51 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem53 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem52 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem54 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem55 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem56 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem60 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem66 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem57 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem58 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem59 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem67 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem73 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem61 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem62 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem63 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem74 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem80 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem64 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem65 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem68 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem81 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem87 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem69 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem70 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem71 = new System.Windows.Forms.ToolStripMenuItem();
            this.линияПоКоординатамToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFormat = new System.Windows.Forms.ToolStripMenuItem();
            this.строкиToolStripMenuItem11 = new System.Windows.Forms.ToolStripMenuItem();
            this.общийToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.числовойToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.текстовыйToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.датаToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.колонкиToolStripMenuItem11 = new System.Windows.Forms.ToolStripMenuItem();
            this.общийToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem15 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem16 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem17 = new System.Windows.Forms.ToolStripMenuItem();
            this.ячейкиToolStripMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
            this.общийToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem18 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem19 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem20 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuTextWrap = new System.Windows.Forms.ToolStripMenuItem();
            this.строкиToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.колонкиToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.ячейкиToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuTextMove = new System.Windows.Forms.ToolStripMenuItem();
            this.строкиToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.колонкиToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.ячейкиToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.textMoveCancel = new System.Windows.Forms.ToolStripMenuItem();
            this.строкиToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.колонкиToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ячейкиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFillCell = new System.Windows.Forms.ToolStripMenuItem();
            this.menuPhotoToCell = new System.Windows.Forms.ToolStripMenuItem();
            this.выводШрихкодаВЯчейкуToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuGerbToCell = new System.Windows.Forms.ToolStripMenuItem();
            this.menuWord = new System.Windows.Forms.ToolStripMenuItem();
            this.открытьWORDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.заменитьВWORDeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.соранитьWORDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.выводТаблицыНачинаяСToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.левыйВерхнийУголToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.левыйВерхнийУголСИтогамиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.documentMap1 = new FastColoredTextBoxNS.DocumentMap();
            this.cbStopExecution = new System.Windows.Forms.Button();
            this.bSave = new System.Windows.Forms.Button();
            this.bCursor = new System.Windows.Forms.Button();
            this.bStops = new System.Windows.Forms.Button();
            this.bStep = new System.Windows.Forms.Button();
            this.cbStartExecution = new System.Windows.Forms.Button();
            this.cbShowMap = new System.Windows.Forms.CheckBox();
            this.cbLineIndex = new System.Windows.Forms.CheckBox();
            this.statusStripMain = new System.Windows.Forms.StatusStrip();
            this.lbString = new System.Windows.Forms.ToolStripStatusLabel();
            this.lbColumn = new System.Windows.Forms.ToolStripStatusLabel();
            this.lbCompilerStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.файлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.сохранитьToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.выйтиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.редактироватьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.удалитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.копироватьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.вырезатьToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.вставитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.запускToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.запуститьToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.запуститьПошаговоToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.запуститьДоКурсораToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.запуститьПоМеткамToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.меткиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.добавиттьМеткуToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.удалитьМеткуToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.удалитьВсеМеткиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.перейтиКСледующейМеткеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.поискToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.найтиToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.найттиИЗаменитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.найтиДалееToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.dgVariables = new System.Windows.Forms.DataGridView();
            this.name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Value = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tbTextParam = new System.Windows.Forms.TextBox();
            this.cbArrays = new System.Windows.Forms.ComboBox();
            this.dgArray = new System.Windows.Forms.DataGridView();
            this.сохранитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.сохранитьToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.компиляторToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.запуститьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.пошаговоToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.игнорироватьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.доКурсораToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ttMain = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.MainSplitter)).BeginInit();
            this.MainSplitter.Panel1.SuspendLayout();
            this.MainSplitter.Panel2.SuspendLayout();
            this.MainSplitter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TopSplitter)).BeginInit();
            this.TopSplitter.Panel1.SuspendLayout();
            this.TopSplitter.Panel2.SuspendLayout();
            this.TopSplitter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbMain)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.statusStripMain.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgVariables)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgArray)).BeginInit();
            this.SuspendLayout();
            // 
            // MainSplitter
            // 
            this.MainSplitter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainSplitter.Location = new System.Drawing.Point(0, 0);
            this.MainSplitter.Name = "MainSplitter";
            this.MainSplitter.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // MainSplitter.Panel1
            // 
            this.MainSplitter.Panel1.Controls.Add(this.tbFind);
            this.MainSplitter.Panel1.Controls.Add(this.cbDebug);
            this.MainSplitter.Panel1.Controls.Add(this.TopSplitter);
            this.MainSplitter.Panel1.Controls.Add(this.cbStopExecution);
            this.MainSplitter.Panel1.Controls.Add(this.bSave);
            this.MainSplitter.Panel1.Controls.Add(this.bCursor);
            this.MainSplitter.Panel1.Controls.Add(this.bStops);
            this.MainSplitter.Panel1.Controls.Add(this.bStep);
            this.MainSplitter.Panel1.Controls.Add(this.cbStartExecution);
            this.MainSplitter.Panel1.Controls.Add(this.cbShowMap);
            this.MainSplitter.Panel1.Controls.Add(this.cbLineIndex);
            this.MainSplitter.Panel1.Controls.Add(this.statusStripMain);
            this.MainSplitter.Panel1.Controls.Add(this.menuStrip1);
            // 
            // MainSplitter.Panel2
            // 
            this.MainSplitter.Panel2.Controls.Add(this.splitContainer1);
            this.MainSplitter.Panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.MainSplitter_Panel2_Paint);
            this.MainSplitter.Panel2MinSize = 100;
            this.MainSplitter.Size = new System.Drawing.Size(1008, 688);
            this.MainSplitter.SplitterDistance = 550;
            this.MainSplitter.TabIndex = 2;
            this.MainSplitter.Paint += new System.Windows.Forms.PaintEventHandler(this.splitContainer1_Paint);
            // 
            // tbFind
            // 
            this.tbFind.Location = new System.Drawing.Point(589, 30);
            this.tbFind.Name = "tbFind";
            this.tbFind.Size = new System.Drawing.Size(112, 20);
            this.tbFind.TabIndex = 11;
            this.tbFind.Visible = false;
            this.tbFind.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tbFind_KeyUp);
            // 
            // cbDebug
            // 
            this.cbDebug.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDebug.FormattingEnabled = true;
            this.cbDebug.Location = new System.Drawing.Point(382, 29);
            this.cbDebug.Name = "cbDebug";
            this.cbDebug.Size = new System.Drawing.Size(201, 21);
            this.cbDebug.TabIndex = 10;
            this.cbDebug.Visible = false;
            this.cbDebug.SelectedIndexChanged += new System.EventHandler(this.cbDebug_SelectedIndexChanged);
            // 
            // TopSplitter
            // 
            this.TopSplitter.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TopSplitter.Location = new System.Drawing.Point(3, 55);
            this.TopSplitter.Name = "TopSplitter";
            // 
            // TopSplitter.Panel1
            // 
            this.TopSplitter.Panel1.Controls.Add(this.tbMain);
            // 
            // TopSplitter.Panel2
            // 
            this.TopSplitter.Panel2.Controls.Add(this.documentMap1);
            this.TopSplitter.Size = new System.Drawing.Size(1002, 470);
            this.TopSplitter.SplitterDistance = 828;
            this.TopSplitter.TabIndex = 9;
            // 
            // tbMain
            // 
            this.tbMain.AutoCompleteBracketsList = new char[] {
        '(',
        ')',
        '\"',
        '\"'};
            this.tbMain.AutoScrollMinSize = new System.Drawing.Size(130, 15);
            this.tbMain.BackBrush = null;
            this.tbMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbMain.CharHeight = 15;
            this.tbMain.CharWidth = 7;
            this.tbMain.ContextMenuStrip = this.contextMenuStrip1;
            this.tbMain.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbMain.DelayedEventsInterval = 200;
            this.tbMain.DelayedTextChangedInterval = 400;
            this.tbMain.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.tbMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbMain.Font = new System.Drawing.Font("Consolas", 9.75F);
            this.tbMain.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tbMain.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.tbMain.IsReplaceMode = false;
            this.tbMain.LeftPadding = 30;
            this.tbMain.Location = new System.Drawing.Point(0, 0);
            this.tbMain.Name = "tbMain";
            this.tbMain.Paddings = new System.Windows.Forms.Padding(0);
            this.tbMain.SelectionColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.tbMain.ShowFoldingLines = true;
            this.tbMain.ShowLineNumbers = false;
            this.tbMain.Size = new System.Drawing.Size(828, 470);
            this.tbMain.TabIndex = 1;
            this.tbMain.Text = "//пустая форма";
            this.tbMain.WordWrapAutoIndent = false;
            this.tbMain.Zoom = 100;
            this.tbMain.TextChanged += new System.EventHandler<FastColoredTextBoxNS.TextChangedEventArgs>(this.tbMain_TextChanged);
            this.tbMain.SelectionChanged += new System.EventHandler(this.tbMain_SelectionChanged);
            this.tbMain.TextChangedDelayed += new System.EventHandler<FastColoredTextBoxNS.TextChangedEventArgs>(this.tbMain_TextChangedDelayed);
            this.tbMain.SelectionChangedDelayed += new System.EventHandler(this.tbMain_SelectionChangedDelayed);
            this.tbMain.AutoIndentNeeded += new System.EventHandler<FastColoredTextBoxNS.AutoIndentEventArgs>(this.tbMain_AutoIndentNeeded);
            this.tbMain.PaintLine += new System.EventHandler<FastColoredTextBoxNS.PaintLineEventArgs>(this.tbMain_PaintLine);
            this.tbMain.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbMain_KeyDown);
            this.tbMain.MouseDown += new System.Windows.Forms.MouseEventHandler(this.tbMain_MouseDown);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuCopy,
            this.menuCut,
            this.menuPaste,
            this.menuMacroSplit,
            this.menuMacro,
            this.menuMacroForm,
            this.toolStripSeparator1,
            this.menuKomandKonstr,
            this.menuFunkc,
            this.menuEXCEL,
            this.menuShirina,
            this.menuVisota,
            this.menuTextToCell,
            this.menuFont,
            this.menuMerge,
            this.menuInCell,
            this.menuShrink,
            this.menuLine,
            this.menuFormat,
            this.menuTextWrap,
            this.menuTextMove,
            this.textMoveCancel,
            this.menuFillCell,
            this.menuPhotoToCell,
            this.выводШрихкодаВЯчейкуToolStripMenuItem,
            this.menuGerbToCell,
            this.menuWord,
            this.выводТаблицыНачинаяСToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(218, 588);
            // 
            // menuCopy
            // 
            this.menuCopy.Name = "menuCopy";
            this.menuCopy.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.menuCopy.Size = new System.Drawing.Size(217, 22);
            this.menuCopy.Text = "Копировать";
            this.menuCopy.Click += new System.EventHandler(this.menuCopy_Click);
            // 
            // menuCut
            // 
            this.menuCut.Name = "menuCut";
            this.menuCut.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.menuCut.Size = new System.Drawing.Size(217, 22);
            this.menuCut.Text = "Вырезать";
            this.menuCut.Click += new System.EventHandler(this.menuCut_Click);
            // 
            // menuPaste
            // 
            this.menuPaste.Name = "menuPaste";
            this.menuPaste.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.menuPaste.Size = new System.Drawing.Size(217, 22);
            this.menuPaste.Text = "Вставить";
            this.menuPaste.Click += new System.EventHandler(this.menuPaste_Click);
            // 
            // menuMacroSplit
            // 
            this.menuMacroSplit.Name = "menuMacroSplit";
            this.menuMacroSplit.Size = new System.Drawing.Size(214, 6);
            // 
            // menuMacro
            // 
            this.menuMacro.Name = "menuMacro";
            this.menuMacro.Size = new System.Drawing.Size(217, 22);
            this.menuMacro.Text = "Общие макросы";
            // 
            // menuMacroForm
            // 
            this.menuMacroForm.Name = "menuMacroForm";
            this.menuMacroForm.Size = new System.Drawing.Size(217, 22);
            this.menuMacroForm.Text = "Макросы формы";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(214, 6);
            // 
            // menuKomandKonstr
            // 
            this.menuKomandKonstr.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.пToolStripMenuItem,
            this.переченьПеременныхToolStripMenuItem,
            this.еслиИначеКонецToolStripMenuItem,
            this.циклКонецToolStripMenuItem,
            this.покаКонецToolStripMenuItem,
            this.очиститьМассивToolStripMenuItem,
            this.запросSELECTFROMToolStripMenuItem,
            this.параметрЗапросаToolStripMenuItem,
            this.открытьЗапроскудаToolStripMenuItem,
            this.сортироватьToolStripMenuItem});
            this.menuKomandKonstr.Name = "menuKomandKonstr";
            this.menuKomandKonstr.Size = new System.Drawing.Size(217, 22);
            this.menuKomandKonstr.Text = "Команды и конструкции";
            // 
            // пToolStripMenuItem
            // 
            this.пToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.числоToolStripMenuItem,
            this.символToolStripMenuItem});
            this.пToolStripMenuItem.Name = "пToolStripMenuItem";
            this.пToolStripMenuItem.Size = new System.Drawing.Size(245, 22);
            this.пToolStripMenuItem.Text = "Перечень массивов";
            // 
            // числоToolStripMenuItem
            // 
            this.числоToolStripMenuItem.Name = "числоToolStripMenuItem";
            this.числоToolStripMenuItem.Size = new System.Drawing.Size(115, 22);
            this.числоToolStripMenuItem.Text = "Число";
            this.числоToolStripMenuItem.Click += new System.EventHandler(this.числоToolStripMenuItem_Click);
            // 
            // символToolStripMenuItem
            // 
            this.символToolStripMenuItem.Name = "символToolStripMenuItem";
            this.символToolStripMenuItem.Size = new System.Drawing.Size(115, 22);
            this.символToolStripMenuItem.Text = "Символ";
            this.символToolStripMenuItem.Click += new System.EventHandler(this.символToolStripMenuItem_Click);
            // 
            // переченьПеременныхToolStripMenuItem
            // 
            this.переченьПеременныхToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.числоToolStripMenuItem1,
            this.датаToolStripMenuItem,
            this.датаToolStripMenuItem1});
            this.переченьПеременныхToolStripMenuItem.Name = "переченьПеременныхToolStripMenuItem";
            this.переченьПеременныхToolStripMenuItem.Size = new System.Drawing.Size(245, 22);
            this.переченьПеременныхToolStripMenuItem.Text = "Перечень переменных";
            // 
            // числоToolStripMenuItem1
            // 
            this.числоToolStripMenuItem1.Name = "числоToolStripMenuItem1";
            this.числоToolStripMenuItem1.Size = new System.Drawing.Size(115, 22);
            this.числоToolStripMenuItem1.Text = "Число";
            this.числоToolStripMenuItem1.Click += new System.EventHandler(this.числоToolStripMenuItem1_Click);
            // 
            // датаToolStripMenuItem
            // 
            this.датаToolStripMenuItem.Name = "датаToolStripMenuItem";
            this.датаToolStripMenuItem.Size = new System.Drawing.Size(115, 22);
            this.датаToolStripMenuItem.Text = "Символ";
            this.датаToolStripMenuItem.Click += new System.EventHandler(this.датаToolStripMenuItem_Click);
            // 
            // датаToolStripMenuItem1
            // 
            this.датаToolStripMenuItem1.Name = "датаToolStripMenuItem1";
            this.датаToolStripMenuItem1.Size = new System.Drawing.Size(115, 22);
            this.датаToolStripMenuItem1.Text = "Дата";
            this.датаToolStripMenuItem1.Click += new System.EventHandler(this.датаToolStripMenuItem1_Click);
            // 
            // еслиИначеКонецToolStripMenuItem
            // 
            this.еслиИначеКонецToolStripMenuItem.Name = "еслиИначеКонецToolStripMenuItem";
            this.еслиИначеКонецToolStripMenuItem.Size = new System.Drawing.Size(245, 22);
            this.еслиИначеКонецToolStripMenuItem.Text = "если (условие) ... иначе ... конец";
            this.еслиИначеКонецToolStripMenuItem.Click += new System.EventHandler(this.еслиИначеКонецToolStripMenuItem_Click);
            // 
            // циклКонецToolStripMenuItem
            // 
            this.циклКонецToolStripMenuItem.Name = "циклКонецToolStripMenuItem";
            this.циклКонецToolStripMenuItem.Size = new System.Drawing.Size(245, 22);
            this.циклКонецToolStripMenuItem.Text = "цикл ... конец";
            this.циклКонецToolStripMenuItem.Click += new System.EventHandler(this.циклКонецToolStripMenuItem_Click);
            // 
            // покаКонецToolStripMenuItem
            // 
            this.покаКонецToolStripMenuItem.Name = "покаКонецToolStripMenuItem";
            this.покаКонецToolStripMenuItem.Size = new System.Drawing.Size(245, 22);
            this.покаКонецToolStripMenuItem.Text = "пока ... конец";
            this.покаКонецToolStripMenuItem.Click += new System.EventHandler(this.покаКонецToolStripMenuItem_Click);
            // 
            // очиститьМассивToolStripMenuItem
            // 
            this.очиститьМассивToolStripMenuItem.Name = "очиститьМассивToolStripMenuItem";
            this.очиститьМассивToolStripMenuItem.Size = new System.Drawing.Size(245, 22);
            this.очиститьМассивToolStripMenuItem.Text = "Очистить массив";
            this.очиститьМассивToolStripMenuItem.Click += new System.EventHandler(this.очиститьМассивToolStripMenuItem_Click);
            // 
            // запросSELECTFROMToolStripMenuItem
            // 
            this.запросSELECTFROMToolStripMenuItem.Name = "запросSELECTFROMToolStripMenuItem";
            this.запросSELECTFROMToolStripMenuItem.Size = new System.Drawing.Size(245, 22);
            this.запросSELECTFROMToolStripMenuItem.Text = "Запрос(SELECT ... FROM ...)";
            this.запросSELECTFROMToolStripMenuItem.Click += new System.EventHandler(this.запросSELECTFROMToolStripMenuItem_Click);
            // 
            // параметрЗапросаToolStripMenuItem
            // 
            this.параметрЗапросаToolStripMenuItem.Name = "параметрЗапросаToolStripMenuItem";
            this.параметрЗапросаToolStripMenuItem.Size = new System.Drawing.Size(245, 22);
            this.параметрЗапросаToolStripMenuItem.Text = "Параметр запроса";
            this.параметрЗапросаToolStripMenuItem.Click += new System.EventHandler(this.параметрЗапросаToolStripMenuItem_Click);
            // 
            // открытьЗапроскудаToolStripMenuItem
            // 
            this.открытьЗапроскудаToolStripMenuItem.Name = "открытьЗапроскудаToolStripMenuItem";
            this.открытьЗапроскудаToolStripMenuItem.Size = new System.Drawing.Size(245, 22);
            this.открытьЗапроскудаToolStripMenuItem.Text = "Открыть запрос(куда)";
            this.открытьЗапроскудаToolStripMenuItem.Click += new System.EventHandler(this.открытьЗапроскудаToolStripMenuItem_Click);
            // 
            // сортироватьToolStripMenuItem
            // 
            this.сортироватьToolStripMenuItem.Name = "сортироватьToolStripMenuItem";
            this.сортироватьToolStripMenuItem.Size = new System.Drawing.Size(245, 22);
            this.сортироватьToolStripMenuItem.Text = "Сортировать";
            this.сортироватьToolStripMenuItem.Click += new System.EventHandler(this.сортироватьToolStripMenuItem_Click);
            // 
            // menuFunkc
            // 
            this.menuFunkc.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.строкмассивКоличествоСтрокВМассивеToolStripMenuItem,
            this.столбцовмассивКоличествоСтолбцовВМассивеToolStripMenuItem,
            this.удалениеПробеловСправаToolStripMenuItem,
            this.удалениеПробеловСлеваToolStripMenuItem,
            this.удалениеПробеловСправаИСлеваToolStripMenuItem,
            this.округлитьToolStripMenuItem,
            this.найтиToolStripMenuItem,
            this.вырезатьToolStripMenuItem,
            this.длиннаToolStripMenuItem,
            this.количествоДнейМеждуДатамиToolStripMenuItem,
            this.строкаВВидеСтрокаToolStripMenuItem,
            this.строкаВВидеСТРОКАToolStripMenuItem1,
            this.строкаВВидестрокаToolStripMenuItem2,
            this.датаToolStripMenuItem2,
            this.датаПрописьюToolStripMenuItem,
            this.сToolStripMenuItem,
            this.деньгиПрописьюToolStripMenuItem});
            this.menuFunkc.Name = "menuFunkc";
            this.menuFunkc.Size = new System.Drawing.Size(217, 22);
            this.menuFunkc.Text = "Функции";
            // 
            // строкмассивКоличествоСтрокВМассивеToolStripMenuItem
            // 
            this.строкмассивКоличествоСтрокВМассивеToolStripMenuItem.Name = "строкмассивКоличествоСтрокВМассивеToolStripMenuItem";
            this.строкмассивКоличествоСтрокВМассивеToolStripMenuItem.Size = new System.Drawing.Size(349, 22);
            this.строкмассивКоличествоСтрокВМассивеToolStripMenuItem.Text = "Строк(массив) - количество строк в массиве";
            this.строкмассивКоличествоСтрокВМассивеToolStripMenuItem.Click += new System.EventHandler(this.строкмассивКоличествоСтрокВМассивеToolStripMenuItem_Click);
            // 
            // столбцовмассивКоличествоСтолбцовВМассивеToolStripMenuItem
            // 
            this.столбцовмассивКоличествоСтолбцовВМассивеToolStripMenuItem.Name = "столбцовмассивКоличествоСтолбцовВМассивеToolStripMenuItem";
            this.столбцовмассивКоличествоСтолбцовВМассивеToolStripMenuItem.Size = new System.Drawing.Size(349, 22);
            this.столбцовмассивКоличествоСтолбцовВМассивеToolStripMenuItem.Text = "Столбцов(массив) - количество столбцов в массиве";
            this.столбцовмассивКоличествоСтолбцовВМассивеToolStripMenuItem.Click += new System.EventHandler(this.столбцовмассивКоличествоСтолбцовВМассивеToolStripMenuItem_Click);
            // 
            // удалениеПробеловСправаToolStripMenuItem
            // 
            this.удалениеПробеловСправаToolStripMenuItem.Name = "удалениеПробеловСправаToolStripMenuItem";
            this.удалениеПробеловСправаToolStripMenuItem.Size = new System.Drawing.Size(349, 22);
            this.удалениеПробеловСправаToolStripMenuItem.Text = "Удаление пробелов справа";
            this.удалениеПробеловСправаToolStripMenuItem.Click += new System.EventHandler(this.удалениеПробеловСправаToolStripMenuItem_Click);
            // 
            // удалениеПробеловСлеваToolStripMenuItem
            // 
            this.удалениеПробеловСлеваToolStripMenuItem.Name = "удалениеПробеловСлеваToolStripMenuItem";
            this.удалениеПробеловСлеваToolStripMenuItem.Size = new System.Drawing.Size(349, 22);
            this.удалениеПробеловСлеваToolStripMenuItem.Text = "Удаление пробелов слева";
            this.удалениеПробеловСлеваToolStripMenuItem.Click += new System.EventHandler(this.удалениеПробеловСлеваToolStripMenuItem_Click);
            // 
            // удалениеПробеловСправаИСлеваToolStripMenuItem
            // 
            this.удалениеПробеловСправаИСлеваToolStripMenuItem.Name = "удалениеПробеловСправаИСлеваToolStripMenuItem";
            this.удалениеПробеловСправаИСлеваToolStripMenuItem.Size = new System.Drawing.Size(349, 22);
            this.удалениеПробеловСправаИСлеваToolStripMenuItem.Text = "Удаление пробелов справа и слева";
            this.удалениеПробеловСправаИСлеваToolStripMenuItem.Click += new System.EventHandler(this.удалениеПробеловСправаИСлеваToolStripMenuItem_Click);
            // 
            // округлитьToolStripMenuItem
            // 
            this.округлитьToolStripMenuItem.Name = "округлитьToolStripMenuItem";
            this.округлитьToolStripMenuItem.Size = new System.Drawing.Size(349, 22);
            this.округлитьToolStripMenuItem.Text = "Округлить";
            this.округлитьToolStripMenuItem.Click += new System.EventHandler(this.округлитьToolStripMenuItem_Click);
            // 
            // найтиToolStripMenuItem
            // 
            this.найтиToolStripMenuItem.Name = "найтиToolStripMenuItem";
            this.найтиToolStripMenuItem.Size = new System.Drawing.Size(349, 22);
            this.найтиToolStripMenuItem.Text = "Найти";
            this.найтиToolStripMenuItem.Click += new System.EventHandler(this.найтиToolStripMenuItem_Click);
            // 
            // вырезатьToolStripMenuItem
            // 
            this.вырезатьToolStripMenuItem.Name = "вырезатьToolStripMenuItem";
            this.вырезатьToolStripMenuItem.Size = new System.Drawing.Size(349, 22);
            this.вырезатьToolStripMenuItem.Text = "Вырезать";
            this.вырезатьToolStripMenuItem.Click += new System.EventHandler(this.вырезатьToolStripMenuItem_Click);
            // 
            // длиннаToolStripMenuItem
            // 
            this.длиннаToolStripMenuItem.Name = "длиннаToolStripMenuItem";
            this.длиннаToolStripMenuItem.Size = new System.Drawing.Size(349, 22);
            this.длиннаToolStripMenuItem.Text = "Длина";
            this.длиннаToolStripMenuItem.Click += new System.EventHandler(this.длиннаToolStripMenuItem_Click);
            // 
            // количествоДнейМеждуДатамиToolStripMenuItem
            // 
            this.количествоДнейМеждуДатамиToolStripMenuItem.Name = "количествоДнейМеждуДатамиToolStripMenuItem";
            this.количествоДнейМеждуДатамиToolStripMenuItem.Size = new System.Drawing.Size(349, 22);
            this.количествоДнейМеждуДатамиToolStripMenuItem.Text = "Количество дней между датами";
            this.количествоДнейМеждуДатамиToolStripMenuItem.Click += new System.EventHandler(this.количествоДнейМеждуДатамиToolStripMenuItem_Click);
            // 
            // строкаВВидеСтрокаToolStripMenuItem
            // 
            this.строкаВВидеСтрокаToolStripMenuItem.Name = "строкаВВидеСтрокаToolStripMenuItem";
            this.строкаВВидеСтрокаToolStripMenuItem.Size = new System.Drawing.Size(349, 22);
            this.строкаВВидеСтрокаToolStripMenuItem.Text = "Строка в виде \'Строка\'";
            this.строкаВВидеСтрокаToolStripMenuItem.Click += new System.EventHandler(this.строкаВВидеСтрокаToolStripMenuItem_Click);
            // 
            // строкаВВидеСТРОКАToolStripMenuItem1
            // 
            this.строкаВВидеСТРОКАToolStripMenuItem1.Name = "строкаВВидеСТРОКАToolStripMenuItem1";
            this.строкаВВидеСТРОКАToolStripMenuItem1.Size = new System.Drawing.Size(349, 22);
            this.строкаВВидеСТРОКАToolStripMenuItem1.Text = "Строка в виде \'СТРОКА\'";
            this.строкаВВидеСТРОКАToolStripMenuItem1.Click += new System.EventHandler(this.строкаВВидеСТРОКАToolStripMenuItem1_Click);
            // 
            // строкаВВидестрокаToolStripMenuItem2
            // 
            this.строкаВВидестрокаToolStripMenuItem2.Name = "строкаВВидестрокаToolStripMenuItem2";
            this.строкаВВидестрокаToolStripMenuItem2.Size = new System.Drawing.Size(349, 22);
            this.строкаВВидестрокаToolStripMenuItem2.Text = "Строка в виде \'строка\'";
            this.строкаВВидестрокаToolStripMenuItem2.Click += new System.EventHandler(this.строкаВВидестрокаToolStripMenuItem2_Click);
            // 
            // датаToolStripMenuItem2
            // 
            this.датаToolStripMenuItem2.Name = "датаToolStripMenuItem2";
            this.датаToolStripMenuItem2.Size = new System.Drawing.Size(349, 22);
            this.датаToolStripMenuItem2.Text = "Дата";
            this.датаToolStripMenuItem2.Click += new System.EventHandler(this.датаToolStripMenuItem2_Click);
            // 
            // датаПрописьюToolStripMenuItem
            // 
            this.датаПрописьюToolStripMenuItem.Name = "датаПрописьюToolStripMenuItem";
            this.датаПрописьюToolStripMenuItem.Size = new System.Drawing.Size(349, 22);
            this.датаПрописьюToolStripMenuItem.Text = "Дата прописью";
            this.датаПрописьюToolStripMenuItem.Click += new System.EventHandler(this.датаПрописьюToolStripMenuItem_Click);
            // 
            // сToolStripMenuItem
            // 
            this.сToolStripMenuItem.Name = "сToolStripMenuItem";
            this.сToolStripMenuItem.Size = new System.Drawing.Size(349, 22);
            this.сToolStripMenuItem.Text = "С новой строки";
            this.сToolStripMenuItem.Click += new System.EventHandler(this.сToolStripMenuItem_Click);
            // 
            // деньгиПрописьюToolStripMenuItem
            // 
            this.деньгиПрописьюToolStripMenuItem.Name = "деньгиПрописьюToolStripMenuItem";
            this.деньгиПрописьюToolStripMenuItem.Size = new System.Drawing.Size(349, 22);
            this.деньгиПрописьюToolStripMenuItem.Text = "Деньги прописью";
            this.деньгиПрописьюToolStripMenuItem.Click += new System.EventHandler(this.деньгиПрописьюToolStripMenuItem_Click);
            // 
            // menuEXCEL
            // 
            this.menuEXCEL.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.создатьEXCELToolStripMenuItem,
            this.создатьOPENToolStripMenuItem,
            this.открытьEXCELToolStripMenuItem,
            this.добавитьЛистовToolStripMenuItem,
            this.названиеЛистаToolStripMenuItem,
            this.текущийЛистToolStripMenuItem,
            this.поляЛистаToolStripMenuItem,
            this.ориентацияЛистаToolStripMenuItem,
            this.сохранитьEXCELToolStripMenuItem,
            this.работаEXCELToolStripMenuItem,
            this.форматA3ToolStripMenuItem,
            this.форматА4ToolStripMenuItem,
            this.форматА5ToolStripMenuItem,
            this.заголовокСтраницToolStripMenuItem,
            this.верхнийКолонтитулToolStripMenuItem,
            this.нижнийКолонтитутToolStripMenuItem,
            this.центрироватьНаСтраницеToolStripMenuItem,
            this.разрывСтраницыToolStripMenuItem,
            this.добавитьСтрокуEXCELToolStripMenuItem,
            this.скрытьСтолбецToolStripMenuItem,
            this.скрытьСтрокуToolStripMenuItem,
            this.процедураОбьединенияToolStripMenuItem});
            this.menuEXCEL.Name = "menuEXCEL";
            this.menuEXCEL.Size = new System.Drawing.Size(217, 22);
            this.menuEXCEL.Text = "EXCEL";
            // 
            // создатьEXCELToolStripMenuItem
            // 
            this.создатьEXCELToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.книгаToolStripMenuItem,
            this.альбомToolStripMenuItem});
            this.создатьEXCELToolStripMenuItem.Name = "создатьEXCELToolStripMenuItem";
            this.создатьEXCELToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.создатьEXCELToolStripMenuItem.Text = "Создать EXCEL";
            // 
            // книгаToolStripMenuItem
            // 
            this.книгаToolStripMenuItem.Name = "книгаToolStripMenuItem";
            this.книгаToolStripMenuItem.Size = new System.Drawing.Size(115, 22);
            this.книгаToolStripMenuItem.Text = "Книга";
            this.книгаToolStripMenuItem.Click += new System.EventHandler(this.книгаToolStripMenuItem_Click);
            // 
            // альбомToolStripMenuItem
            // 
            this.альбомToolStripMenuItem.Name = "альбомToolStripMenuItem";
            this.альбомToolStripMenuItem.Size = new System.Drawing.Size(115, 22);
            this.альбомToolStripMenuItem.Text = "Альбом";
            this.альбомToolStripMenuItem.Click += new System.EventHandler(this.альбомToolStripMenuItem_Click);
            // 
            // создатьOPENToolStripMenuItem
            // 
            this.создатьOPENToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.книгаToolStripMenuItem1,
            this.альбомToolStripMenuItem1});
            this.создатьOPENToolStripMenuItem.Name = "создатьOPENToolStripMenuItem";
            this.создатьOPENToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.создатьOPENToolStripMenuItem.Text = "Создать OPEN";
            // 
            // книгаToolStripMenuItem1
            // 
            this.книгаToolStripMenuItem1.Name = "книгаToolStripMenuItem1";
            this.книгаToolStripMenuItem1.Size = new System.Drawing.Size(115, 22);
            this.книгаToolStripMenuItem1.Text = "Книга";
            this.книгаToolStripMenuItem1.Click += new System.EventHandler(this.книгаToolStripMenuItem1_Click);
            // 
            // альбомToolStripMenuItem1
            // 
            this.альбомToolStripMenuItem1.Name = "альбомToolStripMenuItem1";
            this.альбомToolStripMenuItem1.Size = new System.Drawing.Size(115, 22);
            this.альбомToolStripMenuItem1.Text = "Альбом";
            this.альбомToolStripMenuItem1.Click += new System.EventHandler(this.альбомToolStripMenuItem1_Click);
            // 
            // открытьEXCELToolStripMenuItem
            // 
            this.открытьEXCELToolStripMenuItem.Name = "открытьEXCELToolStripMenuItem";
            this.открытьEXCELToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.открытьEXCELToolStripMenuItem.Text = "Открыть EXCEL";
            this.открытьEXCELToolStripMenuItem.Click += new System.EventHandler(this.открытьEXCELToolStripMenuItem_Click);
            // 
            // добавитьЛистовToolStripMenuItem
            // 
            this.добавитьЛистовToolStripMenuItem.Name = "добавитьЛистовToolStripMenuItem";
            this.добавитьЛистовToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.добавитьЛистовToolStripMenuItem.Text = "Добавить листов";
            this.добавитьЛистовToolStripMenuItem.Click += new System.EventHandler(this.добавитьЛистовToolStripMenuItem_Click);
            // 
            // названиеЛистаToolStripMenuItem
            // 
            this.названиеЛистаToolStripMenuItem.Name = "названиеЛистаToolStripMenuItem";
            this.названиеЛистаToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.названиеЛистаToolStripMenuItem.Text = "Название листа";
            this.названиеЛистаToolStripMenuItem.Click += new System.EventHandler(this.названиеЛистаToolStripMenuItem_Click);
            // 
            // текущийЛистToolStripMenuItem
            // 
            this.текущийЛистToolStripMenuItem.Name = "текущийЛистToolStripMenuItem";
            this.текущийЛистToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.текущийЛистToolStripMenuItem.Text = "Текущий лист";
            this.текущийЛистToolStripMenuItem.Click += new System.EventHandler(this.текущийЛистToolStripMenuItem_Click);
            // 
            // поляЛистаToolStripMenuItem
            // 
            this.поляЛистаToolStripMenuItem.Name = "поляЛистаToolStripMenuItem";
            this.поляЛистаToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.поляЛистаToolStripMenuItem.Text = "Поля листа";
            this.поляЛистаToolStripMenuItem.Click += new System.EventHandler(this.поляЛистаToolStripMenuItem_Click);
            // 
            // ориентацияЛистаToolStripMenuItem
            // 
            this.ориентацияЛистаToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.книжнаяToolStripMenuItem,
            this.альбомнаяToolStripMenuItem});
            this.ориентацияЛистаToolStripMenuItem.Name = "ориентацияЛистаToolStripMenuItem";
            this.ориентацияЛистаToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.ориентацияЛистаToolStripMenuItem.Text = "Ориентация листа";
            // 
            // книжнаяToolStripMenuItem
            // 
            this.книжнаяToolStripMenuItem.Name = "книжнаяToolStripMenuItem";
            this.книжнаяToolStripMenuItem.Size = new System.Drawing.Size(134, 22);
            this.книжнаяToolStripMenuItem.Text = "Книжная";
            this.книжнаяToolStripMenuItem.Click += new System.EventHandler(this.книжнаяToolStripMenuItem_Click);
            // 
            // альбомнаяToolStripMenuItem
            // 
            this.альбомнаяToolStripMenuItem.Name = "альбомнаяToolStripMenuItem";
            this.альбомнаяToolStripMenuItem.Size = new System.Drawing.Size(134, 22);
            this.альбомнаяToolStripMenuItem.Text = "Альбомная";
            this.альбомнаяToolStripMenuItem.Click += new System.EventHandler(this.альбомнаяToolStripMenuItem_Click);
            // 
            // сохранитьEXCELToolStripMenuItem
            // 
            this.сохранитьEXCELToolStripMenuItem.Name = "сохранитьEXCELToolStripMenuItem";
            this.сохранитьEXCELToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.сохранитьEXCELToolStripMenuItem.Text = "Сохранить EXCEL";
            this.сохранитьEXCELToolStripMenuItem.Click += new System.EventHandler(this.сохранитьEXCELToolStripMenuItem_Click);
            // 
            // работаEXCELToolStripMenuItem
            // 
            this.работаEXCELToolStripMenuItem.Name = "работаEXCELToolStripMenuItem";
            this.работаEXCELToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.работаEXCELToolStripMenuItem.Text = "Работа EXCEL";
            this.работаEXCELToolStripMenuItem.Click += new System.EventHandler(this.работаEXCELToolStripMenuItem_Click);
            // 
            // форматA3ToolStripMenuItem
            // 
            this.форматA3ToolStripMenuItem.Name = "форматA3ToolStripMenuItem";
            this.форматA3ToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.форматA3ToolStripMenuItem.Text = "Формат A3";
            this.форматA3ToolStripMenuItem.Click += new System.EventHandler(this.форматA3ToolStripMenuItem_Click);
            // 
            // форматА4ToolStripMenuItem
            // 
            this.форматА4ToolStripMenuItem.Name = "форматА4ToolStripMenuItem";
            this.форматА4ToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.форматА4ToolStripMenuItem.Text = "Формат А4";
            this.форматА4ToolStripMenuItem.Click += new System.EventHandler(this.форматА4ToolStripMenuItem_Click);
            // 
            // форматА5ToolStripMenuItem
            // 
            this.форматА5ToolStripMenuItem.Name = "форматА5ToolStripMenuItem";
            this.форматА5ToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.форматА5ToolStripMenuItem.Text = "Формат А5";
            this.форматА5ToolStripMenuItem.Click += new System.EventHandler(this.форматА5ToolStripMenuItem_Click);
            // 
            // заголовокСтраницToolStripMenuItem
            // 
            this.заголовокСтраницToolStripMenuItem.Name = "заголовокСтраницToolStripMenuItem";
            this.заголовокСтраницToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.заголовокСтраницToolStripMenuItem.Text = "Заголовок страниц";
            this.заголовокСтраницToolStripMenuItem.Click += new System.EventHandler(this.заголовокСтраницToolStripMenuItem_Click);
            // 
            // верхнийКолонтитулToolStripMenuItem
            // 
            this.верхнийКолонтитулToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.слеваToolStripMenuItem,
            this.поЦентруToolStripMenuItem,
            this.справаToolStripMenuItem});
            this.верхнийКолонтитулToolStripMenuItem.Name = "верхнийКолонтитулToolStripMenuItem";
            this.верхнийКолонтитулToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.верхнийКолонтитулToolStripMenuItem.Text = "Верхний колонтитул";
            // 
            // слеваToolStripMenuItem
            // 
            this.слеваToolStripMenuItem.Name = "слеваToolStripMenuItem";
            this.слеваToolStripMenuItem.Size = new System.Drawing.Size(129, 22);
            this.слеваToolStripMenuItem.Text = "Слева";
            this.слеваToolStripMenuItem.Click += new System.EventHandler(this.слеваToolStripMenuItem_Click);
            // 
            // поЦентруToolStripMenuItem
            // 
            this.поЦентруToolStripMenuItem.Name = "поЦентруToolStripMenuItem";
            this.поЦентруToolStripMenuItem.Size = new System.Drawing.Size(129, 22);
            this.поЦентруToolStripMenuItem.Text = "По центру";
            this.поЦентруToolStripMenuItem.Click += new System.EventHandler(this.поЦентруToolStripMenuItem_Click);
            // 
            // справаToolStripMenuItem
            // 
            this.справаToolStripMenuItem.Name = "справаToolStripMenuItem";
            this.справаToolStripMenuItem.Size = new System.Drawing.Size(129, 22);
            this.справаToolStripMenuItem.Text = "Справа";
            this.справаToolStripMenuItem.Click += new System.EventHandler(this.справаToolStripMenuItem_Click);
            // 
            // нижнийКолонтитутToolStripMenuItem
            // 
            this.нижнийКолонтитутToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.слеваToolStripMenuItem1,
            this.поЦентруToolStripMenuItem1,
            this.справаToolStripMenuItem1});
            this.нижнийКолонтитутToolStripMenuItem.Name = "нижнийКолонтитутToolStripMenuItem";
            this.нижнийКолонтитутToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.нижнийКолонтитутToolStripMenuItem.Text = "Нижний колонтитут";
            // 
            // слеваToolStripMenuItem1
            // 
            this.слеваToolStripMenuItem1.Name = "слеваToolStripMenuItem1";
            this.слеваToolStripMenuItem1.Size = new System.Drawing.Size(129, 22);
            this.слеваToolStripMenuItem1.Text = "Слева";
            this.слеваToolStripMenuItem1.Click += new System.EventHandler(this.слеваToolStripMenuItem1_Click);
            // 
            // поЦентруToolStripMenuItem1
            // 
            this.поЦентруToolStripMenuItem1.Name = "поЦентруToolStripMenuItem1";
            this.поЦентруToolStripMenuItem1.Size = new System.Drawing.Size(129, 22);
            this.поЦентруToolStripMenuItem1.Text = "По центру";
            this.поЦентруToolStripMenuItem1.Click += new System.EventHandler(this.поЦентруToolStripMenuItem1_Click);
            // 
            // справаToolStripMenuItem1
            // 
            this.справаToolStripMenuItem1.Name = "справаToolStripMenuItem1";
            this.справаToolStripMenuItem1.Size = new System.Drawing.Size(129, 22);
            this.справаToolStripMenuItem1.Text = "Справа";
            this.справаToolStripMenuItem1.Click += new System.EventHandler(this.справаToolStripMenuItem1_Click);
            // 
            // центрироватьНаСтраницеToolStripMenuItem
            // 
            this.центрироватьНаСтраницеToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.горизонтальноToolStripMenuItem,
            this.вертикальноToolStripMenuItem});
            this.центрироватьНаСтраницеToolStripMenuItem.Name = "центрироватьНаСтраницеToolStripMenuItem";
            this.центрироватьНаСтраницеToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.центрироватьНаСтраницеToolStripMenuItem.Text = "Центрировать на странице";
            // 
            // горизонтальноToolStripMenuItem
            // 
            this.горизонтальноToolStripMenuItem.Name = "горизонтальноToolStripMenuItem";
            this.горизонтальноToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.горизонтальноToolStripMenuItem.Text = "Горизонтально";
            this.горизонтальноToolStripMenuItem.Click += new System.EventHandler(this.горизонтальноToolStripMenuItem_Click);
            // 
            // вертикальноToolStripMenuItem
            // 
            this.вертикальноToolStripMenuItem.Name = "вертикальноToolStripMenuItem";
            this.вертикальноToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.вертикальноToolStripMenuItem.Text = "Вертикально";
            this.вертикальноToolStripMenuItem.Click += new System.EventHandler(this.вертикальноToolStripMenuItem_Click);
            // 
            // разрывСтраницыToolStripMenuItem
            // 
            this.разрывСтраницыToolStripMenuItem.Name = "разрывСтраницыToolStripMenuItem";
            this.разрывСтраницыToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.разрывСтраницыToolStripMenuItem.Text = "Разрыв страницы";
            this.разрывСтраницыToolStripMenuItem.Click += new System.EventHandler(this.разрывСтраницыToolStripMenuItem_Click);
            // 
            // добавитьСтрокуEXCELToolStripMenuItem
            // 
            this.добавитьСтрокуEXCELToolStripMenuItem.Name = "добавитьСтрокуEXCELToolStripMenuItem";
            this.добавитьСтрокуEXCELToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.добавитьСтрокуEXCELToolStripMenuItem.Text = "Добавить строку EXCEL";
            this.добавитьСтрокуEXCELToolStripMenuItem.Click += new System.EventHandler(this.добавитьСтрокуEXCELToolStripMenuItem_Click);
            // 
            // скрытьСтолбецToolStripMenuItem
            // 
            this.скрытьСтолбецToolStripMenuItem.Name = "скрытьСтолбецToolStripMenuItem";
            this.скрытьСтолбецToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.скрытьСтолбецToolStripMenuItem.Text = "Скрыть столбец";
            this.скрытьСтолбецToolStripMenuItem.Click += new System.EventHandler(this.скрытьСтолбецToolStripMenuItem_Click);
            // 
            // скрытьСтрокуToolStripMenuItem
            // 
            this.скрытьСтрокуToolStripMenuItem.Name = "скрытьСтрокуToolStripMenuItem";
            this.скрытьСтрокуToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.скрытьСтрокуToolStripMenuItem.Text = "Скрыть строку";
            this.скрытьСтрокуToolStripMenuItem.Click += new System.EventHandler(this.скрытьСтрокуToolStripMenuItem_Click);
            // 
            // процедураОбьединенияToolStripMenuItem
            // 
            this.процедураОбьединенияToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.пToolStripMenuItem1,
            this.столбцыДляСравненияToolStripMenuItem,
            this.выполнитьОбьединениеToolStripMenuItem});
            this.процедураОбьединенияToolStripMenuItem.Name = "процедураОбьединенияToolStripMenuItem";
            this.процедураОбьединенияToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.процедураОбьединенияToolStripMenuItem.Text = "Процедура обьединения";
            // 
            // пToolStripMenuItem1
            // 
            this.пToolStripMenuItem1.Name = "пToolStripMenuItem1";
            this.пToolStripMenuItem1.Size = new System.Drawing.Size(217, 22);
            this.пToolStripMenuItem1.Text = "Столбцы для обьединения";
            this.пToolStripMenuItem1.Click += new System.EventHandler(this.пToolStripMenuItem1_Click);
            // 
            // столбцыДляСравненияToolStripMenuItem
            // 
            this.столбцыДляСравненияToolStripMenuItem.Name = "столбцыДляСравненияToolStripMenuItem";
            this.столбцыДляСравненияToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
            this.столбцыДляСравненияToolStripMenuItem.Text = "Столбцы для сравнения";
            this.столбцыДляСравненияToolStripMenuItem.Click += new System.EventHandler(this.столбцыДляСравненияToolStripMenuItem_Click);
            // 
            // выполнитьОбьединениеToolStripMenuItem
            // 
            this.выполнитьОбьединениеToolStripMenuItem.Name = "выполнитьОбьединениеToolStripMenuItem";
            this.выполнитьОбьединениеToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
            this.выполнитьОбьединениеToolStripMenuItem.Text = "Выполнить обьединение";
            this.выполнитьОбьединениеToolStripMenuItem.Click += new System.EventHandler(this.выполнитьОбьединениеToolStripMenuItem_Click);
            // 
            // menuShirina
            // 
            this.menuShirina.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.колонкиToolStripMenuItem,
            this.колонокToolStripMenuItem});
            this.menuShirina.Name = "menuShirina";
            this.menuShirina.Size = new System.Drawing.Size(217, 22);
            this.menuShirina.Text = "Ширина";
            // 
            // колонкиToolStripMenuItem
            // 
            this.колонкиToolStripMenuItem.Name = "колонкиToolStripMenuItem";
            this.колонкиToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
            this.колонкиToolStripMenuItem.Text = "Колонки";
            this.колонкиToolStripMenuItem.Click += new System.EventHandler(this.колонкиToolStripMenuItem_Click);
            // 
            // колонокToolStripMenuItem
            // 
            this.колонокToolStripMenuItem.Name = "колонокToolStripMenuItem";
            this.колонокToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
            this.колонокToolStripMenuItem.Text = "Колонок";
            this.колонокToolStripMenuItem.Click += new System.EventHandler(this.колонокToolStripMenuItem_Click);
            // 
            // menuVisota
            // 
            this.menuVisota.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.строкиToolStripMenuItem,
            this.строкToolStripMenuItem,
            this.автоподборСтрокToolStripMenuItem});
            this.menuVisota.Name = "menuVisota";
            this.menuVisota.Size = new System.Drawing.Size(217, 22);
            this.menuVisota.Text = "Высота";
            // 
            // строкиToolStripMenuItem
            // 
            this.строкиToolStripMenuItem.Name = "строкиToolStripMenuItem";
            this.строкиToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.строкиToolStripMenuItem.Text = "Строки";
            this.строкиToolStripMenuItem.Click += new System.EventHandler(this.строкиToolStripMenuItem_Click);
            // 
            // строкToolStripMenuItem
            // 
            this.строкToolStripMenuItem.Name = "строкToolStripMenuItem";
            this.строкToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.строкToolStripMenuItem.Text = "Строк";
            this.строкToolStripMenuItem.Click += new System.EventHandler(this.строкToolStripMenuItem_Click);
            // 
            // автоподборСтрокToolStripMenuItem
            // 
            this.автоподборСтрокToolStripMenuItem.Name = "автоподборСтрокToolStripMenuItem";
            this.автоподборСтрокToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.автоподборСтрокToolStripMenuItem.Text = "Автоподбор строк";
            this.автоподборСтрокToolStripMenuItem.Click += new System.EventHandler(this.автоподборСтрокToolStripMenuItem_Click);
            // 
            // menuTextToCell
            // 
            this.menuTextToCell.Name = "menuTextToCell";
            this.menuTextToCell.Size = new System.Drawing.Size(217, 22);
            this.menuTextToCell.Text = "Вывод текста в ячейку";
            this.menuTextToCell.Click += new System.EventHandler(this.menuTextToCell_Click);
            // 
            // menuFont
            // 
            this.menuFont.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.размерШрифтаToolStripMenuItem,
            this.названиеШрифтаToolStripMenuItem,
            this.жирностьШрифтаToolStripMenuItem,
            this.подчеркиваниеШрифтаToolStripMenuItem,
            this.наклонностьШрифтаToolStripMenuItem});
            this.menuFont.Name = "menuFont";
            this.menuFont.Size = new System.Drawing.Size(217, 22);
            this.menuFont.Text = "Шрифт";
            // 
            // размерШрифтаToolStripMenuItem
            // 
            this.размерШрифтаToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.строкиToolStripMenuItem4,
            this.колонкиToolStripMenuItem4,
            this.ячейкиToolStripMenuItem3});
            this.размерШрифтаToolStripMenuItem.Name = "размерШрифтаToolStripMenuItem";
            this.размерШрифтаToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.размерШрифтаToolStripMenuItem.Text = "Размер шрифта";
            // 
            // строкиToolStripMenuItem4
            // 
            this.строкиToolStripMenuItem4.Name = "строкиToolStripMenuItem4";
            this.строкиToolStripMenuItem4.Size = new System.Drawing.Size(120, 22);
            this.строкиToolStripMenuItem4.Text = "Строки";
            this.строкиToolStripMenuItem4.Click += new System.EventHandler(this.строкиToolStripMenuItem4_Click);
            // 
            // колонкиToolStripMenuItem4
            // 
            this.колонкиToolStripMenuItem4.Name = "колонкиToolStripMenuItem4";
            this.колонкиToolStripMenuItem4.Size = new System.Drawing.Size(120, 22);
            this.колонкиToolStripMenuItem4.Text = "Колонки";
            this.колонкиToolStripMenuItem4.Click += new System.EventHandler(this.колонкиToolStripMenuItem4_Click);
            // 
            // ячейкиToolStripMenuItem3
            // 
            this.ячейкиToolStripMenuItem3.Name = "ячейкиToolStripMenuItem3";
            this.ячейкиToolStripMenuItem3.Size = new System.Drawing.Size(120, 22);
            this.ячейкиToolStripMenuItem3.Text = "Ячейки";
            this.ячейкиToolStripMenuItem3.Click += new System.EventHandler(this.ячейкиToolStripMenuItem3_Click);
            // 
            // названиеШрифтаToolStripMenuItem
            // 
            this.названиеШрифтаToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.строкиToolStripMenuItem5,
            this.колонкиToolStripMenuItem5,
            this.ячейкиToolStripMenuItem4});
            this.названиеШрифтаToolStripMenuItem.Name = "названиеШрифтаToolStripMenuItem";
            this.названиеШрифтаToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.названиеШрифтаToolStripMenuItem.Text = "Название шрифта";
            // 
            // строкиToolStripMenuItem5
            // 
            this.строкиToolStripMenuItem5.Name = "строкиToolStripMenuItem5";
            this.строкиToolStripMenuItem5.Size = new System.Drawing.Size(120, 22);
            this.строкиToolStripMenuItem5.Text = "Строки";
            this.строкиToolStripMenuItem5.Click += new System.EventHandler(this.строкиToolStripMenuItem5_Click);
            // 
            // колонкиToolStripMenuItem5
            // 
            this.колонкиToolStripMenuItem5.Name = "колонкиToolStripMenuItem5";
            this.колонкиToolStripMenuItem5.Size = new System.Drawing.Size(120, 22);
            this.колонкиToolStripMenuItem5.Text = "Колонки";
            this.колонкиToolStripMenuItem5.Click += new System.EventHandler(this.колонкиToolStripMenuItem5_Click);
            // 
            // ячейкиToolStripMenuItem4
            // 
            this.ячейкиToolStripMenuItem4.Name = "ячейкиToolStripMenuItem4";
            this.ячейкиToolStripMenuItem4.Size = new System.Drawing.Size(120, 22);
            this.ячейкиToolStripMenuItem4.Text = "Ячейки";
            this.ячейкиToolStripMenuItem4.Click += new System.EventHandler(this.ячейкиToolStripMenuItem4_Click);
            // 
            // жирностьШрифтаToolStripMenuItem
            // 
            this.жирностьШрифтаToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.строкиToolStripMenuItem6,
            this.колонкиToolStripMenuItem6,
            this.ячейкиToolStripMenuItem5});
            this.жирностьШрифтаToolStripMenuItem.Name = "жирностьШрифтаToolStripMenuItem";
            this.жирностьШрифтаToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.жирностьШрифтаToolStripMenuItem.Text = "Жирность шрифта";
            // 
            // строкиToolStripMenuItem6
            // 
            this.строкиToolStripMenuItem6.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.жирныйToolStripMenuItem,
            this.отменаToolStripMenuItem});
            this.строкиToolStripMenuItem6.Name = "строкиToolStripMenuItem6";
            this.строкиToolStripMenuItem6.Size = new System.Drawing.Size(120, 22);
            this.строкиToolStripMenuItem6.Text = "Строки";
            // 
            // жирныйToolStripMenuItem
            // 
            this.жирныйToolStripMenuItem.Name = "жирныйToolStripMenuItem";
            this.жирныйToolStripMenuItem.Size = new System.Drawing.Size(121, 22);
            this.жирныйToolStripMenuItem.Text = "Жирный";
            this.жирныйToolStripMenuItem.Click += new System.EventHandler(this.жирныйToolStripMenuItem_Click);
            // 
            // отменаToolStripMenuItem
            // 
            this.отменаToolStripMenuItem.Name = "отменаToolStripMenuItem";
            this.отменаToolStripMenuItem.Size = new System.Drawing.Size(121, 22);
            this.отменаToolStripMenuItem.Text = "Отмена";
            this.отменаToolStripMenuItem.Click += new System.EventHandler(this.отменаToolStripMenuItem_Click);
            // 
            // колонкиToolStripMenuItem6
            // 
            this.колонкиToolStripMenuItem6.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.жирныйToolStripMenuItem1,
            this.отменаToolStripMenuItem1});
            this.колонкиToolStripMenuItem6.Name = "колонкиToolStripMenuItem6";
            this.колонкиToolStripMenuItem6.Size = new System.Drawing.Size(120, 22);
            this.колонкиToolStripMenuItem6.Text = "Колонки";
            // 
            // жирныйToolStripMenuItem1
            // 
            this.жирныйToolStripMenuItem1.Name = "жирныйToolStripMenuItem1";
            this.жирныйToolStripMenuItem1.Size = new System.Drawing.Size(121, 22);
            this.жирныйToolStripMenuItem1.Text = "Жирный";
            this.жирныйToolStripMenuItem1.Click += new System.EventHandler(this.жирныйToolStripMenuItem1_Click);
            // 
            // отменаToolStripMenuItem1
            // 
            this.отменаToolStripMenuItem1.Name = "отменаToolStripMenuItem1";
            this.отменаToolStripMenuItem1.Size = new System.Drawing.Size(121, 22);
            this.отменаToolStripMenuItem1.Text = "Отмена";
            this.отменаToolStripMenuItem1.Click += new System.EventHandler(this.отменаToolStripMenuItem1_Click);
            // 
            // ячейкиToolStripMenuItem5
            // 
            this.ячейкиToolStripMenuItem5.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.жирныйToolStripMenuItem2,
            this.отменаToolStripMenuItem2});
            this.ячейкиToolStripMenuItem5.Name = "ячейкиToolStripMenuItem5";
            this.ячейкиToolStripMenuItem5.Size = new System.Drawing.Size(120, 22);
            this.ячейкиToolStripMenuItem5.Text = "Ячейки";
            // 
            // жирныйToolStripMenuItem2
            // 
            this.жирныйToolStripMenuItem2.Name = "жирныйToolStripMenuItem2";
            this.жирныйToolStripMenuItem2.Size = new System.Drawing.Size(121, 22);
            this.жирныйToolStripMenuItem2.Text = "Жирный";
            this.жирныйToolStripMenuItem2.Click += new System.EventHandler(this.жирныйToolStripMenuItem2_Click);
            // 
            // отменаToolStripMenuItem2
            // 
            this.отменаToolStripMenuItem2.Name = "отменаToolStripMenuItem2";
            this.отменаToolStripMenuItem2.Size = new System.Drawing.Size(121, 22);
            this.отменаToolStripMenuItem2.Text = "Отмена";
            this.отменаToolStripMenuItem2.Click += new System.EventHandler(this.отменаToolStripMenuItem2_Click);
            // 
            // подчеркиваниеШрифтаToolStripMenuItem
            // 
            this.подчеркиваниеШрифтаToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.строкиToolStripMenuItem7,
            this.колонкиToolStripMenuItem7,
            this.ячейкиToolStripMenuItem6});
            this.подчеркиваниеШрифтаToolStripMenuItem.Name = "подчеркиваниеШрифтаToolStripMenuItem";
            this.подчеркиваниеШрифтаToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.подчеркиваниеШрифтаToolStripMenuItem.Text = "Подчеркивание шрифта";
            // 
            // строкиToolStripMenuItem7
            // 
            this.строкиToolStripMenuItem7.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.тонкойЛиниейToolStripMenuItem,
            this.толстойЛиниейToolStripMenuItem,
            this.отменаToolStripMenuItem3});
            this.строкиToolStripMenuItem7.Name = "строкиToolStripMenuItem7";
            this.строкиToolStripMenuItem7.Size = new System.Drawing.Size(120, 22);
            this.строкиToolStripMenuItem7.Text = "Строки";
            // 
            // тонкойЛиниейToolStripMenuItem
            // 
            this.тонкойЛиниейToolStripMenuItem.Name = "тонкойЛиниейToolStripMenuItem";
            this.тонкойЛиниейToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.тонкойЛиниейToolStripMenuItem.Text = "Тонкой линией";
            this.тонкойЛиниейToolStripMenuItem.Click += new System.EventHandler(this.тонкойЛиниейToolStripMenuItem_Click);
            // 
            // толстойЛиниейToolStripMenuItem
            // 
            this.толстойЛиниейToolStripMenuItem.Name = "толстойЛиниейToolStripMenuItem";
            this.толстойЛиниейToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.толстойЛиниейToolStripMenuItem.Text = "Толстой линией";
            this.толстойЛиниейToolStripMenuItem.Click += new System.EventHandler(this.толстойЛиниейToolStripMenuItem_Click);
            // 
            // отменаToolStripMenuItem3
            // 
            this.отменаToolStripMenuItem3.Name = "отменаToolStripMenuItem3";
            this.отменаToolStripMenuItem3.Size = new System.Drawing.Size(159, 22);
            this.отменаToolStripMenuItem3.Text = "Отмена";
            this.отменаToolStripMenuItem3.Click += new System.EventHandler(this.отменаToolStripMenuItem3_Click);
            // 
            // колонкиToolStripMenuItem7
            // 
            this.колонкиToolStripMenuItem7.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.тонкойЛиниейToolStripMenuItem1,
            this.толстойЛиниейToolStripMenuItem1,
            this.отменаToolStripMenuItem4});
            this.колонкиToolStripMenuItem7.Name = "колонкиToolStripMenuItem7";
            this.колонкиToolStripMenuItem7.Size = new System.Drawing.Size(120, 22);
            this.колонкиToolStripMenuItem7.Text = "Колонки";
            // 
            // тонкойЛиниейToolStripMenuItem1
            // 
            this.тонкойЛиниейToolStripMenuItem1.Name = "тонкойЛиниейToolStripMenuItem1";
            this.тонкойЛиниейToolStripMenuItem1.Size = new System.Drawing.Size(159, 22);
            this.тонкойЛиниейToolStripMenuItem1.Text = "Тонкой линией";
            this.тонкойЛиниейToolStripMenuItem1.Click += new System.EventHandler(this.тонкойЛиниейToolStripMenuItem1_Click);
            // 
            // толстойЛиниейToolStripMenuItem1
            // 
            this.толстойЛиниейToolStripMenuItem1.Name = "толстойЛиниейToolStripMenuItem1";
            this.толстойЛиниейToolStripMenuItem1.Size = new System.Drawing.Size(159, 22);
            this.толстойЛиниейToolStripMenuItem1.Text = "Толстой линией";
            this.толстойЛиниейToolStripMenuItem1.Click += new System.EventHandler(this.толстойЛиниейToolStripMenuItem1_Click);
            // 
            // отменаToolStripMenuItem4
            // 
            this.отменаToolStripMenuItem4.Name = "отменаToolStripMenuItem4";
            this.отменаToolStripMenuItem4.Size = new System.Drawing.Size(159, 22);
            this.отменаToolStripMenuItem4.Text = "Отмена";
            this.отменаToolStripMenuItem4.Click += new System.EventHandler(this.отменаToolStripMenuItem4_Click);
            // 
            // ячейкиToolStripMenuItem6
            // 
            this.ячейкиToolStripMenuItem6.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.тонкойЛиниейToolStripMenuItem2,
            this.толстойЛиниейToolStripMenuItem2,
            this.отменаToolStripMenuItem5});
            this.ячейкиToolStripMenuItem6.Name = "ячейкиToolStripMenuItem6";
            this.ячейкиToolStripMenuItem6.Size = new System.Drawing.Size(120, 22);
            this.ячейкиToolStripMenuItem6.Text = "Ячейки";
            // 
            // тонкойЛиниейToolStripMenuItem2
            // 
            this.тонкойЛиниейToolStripMenuItem2.Name = "тонкойЛиниейToolStripMenuItem2";
            this.тонкойЛиниейToolStripMenuItem2.Size = new System.Drawing.Size(159, 22);
            this.тонкойЛиниейToolStripMenuItem2.Text = "Тонкой линией";
            this.тонкойЛиниейToolStripMenuItem2.Click += new System.EventHandler(this.тонкойЛиниейToolStripMenuItem2_Click);
            // 
            // толстойЛиниейToolStripMenuItem2
            // 
            this.толстойЛиниейToolStripMenuItem2.Name = "толстойЛиниейToolStripMenuItem2";
            this.толстойЛиниейToolStripMenuItem2.Size = new System.Drawing.Size(159, 22);
            this.толстойЛиниейToolStripMenuItem2.Text = "Толстой линией";
            this.толстойЛиниейToolStripMenuItem2.Click += new System.EventHandler(this.толстойЛиниейToolStripMenuItem2_Click);
            // 
            // отменаToolStripMenuItem5
            // 
            this.отменаToolStripMenuItem5.Name = "отменаToolStripMenuItem5";
            this.отменаToolStripMenuItem5.Size = new System.Drawing.Size(159, 22);
            this.отменаToolStripMenuItem5.Text = "Отмена";
            this.отменаToolStripMenuItem5.Click += new System.EventHandler(this.отменаToolStripMenuItem5_Click);
            // 
            // наклонностьШрифтаToolStripMenuItem
            // 
            this.наклонностьШрифтаToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.строкиToolStripMenuItem8,
            this.колонкиToolStripMenuItem8,
            this.ячейкиToolStripMenuItem7});
            this.наклонностьШрифтаToolStripMenuItem.Name = "наклонностьШрифтаToolStripMenuItem";
            this.наклонностьШрифтаToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.наклонностьШрифтаToolStripMenuItem.Text = "Наклонность шрифта";
            // 
            // строкиToolStripMenuItem8
            // 
            this.строкиToolStripMenuItem8.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.наклонныйToolStripMenuItem,
            this.отменаToolStripMenuItem6});
            this.строкиToolStripMenuItem8.Name = "строкиToolStripMenuItem8";
            this.строкиToolStripMenuItem8.Size = new System.Drawing.Size(120, 22);
            this.строкиToolStripMenuItem8.Text = "Строки";
            // 
            // наклонныйToolStripMenuItem
            // 
            this.наклонныйToolStripMenuItem.Name = "наклонныйToolStripMenuItem";
            this.наклонныйToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.наклонныйToolStripMenuItem.Text = "Наклонный";
            this.наклонныйToolStripMenuItem.Click += new System.EventHandler(this.наклонныйToolStripMenuItem_Click);
            // 
            // отменаToolStripMenuItem6
            // 
            this.отменаToolStripMenuItem6.Name = "отменаToolStripMenuItem6";
            this.отменаToolStripMenuItem6.Size = new System.Drawing.Size(136, 22);
            this.отменаToolStripMenuItem6.Text = "Отмена";
            this.отменаToolStripMenuItem6.Click += new System.EventHandler(this.отменаToolStripMenuItem6_Click);
            // 
            // колонкиToolStripMenuItem8
            // 
            this.колонкиToolStripMenuItem8.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.наклонныйToolStripMenuItem1,
            this.отменаToolStripMenuItem7});
            this.колонкиToolStripMenuItem8.Name = "колонкиToolStripMenuItem8";
            this.колонкиToolStripMenuItem8.Size = new System.Drawing.Size(120, 22);
            this.колонкиToolStripMenuItem8.Text = "Колонки";
            // 
            // наклонныйToolStripMenuItem1
            // 
            this.наклонныйToolStripMenuItem1.Name = "наклонныйToolStripMenuItem1";
            this.наклонныйToolStripMenuItem1.Size = new System.Drawing.Size(136, 22);
            this.наклонныйToolStripMenuItem1.Text = "Наклонный";
            this.наклонныйToolStripMenuItem1.Click += new System.EventHandler(this.наклонныйToolStripMenuItem1_Click);
            // 
            // отменаToolStripMenuItem7
            // 
            this.отменаToolStripMenuItem7.Name = "отменаToolStripMenuItem7";
            this.отменаToolStripMenuItem7.Size = new System.Drawing.Size(136, 22);
            this.отменаToolStripMenuItem7.Text = "Отмена";
            this.отменаToolStripMenuItem7.Click += new System.EventHandler(this.отменаToolStripMenuItem7_Click);
            // 
            // ячейкиToolStripMenuItem7
            // 
            this.ячейкиToolStripMenuItem7.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.наклонныйToolStripMenuItem2,
            this.отменаToolStripMenuItem8});
            this.ячейкиToolStripMenuItem7.Name = "ячейкиToolStripMenuItem7";
            this.ячейкиToolStripMenuItem7.Size = new System.Drawing.Size(120, 22);
            this.ячейкиToolStripMenuItem7.Text = "Ячейки";
            // 
            // наклонныйToolStripMenuItem2
            // 
            this.наклонныйToolStripMenuItem2.Name = "наклонныйToolStripMenuItem2";
            this.наклонныйToolStripMenuItem2.Size = new System.Drawing.Size(136, 22);
            this.наклонныйToolStripMenuItem2.Text = "Наклонный";
            this.наклонныйToolStripMenuItem2.Click += new System.EventHandler(this.наклонныйToolStripMenuItem2_Click);
            // 
            // отменаToolStripMenuItem8
            // 
            this.отменаToolStripMenuItem8.Name = "отменаToolStripMenuItem8";
            this.отменаToolStripMenuItem8.Size = new System.Drawing.Size(136, 22);
            this.отменаToolStripMenuItem8.Text = "Отмена";
            this.отменаToolStripMenuItem8.Click += new System.EventHandler(this.отменаToolStripMenuItem8_Click);
            // 
            // menuMerge
            // 
            this.menuMerge.Name = "menuMerge";
            this.menuMerge.Size = new System.Drawing.Size(217, 22);
            this.menuMerge.Text = "Объединение ячеек";
            this.menuMerge.Click += new System.EventHandler(this.menuMerge_Click);
            // 
            // menuInCell
            // 
            this.menuInCell.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.размерСимволовToolStripMenuItem,
            this.жирностьСимволовToolStripMenuItem,
            this.наклонностьСимволовToolStripMenuItem,
            this.подчеркиваниеСимволовToolStripMenuItem});
            this.menuInCell.Name = "menuInCell";
            this.menuInCell.Size = new System.Drawing.Size(217, 22);
            this.menuInCell.Text = "В ячейке";
            // 
            // размерСимволовToolStripMenuItem
            // 
            this.размерСимволовToolStripMenuItem.Name = "размерСимволовToolStripMenuItem";
            this.размерСимволовToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.размерСимволовToolStripMenuItem.Text = "Размер символов";
            this.размерСимволовToolStripMenuItem.Click += new System.EventHandler(this.размерСимволовToolStripMenuItem_Click);
            // 
            // жирностьСимволовToolStripMenuItem
            // 
            this.жирностьСимволовToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.жирныйToolStripMenuItem3,
            this.отменаToolStripMenuItem9});
            this.жирностьСимволовToolStripMenuItem.Name = "жирностьСимволовToolStripMenuItem";
            this.жирностьСимволовToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.жирностьСимволовToolStripMenuItem.Text = "Жирность символов";
            // 
            // жирныйToolStripMenuItem3
            // 
            this.жирныйToolStripMenuItem3.Name = "жирныйToolStripMenuItem3";
            this.жирныйToolStripMenuItem3.Size = new System.Drawing.Size(121, 22);
            this.жирныйToolStripMenuItem3.Text = "Жирный";
            this.жирныйToolStripMenuItem3.Click += new System.EventHandler(this.жирныйToolStripMenuItem3_Click);
            // 
            // отменаToolStripMenuItem9
            // 
            this.отменаToolStripMenuItem9.Name = "отменаToolStripMenuItem9";
            this.отменаToolStripMenuItem9.Size = new System.Drawing.Size(121, 22);
            this.отменаToolStripMenuItem9.Text = "Отмена";
            this.отменаToolStripMenuItem9.Click += new System.EventHandler(this.отменаToolStripMenuItem9_Click);
            // 
            // наклонностьСимволовToolStripMenuItem
            // 
            this.наклонностьСимволовToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.наклонныйToolStripMenuItem3,
            this.отменаToolStripMenuItem10});
            this.наклонностьСимволовToolStripMenuItem.Name = "наклонностьСимволовToolStripMenuItem";
            this.наклонностьСимволовToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.наклонностьСимволовToolStripMenuItem.Text = "Наклонность символов";
            // 
            // наклонныйToolStripMenuItem3
            // 
            this.наклонныйToolStripMenuItem3.Name = "наклонныйToolStripMenuItem3";
            this.наклонныйToolStripMenuItem3.Size = new System.Drawing.Size(136, 22);
            this.наклонныйToolStripMenuItem3.Text = "Наклонный";
            this.наклонныйToolStripMenuItem3.Click += new System.EventHandler(this.наклонныйToolStripMenuItem3_Click);
            // 
            // отменаToolStripMenuItem10
            // 
            this.отменаToolStripMenuItem10.Name = "отменаToolStripMenuItem10";
            this.отменаToolStripMenuItem10.Size = new System.Drawing.Size(136, 22);
            this.отменаToolStripMenuItem10.Text = "Отмена";
            this.отменаToolStripMenuItem10.Click += new System.EventHandler(this.отменаToolStripMenuItem10_Click);
            // 
            // подчеркиваниеСимволовToolStripMenuItem
            // 
            this.подчеркиваниеСимволовToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.тонкойЛиниейToolStripMenuItem3,
            this.толстойЛиниейToolStripMenuItem3,
            this.отменаToolStripMenuItem11});
            this.подчеркиваниеСимволовToolStripMenuItem.Name = "подчеркиваниеСимволовToolStripMenuItem";
            this.подчеркиваниеСимволовToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.подчеркиваниеСимволовToolStripMenuItem.Text = "Подчеркивание символов";
            // 
            // тонкойЛиниейToolStripMenuItem3
            // 
            this.тонкойЛиниейToolStripMenuItem3.Name = "тонкойЛиниейToolStripMenuItem3";
            this.тонкойЛиниейToolStripMenuItem3.Size = new System.Drawing.Size(159, 22);
            this.тонкойЛиниейToolStripMenuItem3.Text = "Тонкой линией";
            this.тонкойЛиниейToolStripMenuItem3.Click += new System.EventHandler(this.тонкойЛиниейToolStripMenuItem3_Click);
            // 
            // толстойЛиниейToolStripMenuItem3
            // 
            this.толстойЛиниейToolStripMenuItem3.Name = "толстойЛиниейToolStripMenuItem3";
            this.толстойЛиниейToolStripMenuItem3.Size = new System.Drawing.Size(159, 22);
            this.толстойЛиниейToolStripMenuItem3.Text = "Толстой линией";
            this.толстойЛиниейToolStripMenuItem3.Click += new System.EventHandler(this.толстойЛиниейToolStripMenuItem3_Click);
            // 
            // отменаToolStripMenuItem11
            // 
            this.отменаToolStripMenuItem11.Name = "отменаToolStripMenuItem11";
            this.отменаToolStripMenuItem11.Size = new System.Drawing.Size(159, 22);
            this.отменаToolStripMenuItem11.Text = "Отмена";
            this.отменаToolStripMenuItem11.Click += new System.EventHandler(this.отменаToolStripMenuItem11_Click);
            // 
            // menuShrink
            // 
            this.menuShrink.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.поГоризонталиToolStripMenuItem,
            this.поВертикалиToolStripMenuItem});
            this.menuShrink.Name = "menuShrink";
            this.menuShrink.Size = new System.Drawing.Size(217, 22);
            this.menuShrink.Text = "Выравнивание";
            // 
            // поГоризонталиToolStripMenuItem
            // 
            this.поГоризонталиToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.строкиToolStripMenuItem9,
            this.колонкиToolStripMenuItem9,
            this.ячейкиToolStripMenuItem8});
            this.поГоризонталиToolStripMenuItem.Name = "поГоризонталиToolStripMenuItem";
            this.поГоризонталиToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.поГоризонталиToolStripMenuItem.Text = "По горизонтали";
            // 
            // строкиToolStripMenuItem9
            // 
            this.строкиToolStripMenuItem9.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.поЗначениюToolStripMenuItem,
            this.поЛевомуКраюToolStripMenuItem,
            this.поЦентруToolStripMenuItem2,
            this.поПравомуКраюToolStripMenuItem,
            this.поШиринеToolStripMenuItem});
            this.строкиToolStripMenuItem9.Name = "строкиToolStripMenuItem9";
            this.строкиToolStripMenuItem9.Size = new System.Drawing.Size(120, 22);
            this.строкиToolStripMenuItem9.Text = "Строки";
            // 
            // поЗначениюToolStripMenuItem
            // 
            this.поЗначениюToolStripMenuItem.Name = "поЗначениюToolStripMenuItem";
            this.поЗначениюToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.поЗначениюToolStripMenuItem.Text = "По значению";
            this.поЗначениюToolStripMenuItem.Click += new System.EventHandler(this.поЗначениюToolStripMenuItem_Click);
            // 
            // поЛевомуКраюToolStripMenuItem
            // 
            this.поЛевомуКраюToolStripMenuItem.Name = "поЛевомуКраюToolStripMenuItem";
            this.поЛевомуКраюToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.поЛевомуКраюToolStripMenuItem.Text = "По левому краю";
            this.поЛевомуКраюToolStripMenuItem.Click += new System.EventHandler(this.поЛевомуКраюToolStripMenuItem_Click);
            // 
            // поЦентруToolStripMenuItem2
            // 
            this.поЦентруToolStripMenuItem2.Name = "поЦентруToolStripMenuItem2";
            this.поЦентруToolStripMenuItem2.Size = new System.Drawing.Size(169, 22);
            this.поЦентруToolStripMenuItem2.Text = "По центру";
            this.поЦентруToolStripMenuItem2.Click += new System.EventHandler(this.поЦентруToolStripMenuItem2_Click);
            // 
            // поПравомуКраюToolStripMenuItem
            // 
            this.поПравомуКраюToolStripMenuItem.Name = "поПравомуКраюToolStripMenuItem";
            this.поПравомуКраюToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.поПравомуКраюToolStripMenuItem.Text = "По правому краю";
            this.поПравомуКраюToolStripMenuItem.Click += new System.EventHandler(this.поПравомуКраюToolStripMenuItem_Click);
            // 
            // поШиринеToolStripMenuItem
            // 
            this.поШиринеToolStripMenuItem.Name = "поШиринеToolStripMenuItem";
            this.поШиринеToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.поШиринеToolStripMenuItem.Text = "По ширине";
            this.поШиринеToolStripMenuItem.Click += new System.EventHandler(this.поШиринеToolStripMenuItem_Click);
            // 
            // колонкиToolStripMenuItem9
            // 
            this.колонкиToolStripMenuItem9.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.поЗначениюToolStripMenuItem1,
            this.toolStripMenuItem2,
            this.toolStripMenuItem3,
            this.toolStripMenuItem4,
            this.toolStripMenuItem5});
            this.колонкиToolStripMenuItem9.Name = "колонкиToolStripMenuItem9";
            this.колонкиToolStripMenuItem9.Size = new System.Drawing.Size(120, 22);
            this.колонкиToolStripMenuItem9.Text = "Колонки";
            // 
            // поЗначениюToolStripMenuItem1
            // 
            this.поЗначениюToolStripMenuItem1.Name = "поЗначениюToolStripMenuItem1";
            this.поЗначениюToolStripMenuItem1.Size = new System.Drawing.Size(169, 22);
            this.поЗначениюToolStripMenuItem1.Text = "По значению";
            this.поЗначениюToolStripMenuItem1.Click += new System.EventHandler(this.поЗначениюToolStripMenuItem1_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(169, 22);
            this.toolStripMenuItem2.Text = "По левому краю";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(169, 22);
            this.toolStripMenuItem3.Text = "По центру";
            this.toolStripMenuItem3.Click += new System.EventHandler(this.toolStripMenuItem3_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(169, 22);
            this.toolStripMenuItem4.Text = "По правому краю";
            this.toolStripMenuItem4.Click += new System.EventHandler(this.toolStripMenuItem4_Click);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(169, 22);
            this.toolStripMenuItem5.Text = "По ширине";
            this.toolStripMenuItem5.Click += new System.EventHandler(this.toolStripMenuItem5_Click);
            // 
            // ячейкиToolStripMenuItem8
            // 
            this.ячейкиToolStripMenuItem8.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.поЗначениюToolStripMenuItem2,
            this.toolStripMenuItem6,
            this.toolStripMenuItem7,
            this.toolStripMenuItem8,
            this.toolStripMenuItem9});
            this.ячейкиToolStripMenuItem8.Name = "ячейкиToolStripMenuItem8";
            this.ячейкиToolStripMenuItem8.Size = new System.Drawing.Size(120, 22);
            this.ячейкиToolStripMenuItem8.Text = "Ячейки";
            // 
            // поЗначениюToolStripMenuItem2
            // 
            this.поЗначениюToolStripMenuItem2.Name = "поЗначениюToolStripMenuItem2";
            this.поЗначениюToolStripMenuItem2.Size = new System.Drawing.Size(169, 22);
            this.поЗначениюToolStripMenuItem2.Text = "По значению";
            this.поЗначениюToolStripMenuItem2.Click += new System.EventHandler(this.поЗначениюToolStripMenuItem2_Click);
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(169, 22);
            this.toolStripMenuItem6.Text = "По левому краю";
            this.toolStripMenuItem6.Click += new System.EventHandler(this.toolStripMenuItem6_Click);
            // 
            // toolStripMenuItem7
            // 
            this.toolStripMenuItem7.Name = "toolStripMenuItem7";
            this.toolStripMenuItem7.Size = new System.Drawing.Size(169, 22);
            this.toolStripMenuItem7.Text = "По центру";
            this.toolStripMenuItem7.Click += new System.EventHandler(this.toolStripMenuItem7_Click);
            // 
            // toolStripMenuItem8
            // 
            this.toolStripMenuItem8.Name = "toolStripMenuItem8";
            this.toolStripMenuItem8.Size = new System.Drawing.Size(169, 22);
            this.toolStripMenuItem8.Text = "По правому краю";
            this.toolStripMenuItem8.Click += new System.EventHandler(this.toolStripMenuItem8_Click);
            // 
            // toolStripMenuItem9
            // 
            this.toolStripMenuItem9.Name = "toolStripMenuItem9";
            this.toolStripMenuItem9.Size = new System.Drawing.Size(169, 22);
            this.toolStripMenuItem9.Text = "По ширине";
            this.toolStripMenuItem9.Click += new System.EventHandler(this.toolStripMenuItem9_Click);
            // 
            // поВертикалиToolStripMenuItem
            // 
            this.поВертикалиToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.строкиToolStripMenuItem10,
            this.колонкиToolStripMenuItem10,
            this.ячейкиToolStripMenuItem9});
            this.поВертикалиToolStripMenuItem.Name = "поВертикалиToolStripMenuItem";
            this.поВертикалиToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.поВертикалиToolStripMenuItem.Text = "По вертикали";
            // 
            // строкиToolStripMenuItem10
            // 
            this.строкиToolStripMenuItem10.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.поВерхнемуКраюToolStripMenuItem,
            this.поЦентруToolStripMenuItem3,
            this.поНижнемуКраюToolStripMenuItem,
            this.поВысотеToolStripMenuItem});
            this.строкиToolStripMenuItem10.Name = "строкиToolStripMenuItem10";
            this.строкиToolStripMenuItem10.Size = new System.Drawing.Size(120, 22);
            this.строкиToolStripMenuItem10.Text = "Строки";
            // 
            // поВерхнемуКраюToolStripMenuItem
            // 
            this.поВерхнемуКраюToolStripMenuItem.Name = "поВерхнемуКраюToolStripMenuItem";
            this.поВерхнемуКраюToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.поВерхнемуКраюToolStripMenuItem.Text = "По верхнему краю";
            this.поВерхнемуКраюToolStripMenuItem.Click += new System.EventHandler(this.поВерхнемуКраюToolStripMenuItem_Click);
            // 
            // поЦентруToolStripMenuItem3
            // 
            this.поЦентруToolStripMenuItem3.Name = "поЦентруToolStripMenuItem3";
            this.поЦентруToolStripMenuItem3.Size = new System.Drawing.Size(173, 22);
            this.поЦентруToolStripMenuItem3.Text = "По центру";
            this.поЦентруToolStripMenuItem3.Click += new System.EventHandler(this.поЦентруToolStripMenuItem3_Click);
            // 
            // поНижнемуКраюToolStripMenuItem
            // 
            this.поНижнемуКраюToolStripMenuItem.Name = "поНижнемуКраюToolStripMenuItem";
            this.поНижнемуКраюToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.поНижнемуКраюToolStripMenuItem.Text = "По нижнему краю";
            this.поНижнемуКраюToolStripMenuItem.Click += new System.EventHandler(this.поНижнемуКраюToolStripMenuItem_Click);
            // 
            // поВысотеToolStripMenuItem
            // 
            this.поВысотеToolStripMenuItem.Name = "поВысотеToolStripMenuItem";
            this.поВысотеToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.поВысотеToolStripMenuItem.Text = "По высоте";
            this.поВысотеToolStripMenuItem.Click += new System.EventHandler(this.поВысотеToolStripMenuItem_Click);
            // 
            // колонкиToolStripMenuItem10
            // 
            this.колонкиToolStripMenuItem10.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.поВерхнемуКраюToolStripMenuItem1,
            this.toolStripMenuItem1,
            this.toolStripMenuItem10,
            this.toolStripMenuItem11});
            this.колонкиToolStripMenuItem10.Name = "колонкиToolStripMenuItem10";
            this.колонкиToolStripMenuItem10.Size = new System.Drawing.Size(120, 22);
            this.колонкиToolStripMenuItem10.Text = "Колонки";
            // 
            // поВерхнемуКраюToolStripMenuItem1
            // 
            this.поВерхнемуКраюToolStripMenuItem1.Name = "поВерхнемуКраюToolStripMenuItem1";
            this.поВерхнемуКраюToolStripMenuItem1.Size = new System.Drawing.Size(173, 22);
            this.поВерхнемуКраюToolStripMenuItem1.Text = "По верхнему краю";
            this.поВерхнемуКраюToolStripMenuItem1.Click += new System.EventHandler(this.поВерхнемуКраюToolStripMenuItem1_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(173, 22);
            this.toolStripMenuItem1.Text = "По центру";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // toolStripMenuItem10
            // 
            this.toolStripMenuItem10.Name = "toolStripMenuItem10";
            this.toolStripMenuItem10.Size = new System.Drawing.Size(173, 22);
            this.toolStripMenuItem10.Text = "По нижнему краю";
            this.toolStripMenuItem10.Click += new System.EventHandler(this.toolStripMenuItem10_Click);
            // 
            // toolStripMenuItem11
            // 
            this.toolStripMenuItem11.Name = "toolStripMenuItem11";
            this.toolStripMenuItem11.Size = new System.Drawing.Size(173, 22);
            this.toolStripMenuItem11.Text = "По высоте";
            this.toolStripMenuItem11.Click += new System.EventHandler(this.toolStripMenuItem11_Click);
            // 
            // ячейкиToolStripMenuItem9
            // 
            this.ячейкиToolStripMenuItem9.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.поВерхнемуКраюToolStripMenuItem2,
            this.toolStripMenuItem12,
            this.toolStripMenuItem13,
            this.toolStripMenuItem14});
            this.ячейкиToolStripMenuItem9.Name = "ячейкиToolStripMenuItem9";
            this.ячейкиToolStripMenuItem9.Size = new System.Drawing.Size(120, 22);
            this.ячейкиToolStripMenuItem9.Text = "Ячейки";
            // 
            // поВерхнемуКраюToolStripMenuItem2
            // 
            this.поВерхнемуКраюToolStripMenuItem2.Name = "поВерхнемуКраюToolStripMenuItem2";
            this.поВерхнемуКраюToolStripMenuItem2.Size = new System.Drawing.Size(173, 22);
            this.поВерхнемуКраюToolStripMenuItem2.Text = "По верхнему краю";
            this.поВерхнемуКраюToolStripMenuItem2.Click += new System.EventHandler(this.поВерхнемуКраюToolStripMenuItem2_Click);
            // 
            // toolStripMenuItem12
            // 
            this.toolStripMenuItem12.Name = "toolStripMenuItem12";
            this.toolStripMenuItem12.Size = new System.Drawing.Size(173, 22);
            this.toolStripMenuItem12.Text = "По центру";
            this.toolStripMenuItem12.Click += new System.EventHandler(this.toolStripMenuItem12_Click);
            // 
            // toolStripMenuItem13
            // 
            this.toolStripMenuItem13.Name = "toolStripMenuItem13";
            this.toolStripMenuItem13.Size = new System.Drawing.Size(173, 22);
            this.toolStripMenuItem13.Text = "По нижнему краю";
            this.toolStripMenuItem13.Click += new System.EventHandler(this.toolStripMenuItem13_Click);
            // 
            // toolStripMenuItem14
            // 
            this.toolStripMenuItem14.Name = "toolStripMenuItem14";
            this.toolStripMenuItem14.Size = new System.Drawing.Size(173, 22);
            this.toolStripMenuItem14.Text = "По высоте";
            this.toolStripMenuItem14.Click += new System.EventHandler(this.toolStripMenuItem14_Click);
            // 
            // menuLine
            // 
            this.menuLine.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.видЛинииToolStripMenuItem,
            this.толщинаЛинииToolStripMenuItem,
            this.линияПоКоординатамToolStripMenuItem});
            this.menuLine.Name = "menuLine";
            this.menuLine.Size = new System.Drawing.Size(217, 22);
            this.menuLine.Text = "Линия";
            // 
            // видЛинииToolStripMenuItem
            // 
            this.видЛинииToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.всейЯчейкиToolStripMenuItem,
            this.вокругЯчейкиToolStripMenuItem,
            this.сверхуЯчейкиToolStripMenuItem,
            this.слеваЯчейкиToolStripMenuItem,
            this.снизуЯчейкиToolStripMenuItem,
            this.справаЯчейкиToolStripMenuItem});
            this.видЛинииToolStripMenuItem.Name = "видЛинииToolStripMenuItem";
            this.видЛинииToolStripMenuItem.Size = new System.Drawing.Size(200, 22);
            this.видЛинииToolStripMenuItem.Text = "Вид линии";
            // 
            // всейЯчейкиToolStripMenuItem
            // 
            this.всейЯчейкиToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.нетToolStripMenuItem,
            this.толстаяToolStripMenuItem,
            this.тонкаяToolStripMenuItem,
            this.двойнаяToolStripMenuItem,
            this.пунктирнаяToolStripMenuItem,
            this.точкамиToolStripMenuItem});
            this.всейЯчейкиToolStripMenuItem.Name = "всейЯчейкиToolStripMenuItem";
            this.всейЯчейкиToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.всейЯчейкиToolStripMenuItem.Text = "Всей ячейки";
            // 
            // нетToolStripMenuItem
            // 
            this.нетToolStripMenuItem.Name = "нетToolStripMenuItem";
            this.нетToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.нетToolStripMenuItem.Text = "Нет";
            this.нетToolStripMenuItem.Click += new System.EventHandler(this.нетToolStripMenuItem_Click);
            // 
            // толстаяToolStripMenuItem
            // 
            this.толстаяToolStripMenuItem.Name = "толстаяToolStripMenuItem";
            this.толстаяToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.толстаяToolStripMenuItem.Text = "Толстая";
            this.толстаяToolStripMenuItem.Click += new System.EventHandler(this.толстаяToolStripMenuItem_Click);
            // 
            // тонкаяToolStripMenuItem
            // 
            this.тонкаяToolStripMenuItem.Name = "тонкаяToolStripMenuItem";
            this.тонкаяToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.тонкаяToolStripMenuItem.Text = "Тонкая";
            this.тонкаяToolStripMenuItem.Click += new System.EventHandler(this.тонкаяToolStripMenuItem_Click);
            // 
            // двойнаяToolStripMenuItem
            // 
            this.двойнаяToolStripMenuItem.Name = "двойнаяToolStripMenuItem";
            this.двойнаяToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.двойнаяToolStripMenuItem.Text = "Двойная";
            this.двойнаяToolStripMenuItem.Click += new System.EventHandler(this.двойнаяToolStripMenuItem_Click);
            // 
            // пунктирнаяToolStripMenuItem
            // 
            this.пунктирнаяToolStripMenuItem.Name = "пунктирнаяToolStripMenuItem";
            this.пунктирнаяToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.пунктирнаяToolStripMenuItem.Text = "Пунктирная";
            this.пунктирнаяToolStripMenuItem.Click += new System.EventHandler(this.пунктирнаяToolStripMenuItem_Click);
            // 
            // точкамиToolStripMenuItem
            // 
            this.точкамиToolStripMenuItem.Name = "точкамиToolStripMenuItem";
            this.точкамиToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.точкамиToolStripMenuItem.Text = "Точками";
            this.точкамиToolStripMenuItem.Click += new System.EventHandler(this.точкамиToolStripMenuItem_Click);
            // 
            // вокругЯчейкиToolStripMenuItem
            // 
            this.вокругЯчейкиToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.нетToolStripMenuItem1,
            this.toolStripMenuItem21,
            this.toolStripMenuItem22,
            this.toolStripMenuItem23,
            this.toolStripMenuItem24,
            this.toolStripMenuItem25});
            this.вокругЯчейкиToolStripMenuItem.Name = "вокругЯчейкиToolStripMenuItem";
            this.вокругЯчейкиToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.вокругЯчейкиToolStripMenuItem.Text = "Вокруг ячейки";
            // 
            // нетToolStripMenuItem1
            // 
            this.нетToolStripMenuItem1.Name = "нетToolStripMenuItem1";
            this.нетToolStripMenuItem1.Size = new System.Drawing.Size(138, 22);
            this.нетToolStripMenuItem1.Text = "Нет";
            this.нетToolStripMenuItem1.Click += new System.EventHandler(this.нетToolStripMenuItem1_Click);
            // 
            // toolStripMenuItem21
            // 
            this.toolStripMenuItem21.Name = "toolStripMenuItem21";
            this.toolStripMenuItem21.Size = new System.Drawing.Size(138, 22);
            this.toolStripMenuItem21.Text = "Точками";
            this.toolStripMenuItem21.Click += new System.EventHandler(this.toolStripMenuItem21_Click);
            // 
            // toolStripMenuItem22
            // 
            this.toolStripMenuItem22.Name = "toolStripMenuItem22";
            this.toolStripMenuItem22.Size = new System.Drawing.Size(138, 22);
            this.toolStripMenuItem22.Text = "Пунктирная";
            this.toolStripMenuItem22.Click += new System.EventHandler(this.toolStripMenuItem22_Click);
            // 
            // toolStripMenuItem23
            // 
            this.toolStripMenuItem23.Name = "toolStripMenuItem23";
            this.toolStripMenuItem23.Size = new System.Drawing.Size(138, 22);
            this.toolStripMenuItem23.Text = "Двойная";
            this.toolStripMenuItem23.Click += new System.EventHandler(this.toolStripMenuItem23_Click);
            // 
            // toolStripMenuItem24
            // 
            this.toolStripMenuItem24.Name = "toolStripMenuItem24";
            this.toolStripMenuItem24.Size = new System.Drawing.Size(138, 22);
            this.toolStripMenuItem24.Text = "Тонкая";
            this.toolStripMenuItem24.Click += new System.EventHandler(this.toolStripMenuItem24_Click);
            // 
            // toolStripMenuItem25
            // 
            this.toolStripMenuItem25.Name = "toolStripMenuItem25";
            this.toolStripMenuItem25.Size = new System.Drawing.Size(138, 22);
            this.toolStripMenuItem25.Text = "Толстая";
            this.toolStripMenuItem25.Click += new System.EventHandler(this.toolStripMenuItem25_Click);
            // 
            // сверхуЯчейкиToolStripMenuItem
            // 
            this.сверхуЯчейкиToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.нетToolStripMenuItem2,
            this.toolStripMenuItem26,
            this.toolStripMenuItem27,
            this.toolStripMenuItem28,
            this.toolStripMenuItem29,
            this.toolStripMenuItem30});
            this.сверхуЯчейкиToolStripMenuItem.Name = "сверхуЯчейкиToolStripMenuItem";
            this.сверхуЯчейкиToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.сверхуЯчейкиToolStripMenuItem.Text = "Сверху ячейки";
            // 
            // нетToolStripMenuItem2
            // 
            this.нетToolStripMenuItem2.Name = "нетToolStripMenuItem2";
            this.нетToolStripMenuItem2.Size = new System.Drawing.Size(138, 22);
            this.нетToolStripMenuItem2.Text = "Нет";
            this.нетToolStripMenuItem2.Click += new System.EventHandler(this.нетToolStripMenuItem2_Click);
            // 
            // toolStripMenuItem26
            // 
            this.toolStripMenuItem26.Name = "toolStripMenuItem26";
            this.toolStripMenuItem26.Size = new System.Drawing.Size(138, 22);
            this.toolStripMenuItem26.Text = "Точками";
            this.toolStripMenuItem26.Click += new System.EventHandler(this.toolStripMenuItem26_Click);
            // 
            // toolStripMenuItem27
            // 
            this.toolStripMenuItem27.Name = "toolStripMenuItem27";
            this.toolStripMenuItem27.Size = new System.Drawing.Size(138, 22);
            this.toolStripMenuItem27.Text = "Пунктирная";
            this.toolStripMenuItem27.Click += new System.EventHandler(this.toolStripMenuItem27_Click);
            // 
            // toolStripMenuItem28
            // 
            this.toolStripMenuItem28.Name = "toolStripMenuItem28";
            this.toolStripMenuItem28.Size = new System.Drawing.Size(138, 22);
            this.toolStripMenuItem28.Text = "Двойная";
            this.toolStripMenuItem28.Click += new System.EventHandler(this.toolStripMenuItem28_Click);
            // 
            // toolStripMenuItem29
            // 
            this.toolStripMenuItem29.Name = "toolStripMenuItem29";
            this.toolStripMenuItem29.Size = new System.Drawing.Size(138, 22);
            this.toolStripMenuItem29.Text = "Тонкая";
            this.toolStripMenuItem29.Click += new System.EventHandler(this.toolStripMenuItem29_Click);
            // 
            // toolStripMenuItem30
            // 
            this.toolStripMenuItem30.Name = "toolStripMenuItem30";
            this.toolStripMenuItem30.Size = new System.Drawing.Size(138, 22);
            this.toolStripMenuItem30.Text = "Толстая";
            this.toolStripMenuItem30.Click += new System.EventHandler(this.toolStripMenuItem30_Click);
            // 
            // слеваЯчейкиToolStripMenuItem
            // 
            this.слеваЯчейкиToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.нетToolStripMenuItem3,
            this.toolStripMenuItem31,
            this.toolStripMenuItem32,
            this.toolStripMenuItem33,
            this.toolStripMenuItem34,
            this.toolStripMenuItem35});
            this.слеваЯчейкиToolStripMenuItem.Name = "слеваЯчейкиToolStripMenuItem";
            this.слеваЯчейкиToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.слеваЯчейкиToolStripMenuItem.Text = "Слева ячейки";
            // 
            // нетToolStripMenuItem3
            // 
            this.нетToolStripMenuItem3.Name = "нетToolStripMenuItem3";
            this.нетToolStripMenuItem3.Size = new System.Drawing.Size(138, 22);
            this.нетToolStripMenuItem3.Text = "Нет";
            this.нетToolStripMenuItem3.Click += new System.EventHandler(this.нетToolStripMenuItem3_Click);
            // 
            // toolStripMenuItem31
            // 
            this.toolStripMenuItem31.Name = "toolStripMenuItem31";
            this.toolStripMenuItem31.Size = new System.Drawing.Size(138, 22);
            this.toolStripMenuItem31.Text = "Точками";
            this.toolStripMenuItem31.Click += new System.EventHandler(this.toolStripMenuItem31_Click);
            // 
            // toolStripMenuItem32
            // 
            this.toolStripMenuItem32.Name = "toolStripMenuItem32";
            this.toolStripMenuItem32.Size = new System.Drawing.Size(138, 22);
            this.toolStripMenuItem32.Text = "Пунктирная";
            this.toolStripMenuItem32.Click += new System.EventHandler(this.toolStripMenuItem32_Click);
            // 
            // toolStripMenuItem33
            // 
            this.toolStripMenuItem33.Name = "toolStripMenuItem33";
            this.toolStripMenuItem33.Size = new System.Drawing.Size(138, 22);
            this.toolStripMenuItem33.Text = "Двойная";
            this.toolStripMenuItem33.Click += new System.EventHandler(this.toolStripMenuItem33_Click);
            // 
            // toolStripMenuItem34
            // 
            this.toolStripMenuItem34.Name = "toolStripMenuItem34";
            this.toolStripMenuItem34.Size = new System.Drawing.Size(138, 22);
            this.toolStripMenuItem34.Text = "Тонкая";
            this.toolStripMenuItem34.Click += new System.EventHandler(this.toolStripMenuItem34_Click);
            // 
            // toolStripMenuItem35
            // 
            this.toolStripMenuItem35.Name = "toolStripMenuItem35";
            this.toolStripMenuItem35.Size = new System.Drawing.Size(138, 22);
            this.toolStripMenuItem35.Text = "Толстая";
            this.toolStripMenuItem35.Click += new System.EventHandler(this.toolStripMenuItem35_Click);
            // 
            // снизуЯчейкиToolStripMenuItem
            // 
            this.снизуЯчейкиToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.нетToolStripMenuItem4,
            this.toolStripMenuItem36,
            this.toolStripMenuItem37,
            this.toolStripMenuItem38,
            this.toolStripMenuItem39,
            this.toolStripMenuItem40});
            this.снизуЯчейкиToolStripMenuItem.Name = "снизуЯчейкиToolStripMenuItem";
            this.снизуЯчейкиToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.снизуЯчейкиToolStripMenuItem.Text = "Снизу ячейки";
            // 
            // нетToolStripMenuItem4
            // 
            this.нетToolStripMenuItem4.Name = "нетToolStripMenuItem4";
            this.нетToolStripMenuItem4.Size = new System.Drawing.Size(138, 22);
            this.нетToolStripMenuItem4.Text = "Нет";
            this.нетToolStripMenuItem4.Click += new System.EventHandler(this.нетToolStripMenuItem4_Click);
            // 
            // toolStripMenuItem36
            // 
            this.toolStripMenuItem36.Name = "toolStripMenuItem36";
            this.toolStripMenuItem36.Size = new System.Drawing.Size(138, 22);
            this.toolStripMenuItem36.Text = "Точками";
            this.toolStripMenuItem36.Click += new System.EventHandler(this.toolStripMenuItem36_Click);
            // 
            // toolStripMenuItem37
            // 
            this.toolStripMenuItem37.Name = "toolStripMenuItem37";
            this.toolStripMenuItem37.Size = new System.Drawing.Size(138, 22);
            this.toolStripMenuItem37.Text = "Пунктирная";
            this.toolStripMenuItem37.Click += new System.EventHandler(this.toolStripMenuItem37_Click);
            // 
            // toolStripMenuItem38
            // 
            this.toolStripMenuItem38.Name = "toolStripMenuItem38";
            this.toolStripMenuItem38.Size = new System.Drawing.Size(138, 22);
            this.toolStripMenuItem38.Text = "Двойная";
            this.toolStripMenuItem38.Click += new System.EventHandler(this.toolStripMenuItem38_Click);
            // 
            // toolStripMenuItem39
            // 
            this.toolStripMenuItem39.Name = "toolStripMenuItem39";
            this.toolStripMenuItem39.Size = new System.Drawing.Size(138, 22);
            this.toolStripMenuItem39.Text = "Тонкая";
            this.toolStripMenuItem39.Click += new System.EventHandler(this.toolStripMenuItem39_Click);
            // 
            // toolStripMenuItem40
            // 
            this.toolStripMenuItem40.Name = "toolStripMenuItem40";
            this.toolStripMenuItem40.Size = new System.Drawing.Size(138, 22);
            this.toolStripMenuItem40.Text = "Толстая";
            this.toolStripMenuItem40.Click += new System.EventHandler(this.toolStripMenuItem40_Click);
            // 
            // справаЯчейкиToolStripMenuItem
            // 
            this.справаЯчейкиToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.нетToolStripMenuItem5,
            this.toolStripMenuItem41,
            this.toolStripMenuItem42,
            this.toolStripMenuItem43,
            this.toolStripMenuItem44,
            this.toolStripMenuItem45});
            this.справаЯчейкиToolStripMenuItem.Name = "справаЯчейкиToolStripMenuItem";
            this.справаЯчейкиToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.справаЯчейкиToolStripMenuItem.Text = "Справа ячейки";
            // 
            // нетToolStripMenuItem5
            // 
            this.нетToolStripMenuItem5.Name = "нетToolStripMenuItem5";
            this.нетToolStripMenuItem5.Size = new System.Drawing.Size(138, 22);
            this.нетToolStripMenuItem5.Text = "Нет";
            this.нетToolStripMenuItem5.Click += new System.EventHandler(this.нетToolStripMenuItem5_Click);
            // 
            // toolStripMenuItem41
            // 
            this.toolStripMenuItem41.Name = "toolStripMenuItem41";
            this.toolStripMenuItem41.Size = new System.Drawing.Size(138, 22);
            this.toolStripMenuItem41.Text = "Точками";
            this.toolStripMenuItem41.Click += new System.EventHandler(this.toolStripMenuItem41_Click);
            // 
            // toolStripMenuItem42
            // 
            this.toolStripMenuItem42.Name = "toolStripMenuItem42";
            this.toolStripMenuItem42.Size = new System.Drawing.Size(138, 22);
            this.toolStripMenuItem42.Text = "Пунктирная";
            this.toolStripMenuItem42.Click += new System.EventHandler(this.toolStripMenuItem42_Click);
            // 
            // toolStripMenuItem43
            // 
            this.toolStripMenuItem43.Name = "toolStripMenuItem43";
            this.toolStripMenuItem43.Size = new System.Drawing.Size(138, 22);
            this.toolStripMenuItem43.Text = "Двойная";
            this.toolStripMenuItem43.Click += new System.EventHandler(this.toolStripMenuItem43_Click);
            // 
            // toolStripMenuItem44
            // 
            this.toolStripMenuItem44.Name = "toolStripMenuItem44";
            this.toolStripMenuItem44.Size = new System.Drawing.Size(138, 22);
            this.toolStripMenuItem44.Text = "Тонкая";
            this.toolStripMenuItem44.Click += new System.EventHandler(this.toolStripMenuItem44_Click);
            // 
            // toolStripMenuItem45
            // 
            this.toolStripMenuItem45.Name = "toolStripMenuItem45";
            this.toolStripMenuItem45.Size = new System.Drawing.Size(138, 22);
            this.toolStripMenuItem45.Text = "Толстая";
            this.toolStripMenuItem45.Click += new System.EventHandler(this.toolStripMenuItem45_Click);
            // 
            // толщинаЛинииToolStripMenuItem
            // 
            this.толщинаЛинииToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem46,
            this.toolStripMenuItem53,
            this.toolStripMenuItem60,
            this.toolStripMenuItem67,
            this.toolStripMenuItem74,
            this.toolStripMenuItem81});
            this.толщинаЛинииToolStripMenuItem.Name = "толщинаЛинииToolStripMenuItem";
            this.толщинаЛинииToolStripMenuItem.Size = new System.Drawing.Size(200, 22);
            this.толщинаЛинииToolStripMenuItem.Text = "Толщина линии";
            // 
            // toolStripMenuItem46
            // 
            this.toolStripMenuItem46.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem48,
            this.toolStripMenuItem49,
            this.toolStripMenuItem50,
            this.toolStripMenuItem51});
            this.toolStripMenuItem46.Name = "toolStripMenuItem46";
            this.toolStripMenuItem46.Size = new System.Drawing.Size(155, 22);
            this.toolStripMenuItem46.Text = "Всей ячейки";
            // 
            // toolStripMenuItem48
            // 
            this.toolStripMenuItem48.Name = "toolStripMenuItem48";
            this.toolStripMenuItem48.Size = new System.Drawing.Size(80, 22);
            this.toolStripMenuItem48.Text = "1";
            this.toolStripMenuItem48.Click += new System.EventHandler(this.toolStripMenuItem48_Click);
            // 
            // toolStripMenuItem49
            // 
            this.toolStripMenuItem49.Name = "toolStripMenuItem49";
            this.toolStripMenuItem49.Size = new System.Drawing.Size(80, 22);
            this.toolStripMenuItem49.Text = "2";
            this.toolStripMenuItem49.Click += new System.EventHandler(this.toolStripMenuItem49_Click);
            // 
            // toolStripMenuItem50
            // 
            this.toolStripMenuItem50.Name = "toolStripMenuItem50";
            this.toolStripMenuItem50.Size = new System.Drawing.Size(80, 22);
            this.toolStripMenuItem50.Text = "3";
            this.toolStripMenuItem50.Click += new System.EventHandler(this.toolStripMenuItem50_Click);
            // 
            // toolStripMenuItem51
            // 
            this.toolStripMenuItem51.Name = "toolStripMenuItem51";
            this.toolStripMenuItem51.Size = new System.Drawing.Size(80, 22);
            this.toolStripMenuItem51.Text = "4";
            this.toolStripMenuItem51.Click += new System.EventHandler(this.toolStripMenuItem51_Click);
            // 
            // toolStripMenuItem53
            // 
            this.toolStripMenuItem53.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem52,
            this.toolStripMenuItem54,
            this.toolStripMenuItem55,
            this.toolStripMenuItem56});
            this.toolStripMenuItem53.Name = "toolStripMenuItem53";
            this.toolStripMenuItem53.Size = new System.Drawing.Size(155, 22);
            this.toolStripMenuItem53.Text = "Вокруг ячейки";
            // 
            // toolStripMenuItem52
            // 
            this.toolStripMenuItem52.Name = "toolStripMenuItem52";
            this.toolStripMenuItem52.Size = new System.Drawing.Size(80, 22);
            this.toolStripMenuItem52.Text = "1";
            this.toolStripMenuItem52.Click += new System.EventHandler(this.toolStripMenuItem52_Click);
            // 
            // toolStripMenuItem54
            // 
            this.toolStripMenuItem54.Name = "toolStripMenuItem54";
            this.toolStripMenuItem54.Size = new System.Drawing.Size(80, 22);
            this.toolStripMenuItem54.Text = "2";
            this.toolStripMenuItem54.Click += new System.EventHandler(this.toolStripMenuItem54_Click);
            // 
            // toolStripMenuItem55
            // 
            this.toolStripMenuItem55.Name = "toolStripMenuItem55";
            this.toolStripMenuItem55.Size = new System.Drawing.Size(80, 22);
            this.toolStripMenuItem55.Text = "3";
            this.toolStripMenuItem55.Click += new System.EventHandler(this.toolStripMenuItem55_Click);
            // 
            // toolStripMenuItem56
            // 
            this.toolStripMenuItem56.Name = "toolStripMenuItem56";
            this.toolStripMenuItem56.Size = new System.Drawing.Size(80, 22);
            this.toolStripMenuItem56.Text = "4";
            this.toolStripMenuItem56.Click += new System.EventHandler(this.toolStripMenuItem56_Click);
            // 
            // toolStripMenuItem60
            // 
            this.toolStripMenuItem60.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem66,
            this.toolStripMenuItem57,
            this.toolStripMenuItem58,
            this.toolStripMenuItem59});
            this.toolStripMenuItem60.Name = "toolStripMenuItem60";
            this.toolStripMenuItem60.Size = new System.Drawing.Size(155, 22);
            this.toolStripMenuItem60.Text = "Сверху ячейки";
            // 
            // toolStripMenuItem66
            // 
            this.toolStripMenuItem66.Name = "toolStripMenuItem66";
            this.toolStripMenuItem66.Size = new System.Drawing.Size(80, 22);
            this.toolStripMenuItem66.Text = "1";
            this.toolStripMenuItem66.Click += new System.EventHandler(this.toolStripMenuItem66_Click);
            // 
            // toolStripMenuItem57
            // 
            this.toolStripMenuItem57.Name = "toolStripMenuItem57";
            this.toolStripMenuItem57.Size = new System.Drawing.Size(80, 22);
            this.toolStripMenuItem57.Text = "2";
            this.toolStripMenuItem57.Click += new System.EventHandler(this.toolStripMenuItem57_Click);
            // 
            // toolStripMenuItem58
            // 
            this.toolStripMenuItem58.Name = "toolStripMenuItem58";
            this.toolStripMenuItem58.Size = new System.Drawing.Size(80, 22);
            this.toolStripMenuItem58.Text = "3";
            this.toolStripMenuItem58.Click += new System.EventHandler(this.toolStripMenuItem58_Click);
            // 
            // toolStripMenuItem59
            // 
            this.toolStripMenuItem59.Name = "toolStripMenuItem59";
            this.toolStripMenuItem59.Size = new System.Drawing.Size(80, 22);
            this.toolStripMenuItem59.Text = "4";
            this.toolStripMenuItem59.Click += new System.EventHandler(this.toolStripMenuItem59_Click);
            // 
            // toolStripMenuItem67
            // 
            this.toolStripMenuItem67.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem73,
            this.toolStripMenuItem61,
            this.toolStripMenuItem62,
            this.toolStripMenuItem63});
            this.toolStripMenuItem67.Name = "toolStripMenuItem67";
            this.toolStripMenuItem67.Size = new System.Drawing.Size(155, 22);
            this.toolStripMenuItem67.Text = "Слева ячейки";
            // 
            // toolStripMenuItem73
            // 
            this.toolStripMenuItem73.Name = "toolStripMenuItem73";
            this.toolStripMenuItem73.Size = new System.Drawing.Size(80, 22);
            this.toolStripMenuItem73.Text = "1";
            this.toolStripMenuItem73.Click += new System.EventHandler(this.toolStripMenuItem73_Click);
            // 
            // toolStripMenuItem61
            // 
            this.toolStripMenuItem61.Name = "toolStripMenuItem61";
            this.toolStripMenuItem61.Size = new System.Drawing.Size(80, 22);
            this.toolStripMenuItem61.Text = "2";
            this.toolStripMenuItem61.Click += new System.EventHandler(this.toolStripMenuItem61_Click);
            // 
            // toolStripMenuItem62
            // 
            this.toolStripMenuItem62.Name = "toolStripMenuItem62";
            this.toolStripMenuItem62.Size = new System.Drawing.Size(80, 22);
            this.toolStripMenuItem62.Text = "3";
            this.toolStripMenuItem62.Click += new System.EventHandler(this.toolStripMenuItem62_Click);
            // 
            // toolStripMenuItem63
            // 
            this.toolStripMenuItem63.Name = "toolStripMenuItem63";
            this.toolStripMenuItem63.Size = new System.Drawing.Size(80, 22);
            this.toolStripMenuItem63.Text = "4";
            this.toolStripMenuItem63.Click += new System.EventHandler(this.toolStripMenuItem63_Click);
            // 
            // toolStripMenuItem74
            // 
            this.toolStripMenuItem74.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem80,
            this.toolStripMenuItem64,
            this.toolStripMenuItem65,
            this.toolStripMenuItem68});
            this.toolStripMenuItem74.Name = "toolStripMenuItem74";
            this.toolStripMenuItem74.Size = new System.Drawing.Size(155, 22);
            this.toolStripMenuItem74.Text = "Снизу ячейки";
            // 
            // toolStripMenuItem80
            // 
            this.toolStripMenuItem80.Name = "toolStripMenuItem80";
            this.toolStripMenuItem80.Size = new System.Drawing.Size(80, 22);
            this.toolStripMenuItem80.Text = "1";
            this.toolStripMenuItem80.Click += new System.EventHandler(this.toolStripMenuItem80_Click);
            // 
            // toolStripMenuItem64
            // 
            this.toolStripMenuItem64.Name = "toolStripMenuItem64";
            this.toolStripMenuItem64.Size = new System.Drawing.Size(80, 22);
            this.toolStripMenuItem64.Text = "2";
            this.toolStripMenuItem64.Click += new System.EventHandler(this.toolStripMenuItem64_Click);
            // 
            // toolStripMenuItem65
            // 
            this.toolStripMenuItem65.Name = "toolStripMenuItem65";
            this.toolStripMenuItem65.Size = new System.Drawing.Size(80, 22);
            this.toolStripMenuItem65.Text = "3";
            this.toolStripMenuItem65.Click += new System.EventHandler(this.toolStripMenuItem65_Click);
            // 
            // toolStripMenuItem68
            // 
            this.toolStripMenuItem68.Name = "toolStripMenuItem68";
            this.toolStripMenuItem68.Size = new System.Drawing.Size(80, 22);
            this.toolStripMenuItem68.Text = "4";
            this.toolStripMenuItem68.Click += new System.EventHandler(this.toolStripMenuItem68_Click);
            // 
            // toolStripMenuItem81
            // 
            this.toolStripMenuItem81.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem87,
            this.toolStripMenuItem69,
            this.toolStripMenuItem70,
            this.toolStripMenuItem71});
            this.toolStripMenuItem81.Name = "toolStripMenuItem81";
            this.toolStripMenuItem81.Size = new System.Drawing.Size(155, 22);
            this.toolStripMenuItem81.Text = "Справа ячейки";
            // 
            // toolStripMenuItem87
            // 
            this.toolStripMenuItem87.Name = "toolStripMenuItem87";
            this.toolStripMenuItem87.Size = new System.Drawing.Size(80, 22);
            this.toolStripMenuItem87.Text = "1";
            this.toolStripMenuItem87.Click += new System.EventHandler(this.toolStripMenuItem87_Click);
            // 
            // toolStripMenuItem69
            // 
            this.toolStripMenuItem69.Name = "toolStripMenuItem69";
            this.toolStripMenuItem69.Size = new System.Drawing.Size(80, 22);
            this.toolStripMenuItem69.Text = "2";
            this.toolStripMenuItem69.Click += new System.EventHandler(this.toolStripMenuItem69_Click);
            // 
            // toolStripMenuItem70
            // 
            this.toolStripMenuItem70.Name = "toolStripMenuItem70";
            this.toolStripMenuItem70.Size = new System.Drawing.Size(80, 22);
            this.toolStripMenuItem70.Text = "3";
            this.toolStripMenuItem70.Click += new System.EventHandler(this.toolStripMenuItem70_Click);
            // 
            // toolStripMenuItem71
            // 
            this.toolStripMenuItem71.Name = "toolStripMenuItem71";
            this.toolStripMenuItem71.Size = new System.Drawing.Size(80, 22);
            this.toolStripMenuItem71.Text = "4";
            this.toolStripMenuItem71.Click += new System.EventHandler(this.toolStripMenuItem71_Click);
            // 
            // линияПоКоординатамToolStripMenuItem
            // 
            this.линияПоКоординатамToolStripMenuItem.Name = "линияПоКоординатамToolStripMenuItem";
            this.линияПоКоординатамToolStripMenuItem.Size = new System.Drawing.Size(200, 22);
            this.линияПоКоординатамToolStripMenuItem.Text = "Линия по координатам";
            this.линияПоКоординатамToolStripMenuItem.Click += new System.EventHandler(this.линияПоКоординатамToolStripMenuItem_Click);
            // 
            // menuFormat
            // 
            this.menuFormat.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.строкиToolStripMenuItem11,
            this.колонкиToolStripMenuItem11,
            this.ячейкиToolStripMenuItem10});
            this.menuFormat.Name = "menuFormat";
            this.menuFormat.Size = new System.Drawing.Size(217, 22);
            this.menuFormat.Text = "Формат";
            // 
            // строкиToolStripMenuItem11
            // 
            this.строкиToolStripMenuItem11.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.общийToolStripMenuItem,
            this.числовойToolStripMenuItem,
            this.текстовыйToolStripMenuItem,
            this.датаToolStripMenuItem3});
            this.строкиToolStripMenuItem11.Name = "строкиToolStripMenuItem11";
            this.строкиToolStripMenuItem11.Size = new System.Drawing.Size(120, 22);
            this.строкиToolStripMenuItem11.Text = "Строки";
            // 
            // общийToolStripMenuItem
            // 
            this.общийToolStripMenuItem.Name = "общийToolStripMenuItem";
            this.общийToolStripMenuItem.Size = new System.Drawing.Size(129, 22);
            this.общийToolStripMenuItem.Text = "Общий";
            this.общийToolStripMenuItem.Click += new System.EventHandler(this.общийToolStripMenuItem_Click);
            // 
            // числовойToolStripMenuItem
            // 
            this.числовойToolStripMenuItem.Name = "числовойToolStripMenuItem";
            this.числовойToolStripMenuItem.Size = new System.Drawing.Size(129, 22);
            this.числовойToolStripMenuItem.Text = "Числовой";
            this.числовойToolStripMenuItem.Click += new System.EventHandler(this.числовойToolStripMenuItem_Click);
            // 
            // текстовыйToolStripMenuItem
            // 
            this.текстовыйToolStripMenuItem.Name = "текстовыйToolStripMenuItem";
            this.текстовыйToolStripMenuItem.Size = new System.Drawing.Size(129, 22);
            this.текстовыйToolStripMenuItem.Text = "Текстовый";
            this.текстовыйToolStripMenuItem.Click += new System.EventHandler(this.текстовыйToolStripMenuItem_Click);
            // 
            // датаToolStripMenuItem3
            // 
            this.датаToolStripMenuItem3.Name = "датаToolStripMenuItem3";
            this.датаToolStripMenuItem3.Size = new System.Drawing.Size(129, 22);
            this.датаToolStripMenuItem3.Text = "Дата";
            this.датаToolStripMenuItem3.Click += new System.EventHandler(this.датаToolStripMenuItem3_Click);
            // 
            // колонкиToolStripMenuItem11
            // 
            this.колонкиToolStripMenuItem11.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.общийToolStripMenuItem1,
            this.toolStripMenuItem15,
            this.toolStripMenuItem16,
            this.toolStripMenuItem17});
            this.колонкиToolStripMenuItem11.Name = "колонкиToolStripMenuItem11";
            this.колонкиToolStripMenuItem11.Size = new System.Drawing.Size(120, 22);
            this.колонкиToolStripMenuItem11.Text = "Колонки";
            // 
            // общийToolStripMenuItem1
            // 
            this.общийToolStripMenuItem1.Name = "общийToolStripMenuItem1";
            this.общийToolStripMenuItem1.Size = new System.Drawing.Size(129, 22);
            this.общийToolStripMenuItem1.Text = "Общий";
            this.общийToolStripMenuItem1.Click += new System.EventHandler(this.общийToolStripMenuItem1_Click);
            // 
            // toolStripMenuItem15
            // 
            this.toolStripMenuItem15.Name = "toolStripMenuItem15";
            this.toolStripMenuItem15.Size = new System.Drawing.Size(129, 22);
            this.toolStripMenuItem15.Text = "Числовой";
            this.toolStripMenuItem15.Click += new System.EventHandler(this.toolStripMenuItem15_Click);
            // 
            // toolStripMenuItem16
            // 
            this.toolStripMenuItem16.Name = "toolStripMenuItem16";
            this.toolStripMenuItem16.Size = new System.Drawing.Size(129, 22);
            this.toolStripMenuItem16.Text = "Текстовый";
            this.toolStripMenuItem16.Click += new System.EventHandler(this.toolStripMenuItem16_Click);
            // 
            // toolStripMenuItem17
            // 
            this.toolStripMenuItem17.Name = "toolStripMenuItem17";
            this.toolStripMenuItem17.Size = new System.Drawing.Size(129, 22);
            this.toolStripMenuItem17.Text = "Дата";
            this.toolStripMenuItem17.Click += new System.EventHandler(this.toolStripMenuItem17_Click);
            // 
            // ячейкиToolStripMenuItem10
            // 
            this.ячейкиToolStripMenuItem10.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.общийToolStripMenuItem2,
            this.toolStripMenuItem18,
            this.toolStripMenuItem19,
            this.toolStripMenuItem20});
            this.ячейкиToolStripMenuItem10.Name = "ячейкиToolStripMenuItem10";
            this.ячейкиToolStripMenuItem10.Size = new System.Drawing.Size(120, 22);
            this.ячейкиToolStripMenuItem10.Text = "Ячейки";
            // 
            // общийToolStripMenuItem2
            // 
            this.общийToolStripMenuItem2.Name = "общийToolStripMenuItem2";
            this.общийToolStripMenuItem2.Size = new System.Drawing.Size(129, 22);
            this.общийToolStripMenuItem2.Text = "Общий";
            this.общийToolStripMenuItem2.Click += new System.EventHandler(this.общийToolStripMenuItem2_Click);
            // 
            // toolStripMenuItem18
            // 
            this.toolStripMenuItem18.Name = "toolStripMenuItem18";
            this.toolStripMenuItem18.Size = new System.Drawing.Size(129, 22);
            this.toolStripMenuItem18.Text = "Числовой";
            this.toolStripMenuItem18.Click += new System.EventHandler(this.toolStripMenuItem18_Click);
            // 
            // toolStripMenuItem19
            // 
            this.toolStripMenuItem19.Name = "toolStripMenuItem19";
            this.toolStripMenuItem19.Size = new System.Drawing.Size(129, 22);
            this.toolStripMenuItem19.Text = "Текстовый";
            this.toolStripMenuItem19.Click += new System.EventHandler(this.toolStripMenuItem19_Click);
            // 
            // toolStripMenuItem20
            // 
            this.toolStripMenuItem20.Name = "toolStripMenuItem20";
            this.toolStripMenuItem20.Size = new System.Drawing.Size(129, 22);
            this.toolStripMenuItem20.Text = "Дата";
            this.toolStripMenuItem20.Click += new System.EventHandler(this.toolStripMenuItem20_Click);
            // 
            // menuTextWrap
            // 
            this.menuTextWrap.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.строкиToolStripMenuItem3,
            this.колонкиToolStripMenuItem3,
            this.ячейкиToolStripMenuItem2});
            this.menuTextWrap.Name = "menuTextWrap";
            this.menuTextWrap.Size = new System.Drawing.Size(217, 22);
            this.menuTextWrap.Text = "Поворот текста";
            // 
            // строкиToolStripMenuItem3
            // 
            this.строкиToolStripMenuItem3.Name = "строкиToolStripMenuItem3";
            this.строкиToolStripMenuItem3.Size = new System.Drawing.Size(120, 22);
            this.строкиToolStripMenuItem3.Text = "Строки";
            this.строкиToolStripMenuItem3.Click += new System.EventHandler(this.строкиToolStripMenuItem3_Click);
            // 
            // колонкиToolStripMenuItem3
            // 
            this.колонкиToolStripMenuItem3.Name = "колонкиToolStripMenuItem3";
            this.колонкиToolStripMenuItem3.Size = new System.Drawing.Size(120, 22);
            this.колонкиToolStripMenuItem3.Text = "Колонки";
            this.колонкиToolStripMenuItem3.Click += new System.EventHandler(this.колонкиToolStripMenuItem3_Click);
            // 
            // ячейкиToolStripMenuItem2
            // 
            this.ячейкиToolStripMenuItem2.Name = "ячейкиToolStripMenuItem2";
            this.ячейкиToolStripMenuItem2.Size = new System.Drawing.Size(120, 22);
            this.ячейкиToolStripMenuItem2.Text = "Ячейки";
            this.ячейкиToolStripMenuItem2.Click += new System.EventHandler(this.ячейкиToolStripMenuItem2_Click);
            // 
            // menuTextMove
            // 
            this.menuTextMove.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.строкиToolStripMenuItem2,
            this.колонкиToolStripMenuItem2,
            this.ячейкиToolStripMenuItem1});
            this.menuTextMove.Name = "menuTextMove";
            this.menuTextMove.Size = new System.Drawing.Size(217, 22);
            this.menuTextMove.Text = "Перенос текста";
            // 
            // строкиToolStripMenuItem2
            // 
            this.строкиToolStripMenuItem2.Name = "строкиToolStripMenuItem2";
            this.строкиToolStripMenuItem2.Size = new System.Drawing.Size(120, 22);
            this.строкиToolStripMenuItem2.Text = "Строки";
            this.строкиToolStripMenuItem2.Click += new System.EventHandler(this.строкиToolStripMenuItem2_Click);
            // 
            // колонкиToolStripMenuItem2
            // 
            this.колонкиToolStripMenuItem2.Name = "колонкиToolStripMenuItem2";
            this.колонкиToolStripMenuItem2.Size = new System.Drawing.Size(120, 22);
            this.колонкиToolStripMenuItem2.Text = "Колонки";
            this.колонкиToolStripMenuItem2.Click += new System.EventHandler(this.колонкиToolStripMenuItem2_Click);
            // 
            // ячейкиToolStripMenuItem1
            // 
            this.ячейкиToolStripMenuItem1.Name = "ячейкиToolStripMenuItem1";
            this.ячейкиToolStripMenuItem1.Size = new System.Drawing.Size(120, 22);
            this.ячейкиToolStripMenuItem1.Text = "Ячейки";
            this.ячейкиToolStripMenuItem1.Click += new System.EventHandler(this.ячейкиToolStripMenuItem1_Click);
            // 
            // textMoveCancel
            // 
            this.textMoveCancel.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.строкиToolStripMenuItem1,
            this.колонкиToolStripMenuItem1,
            this.ячейкиToolStripMenuItem});
            this.textMoveCancel.Name = "textMoveCancel";
            this.textMoveCancel.Size = new System.Drawing.Size(217, 22);
            this.textMoveCancel.Text = "Отмена переноса текста";
            // 
            // строкиToolStripMenuItem1
            // 
            this.строкиToolStripMenuItem1.Name = "строкиToolStripMenuItem1";
            this.строкиToolStripMenuItem1.Size = new System.Drawing.Size(120, 22);
            this.строкиToolStripMenuItem1.Text = "Строки";
            this.строкиToolStripMenuItem1.Click += new System.EventHandler(this.строкиToolStripMenuItem1_Click);
            // 
            // колонкиToolStripMenuItem1
            // 
            this.колонкиToolStripMenuItem1.Name = "колонкиToolStripMenuItem1";
            this.колонкиToolStripMenuItem1.Size = new System.Drawing.Size(120, 22);
            this.колонкиToolStripMenuItem1.Text = "Колонки";
            this.колонкиToolStripMenuItem1.Click += new System.EventHandler(this.колонкиToolStripMenuItem1_Click);
            // 
            // ячейкиToolStripMenuItem
            // 
            this.ячейкиToolStripMenuItem.Name = "ячейкиToolStripMenuItem";
            this.ячейкиToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
            this.ячейкиToolStripMenuItem.Text = "Ячейки";
            this.ячейкиToolStripMenuItem.Click += new System.EventHandler(this.ячейкиToolStripMenuItem_Click);
            // 
            // menuFillCell
            // 
            this.menuFillCell.Name = "menuFillCell";
            this.menuFillCell.Size = new System.Drawing.Size(217, 22);
            this.menuFillCell.Text = "Заливка ячейки";
            this.menuFillCell.Click += new System.EventHandler(this.menuFillCell_Click);
            // 
            // menuPhotoToCell
            // 
            this.menuPhotoToCell.Name = "menuPhotoToCell";
            this.menuPhotoToCell.Size = new System.Drawing.Size(217, 22);
            this.menuPhotoToCell.Text = "Вывод фото в ячейку";
            this.menuPhotoToCell.Click += new System.EventHandler(this.menuPhotoToCell_Click);
            // 
            // выводШрихкодаВЯчейкуToolStripMenuItem
            // 
            this.выводШрихкодаВЯчейкуToolStripMenuItem.Name = "выводШрихкодаВЯчейкуToolStripMenuItem";
            this.выводШрихкодаВЯчейкуToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
            this.выводШрихкодаВЯчейкуToolStripMenuItem.Text = "Вывод шрих-кода в ячейку";
            this.выводШрихкодаВЯчейкуToolStripMenuItem.Click += new System.EventHandler(this.выводШрихкодаВЯчейкуToolStripMenuItem_Click);
            // 
            // menuGerbToCell
            // 
            this.menuGerbToCell.Name = "menuGerbToCell";
            this.menuGerbToCell.Size = new System.Drawing.Size(217, 22);
            this.menuGerbToCell.Text = "Вывод герба в ячейку";
            this.menuGerbToCell.Click += new System.EventHandler(this.menuGerbToCell_Click);
            // 
            // menuWord
            // 
            this.menuWord.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.открытьWORDToolStripMenuItem,
            this.заменитьВWORDeToolStripMenuItem,
            this.соранитьWORDToolStripMenuItem});
            this.menuWord.Name = "menuWord";
            this.menuWord.Size = new System.Drawing.Size(217, 22);
            this.menuWord.Text = "WORD";
            // 
            // открытьWORDToolStripMenuItem
            // 
            this.открытьWORDToolStripMenuItem.Name = "открытьWORDToolStripMenuItem";
            this.открытьWORDToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.открытьWORDToolStripMenuItem.Text = "Открыть WORD";
            this.открытьWORDToolStripMenuItem.Click += new System.EventHandler(this.открытьWORDToolStripMenuItem_Click);
            // 
            // заменитьВWORDeToolStripMenuItem
            // 
            this.заменитьВWORDeToolStripMenuItem.Name = "заменитьВWORDeToolStripMenuItem";
            this.заменитьВWORDeToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.заменитьВWORDeToolStripMenuItem.Text = "Заменить в WORD\'e";
            this.заменитьВWORDeToolStripMenuItem.Click += new System.EventHandler(this.заменитьВWORDeToolStripMenuItem_Click);
            // 
            // соранитьWORDToolStripMenuItem
            // 
            this.соранитьWORDToolStripMenuItem.Name = "соранитьWORDToolStripMenuItem";
            this.соранитьWORDToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.соранитьWORDToolStripMenuItem.Text = "Соранить WORD";
            this.соранитьWORDToolStripMenuItem.Click += new System.EventHandler(this.соранитьWORDToolStripMenuItem_Click);
            // 
            // выводТаблицыНачинаяСToolStripMenuItem
            // 
            this.выводТаблицыНачинаяСToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.левыйВерхнийУголToolStripMenuItem,
            this.левыйВерхнийУголСИтогамиToolStripMenuItem});
            this.выводТаблицыНачинаяСToolStripMenuItem.Name = "выводТаблицыНачинаяСToolStripMenuItem";
            this.выводТаблицыНачинаяСToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
            this.выводТаблицыНачинаяСToolStripMenuItem.Text = "Вывод таблицы";
            // 
            // левыйВерхнийУголToolStripMenuItem
            // 
            this.левыйВерхнийУголToolStripMenuItem.Name = "левыйВерхнийУголToolStripMenuItem";
            this.левыйВерхнийУголToolStripMenuItem.Size = new System.Drawing.Size(240, 22);
            this.левыйВерхнийУголToolStripMenuItem.Text = " левый верхний угол";
            this.левыйВерхнийУголToolStripMenuItem.Click += new System.EventHandler(this.левыйВерхнийУголToolStripMenuItem_Click);
            // 
            // левыйВерхнийУголСИтогамиToolStripMenuItem
            // 
            this.левыйВерхнийУголСИтогамиToolStripMenuItem.Name = "левыйВерхнийУголСИтогамиToolStripMenuItem";
            this.левыйВерхнийУголСИтогамиToolStripMenuItem.Size = new System.Drawing.Size(240, 22);
            this.левыйВерхнийУголСИтогамиToolStripMenuItem.Text = " левый верхний угол с итогами";
            this.левыйВерхнийУголСИтогамиToolStripMenuItem.Click += new System.EventHandler(this.левыйВерхнийУголСИтогамиToolStripMenuItem_Click);
            // 
            // documentMap1
            // 
            this.documentMap1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.documentMap1.ForeColor = System.Drawing.Color.OliveDrab;
            this.documentMap1.Location = new System.Drawing.Point(0, 0);
            this.documentMap1.Name = "documentMap1";
            this.documentMap1.Size = new System.Drawing.Size(170, 470);
            this.documentMap1.TabIndex = 3;
            this.documentMap1.Target = this.tbMain;
            this.documentMap1.Text = "documentMap1";
            // 
            // cbStopExecution
            // 
            this.cbStopExecution.Image = global::MKP.Printing.Properties.Resources.stop;
            this.cbStopExecution.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cbStopExecution.Location = new System.Drawing.Point(282, 27);
            this.cbStopExecution.Name = "cbStopExecution";
            this.cbStopExecution.Size = new System.Drawing.Size(94, 26);
            this.cbStopExecution.TabIndex = 8;
            this.cbStopExecution.Text = "Остановить";
            this.cbStopExecution.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ttMain.SetToolTip(this.cbStopExecution, "Остановить");
            this.cbStopExecution.UseVisualStyleBackColor = true;
            this.cbStopExecution.Visible = false;
            this.cbStopExecution.Click += new System.EventHandler(this.cbStopExecution_Click);
            // 
            // bSave
            // 
            this.bSave.Image = global::MKP.Printing.Properties.Resources.save;
            this.bSave.Location = new System.Drawing.Point(12, 27);
            this.bSave.Name = "bSave";
            this.bSave.Size = new System.Drawing.Size(26, 26);
            this.bSave.TabIndex = 8;
            this.ttMain.SetToolTip(this.bSave, "Сохранить");
            this.bSave.UseVisualStyleBackColor = true;
            this.bSave.Click += new System.EventHandler(this.сохранитьToolStripMenuItem1_Click);
            // 
            // bCursor
            // 
            this.bCursor.Image = global::MKP.Printing.Properties.Resources._520735_mouse_arrow_20;
            this.bCursor.Location = new System.Drawing.Point(230, 27);
            this.bCursor.Name = "bCursor";
            this.bCursor.Size = new System.Drawing.Size(26, 26);
            this.bCursor.TabIndex = 8;
            this.ttMain.SetToolTip(this.bCursor, "Выполнить до курсора F4");
            this.bCursor.UseVisualStyleBackColor = true;
            this.bCursor.Click += new System.EventHandler(this.доКурсораToolStripMenuItem_Click);
            // 
            // bStops
            // 
            this.bStops.Image = global::MKP.Printing.Properties.Resources.button_record;
            this.bStops.Location = new System.Drawing.Point(198, 27);
            this.bStops.Name = "bStops";
            this.bStops.Size = new System.Drawing.Size(26, 26);
            this.bStops.TabIndex = 8;
            this.ttMain.SetToolTip(this.bStops, "Выполнить по меткам F5");
            this.bStops.UseVisualStyleBackColor = true;
            this.bStops.Click += new System.EventHandler(this.запуститьПоМеткамToolStripMenuItem_Click);
            // 
            // bStep
            // 
            this.bStep.Image = global::MKP.Printing.Properties.Resources.step;
            this.bStep.Location = new System.Drawing.Point(166, 27);
            this.bStep.Name = "bStep";
            this.bStep.Size = new System.Drawing.Size(26, 26);
            this.bStep.TabIndex = 8;
            this.ttMain.SetToolTip(this.bStep, "Выполнить пошагово F7");
            this.bStep.UseVisualStyleBackColor = true;
            this.bStep.Click += new System.EventHandler(this.пошаговоToolStripMenuItem_Click);
            // 
            // cbStartExecution
            // 
            this.cbStartExecution.Image = global::MKP.Printing.Properties.Resources.Go;
            this.cbStartExecution.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cbStartExecution.Location = new System.Drawing.Point(75, 27);
            this.cbStartExecution.Name = "cbStartExecution";
            this.cbStartExecution.Size = new System.Drawing.Size(85, 26);
            this.cbStartExecution.TabIndex = 8;
            this.cbStartExecution.Text = "Выполнить";
            this.cbStartExecution.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ttMain.SetToolTip(this.cbStartExecution, "Выполнить F9");
            this.cbStartExecution.UseVisualStyleBackColor = true;
            this.cbStartExecution.Click += new System.EventHandler(this.запуститьToolStripMenuItem_Click);
            // 
            // cbShowMap
            // 
            this.cbShowMap.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbShowMap.AutoSize = true;
            this.cbShowMap.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cbShowMap.Checked = true;
            this.cbShowMap.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbShowMap.Location = new System.Drawing.Point(835, 33);
            this.cbShowMap.Name = "cbShowMap";
            this.cbShowMap.Size = new System.Drawing.Size(120, 17);
            this.cbShowMap.TabIndex = 7;
            this.cbShowMap.Text = "Показывать карту";
            this.ttMain.SetToolTip(this.cbShowMap, "Показать карту скрипта слева");
            this.cbShowMap.UseVisualStyleBackColor = true;
            this.cbShowMap.CheckedChanged += new System.EventHandler(this.cbShowMap_CheckedChanged);
            // 
            // cbLineIndex
            // 
            this.cbLineIndex.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbLineIndex.AutoSize = true;
            this.cbLineIndex.Location = new System.Drawing.Point(707, 33);
            this.cbLineIndex.Name = "cbLineIndex";
            this.cbLineIndex.Size = new System.Drawing.Size(126, 17);
            this.cbLineIndex.TabIndex = 7;
            this.cbLineIndex.Text = "Нумеровать строки";
            this.ttMain.SetToolTip(this.cbLineIndex, "Нумеровать строки скрипта");
            this.cbLineIndex.UseVisualStyleBackColor = true;
            this.cbLineIndex.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // statusStripMain
            // 
            this.statusStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lbString,
            this.lbColumn,
            this.lbCompilerStatus});
            this.statusStripMain.Location = new System.Drawing.Point(0, 528);
            this.statusStripMain.Name = "statusStripMain";
            this.statusStripMain.Size = new System.Drawing.Size(1008, 22);
            this.statusStripMain.TabIndex = 4;
            // 
            // lbString
            // 
            this.lbString.Name = "lbString";
            this.lbString.Size = new System.Drawing.Size(48, 17);
            this.lbString.Text = "Строка:";
            // 
            // lbColumn
            // 
            this.lbColumn.Name = "lbColumn";
            this.lbColumn.Size = new System.Drawing.Size(52, 17);
            this.lbColumn.Text = "Столбец";
            // 
            // lbCompilerStatus
            // 
            this.lbCompilerStatus.Name = "lbCompilerStatus";
            this.lbCompilerStatus.Size = new System.Drawing.Size(119, 17);
            this.lbCompilerStatus.Text = "Готов к выполнению";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.файлToolStripMenuItem,
            this.редактироватьToolStripMenuItem,
            this.запускToolStripMenuItem,
            this.меткиToolStripMenuItem,
            this.поискToolStripMenuItem});
            this.menuStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1008, 24);
            this.menuStrip1.TabIndex = 6;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // файлToolStripMenuItem
            // 
            this.файлToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.сохранитьToolStripMenuItem2,
            this.выйтиToolStripMenuItem});
            this.файлToolStripMenuItem.Name = "файлToolStripMenuItem";
            this.файлToolStripMenuItem.Size = new System.Drawing.Size(45, 20);
            this.файлToolStripMenuItem.Text = "Файл";
            // 
            // сохранитьToolStripMenuItem2
            // 
            this.сохранитьToolStripMenuItem2.Name = "сохранитьToolStripMenuItem2";
            this.сохранитьToolStripMenuItem2.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.сохранитьToolStripMenuItem2.Size = new System.Drawing.Size(170, 22);
            this.сохранитьToolStripMenuItem2.Text = "Сохранить";
            this.сохранитьToolStripMenuItem2.Click += new System.EventHandler(this.сохранитьToolStripMenuItem1_Click);
            // 
            // выйтиToolStripMenuItem
            // 
            this.выйтиToolStripMenuItem.Name = "выйтиToolStripMenuItem";
            this.выйтиToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.выйтиToolStripMenuItem.Text = "Выйти";
            this.выйтиToolStripMenuItem.Click += new System.EventHandler(this.выйтиToolStripMenuItem_Click);
            // 
            // редактироватьToolStripMenuItem
            // 
            this.редактироватьToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.удалитьToolStripMenuItem,
            this.копироватьToolStripMenuItem,
            this.вырезатьToolStripMenuItem1,
            this.вставитьToolStripMenuItem});
            this.редактироватьToolStripMenuItem.Name = "редактироватьToolStripMenuItem";
            this.редактироватьToolStripMenuItem.Size = new System.Drawing.Size(98, 20);
            this.редактироватьToolStripMenuItem.Text = "Редактировать";
            // 
            // удалитьToolStripMenuItem
            // 
            this.удалитьToolStripMenuItem.Name = "удалитьToolStripMenuItem";
            this.удалитьToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.удалитьToolStripMenuItem.Text = "Удалить";
            // 
            // копироватьToolStripMenuItem
            // 
            this.копироватьToolStripMenuItem.Name = "копироватьToolStripMenuItem";
            this.копироватьToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.копироватьToolStripMenuItem.Text = "Копировать";
            // 
            // вырезатьToolStripMenuItem1
            // 
            this.вырезатьToolStripMenuItem1.Name = "вырезатьToolStripMenuItem1";
            this.вырезатьToolStripMenuItem1.Size = new System.Drawing.Size(138, 22);
            this.вырезатьToolStripMenuItem1.Text = "Вырезать";
            // 
            // вставитьToolStripMenuItem
            // 
            this.вставитьToolStripMenuItem.Name = "вставитьToolStripMenuItem";
            this.вставитьToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.вставитьToolStripMenuItem.Text = "Вставить";
            // 
            // запускToolStripMenuItem
            // 
            this.запускToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.запуститьToolStripMenuItem1,
            this.запуститьПошаговоToolStripMenuItem,
            this.запуститьДоКурсораToolStripMenuItem,
            this.запуститьПоМеткамToolStripMenuItem});
            this.запускToolStripMenuItem.Name = "запускToolStripMenuItem";
            this.запускToolStripMenuItem.Size = new System.Drawing.Size(87, 20);
            this.запускToolStripMenuItem.Text = "Выполнение";
            // 
            // запуститьToolStripMenuItem1
            // 
            this.запуститьToolStripMenuItem1.Name = "запуститьToolStripMenuItem1";
            this.запуститьToolStripMenuItem1.ShortcutKeys = System.Windows.Forms.Keys.F9;
            this.запуститьToolStripMenuItem1.Size = new System.Drawing.Size(215, 22);
            this.запуститьToolStripMenuItem1.Text = "Выполнить";
            this.запуститьToolStripMenuItem1.Click += new System.EventHandler(this.запуститьToolStripMenuItem_Click);
            // 
            // запуститьПошаговоToolStripMenuItem
            // 
            this.запуститьПошаговоToolStripMenuItem.Name = "запуститьПошаговоToolStripMenuItem";
            this.запуститьПошаговоToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F7;
            this.запуститьПошаговоToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.запуститьПошаговоToolStripMenuItem.Text = "Выполнить пошагово";
            this.запуститьПошаговоToolStripMenuItem.Click += new System.EventHandler(this.пошаговоToolStripMenuItem_Click);
            // 
            // запуститьДоКурсораToolStripMenuItem
            // 
            this.запуститьДоКурсораToolStripMenuItem.Name = "запуститьДоКурсораToolStripMenuItem";
            this.запуститьДоКурсораToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F4;
            this.запуститьДоКурсораToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.запуститьДоКурсораToolStripMenuItem.Text = "Выполнить до курсора";
            this.запуститьДоКурсораToolStripMenuItem.Click += new System.EventHandler(this.доКурсораToolStripMenuItem_Click);
            // 
            // запуститьПоМеткамToolStripMenuItem
            // 
            this.запуститьПоМеткамToolStripMenuItem.Name = "запуститьПоМеткамToolStripMenuItem";
            this.запуститьПоМеткамToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.запуститьПоМеткамToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.запуститьПоМеткамToolStripMenuItem.Text = "Выполнить по меткам";
            this.запуститьПоМеткамToolStripMenuItem.Click += new System.EventHandler(this.запуститьПоМеткамToolStripMenuItem_Click);
            // 
            // меткиToolStripMenuItem
            // 
            this.меткиToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.добавиттьМеткуToolStripMenuItem,
            this.удалитьМеткуToolStripMenuItem,
            this.удалитьВсеМеткиToolStripMenuItem,
            this.перейтиКСледующейМеткеToolStripMenuItem});
            this.меткиToolStripMenuItem.Name = "меткиToolStripMenuItem";
            this.меткиToolStripMenuItem.Size = new System.Drawing.Size(53, 20);
            this.меткиToolStripMenuItem.Text = "Метки";
            // 
            // добавиттьМеткуToolStripMenuItem
            // 
            this.добавиттьМеткуToolStripMenuItem.Name = "добавиттьМеткуToolStripMenuItem";
            this.добавиттьМеткуToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.B)));
            this.добавиттьМеткуToolStripMenuItem.Size = new System.Drawing.Size(266, 22);
            this.добавиттьМеткуToolStripMenuItem.Text = "Добавить метку";
            this.добавиттьМеткуToolStripMenuItem.Click += new System.EventHandler(this.добавиттьМеткуToolStripMenuItem_Click);
            // 
            // удалитьМеткуToolStripMenuItem
            // 
            this.удалитьМеткуToolStripMenuItem.Name = "удалитьМеткуToolStripMenuItem";
            this.удалитьМеткуToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.B)));
            this.удалитьМеткуToolStripMenuItem.Size = new System.Drawing.Size(266, 22);
            this.удалитьМеткуToolStripMenuItem.Text = "Удалить метку";
            this.удалитьМеткуToolStripMenuItem.Click += new System.EventHandler(this.добавиттьМеткуToolStripMenuItem_Click);
            // 
            // удалитьВсеМеткиToolStripMenuItem
            // 
            this.удалитьВсеМеткиToolStripMenuItem.Name = "удалитьВсеМеткиToolStripMenuItem";
            this.удалитьВсеМеткиToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.B)));
            this.удалитьВсеМеткиToolStripMenuItem.Size = new System.Drawing.Size(266, 22);
            this.удалитьВсеМеткиToolStripMenuItem.Text = "Удалить все метки";
            this.удалитьВсеМеткиToolStripMenuItem.Click += new System.EventHandler(this.удалитьВсеМеткиToolStripMenuItem_Click);
            // 
            // перейтиКСледующейМеткеToolStripMenuItem
            // 
            this.перейтиКСледующейМеткеToolStripMenuItem.Name = "перейтиКСледующейМеткеToolStripMenuItem";
            this.перейтиКСледующейМеткеToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.перейтиКСледующейМеткеToolStripMenuItem.Size = new System.Drawing.Size(266, 22);
            this.перейтиКСледующейМеткеToolStripMenuItem.Text = "Перейти к следующей метке";
            this.перейтиКСледующейМеткеToolStripMenuItem.Click += new System.EventHandler(this.перейтиКСледующейМеткеToolStripMenuItem_Click);
            // 
            // поискToolStripMenuItem
            // 
            this.поискToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.найтиToolStripMenuItem1,
            this.найттиИЗаменитьToolStripMenuItem,
            this.найтиДалееToolStripMenuItem});
            this.поискToolStripMenuItem.Name = "поискToolStripMenuItem";
            this.поискToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.поискToolStripMenuItem.Text = "Поиск";
            // 
            // найтиToolStripMenuItem1
            // 
            this.найтиToolStripMenuItem1.Name = "найтиToolStripMenuItem1";
            this.найтиToolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.F)));
            this.найтиToolStripMenuItem1.Size = new System.Drawing.Size(209, 22);
            this.найтиToolStripMenuItem1.Text = "Найти";
            this.найтиToolStripMenuItem1.Click += new System.EventHandler(this.найтиToolStripMenuItem1_Click);
            // 
            // найттиИЗаменитьToolStripMenuItem
            // 
            this.найттиИЗаменитьToolStripMenuItem.Name = "найттиИЗаменитьToolStripMenuItem";
            this.найттиИЗаменитьToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
            this.найттиИЗаменитьToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.найттиИЗаменитьToolStripMenuItem.Text = "Найти и заменить";
            this.найттиИЗаменитьToolStripMenuItem.Click += new System.EventHandler(this.найттиИЗаменитьToolStripMenuItem_Click);
            // 
            // найтиДалееToolStripMenuItem
            // 
            this.найтиДалееToolStripMenuItem.Name = "найтиДалееToolStripMenuItem";
            this.найтиДалееToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F3;
            this.найтиДалееToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.найтиДалееToolStripMenuItem.Text = "Найти далее";
            this.найтиДалееToolStripMenuItem.Click += new System.EventHandler(this.найтиДалееToolStripMenuItem_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.dgVariables);
            this.splitContainer1.Panel1.Controls.Add(this.tbTextParam);
            this.splitContainer1.Panel1.Controls.Add(this.cbArrays);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dgArray);
            this.splitContainer1.Size = new System.Drawing.Size(1002, 132);
            this.splitContainer1.SplitterDistance = 413;
            this.splitContainer1.TabIndex = 4;
            // 
            // dgVariables
            // 
            this.dgVariables.AllowUserToResizeRows = false;
            this.dgVariables.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgVariables.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.dgVariables.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgVariables.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.name,
            this.Value});
            this.dgVariables.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgVariables.Location = new System.Drawing.Point(3, 36);
            this.dgVariables.Name = "dgVariables";
            this.dgVariables.RowHeadersVisible = false;
            this.dgVariables.Size = new System.Drawing.Size(403, 94);
            this.dgVariables.TabIndex = 2;
            this.dgVariables.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellEndEdit);
            // 
            // name
            // 
            this.name.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.name.HeaderText = "Имя переменной";
            this.name.Name = "name";
            // 
            // Value
            // 
            this.Value.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Value.HeaderText = "Значение";
            this.Value.Name = "Value";
            // 
            // tbTextParam
            // 
            this.tbTextParam.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.tbTextParam.Location = new System.Drawing.Point(3, 10);
            this.tbTextParam.Name = "tbTextParam";
            this.tbTextParam.Size = new System.Drawing.Size(198, 20);
            this.tbTextParam.TabIndex = 0;
            this.tbTextParam.Visible = false;
            this.tbTextParam.TextChanged += new System.EventHandler(this.tbTextParam_TextChanged);
            // 
            // cbArrays
            // 
            this.cbArrays.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbArrays.FormattingEnabled = true;
            this.cbArrays.Location = new System.Drawing.Point(207, 9);
            this.cbArrays.Name = "cbArrays";
            this.cbArrays.Size = new System.Drawing.Size(201, 21);
            this.cbArrays.TabIndex = 3;
            this.cbArrays.SelectedIndexChanged += new System.EventHandler(this.cbArrays_SelectedIndexChanged);
            // 
            // dgArray
            // 
            this.dgArray.AllowUserToAddRows = false;
            this.dgArray.AllowUserToDeleteRows = false;
            this.dgArray.AllowUserToResizeRows = false;
            this.dgArray.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgArray.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.dgArray.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgArray.Location = new System.Drawing.Point(4, 4);
            this.dgArray.Name = "dgArray";
            this.dgArray.RowHeadersWidth = 50;
            this.dgArray.Size = new System.Drawing.Size(576, 122);
            this.dgArray.TabIndex = 1;
            // 
            // сохранитьToolStripMenuItem
            // 
            this.сохранитьToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.сохранитьToolStripMenuItem1});
            this.сохранитьToolStripMenuItem.Name = "сохранитьToolStripMenuItem";
            this.сохранитьToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.сохранитьToolStripMenuItem.Text = "Файл";
            // 
            // сохранитьToolStripMenuItem1
            // 
            this.сохранитьToolStripMenuItem1.Name = "сохранитьToolStripMenuItem1";
            this.сохранитьToolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.сохранитьToolStripMenuItem1.Size = new System.Drawing.Size(170, 22);
            this.сохранитьToolStripMenuItem1.Text = "Сохранить";
            this.сохранитьToolStripMenuItem1.Click += new System.EventHandler(this.сохранитьToolStripMenuItem1_Click);
            // 
            // компиляторToolStripMenuItem
            // 
            this.компиляторToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.запуститьToolStripMenuItem,
            this.пошаговоToolStripMenuItem,
            this.игнорироватьToolStripMenuItem,
            this.доКурсораToolStripMenuItem});
            this.компиляторToolStripMenuItem.Name = "компиляторToolStripMenuItem";
            this.компиляторToolStripMenuItem.Size = new System.Drawing.Size(88, 20);
            this.компиляторToolStripMenuItem.Text = "Компилятор";
            // 
            // запуститьToolStripMenuItem
            // 
            this.запуститьToolStripMenuItem.Name = "запуститьToolStripMenuItem";
            this.запуститьToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.запуститьToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.запуститьToolStripMenuItem.Text = "Запустить";
            this.запуститьToolStripMenuItem.Click += new System.EventHandler(this.запуститьToolStripMenuItem_Click);
            // 
            // пошаговоToolStripMenuItem
            // 
            this.пошаговоToolStripMenuItem.Name = "пошаговоToolStripMenuItem";
            this.пошаговоToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F7;
            this.пошаговоToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.пошаговоToolStripMenuItem.Text = "Пошагово";
            this.пошаговоToolStripMenuItem.Click += new System.EventHandler(this.пошаговоToolStripMenuItem_Click);
            // 
            // игнорироватьToolStripMenuItem
            // 
            this.игнорироватьToolStripMenuItem.Name = "игнорироватьToolStripMenuItem";
            this.игнорироватьToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F10;
            this.игнорироватьToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.игнорироватьToolStripMenuItem.Text = "Игнорировать";
            // 
            // доКурсораToolStripMenuItem
            // 
            this.доКурсораToolStripMenuItem.Name = "доКурсораToolStripMenuItem";
            this.доКурсораToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.доКурсораToolStripMenuItem.Text = "До курсора";
            this.доКурсораToolStripMenuItem.Click += new System.EventHandler(this.доКурсораToolStripMenuItem_Click);
            // 
            // FMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 688);
            this.Controls.Add(this.MainSplitter);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FMain";
            this.Text = "Скрипт печатной формы";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FMain_FormClosing);
            this.Load += new System.EventHandler(this.FMain_Load);
            this.VisibleChanged += new System.EventHandler(this.FMain_VisibleChanged);
            this.MainSplitter.Panel1.ResumeLayout(false);
            this.MainSplitter.Panel1.PerformLayout();
            this.MainSplitter.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MainSplitter)).EndInit();
            this.MainSplitter.ResumeLayout(false);
            this.TopSplitter.Panel1.ResumeLayout(false);
            this.TopSplitter.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TopSplitter)).EndInit();
            this.TopSplitter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tbMain)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.statusStripMain.ResumeLayout(false);
            this.statusStripMain.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgVariables)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgArray)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer MainSplitter;
        private FastColoredTextBoxNS.DocumentMap documentMap1;
        private System.Windows.Forms.StatusStrip statusStripMain;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuCopy;
        private System.Windows.Forms.ToolStripMenuItem menuCut;
        private System.Windows.Forms.ToolStripMenuItem menuPaste;
        private System.Windows.Forms.ToolStripSeparator menuMacroSplit;
        private System.Windows.Forms.ToolStripMenuItem menuKomandKonstr;
        private System.Windows.Forms.ToolStripMenuItem menuFunkc;
        private System.Windows.Forms.ToolStripMenuItem menuEXCEL;
        private System.Windows.Forms.ToolStripMenuItem menuShirina;
        private System.Windows.Forms.ToolStripMenuItem menuVisota;
        private System.Windows.Forms.ToolStripMenuItem menuTextToCell;
        private System.Windows.Forms.ToolStripMenuItem menuFont;
        private System.Windows.Forms.ToolStripMenuItem menuMerge;
        private System.Windows.Forms.ToolStripMenuItem menuInCell;
        private System.Windows.Forms.ToolStripMenuItem menuShrink;
        private System.Windows.Forms.ToolStripMenuItem menuLine;
        private System.Windows.Forms.ToolStripMenuItem menuFormat;
        private System.Windows.Forms.ToolStripMenuItem menuTextWrap;
        private System.Windows.Forms.ToolStripMenuItem menuTextMove;
        private System.Windows.Forms.ToolStripMenuItem textMoveCancel;
        private System.Windows.Forms.ToolStripMenuItem menuFillCell;
        private System.Windows.Forms.ToolStripMenuItem menuPhotoToCell;
        private System.Windows.Forms.ToolStripMenuItem menuGerbToCell;
        private System.Windows.Forms.ToolStripMenuItem menuWord;
        private System.Windows.Forms.ToolStripMenuItem пToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem числоToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem символToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem переченьПеременныхToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem числоToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem датаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem датаToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem еслиИначеКонецToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem циклКонецToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem покаКонецToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem очиститьМассивToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem запросSELECTFROMToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem параметрЗапросаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem открытьЗапроскудаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сортироватьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem колонкиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem колонокToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem строкиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem строкToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem автоподборСтрокToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem размерШрифтаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem названиеШрифтаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem жирностьШрифтаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem подчеркиваниеШрифтаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem наклонностьШрифтаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem строкиToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem колонкиToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem ячейкиToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem строкиToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem колонкиToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem ячейкиToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem строкиToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem колонкиToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem ячейкиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem строкмассивКоличествоСтрокВМассивеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem столбцовмассивКоличествоСтолбцовВМассивеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem удалениеПробеловСправаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem удалениеПробеловСлеваToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem удалениеПробеловСправаИСлеваToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem округлитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem найтиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem вырезатьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem длиннаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem количествоДнейМеждуДатамиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem строкаВВидеСтрокаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem строкаВВидеСТРОКАToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem строкаВВидестрокаToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem датаToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem датаПрописьюToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem деньгиПрописьюToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem создатьEXCELToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem книгаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem альбомToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem открытьEXCELToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem добавитьЛистовToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem названиеЛистаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem текущийЛистToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem поляЛистаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ориентацияЛистаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem книжнаяToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem альбомнаяToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сохранитьEXCELToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem работаEXCELToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem форматA3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem форматА4ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem форматА5ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem заголовокСтраницToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem верхнийКолонтитулToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem слеваToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem поЦентруToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem справаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem нижнийКолонтитутToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem слеваToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem поЦентруToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem справаToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem центрироватьНаСтраницеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem горизонтальноToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem вертикальноToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem разрывСтраницыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem добавитьСтрокуEXCELToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem строкиToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem колонкиToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem ячейкиToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem строкиToolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem колонкиToolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem ячейкиToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem строкиToolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem жирныйToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem отменаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem колонкиToolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem жирныйToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem отменаToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem ячейкиToolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem жирныйToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem отменаToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem строкиToolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem тонкойЛиниейToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem толстойЛиниейToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem отменаToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem колонкиToolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem тонкойЛиниейToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem толстойЛиниейToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem отменаToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem ячейкиToolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem строкиToolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem колонкиToolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem ячейкиToolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem тонкойЛиниейToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem толстойЛиниейToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem отменаToolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem наклонныйToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem отменаToolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem наклонныйToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem отменаToolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem наклонныйToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem отменаToolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem размерСимволовToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem жирностьСимволовToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem жирныйToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem отменаToolStripMenuItem9;
        private System.Windows.Forms.ToolStripMenuItem наклонностьСимволовToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem наклонныйToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem отменаToolStripMenuItem10;
        private System.Windows.Forms.ToolStripMenuItem подчеркиваниеСимволовToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem тонкойЛиниейToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem толстойЛиниейToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem отменаToolStripMenuItem11;
        private System.Windows.Forms.ToolStripMenuItem поГоризонталиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem строкиToolStripMenuItem9;
        private System.Windows.Forms.ToolStripMenuItem поЗначениюToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem поЛевомуКраюToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem поЦентруToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem поПравомуКраюToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem поШиринеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem колонкиToolStripMenuItem9;
        private System.Windows.Forms.ToolStripMenuItem ячейкиToolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem поВертикалиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem строкиToolStripMenuItem10;
        private System.Windows.Forms.ToolStripMenuItem колонкиToolStripMenuItem10;
        private System.Windows.Forms.ToolStripMenuItem ячейкиToolStripMenuItem9;
        private System.Windows.Forms.ToolStripMenuItem поЗначениюToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem поЗначениюToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem9;
        private System.Windows.Forms.ToolStripMenuItem поВерхнемуКраюToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem поЦентруToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem поНижнемуКраюToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem поВысотеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem поВерхнемуКраюToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem10;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem11;
        private System.Windows.Forms.ToolStripMenuItem поВерхнемуКраюToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem12;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem13;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem14;
        private System.Windows.Forms.ToolStripMenuItem видЛинииToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem всейЯчейкиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem нетToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem толстаяToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem тонкаяToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem двойнаяToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem пунктирнаяToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem точкамиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem вокругЯчейкиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem нетToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem21;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem22;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem23;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem24;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem25;
        private System.Windows.Forms.ToolStripMenuItem сверхуЯчейкиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem нетToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem26;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem27;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem28;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem29;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem30;
        private System.Windows.Forms.ToolStripMenuItem слеваЯчейкиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem нетToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem31;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem32;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem33;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem34;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem35;
        private System.Windows.Forms.ToolStripMenuItem снизуЯчейкиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem нетToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem36;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem37;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem38;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem39;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem40;
        private System.Windows.Forms.ToolStripMenuItem справаЯчейкиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem нетToolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem41;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem42;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem43;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem44;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem45;
        private System.Windows.Forms.ToolStripMenuItem толщинаЛинииToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem46;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem48;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem49;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem50;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem51;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem53;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem52;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem54;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem55;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem56;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem60;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem66;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem57;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem58;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem59;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem67;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem73;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem61;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem62;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem63;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem74;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem80;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem64;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem65;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem68;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem81;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem87;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem69;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem70;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem71;
        private System.Windows.Forms.ToolStripMenuItem строкиToolStripMenuItem11;
        private System.Windows.Forms.ToolStripMenuItem общийToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem числовойToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem текстовыйToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem датаToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem колонкиToolStripMenuItem11;
        private System.Windows.Forms.ToolStripMenuItem общийToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem15;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem16;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem17;
        private System.Windows.Forms.ToolStripMenuItem ячейкиToolStripMenuItem10;
        private System.Windows.Forms.ToolStripMenuItem общийToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem18;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem19;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem20;
        private System.Windows.Forms.ToolStripMenuItem menuMacro;
        private System.Windows.Forms.ToolStripMenuItem menuMacroForm;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem открытьWORDToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem заменитьВWORDeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem соранитьWORDToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem сохранитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сохранитьToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem компиляторToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem запуститьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem пошаговоToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem игнорироватьToolStripMenuItem;
        private System.Windows.Forms.TextBox tbTextParam;
        private System.Windows.Forms.ToolStripMenuItem доКурсораToolStripMenuItem;
        private System.Windows.Forms.ComboBox cbArrays;
        private System.Windows.Forms.DataGridView dgVariables;
        private System.Windows.Forms.DataGridView dgArray;
        private System.Windows.Forms.DataGridViewTextBoxColumn name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Value;
        private System.Windows.Forms.CheckBox cbLineIndex;
        private System.Windows.Forms.Button cbStartExecution;
        private System.Windows.Forms.ToolStripMenuItem файлToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сохранитьToolStripMenuItem2;
        private System.Windows.Forms.ToolStripStatusLabel lbString;
        private System.Windows.Forms.ToolStripStatusLabel lbColumn;
        private System.Windows.Forms.ToolStripMenuItem запускToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem запуститьToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem запуститьПошаговоToolStripMenuItem;
        private System.Windows.Forms.Button cbStopExecution;
        private System.Windows.Forms.ToolStripMenuItem запуститьДоКурсораToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem запуститьПоМеткамToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel lbCompilerStatus;
        private System.Windows.Forms.ToolTip ttMain;
        private System.Windows.Forms.Button bSave;
        private System.Windows.Forms.Button bCursor;
        private System.Windows.Forms.Button bStops;
        private System.Windows.Forms.Button bStep;
        private System.Windows.Forms.ToolStripMenuItem меткиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem добавиттьМеткуToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem удалитьМеткуToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem удалитьВсеМеткиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem поискToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem найтиToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem найттиИЗаменитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem найтиДалееToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem перейтиКСледующейМеткеToolStripMenuItem;
        private System.Windows.Forms.CheckBox cbShowMap;
        private System.Windows.Forms.ToolStripMenuItem выйтиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem редактироватьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem удалитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem копироватьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem вырезатьToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem вставитьToolStripMenuItem;
        private System.Windows.Forms.SplitContainer TopSplitter;
        private FastColoredTextBoxNS.FastColoredTextBox tbMain;
        private System.Windows.Forms.ToolStripMenuItem создатьOPENToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem книгаToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem альбомToolStripMenuItem1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ToolStripMenuItem линияПоКоординатамToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem скрытьСтолбецToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem скрытьСтрокуToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem процедураОбьединенияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem пToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem столбцыДляСравненияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem выполнитьОбьединениеToolStripMenuItem;
        private System.Windows.Forms.ComboBox cbDebug;
        private System.Windows.Forms.TextBox tbFind;
        private System.Windows.Forms.ToolStripMenuItem выводТаблицыНачинаяСToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem левыйВерхнийУголToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem левыйВерхнийУголСИтогамиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem выводШрихкодаВЯчейкуToolStripMenuItem;
    }
}

