﻿using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace MKP.Printing.Helper
{
    public class GroupResult
    {
        public object FieldName { get; set; }
        public object Key { get; set; }

        public int Count { get; set; }

        public List<DataRow> Items { get; set; }

        public List<GroupResult> SubGroups { get; set; }

        public List<KeyValuePair<DataRow, DataRow>> listSummaryRows
        {
            get
            {
                var rez = new List<KeyValuePair<DataRow, DataRow>>();
                if (SubGroups != null)
                    foreach (var item in SubGroups)
                    {
                        rez.Add(item.SummaryRow);
                    }
                return rez;
            }
        }

        KeyValuePair<DataRow, DataRow> SummaryRow
        {
            get
            {
                if (Items.Count <= 1)
                    return new KeyValuePair<DataRow, DataRow>();
                DataRow rezult = Items.First().Table.NewRow();
                var dtTmp = Items.First().Table.Copy();
                dtTmp.Rows.Clear();
                foreach (var item in Items)
                {
                    dtTmp.Rows.Add(item.ItemArray);
                }
                var column = dtTmp.Columns.Cast<DataColumn>().Where(i => i.Prefix == "DataArea").Select(i => i).ToArray();
                DataRow rw = Items.First().Table.NewRow();
                foreach (var col in column)
                {
                    object sum = col.DataType == typeof(double) ? dtTmp.Compute($"avg({col.ColumnName})", "") : dtTmp.Compute($"SUM({col.ColumnName})", "");
                    rw.SetField(col.ColumnName, sum);
                }
                column = dtTmp.Columns.Cast<DataColumn>().Where(i => i.Prefix == "RowArea").Select(i => i).ToArray();
                bool summaryColumn = false;
                foreach (var col in column)
                {
                    if (!summaryColumn)
                    {
                        summaryColumn = col.ColumnName == (string)FieldName;
                        if (!summaryColumn)
                            rw.SetField(col.ColumnName, Items.First().Field<string>(col.ColumnName));
                    }
                    else
                        rw.SetField(col.ColumnName, $"Всего {Key}");
                }
                rezult.ItemArray = rw.ItemArray;
                var rez = new KeyValuePair<DataRow, DataRow>(Items.Last(), rezult);
                return rez;
            }
        }
    }
}
