﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace MKP.Printing.Helper
{
    static class DataTableExtensions
    {
        public static object GetValue(this DataTable dt,int rowIndex,int columnIndex)
        {
            return dt.Rows[rowIndex][columnIndex] ?? "";
        }
        public static object[,] ToArray(this DataTable data)
        {
            var ret = Array.CreateInstance(typeof(object), data.Rows.Count+1, data.Columns.Count) as object[,];
            int r = 0;
            int col = 0;
            foreach (DataColumn item in data.Columns)
            {
                
                ret[r, col] = item.Caption;
                col++;
            }
            r++;
            foreach (DataRow row in data.Rows)
            {
                col = 0;
                foreach (DataColumn item in data.Columns)
                {
                    ret[r, col] = row[item.ColumnName];
                    col++;
                }
                r++;
            }
            return ret;
        }
        public static IEnumerable<GroupResult> GroupByMany(this IEnumerable<DataRow> elements, params string[] groupSelectors)
        {
            if (groupSelectors.Length > 0)
            {
                var selector = groupSelectors.First();
                Func<DataRow, object> funcSelector = i => i.Field<string>(selector);
                var nextSelectors = groupSelectors.Skip(1).ToArray();
                return elements.GroupBy(funcSelector).Select(
                    g => new GroupResult
                    {
                        FieldName = selector,
                        Key = g.Key,
                        Count = g.Count(),
                        Items = g.ToList(),
                        SubGroups = nextSelectors.Length > 0 ? g.GroupByMany(nextSelectors).ToList() : null
                    });
            }
            return null;
        }
    }
}
