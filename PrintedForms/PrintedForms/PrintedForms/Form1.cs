﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FastColoredTextBoxNS;
using FirebirdSql.Data.FirebirdClient;
using System.Text.RegularExpressions;
using System.Drawing.Drawing2D;
using System.Globalization;
using Microsoft.Win32;
/*
Заходит однажды тестировщик в бар.

Забегает в бар.
Пролезает в бар.
Танцуя, проникает в бар.
Крадется в бар.
Врывается в бар.
Прыгает в бар

и заказывает:
кружку пива,
2 кружки пива,
0 кружек пива,
999999999 кружек пива,
ящерицу в стакане,
–1 кружку пива,
qwertyuip кружек пива.
 */
namespace MKP.Printing
{
    public partial class FMain : Form
    {
        #region Custom Styles
        MarkerStyle ErrorLineBrush = new MarkerStyle(new SolidBrush(Color.FromArgb(110, Color.Red)));
        MarkerStyle CurrentLineBrush = new MarkerStyle(new SolidBrush(Color.FromArgb(115, Color.Yellow)));
        Style KeywordsStyle = new TextStyle(Brushes.DarkGreen, null, FontStyle.Regular);
        Style SpecMacroStyle = new TextStyle(Brushes.Purple, null, FontStyle.Regular);
        Style NumberStyle = new TextStyle(Brushes.SaddleBrown, null, FontStyle.Regular);
        Style StringStyle = new TextStyle(Brushes.Blue, null, FontStyle.Regular);
        Style CommentStyle = new TextStyle(Brushes.Tomato, null, FontStyle.Regular);
        Style BoldStyle = new TextStyle(null, null, FontStyle.Bold);
        Style BoldСolorStyle = new TextStyle(Brushes.DarkBlue, null, FontStyle.Bold);
        MarkerStyle SameWordsStyle = new MarkerStyle(new SolidBrush(Color.FromArgb(40, Color.Gray))); 
        #endregion
        
        Compiler cmp = null;
        private int ErrorLine = -1;
       
       // private string connectionStringBDD = @"User = SYSDBA;Password = masterkey;Database = D:\work\BDD;DataSource = 127.0.0.1;Port = 3050;Dialect = 3;Charset = WIN1251;Role =;Connection lifetime = 15;Pooling = true;MinPoolSize = 0;MaxPoolSize = 50;Packet Size = 8192;ServerType = 0";
        private string connectionStringBDD = @"User = MKP;Password = 17051945;Database = E:\BD\BDD;DataSource = 127.0.0.1;Port = 3050;Dialect = 3;Charset = WIN1251;Role =;Connection lifetime = 15;Pooling = true;MinPoolSize = 0;MaxPoolSize = 50;Packet Size = 8192;ServerType = 0";
        private string ID;        
        //public SpecialMacro[] specMacro = new SpecialMacro[0];
        public List<SpecialMacro> specMacro = new List<SpecialMacro>();
        private int? currLine = null;
        private List<int> stops = new List<int>();
        public Dictionary<int, string> stopsDict = new Dictionary<int, string>();
        private FindForm ff;
        private ReplaceForm rf;
        Style wavyStyle = new WavyLineStyle(255, Color.Red);

        private bool Saved = true;
      
        public bool cmdPoint = false;
        public FMain()
        {
            SetUp();
            Saved = true;
            //splitContainer1.BackColor = SystemColors.GrayText;
        }


        public FMain(string ID, List<SpecialMacro> args, string connection)
        {
            connectionStringBDD = connection;
            SetUp();
            this.ID = ID;
            customizeMacro(ID, args);
            Saved = true;
        }

        public FMain(string ID, List<SpecialMacro> args, string connection, string zagolovok)
        {
            connectionStringBDD = connection;
            SetUp();
            this.ID = ID;
            customizeMacro(ID, args);
            Saved = true;
            this.Text = this.Text + ": " + zagolovok;
            
        }

        private void SetUp()
        {
            InitializeComponent();

            //ПОРЯДОК ОТОБРАЖЕНИЯ СТИЛЕЙ
            tbMain.AddStyle(ErrorLineBrush);
            tbMain.AddStyle(CurrentLineBrush);
            tbMain.AddStyle(CommentStyle);
            tbMain.AddStyle(StringStyle);
            tbMain.AddStyle(BoldСolorStyle);
            tbMain.AddStyle(KeywordsStyle);
            tbMain.AddStyle(SpecMacroStyle);
            tbMain.AddStyle(BoldStyle);

            tbMain.LeftBracket = '\x0';
            tbMain.LeftBracket2 = '\x0';
            tbMain.RightBracket2 = '\x0';
            tbMain.RightBracket = '\x0';

            ReDrawLines();
        }

        public string GetConnectionString()
        {
            return connectionStringBDD;
        }

        private void customizeMacro(string ID, List<SpecialMacro> args)
        {
            using (FbConnection SelectConnection = new FbConnection(connectionStringBDD))
            {
                SelectConnection.Open();
                FbCommand command = new FbCommand("select formi8 from formi where formi1 = @m0", SelectConnection);
                command.Parameters.Add("@m0", ID); // код 
                FbDataReader r = command.ExecuteReader();
                if(r.Read())
                {
                    if (!(r["formi8"] is DBNull))
                    {
                        string str = System.Text.Encoding.Default.GetString((byte[])r["formi8"]);
                        tbMain.Text = str;
                    }
                    else
                    {
                        tbMain.Text = "//пустой документ";
                    }
                }
            }
            specMacro = args;
            if (specMacro.Count() > 0)
            {
                for (int i = 0; i < specMacro.Count(); i++)
                {
                    ToolStripItem it = new ToolStripMenuItem(specMacro[i].ShortText);
                    string sp = specMacro[i].ShortText;
                    it.Click += (s, e) => {tbMain.InsertText(sp, true); ; };
                    menuMacroForm.DropDownItems.Add(it);
                }
            }
        }

        private void ReDrawLines()
        {
            for (int i = 0; i < tbMain.Lines.Count; i++)
            {
                tbMain_TextChanged(new object(), new TextChangedEventArgs(new Range(tbMain,
                    0, i, tbMain.Lines[i].Length, i)));
            }
        }

        private void menuCopy_Click(object sender, EventArgs e)
        {
            tbMain.Copy();
        }

        private void menuCut_Click(object sender, EventArgs e)
        {
            tbMain.Cut();
        }

        private void menuPaste_Click(object sender, EventArgs e)
        {
            tbMain.Paste();
        }

        private void tbMain_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                if (tbMain.Selection.Text == "")
                    tbMain.Selection.Start = tbMain.PointToPlace(new Point(e.X, e.Y));
            }
            else
            {
                int a = tbMain.PointToPlace(new Point(e.X, e.Y)).iLine;
                bool isInList = stops.Contains(a);
                if (e.X < tbMain.LeftPadding + ((cbLineIndex.Checked) ? 20 : 0) - 10 && e.Button == MouseButtons.Left)
                {
                    if (!isInList)
                    {
                        stops.Add(a);
                        stopsDict.Add(a, "");
                        isInList = true;
                    }
                    else
                    {
                        stops.Remove(a);
                        stopsDict.Remove(a);
                        isInList = false;
                    }
                    tbMain.Invalidate();
                    tbMain.Selection = new Range(tbMain, 0, a, 0, a);
                }
                if (isInList)
                {
                    tbTextParam.Visible = true;
                    tbTextParam.Text = stopsDict[a];
                }
                else
                {
                    tbTextParam.Visible = false;
                }
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (cbLineIndex.Checked == true)
                tbMain.ShowLineNumbers = true;
            else
                tbMain.ShowLineNumbers = false;
        }

        private void tbMain_SelectionChanged(object sender, EventArgs e)
        {
            lbColumn.Text = "Столбец: " + (tbMain.Selection.Bounds.iStartChar + 1).ToString();
            lbString.Text = "Строка: " + (tbMain.Selection.Bounds.iStartLine + 1).ToString();
        }

        #region right-click menu
        private void числоToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbMain.Lines[tbMain.Selection.Start.iLine]))
            {
                tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
                tbMain.InsertText("перечень массивов m1,m2,... число\n", true);
            }
            else
                tbMain.InsertText("перечень массивов m1,m2,... число", true);
        }

        private void символToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbMain.Lines[tbMain.Selection.Start.iLine]))
            {
                tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
                tbMain.InsertText("перечень массивов m1,m2,... символ\n", true);
            }
            else
                tbMain.InsertText("перечень массивов m1,m2,... символ", true);
        }

        private void еслиИначеКонецToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbMain.Lines[tbMain.Selection.Start.iLine]))
            {
                tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
                tbMain.InsertText("если (условие) \n \nиначе \n \nконец\n", true);
            }
            else
                tbMain.InsertText("если (условие) \n \nиначе \n \nконец", true);
        }

        private void циклКонецToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbMain.Lines[tbMain.Selection.Start.iLine]))
            {
                tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
                tbMain.InsertText("цикл = до\n \n конец\n", true);
            }
            else
                tbMain.InsertText("цикл = до\n \n конец", true);
        }

        private void покаКонецToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbMain.Lines[tbMain.Selection.Start.iLine]))
            {
                tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
                tbMain.InsertText("пока\n \n конец\n", true);
            }
            else
                tbMain.InsertText("пока\n \n конец", true);
        }

        private void очиститьМассивToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbMain.Lines[tbMain.Selection.Start.iLine]))
            {
                tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
                tbMain.InsertText("ОчиститьМассив(массив)\n", true);
            }
            else
                tbMain.InsertText("ОчиститьМассив(массив)", true);

        }

        private void запросSELECTFROMToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbMain.Lines[tbMain.Selection.Start.iLine]))
            {
                tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
                tbMain.InsertText("Запрос(\"имя запроса   или   select ... from ...\")\n", true);
            }
            else
                tbMain.InsertText("Запрос(\"имя запроса   или   select ... from ...\")", true);
            
        }

        private void параметрЗапросаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbMain.Lines[tbMain.Selection.Start.iLine]))
            {
                tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
                tbMain.InsertText("ПараметрЗапроса[0]=...\n", true);
            }
            else
                tbMain.InsertText("ПараметрЗапроса[0]=...", true);

            
        }

        private void открытьЗапроскудаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbMain.Lines[tbMain.Selection.Start.iLine]))
            {
                tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
                tbMain.InsertText("ОткрытьЗапрос(массив или переменная)\n", true);
            }
            else tbMain.InsertText("ОткрытьЗапрос(массив или переменная)", true);
        }

        private void сортироватьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbMain.Lines[tbMain.Selection.Start.iLine]))
            {
                tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
                tbMain.InsertText("Сортировать(массив; с какой строки сортировать 0...; № поля; № поля... )\n", true);
            }
            else
                tbMain.InsertText("Сортировать(массив; с какой строки сортировать 0...; № поля; № поля... )", true);
        }

        private void строкмассивКоличествоСтрокВМассивеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.InsertText("Строк(массив)\n", true); 
        }

        private void столбцовмассивКоличествоСтолбцовВМассивеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.InsertText("Столбцов(массив)\n", true); 
        }

        private void удалениеПробеловСправаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.InsertText("УдалПробСправа(переменная)\n", true); 
        }

        private void удалениеПробеловСлеваToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.InsertText("УдалПробСлева(переменная)\n", true); 
        }

        private void удалениеПробеловСправаИСлеваToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.InsertText("УдалПробСпрСлева(переменная)\n", true); 
        }

        private void округлитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.InsertText("Округлить(...;0)\n", true); 
        }

        private void найтиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.InsertText("Найти(подстрока ; строка)\n", true); 
        }

        private void вырезатьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.InsertText("Вырезать(...;1;n)\n", true); 
        }

        private void длиннаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.InsertText("Длина()\n", true); 
        }

        private void количествоДнейМеждуДатамиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.InsertText("КолДней(дата;дата)\n", true); 
        }

        private void строкаВВидеСтрокаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.InsertText("Строка(...)\n", true); 
        }

        private void строкаВВидеСТРОКАToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            tbMain.InsertText("СТРОКА(...)\n", true); 
        }

        private void строкаВВидестрокаToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            tbMain.InsertText("строка(...)\n", true); 
        }

        private void датаToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            tbMain.InsertText("Дата\n", true); 
        }

        private void датаПрописьюToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.InsertText("ДатаПрописью(01.01.2008)\n", true); 
        }

        private void сToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.InsertText("С новой строки\n", true); 
        }

        private void деньгиПрописьюToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.InsertText("ДеньгиПрописью(переменная ; рубль,рубля,рублей ; копейка,копейки,копеек)\n", true); 
        }

        private void книгаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Создать книгу EXCEL (название_листа; поле_слева ; поле_справа ; поле_сверху ; поле_снизу)\n", true);
        }

        private void альбомToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbMain.Lines[tbMain.Selection.Start.iLine]))
            {
                tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
                tbMain.InsertText("Создать альбом EXCEL (название листа; поле_слева ; поле_справа ; поле_сверху ; поле_снизу)\n", true);
            }
            else
                tbMain.InsertText("Создать альбом EXCEL (название листа; поле_слева ; поле_справа ; поле_сверху ; поле_снизу)", true);
        }

        private void открытьEXCELToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbMain.Lines[tbMain.Selection.Start.iLine]))
            {
                tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
                tbMain.InsertText("Открыть EXCEL (имя_папки/имя_файла)\n", true);
            }
            else
                tbMain.InsertText("Открыть EXCEL (имя_папки/имя_файла)", true);
        }

        private void добавитьЛистовToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbMain.Lines[tbMain.Selection.Start.iLine]))
            {
                tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
                tbMain.InsertText("Добавить листов (количество дополнительных листов)\n", true);
            }
            else
                tbMain.InsertText("Добавить листов (количество дополнительных листов)", true);
        }

        private void названиеЛистаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbMain.Lines[tbMain.Selection.Start.iLine]))
            {
                tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
                tbMain.InsertText("Название листа (№ листа; название листа)\n", true);
            }
            else
                tbMain.InsertText("Название листа (№ листа; название листа)\n", true);
        }

        private void текущийЛистToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbMain.Lines[tbMain.Selection.Start.iLine]))
            {
                tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
                tbMain.InsertText("Текущий лист (№ листа)\n", true);
            }
            else
                tbMain.InsertText("Текущий лист (№ листа)", true);
        }

        private void поляЛистаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Поля листа (поле_слева ; поле_справа ; поле_сверху ; поле_снизу)\n", true);
        }

        private void книжнаяToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Ориентация книга\n", true);
        }

        private void альбомнаяToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Ориентация альбом\n", true);
        }

        private void сохранитьEXCELToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Сохранить EXCEL (имя_папки ; имя_файла)\n", true);
        }

        private void работаEXCELToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Работа EXCEL\n", true);
        }

        private void форматA3ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Формат А3\n", true);
        }

        private void форматА4ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Формат А4\n", true);
        }

        private void форматА5ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Формат А5\n", true);
        }

        private void заголовокСтраницToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Заголовок страниц (№ строки начала; № строки конца)\n", true);
        }

        private void слеваToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Верхний колонтитул слева (текст &P страница &N страниц &D дата &T время &Z путь,файл &F файл)\n", true);
        }

        private void поЦентруToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Верхний колонтитул по центру (текст &P страница &N страниц &D дата &T время &Z путь,файл &F файл)\n", true);
        }

        private void справаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Верхний колонтитул справа (текст &P страница &N страниц &D дата &T время &Z путь,файл &F файл)\n", true);
        }

        private void слеваToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Нижний колонтитул слева (текст &P страница &N страниц &D дата &T время &Z путь,файл &F файл)\n", true);
        }

        private void поЦентруToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Нижний колонтитул по центру (текст&P страница &N страниц &D дата &T время &Z путь,файл &F файл)\n", true);
        }

        private void справаToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Нижний колонтитул справа (текст &P страница &N страниц &D дата &T время &Z путь,файл &F файл)\n", true);
        }

        private void горизонтальноToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Центрировать на странице горизонтально\n", true);
        }

        private void вертикальноToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Центрировать на странице вертикально\n", true);
        }

        private void разрывСтраницыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Разрыв страницы (строка)\n", true);
        }

        private void добавитьСтрокуEXCELToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Добавить строку EXCEL (номер вставляемой строки)\n", true);
        }

        private void колонкиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Ширина колонки (колонка ; ширина)\n", true);
        }

        private void колонокToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Ширина колонок (от_колонки ; до_колонки ; ширина)\n", true);
        }

        private void строкиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Высота строки (строка ; высота)\n", true);
        }

        private void строкToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Высота строк (от_строки ; до_строки ; высота)\n", true);
        }

        private void автоподборСтрокToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Автоподбор высоты\n", true);
        }

        private void строкиToolStripMenuItem4_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Размер шрифта строки (строка ; размер)\n", true);
        }

        private void колонкиToolStripMenuItem4_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Размер шрифта колонки (колонка ; размер)\n", true);
        }

        private void ячейкиToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Размер шрифта ячейки (строка_верх ; колонка_верх ; строка_низ ; колонка_низ ; размер)\n", true);
        }

        private void строкиToolStripMenuItem5_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Название шрифта строки (строка ; название шрифта)\n", true);
        }

        private void колонкиToolStripMenuItem5_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Название шрифта колонки (колонка ; название шрифта)\n", true);
        }

        private void ячейкиToolStripMenuItem4_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Название шрифта ячейки (строка_верх ; колонка_верх ; строка_низ ; колонка_низ ; название шрифта)\n", true);
        }

        private void жирныйToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Жирный шрифт строки (строка)\n", true);
        }

        private void отменаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Жирный шрифт строки отмена (строка)\n", true);
        }

        private void жирныйToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Жирный шрифт колонки (колонка)\n", true);
        }

        private void отменаToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Жирный шрифт колонки отмена (колонка)\n", true);
        }

        private void жирныйToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Жирный шрифт ячейки (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void отменаToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Жирный шрифт ячейки отмена (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void тонкойЛиниейToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Подчеркивание шрифта строки тонкой линией (строка)\n", true);
        }

        private void толстойЛиниейToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Подчеркивание шрифта строки толстой линией (строка)\n", true);
        }

        private void отменаToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Подчеркивание шрифта строки без линии (строка)\n", true);
        }

        private void тонкойЛиниейToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Подчеркивание шрифта колонки тонкой линией (колонка)\n", true);
        }

        private void толстойЛиниейToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Подчеркивание шрифта колонки толстой линией (колонка)\n", true);
        }

        private void отменаToolStripMenuItem4_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Подчеркивание шрифта колонки без линии (колонка)\n", true);
        }

        private void тонкойЛиниейToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Подчеркивание шрифта ячейки тонкой линией (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void толстойЛиниейToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Подчеркивание шрифта ячейки толстой линией (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void отменаToolStripMenuItem5_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Подчеркивание шрифта ячейки без линии (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void наклонныйToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Наклонный шрифт строки (строка)\n", true);
        }

        private void отменаToolStripMenuItem6_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Наклонный шрифт строки отмена (строка)\n", true);
        }

        private void наклонныйToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Наклонный шрифт колонки (колонка)\n", true);
        }

        private void отменаToolStripMenuItem7_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Наклонный шрифт колонки отмена (колонка)\n", true);
        }

        private void наклонныйToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Наклонный шрифт ячейки (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void отменаToolStripMenuItem8_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Наклонный шрифт ячейки отмена (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void menuMerge_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Объединение ячеек (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void menuTextToCell_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Текст ячейки (строка ; колонка ; текст)\n", true);
        }


        private void размерСимволовToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Размер символов в ячейке (строка ; колонка ; с_какого_символа ; сколько_символов ; размер)\n", true);
        }

        private void жирныйToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Жирность символов в ячейке (строка ; колонка ; с_какого_символа ; сколько_символов)\n", true);
        }

        private void отменаToolStripMenuItem9_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Жирность символов в ячейке отмена (строка ; колонка ; с_какого_символа ; сколько_символов)\n", true);
        }

        private void наклонныйToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Наклонность символов в ячейке (строка ; колонка ; с_какого_символа ; сколько_символов)\n", true);
        }

        private void отменаToolStripMenuItem10_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Наклонность символов в ячейке отмена (строка ; колонка ; с_какого_символа ; сколько_символов)\n", true);
        }

        private void тонкойЛиниейToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Подчеркивание символов в ячейке тонкой линией (строка ; колонка ; с_какого_символа ; сколько_символов)\n", true);
        }

        private void толстойЛиниейToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Подчеркивание символов в ячейке толстой линией (строка ; колонка ; с_какого_символа ; сколько_символов)\n", true);
        }

        private void отменаToolStripMenuItem11_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Подчеркивание символов в ячейке отмена (строка ; колонка ; с_какого_символа ; сколько_символов)\n", true);
        }

        private void поЗначениюToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Выравнивание по горизонтали строки по значению (строка)\n", true);
        }

        private void поЛевомуКраюToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Выравнивание по горизонтали строки по левому краю (строка)\n", true);
        }

        private void поЦентруToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Выравнивание по горизонтали строки по центру (строка)\n", true);
        }

        private void поПравомуКраюToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Выравнивание по горизонтали строки по правому краю (строка)\n", true);
        }

        private void поШиринеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Выравнивание по горизонтали строки по ширине (строка)\n", true);
        }

        private void поЗначениюToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Выравнивание по горизонтали колонки по значению (колонка)\n", true);
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Выравнивание по горизонтали колонки по левому краю (колонка)\n", true);
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Выравнивание по горизонтали колонки по центру (колонка)\n", true);
        }

        private void toolStripMenuItem4_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Выравнивание по горизонтали колонки по правому краю (колонка)\n", true);
        }

        private void toolStripMenuItem5_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Выравнивание по горизонтали колонки по ширине (колонка)\n", true);
        }

        private void поЗначениюToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Выравнивание по горизонтали ячейки по значению (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem6_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Выравнивание по горизонтали ячейки по левому краю (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem7_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Выравнивание по горизонтали ячейки по центру (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem8_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Выравнивание по горизонтали ячейки по правому краю (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem9_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Выравнивание по горизонтали ячейки по ширине (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void поВерхнемуКраюToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Выравнивание по вертикали строки по верхнему краю (строка)\n", true);
        }

        private void поЦентруToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Выравнивание по вертикали строки по центру (строка)\n", true);
        }

        private void поНижнемуКраюToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Выравнивание по вертикали строки по нижнему краю (строка)\n", true);
        }

        private void поВысотеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Выравнивание по вертикали строки по высоте (строка)\n", true);
        }

        private void поВерхнемуКраюToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Выравнивание по вертикали колонки по верхнему краю (колонка)\n", true);
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Выравнивание по вертикали колонки по центру (колонка)\n", true);
        }

        private void toolStripMenuItem10_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Выравнивание по вертикали колонки по нижнему краю (колонка)\n", true);
        }

        private void toolStripMenuItem11_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Выравнивание по вертикали колонки по высоте (колонка)\n", true);
        }

        private void поВерхнемуКраюToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Выравнивание по вертикали ячейки по верхнему краю (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem12_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Выравнивание по вертикали ячейки по центру (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem13_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Выравнивание по вертикали ячейки по нижнему краю (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem14_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Выравнивание по вертикали ячейки по высоте (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void нетToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Линия всей ячейки отмена (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void толстаяToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Линия всей ячейки толстая (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void тонкаяToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Линия всей ячейки тонкая (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void двойнаяToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Линия всей ячейки двойная (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void пунктирнаяToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Линия всей ячейки пунктирная (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void точкамиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Линия всей ячейки точками (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void нетToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Линия вокруг ячейки отмена (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem21_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Линия вокруг ячейки точками (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem22_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Линия вокруг ячейки пунктирная (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem23_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Линия вокруг ячейки двойная (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem24_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Линия вокруг ячейки тонкая (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem25_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Линия вокруг ячейки толстая (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void нетToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Линия ячейки сверху отмена (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem26_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Линия ячейки сверху точками (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem27_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Линия ячейки сверху пунктирная (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem28_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Линия ячейки сверху двойная (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem29_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Линия ячейки сверху тонкая (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem30_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Линия ячейки сверху толстая (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void нетToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Линия ячейки слева отмена (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem31_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Линия ячейки слева точками (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem32_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Линия ячейки слева пунктирная (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem33_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Линия ячейки слева двойная (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem34_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Линия ячейки слева тонкая (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem35_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Линия ячейки слева толстая (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void нетToolStripMenuItem4_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Линия ячейки снизу отмена (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem36_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Линия ячейки снизу точками (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem37_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Линия ячейки снизу пунктирная (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem38_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Линия ячейки снизу двойная (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem39_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Линия ячейки снизу тонкая (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem40_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Линия ячейки снизу толстая (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void нетToolStripMenuItem5_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Линия ячейки справа отмена (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem41_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Линия ячейки справа точками (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem42_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Линия ячейки справа пунктирная (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem43_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Линия ячейки справа двойная (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem44_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Линия ячейки справа тонкая (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem45_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Линия ячейки справа толстая (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem48_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Толщина линии всей ячейки 1 (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem49_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Толщина линии всей ячейки 2 (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem50_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Толщина линии всей ячейки 3 (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem51_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Толщина линии всей ячейки 4 (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem52_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Толщина линии вокруг ячейки 1 (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem54_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Толщина линии вокруг ячейки 2 (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem55_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Толщина линии вокруг ячейки 3 (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem56_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Толщина линии вокруг ячейки 4 (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem66_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Толщина линии сверху ячейки 1 (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem57_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Толщина линии сверху ячейки 2 (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem58_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Толщина линии сверху ячейки 3 (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem59_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Толщина линии сверху ячейки 4 (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem73_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Толщина линии слева ячейки 1 (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem61_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Толщина линии слева ячейки 2 (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem62_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Толщина линии слева ячейки 3 (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem63_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Толщина линии слева ячейки 4 (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem80_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Толщина линии снизу ячейки 1 (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem64_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Толщина линии снизу ячейки 2 (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem65_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Толщина линии снизу ячейки 3 (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem68_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Толщина линии снизу ячейки 4 (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem87_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Толщина линии справа ячейки 1 (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem69_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Толщина линии справа ячейки 2 (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem70_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Толщина линии справа ячейки 3 (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem71_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Толщина линии справа ячейки 4 (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void общийToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Формат строки общий (строка)\n", true);
        }

        private void числовойToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Формат строки числовой (строка)\n", true);
        }

        private void текстовыйToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Формат строки текстовый (строка)\n", true);
        }

        private void датаToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Формат строки дата (строка)\n", true);
        }

        private void общийToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Формат колонки общий (колонка)\n", true);
        }

        private void toolStripMenuItem15_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Формат колонки числовой (колонка)\n", true);
        }

        private void toolStripMenuItem16_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Формат колонки текстовый (колонка)\n", true);
        }

        private void toolStripMenuItem17_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Формат колонки дата (колонка)\n", true);
        }

        private void общийToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Формат ячейки общий (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem18_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Формат ячейки числовой (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem19_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Формат ячейки текстовый (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void toolStripMenuItem20_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Формат ячейки дата (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void строкиToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Поворот текста строки (строка ; от -90 до 90)\n", true);
        }

        private void колонкиToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Поворот текста колонки (колонка ; от -90 до 90)\n", true);
        }

        private void ячейкиToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Поворот текста ячейки (строка_верх ; колонка_верх ; строка_низ ; колонка_низ ; от -90 до 90)\n", true);
        }

        private void строкиToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Перенос текста строки (строка)\n", true);
        }

        private void колонкиToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Перенос текста колонки (колонка)\n", true);
        }

        private void ячейкиToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Перенос текста ячейки (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void строкиToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Отмена переноса текста строки (строка)\n", true);
        }

        private void колонкиToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Отмена переноса текста колонки (колонка)\n", true);
        }

        private void ячейкиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Отмена переноса текста ячейки (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void menuFillCell_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Заливка ячейки (строка_верх ; колонка_верх ; строка_низ ; колонка_низ)\n", true);
        }

        private void menuPhotoToCell_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Вывод фото в ячейку (строка ; колонка)\n", true);
        }

        private void menuGerbToCell_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Вывод герба в ячейку (строка ; колонка)\n", true);
        }

        private void открытьWORDToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Открыть Word ((\"путь и имя файла\") ; 1 - отобразить 0 - скрыть)\n", true);
        }

        private void заменитьВWORDeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Заменить в Word-е ( (\"что найти-заменить\") ; на что заменить)\n", true);
        }

        private void соранитьWORDToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Сохранить Word (имя_папки ; имя_файла ; 1 - отобразить 0 - скрыть)\n", true);
        }

        private void числоToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("перечень переменных m1,m2,... число\n", true);
        }

        private void датаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("перечень переменных m1,m2,... символ\n", true);
        }

        private void датаToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("перечень массивов m1,m2,... дата\n", true);
        }

        #endregion

        private void tbMain_TextChangedDelayed(object sender, TextChangedEventArgs e)
        {             
            CompilerStatusBar(0);
            e.ChangedRange.ClearStyle(CommentStyle, NumberStyle, KeywordsStyle, StringStyle, BoldСolorStyle, SpecMacroStyle);

            //e.ChangedRange.ClearStyle(wavyStyle);
            //e.ChangedRange.SetStyle(wavyStyle, @"\btest\b");
           
            //комментарии
            Range range = tbMain.Range;
            range.ClearStyle(CommentStyle);
            //range.SetStyle(CommentStyle, @"//.*$", RegexOptions.Singleline);
            range.SetStyle(CommentStyle, @"//.*$", RegexOptions.Multiline);
            range.SetStyle(CommentStyle, @"(\{.*?\})|(\{.*)", RegexOptions.Singleline);
            range.SetStyle(CommentStyle, @"(\{.*?\})|(.*\})", RegexOptions.Singleline |
                        RegexOptions.RightToLeft);
            //e.ChangedRange.SetStyle(CommentStyle, @"\/\/(.){1,}");
            range.SetStyle(StringStyle, @"(\^.*?\^)|(\^.*)", RegexOptions.Singleline);
            range.SetStyle(StringStyle, @"(\^.*?\^)|(.*\^)", RegexOptions.Singleline |
                        RegexOptions.RightToLeft);
            //строки
            string match = "\\^.*?\\^";
            e.ChangedRange.SetStyle(StringStyle, match);

            //цифры
            e.ChangedRange.SetStyle(NumberStyle, @"\b[0-9]{0,10}");
            
            //спецкоманды
            e.ChangedRange.SetStyle(BoldСolorStyle, @"\bКолДней\b");
            e.ChangedRange.SetStyle(BoldСolorStyle, @"\bСтрок\b");
            e.ChangedRange.SetStyle(BoldСolorStyle, @"\bСтолбцов\b");
            e.ChangedRange.SetStyle(BoldСolorStyle, @"\bУдалПробСправа\b");
            e.ChangedRange.SetStyle(BoldСolorStyle, @"\bДатаПрописью\b");
            e.ChangedRange.SetStyle(BoldСolorStyle, @"\bУдалПробСлева\b");
            e.ChangedRange.SetStyle(BoldСolorStyle, @"\bУдалПробСпрСлева\b");
            e.ChangedRange.SetStyle(BoldСolorStyle, @"\bОкруглить\b");
            e.ChangedRange.SetStyle(BoldСolorStyle, @"\bВырезать\b");
            e.ChangedRange.SetStyle(BoldСolorStyle, @"\bНайти\b");
            e.ChangedRange.SetStyle(BoldСolorStyle, @"\bДлина\b");
            e.ChangedRange.SetStyle(BoldСolorStyle, @"\bСтрока\b");
            e.ChangedRange.SetStyle(BoldСolorStyle, @"\bстрока\b");
            e.ChangedRange.SetStyle(BoldСolorStyle, @"\bСТРОКА\b");
            e.ChangedRange.SetStyle(BoldСolorStyle, @"\bДата\b");
            e.ChangedRange.SetStyle(BoldСolorStyle, @"\bС новой строки\b");
            e.ChangedRange.SetStyle(BoldСolorStyle, @"\bДеньгиПрописью\b");

            //команды
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bФормат А3\b");
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bФормат А4\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bФормат А5\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bРабота EXCEL\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bСоздать книгу EXCEL\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bСоздать альбом EXCEL\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bСохранить EXCEL\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bОткрыть EXCEL\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bШирина колонки\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bШирина колонок\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bВысота строки\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bВысота строк\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bВерхний колонтитул слева\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bВерхний колонтитул по центру\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bВерхний колонтитул справа\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bНижний колонтитул слева\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bНижний колонтитул по центру\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bНижний колонтитул по справа\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bТекст ячейки\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bВывод фото в ячейку\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bВывод герба в ячейку\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bРазмер шрифта строки\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bРазмер шрифта колонки\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bРазмер шрифта ячейки\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bНазвание шрифта строки\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bНазвание шрифта колонки\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bНазвание шрифта ячейки\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bЖирный шрифт строки\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bЖирный шрифт строки отмена\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bЖирный шрифт колонки\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bЖирный шрифт колонки отмена\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bЖирный шрифт ячейки\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bЖирный шрифт ячейки отмена\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bПодчеркивание шрифта строки тонкой линией\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bПодчеркивание шрифта строки толстой линией\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bПодчеркивание шрифта строки без линии\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bПодчеркивание шрифта колонки тонкой линией\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bПодчеркивание шрифта колонки толстой линией\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bПодчеркивание шрифта колонки без линии\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bПодчеркивание шрифта ячейки тонкой линией\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bПодчеркивание шрифта ячейки толстой линией\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bПодчеркивание шрифта ячейки без линии\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bНаклонный шрифт строки\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bНаклонный шрифт строки отмена\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bНаклонный шрифт колонки\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bНаклонный шрифт колонки отмена\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bНаклонный шрифт ячейки\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bНаклонный шрифт ячейки отмена\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bОбъединение ячеек\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bРазмер символов в ячейке\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bЖирность символов в ячейке\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bЖирность символов в ячейке отмена\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bНаклонность символов в ячейке\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bНаклонность символов в ячейке отмена\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bПодчеркивание символов в ячейке тонкой линией\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bПодчеркивание символов в ячейке толстой линией\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bПодчеркивание символов в ячейке отмена\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bВыравнивание по горизонтали строки по значению\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bВыравнивание по горизонтали строки по левому краю\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bВыравнивание по горизонтали строки по центру\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bВыравнивание по горизонтали строки по правому краю\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bВыравнивание по горизонтали колонки по значению\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bВыравнивание по горизонтали колонки по левому краю\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bВыравнивание по горизонтали колонки по центру\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bВыравнивание по горизонтали колонки по правому краю\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bВыравнивание по горизонтали ячейки по значению\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bВыравнивание по горизонтали ячейки по левому краю\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bВыравнивание по горизонтали ячейки по центру\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bВыравнивание по горизонтали ячейки по правому краю\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bВыравнивание по вертикали строки по верхнему краю\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bВыравнивание по вертикали строки по центру\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bВыравнивание по вертикали строки по нижнему краю\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bВыравнивание по вертикали строки по высоте\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bВыравнивание по вертикали колонки по верхнему краю\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bВыравнивание по вертикали колонки по центру\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bВыравнивание по вертикали колонки по нижнему краю\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bВыравнивание по вертикали колонки по высоте\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bВыравнивание по вертикали ячейки по верхнему краю\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bВыравнивание по вертикали ячейки по центру\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bВыравнивание по вертикали ячейки по нижнему краю\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bВыравнивание по вертикали ячейки по высоте\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bЛиния всей ячейки отмена\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bЛиния всей ячейки тонкая\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bЛиния всей ячейки толстая\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bЛиния всей ячейки двойная\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bЛиния всей ячейки пунктирная\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bЛиния всей ячейки точками\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bЛиния вокруг ячейки отмена\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bЛиния вокруг ячейки тонкая\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bЛиния вокруг ячейки толстая\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bЛиния вокруг ячейки двойная\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bЛиния вокруг ячейки пунктирная\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bЛиния вокруг ячейки точками\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bЛиния ячейки сверху отмена\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bЛиния ячейки сверху тонкая\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bЛиния ячейки сверху толстая\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bЛиния ячейки сверху двойная\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bЛиния ячейки сверху пунктирная\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bЛиния ячейки сверху точками\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bЛиния ячейки слева отмена\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bЛиния ячейки слева тонкая\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bЛиния ячейки слева толстая\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bЛиния ячейки слева двойная\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bЛиния ячейки слева пунктирная\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bЛиния ячейки слева точками\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bЛиния ячейки снизу отмена\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bЛиния ячейки снизу тонкая\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bЛиния ячейки снизу толстая\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bЛиния ячейки снизу двойная\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bЛиния ячейки снизу пунктирная\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bЛиния ячейки снизу точками\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bЛиния ячейки справа отмена\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bЛиния ячейки справа тонкая\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bЛиния ячейки справа толстая\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bЛиния ячейки справа двойная\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bЛиния ячейки справа пунктирная\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bЛиния ячейки справа точками\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bТолщина линии всей ячейки 1\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bТолщина линии всей ячейки 2\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bТолщина линии всей ячейки 3\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bТолщина линии всей ячейки 4\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bТолщина линии вокруг ячейки 1\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bТолщина линии вокруг ячейки 2\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bТолщина линии вокруг ячейки 3\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bТолщина линии вокруг ячейки 4\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bТолщина линии сверху ячейки 1\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bТолщина линии сверху ячейки 2\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bТолщина линии сверху ячейки 3\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bТолщина линии сверху ячейки 4\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bТолщина линии сверху ячейки 1\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bТолщина линии сверху ячейки 2\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bТолщина линии сверху ячейки 3\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bТолщина линии сверху ячейки 4\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bТолщина линии снизу ячейки 1\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bТолщина линии снизу ячейки 2\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bТолщина линии снизу ячейки 3\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bТолщина линии снизу ячейки 4\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bТолщина линии справа ячейки 1\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bТолщина линии справа ячейки 2\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bТолщина линии справа ячейки 3\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bТолщина линии справа ячейки 4\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bФормат строки общий\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bФормат строки числовой\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bФормат строки текстовый\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bФормат строки дата\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bФормат колонки общий\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bФормат колонки числовой\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bФормат колонки текстовый\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bФормат колонки дата\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bФормат ячейки общий\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bФормат ячейки числовой\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bФормат ячейки текстовый\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bФормат ячейки дата\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bПоворот текста строки\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bПоворот текста колонки\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bПоворот текста ячейки\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bПеренос текста строки\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bПеренос текста колонки\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bПеренос текста ячейки\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bОтмена переноса текста строки\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bОтмена переноса текста колонки\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bОтмена переноса текста ячейки\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bЗаливка ячейки\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bЗаголовок страниц\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bЦентрировать на странице горизонтально\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bЦентрировать на странице вертикально\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bДобавить листов\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bНазвание листа\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bТекущий лист\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bПоля листа\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bОриентация книга\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            e.ChangedRange.SetStyle(KeywordsStyle, @"\bОриентация альбом\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);

            //спец макросы
            if(specMacro.Count() > 0)
            {
                for (int i = 0; i < specMacro.Count(); i++)
                {
                    e.ChangedRange.SetStyle(SpecMacroStyle, @"\b" + (specMacro[i].ShortText.Contains("[,]") ? specMacro[i].ShortText.Substring(0, specMacro[i].ShortText.Length - 3) : specMacro[i].ShortText + @"\b"));
                }
            }
            //скобочки
            e.ChangedRange.SetStyle(KeywordsStyle, @"\(|\)|\;");

            //циклы
            e.ChangedRange.SetStyle(BoldStyle, "если|иначе|конец|перечень массивов|перечень переменных|цикл|до|число|символ|ОчиститьМассив|Запрос|ПараметрЗапроса|ОткрытьЗапрос|Сортировать|пока");
        }

        private void сохранитьToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            using (FbConnection UpdateConnection = new FbConnection(connectionStringBDD))
            {
                byte[] array = Encoding.Default.GetBytes(tbMain.Text);
                UpdateConnection.Open();
                FbTransaction transaction = UpdateConnection.BeginTransaction();
                FbCommand command = new FbCommand("UPDATE formi SET formi8 = @m0 WHERE formi1 = @m1", UpdateConnection, transaction);
                command.Parameters.Add("@m0", array);
                command.Parameters.Add("@m1", ID);
                command.ExecuteNonQuery();
                transaction.Commit();
                UpdateConnection.Close();
            }
            Saved = true;
            CompilerStatusBar(6);
        }

        private void запуститьToolStripMenuItem_Click(object sender, EventArgs e)
        {            
            cmp = new Compiler(this);
            CompilationResult res = cmp.Compile(tbMain.Text);
            if (res.Result == CompilationResult.Compilation_answer.compiled)
            {
                ExecutionModeStart();
                //System.Diagnostics.Stopwatch s = new System.Diagnostics.Stopwatch();
                //s.Start();
                cmp.Run(false, true, stops);
                //s.Stop();
                //MessageBox.Show("Скрипт успешно выполнен за " + s.ElapsedMilliseconds + " мс.");
            }
            else
            {
                MessageBox.Show(res.ErrorText + Environment.NewLine + "Проверьте строку номер " + res.FirstFailedLine, "Ошибка компиляции", MessageBoxButtons.OK, MessageBoxIcon.Error);
                ErrorLine = res.FirstFailedLine - 1;
                tbMain.Navigate(ErrorLine);
                ExecutionModeEnd();
            }
            if (this.Visible == false) this.Close();
        }

        public void Update(int Line)
        {
            changeLine(Line);
            UpdateArrays();
            UpdateArrayGrid();
            UpdateVariablesValues();
        }

        public void changeLine(int line)
        {
            currLine = line;
            tbMain.ClearStyle(StyleIndex.Style1);
           // if (tbMain.LinesCount>=line) line=tbMain.LinesCount-1;
            Range r = new Range(tbMain, 0, line, tbMain.Lines[line].Length, line);
            r.SetStyle(StyleIndex.Style1);
            Application.DoEvents();
            Range n = tbMain.VisibleRange;
            if ((n.Bounds.iStartLine > currLine && n.Bounds.iEndLine > currLine)
                || n.Bounds.iStartLine < currLine && n.Bounds.iEndLine < currLine)
                tbMain.Navigate(line);
        }

        private void UpdateArrays()
        {
            string[] arrays = cmp.GetArraysNames();
            foreach(string p in arrays)
            {
                if(!cbArrays.Items.Contains(p))
                    cbArrays.Items.Add(p);
            }
        }

        private void tbMain_PaintLine(object sender, PaintLineEventArgs e)
        {
            //draw current line marker
            foreach (int s in stops)
            {
                if (e.LineIndex == s)
                {
                    LinearGradientBrush brush;
                    if (stopsDict[s].Length > 0) brush = new LinearGradientBrush(new Rectangle(0, e.LineRect.Top, 15, 15), Color.Yellow, Color.LightGray, 45);
                    else brush = new LinearGradientBrush(new Rectangle(0, e.LineRect.Top, 15, 15), Color.Red, Color.Gray, 45);
                    using (brush)
                    {
                        e.Graphics.FillEllipse(brush, 4, e.LineRect.Top + 4, 8, 8);
                    }
                }
            }
            if (e.LineIndex == ErrorLine)
            {
                tbMain.Range.ClearStyle(ErrorLineBrush);

                Range r = new Range(tbMain, 0, ErrorLine, tbMain.Lines[ErrorLine].Length, ErrorLine);
                r.SetStyle(ErrorLineBrush);
                ErrorLine = -1;

            }
        }

        private void пошаговоToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (cmp == null)
            {
                cmp = new Compiler(this);
                CompilationResult res = cmp.Compile(tbMain.Text);
                if (res.Result == CompilationResult.Compilation_answer.compiled)
                {
                    cmp.Run(true, true, stops);
                    ExecutionModeStart();
                }
                else
                {
                    MessageBox.Show(res.ErrorText + Environment.NewLine + "Проверьте строку номер " + res.FirstFailedLine, "Ошибка компиляции", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    ErrorLine = res.FirstFailedLine - 1;
                    tbMain.Navigate(ErrorLine);
                    ExecutionModeEnd();
                }
            }
            else
            {
                cmp.Run(true, false, stops);
            }
        }

        private void доКурсораToolStripMenuItem_Click(object sender, EventArgs e)
        {
            List<int> cursorStop = new List<int>(stops);
            cursorStop.Add(tbMain.Selection.Start.iLine);
            if (cmp == null)
            {
                cmp = new Compiler(this);
                CompilationResult res = cmp.Compile(tbMain.Text);
                if (res.Result == CompilationResult.Compilation_answer.compiled)
                {                    
                    ExecutionModeStart();
                    //System.Diagnostics.Stopwatch s = new System.Diagnostics.Stopwatch();
                    //s.Start();
                    cmp.Run(false, false, cursorStop);
                    //s.Stop();
                    //MessageBox.Show("Скрипт успешно выполнен за " + s.ElapsedMilliseconds + " мс.");
                    
                }
                else
                {
                    MessageBox.Show(res.ErrorText + Environment.NewLine + "Проверьте строку номер " + res.FirstFailedLine, "Ошибка компиляции", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    ErrorLine = res.FirstFailedLine - 1;
                    tbMain.Navigate(ErrorLine);
                    ExecutionModeEnd();
                }
            }
            else
            {
                cmp.Run(false, false, cursorStop);
            }
        }

        private void tbTextParam_TextChanged(object sender, EventArgs e)
        {
            int a = tbMain.Selection.Start.iLine;
            stopsDict[a] = tbTextParam.Text;
            tbMain.Invalidate();
        }

        private void cbArrays_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateArrayGrid();
        }

        private void UpdateArrayGrid()
        {
            if (cbArrays.SelectedItem == null) return;
            dgArray.Rows.Clear();
            dgArray.Columns.Clear();
            //dgArray.RowHeadersWidth = 25;
            var p = cmp.GetArrayContent(cbArrays.SelectedItem.ToString());
            if (p is object[,])
            {
                for (int i = 0; i < (p as object[,]).GetLength(1); i++)
                {
                    dgArray.Columns.Add(i.ToString(), i.ToString());
                }
                for (int i = 0; i < (p as object[,]).GetLength(0); i++)
                {
                    dgArray.Rows.Add();
                    dgArray.Rows[i].HeaderCell.Value = i.ToString();
                }

                for (int i = 0; i < (p as object[,]).GetLength(0); i++)
                {
                    for (int j = 0; j < (p as object[,]).GetLength(1); j++)
                    {
                        dgArray[j, i].Value = (p as object[,])[i, j];
                    }
                }
            }
            else if (p is double[,])
            {
                for (int i = 0; i < (p as double[,]).GetLength(1); i++)
                {
                    dgArray.Columns.Add(i.ToString(), i.ToString());
                }
                for (int i = 0; i < (p as double[,]).GetLength(0); i++)
                {
                    dgArray.Rows.Add();
                    dgArray.Rows[i].HeaderCell.Value = i.ToString();
                }

                for (int i = 0; i < (p as double[,]).GetLength(0); i++)
                {
                    for (int j = 0; j < (p as double[,]).GetLength(1); j++)
                    {
                        dgArray[j, i].Value = (p as double[,])[i, j];
                    }
                }
            }
            else if (p is string[,])
            {
                for (int i = 0; i < (p as string[,]).GetLength(1); i++)
                {
                    dgArray.Columns.Add(i.ToString(), i.ToString());
                }
                for (int i = 0; i < (p as string[,]).GetLength(0); i++)
                {
                    dgArray.Rows.Add();
                    dgArray.Rows[i].HeaderCell.Value = i.ToString();
                }

                for (int i = 0; i < (p as string[,]).GetLength(0); i++)
                {
                    for (int j = 0; j < (p as string[,]).GetLength(1); j++)
                    {
                        dgArray[j, i].Value = (p as string[,])[i, j];
                    }
                }
            }
            else
            {
                //dgArray.DataSource = p as string[,];
            }
        }

        private void dataGridView2_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (dgVariables[0, e.RowIndex].Value != null && cmp != null)
            {

                dgVariables[1, e.RowIndex].Value = cmp.GetVariableValue(dgVariables[0, e.RowIndex].Value.ToString());
            }
        }

        private void UpdateVariablesValues()
        {
            for (int i = 0; i < dgVariables.Rows.Count - 1; i++)
            {
                dgVariables[1, i].Value = cmp.GetVariableValue(dgVariables[0, i].Value.ToString());
            }
        }

        public void ExecutionModeStart()
        {
            //cbStartExecution.Enabled = false;
            cbStopExecution.Visible = true;
            tbMain.ReadOnly = true;
        }

        public void ExecutionModeEnd()
        {
            cmp = null;
            //ErrorLine = -1;
            currLine = null;
            //cbStartExecution.Enabled = true;
            cbStopExecution.Visible = false;
            tbMain.ClearStyle(StyleIndex.Style1);
            tbMain.ReadOnly = false;
        }

        private void cbStopExecution_Click(object sender, EventArgs e)
        {
            ExecutionModeEnd();
        }

        private void tbMain_TextChanged(object sender, TextChangedEventArgs e)
        {
            //clear folding markers
            e.ChangedRange.ClearFoldingMarkers();

            //set folding markers
            e.ChangedRange.SetFoldingMarkers(@"(\\n|^)\s*(если|пока|цикл)", "конец");

            tbMain.Range.ClearStyle(ErrorLineBrush);
            
            Saved = false;
        }

        public void CompilerStatusBar(int code)
        {
            switch (code)
            {
                case 0: lbCompilerStatus.Text = "Готов к выполнению";
                    break;

                case 1: lbCompilerStatus.Text = "Компилируется...";
                    break;

                case 2: lbCompilerStatus.Text = "Выполняется...";
                    break;

                case 3: lbCompilerStatus.Text = "Ошибка компиляции";
                    break;

                case 4: lbCompilerStatus.Text = "Ошибка выполнения";
                    break;

                case 5: lbCompilerStatus.Text = "Остановка";
                    break;

                case 6: lbCompilerStatus.Text = "Сохранено";
                    break;
            }
        }
        
        private void tbMain_AutoIndentNeeded(object sender, AutoIndentEventArgs args)
        {
            //block 
            if (Regex.IsMatch(args.LineText, @"^[^""']*(цикл|пока|если).*конец[^""']*$"))
                return;
            //start of block 
            if (Regex.IsMatch(args.LineText, @"^[^""']*(цикл|пока|если)"))
            {
                args.ShiftNextLines = args.TabLength;
                return;
            }

            if (Regex.IsMatch(args.LineText, @"иначе"))
            {
                args.Shift = -args.TabLength;
                return;
            }
            //end of block 
            if (Regex.IsMatch(args.LineText, @"конец[^""']*$"))
            {
                args.Shift = -args.TabLength;
                args.ShiftNextLines = -args.TabLength;
                return;
            }
            
        }

        private void запуститьПоМеткамToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (cmp == null)
            {
                cmp = new Compiler(this);
                CompilationResult res = cmp.Compile(tbMain.Text);
                if (res.Result == CompilationResult.Compilation_answer.compiled)
                {
                    ExecutionModeStart();
                    //System.Diagnostics.Stopwatch s = new System.Diagnostics.Stopwatch();
                    //s.Start();
                    cmp.Run(false, false, stops);
                    cmp.stepOutStriper = false;
                    //s.Stop();
                    //MessageBox.Show("Скрипт успешно выполнен за " + s.ElapsedMilliseconds + " мс.");
                }
                else
                {
                    MessageBox.Show(res.ErrorText + Environment.NewLine + "Проверьте строку номер " + res.FirstFailedLine, "Ошибка компиляции", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    ErrorLine = res.FirstFailedLine - 1;
                    tbMain.Navigate(ErrorLine);
                    ExecutionModeEnd();
                }
            }
            else
                cmp.Run(false, false, stops);
        }

        private void добавиттьМеткуToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int a = tbMain.Selection.Start.iLine;
            if (!stops.Contains(a))
            {
                stops.Add(a);
                stopsDict.Add(a, "");
            }
            else
            {
                stops.Remove(a);
                stopsDict.Remove(a);
            }
            tbMain.Invalidate();
        }

        private void удалитьВсеМеткиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            stops.Clear();
            stopsDict.Clear();
            tbMain.Invalidate();
        }

        private void найтиToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (ff == null)
                ff = new FindForm(tbMain);
            if (!tbMain.Selection.IsEmpty && tbMain.Selection.Start.iLine == tbMain.Selection.End.iLine)
                ff.tbFind.Text = tbMain.Selection.Text;

            ff.tbFind.SelectAll();
            ff.Show();
        }

        private void найттиИЗаменитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (tbMain.ReadOnly)
                return;
            if (rf == null)
                rf = new ReplaceForm(tbMain);
            if (!tbMain.Selection.IsEmpty && tbMain.Selection.Start.iLine == tbMain.Selection.End.iLine)
                rf.tbFind.Text = tbMain.Selection.Text;

            rf.tbFind.SelectAll();
            rf.Show();
        }

        private void найтиДалееToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ff == null || ff.tbFind.Text == "")
                найтиToolStripMenuItem1_Click(sender, e);
            else
                ff.FindNext(ff.tbFind.Text);
        }

        private void tbMain_SelectionChangedDelayed(object sender, EventArgs e)
        {
            tbMain.VisibleRange.ClearStyle(SameWordsStyle);
            if (!tbMain.Selection.IsEmpty)
                return;//user selected diapason

            //get fragment around caret
            var fragment = tbMain.Selection.GetFragment(@"\w");
            string text = fragment.Text;
            if (text.Length == 0)
                return;
            //highlight same words
            var ranges = tbMain.VisibleRange.GetRanges("\\b" + text + "\\b").ToArray();
            if (ranges.Length > 1)
                foreach (var r in ranges)
                    r.SetStyle(SameWordsStyle);
        }

        private void перейтиКСледующейМеткеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (stops.Count > 0)
            {
                int current = tbMain.Selection.Start.iLine;
                stops.Sort();
                foreach (int a in stops)
                {
                    if (a > current)
                    {
                        tbMain.Navigate(a);
                        return;
                    }
                }
                tbMain.Navigate(stops[0]);
            }
            return;
        }

        private void FMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!Saved)
            {
                switch (MessageBox.Show("Сохранить перед выходом?", "Сохранение", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
                {
                    case System.Windows.Forms.DialogResult.Yes :
                        сохранитьToolStripMenuItem1_Click(null, null);
                        break;
                    case System.Windows.Forms.DialogResult.Ignore:
                        break;
                    case System.Windows.Forms.DialogResult.Cancel:
                        e.Cancel = true;
                        break;
                }
            }
        }

        private void splitContainer1_Paint(object sender, PaintEventArgs e)
        {
            SplitContainer l_SplitContainer = sender as SplitContainer;

            if (l_SplitContainer != null)
            {
                Rectangle ll_ShrinkedSplitterRectangle = l_SplitContainer.SplitterRectangle;
                ll_ShrinkedSplitterRectangle.Offset(0, 2);
                ll_ShrinkedSplitterRectangle.Height = ll_ShrinkedSplitterRectangle.Height - 2;
                e.Graphics.FillRectangle(Brushes.DarkGray, ll_ShrinkedSplitterRectangle);
            }
        }

        private void cbShowMap_CheckedChanged(object sender, EventArgs e)
        {
            if(cbShowMap.Checked)
                TopSplitter.Panel2Collapsed = false;
            else
                TopSplitter.Panel2Collapsed = true;
        }

        private void tbMain_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F6)
            {
                if(!tbMain.Selection.IsEmpty)
                    dgVariables.Rows.Add(tbMain.Selection.Text);
                else
                    dgVariables.Rows.Add(tbMain.Selection.GetFragment(@"\w").Text);
            }
        }              

        private void FMain_VisibleChanged(object sender, EventArgs e)
        {
           /* if (this.Visible == false)
            {
                запуститьToolStripMenuItem_Click(this, null);
            }*/
        }

        private void FMain_Load(object sender, EventArgs e)
        {
            this.Visible = true;
            if (this.Text.Contains("visible=false"))
            {                
                this.Text.Replace("visible=false", "");
                MessageBox.Show("Скрипт выполняется...");
                this.Visible = false;
                запуститьToolStripMenuItem_Click(this, null);                
            }
            
        }

        private void выйтиToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void книгаToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Создать книгу OPEN (название_листа; поле_слева ; поле_справа ; поле_сверху ; поле_снизу)\n", true);
        }

        private void альбомToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            tbMain.Navigate(tbMain.Selection.Start.iLine + 1);
            tbMain.InsertText("Создать альбом OPEN (название_листа; поле_слева ; поле_справа ; поле_сверху ; поле_снизу)\n", true);
        }

    }

    class Compiler
    {
        private string connectionString = @"User = SYSDBA;Password = masterkey;Database = D:\BD\BD;DataSource = 127.0.0.1;Port = 3050;Dialect = 3;Charset = WIN1251;Role =;Connection lifetime = 15;Pooling = true;MinPoolSize = 0;MaxPoolSize = 50;Packet Size = 8192;ServerType = 0";
        private FMain f;
        private static bool open = false;
        private static bool useopen = false;
        //скомпилированый текст
        private string[] compiledText;

        //список переменных и массивов
        private Dictionary<string, object> VariablesList = new Dictionary<string, object>();

        //список константных строк
        private Dictionary<string, string> StringsList = new Dictionary<string, string>();
        private int stringscounter = 0;

        //current line
        private int currentLine = 0;

        //предыдущая линия
        private int prevLine = -1;

        //команда запроса в базу
        private string DataBaseRequest = "";

        //список параметров запроса
        private string[] param = new string[0];

        public bool stepOutStriper = true;

        // #l - for loop
        // #i - if loop
        // #w - while loop
        // #r - database request
        // #p - database parameter                 ПараметрЗапроса[0]=5
        // #o - database open request              ОткрытьЗапрос(массив или переменная)
        // #s - special marco commands             График(массив)
        // %058 - command
        // &30 - adress
        // $var - variable
        // 5 - constant variable

        public Compiler(FMain f1)
        {
            f = f1;
            connectionString = f1.GetConnectionString();
        }

        private bool contains(string input)
        {
        if (input.Contains("января") || input.Contains("февраля") || input.Contains("марта") || input.Contains("апреля") || input.Contains("мая") || input.Contains("июня") || input.Contains("июля") || input.Contains("августа") || input.Contains("сентября") || input.Contains("октября") || input.Contains("ноября") || input.Contains("декабря")) return true;
            return false;
        }
        //запуск скрипта
        public void Run(bool stepByStep, bool ignoreStops, List<int> stops)
        {
            try
            {
                bool end = true;                
                while (end)
                {
                    if (currentLine > compiledText.Count() - 1)
                    {
                        f.ExecutionModeEnd();
                        return;
                    }

                    if (compiledText[currentLine].StartsWith("ignore"))
                    {
                        currentLine++;
                        continue;
                    }

                    if (stops.Contains(currentLine) && ignoreStops == false)
                    {
                        if (f.stopsDict.ContainsKey(currentLine) && !string.IsNullOrEmpty(f.stopsDict[currentLine]))
                        {                            
                            if (RecognizeLogical(f.stopsDict[currentLine]))
                            {
                                string[] input;
                                string param = f.stopsDict[currentLine];
                                if (param.Contains("="))
                                input=param.Split(new string[] { "=" }, StringSplitOptions.RemoveEmptyEntries);
                                else input = param.Split(new string[] { "<" }, StringSplitOptions.RemoveEmptyEntries);
                                if (VariablesList[input[0]].ToString()==input[1])
                                {
                                    end = false;
                                    f.Update(currentLine++);
                                    continue;
                                }
                            }
                        }
                        else
                        {
                            end = false;
                            //f.Update(currentLine++);
                            f.Update(currentLine);
                            continue;
                        }
                    }

                    if (stepByStep)
                    {
                        if (stepOutStriper)
                        {
                            end = false;
                            f.Update(currentLine);
                            stepOutStriper = false;
                            continue;
                        }
                    }
                    stepOutStriper = true;
                    if (compiledText[currentLine].StartsWith("%"))
                    {
                        //launch command
                        string p = compiledText[currentLine];
                        p = Regex.Replace(p, @"(ДатаПрописью(\(.+?\)))", m => (DataP(m.Groups[2].Value)));
                        p = Regex.Replace(p, @"(Найти(\(.+?\)))", m => (Find(m.Groups[2].Value,true)));
                        p = Regex.Replace(p, @"(Строк(\(.+?\)))", m => (RowsCount(m.Groups[2].Value)));
                        p = Regex.Replace(p, @"(Столбцов(\(.+?\)))", m => (ColumnsCount(m.Groups[2].Value)));
                        p = Regex.Replace(p, @"(Длина(\(.+?\)))", m => (LengthCommand(m.Groups[2].Value,true)));
                        string[] command = p.Split(new char[] { '%', '(' }, StringSplitOptions.RemoveEmptyEntries);
                        string[] param = null;
                        if (command.Length > 1)
                        {
                            param = command[1].Split(new char[] { ';', ')', ' '}, StringSplitOptions.RemoveEmptyEntries);
                        }
                        if (param != null)
                        {
                            //преобразовываем все переменные в значения
                            for (int i = 0; i < param.GetLength(0); i++)
                            {
                                if ((param[i].Contains("|")) && (param[i].Length>8))
                                {
                                    param[i] = param[i].Replace("|", " ");
                                    param[i] = (SequenceString(param[i]));
                                    
                                } else
                                if (StringIsNumeric(param[i]))
                                {                                                            
                                    param[i] = Sequence(RecognizeVariables(param[i])).ToString();
                                }
                                else
                                {
                                    param[i] = (SequenceString(param[i]));
                                }
                            }
                        }
                        RegistryKey hkcr = Registry.ClassesRoot;

                        RegistryKey excelKey = hkcr.OpenSubKey("Excel.Application");

                        bool excelInstalled = excelKey == null ? false : true;
                        if (!excelInstalled) open=true;                        
                        if (Convert.ToInt32(command[0]) == 1800)
                        {
                            useopen = true; command[0] = "18";
                        }
                        if ((!open) && (!useopen))
                            ExcelAction.Perform(Convert.ToInt32(command[0]), param);
                        else OpenAction.Perform(Convert.ToInt32(command[0]), param);
                        currentLine++;
                        continue;
                    }
                    //сортировать
                    if (compiledText[currentLine].StartsWith("Сортировать"))
                    {
                        SortArray(compiledText[currentLine]);
                        currentLine++;
                        continue;
                    }
                    //спецмакросы
                    /*else if (compiledText[currentLine].StartsWith("#s"))
                    {
                        string[] vars = compiledText[currentLine].Split(new string[] { "#s", "(", ")", "$" }, StringSplitOptions.RemoveEmptyEntries);
                        VariablesList[vars[0]] = f.specMacro[Convert.ToInt32(vars[1])].macroCommandFuntion();
                    }*/

                    //если
                    else if (compiledText[currentLine].StartsWith("#i")) 
                    {
                        compiledText[currentLine] = Regex.Replace(compiledText[currentLine], @"Длина\(.+\)", m => (LengthCommand(m.Groups[0].Value,false)));
                        compiledText[currentLine] = Regex.Replace(compiledText[currentLine], @"Вырезать\(.+\)", m => (CutCommand(m.Groups[0].Value)));
                        compiledText[currentLine] = Regex.Replace(compiledText[currentLine], @"ДатаПрописью\(.+\)", m => (DataP(m.Groups[0].Value)));
                        compiledText[currentLine] = Regex.Replace(compiledText[currentLine], @"Найти\(.+\)", m => (Find(m.Groups[0].Value,false)));
                        //формируем значения для проверки
                        string[] vars = compiledText[currentLine].Split(new string[] { "#i", ";" }, StringSplitOptions.RemoveEmptyEntries);
                        //проверяем, попали ли мы сюда в первый раз или нет
                        if (currentLine < prevLine)
                        {
                            // 4 для если иначе конец
                            if (vars.Count() == 3)
                            {
                                prevLine = currentLine;
                                currentLine = Convert.ToInt32(vars[2]) + 1;
                                continue;
                            }
                            else // 2 для если конец
                            {
                                prevLine = currentLine;
                                currentLine = Convert.ToInt32(vars[1]) + 1;
                                /*if (currentLine >= compiledText.Count() - 1)
                                    currentLine = compiledText.Count() - 1;*/
                                //currentLine = Convert.ToInt32(vars[1]);
                                continue;
                            }
                        }

                        //преобразовываем все переменные в значения
                        //foreach (string st in VariablesList.Keys)
                        //{
                        //    if (VariablesList[st] is double)
                        //    {
                        //        vars[0] = Regex.Replace(vars[0], @"\$" + st, Convert.ToDouble(VariablesList[st]).ToString());
                        //        vars[1] = Regex.Replace(vars[1], @"\$" + st, Convert.ToDouble(VariablesList[st]).ToString());
                        //    }
                        //}

                        //проверяем выполнение условия оператора
                        if (RecognizeLogical(vars[0]))
                        {
                            //если условие выполнилось
                            //идем дальше
                            prevLine = currentLine;
                            currentLine++;
                            continue;
                        }
                        else
                        {
                            //если нет
                            //идем в иначе или в конец 
                            //это определять не нужно, так каконо всегда будет во втором поле
                            prevLine = currentLine;
                            currentLine = Convert.ToInt32(vars[1]) + 1;
                            continue;
                        }
                    }

                    else if (compiledText[currentLine].StartsWith("#w"))
                    {
                        //формируем значения для проверки
                        string[] vars = compiledText[currentLine].Split(new string[] { "#w", ";" }, StringSplitOptions.RemoveEmptyEntries);

                        //проверяем выполнение условия оператора
                        if (RecognizeLogical(vars[0]))
                        {
                            //если условие выполнилось
                            //идем дальше
                            prevLine = currentLine;
                            currentLine++;
                            continue;
                        }
                        else
                        {
                            //если нет, то выходим из цикла

                            //последняя строка цикла

                            prevLine = currentLine;
                            currentLine = Convert.ToInt32(vars[1]) + 1;
                        }
                        continue;
                    }

                    else if (compiledText[currentLine].StartsWith("#l"))
                    {
                        string[] vars = compiledText[currentLine].Split(new string[] { "=", "#l", ";" }, StringSplitOptions.RemoveEmptyEntries);

                        if (currentLine > prevLine)
                        {
                            //если предыдущая строка больше текущей, это значит что мы попали в цикл впервые,
                            //и необходимо присвоить  переменной начальное значение
                            SetNewValue(vars[0], vars[1]);
                        }
                        else
                        {
                            //если нет, то это значит что мы уже на следующей итерации
                            //необходимо увеличить значение итерируемой переменной
                            SetNewValue(vars[0], Convert.ToInt32(Sequence(RecognizeVariables(vars[0]))) + 1);
                        }

                        if (Convert.ToInt32(Sequence(RecognizeVariables(vars[0]))) <= Convert.ToInt32(Sequence(RecognizeVariables(vars[2]))))
                        {
                            //если условие цикла выполняется, идем на след. строку
                            prevLine = currentLine;
                            currentLine++;
                        }
                        else
                        {
                            prevLine = currentLine;
                            currentLine = Convert.ToInt32(vars[3]) + 1;
                        }
                        continue;
                    }

                    //запрос в базу
                    else if (compiledText[currentLine].StartsWith("#r"))
                    {
                        DataBaseRequest = compiledText[currentLine].Split(new string[] { "(^", "#r", "^)" }, StringSplitOptions.RemoveEmptyEntries)[0];
                        prevLine = currentLine;
                        currentLine++;
                        continue;
                    }

                    //параметры запроса
                    else if (compiledText[currentLine].StartsWith("#p"))
                    {
                        string[] vars = compiledText[currentLine].Split(new string[] { "=", "#p", "[", "]" }, StringSplitOptions.RemoveEmptyEntries);
                        Array.Resize(ref param, (int)Math.Max(param.Count(), Convert.ToInt32(Sequence(RecognizeVariables(vars[0]))) + 1));

                        param[Convert.ToInt32(Sequence(RecognizeVariables(vars[0])))] = Sequence(RecognizeVariables(vars[1])).ToString();

                        prevLine = currentLine;
                        currentLine++;
                        continue;
                    }

                    //выполнить запрос
                    else if (compiledText[currentLine].StartsWith("#o"))
                    {
                        string vars = compiledText[currentLine].Split(new string[] { "(", "#o", ")" }, StringSplitOptions.RemoveEmptyEntries)[0];
                        VariablesList[vars] = ToDataBase();
                        prevLine = currentLine;
                        currentLine++;
                        continue;
                    }

                    //адрес
                    else if (compiledText[currentLine].StartsWith("&"))
                    {
                        prevLine = currentLine;
                        currentLine = Convert.ToInt32(compiledText[currentLine].Substring(1, compiledText[currentLine].Length - 1));
                        continue;
                    }


                    //переменная 
                    else if (compiledText[currentLine].StartsWith("$"))
                    {
                        string[] p;
                        compiledText[currentLine] = compiledText[currentLine].Replace(" ", "");
                        p = compiledText[currentLine].Split('=');
                        //проверяем, имеем дело с массивом или просто переменной
                        if (p[0].Contains("[") && p[0].Contains("]"))
                        {
                            string[] rightArray = p[0].Split(new char[] { '[', ']', ',', '$' }, StringSplitOptions.RemoveEmptyEntries);

                            if (VariablesList[rightArray[0]] is double[,])
                            {
                                VariablesList[rightArray[0]] = ResizeArray<double>(VariablesList[rightArray[0]] as double[,],
                                    Math.Max((Convert.ToInt32(Sequence(RecognizeVariables(rightArray[1]))) + 1), (VariablesList[rightArray[0]] as double[,]).GetLength(0)),
                                    Math.Max((Convert.ToInt32(Sequence(RecognizeVariables(rightArray[2]))) + 1), (VariablesList[rightArray[0]] as double[,]).GetLength(1)));

                                (VariablesList[rightArray[0]] as double[,])[Convert.ToInt32(Sequence(RecognizeVariables(rightArray[1]))),
                                    Convert.ToInt32(Sequence(RecognizeVariables(rightArray[2])))] = Sequence(RecognizeVariables(p[1]));
                            }
                            else if (VariablesList[rightArray[0]] is string[,])
                            {
                                VariablesList[rightArray[0]] = ResizeArray<string>(VariablesList[rightArray[0]] as string[,],
                                    Math.Max((Convert.ToInt32(Sequence(RecognizeVariables(rightArray[1]))) + 1), (VariablesList[rightArray[0]] as string[,]).GetLength(0)),
                                    Math.Max((Convert.ToInt32(Sequence(RecognizeVariables(rightArray[2]))) + 1), (VariablesList[rightArray[0]] as string[,]).GetLength(1)));

                                //TODO!
                                (VariablesList[rightArray[0]] as string[,])[Convert.ToInt32(Sequence(RecognizeVariables(rightArray[1]))),
                                    Convert.ToInt32(Sequence(RecognizeVariables(rightArray[2])))] = SequenceString(p[1]);
                            }
                        }
                        else
                        {
                            p[0] = p[0].Replace("$", "");
                            if (VariablesList[p[0]] is double)
                            {
                                VariablesList[p[0]] = Sequence(RecognizeVariables(p[1]));
                            }
                            else if (VariablesList[p[0]] is string)
                            {
                                VariablesList[p[0]] = SequenceString(p[1]);
                            }
                        }
                        prevLine = currentLine;
                        currentLine++;
                        continue;
                    }

                    else
                    {
                        prevLine = currentLine;
                        currentLine++;
                        continue;
                    }

                    //if (currentLine == compiledText.Length)
                    //{
                    //    end = false;
                    //    f.Update(currentLine);
                    //}

                }
            }
            catch (FormatException)
            {
                MessageBox.Show("Неверный формат числа. Попробуйте запятую вместо точки или наоборот. Ошибка на строке " + (currentLine + 1).ToString(), "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка выполнения на строке " + (currentLine + 1).ToString() + Environment.NewLine + Environment.NewLine +
                    "Внутренее сообщение ошибки:" + Environment.NewLine +
                    ex.ToString(), "Ошибка выполнения скрипта", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //компиляция скрипта
        public CompilationResult Compile(string text)
        {
            //сбрасываем строку
            currentLine = 0;

            RemoveMultilineCommments(ref text);

            //находим команды экселя
            text = RecognizeCommands(text);
            
            //разделяем строку на массив строк
            compiledText = text.Split(new[] { '\n' }/*, StringSplitOptions.RemoveEmptyEntries*/);

            //инициализируем стэк адресов
            Stack<int> adretack = new Stack<int>();
            int i = 0;
            int trim = 0;
            //удаляем ненужные пробелы
            for (i = 0; i < compiledText.Length; i++)
            {
                if ((compiledText[i] == "\r") || (compiledText[i].Replace(" ","")=="\r"))
                {
                    compiledText[i] = "ignore";
                    continue;
                }               
                trim++;
                //ВНИМАНИЕ!!! ----------------------------------------------------------------------------
                compiledText[i] = compiledText[i].Trim();
                //compiledText[i] = compiledText[i].Replace("\r","");
            }     
            //удаляем пустые строки
            compiledText = compiledText.Where(x => !string.IsNullOrEmpty(x)).ToArray();

            try
            {
                bool ignore = false; int numBR = 0;
                //преобразуем строки в команды(компилируем)
                for (i = 0; i < compiledText.Length; i++)
                {
                    if (i>1)
                    {
                        if (compiledText[i - 1] == "") compiledText[i - 1] = "ignore";
                    }
                    //делаем коментарии игнорируемыми
                    if (compiledText[i].StartsWith("//", false, System.Globalization.CultureInfo.CurrentCulture) || (compiledText[i].Replace(" ", "").StartsWith("//", false, System.Globalization.CultureInfo.CurrentCulture)))
                    {
                       // if (compiledText[i] == "//") compiledText[i+1] = "ignore";
                        compiledText[i] = "ignore";
                        //compiledText[i] = "";
                           
                        continue;
                    }
                    if (compiledText[i].StartsWith("{", false, System.Globalization.CultureInfo.CurrentCulture) && compiledText[i].EndsWith("}", false, System.Globalization.CultureInfo.CurrentCulture))
                    {
                        compiledText[i] = "ignore";
                        //compiledText[i] = "";
                        continue;
                    }
                    if (compiledText[i].Contains("{") && compiledText[i].Contains("}"))
                    {
                        compiledText[i] = Regex.Replace(compiledText[i], @"{.*?\}", "");                        
                        continue;
                    }
                    if (compiledText[i].Contains("{"))
                    {
                        numBR++;
                        compiledText[i] = Regex.Replace(compiledText[i], @"\{.*", "");
                        ignore = true;
                        continue;
                    }
                    if (compiledText[i].Contains("}"))
                    {
                        compiledText[i] = Regex.Replace(compiledText[i], @".*\}", "");                       
                        ignore = false;
                        continue;
                    }
                    if ((ignore) && (numBR>0))
                    {
                        numBR--;
                        compiledText[i] = "ignore";                        
                        continue;
                    }
                    if (compiledText[i].Contains("^"))
                    {
                        int n;
                        string sourcestring = compiledText[i];
                        string substring = "^";
                        int count = (sourcestring.Length - sourcestring.Replace(substring, "").Length) / substring.Length;
                        if (count==1)
                        {
                            n = i;
                        i++;
                        while (!compiledText[i].Contains("^"))
                        {
                            compiledText[n] += compiledText[i];
                            compiledText[i] = "ignore";
                            i++;
                        }
                        compiledText[n] += compiledText[i];
                        compiledText[i] = "ignore";
                        i = n;
                        //continue;
                        }
                    }

                    //делаем коментарии игнорируемыми
                    if (compiledText[i].StartsWith("ignore", false, System.Globalization.CultureInfo.CurrentCulture))
                    {
                        continue;
                    }


                    //удаляем коментарии после строк
                    compiledText[i] = Regex.Replace(compiledText[i], @"\/\/.*", "");
                    //compiledText[i] = Regex.Replace(compiledText[i], @"\/\/.*", "");
                    //Переменные
                    if (compiledText[i].StartsWith("перечень переменных", false, System.Globalization.CultureInfo.CurrentCulture) ||
                        compiledText[i].StartsWith("перечень массивов", false, System.Globalization.CultureInfo.CurrentCulture))
                    {
                        string[] test = compiledText[i].Split(new char[] { ',', ' ' }, StringSplitOptions.RemoveEmptyEntries);

                        for (int j = 2; j < test.Count() - 1; j++)
                        {
                            if (!VariablesList.ContainsKey(test[j]))
                            {
                                string pattern1 = @"([a-zA-Zа-яА-Я_]+[a-zA-Zа-яА-Я0-9]*)";
                                Regex reg1 = new Regex(pattern1);
                                if (!reg1.IsMatch(test[j]))
                                {
                                    return new CompilationResult() { Result = CompilationResult.Compilation_answer.failed, ErrorText = "Неправильный формат переменной '" + test[j] +
                                        "'" + Environment.NewLine + "Переменная должна начинаться с буквы.", FirstFailedLine = i + 1};
                                }
                                if (test[1] == "переменных")
                                {
                                    if (test[test.Length - 1] == "число")
                                        VariablesList.Add(test[j], new double());
                                    else if (test[test.Length - 1] == "символ")
                                        VariablesList.Add(test[j], string.Empty);
                                    else if (test[test.Length - 1] == "дата")
                                        VariablesList.Add(test[j], new DateTime());
                                }
                                else
                                {
                                    if (test[test.Length - 1] == "число")
                                        VariablesList.Add(test[j], new double[1, 1]);
                                    else if (test[test.Length - 1] == "символ")
                                        VariablesList.Add(test[j], new string[1, 1]);
                                }
                            }
                            else
                            {
                                return new CompilationResult() { Result = CompilationResult.Compilation_answer.failed, ErrorText = "Переменная уже инициализирована", FirstFailedLine = i + 1};
                            }
                        }
                        compiledText[i] = "ignore";
                        continue;
                    }

                    //цикл for
                    if (compiledText[i].StartsWith("цикл ", false, System.Globalization.CultureInfo.CurrentCulture))
                    {
                        compiledText[i] = Regex.Replace(compiledText[i], @"\bцикл\b", "#l");
                        compiledText[i] = Regex.Replace(compiledText[i], @"\bдо\b", ";");

                        foreach (string s in VariablesList.Keys)
                        {
                            compiledText[i] = Regex.Replace(compiledText[i], @"\b" + s + @"(\b|=)", m => string.Format(@"${0}", m.Value));
                        }

                        compiledText[i] = compiledText[i].Replace(" ", "");
                        compiledText[i] += ';';
                        adretack.Push(i);
                        continue;
                    }

                    //запрос в базу
                    if (compiledText[i].StartsWith("Запрос", false, System.Globalization.CultureInfo.CurrentCulture))
                    {
                        compiledText[i] = Regex.Replace(compiledText[i], @"\bЗапрос", "#r");
                        continue;
                    }

                    //параметр запроса
                    if (compiledText[i].StartsWith("ПараметрЗапроса", false, System.Globalization.CultureInfo.CurrentCulture))
                    {
                        compiledText[i] = Regex.Replace(compiledText[i], @"\bПараметрЗапроса", "#p");
                        continue;
                    }

                    //открыть запрос 
                    if (compiledText[i].StartsWith("ОткрытьЗапрос", false, System.Globalization.CultureInfo.CurrentCulture))
                    {
                        compiledText[i] = Regex.Replace(compiledText[i], @"\bОткрытьЗапрос", "#o");
                        continue;
                    }
                    //спец макросы
                    for (int mi = 0; mi < f.specMacro.Count(); mi++)
                        if (compiledText[i].StartsWith(f.specMacro[mi].ShortText, false, System.Globalization.CultureInfo.CurrentCulture))
                        {
                            compiledText[i] = Regex.Replace(compiledText[i], @"\b" + f.specMacro[mi].ShortText, "#s");
                            compiledText[i] += mi.ToString();
                            continue;
                        }

                    //цикл while
                    if (compiledText[i].StartsWith("пока ", false, System.Globalization.CultureInfo.CurrentCulture))
                    {
                        compiledText[i] = Regex.Replace(compiledText[i], @"\bпока\b", "#w");

                        foreach (string s in VariablesList.Keys)
                        {
                            compiledText[i] = Regex.Replace(compiledText[i], @"\b" + s + @"(\b|=)", m => string.Format(@"${0}", m.Value));
                        }

                        compiledText[i] = compiledText[i].Replace(" ", "");
                        compiledText[i] += ';';
                        adretack.Push(i);
                        continue;
                    }

                    //if
                    if (compiledText[i].StartsWith("если ", false, System.Globalization.CultureInfo.CurrentCulture))
                    {
                        compiledText[i] = Regex.Replace(compiledText[i], @"\bесли\b", "#i");

                        //foreach (string s in VariablesList.Keys)
                        //{
                        //    compiledText[i] = Regex.Replace(compiledText[i], @"\b" + s + @"(\b|=)", m => string.Format(@"${0}", m.Value));
                        //}
                        if (compiledText[i].Contains("="))
                        {
                            string[] comp1 = compiledText[i].Split(new string[] { "=" }, StringSplitOptions.RemoveEmptyEntries);
                            if (comp1.Length > 2) compiledText[i] = compiledText[i].Replace(" ", "");
                            else
                            {
                                compiledText[i] = comp1[0].Replace(" ", "") + "=" + comp1[1];
                            }


                        }else
                        compiledText[i] = compiledText[i].Replace(" ", "");
                        compiledText[i] += ';';
                        adretack.Push(i);
                        //continue;
                    }

                    //end
                    if (compiledText[i].StartsWith("конец", false, System.Globalization.CultureInfo.CurrentCulture))
                    {
                        int id = adretack.Pop();
                        compiledText[i] = "&" + id.ToString();
                        compiledText[id] += i.ToString();
                        continue;
                    }

                    //else
                    if (compiledText[i] == "иначе")
                    {
                        int id = adretack.Pop();
                        compiledText[i] = "&" + id.ToString();
                        compiledText[id] += i.ToString() + ';';
                        adretack.Push(id);
                        continue;
                    }
                    string patternstring = "\\^(.*?)\\^";
                    //string patternstring = "\"(.*?)\"";// старый вариант
                    //string patternstring ="((\"\\^.*?\\^\")|(\"(?!\\^).*?\")|(\\^(?!\").*?\\^))";
                    //(\b|\()                   
                    //string patternstring = "(\"+?.*\")";   
                    //string patternstring = "(\"\\/\".*?\\/\"\")|(\".*?\")";
                    //string patternstring = "(\"\\/\".*?\\/\"\")|(\".*?\")";
                    //string patternstring = "((\"\\/\".*?\\/\"\")|(\".*?\"))";
                    Regex regstring = new Regex(patternstring);
                    MatchCollection matchstring = regstring.Matches(compiledText[i]);                    
                    List<int> addToListstring = new List<int>();
                    for (int j = 0; j < matchstring.Count; j++)
                    {
                        //if ((matchstring[j].Value == null) || (matchstring[j].Value.Length < 1)) continue;
                        StringsList.Add((++stringscounter).ToString(), matchstring[j].Groups[1].Value);                       
                        string replaced = @matchstring[j].Value;
                        string replaces = '^' + stringscounter.ToString();
                        //compiledText[i] = Regex.Replace(compiledText[i], TEST, m => string.Format("^{0}", stringscounter));
                        compiledText[i] = compiledText[i].Replace(replaced, replaces);
                        /*if ((StringsList[stringscounter.ToString()] != "") && (StringsList[stringscounter.ToString()] != null))
                        { 
                            string spl=StringsList[stringscounter.ToString()];
                            if (spl[0] == '"') spl.Remove(0, 1);
                            if (spl[spl.Length - 1] == '"') spl.Remove(spl.Length-1, 1);
                            StringsList[stringscounter.ToString()] = spl;
                            StringsList[stringscounter.ToString()] = StringsList[stringscounter.ToString()].Replace("/\"", "\"");
                        }*/  
                    }

                    string pattern = @"([0-9]*[a-zA-Zа-яА-Я_]+[a-zA-Zа-яА-Я0-9]*)\b(?!\s*\()";
                    Regex reg = new Regex(pattern);
                    MatchCollection match = reg.Matches(compiledText[i]);
                    List<int> addToList = new List<int>();
                    for (int j = 0; j < match.Count; j++)
                    {
                        if (!VariablesList.ContainsKey(match[j].Value) && match[j].Value != "и" && match[j].Value != "или" && match[j].Value != "Сортировать" && !rate(match[j].Value))
                        {
                            bool found = false;
                            foreach(SpecialMacro sp in f.specMacro)
                            {
                                if (match[j].Value == (sp.ShortText.Contains("[,]") ? sp.ShortText.Substring(0, sp.ShortText.Length - 3) : sp.ShortText))
                                { 
                                    found = true; break;
                                }
                            }
                            if(!found)
                                return new CompilationResult() { Result = CompilationResult.Compilation_answer.failed, ErrorText = "Неизвестная переменная '" + match[j].Value + "'", FirstFailedLine = i + 1 };
                            else
                                addToList.Add(match[j].Index);
                        }
                        else
                        {
                            addToList.Add(match[j].Index);
                        }
                    }
                    for (int j = 0; j < addToList.Count; j++)
                    {
                        compiledText[i] = compiledText[i].Insert(addToList[j] + j, "$");
                    }


                    //если не опознана команда
                    if (compiledText[i][0] != '#' && compiledText[i][0] != '%' && compiledText[i][0] != '$' && compiledText[i][0] != '&' && !compiledText[i].StartsWith("ignore")
                        && !compiledText[i].StartsWith("Сортировать"))
                    {
                        return new CompilationResult() { Result = CompilationResult.Compilation_answer.failed, ErrorText = "Ошибка компиляции печатной формы. Неопознанная команда.", FirstFailedLine = i + 1};
                    }

                }
            }
            catch (Exception)
            {
                return new CompilationResult() { Result = CompilationResult.Compilation_answer.failed, ErrorText = "Неизвестная ошибка", FirstFailedLine = i + 1};
            }
            return new CompilationResult() { Result = CompilationResult.Compilation_answer.compiled };
        }
        private bool rate(string value)
        {
            switch (value)
            {
                case "ДатаПрописью": return false;
                case "Найти": return false;
                case "Вырезать": return false;
                case "Строк": return false;
                case "Столбцов": return false;
                case "Длина": return false;
            }

            return true;
        }
        //удаляем многострочные комментарии
        private void RemoveMultilineCommments(ref string text)
        {
            string pattern1 = @"(\{.*?\})|(\{.*?)";//оригинал
            //string pattern1 = @"\{(.|\s)*\}";  
            //string pattern1 = @"(/\*.*\*/)|(/\*.*)|(.*\*/)";
            Regex r = new Regex(pattern1, RegexOptions.Singleline);
            MatchCollection m = r.Matches(text);
            foreach (Match match in m)
            {
                MatchCollection newlines = new Regex(@"\n").Matches(match.Value);
                if (newlines.Count > 0)
                {
                    string ig = "";
                    for (int i = 0; i < newlines.Count; i++)
                        ig += "ignore\n";                    
                //text = Regex.Replace(text, match.Value.ToString(), ig);
                    text = text.Replace(match.Value.ToString(), ig);
                }
                else
                    text = text.Replace(match.Value, "");
            }

            string pattern2 = @"(\{.*?\})|(.*\})";
            Regex r2 = new Regex(pattern2, RegexOptions.Singleline | RegexOptions.RightToLeft);
            MatchCollection m2 = r2.Matches(text);
            foreach (Match match in m2)
            {
                MatchCollection newlines = new Regex(@"\n").Matches(match.Value);
                if (newlines.Count > 0)
                {
                    string ig = "";
                    for (int i = 0; i < newlines.Count; i++)
                        ig += "ignore\n";
                    //text = Regex.Replace(text, match.Value, ig);
                    text = text.Replace(match.Value.ToString(), ig);
                }
                else
                    text = text.Replace(match.Value, "");
            }

            
           /* string pattern3 = @"(\^.*?)";//оригинал           
            Regex r3 = new Regex(pattern3, RegexOptions.Singleline);
            MatchCollection m3 = r3.Matches(text);
            foreach (Match match in m3)
            {
                MatchCollection newlines = new Regex(@"\n").Matches(match.Value);
                if (newlines.Count > 0)
                {
                    string temp = match.Value.ToString();
                    string ig = "";
                    for (int i = 0; i < newlines.Count; i++)
                        ig += "ignore\n";                    
                    text = text.Replace(match.Value.ToString(), ig);
                }
                else
                    text = text.Replace(match.Value, "");
            }*/

            /*string pattern4 = @"(.*\^)";
            Regex r4 = new Regex(pattern4, RegexOptions.Singleline | RegexOptions.RightToLeft);
            MatchCollection m4 = r4.Matches(text);
            foreach (Match match in m4)
            {
                MatchCollection newlines = new Regex(@"\n").Matches(match.Value);
                if (newlines.Count > 0)
                {
                    string ig = "";
                    for (int i = 0; i < newlines.Count; i++)
                        ig += "ignore\n";
                    //text = Regex.Replace(text, match.Value, ig);
                    text = text.Replace(match.Value.ToString(), ig);
                }
                else
                    text = text.Replace(match.Value, "");
            }*/
           
        }
       

        //определяет тип переменной и возвращает значение
        private object Qualify(string s)
        {
            switch (s[0])
            {
                // $ - переменная
                case '$': return VariablesList[s.Substring(1, s.Length - 1)];

                // ^ - константа
                case '^': return Convert.ToInt32(s.Substring(1, s.Length - 1));
            }
            return Int32.MinValue;
            
        }

        //распознавание команд
        private string RecognizeCommands(string input)
        {
            //input =  Regex.Replace(input, "test", "53");
            input =  Regex.Replace(input, @"(\s|\A)Формат А3(\b|\()", "\n%0");
            input =  Regex.Replace(input, @"(\s|\A)Формат А4(\b|\()", "\n%1");
            input =  Regex.Replace(input, @"(\s|\A)Формат А5(\b|\()", "\n%2");
            input =  Regex.Replace(input, @"(\s|\A)Формат письмо(\b|\()", "\n%3");
            input = Regex.Replace(input, @"(\s|\A)Работа EXCEL(\b|\()", "\n%8");
            input =  Regex.Replace(input, @"(\s|\A)Создать книгу EXCEL(\b|\()", "\n%18");
            input = Regex.Replace(input, @"(\s|\A)Создать альбом EXCEL(\b|\()", "\n%18");
            input = Regex.Replace(input, @"(\s|\A)Создать книгу OPEN(\b|\()", "\n%1800");
            input = Regex.Replace(input, @"(\s|\A)Создать альбом OPEN(\b|\()", "\n%1800");
            input = Regex.Replace(input, @"(\s|\A)Сохранить EXCEL(\b|\()", "\n%20"); // (\s\d+\s;\s\d+\s;\s\d+\s;\s\d+\s;\s\d+\s\)
            input =  Regex.Replace(input, @"(\s|\A)Открыть EXCEL(\b|\()", "\n%21");
            input =  Regex.Replace(input, @"(\s|\A)Ширина колонки(\b|\()", "\n%34");
            input =  Regex.Replace(input, @"(\s|\A)Ширина колонок(\b|\()", "\n%35");
            input =  Regex.Replace(input, @"(\s|\A)Высота строки(\b|\()", "\n%36");
            input =  Regex.Replace(input, @"(\s|\A)Высота строк(\b|\()", "\n%37");
            input =  Regex.Replace(input, @"(\s|\A)Верхний колонтитул слева(\b|\()", "\n%27");
            input =  Regex.Replace(input, @"(\s|\A)Верхний колонтитул по центру(\b|\()", "\n%28");
            input =  Regex.Replace(input, @"(\s|\A)Верхний колонтитул справа(\b|\()", "\n%29");
            input =  Regex.Replace(input, @"(\s|\A)Нижний колонтитул слева(\b|\()", "\n%30");
            input =  Regex.Replace(input, @"(\s|\A)Нижний колонтитул по центру(\b|\()", "\n%31");
            input =  Regex.Replace(input, @"(\s|\A)Нижний колонтитул по справа(\b|\()", "\n%32");
            input =  Regex.Replace(input, @"(\s|\A)Текст ячейки(\b|\()", "\n%15");
            input =  Regex.Replace(input, @"(\s|\A)Вывод фото в ячейку(\b|\()", "\n%12");
            input =  Regex.Replace(input, @"(\s|\A)Вывод герба в ячейку(\b|\()", "\n%13");
            input =  Regex.Replace(input, @"(\s|\A)Размер шрифта строки(\b|\()", "\n%47");
            input =  Regex.Replace(input, @"(\s|\A)Размер шрифта колонки(\b|\()", "\n%48");
            input =  Regex.Replace(input, @"(\s|\A)Размер шрифта ячейки(\b|\()", "\n%49");
            input =  Regex.Replace(input, @"(\s|\A)Название шрифта строки(\b|\()", "\n%50");
            input =  Regex.Replace(input, @"(\s|\A)Название шрифта колонки(\b|\()", "\n%51");
            input =  Regex.Replace(input, @"(\s|\A)Название шрифта ячейки(\b|\()", "\n%52");
            input =  Regex.Replace(input, @"(\s|\A)Жирный шрифт строки(\b|\()", "\n%77");
            input =  Regex.Replace(input, @"(\s|\A)Жирный шрифт строки отмена(\b|\()", "\n%78");
            input =  Regex.Replace(input, @"(\s|\A)Жирный шрифт колонки(\b|\()", "\n%79");
            input =  Regex.Replace(input, @"(\s|\A)Жирный шрифт колонки отмена(\b|\()", "\n%80");
            input =  Regex.Replace(input, @"(\s|\A)Жирный шрифт ячейки(\b|\()", "\n%81");
            input =  Regex.Replace(input, @"(\s|\A)Жирный шрифт ячейки отмена(\b|\()", "\n%82");
            input =  Regex.Replace(input, @"(\s|\A)Подчеркивание шрифта строки тонкой линией(\b|\()", "\n%89");
            input =  Regex.Replace(input, @"(\s|\A)Подчеркивание шрифта строки толстой линией(\b|\()", "\n%90");
            input =  Regex.Replace(input, @"(\s|\A)Подчеркивание шрифта строки без линии(\b|\()", "\n%91");
            input =  Regex.Replace(input, @"(\s|\A)Подчеркивание шрифта колонки тонкой линией(\b|\()", "\n%92");
            input =  Regex.Replace(input, @"(\s|\A)Подчеркивание шрифта колонки толстой линией(\b|\()", "\n%93");
            input =  Regex.Replace(input, @"(\s|\A)Подчеркивание шрифта колонки без линии(\b|\()", "\n%94");
            input =  Regex.Replace(input, @"(\s|\A)Подчеркивание шрифта ячейки тонкой линией(\b|\()", "\n%95");
            input =  Regex.Replace(input, @"(\s|\A)Подчеркивание шрифта ячейки толстой линией(\b|\()", "\n%96");
            input =  Regex.Replace(input, @"(\s|\A)Подчеркивание шрифта ячейки без линии(\b|\()", "\n%97");
            input =  Regex.Replace(input, @"(\s|\A)Наклонный шрифт строки(\b|\()", "\n%83");
            input =  Regex.Replace(input, @"(\s|\A)Наклонный шрифт строки отмена(\b|\()", "\n%84");
            input =  Regex.Replace(input, @"(\s|\A)Наклонный шрифт колонки(\b|\()", "\n%85");
            input =  Regex.Replace(input, @"(\s|\A)Наклонный шрифт колонки отмена(\b|\()", "\n%86");
            input =  Regex.Replace(input, @"(\s|\A)Наклонный шрифт ячейки(\b|\()", "\n%87");
            input =  Regex.Replace(input, @"(\s|\A)Наклонный шрифт ячейки отмена(\b|\()", "\n%88");
            input =  Regex.Replace(input, @"(\s|\A)Объединение ячеек(\b|\()", "\n%33");
            input =  Regex.Replace(input, @"(\s|\A)Размер символов в ячейке(\b|\()", "\n%185");
            input =  Regex.Replace(input, @"(\s|\A)Жирность символов в ячейке(\b|\()", "\n%181");
            input =  Regex.Replace(input, @"(\s|\A)Жирность символов в ячейке отмена(\b|\()", "\n%182");
            input =  Regex.Replace(input, @"(\s|\A)Наклонность символов в ячейке(\b|\()", "\n%183");
            input =  Regex.Replace(input, @"(\s|\A)Наклонность символов в ячейке отмена(\b|\()", "\n%184");
            input =  Regex.Replace(input, @"(\s|\A)Подчеркивание символов в ячейке тонкой линией(\b|\()", "\n%179");
            input =  Regex.Replace(input, @"(\s|\A)Подчеркивание символов в ячейке толстой линией(\b|\()", "\n%180");
            input =  Regex.Replace(input, @"(\s|\A)Подчеркивание символов в ячейке отмена(\b|\()", "\n%181");
            input =  Regex.Replace(input, @"(\s|\A)Выравнивание по горизонтали строки по значению(\b|\()", "\n%65");
            input =  Regex.Replace(input, @"(\s|\A)Выравнивание по горизонтали строки по левому краю(\b|\()", "\n%66");
            input =  Regex.Replace(input, @"(\s|\A)Выравнивание по горизонтали строки по центру(\b|\()", "\n%67");
            input =  Regex.Replace(input, @"(\s|\A)Выравнивание по горизонтали строки по правому краю(\b|\()", "\n%68");
            input =  Regex.Replace(input, @"(\s|\A)Выравнивание по горизонтали колонки по значению(\b|\()", "\n%69");
            input =  Regex.Replace(input, @"(\s|\A)Выравнивание по горизонтали колонки по левому краю(\b|\()", "\n%70");
            input =  Regex.Replace(input, @"(\s|\A)Выравнивание по горизонтали колонки по центру(\b|\()", "\n%71");
            input =  Regex.Replace(input, @"(\s|\A)Выравнивание по горизонтали колонки по правому краю(\b|\()", "\n%72");
            input =  Regex.Replace(input, @"(\s|\A)Выравнивание по горизонтали ячейки по значению(\b|\()", "\n%73");
            input =  Regex.Replace(input, @"(\s|\A)Выравнивание по горизонтали ячейки по левому краю(\b|\()", "\n%74");
            input =  Regex.Replace(input, @"(\s|\A)Выравнивание по горизонтали ячейки по центру(\b|\()", "\n%75");
            input =  Regex.Replace(input, @"(\s|\A)Выравнивание по горизонтали ячейки по правому краю(\b|\()", "\n%76");
            input =  Regex.Replace(input, @"(\s|\A)Выравнивание по вертикали строки по верхнему краю(\b|\()", "\n%53");
            input =  Regex.Replace(input, @"(\s|\A)Выравнивание по вертикали строки по центру(\b|\()", "\n%54");
            input =  Regex.Replace(input, @"(\s|\A)Выравнивание по вертикали строки по нижнему краю(\b|\()", "\n%55");
            input =  Regex.Replace(input, @"(\s|\A)Выравнивание по вертикали строки по высоте(\b|\()", "\n%56");
            input =  Regex.Replace(input, @"(\s|\A)Выравнивание по вертикали колонки по верхнему краю(\b|\()", "\n%57");
            input =  Regex.Replace(input, @"(\s|\A)Выравнивание по вертикали колонки по центру(\b|\()", "\n%58");
            input =  Regex.Replace(input, @"(\s|\A)Выравнивание по вертикали колонки по нижнему краю(\b|\()", "\n%59");
            input =  Regex.Replace(input, @"(\s|\A)Выравнивание по вертикали колонки по высоте(\b|\()", "\n%60");
            input =  Regex.Replace(input, @"(\s|\A)Выравнивание по вертикали ячейки по верхнему краю(\b|\()", "\n%61");
            input =  Regex.Replace(input, @"(\s|\A)Выравнивание по вертикали ячейки по центру(\b|\()", "\n%62");
            input =  Regex.Replace(input, @"(\s|\A)Выравнивание по вертикали ячейки по нижнему краю(\b|\()", "\n%63");
            input =  Regex.Replace(input, @"(\s|\A)Выравнивание по вертикали ячейки по высоте(\b|\()", "\n%64");
            input =  Regex.Replace(input, @"(\s|\A)Линия всей ячейки отмена(\b|\()", "\n%101");
            input =  Regex.Replace(input, @"(\s|\A)Линия всей ячейки тонкая(\b|\()", "\n%99");
            input =  Regex.Replace(input, @"(\s|\A)Линия всей ячейки толстая(\b|\()", "\n%100");
            input =  Regex.Replace(input, @"(\s|\A)Линия всей ячейки двойная(\b|\()", "\n%102");
            input =  Regex.Replace(input, @"(\s|\A)Линия всей ячейки пунктирная(\b|\()", "\n%103");
            input =  Regex.Replace(input, @"(\s|\A)Линия всей ячейки точками(\b|\()", "\n%104");
            input =  Regex.Replace(input, @"(\s|\A)Линия вокруг ячейки отмена(\b|\()", "\n%108");
            input =  Regex.Replace(input, @"(\s|\A)Линия вокруг ячейки тонкая(\b|\()", "\n%106");
            input =  Regex.Replace(input, @"(\s|\A)Линия вокруг ячейки толстая(\b|\()", "\n%107");
            input =  Regex.Replace(input, @"(\s|\A)Линия вокруг ячейки двойная(\b|\()", "\n%109");
            input =  Regex.Replace(input, @"(\s|\A)Линия вокруг ячейки пунктирная(\b|\()", "\n%110");
            input =  Regex.Replace(input, @"(\s|\A)Линия вокруг ячейки точками(\b|\()", "\n%111");
            input =  Regex.Replace(input, @"(\s|\A)Линия ячейки сверху отмена(\b|\()", "\n%115");
            input =  Regex.Replace(input, @"(\s|\A)Линия ячейки сверху тонкая(\b|\()", "\n%113");
            input =  Regex.Replace(input, @"(\s|\A)Линия ячейки сверху толстая(\b|\()", "\n%114");
            input =  Regex.Replace(input, @"(\s|\A)Линия ячейки сверху двойная(\b|\()", "\n%116");
            input =  Regex.Replace(input, @"(\s|\A)Линия ячейки сверху пунктирная(\b|\()", "\n%117");
            input =  Regex.Replace(input, @"(\s|\A)Линия ячейки сверху точками(\b|\()", "\n%118");
            input =  Regex.Replace(input, @"(\s|\A)Линия ячейки слева отмена(\b|\()", "\n%122");
            input =  Regex.Replace(input, @"(\s|\A)Линия ячейки слева тонкая(\b|\()", "\n%120");
            input =  Regex.Replace(input, @"(\s|\A)Линия ячейки слева толстая(\b|\()", "\n%121");
            input =  Regex.Replace(input, @"(\s|\A)Линия ячейки слева двойная(\b|\()", "\n%123");
            input =  Regex.Replace(input, @"(\s|\A)Линия ячейки слева пунктирная(\b|\()", "\n%124");
            input =  Regex.Replace(input, @"(\s|\A)Линия ячейки слева точками(\b|\()", "\n%125");
            input =  Regex.Replace(input, @"(\s|\A)Линия ячейки снизу отмена(\b|\()", "\n%129");
            input =  Regex.Replace(input, @"(\s|\A)Линия ячейки снизу тонкая(\b|\()", "\n%127");
            input =  Regex.Replace(input, @"(\s|\A)Линия ячейки снизу толстая(\b|\()", "\n%128");
            input =  Regex.Replace(input, @"(\s|\A)Линия ячейки снизу двойная(\b|\()", "\n%130");
            input =  Regex.Replace(input, @"(\s|\A)Линия ячейки снизу пунктирная(\b|\()", "\n%131");
            input =  Regex.Replace(input, @"(\s|\A)Линия ячейки снизу точками(\b|\()", "\n%132");
            input =  Regex.Replace(input, @"(\s|\A)Линия ячейки справа отмена(\b|\()", "\n%136");
            input =  Regex.Replace(input, @"(\s|\A)Линия ячейки справа тонкая(\b|\()", "\n%134");
            input =  Regex.Replace(input, @"(\s|\A)Линия ячейки справа толстая(\b|\()", "\n%135");
            input =  Regex.Replace(input, @"(\s|\A)Линия ячейки справа двойная(\b|\()", "\n%137");
            input =  Regex.Replace(input, @"(\s|\A)Линия ячейки справа пунктирная(\b|\()", "\n%138");
            input =  Regex.Replace(input, @"(\s|\A)Линия ячейки справа точками(\b|\()", "\n%139");
            input =  Regex.Replace(input, @"(\s|\A)Толщина линии всей ячейки 1(\b|\()", "\n%140");
            input =  Regex.Replace(input, @"(\s|\A)Толщина линии всей ячейки 2(\b|\()", "\n%141");
            input =  Regex.Replace(input, @"(\s|\A)Толщина линии всей ячейки 3(\b|\()", "\n%142");
            input =  Regex.Replace(input, @"(\s|\A)Толщина линии всей ячейки 4(\b|\()", "\n%143");
            input =  Regex.Replace(input, @"(\s|\A)Толщина линии вокруг ячейки 1(\b|\()", "\n%144");
            input =  Regex.Replace(input, @"(\s|\A)Толщина линии вокруг ячейки 2(\b|\()", "\n%145");
            input =  Regex.Replace(input, @"(\s|\A)Толщина линии вокруг ячейки 3(\b|\()", "\n%146");
            input =  Regex.Replace(input, @"(\s|\A)Толщина линии вокруг ячейки 4(\b|\()", "\n%147");
            input =  Regex.Replace(input, @"(\s|\A)Толщина линии сверху ячейки 1(\b|\()", "\n%148");
            input =  Regex.Replace(input, @"(\s|\A)Толщина линии сверху ячейки 2(\b|\()", "\n%150");
            input =  Regex.Replace(input, @"(\s|\A)Толщина линии сверху ячейки 3(\b|\()", "\n%151");
            input =  Regex.Replace(input, @"(\s|\A)Толщина линии сверху ячейки 4(\b|\()", "\n%152");
            input =  Regex.Replace(input, @"(\s|\A)Толщина линии снизу ячейки 1(\b|\()", "\n%153");
            input =  Regex.Replace(input, @"(\s|\A)Толщина линии снизу ячейки 2(\b|\()", "\n%154");
            input =  Regex.Replace(input, @"(\s|\A)Толщина линии снизу ячейки 3(\b|\()", "\n%155");
            input =  Regex.Replace(input, @"(\s|\A)Толщина линии снизу ячейки 4(\b|\()", "\n%156");
            input =  Regex.Replace(input, @"(\s|\A)Толщина линии слева ячейки 1(\b|\()", "\n%157");
            input =  Regex.Replace(input, @"(\s|\A)Толщина линии слева ячейки 2(\b|\()", "\n%158");
            input =  Regex.Replace(input, @"(\s|\A)Толщина линии слева ячейки 3(\b|\()", "\n%159");
            input =  Regex.Replace(input, @"(\s|\A)Толщина линии слева ячейки 4(\b|\()", "\n%160");
            input =  Regex.Replace(input, @"(\s|\A)Толщина линии справа ячейки 1(\b|\()", "\n%161");
            input =  Regex.Replace(input, @"(\s|\A)Толщина линии справа ячейки 2(\b|\()", "\n%162");
            input =  Regex.Replace(input, @"(\s|\A)Толщина линии справа ячейки 3(\b|\()", "\n%163");
            input =  Regex.Replace(input, @"(\s|\A)Толщина линии справа ячейки 4(\b|\()", "\n%164");
            input =  Regex.Replace(input, @"(\s|\A)Формат строки общий(\b|\()", "\n%165");
            input =  Regex.Replace(input, @"(\s|\A)Формат строки числовой(\b|\()", "\n%166");
            input =  Regex.Replace(input, @"(\s|\A)Формат строки текстовый(\b|\()", "\n%167");
            input =  Regex.Replace(input, @"(\s|\A)Формат строки дата(\b|\()", "\n%168");
            input =  Regex.Replace(input, @"(\s|\A)Формат колонки общий(\b|\()", "\n%169");
            input =  Regex.Replace(input, @"(\s|\A)Формат колонки числовой(\b|\()", "\n%170");
            input =  Regex.Replace(input, @"(\s|\A)Формат колонки текстовый(\b|\()", "\n%171");
            input =  Regex.Replace(input, @"(\s|\A)Формат колонки дата(\b|\()", "\n%172");
            input =  Regex.Replace(input, @"(\s|\A)Формат ячейки общий(\b|\()", "\n%173");
            input =  Regex.Replace(input, @"(\s|\A)Формат ячейки числовой(\b|\()", "\n%174");
            input =  Regex.Replace(input, @"(\s|\A)Формат ячейки текстовый(\b|\()", "\n%175");
            input =  Regex.Replace(input, @"(\s|\A)Формат ячейки дата(\b|\()", "\n%176");
            input =  Regex.Replace(input, @"(\s|\A)Поворот текста строки(\b|\()", "\n%44");
            input =  Regex.Replace(input, @"(\s|\A)Поворот текста колонки(\b|\()", "\n%45");
            input =  Regex.Replace(input, @"(\s|\A)Поворот текста ячейки(\b|\()", "\n%46");
            input =  Regex.Replace(input, @"(\s|\A)Перенос текста строки(\b|\()", "\n%38");
            input =  Regex.Replace(input, @"(\s|\A)Перенос текста колонки(\b|\()", "\n%39");
            input =  Regex.Replace(input, @"(\s|\A)Перенос текста ячейки(\b|\()", "\n%30");
            input =  Regex.Replace(input, @"(\s|\A)Отмена переноса текста строки(\b|\()", "\n%41");
            input =  Regex.Replace(input, @"(\s|\A)Отмена переноса текста колонки(\b|\()", "\n%42");
            input =  Regex.Replace(input, @"(\s|\A)Отмена переноса текста ячейки(\b|\()", "\n%43");
            input =  Regex.Replace(input, @"(\s|\A)Заливка ячейки(\b|\()", "\n%11");
            input =  Regex.Replace(input, @"(\s|\A)Заголовок страниц(\b|\()", "\n%25");
            input =  Regex.Replace(input, @"(\s|\A)Центрировать на странице горизонтально(\b|\()", "\n%6");
            input =  Regex.Replace(input, @"(\s|\A)Центрировать на странице вертикально(\b|\()", "\n%7");
            input =  Regex.Replace(input, @"(\s|\A)Добавить листов(\b|\()", "\n%192");
            input =  Regex.Replace(input, @"(\s|\A)Название листа(\b|\()", "\n%16"); // 193
            input =  Regex.Replace(input, @"(\s|\A)Текущий лист(\b|\()", "\n%17"); // 194
            input =  Regex.Replace(input, @"(\s|\A)Поля листа(\b|\()", "\n%10");
            input =  Regex.Replace(input, @"(\s|\A)Ориентация книга(\b|\()", "\n%4");
            input =  Regex.Replace(input, @"(\s|\A)Ориентация альбом(\b|\()", "\n%5");
            return input;
        }

        //вычисление значения скобочной последовательности
        private double Sequence(string input)
        {
            if (string.IsNullOrEmpty(input))
                return 0;
            input = Sep(input);
            Stack<string> stack = new Stack<string>();
            Stack<double?> result = new Stack<double?>();
            List<object> list = new List<object>();
            string number = null;
            input = input.Replace(" ", "");
            //преобразовует 4-5 в 4+-5
            //каждое действие должно иметь знак и 2 операндa
            for (int i = 1; i < input.Length - 1; i++)
            {
                if (input[i] == '-' && char.IsDigit(input[i - 1]))
                {
                    input = input.Insert(i, "+");
                }
            }

            //преобразовует инфиксную в постфиксую запись
            for(int i = 0; i < input.Length; i++)
            {
                if (i < input.Length - 1 && ((input[i] == '-' && (char.IsDigit(input[i+1]) || input[i + 1] == ','))
                    || (char.IsDigit(input[i]) && (char.IsDigit(input[i + 1]) || input[i + 1] == ',')) || input[i] == ','))
                {
                    number += input[i];
                    continue;
                }
                else if (char.IsDigit(input[i]))
                {
                    number += input[i];
                    list.Add(number);
                    number = string.Empty;
                    continue;
                }

                else if (input[i] == '(')
                {
                    stack.Push("(");
                    continue;
                }
                else if (input[i] == ')')
                {
                    bool perform = true;
                    string a = null;
                    while (perform)
                    {
                        if (stack.Count > 0)
                        {
                            a = stack.Pop();
                            if (a != "(")
                                list.Add(a);
                        }
                        else perform = false;
                    }
                }

                else if (input[i] == '+' || input[i] == '-' || input[i] == '*' || input[i] == '/')
                {
                    for (int j = i; j >= 0; j--)
                    {
                        if (stack.Count > 0)
                        {
                            string p = stack.Peek();
                            if (p == "(")
                            {
                                // ??? POP ???
                                continue;
                            }
                            if (input[i] == '+' || input[i] == '-')
                            {
                                list.Add(p);
                                stack.Pop();
                            }
                            else if ((input[i] == '*' || input[i] == '/') && (p == "*" || p == "/"))
                            {
                                list.Add(p);
                                stack.Pop();
                            }
                        }
                        else break;
                    }
                    stack.Push(input[i].ToString());
                }
            }

            while (stack.Count > 0)
            {
                list.Add(stack.Pop());
            }


            //производит вычисления в постфиксной записи
            //вывод - последнее и единственное число в стеке
            foreach (object obj in list)
            {
                double n;
                bool isNumeric = double.TryParse(obj as string, out n);
                if (isNumeric)
                {
                    result.Push(n as double?);
                }
                else
                {
                    if (result.Count == 0)
                        return 0;
                    double? a = result.Pop() as double?;
                    if (result.Count == 0)
                        return 0;
                    double? b = result.Pop() as double?;
                    double c = 0;
                    switch (obj as string)
                    {
                        case "+": c = b.Value + a.Value;
                            break;
                        case "-": c = b.Value - a.Value;
                            break;
                        case "*": c = b.Value * a.Value;
                            break;
                        case "/": c = b.Value / a.Value;
                            break;
                    }
                    result.Push(c);
                }
            }
            return result.Pop()??0;
        }

        //запрос в базу данных
        private object ToDataBase()
        {
            object[,] res = new object[0, 0];

            Regex r = new Regex(@"@([a-zA-Z][a-zA-Z0-9_]*)");
            MatchCollection m =  r.Matches(DataBaseRequest);
            using (FbConnection SelectConnection = new FbConnection(connectionString))
            {
                SelectConnection.Open();
                FbCommand command = new FbCommand(DataBaseRequest, SelectConnection);
                if (m.Count == param.Count())
                {
                    for (int i = 0; i < m.Count; i++ )
                    {
                        command.Parameters.Add(m[i].Value, param[0]);
                    }
                }
                else
                {
                    MessageBox.Show("Не совпадает количество параметров в запросе к базе данных",
                        "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return null;
                }
                FbDataReader reader = command.ExecuteReader();
                int k = 0;
                while (reader.Read())
                {
                    res = ResizeArray<object>(res, k + 1, reader.FieldCount);
                    for (int j = 0; j < reader.FieldCount; j++)
                    {
                        res[k, j] = reader.GetValue(j);
                    }
                    k++;
                }
            }
            Array.Clear(param, 0, 0);
            return res;
        }

        //изменение размеров массива
        private T[,] ResizeArray<T>(T[,] original, int rows, int cols)
        {
            var newArray = new T[rows, cols];
            int minRows = Math.Min(rows, original.GetLength(0));
            int minCols = Math.Min(cols, original.GetLength(1));
            for (int i = 0; i < minRows; i++)
                for (int j = 0; j < minCols; j++)
                    newArray[i, j] = original[i, j];
            return newArray;
        }

        //оператор из строки( <> < > = <= >=)
        private bool Comparative(string operat, double val1, double val2)
        {
            switch (operat)
            {
                case ">": return (int)val1 > (int)val2;
                case "<": return (int)val1 < (int)val2;
                case ">=": return (int)val1 >= (int)val2;
                case "<=": return (int)val1 <= (int)val2;
                case "<>": return (int)val1 != (int)val2;
            }
            return false;
        }

        //определить значение логического выражения
        private char GetBooleanValue(string input)
        {
            //формируем оператор сравнения
            string operat = "";
            int c = input.IndexOfAny(new char[] { '=', '<', '>' }, 0);
            operat += input[c];
            if (input[c + 1] == '>' || input[c + 1] == '=')
            {
                operat += input[c + 1];
            }

            input = input.Substring(1, input.Length - 2);
            string[] p = input.Split(new string[] { "<", "=", ">", "<=", ">=", "<>" }, StringSplitOptions.RemoveEmptyEntries);
            if (StringIsNumeric(p[0]) && StringIsNumeric(p[1]))
            {
                switch (operat)
                {
                    case ">": return ((int)(Sequence(RecognizeVariables(p[0]))) > (int)(Sequence(RecognizeVariables(p[1])))) ? '1' : '0';
                    case "<": return ((int)Sequence(RecognizeVariables(p[0])) < (int)Sequence(RecognizeVariables(p[1]))) ? '1' : '0';
                    case ">=": return ((int)Sequence(RecognizeVariables(p[0])) >= (int)Sequence(RecognizeVariables(p[1]))) ? '1' : '0';
                    case "=": return ((int)Sequence(RecognizeVariables(p[0])) == (int)Sequence(RecognizeVariables(p[1]))) ? '1' : '0';
                    case "<=": return ((int)Sequence(RecognizeVariables(p[0])) <= (int)Sequence(RecognizeVariables(p[1]))) ? '1' : '0';
                    case "<>": return ((int)Sequence(RecognizeVariables(p[0])) != (int)Sequence(RecognizeVariables(p[1])) ? '1' : '0');
                    default: return '0';
                }
            }
            else if (!StringIsNumeric(p[0]) && !StringIsNumeric(p[1]))
            {
                switch (operat)
                {
                    case ">": return (SequenceString(p[0]).Length > SequenceString(p[1]).Length) ? '1' : '0';
                    case "<": return (SequenceString(p[0]).Length < SequenceString(p[1]).Length) ? '1' : '0';
                    case ">=": return (SequenceString(p[0]).Length >= SequenceString(p[1]).Length) ? '1' : '0'; ;
                    case "=": return (SequenceString(p[0]) == SequenceString(p[1])) ? '1' : '0'; ;
                    case "<=": return (SequenceString(p[0]).Length <= SequenceString(p[1]).Length) ? '1' : '0'; ;
                    case "<>": return (SequenceString(p[0]) != SequenceString(p[1])) ? '1' : '0'; ;
                    default: return '0';
                }
                return (SequenceString(p[0]) == SequenceString(p[1])) ? '1' : '0';
            }
            else return '0';
        }

        private string RecognizeFunctions(string input)
        {
            string output = input;
            input = Regex.Replace(input, @"(ДатаПрописью(\(.+?\)))", m => (DataP(m.Groups[2].Value)));
            input = Regex.Replace(input, @"(Найти(\(.+?\)))", m => (Find(m.Groups[2].Value, true)));
            input = Regex.Replace(input, @"(Строк(\(.+?\)))", m => (RowsCount(m.Groups[2].Value)));
            input = Regex.Replace(input, @"(Столбцов(\(.+?\)))", m => (ColumnsCount(m.Groups[2].Value)));
            input = Regex.Replace(input, @"(Длина(\(.+?\)))", m => (LengthCommand(m.Groups[2].Value, true)));
            if (output != input)
                input = RecognizeFunctions(input);
            return input;
        }

        //извлечь значения для переменных
        private string RecognizeVariables(string input)
        {
            input = input.Replace('^', '\0');
            //числа
            input = Regex.Replace(input, @"(ДатаПрописью(\(.+?\)))", m => (DataP(m.Groups[2].Value)));
            input = Regex.Replace(input, @"(Найти(\(.+?\)))", m => (Find(m.Groups[2].Value, true)));
            input = Regex.Replace(input, @"(Строк(\(.+?\)))", m => (RowsCount(m.Groups[2].Value)));
            input = Regex.Replace(input, @"(Столбцов(\(.+?\)))", m => (ColumnsCount(m.Groups[2].Value)));
            input = Regex.Replace(input, @"(Длина(\(.+?\)))", m => (LengthCommand(m.Groups[2].Value,true)));
            foreach (string st in VariablesList.Keys)
            {
                if (VariablesList[st] is double || VariablesList[st] is string || VariablesList[st] is DateTime)
                {
                    input = Regex.Replace(input, @"\$" + st + @"\b", VariablesList[st].ToString());
                    input = Regex.Replace(input, @"\b" + st + @"\b", VariablesList[st].ToString());
                }
                else
                {
                    string pattern = @"\$" + st + @"\[\s*((?:[^\[\]]|(?<o>\[)|(?<-o>\]))+(?(o)(?!))),\s*((?:[^\[\]]|(?<o>\[)|(?<-o>\]))+(?(o)(?!)))\]";
                    if (VariablesList[st] is double[,])
                    {
                        input = Regex.Replace(input, pattern,
                            m => ((VariablesList[st] as double[,])[Convert.ToInt32(Sequence(RecognizeVariables(m.Groups[1].Value))),
                                Convert.ToInt32(Sequence(RecognizeVariables(m.Groups[2].Value)))].ToString()));
                    }
                    else if (VariablesList[st] is string[,])
                    {
                        input = Regex.Replace(input, pattern,
                                m =>
                                    (((VariablesList[st] as string[,])[Convert.ToInt32(Sequence(RecognizeVariables(m.Groups[1].Value))),
                                    Convert.ToInt32(Sequence(RecognizeVariables(m.Groups[2].Value)))] == null)?"0":
                                    (VariablesList[st] as string[,])[Convert.ToInt32(Sequence(RecognizeVariables(m.Groups[1].Value))),
                                    Convert.ToInt32(Sequence(RecognizeVariables(m.Groups[2].Value)))].ToString()));
                    }
                }
            }
            foreach (SpecialMacro sm in f.specMacro)
            {
                if (!sm.ShortText.Contains("["))
                {
                    input = Regex.Replace(input, @"\$" + sm.ShortText, sm.value.ToString());
                }
                else
                {
                    string[] p = sm.ShortText.Split('[');
                    string pattern = @"\$" + p[0] + @"\[\s*((?:[^\[\]]|(?<o>\[)|(?<-o>\]))+(?(o)(?!))),\s*((?:[^\[\]]|(?<o>\[)|(?<-o>\]))+(?(o)(?!)))\]";
                    if (sm.value is string[,])
                        input = Regex.Replace(input, pattern,
                            m => ((sm.value as string[,])[Convert.ToInt32(Sequence(RecognizeVariables(m.Groups[1].Value))),
                                Convert.ToInt32(Sequence(RecognizeVariables(m.Groups[2].Value)))].ToString()));
                    else if (sm.value is double[,])
                        input = Regex.Replace(input, pattern,
                            m => ((sm.value as double[,])[Convert.ToInt32(Sequence(RecognizeVariables(m.Groups[1].Value))),
                                Convert.ToInt32(Sequence(RecognizeVariables(m.Groups[2].Value)))].ToString()));

                }
            }
            return input;
        }

        private string DataP(string p,bool str) // // ДатаПрописью($m1)=^201.01.2008\")
        {           
            {
                string variable = p;
                if (p.StartsWith("ДатаПрописью($"))
                {
                    p = p.Replace("$", "");
                    variable = p;
                    p = p.Remove(0, 13);
                    p = p.Remove(p.Length - 1, 1);
                    if (VariablesList.ContainsKey(p))
                    {
                        if (VariablesList[p] is double[,])
                        {

                            return conData((VariablesList[p] as double[,]).ToString(), false);
                        }
                        if (VariablesList[p] is string[,])
                        {

                            return conData((VariablesList[p] as string[,]).ToString(), false);
                        }

                        return conData(VariablesList[p].ToString(), false);
                    }
                    else
                    {
                        string p1; int i = 0, j = 0;
                        bool mass = false;
                        if (p[p.Length - 1] == ']') mass = true;
                        if (mass)
                        {
                            variable = p;
                            p1 = p.Substring(p.IndexOf('[') + 1, p.IndexOf(']'));
                            string[] pMass = p1.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                            i = int.Parse(pMass[0]);
                            j = int.Parse(pMass[1]);
                        }
                        
                        string pp = p.Substring(0, p.IndexOf('['));
                        foreach (SpecialMacro m in f.specMacro)
                            if (m.ShortText.Substring(0, m.ShortText.Count() - 3) == pp)
                            {
                                if (mass)
                                {
                                    if (m.value is double[,])
                                    {
                                        return conData((m.value as double[,]).GetValue(j, i).ToString(), false);
                                    }
                                    if (m.value is string[,])
                                    {
                                        return conData((m.value as string[,]).GetValue(j, i).ToString(), false);
                                    }
                                }
                                else
                                {
                                    return conData(m.value.ToString(), false);
                                }
                            }
                    }
                }
                else  if (p.Contains("^"))
                {                    
                    p = p.Replace("^", "");
                    p = p.Replace("(", "");
                    p = p.Replace(")", "");
                    p = p.Replace("ДатаПрописью", "");
                    p = p.Replace("(", "");
                    p = p.Replace(")", "");
                    p = p.Replace("$", "");
                    return conData(StringsList[p].ToString(), false);
                    
                }

                {
                    p = p.Replace("$", "");
                    if (p[0] == '(')
                        p = p.Remove(0, 1);
                    if (p[p.Length - 1] == ')')
                        p = p.Remove(p.Length - 1);
                    string p1; int i = 0, j = 0;
                    bool mass = false;
                    if (p[p.Length - 1] == ']') mass = true;
                    if (mass)
                    {
                        p1 = p.Substring(p.Length - 4, 3);                       
                        string[] pMass = p1.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                        i = int.Parse(pMass[0]);
                        j = int.Parse(pMass[1]);
                    }

                    string pp = p.Substring(0, p.Length - 5);
                    foreach (SpecialMacro m in f.specMacro)
                        if (m.ShortText.Substring(0, m.ShortText.Count() - 3) == pp)
                        {
                            if (mass)
                            {
                                return conData((m.value as string[,]).GetValue(j, i).ToString(), true);
                            }
                            else
                                return conData(m.value.ToString(), true);
                        }
                }

                p = p.Replace(" ", "");
                p = p.Remove(0, 13);
                p = p.Remove(p.Length - 1, 1);
                p = p.Replace("$", "");
                string[] output = p.Split(new string[] { "." }, StringSplitOptions.RemoveEmptyEntries);
                output[0] = output[0].TrimStart('0');
                output[1] = output[1].TrimStart('0');
                switch (output[1])
                {
                    case "1": output[1] = "января"; break;
                    case "2": output[1] = "февраля"; break;
                    case "3": output[1] = "марта"; break;
                    case "4": output[1] = "апреля"; break;
                    case "5": output[1] = "мая"; break;
                    case "6": output[1] = "июня"; break;
                    case "7": output[1] = "июля"; break;
                    case "8": output[1] = "августа"; break;
                    case "9": output[1] = "сентября"; break;
                    case "10": output[1] = "октября"; break;
                    case "11": output[1] = "ноября"; break;
                    case "12": output[1] = "декабря"; break;
                }
               /* switch (output[1])
                {
                    case "1": output[1] = "січень"; break;
                    case "2": output[1] = "лютий"; break;
                    case "3": output[1] = "березень"; break;
                    case "4": output[1] = "квітень"; break;
                    case "5": output[1] = "травень"; break;
                    case "6": output[1] = "червень"; break;
                    case "7": output[1] = "липень"; break;
                    case "8": output[1] = "серпень"; break;
                    case "9": output[1] = "вересень"; break;
                    case "10": output[1] = "жовтень"; break;
                    case "11": output[1] = "листопад"; break;
                    case "12": output[1] = "грудень"; break;
                }*/

                return output[0] + " " + output[1] + " " + output[2];
            }
            return "0";

        }

        private string Find(string input,bool b)
        {
            input = input.Replace("\0", "^");
            if (b)
            {
                string all = input;
                string ret = input;
                ret = ret.Replace("Найти(", "");
                string[] output = input.Split(new string[] { "Найти", "(", ")" }, StringSplitOptions.RemoveEmptyEntries);
                output[0] = "";
                for (int a = 0, add = 0; a < ret.Length; a++)
                {
                    if (ret[a] == '(') add++;
                    if (ret[a] == ')')
                    {
                        if (add == 0)
                        {
                            break;
                        }
                        else add--;
                    }
                    output[0] = output[0] + ret[a];
                }
                string rGet = "Найти(" + output[0] + ")";
                output[0] = RecognizeFunctions(output[0]);
                output[0] = Regex.Replace(output[0], @"\$", "");     
                string[] str = output[0].Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                string podstroka=""; string stroka="";
                if ((str[0].Contains("[")) && (str[0].Contains("]")))//macros
                {                
                    str[0] = str[0].Remove(0, 6);//Найти(
                    str[0] = str[0].Remove(input.Length - 1, 1);//)
                    str[0] = str[0].Replace("$", "");
                    {
                        string p1; int i = 0, j = 0;
                        bool mass = false;
                        if (str[0][str[0].Length - 1] == ']') mass = true;
                        if (mass)
                        {

                            p1 = str[0].Substring(str[0].IndexOf('[') + 1, 3);
                            string[] pMass = p1.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                            i = int.Parse(pMass[0]);
                            j = int.Parse(pMass[1]);
                        }

                        string pp = str[0].Substring(0, str[0].IndexOf('['));
                        foreach (SpecialMacro m in f.specMacro)
                            if (m.ShortText.Substring(0, m.ShortText.Count() - 3) == pp)
                            {
                                if (mass)
                                {
                                    if (m.value is double[,])
                                    {
                                        podstroka=(m.value as double[,]).GetValue(j, i).ToString();
                                    }
                                    if (m.value is string[,])
                                    {
                                        podstroka=(m.value as string[,]).GetValue(j, i).ToString();
                                    }
                                }
                                else
                                {
                                    podstroka=m.value.ToString();
                                }
                            }
                    }
                }
                else if (str[0].Contains("$"))//peremennaia
                {
                    str[0] = str[0].Replace("$", "");
                    str[0] = str[0].Remove(0, 6);
                    str[0] = str[0].Remove(str[0].Length - 1, 1);
                    if (VariablesList.ContainsKey(str[0]))
                    {
                        if (VariablesList[str[0]] is double[,])
                        {

                            podstroka = (VariablesList[str[0]] as double[,]).ToString();
                        }
                        if (VariablesList[str[0]] is string[,])
                        {

                            podstroka = (VariablesList[str[0]] as string[,]).ToString();
                        }

                        podstroka = VariablesList[str[0]].ToString();
                    }
                }
                else if (str[0].Contains("^"))//string
                {
                    str[0] = str[0].Replace("^", "");
                    str[0] = str[0].Replace("(", "");
                    str[0] = str[0].Replace(")", "");
                    str[0] = str[0].Replace("Найти", "");
                    str[0] = str[0].Replace("(", "");
                    str[0] = str[0].Replace(")", "");
                    str[0] = str[0].Replace("$", "");
                    podstroka = StringsList[str[0]].ToString();
                }

                if ((str[1].Contains("[")) && (str[1].Contains("]")))//macros
                {
                    str[1] = str[1].Replace("(", "");
                    str[1] = str[1].Replace(")", "");
                    str[1] = str[1].Replace("Найти", "");
                    str[1] = str[1].Replace("(", "");
                    str[1] = str[1].Replace(")", "");
                    str[1] = str[1].Replace("$", "");
                    str[1] = str[1].Replace("$", "");
                    {
                        string p1; int i = 0, j = 0;
                        bool mass = false;
                        if (str[1][str[1].Length - 1] == ']') mass = true;
                        if (mass)
                        {

                            p1 = str[1].Substring(str[1].IndexOf('[') + 1, 3);
                            string[] pMass = p1.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                            i = int.Parse(pMass[1]);
                            j = int.Parse(pMass[1]);
                        }

                        string pp = str[1].Substring(0, str[1].IndexOf('['));
                        foreach (SpecialMacro m in f.specMacro)
                            if (m.ShortText.Substring(0, m.ShortText.Count() - 3) == pp)
                            {
                                if (mass)
                                {
                                    if (m.value is double[,])
                                    {
                                        stroka = (m.value as double[,]).GetValue(j, i).ToString();
                                    }
                                    if (m.value is string[,])
                                    {
                                        stroka = (m.value as string[,]).GetValue(j, i).ToString();
                                    }
                                }
                                else
                                {
                                    stroka = m.value.ToString();
                                }
                            }
                    }
                }
                else if (str[1].Contains("$"))//peremennaia
                {
                    str[1] = str[1].Replace("$", "");
                    str[1] = str[1].Replace("(", "");
                    str[1] = str[1].Replace(")", "");
                    str[1] = str[1].Replace("Найти", "");
                    str[1] = str[1].Replace("(", "");
                    str[1] = str[1].Replace(")", "");
                    str[1] = str[1].Replace("$", "");
                    if (VariablesList.ContainsKey(str[1]))
                    {
                        if (VariablesList[str[1]] is double[,])
                        {

                            stroka = (VariablesList[str[1]] as double[,]).ToString();
                        }
                        if (VariablesList[str[1]] is string[,])
                        {

                            stroka = (VariablesList[str[1]] as string[,]).ToString();
                        }

                        stroka = VariablesList[str[1]].ToString();
                    }
                }
                else if (str[1].Contains("^"))//string
                {
                    str[1] = str[1].Replace("^", "");
                    str[1] = str[1].Replace("(", "");
                    str[1] = str[1].Replace(")", "");
                    str[1] = str[1].Replace("Найти", "");
                    str[1] = str[1].Replace("(", "");
                    str[1] = str[1].Replace(")", "");
                    str[1] = str[1].Replace("$", "");
                    stroka = StringsList[str[1]].ToString();
                }
                return FuncReplace(stroka.IndexOf(podstroka).ToString(), all, rGet);
                



            }
            else// если
            {
                string p = input;
                 if (p.StartsWith("$"))
            {
                p.Replace("$","");
                return VariablesList[p].ToString();
            }

            //if
                 string key = "";
                 string ret = "";
                 if (p.Contains('='))
                 {     
                     string[] data = p.Split(new string[] { "=" }, StringSplitOptions.RemoveEmptyEntries);
                     string[] str = data[0].Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                     string podstroka = ""; string stroka = "";
                     if ((str[0].StartsWith("Найти($")) && (!str[0].Contains('[')) && (!str[0].Contains(']')))
                     {                         
                         str[0] = str[0].Replace("$", "");                         
                         str[0] = str[0].Replace("(", "");
                         str[0] = str[0].Replace(")", "");                        
                         ret+= str[0];//ДатаПрописью(m1)
                         str[0] = str[0].Replace("Найти", "");                         
                         podstroka = VariablesList[str[0]].ToString();
                         ret = ret.Replace("(", "");
                         ret = ret.Replace(")", "");

                     }
                     else if ((str[0].Contains('[')) && (str[0].Contains(']')))                       //ДатаПрописью($Параметры[1,3])
                     {
                         string variable = data[0];
                         variable = variable.Replace("(", "");
                         variable = variable.Replace(")", "");
                         variable = variable.Replace("$", "");                         
                         str[0] = str[0].Replace("$", "");
                         variable=variable.Replace("Найти","");                       

                         string p1=""; int i = 0, j = 0;
                         bool mass = false;
                         if (str[0][str[0].Length - 1] == ']') mass = true;
                         string pp = str[0];
                         if (mass)
                         {
                             pp = str[0].Substring(0, str[0].IndexOf('['));
                             p1 = str[0];
                             p1 = p1.Replace(pp, "");
                             p1.Replace("[","");
                             p1.Replace("]", "");
                             string[] pMass = p1.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                             i = int.Parse(pMass[0]);
                             j = int.Parse(pMass[1]);
                         }
                         ret += pp + i.ToString() + j.ToString();
                         foreach (SpecialMacro m in f.specMacro)
                             if (m.ShortText.Substring(0, m.ShortText.Count() - 3) == pp)
                             {
                                 if (mass)
                                 {
                                     if (m.value is double[,])
                                     {
                                         podstroka = (m.value as double[,]).GetValue(i, j).ToString();                                         
                                     }
                                     if (m.value is string[,])
                                     {
                                         podstroka=(m.value as string[,]).GetValue(i, j).ToString();                                         
                                     }
                                 }
                                 else
                                 {
                                 podstroka=m.value.ToString();                                     
                                 }
                             }
                     }
                     else if (str[0].Contains("$"))
                     {
                         string variable = str[0];
                         variable = variable.Replace("(", "");
                         variable = variable.Replace(")", "");
                         variable = variable.Replace("$", "");
                         str[0] = str[0].Replace("$", "");
                         variable = variable.Replace("Найти", "");
                         podstroka = VariablesList[str[0]].ToString();
                         ret += variable;//ДатаПрописью(m1)
                         /*data[0] = data[0].Remove(0, 13);
                         data[0] = data[0].Remove(data[0].Length - 1, 1);*/
                         ret = ret.Replace("(", "");
                         ret = ret.Replace(")", "");
                     }
                     else if (str[0].Contains("^"))
                     {
                         string variable = str[0];
                         variable = variable.Replace("(", "");
                         variable = variable.Replace(")", "");
                         variable = variable.Replace("^", "");
                         str[0] = str[0].Replace("^", "");
                         str[0] = str[0].Replace("Найти", "");
                         str[0] = str[0].Replace("(", "");
                         str[0] = str[0].Replace(")", "");
                        // variable = variable.Replace("Найти", "");
                         podstroka = StringsList[str[0]].ToString();
                         ret += variable;//ДатаПрописью(m1)
                         /*data[0] = data[0].Remove(0, 13);
                         data[0] = data[0].Remove(data[0].Length - 1, 1);*/
                         ret = ret.Replace("(", "");
                         ret = ret.Replace(")", "");
                         ret = ret.Replace("^", "");
                     }

                     if ((str[1].StartsWith("Найти($")) && (!str[1].Contains('[')) && (!str[1].Contains(']')))
                     {
                         str[1] = str[1].Replace("$", "");
                         str[1] = str[1].Replace("(", "");
                         str[1] = str[1].Replace(")", "");
                         ret += str[1];//ДатаПрописью(m1)
                         str[1] = str[1].Replace("Найти", "");
                         stroka = VariablesList[str[1]].ToString();
                         ret = ret.Replace("(", "");
                         ret = ret.Replace(")", "");

                     }
                     else if ((str[1].Contains('[')) && (str[1].Contains(']')))                       //ДатаПрописью($Параметры[1,3])
                     {
                         string variable = str[1];
                         //string[] st = variable.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                         variable = variable.Replace("(", "");
                         variable = variable.Replace(")", "");
                         variable = variable.Replace("$", "");
                         variable = variable.Replace("Найти", "");
                         str[1] = str[1].Replace("(", "");
                         str[1] = str[1].Replace("$", "");
                         str[1] = str[1].Replace(")", "");
                         str[1] = str[1].Replace("Найти", "");                         
                         

                         string p1 = ""; int i = 0, j = 0;
                         bool mass = false;
                         if (str[1][str[1].Length - 1] == ']') mass = true;
                         string pp = str[1]; ;
                         if (mass)
                         {
                             pp = str[1].Substring(0, str[1].IndexOf('['));
                             p1 = str[1];
                             p1 = p1.Replace(pp, "");
                             p1= p1.Replace("[", "");
                             p1=p1.Replace("]", "");
                             string[] pMass = p1.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                             i = int.Parse(pMass[0]);
                             j = int.Parse(pMass[1]);
                         }
                         ret += pp + i.ToString() + j.ToString();
                         foreach (SpecialMacro m in f.specMacro)
                             if (m.ShortText.Substring(0, m.ShortText.Count() - 3) == pp)
                             {
                                 if (mass)
                                 {
                                     if (m.value is double[,])
                                     {
                                         stroka = (m.value as double[,]).GetValue(i, j).ToString();
                                     }
                                     if (m.value is string[,])
                                     {
                                         stroka = (m.value as string[,]).GetValue(i, j).ToString();
                                     }
                                 }
                                 else
                                 {
                                     stroka = m.value.ToString();
                                 }
                             }
                     }
                     else if (str[1].Contains("$"))
                     {
                         string variable = str[1];
                         variable = variable.Replace("(", "");
                         variable = variable.Replace(")", "");
                         variable = variable.Replace("$", "");
                         str[1] = str[1].Replace("$", "");
                         variable = variable.Replace("Найти", "");
                         stroka = VariablesList[str[1]].ToString();
                         ret += variable;//ДатаПрописью(m1)
                         /*data[0] = data[0].Remove(0, 13);
                         data[0] = data[0].Remove(data[0].Length - 1, 1);*/
                         ret = ret.Replace("(", "");
                         ret = ret.Replace(")", "");
                     }
                     else if (str[1].Contains("^"))
                     {
                         string variable = str[1];
                         variable = variable.Replace("(", "");
                         variable = variable.Replace(")", "");
                         variable = variable.Replace("^", "");
                         str[1] = str[1].Replace("^", "");
                         str[1] = str[1].Replace("Найти", "");
                         str[1] = str[1].Replace("(", "");
                         str[1] = str[1].Replace(")", "");
                         str[1] = str[1].Replace("^", "");
                         variable = variable.Replace("Найти", "");
                         stroka = StringsList[str[1]].ToString();
                         ret += variable;//ДатаПрописью(m1)
                         /*data[0] = data[0].Remove(0, 13);
                         data[0] = data[0].Remove(data[0].Length - 1, 1);*/
                         ret = ret.Replace("(", "");
                         ret = ret.Replace(")", "");
                         ret = ret.Replace("^", "");
                     }
                     VariablesList.Add(ret,stroka.IndexOf(podstroka).ToString());
                     return "$" + ret +"="+ data[1];








                 }
                 else
                 {
                     string variable = p;
                     if (p.StartsWith("Найти($"))
                     {
                         p = p.Replace("$", "");
                         VariablesList.Add(p, string.Empty);
                         variable = p;
                         p = p.Remove(0, 13);
                         p = p.Remove(p.Length - 1, 1);
                         if (VariablesList.ContainsKey(p))
                         {
                             if (VariablesList[p] is double[,])
                             {
                                 VariablesList[variable] = conData((VariablesList[p] as double[,]).ToString(), false);
                                 return "$" + variable;
                             }
                             if (VariablesList[p] is string[,])
                             {
                                 VariablesList[variable] = conData((VariablesList[p] as string[,]).ToString(), false);
                                 return "$" + variable;
                             }
                             VariablesList[variable] = conData(VariablesList[p].ToString(), false);
                             return "$" + variable;
                         }
                         else
                         {
                             string p1; int i = 0, j = 0;
                             bool mass = false;
                             if (p[p.Length - 1] == ']') mass = true;
                             if (mass)
                             {
                                 VariablesList.Add(p, string.Empty);
                                 variable = p;
                                 p1 = p.Substring(p.Length - 4, 3);
                                 string[] pMass = p1.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                                 i = int.Parse(pMass[0]);
                                 j = int.Parse(pMass[1]);
                             }

                             string pp = p.Substring(0, p.Length - 5);
                             foreach (SpecialMacro m in f.specMacro)
                                 if (m.ShortText.Substring(0, m.ShortText.Count() - 3) == pp)
                                 {
                                     if (mass)
                                     {
                                         if (m.value is double[,])
                                         {
                                             VariablesList[variable] = conData((m.value as double[,]).GetValue(j, i).ToString(), false);
                                             return "$" + variable;
                                         }
                                         if (m.value is string[,])
                                         {
                                             VariablesList[variable] = conData((m.value as string[,]).GetValue(j, i).ToString(), false);
                                             return "$" + variable;
                                         }
                                     }
                                     else
                                     {
                                         VariablesList[variable] = conData(m.value.ToString(), false);
                                         return "$" + variable;
                                     }
                                 }
                         }

                     }
                 }



















            }
            return "0";

                 /*string[] rightArray = output[0].Split(new char[] { '[', ']', ',', '$' }, StringSplitOptions.RemoveEmptyEntries);

                        int i = int.Parse(output[1]);
                        int j = int.Parse(output[2]);
                        foreach (SpecialMacro m in f.specMacro)
                            if (m.ShortText.Substring(0, m.ShortText.Count() - 3) == output[0])
                            {


                                return (m.value as string[,]).GetValue(j, i).ToString().IndexOf(output[0]).ToString();
                            }


                        return (VariablesList[rightArray[0]] as string[,])[Convert.ToInt32(Sequence(RecognizeVariables(rightArray[1]))),
                            Convert.ToInt32(Sequence(RecognizeVariables(rightArray[2])))].IndexOf(output[0]).ToString();
                    

                    return (VariablesList[output[0]] as string).IndexOf(output[0]).ToString();*/
            /*if (output[1].Contains("[") && output[1].Contains("]"))
            {
                string[] rightArray = output[1].Split(new char[] { '[', ']', ',', '$' }, StringSplitOptions.RemoveEmptyEntries);

                return (VariablesList[rightArray[0]] as string[,])[Convert.ToInt32(Sequence(RecognizeVariables(rightArray[1]))),
                    Convert.ToInt32(Sequence(RecognizeVariables(rightArray[2])))].IndexOf(output[0] as string);
            }

            return (VariablesList[output[0]] as string).IndexOf(output[0] as string);;*/
        }

        private string DataP(string p) // // ДатаПрописью($m1)=^201.01.2008\")
        {
            p=RecognizeFunctions(p);
            if (p.StartsWith("$"))
            {
                p.Replace("$","");
                return VariablesList[p].ToString();
            }
//остальное           
            if (p.StartsWith("(") &&(!p.Contains("ДатаПрописью")))
            {
                p = p.Remove(0, 1);
                p = p.Remove(p.Length - 1, 1);
                string variable = p;
                if (p.StartsWith("$")&&(!p.Contains("[")))
                {
                    p = p.Replace("$", "");
                    p = p.Replace("(", "");
                    p = p.Replace(")", "");
                    p = "ДатаПрописью" + p;
                    variable = p;                    
                    VariablesList.Add(p, string.Empty);
                    variable = p;
                    //p = p.Remove(0, 13);
                    //p = p.Remove(p.Length - 1, 1);
                    if (VariablesList.ContainsKey(p))
                    {
                        if (VariablesList[p] is double[,])
                        {
                            VariablesList[variable] = conData((VariablesList[p] as double[,]).ToString(), false);
                            return "$" + variable;
                        }
                        if (VariablesList[p] is string[,])
                        {
                            VariablesList[variable] = conData((VariablesList[p] as string[,]).ToString(), false);
                            return "$" + variable;
                        }
                        VariablesList[variable] = conData(VariablesList[p].ToString(), false);
                        return "$" + variable;
                    }
                }
                else
                {
                    p = p.Replace("$", "");
                    p = p.Replace("(", "");
                    p = p.Replace(")", "");
                    string p1; int i = 0, j = 0;
                    bool mass = false;
                    if (p[p.Length - 1] == ']') mass = true;
                    if (mass)
                    {
                        variable = variable.Replace("[", "");
                        variable = variable.Replace("]", "");
                        variable = variable.Replace("$", "");
                        variable = "ДатаПрописью" + variable;
                        VariablesList.Add(variable, string.Empty);
                       // variable = p;
                        p1 = p.Substring(p.IndexOf("[")+1, p.IndexOf("]") - p.IndexOf("[")-1);
                        string[] pMass = p1.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                        i = int.Parse(pMass[0]);
                        j = int.Parse(pMass[1]);
                    }

                    string pp = p.Substring(0, p.IndexOf("["));
                    foreach (SpecialMacro m in f.specMacro)
                        if (m.ShortText.Substring(0, m.ShortText.Count() - 3) == pp)
                        {
                            if (mass)
                            {
                                if (m.value is double[,])
                                {
                                    VariablesList[variable] = conData((m.value as double[,]).GetValue(j, i).ToString(), false);
                                    return "$" + variable;
                                }
                                if (m.value is string[,])
                                {
                                    VariablesList[variable] = conData((m.value as string[,]).GetValue(j, i).ToString(), false);
                                    return "$" + variable;
                                }
                            }
                            else
                            {
                                VariablesList[variable] = conData(m.value.ToString(), false);
                                return "$" + variable;
                            }
                        }
                }
                
            }








            //if
            if (p.Contains('=')) 
            {
                string[] data = p.Split(new string[] { "=" }, StringSplitOptions.RemoveEmptyEntries);
                if ((data[0].StartsWith("ДатаПрописью($")) && (!data[0].Contains('[')) && (!data[0].Contains(']')))
                {
                    string ret;                    
                    data[0] = data[0].Replace("$", "");   
                    ret = data[0];//ДатаПрописью(m1)
                    data[0] = data[0].Remove(0, 13);
                    data[0] = data[0].Remove(data[0].Length - 1, 1);
                    string key=conData((VariablesList[data[0]].ToString()), false);
                    ret=ret.Replace("(","");
                    ret = ret.Replace(")", "");
                    VariablesList.Add(ret, key);                       
                    return "$" + ret + "=" + data[1];

                }
                else if ((data[0].Contains('[')) && (data[0].Contains(']')))                       //ДатаПрописью($Параметры[1,3])
                {
                    string variable = data[0];
                    variable = variable.Replace("(", "");
                    variable = variable.Replace(")", "");
                    variable = variable.Replace("$", "");
                    variable = variable.Replace("[", "");
                    variable = variable.Replace("]", "");
                    variable = variable.Replace(",", "");
                    data[0] = data[0].Replace("$", ""); 
                    data[0] = data[0].Remove(0, 13);
                    data[0] = data[0].Remove(data[0].Length - 1, 1);
                    
                    string p1; int i = 0, j = 0;
                    bool mass = false;
                    if (data[0][data[0].Length - 1] == ']') mass = true;
                    if (mass)
                    {

                        p1 = data[0];
                        p1 =  p1.Substring(p1.IndexOf("[")+1, 3);
                        string[] pMass = p1.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                        i = int.Parse(pMass[0]);
                        j = int.Parse(pMass[1]);
                    }

                    string pp = data[0].Substring(0, data[0].IndexOf('['));
                    foreach (SpecialMacro m in f.specMacro)
                        if (m.ShortText.Substring(0, m.ShortText.Count() - 3) == pp)
                        {
                            if (mass)
                            {
                                if (m.value is double[,])
                                {
                                    VariablesList.Add(variable, conData((m.value as double[,]).GetValue(i, j).ToString(), false));
                                    return "$" + variable + "=" + data[1]; ;
                                }
                                if (m.value is string[,])
                                {
                                    VariablesList.Add(variable, conData((m.value as string[,]).GetValue(i, j).ToString(), false));
                                    return "$" + variable + "=" + data[1]; ;
                                }
                            }
                            else
                            {
                                VariablesList.Add(variable, conData(m.value.ToString(), false));
                                return "$" + variable + "=" + data[1]; ;
                            }
                        }
                }
                else
                {
                    string ret;
                    data[0] = data[0].Replace("$", "");
                    ret = data[0];//ДатаПрописью(m1)
                    data[0] = data[0].Remove(0, 13);
                    data[0] = data[0].Remove(data[0].Length - 1, 1);
                    string key = conData((data[0].ToString()), false);
                    ret = ret.Replace("(", "");
                    ret = ret.Replace(")", "");
                    VariablesList.Add(ret, key);
                    return "$" + ret + "=" + data[1];
                }
            }
            else
            {
                string variable = p;
                if (p.StartsWith("ДатаПрописью($"))
                {
                    p = p.Replace("$", "");
                    VariablesList.Add(p, string.Empty);
                    variable = p;
                    p = p.Remove(0, 13);
                    p = p.Remove(p.Length - 1, 1);
                    if (VariablesList.ContainsKey(p))
                    {
                        if (VariablesList[p] is double[,])
                        {
                            VariablesList[variable] = conData((VariablesList[p] as double[,]).ToString(), false);
                            return "$" + variable;
                        }
                        if (VariablesList[p] is string[,])
                        {
                            VariablesList[variable] = conData((VariablesList[p] as string[,]).ToString(), false);
                            return "$" + variable;
                        }
                        VariablesList[variable] = conData(VariablesList[p].ToString(), false);
                        return "$" + variable;
                    }
                    else
                    {
                        string p1; int i = 0, j = 0;
                        bool mass = false;
                        if (p[p.Length - 1] == ']') mass = true;
                        if (mass)
                        {
                            VariablesList.Add(p, string.Empty);
                            variable = p;
                            p1 = p.Substring(p.Length - 4, 3);
                            string[] pMass = p1.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                            i = int.Parse(pMass[0]);
                            j = int.Parse(pMass[1]);
                        }

                        string pp = p.Substring(0, p.Length - 5);
                        foreach (SpecialMacro m in f.specMacro)
                            if (m.ShortText.Substring(0, m.ShortText.Count() - 3) == pp)
                            {
                                if (mass)
                                {
                                    if (m.value is double[,])
                                    {
                                        VariablesList[variable] = conData((m.value as double[,]).GetValue(j, i).ToString(), false);
                                        return "$" + variable;
                                    }
                                    if (m.value is string[,])
                                    {
                                        VariablesList[variable] = conData((m.value as string[,]).GetValue(j, i).ToString(), false);
                                        return "$" + variable;
                                    }
                                }
                                else
                                {
                                    VariablesList[variable]=conData(m.value.ToString(), false);
                                    return "$" + variable;
                                }
                            }
                    }
                }

                {
                    p = p.Replace("$", "");
                    if (p[0] == '(')
                        p = p.Remove(0, 1);
                    if (p[p.Length - 1] == ')')
                        p = p.Remove(p.Length - 1);
                    string p1; int i = 0, j = 0;
                    bool mass = false;
                    if (p[p.Length - 1] == ']') mass = true;
                    if (mass)
                    {
                        p1 = p.Substring(p.Length - 4, 3);
                        string[] pMass = p1.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                        i = int.Parse(pMass[0]);
                        j = int.Parse(pMass[1]);
                    }

                    string pp = p.Substring(0, p.Length - 5);
                    foreach (SpecialMacro m in f.specMacro)
                        if (m.ShortText.Substring(0, m.ShortText.Count() - 3) == pp)
                        {
                            if (mass)
                            {
                                return conData((m.value as string[,]).GetValue(j, i).ToString(), true);
                            }
                            else
                                return conData(m.value.ToString(), true);
                        }
                }

                p = p.Replace(" ", "");
                p = p.Remove(0, 13);
                p = p.Remove(p.Length - 1, 1);
                p = p.Replace("$", "");
                string[] output = p.Split(new string[] { "." }, StringSplitOptions.RemoveEmptyEntries);
                output[0] = output[0].TrimStart('0');
                output[1] = output[1].TrimStart('0');
                /*switch (output[1])
                {
                    case "1": output[1] = "января"; break;
                    case "2": output[1] = "февраля"; break;
                    case "3": output[1] = "марта"; break;
                    case "4": output[1] = "апреля"; break;
                    case "5": output[1] = "мая"; break;
                    case "6": output[1] = "июня"; break;
                    case "7": output[1] = "июля"; break;
                    case "8": output[1] = "августа"; break;
                    case "9": output[1] = "сентября"; break;
                    case "10": output[1] = "октября"; break;
                    case "11": output[1] = "ноября"; break;
                    case "12": output[1] = "декабря"; break;
                }*/
                switch (output[1])
                {
                    case "1": output[1] = "січня"; break;
                    case "2": output[1] = "лютого"; break;
                    case "3": output[1] = "березня"; break;
                    case "4": output[1] = "квітня"; break;
                    case "5": output[1] = "травня"; break;
                    case "6": output[1] = "червня"; break;
                    case "7": output[1] = "липня"; break;
                    case "8": output[1] = "серпня"; break;
                    case "9": output[1] = "вересня"; break;
                    case "10": output[1] = "жовтня"; break;
                    case "11": output[1] = "листопада"; break;
                    case "12": output[1] = "грудня"; break;
                }

                return output[0] + " " + output[1] + " " + output[2];
            }
            return "0";
            
        }


        private string conData(string p, bool ret)
        {           
            p = p.Replace("$", "");
            string[] output = p.Split(new string[] { "." }, StringSplitOptions.RemoveEmptyEntries);
            output[0] = output[0].TrimStart('0');
            output[1] = output[1].TrimStart('0');
            /*switch (output[1])
            {
                case "1": output[1] = "января"; break;
                case "2": output[1] = "февраля"; break;
                case "3": output[1] = "марта"; break;
                case "4": output[1] = "апреля"; break;
                case "5": output[1] = "мая"; break;
                case "6": output[1] = "июня"; break;
                case "7": output[1] = "июля"; break;
                case "8": output[1] = "августа"; break;
                case "9": output[1] = "сентября"; break;
                case "10": output[1] = "октября"; break;
                case "11": output[1] = "ноября"; break;
                case "12": output[1] = "декабря"; break;
            }*/
            switch (output[1])
            {
                case "1": output[1] = "січня"; break;
                case "2": output[1] = "лютого"; break;
                case "3": output[1] = "березня"; break;
                case "4": output[1] = "квітня"; break;
                case "5": output[1] = "травня"; break;
                case "6": output[1] = "червня"; break;
                case "7": output[1] = "липня"; break;
                case "8": output[1] = "серпня"; break;
                case "9": output[1] = "вересня"; break;
                case "10": output[1] = "жовтня"; break;
                case "11": output[1] = "листопада"; break;
                case "12": output[1] = "грудня"; break;
            }
            if (!ret)
            return output[0] + " " + output[1] + " " + output[2];
            else return output[0] + "|" + output[1] + "|" + output[2];

        }

        private string RowsCount(string p)
        {
            p = p.Substring(1, p.Length - 2);
            p = p.Replace("$", "");
            if (VariablesList.ContainsKey(p))
            {
                if (VariablesList[p] is double[,])
                    return (VariablesList[p] as double[,]).GetLength(0).ToString();
                if (VariablesList[p] is string[,])
                    return (VariablesList[p] as string[,]).GetLength(0).ToString();
            }
            else
            {
                foreach(SpecialMacro m in f.specMacro)
                    if (m.ShortText.Substring(0, m.ShortText.Count() - 3) == p)
                    {
                        if (m.value is double[,])
                            return (m.value as double[,]).GetLength(0).ToString();
                        if (m.value is string[,])
                            return (m.value as string[,]).GetLength(0).ToString();
                    }
                return "0";
            }
            return "0";
        }

        private string ColumnsCount(string p)
        {

            p = p.Substring(1, p.Length - 2);
            p = p.Replace("$", "");
            if (VariablesList.ContainsKey(p))
            {
                if (VariablesList[p] is double[,])
                    return (VariablesList[p] as double[,]).GetLength(1).ToString();
                if (VariablesList[p] is string[,])
                    return (VariablesList[p] as string[,]).GetLength(1).ToString();
            }
            else
            {
                foreach(SpecialMacro m in f.specMacro)
                    if (m.ShortText.Substring(0, m.ShortText.Count() - 3) == p)
                    {
                        if (m.value is double[,])
                            return (m.value as double[,]).GetLength(1).ToString();
                        if (m.value is string[,])
                            return (m.value as string[,]).GetLength(1).ToString();
                    }
                return "0";
            }
            return "0";
        }

        //проверить числовая или символьная строка
        private bool StringIsNumeric(string input)
        {
            if (input.Contains('"'))
                return false;
            if (input.Contains('^'))
                return false;
            if (input.Contains("Вырезать") || input.Contains("Округлить") || input.Contains("УдалПроб") ||input.Contains("строка") ||
                input.Contains("Строка") || input.Contains("СТРОКА") )
                return false;

            foreach(SpecialMacro m in  f.specMacro)
            if(Regex.IsMatch(input, @"\$" + m.ShortText.Substring(0, m.ShortText.Length - 3)))//if (input == "$" + m.ShortText.Substring(0, m.ShortText.Length - 3)) 
                {
                    if (m.value is string[,] || m.value is string)
                    {
                        return false;
                    }
                   // else return true;
                }
            foreach (string key in VariablesList.Keys)
            //  // if (Regex.IsMatch(input, @"\$" + key))   
                if (input == "$" + key)
                {
                    if (VariablesList[key] is string)
                    {
                        return false;
                    }
                    else return true;
                }
            return true;
        }
        

        //изменить значение для переменной
        private void SetNewValue(string input, string value)
        {
            if (input.Contains("[") && input.Contains("]"))
            {
                string m1 = "0", m2 = "0", var = "";
                foreach (string st in VariablesList.Keys)
                {
                    string pattern = @"\$" + st + @"\[\s*((?:[^\[\]]|(?<o>\[)|(?<-o>\]))+(?(o)(?!))),\s*((?:[^\[\]]|(?<o>\[)|(?<-o>\]))+(?(o)(?!)))\]";
                    MatchCollection match = Regex.Matches(input, pattern);
                    if (match.Count != 0)
                    {
                        m1 = match[0].Groups[1].Value;
                        m2 = match[0].Groups[2].Value;
                        var = st;
                        continue;
                    }
                }
                (VariablesList[var] as double[,])[
                    Convert.ToInt32(Sequence(RecognizeVariables(m1))),
                    Convert.ToInt32(Sequence(RecognizeVariables(m2)))] =Convert.ToDouble(Sequence(RecognizeVariables(value)));

            }
            else
            {
                input = input.Replace("$", "");
                VariablesList[input] = Convert.ToDouble(Sequence(RecognizeVariables(value)));
            }

        }

        //изменить значение для переменной
        private void SetNewValue(string input, int value)
        {
            SetNewValue(input, value.ToString());
        }

        //получить все массивы
        public string[] GetArraysNames()
        {
            string[] result = new string[0];
            foreach (string p in VariablesList.Keys)
            {
                if (VariablesList[p] is double[,] || VariablesList[p] is string[,])
                {
                    Array.Resize(ref result, result.Count() + 1);
                    result[result.Count() - 1] = p;
                }
            }
            foreach (SpecialMacro sm in f.specMacro)
            {
                if(sm.ShortText.Contains("[,]"))
                {
                    Array.Resize(ref result, result.Count() + 1);
                    result[result.Count() - 1] = sm.ShortText.Substring(0, sm.ShortText.Length - 3);
                }
            }
            return result;
        }

        //получить содерживое массива
        public object GetArrayContent(string arrayName)
        {
            if (VariablesList.ContainsKey(arrayName))
                return VariablesList[arrayName];
            else
                return f.specMacro.FirstOrDefault(m =>  m.ShortText.Substring(0, m.ShortText.Length - 3) == arrayName).value;
        }

        //получить значение переменной
        public string GetVariableValue(string name)
        {
            string nam = name;
            name = RecognizeFunctions(name);
            if (nam != name)  return name;
            string[] split = name.Split(new char[] { '[', ']', ',' }, StringSplitOptions.RemoveEmptyEntries);
            if (split.Count() > 1)
            {
                if (VariablesList.ContainsKey(split[0]))
                {
                    try
                    {
                        if (VariablesList[split[0]] is double[,])
                            return (VariablesList[split[0]] as double[,])[Convert.ToInt32(split[1]), Convert.ToInt32(split[2])].ToString();
                        else if (VariablesList[split[0]] is string[,])
                            return (VariablesList[split[0]] as string[,])[Convert.ToInt32(split[1]), Convert.ToInt32(split[2])];
                        else if (VariablesList[split[0]] is object[,])
                            return (VariablesList[split[0]] as object[,])[Convert.ToInt32(split[1]), Convert.ToInt32(split[2])].ToString();
                        else
                            return "Переменная " + split[0] + " не является массивом";
                    }
                    catch (IndexOutOfRangeException)
                    {
                        return "Индекс за переделами массива";
                    }
                }
                else
                {
                    if (VariablesList.ContainsKey(name))
                    {
                        return (VariablesList[split[0]]).ToString();
                    }
                    else
                    {                       
                            return "Заданная переменная не существует";                        
                    }
                }
            }
            else
            {
                if (VariablesList.ContainsKey(name))
                {
                    return (VariablesList[split[0]]).ToString();
                }
                else
                {
                    return "Заданная переменная не существует";
                }
            }
        }

        //работа с логическими переменными
        private bool RecognizeLogical(string input)
        {
            string pattern = @"\([^\(\)]*\)";
            input = Regex.Replace(input, pattern,
                        m => (GetBooleanValue(m.Groups[0].Value)).ToString());
            input = Regex.Replace(input, "или" , "|");
            input = Regex.Replace(input, "и", "&");


            //преобразовует инфиксную в постфиксую запись
            Stack<string> stack = new Stack<string>();
            Stack<bool> result = new Stack<bool>();
            List<object> list = new List<object>();

            for (int i = 0; i < input.Length; i++)
            {
                if (input[i] == '0' || input[i] == '1')
                {
                    list.Add(input[i]);
                    continue;
                }

                else if (input[i] == '(')
                {
                    stack.Push("(");
                    continue;
                }
                else if (input[i] == ')')
                {
                    bool perform = true;
                    string a = null;
                    while (perform)
                    {
                        if (stack.Count > 0)
                        {
                            a = stack.Pop();
                            if (a != "(")
                                list.Add(a);
                        }
                        else perform = false;
                    }
                }
                else if (input[i] == '&' || input[i] == '|')
                {
                    for (int j = i; j >= 0; j--)
                    {
                        if (stack.Count > 0)
                        {
                            string p = stack.Peek();
                            if (p == "(")
                            {
                               // list.Add(p);
                                stack.Pop();
                                continue;
                            }
                            if (p == "&" && (input[i] == '&' || input[i] == '|'))
                            {
                                list.Add(p);
                                stack.Pop();
                            }
                            else if (p == "|" && input[i] == '|')
                            {
                                list.Add(p);
                                stack.Pop();
                            }
                            else
                            {
                                break;
                            }
                        }
                        else break;
                    }
                    stack.Push(input[i].ToString());
                }
            }

            while (stack.Count > 0)
            {
                list.Add(stack.Pop());
            }

            foreach (object obj in list)
            {
                //if ((obj as char?).Value == '0' || (obj as char?).Value == '1')
                //{
                //    result.Push(((obj as char?).Value == '0')? false: true);
                //}
                if (obj is char)
                {
                    result.Push(((obj as char?).Value == '0') ? false : true);
                }
                else
                {
                    bool a = result.Pop();
                    bool b = result.Pop();
                    bool c = false;
                    switch (obj as string)
                    {
                        case "&": c = b & a;
                            break;
                        case "|": c = b | a;
                            break;
                    }
                    result.Push(c);
                }
            }
            return result.Pop();
        }

        //работа со строками, сцепление строк
        private string SequenceString(string input)
        {
            //определяем функции  
            input = Regex.Replace(input, @"(Длина\(.+\))", m => (LengthCommand(m.Groups[0].Value, true)));
            input = Regex.Replace(input, @"(ДатаПрописью\(.+\))", m => (DataP(m.Groups[0].Value,false)));
            input = Regex.Replace(input, @"(Найти\(.+\))", m => (Find(m.Groups[0].Value,true)));
            input = Regex.Replace(input, @"(Вырезать\(.+\))", m => (CutCommand(m.Groups[0].Value)));
            //input = Regex.Replace(input, @"(Округлить\(.+\))", m => (CutCommand(m.Groups[0].Value)));
            input = Regex.Replace(input, @"(УдалПробСправа\(.+\))", m => (RemoveSpacesRight(m.Groups[0].Value)));
            input = Regex.Replace(input, @"(УдалПробСлева\(.+\))", m => (RemoveSpacesLeft(m.Groups[0].Value)));
            input = Regex.Replace(input, @"(УдалПробСпрСлева\(.+\))", m => (RemoveSpacesAll(m.Groups[0].Value)));
            input = Regex.Replace(input, @"(СТОКА\(.+\))", m => (ToUpper(m.Groups[0].Value)));
            input = Regex.Replace(input, @"(строка\(.+\))", m => (ToLower(m.Groups[0].Value)));
            input = Regex.Replace(input, @"(Строка\(.+\))", m => (Capitalize(m.Groups[0].Value)));            
            //преобразовываем все переменные в значения
            foreach (string st in VariablesList.Keys)
            {
                input = Regex.Replace(input, @"\$" + st + @"\b", VariablesList[st].ToString());
            }

            //преобразовываем все переменные в значения
            foreach (string st in StringsList.Keys)
            {
                input = Regex.Replace(input, @"\^" + st + @"\b", StringsList[st].ToString());
            }

            foreach (SpecialMacro sm in f.specMacro)
            {
                if (!sm.ShortText.Contains("["))
                {
                    input = Regex.Replace(input, @"\$" + sm.ShortText, sm.value.ToString());
                }
                else
                {

                    string[] p = sm.ShortText.Split('[');
                    string pattern = @"\$" + p[0] + @"\[\s*((?:[^\[\]]|(?<o>\[)|(?<-o>\]))+(?(o)(?!))),\s*((?:[^\[\]]|(?<o>\[)|(?<-o>\]))+(?(o)(?!)))\]";
                    if (sm.value is string[,])
                        input = Regex.Replace(input, pattern,
                            m => ((sm.value as string[,])[Convert.ToInt32(Sequence(RecognizeVariables(m.Groups[1].Value))),
                                Convert.ToInt32(Sequence(RecognizeVariables(m.Groups[2].Value)))].ToString()));
                    else if(sm.value is double[,])
                        input = Regex.Replace(input, pattern,
                            m => ((sm.value as double[,])[Convert.ToInt32(Sequence(RecognizeVariables(m.Groups[1].Value))),
                                Convert.ToInt32(Sequence(RecognizeVariables(m.Groups[2].Value)))].ToString()));

                }
            }

            string[] splited = input.Split('+');
            string output = "";
            for (int i = 0; i < splited.Count();i++ )
            {
                splited[i] = splited[i].Replace("^", "");
                string spl = splited[i];
                /*if ((spl!="") && (spl!=null))
                if ((spl[0]=='"')&&(spl[spl.Length-1]=='"')&&(spl.Length>2))
                {                    
                    splited[i] = splited[i].Remove(0, 1);
                    splited[i] = splited[i].Remove(splited[i].Length-1,1);
                }*/                
                output += splited[i];
            }
            if (output == "") return " ";
            return output;
        }

        private string CutCommand(string input)
        {
            string all = input;
            string ret = input;
            ret = ret.Replace("Вырезать(", "");
            string[] output = input.Split(new string[] { "Вырезать", "(", ")", ";" }, StringSplitOptions.RemoveEmptyEntries);
            output[0] = "";
            for (int a = 0, add = 0; a < ret.Length; a++)
            {
                if (ret[a] == '(') add++;
                if (ret[a] == ')')
                {
                    if (add == 0)
                    {
                        break;
                    }
                    else add--;
                }
                output[0] = output[0] + ret[a];
            }
            string dl = output[0];
            if ((dl[0] == '(') && (dl[dl.Length - 1] == ')'))
            {
                dl = dl.Remove(0, 1);
                dl = dl.Remove(dl.Length - 1, 1);
                output[0] = dl;
            }
            string rGet = "Вырезать(" + output[0] + ")";
            output[0] = RecognizeFunctions(output[0]);
            string p = input;            
            //остальное  
            p = output[0];
            if (p.StartsWith("(") && (!p.Contains("Вырезать")))
            {
                p = p.Remove(0, 1);
                p = p.Remove(p.Length - 1, 1);
                string variable = p;
                if (p.StartsWith("$") && (!p.Contains("[")))
                {
                    p = p.Replace("$", "");
                    p = p.Replace("(", "");
                    p = p.Replace(")", "");
                    p = "Вырезать" + p;
                    variable = p;
                    VariablesList.Add(p, string.Empty);
                    variable = p;
                    //p = p.Remove(0, 13);
                    //p = p.Remove(p.Length - 1, 1);
                    if (VariablesList.ContainsKey(p))
                    {
                        VariablesList[p] = (VariablesList[p] as string[,]).ToString().Substring(Convert.ToInt32(Sequence(RecognizeVariables(output[1]))) - 1, Convert.ToInt32(Sequence(RecognizeVariables(output[2]))));
                            return "$" + variable;
                    }
                }
                else
                {
                    p = p.Replace("$", "");
                    p = p.Replace("(", "");
                    p = p.Replace(")", "");
                    string p1; int i = 0, j = 0;
                    bool mass = false;
                    if (p[p.Length - 1] == ']') mass = true;
                    if (mass)
                    {
                        variable = variable.Replace("[", "");
                        variable = variable.Replace("]", "");
                        variable = variable.Replace("$", "");
                        variable = "Вырезать" + variable;
                        if (!VariablesList.ContainsKey(variable))
                        VariablesList.Add(variable, string.Empty);
                        // variable = p;
                        p1 = p.Substring(p.IndexOf("[") + 1, p.IndexOf("]") - p.IndexOf("[") - 1);
                        string[] pMass = p1.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                        i = int.Parse(pMass[0]);
                        j = int.Parse(pMass[1]);
                    }
                    
                    string pp = p.Substring(0, p.IndexOf("["));
                    foreach (SpecialMacro m in f.specMacro)
                        if (m.ShortText.Substring(0, m.ShortText.Count() - 3) == pp)
                        {
                            if (mass)
                            {                               
                                    VariablesList[variable] = (m.value as string[,]).GetValue(i, j).ToString().Substring(Convert.ToInt32(Sequence(RecognizeVariables(output[1]))) - 1, Convert.ToInt32(Sequence(RecognizeVariables(output[2]))));
                                    return "$" + variable;                               
                            }
                            else
                            {
                                VariablesList[variable] = m.value.ToString().Length;
                                return "$" + variable;
                            }
                        }
                }

            }



            //string[] output = input.Split(new string[] { "Вырезать", "(", ")", ";" }, StringSplitOptions.RemoveEmptyEntries);
           /* output[0] =  Regex.Replace(output[0], @"\$", "");

            if (output[0].Contains("[") && output[0].Contains("]") && (!VariablesList.ContainsKey(output[0])))
                {
                    string[] rightArray = output[0].Split(new char[] { '[', ']', ',', '$' }, StringSplitOptions.RemoveEmptyEntries); //1,2=coords object
                    int i = int.Parse(rightArray[1]); int j = int.Parse(rightArray[2]);
                    foreach (SpecialMacro m in f.specMacro)
                        if (m.ShortText.Substring(0, m.ShortText.Count() - 3) == rightArray[0])
                        {
                            return FuncReplace((m.value as string[,]).GetValue(j, i).ToString().Substring(Convert.ToInt32(Sequence(RecognizeVariables(output[1]))) - 1,
                            Convert.ToInt32(Sequence(RecognizeVariables(output[2])))), all, rGet);                        
                        }

                    MessageBox.Show("private string CutCommand(string input) - не нашли спец. макрос в списке макросов. Сейчас будет ошибка!");
                    return FuncReplace((VariablesList[rightArray[0]] as string[,])[Convert.ToInt32(Sequence(RecognizeVariables(rightArray[1]))),
                        Convert.ToInt32(Sequence(RecognizeVariables(rightArray[2])))].Substring(Convert.ToInt32(Sequence(RecognizeVariables(output[1]))) - 1,
                            Convert.ToInt32(Sequence(RecognizeVariables(output[2])))),all,rGet);
                }

            return FuncReplace((VariablesList[output[0]] as string).Substring(Convert.ToInt32(Sequence(RecognizeVariables(output[1])))-1, 
                Convert.ToInt32(Sequence(RecognizeVariables(output[2])))),all,rGet);*/
           
           
            
            
            
            
            
            
            
            //---
            
            //string[] output = input.Split(new string[] { "Вырезать", "(", ")", ";" }, StringSplitOptions.RemoveEmptyEntries);
            output[0] = Regex.Replace(output[0], @"\$", "");
            output[0] = output[0].Replace(";" + output[1] + ";" + output[2], "");
            if (output[0].Contains("[") && output[0].Contains("]"))
            {
                string[] rightArray = output[0].Split(new char[] { '[', ']', ',', '$' }, StringSplitOptions.RemoveEmptyEntries);

                int i = int.Parse(rightArray[1]);
                int j = int.Parse(rightArray[2]);
                foreach (SpecialMacro m in f.specMacro)
                    if (m.ShortText.Substring(0, m.ShortText.Count() - 3) == rightArray[0])
                    {                       
                        return (m.value as string[,]).GetValue(i, j).ToString().Substring(Convert.ToInt32(Sequence(RecognizeVariables(output[1]))) - 1, Convert.ToInt32(Sequence(RecognizeVariables(output[2]))));
                    }
                    
                        MessageBox.Show("Не найден массив");
                    
               // return (VariablesList[rightArray[0]] as string[,])[i,j].Substring(Convert.ToInt32(Sequence(RecognizeVariables(output[1]))) - 1,
                       //Convert.ToInt32(Sequence(RecognizeVariables(output[2]))));
                /*return (VariablesList[rightArray[0]] as string[,])[Convert.ToInt32(Sequence(RecognizeVariables(rightArray[1]))),
                    Convert.ToInt32(Sequence(RecognizeVariables(rightArray[2])))].Substring(Convert.ToInt32(Sequence(RecognizeVariables(output[1]))) - 1,
                        Convert.ToInt32(Sequence(RecognizeVariables(output[2]))));*/
            }

            return (VariablesList[output[0]] as string).Substring(Convert.ToInt32(Sequence(RecognizeVariables(output[1])))-1,
                Convert.ToInt32(Sequence(RecognizeVariables(output[2]))));
        }
        private string FuncReplace(string ret, string all, string rGet)
        {
            return all.Replace(rGet,ret);
        }

                private string LengthCommand(string input, bool b)
        {         
            if (b)
            {
                string all = input;
                string ret = input;                
                ret = ret.Replace("Длина(", "");
                string[] output = input.Split(new string[] { "Длина", "(", ")" }, StringSplitOptions.RemoveEmptyEntries);
                output[0] = "";
                for (int a = 0, add = 0; a < ret.Length; a++)
                {
                    if (ret[a] == '(') add++;
                    if (ret[a] == ')')
                    {
                        if (add == 0)
                        {
                            break;
                        }
                        else add--;
                    }
                    output[0] = output[0] + ret[a];
                }
                string dl = output[0];   
                if ((dl[0] == '(') && (dl[dl.Length - 1] == ')'))
                {
                    dl = dl.Remove(0, 1);
                    dl = dl.Remove(dl.Length - 1, 1);
                    output[0] = dl;
                }
                string rGet = "(" + output[0] + ")";                
                output[0] = RecognizeFunctions(output[0]);
                output[0] = Regex.Replace(output[0], @"\$", "");                             
                if (output[0].Contains("[") && output[0].Contains("]"))
                {
                    string[] rightArray = output[0].Split(new char[] { '[', ']', ',', '$' }, StringSplitOptions.RemoveEmptyEntries);

                    int i = int.Parse(rightArray[1]);
                    int j = int.Parse(rightArray[2]);
                    foreach (SpecialMacro m in f.specMacro)
                        if (m.ShortText.Substring(0, m.ShortText.Count() - 3) == rightArray[0])
                        {

                            if (m.value is double[,])
                                return FuncReplace((m.value as double[,]).GetValue(j, i).ToString().Length.ToString(), all,rGet);   
                            if (m.value is string[,])
                                return FuncReplace((m.value as string[,]).GetValue(j, i).ToString().Length.ToString(), all,rGet); 
                        }


                    return FuncReplace((VariablesList[rightArray[0]] as string[,])[Convert.ToInt32(Sequence(RecognizeVariables(rightArray[1]))),
                        Convert.ToInt32(Sequence(RecognizeVariables(rightArray[2])))].Length.ToString(), all,rGet);
                }
                else if (output[0].Contains("^"))
                {
                    output[0] = output[0].Replace("^", "");
                    return FuncReplace(StringsList[output[0]].Length.ToString(), all, rGet);
                }else
                {
                    output[0] = output[0].Replace("$", "");
                return FuncReplace((VariablesList[output[0]] as string).Length.ToString(),all,rGet);
                }
                
            }
            else {
                string znak = "=";
                string[] data;
                data=input.Split(new string[] { znak }, StringSplitOptions.RemoveEmptyEntries);
                if (data.Length <= 1)
                {
                    znak = "<";
                    data = input.Split(new string[] { znak }, StringSplitOptions.RemoveEmptyEntries);
                }
                if (data.Length <= 1)
                {
                    znak = ">";
                    data = input.Split(new string[] { znak }, StringSplitOptions.RemoveEmptyEntries);
                }
                if (data.Length <= 1)
                {
                    return "error";
                }
                string[] output = data[0].Split(new string[] { "Длина", "(", ")" }, StringSplitOptions.RemoveEmptyEntries);
                if (output[0].Contains("[") && output[0].Contains("]"))
                {
                    output[0] = Regex.Replace(output[0], @"\$", "");
                    string[] rightArray = output[0].Split(new char[] { '[', ']', ',', '$' }, StringSplitOptions.RemoveEmptyEntries);

                    int i = int.Parse(rightArray[1]);
                    int j = int.Parse(rightArray[2]);
                    string ret = "Длина" + rightArray[0] + i.ToString() + j.ToString();
                    foreach (SpecialMacro m in f.specMacro)
                        if (m.ShortText.Substring(0, m.ShortText.Count() - 3) == rightArray[0])
                        {

                            if (m.value is double[,])
                            {
                                VariablesList.Add(ret, (m.value as double[,]).GetValue(j, i).ToString().Length.ToString());
                                return "$" + ret+ znak + data[1];                                
                            }
                            if (m.value is string[,])
                            {
                                VariablesList.Add(ret, (m.value as string[,]).GetValue(j, i).ToString().Length.ToString());
                                return "$" + ret + znak + data[1];
                            }
                        }
                    VariablesList.Add(ret, rightArray[0].ToString().Length.ToString());
                    return "$" + ret + znak + data[1];
                   
                }
                else if (output[0].Contains("$"))
                {
                    output[0] = output[0].Replace("$", "");
                    output[0] = output[0].Replace("(", "");
                    output[0] = output[0].Replace(")", "");
                    string ret = "Длина"+output[0];
                    if (!VariablesList.ContainsKey(ret)) VariablesList.Add(ret, VariablesList[output[0]].ToString().Length.ToString());
                    else VariablesList[ret] = VariablesList[output[0]].ToString().Length.ToString();
                    return "$" + ret + "=" + data[1];
                }
                else if (output[0].Contains("^"))
                {
                    output[0]=output[0].Replace("^", "");
                    output[0] = output[0].Replace("(", "");
                    output[0] = output[0].Replace(")", "");
                    string ret = "Длина" + output[0];
                    StringsList.Add(ret, StringsList[output[0]].ToString().Length.ToString());
                    return "$" + ret + znak + data[1];
                }
            }
            return "0";
        }

        private string Round(string input)
        {
            string[] output = input.Split(new string[] { "Округлить", "(", ")" }, StringSplitOptions.RemoveEmptyEntries);
            if (output[0].Contains("[") && output[0].Contains("]"))
            {
                string[] rightArray = output[0].Split(new char[] { '[', ']', ',', '$' }, StringSplitOptions.RemoveEmptyEntries);

                return (VariablesList[rightArray[0]] as double[,])[Convert.ToInt32(Sequence(RecognizeVariables(rightArray[1]))),
                    Convert.ToInt32(Sequence(RecognizeVariables(rightArray[2])))].ToString("f" + output[1]);
            }

            return (VariablesList[output[0]] as double?).Value.ToString("f" + output[1]);
        }

        

        private string RemoveSpacesRight(string input)
        {
            return input.TrimEnd();
        }
       
        private string RemoveSpacesLeft(string input)
        {
            return input.TrimStart();
        }

        private string RemoveSpacesAll(string input)
        {
            return input.Trim();
        }

        private string ToUpper(string input)
        {
            return input.ToUpper();
        }

        private string ToLower(string input)
        {
            return input.ToLower();
        }

        private string Capitalize(string input)
        {
            return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(input.ToLower());
        }

        private void SortArray(string sortingParams)
        {
            string[] param = sortingParams.Split(new string[] { ";", "Сортировать", "(", ")" }, StringSplitOptions.RemoveEmptyEntries);

            if (param.Count() < 4)
                throw new ArgumentNullException();

            int[] cols = new int[param.Count() - 1];
            for (int i = 3; i < param.Count(); i++)
                cols[i - 3] = Convert.ToInt32(param[i]);

            param[0] = param[0].Substring(1, param[0].Length - 1);

            if (!VariablesList.ContainsKey(param[0]) && f.specMacro.Any(p => p.ShortText == param[0].Substring(0, param[0].Length - 3)))
                return;

            if (VariablesList.ContainsKey(param[0]))
            {
                if (VariablesList[param[0]] is string[,])
                    SortCustom(VariablesList[param[0]] as string[,], Convert.ToInt32(param[1]), cols, "ASC",param[1]);
                else if(VariablesList[param[0]] is double[,])
                    SortCustom(VariablesList[param[0]] as double[,], Convert.ToInt32(param[1]), cols, "ASC", param[1]);
            }
            else
                SortCustom(f.specMacro.FirstOrDefault(m => m.ShortText.Substring(0, m.ShortText.Length - 3) == param[0]).value as string[,],
                    Convert.ToInt32(param[3]), cols, "ASC", param[1]);
        }

        private string Sep(string str)
        {
            if (System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator == ",") return str.Replace('.', ',');
            else return str.Replace(',', '.');
        }
        private static void SortCustom<T>(T[,] array, int tillRow,  int[] sortCol, string order,string row_not)
        {
            int colCount = array.GetLength(1), rowCount = array.GetLength(0);
            tillRow = int.Parse(row_not)+1;
            DataTable dt = new DataTable();
            // Name the columns with the second dimension index values, e.g., "0", "1", etc.
            for (int col = 0; col < colCount; col++)
            {
                DataColumn dc = new DataColumn(col.ToString(), typeof(T));
                dt.Columns.Add(dc);
            }
            // Load data into the data table:
            for (int rowindex = 0; rowindex < rowCount; rowindex++)
            {
                //if (rowindex <= int.Parse(row_not)+2) continue;
                DataRow rowData = dt.NewRow();
                for (int col = 0; col < colCount; col++)
                     rowData[col] = array[rowindex, col];
                dt.Rows.Add(rowData);
            }
            // Sort by using the column index = name + an optional order:
            string sort = "";
            for (int i = 0; i < sortCol.Length; i++)
                sort += sortCol[i].ToString() + ((i != sortCol.Length - 1) ? "," : "");
            sort += " " + order;
            DataRow[] rows = dt.Select("", sort).Where(m => (dt.Rows.IndexOf(m) >= tillRow)).ToArray();

            for (int row = tillRow; row <= rows.GetUpperBound(0); row++)
            {
                DataRow dr = rows[row - tillRow];
                for (int col = 0; col < colCount; col++)
                {
                    array[row, col] = (T)dr[col];
                }
            }

            dt.Dispose();
        }

    }


    public class SpecialMacro
    {
        public string ShortText;
        public string FullText;        
        //public int ID;
        //public delegate object MarcoDelegate();
        public object value;
        public SpecialMacro(string FullText, string ShortText, object value)
        {
            this.ShortText = ShortText;
            this.FullText = FullText;
            this.value = value;            
        }
    }

    public class CompilationResult
    {
        public enum Compilation_answer
        {
            compiled, failed
        }
        public Compilation_answer Result;
        public int FirstFailedLine = -1;
        public string ErrorText = "Successfuly compiled";

    }
}
