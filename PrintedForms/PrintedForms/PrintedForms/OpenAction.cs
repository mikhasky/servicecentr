﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Win32;
using System.Runtime.InteropServices;
using unoidl.com.sun.star.lang;
using unoidl.com.sun.star.uno;
using unoidl.com.sun.star.bridge;
using unoidl.com.sun.star.frame;
using unoidl.com.sun.star.text;
using unoidl.com.sun.star.beans;
using unoidl.com.sun.star.sheet;
using unoidl.com.sun.star.container;
using unoidl.com.sun.star.table;
using ooo=unoidl.com.sun.star;
using FirebirdSql.Data.FirebirdClient;
using System.IO;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;

namespace MKP.Printing
{
    static class OpenAction
    {
       private static XComponentContext oStrap;
       private static XMultiServiceFactory oServMan;
       private static XComponentLoader oDesk;
       private static string url = @"private:factory/scalc";
       private static PropertyValue[] propVals;
       private static XComponent xComponent;
       private static XSpreadsheets oSheets;
       private static XIndexAccess oSheetsIA;
       private static XSpreadsheet oSheet;
       private static XPropertySet xprop;
       private static XCellRange xrange;
       private static XCell oCell;
       private static ooo.frame.XFrame oFrame;
       private static ooo.awt.XWindow oWindow;
       private static ooo.style.XStyleFamiliesSupplier xSupplier;
       private static ooo.container.XNameAccess xFamilies;
       private static ooo.container.XNameContainer xFamily;
       private static ooo.style.XStyle xStyle;
       private static ooo.view.XPrintable xPrintable;
       private static ooo.table.XColumnRowRange xColRowRange;
       private static ooo.table.XTableColumns xColumns;
       private static ooo.table.XTableRows xRows;
       private static uno.Any aColumnObj;
       private static uno.Any aRowObj;
       private static ooo.util.XMergeable xMerge;
       public static void Perform(int action, string[] param)
       {
           System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
           Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
           switch (action)
           {
               //Формат А3
               case (0):
                   //excelWorksheet.PageSetup.PaperSize = Excel.XlPaperSize.xlPaperA3;
                   break;

               //Формат А4
               case (1):
                  // excelWorksheet.PageSetup.PaperSize = Excel.XlPaperSize.xlPaperA4;
                   break;

               //Формат А5
               case (2):
                  // excelWorksheet.PageSetup.PaperSize = Excel.XlPaperSize.xlPaperA5;
                   break;

               //Формат письмо 
               case (3):
                  // excelWorksheet.PageSetup.PaperSize = Excel.XlPaperSize.xlPaperLetter;
                   break;

               //Ориентация книга
               case (4):
                       propVals = new PropertyValue[1];
                       propVals[0] = new PropertyValue();
                       propVals[0].Name = "PaperOrientation";
                       propVals[0].Value = new uno.Any(typeof(ooo.view.PaperOrientation), ooo.view.PaperOrientation.PORTRAIT);
                       xPrintable.setPrinter(propVals);
                 //  excelWorksheet.PageSetup.Orientation = Excel.XlPageOrientation.xlPortrait;
                   break;

               //Ориентация альбом
               case (5):
                       propVals = new PropertyValue[1];
                       propVals[0] = new PropertyValue();
                       propVals[0].Name = "PaperOrientation";
                       propVals[0].Value = new uno.Any(typeof(ooo.view.PaperOrientation), ooo.view.PaperOrientation.LANDSCAPE);
                       xPrintable.setPrinter(propVals);
                   
                 //  excelWorksheet.PageSetup.Orientation = Excel.XlPageOrientation.xlLandscape;
                   break;

               //Центрировать на странице горизонтально
               case (6):
                 //  excelWorksheet.PageSetup.CenterHorizontally = true;
                   break;

               //Центрировать на странице вертикально
               case (7):
                  // excelWorksheet.PageSetup.CenterVertically = true;
                   break;

               //Работа EXCEL
               case (8):
                   oWindow.setVisible(true);
                   //ooo.frame.XFrame oFrame = ((ooo.frame.XDesktop)oDesk).getCurrentFrame();
                  // ooo.awt.XWindow oWindow = oFrame.getContainerWindow();
                   //oWindow.setVisible(true);
                   //xprop.setPropertyValue("Hidden", new uno.Any(false));
                  // excelApp.Visible = true;
                   break;

               //Закрыть EXCEL
               case (9):
                   oWindow.setVisible(false);
                  // excelApp.Visible = false;
                   break;

               //Поля листа
               case (10):
                     xprop.setPropertyValue("LeftMargin", new uno.Any((short)Convert.ToDouble(param[0])));
                     xprop.setPropertyValue("RightMargin", new uno.Any((short)Convert.ToDouble(param[1])));
                     xprop.setPropertyValue("BottomMargin", new uno.Any((short)Convert.ToDouble(param[2])));
                     xprop.setPropertyValue("TopMargin", new uno.Any((short)Convert.ToDouble(param[3])));
                  // excelWorksheet.PageSetup.LeftMargin = excelApp.InchesToPoints(0.393700787401575 * Convert.ToDouble(param[0]));
                   //excelWorksheet.PageSetup.RightMargin = excelApp.InchesToPoints(0.393700787401575 * Convert.ToDouble(param[1]));
                   //excelWorksheet.PageSetup.TopMargin = excelApp.InchesToPoints(0.393700787401575 * Convert.ToDouble(param[2]));
                   //excelWorksheet.PageSetup.BottomMargin = excelApp.InchesToPoints(0.393700787401575 * Convert.ToDouble(param[3]));
                   break;

               //Заливка ячейки
               case (11):
                    // excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Interior.ColorIndex = 15;
                   break;

               //Вывод фото в ячейку
               case (12):
                  
                   break;


               //Текст ячейки
               case (15):
                   oCell = oSheet.getCellByPosition( Convert.ToInt32(param[0]), Convert.ToInt32(param[1]) ); //A1
                   ((XText)oCell).setString(param[2].ToString());
                  // excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])] = param[2].ToString();
                  
                   break;

               //НазваниеОбъединение ячеек (стр; 10; стр; 24) листа
               case (16):                   
                 
                   break;

               //Текущий лист
               case (17):
                  // excelWorksheet = (Excel.Worksheet)excelSheets.get_Item(Convert.ToInt32(param[0]));
                   break;

               //Создать книгу EXCEL или Создать альбом EXCEL 
               case (18):
                     oStrap = uno.util.Bootstrap.bootstrap();
                     oServMan = (XMultiServiceFactory)oStrap.getServiceManager();
                     oDesk = (XComponentLoader)oServMan.createInstance("com.sun.star.frame.Desktop");
                     url = @"private:factory/scalc";
                     //propVals = new PropertyValue[1];
                     //propVals[0] = new PropertyValue();
                     //propVals[0].Name = "Hidden";
                     //propVals[0].Value = new uno.Any(false);
                     xComponent = oDesk.loadComponentFromURL(url, "_blank", 0, propVals);                    
                     oSheets = ((XSpreadsheetDocument)xComponent).getSheets();
                     oSheetsIA = (XIndexAccess)oSheets;
                     oSheet = (XSpreadsheet)oSheetsIA.getByIndex(0).Value;
                     xSupplier = (ooo.style.XStyleFamiliesSupplier)xComponent;
                     xPrintable = (ooo.view.XPrintable)xComponent;
                     xFamilies = (ooo.container.XNameAccess)xSupplier.getStyleFamilies();
                     xFamily = (ooo.container.XNameContainer)xFamilies.getByName("PageStyles").Value;
                     xStyle = (ooo.style.XStyle)xFamily.getByName("Default").Value;
                     xprop = (ooo.beans.XPropertySet)xStyle;         
                     oFrame = ((ooo.frame.XDesktop)oDesk).getCurrentFrame();
                     oWindow = oFrame.getContainerWindow();
                     oWindow.setVisible(false);
                     //xprop.setPropertyValue("CharFontName", new uno.Any((String)"Times New Roman"));
                     xprop.setPropertyValue("LeftMargin", new uno.Any((short)Convert.ToDouble(param[1])));
                     xprop.setPropertyValue("RightMargin", new uno.Any((short)Convert.ToDouble(param[2])));
                     xprop.setPropertyValue("BottomMargin", new uno.Any((short)Convert.ToDouble(param[3])));
                     xprop.setPropertyValue("TopMargin", new uno.Any((short)Convert.ToDouble(param[4])));
                   
                  
                  // excelApp = new Excel.Application();                  
                  // excelApp.StandardFont = "Times New Roman";
                  // excelApp.StandardFontSize = 12;
                 //  excelApp.DisplayAlerts = false;
                 //  excelApp.SheetsInNewWorkbook = 1;
                 //  excelWorkbook = excelApp.Workbooks.Add();
                 //  excelApp.Visible = false;
                 //  excelSheets = excelWorkbook.Worksheets;
                  // excelWorksheet = (Excel.Worksheet)excelSheets.get_Item(1);
                  // excelWorksheet.Name = param[0];
                                       
                   
                  // excelWorksheet.PageSetup.LeftMargin = excelApp.InchesToPoints(0.393700787401575 * Convert.ToDouble(param[1]));
                  // excelWorksheet.PageSetup.RightMargin = excelApp.InchesToPoints(0.393700787401575 * Convert.ToDouble(param[2]));
                 //  excelWorksheet.PageSetup.TopMargin = excelApp.InchesToPoints(0.393700787401575 * Convert.ToDouble(param[3]));
                 //  excelWorksheet.PageSetup.BottomMargin = excelApp.InchesToPoints(0.393700787401575 * Convert.ToDouble(param[4]));
                 //  excelWorksheet.PageSetup.HeaderMargin = excelApp.InchesToPoints(0);
                  // excelWorksheet.PageSetup.FooterMargin = excelApp.InchesToPoints(0);
                  // excelWorksheet.PageSetup.Zoom = false;
                  // excelWorksheet.PageSetup.FitToPagesWide = 1;
                  // excelWorksheet.PageSetup.FitToPagesTall = 100;
                 //  excelWorksheet.PageSetup.PrintQuality = 600;

                   //if (param[4] == "0")
                   //excelWorksheet.PageSetup.Orientation = Excel.XlPageOrientation.xlLandscape;
                   //else if (param[4] == "1")
                   //excelWorksheet.PageSetup.Orientation = Excel.XlPageOrientation.xlPortrait;
                   
                   break;

               //Создать альбом EXCEL ( !!!!!!! )
               //case (19):
               //    excelWorksheet = (Excel.Worksheet)excelSheets.get_Item(0);
               //    excelApp = new Excel.Application();
               //    excelApp.DisplayAlerts = false;
               //    excelApp.Workbooks.Add();
               //    excelApp.Visible = false;
               //    break;

               //Сохранить EXCEL
               case (20):

                   /*foreach (char c in System.IO.Path.GetInvalidFileNameChars())
                       param[1] = param[1].Replace(c, '_');

                   param[0] = param[0] + "\\" + param[1];
                   excelWorkbook.SaveAs(param[0], Type.Missing,
                       Type.Missing, Type.Missing, Type.Missing, Type.Missing, Excel.XlSaveAsAccessMode.xlNoChange,
                       Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                   excelApp.Visible = true;*/
                   break;

               //ОТКРЫТЬ EXCEL
               case (21):
                 /*  if (excelApp == null)
                   {
                       excelApp = new Excel.Application();
                       excelApp.DisplayAlerts = false;
                       excelWorkbook = excelApp.Workbooks.Open(param[0] + "\\" + param[1],
                                           1, false, 5, "", "", false, Excel.XlPlatform.xlWindows, "",
                                           true, false, 1, true, false, false);
                       excelSheets = excelWorkbook.Worksheets;
                       excelWorksheet = (Excel.Worksheet)excelSheets.get_Item(1);
                   }*/
                   break;

               //Разрыв страницы
               case (22):
                 //  excelWorksheet.PageSetup.Zoom = 100;
                   break;

               //Открыть Word ( !!!!!!!!!!! )
               //case (23):
               //excelWorksheet.PageSetup.Zoom = 100;
               //break;

               //Заменить в Word-е ( !!!!!!!!!!! )
               //case (24):
               //excelWorksheet.PageSetup.Zoom = 100;
               //break;

               //Сохранить Word ( !!!!!!!!!!! )
               //case (24):
               //excelWorksheet.PageSetup.Zoom = 100;
               //break;

               //Заголовок страниц
               case (25):
                  // excelWorksheet.PageSetup.PrintTitleRows = '$' + param[0] + ":$" + param[1];
                   break;

               //Добавить строку EXCEL ( !!!!!!!!!! )
               case (26):
                  // excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).EntireRow.Insert();
                   break;

               //Верхний колонтитул слева
               case (27):
                 //  excelWorksheet.PageSetup.LeftHeader = param[0];
                   break;

               //Верхний колонтитул по центру
               case (28):
                  // excelWorksheet.PageSetup.CenterHeader = param[0];
                   break;

               //Верхний колонтитул справа
               case (29):
                 //  excelWorksheet.PageSetup.RightHeader = param[0];
                   break;

               //Нижний колонтитул слева
               case (30):
                  // excelWorksheet.PageSetup.LeftFooter = param[0];
                   break;

               //Нижний колонтитул по центру
               case (31):
                  // excelWorksheet.PageSetup.CenterFooter = param[0];
                   break;

               //Нижний колонтитул справа
               case (32):
                  // excelWorksheet.PageSetup.RightFooter = param[0];
                   break;

               //Объединение ячеек--ОБЬЕДИНЕНИЕ!!!
               case (33):
                    xrange =  oSheet.getCellRangeByPosition(Convert.ToInt32(param[0]), Convert.ToInt32(param[1]), Convert.ToInt32(param[2]), Convert.ToInt32(param[3]));
                    xMerge = (ooo.util.XMergeable)xrange;
                    xMerge.merge(true);
   // prepareRange(xSheet,"E1:H7","XMergeable");
    //ooo.util.XMergeable xMerge =
           // (OOo.util.XMergeable)xCellRange;
   // xMerge.merge(true);
                  // excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Merge();
                   // excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],
                   //                        excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Merge();
                   break;

               //Ширина колонки
               case (34):
                   // --- Column properties. ---
                   xrange = oSheet.getCellRangeByPosition(Convert.ToInt32(param[0]), 1, Convert.ToInt32(param[0]), 65000);
                   xColRowRange =(ooo.table.XColumnRowRange)xrange;
                   xColumns =  xColRowRange.getColumns();
                   
                   aColumnObj = xColumns.getByIndex(0);
                   xprop = (ooo.beans.XPropertySet)aColumnObj.Value;
                   xprop.setPropertyValue("Width", new uno.Any((Int32)(Convert.ToInt32(param[1])*1000)));
                   /*param[1] = (param[1] == "0") ? "6" : param[1];
                   (excelWorksheet.Cells[1, Convert.ToInt32(param[0])] as Excel.Range).EntireColumn.ColumnWidth = (Convert.ToDouble(param[1]));*/ //Convert.ToInt32(param[1]);
                   break;

               //Ширина колонок
               case (35):
                   /*excelWorksheet.Range[GetExcelColumnName(Convert.ToInt32(param[0])) +
                        "1:" + GetExcelColumnName(Convert.ToInt32(param[1])) + '1'].ColumnWidth = (Convert.ToDouble(param[2]));*/
                   break;

               //Высота строки 
               case (36):
                  // (excelWorksheet.Cells[Convert.ToInt32(param[0]), 1] as Excel.Range).EntireRow.RowHeight = (Convert.ToDouble(param[1]));
                   break;

               //Высота строк 
               case (37):
                   xRows = xColRowRange.getRows();
                   aRowObj = xRows.getByIndex(6);
                   xprop = (ooo.beans.XPropertySet)aRowObj.Value;
                   xprop.setPropertyValue("Height",new uno.Any((Int32)(1000*Convert.ToInt32(param[2])))); //1000 = 1cм
                 //  excelWorksheet.Range['A' + param[0] + ":A" + param[1]].RowHeight = (Convert.ToDouble(param[2]));
                   break;

               //Перенос текста строки
               case (38):
               //    (excelWorksheet.Cells[Convert.ToInt32(param[0]), 1] as Excel.Range).EntireRow.WrapText = true;
                   break;

               //Перенос текста колонки
               case (39):
                  // (excelWorksheet.Cells[1, Convert.ToInt32(param[0])] as Excel.Range).EntireColumn.WrapText = true;
                   break;

               //Перенос текста ячейки
               case (40):
               //    excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]),
                 //      Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).WrapText = true;
                   break;

               //Отмена переноса текста строки
               case (41):
                  // (excelWorksheet.Cells[Convert.ToInt32(param[0]), 1] as Excel.Range).EntireRow.WrapText = false;
                   break;

               //Отмена переноса текста колонки
               case (42):
                  // (excelWorksheet.Cells[1, Convert.ToInt32(param[0])] as Excel.Range).EntireColumn.WrapText = false;
                   break;

               //Отмена переноса текста ячейки
               case (43):
                  // excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).WrapText = false;
                   break;

               //Поворот текста строки
               case (44):
                  // (excelWorksheet.Cells[Convert.ToInt32(param[0]), 1] as Excel.Range).EntireRow.Orientation = Convert.ToInt32(param[1]);
                   break;

               //Поворот текста колонки
               case (45):
                 //  (excelWorksheet.Cells[1, Convert.ToInt32(param[0])] as Excel.Range).EntireColumn.Orientation = Convert.ToInt32(param[1]);
                   break;

               //Поворот текста ячейки
               case (46):
                   // excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Orientation = Convert.ToInt32(param[4]);
                   break;

               //Размер шрифта строки
               case (47):
                  // (excelWorksheet.Cells[Convert.ToInt32(param[0]), 1] as Excel.Range).EntireRow.Font.Size = (Convert.ToDouble(param[1]));
                   break;

               //Размер шрифта колонки
               case (48):
                  // (excelWorksheet.Cells[1, Convert.ToInt32(param[0])] as Excel.Range).EntireColumn.Font.Size = (Convert.ToDouble(param[1]));
                   break;

               //Размер шрифта ячейки
               case (49):
                    xrange = oSheet.getCellRangeByPosition(Convert.ToInt32(param[0]), Convert.ToInt32(param[1]), Convert.ToInt32(param[2]), Convert.ToInt32(param[3]));  
                    xprop = (ooo.beans.XPropertySet)xrange;
                    xprop.setPropertyValue("CharHeight", new uno.Any((Single)Convert.ToDouble(param[4]))); 
                   //  excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Font.Size = (Convert.ToDouble(param[4]));
                   break;

               //Название шрифта строки
               case (50):
                  // (excelWorksheet.Cells[Convert.ToInt32(param[0]), 1] as Excel.Range).EntireRow.Font.Name = param[1];
                   break;

               //Название шрифта колонки
               case (51):
                 //  (excelWorksheet.Cells[1, Convert.ToInt32(param[0])] as Excel.Range).EntireColumn.Font.Name = param[1];
                   break;

               //Название шрифта ячейки
               case (52):
                  // excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Font.Name = param[4];
                   break;

               //Выравнивание по вертикали строки по верхнему краю 
               case (53):
                //   (excelWorksheet.Cells[Convert.ToInt32(param[0]), 1] as Excel.Range).EntireRow.VerticalAlignment = Excel.XlVAlign.xlVAlignTop;
                   break;

               //Выравнивание по вертикали строки по центру
               case (54):
                  // (excelWorksheet.Cells[Convert.ToInt32(param[0]), 1] as Excel.Range).EntireRow.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
                   break;

               //Выравнивание по вертикали строки по нижнему краю 
               case (55):
                  // (excelWorksheet.Cells[Convert.ToInt32(param[0]), 1] as Excel.Range).EntireRow.VerticalAlignment = Excel.XlVAlign.xlVAlignBottom;
                   break;

               //Выравнивание по вертикали строки по высоте
               case (56):
                   //(excelWorksheet.Cells[Convert.ToInt32(param[0]), 1] as Excel.Range).EntireRow.VerticalAlignment = Excel.XlVAlign.xlVAlignDistributed;
                   break;

               //Выравнивание по вертикали колонки по верхнему краю 
               case (57):
                  // (excelWorksheet.Cells[1, Convert.ToInt32(param[0])] as Excel.Range).EntireColumn.VerticalAlignment = Excel.XlVAlign.xlVAlignTop;
                   break;

               //Выравнивание по вертикали колонки по центру
               case (58):
                   //(excelWorksheet.Cells[1, Convert.ToInt32(param[0])] as Excel.Range).EntireColumn.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
                   break;

               //Выравнивание по вертикали колонки по нижнему краю 
               case (59):
                   //(excelWorksheet.Cells[1, Convert.ToInt32(param[0])] as Excel.Range).EntireColumn.VerticalAlignment = Excel.XlVAlign.xlVAlignBottom;
                   break;

               //Выравнивание по вертикали колонки по высоте
               case (60):
                   //(excelWorksheet.Cells[1, Convert.ToInt32(param[0])] as Excel.Range).EntireColumn.VerticalAlignment = Excel.XlVAlign.xlVAlignDistributed;
                   break;

               //Выравнивание по вертикали ячейки по верхнему краю 
               case (61):
                   //excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).VerticalAlignment = Excel.XlVAlign.xlVAlignTop;
                   break;

               //Выравнивание по вертикали ячейки по центру
               case (62):
                  // excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
                   break;

               //Выравнивание по вертикали ячейки по нижнему краю 
               case (63):
                   //excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).VerticalAlignment = Excel.XlVAlign.xlVAlignBottom;
                   break;

               //Выравнивание по вертикали ячейки по высоте
               case (64):
                  // excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).VerticalAlignment = Excel.XlVAlign.xlVAlignDistributed;
                   break;

               //Выравнивание по горизонтали строки по значению 
               case (65):
                  // (excelWorksheet.Cells[Convert.ToInt32(param[0]), 1] as Excel.Range).EntireRow.HorizontalAlignment = Excel.XlHAlign.xlHAlignFill;
                   break;

               //Выравнивание по горизонтали строки по левому краю
               case (66):
                  // (excelWorksheet.Cells[Convert.ToInt32(param[0]), 1] as Excel.Range).EntireRow.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
                   break;

               //Выравнивание по горизонтали строки по центру 
               case (67):
                  // (excelWorksheet.Cells[Convert.ToInt32(param[0]), 1] as Excel.Range).EntireRow.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                   break;

               //Выравнивание по горизонтали строки по правому краю
               case (68):
                  // (excelWorksheet.Cells[Convert.ToInt32(param[0]), 1] as Excel.Range).EntireRow.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
                   break;

               //Выравнивание по горизонтали колонки по значению 
               case (69):
                  // (excelWorksheet.Cells[1, Convert.ToInt32(param[0])] as Excel.Range).EntireColumn.HorizontalAlignment = Excel.XlHAlign.xlHAlignFill;
                   break;

               //Выравнивание по горизонтали колонки по левому краю
               case (70):
                   //(excelWorksheet.Cells[1, Convert.ToInt32(param[0])] as Excel.Range).EntireColumn.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
                   break;

               //Выравнивание по горизонтали колонки по центру 
               case (71):
                  // (excelWorksheet.Cells[1, Convert.ToInt32(param[0])] as Excel.Range).EntireColumn.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                   break;

               //Выравнивание по горизонтали колонки по правому краю
               case (72):
                  // (excelWorksheet.Cells[1, Convert.ToInt32(param[0])] as Excel.Range).EntireColumn.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
                   break;

               //Выравнивание по горизонтали ячейки по значению 
               case (73):
                  // excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).HorizontalAlignment = Excel.XlHAlign.xlHAlignFill;
                   break;

               //Выравнивание по горизонтали ячейки по левому краю
               case (74):
                  // excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
                   break;

               //Выравнивание по горизонтали ячейки по центру 
               case (75):
                   //xPropSet.setPropertyValue("HoriJustify", new uno.Any((Int32)(2)));
                   //xPropSet.setPropertyValue("VertJustify", new uno.Any((Int32)(2)));
                   xrange = oSheet.getCellRangeByPosition(Convert.ToInt32(param[0]), Convert.ToInt32(param[1]), Convert.ToInt32(param[2]), Convert.ToInt32(param[3]));  
                   xprop = (ooo.beans.XPropertySet)xrange;
                   xprop.setPropertyValue("HoriJustify", new uno.Any((Int32)(2)));                    
                   
                   //excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                   break;

               //Выравнивание по горизонтали ячейки по правому краю
               case (76):
                 //  excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
                   break;

               //Жирный шрифт строки
               case (77):
                  // (excelWorksheet.Cells[Convert.ToInt32(param[0]), 1] as Excel.Range).EntireRow.Font.Bold = true;
                   break;

               //Жирный шрифт строки отмена
               case (78):
                 //  (excelWorksheet.Cells[Convert.ToInt32(param[0]), 1] as Excel.Range).EntireRow.Font.Bold = false;
                   break;

               //Жирный шрифт колонки
               case (79):
                  // (excelWorksheet.Cells[1, Convert.ToInt32(param[0])] as Excel.Range).EntireColumn.Font.Bold = true;
                   break;

               //Жирный шрифт колонки отмена
               case (80):
                 //  (excelWorksheet.Cells[1, Convert.ToInt32(param[0])] as Excel.Range).EntireColumn.Font.Bold = false;
                   break;

               //Жирный шрифт ячейки
               case (81):
                 //  excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Font.Bold = true;
                   break;

               //Жирный шрифт ячейки отмена 
               case (82):
                 //  excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Font.Bold = false;
                   break;

               //Наклонный шрифт строки
               case (83):
                  // (excelWorksheet.Cells[Convert.ToInt32(param[0]), 1] as Excel.Range).EntireRow.Font.Italic = true;
                   break;

               //Наклонный шрифт строки отмена
               case (84):
                 //  (excelWorksheet.Cells[Convert.ToInt32(param[0]), 1] as Excel.Range).EntireRow.Font.Italic = false;
                   break;

               //Наклонный шрифт колонки
               case (85):
                //   (excelWorksheet.Cells[1, Convert.ToInt32(param[0])] as Excel.Range).EntireColumn.Font.Italic = true;
                   break;

               //Наклонный шрифт колонки отмена
               case (86):
                //   (excelWorksheet.Cells[1, Convert.ToInt32(param[0])] as Excel.Range).EntireColumn.Font.Italic = false;
                   break;

               //Наклонный шрифт ячейки
               case (87):
                //   excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Font.Italic = true;
                   break;

               //Наклонный шрифт ячейки отмена 
               case (88):

                 //  excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Font.Italic = false;
                   break;

               //Подчеркивание шрифта строки тонкой линией 
               case (89):
                //   (excelWorksheet.Cells[Convert.ToInt32(param[0]), 1] as Excel.Range).EntireRow.Font.Underline = Excel.XlUnderlineStyle.xlUnderlineStyleSingle;
                   break;

               //Подчеркивание шрифта строки толстой линией 
               case (90):
                 //  (excelWorksheet.Cells[Convert.ToInt32(param[0]), 1] as Excel.Range).EntireRow.Font.Underline = Excel.XlUnderlineStyle.xlUnderlineStyleDouble;
                   break;

               //Подчеркивание шрифта строки отмена 
               case (91):
                 //  (excelWorksheet.Cells[Convert.ToInt32(param[0]), 1] as Excel.Range).EntireRow.Font.Underline = Excel.XlUnderlineStyle.xlUnderlineStyleNone;
                   break;

               //Подчеркивание шрифта колонки тонкой линией 
               case (92):
                  // (excelWorksheet.Cells[1, Convert.ToInt32(param[0])] as Excel.Range).EntireColumn.Font.Underline = Excel.XlUnderlineStyle.xlUnderlineStyleSingle;
                   break;

               //Подчеркивание шрифта колонки толстой линией 
               case (93):
                  // (excelWorksheet.Cells[1, Convert.ToInt32(param[0])] as Excel.Range).EntireColumn.Font.Underline = Excel.XlUnderlineStyle.xlUnderlineStyleDouble;
                   break;

               //Подчеркивание шрифта колонки отмена 
               case (94):
                //   (excelWorksheet.Cells[1, Convert.ToInt32(param[0])] as Excel.Range).EntireColumn.Font.Underline = Excel.XlUnderlineStyle.xlUnderlineStyleNone;
                   break;

               //Подчеркивание шрифта ячейки тонкой линией 
               case (95):

                  // excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Font.Underline = Excel.XlUnderlineStyle.xlUnderlineStyleSingle;
                   break;

               //Подчеркивание шрифта ячейки толстой линией 
               case (96):

                //   excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Font.Underline = Excel.XlUnderlineStyle.xlUnderlineStyleDouble;
                   break;

               //Подчеркивание шрифта ячейки отмена 
               case (97):

                //   excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Font.Underline = Excel.XlUnderlineStyle.xlUnderlineStyleNone;
                   break;

               //Линия всей ячейки нормальная 
               case (98):

                //   excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                //   excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders.Weight = Excel.XlBorderWeight.xlMedium;
                   break;

               //Линия всей ячейки тонкая 
               case (99):

               //    excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders.Weight = Excel.XlBorderWeight.xlThin;
                   break;

               //Линия всей ячейки толстая 
               case (100):

               //    excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders.Weight = Excel.XlBorderWeight.xlThick;
                   break;

               //Линия всей ячейки отмена 
               case (101):

               //    excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders.LineStyle = Excel.XlLineStyle.xlLineStyleNone;
                   break;

               //Линия всей ячейки двойная 
               case (102):

                //   excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders.LineStyle = Excel.XlLineStyle.xlDouble;
                   break;

               //Линия всей ячейки пунктирная 
               case (103):

                //   excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders.LineStyle = Excel.XlLineStyle.xlDashDot;
                   break;

               //Линия всей ячейки точками 
               case (104):

                //   excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders.LineStyle = Excel.XlLineStyle.xlDot;
                   break;

               //Линия вокруг ячейки нормальная 
               case (105):

                //   excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle
               //    = Excel.XlLineStyle.xlContinuous;

                 //  excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle
                 //  = Excel.XlLineStyle.xlContinuous;

                //   excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle
                 //  = Excel.XlLineStyle.xlContinuous;

                 //  excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle
                 //  = Excel.XlLineStyle.xlContinuous;


                 //  excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeLeft].Weight
                 //  = Excel.XlBorderWeight.xlMedium;

                 //  excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeRight].Weight
                 //  = Excel.XlBorderWeight.xlMedium;

                  // excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeTop].Weight
                  // = Excel.XlBorderWeight.xlMedium;

                 //  excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeBottom].Weight
                 //  = Excel.XlBorderWeight.xlMedium;

                   break;

               //Линия вокруг ячейки тонкая 
               case (106):

                //   excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeLeft].Weight
                 //  = Excel.XlBorderWeight.xlThin;

                //   excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeRight].Weight
                  // = Excel.XlBorderWeight.xlThin;

                  // excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeTop].Weight
                  // = Excel.XlBorderWeight.xlThin;

                  // excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeBottom].Weight
                  // = Excel.XlBorderWeight.xlThin;
                   break;

               //Линия вокруг ячейки толстая 
               case (107):

                 //  excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeLeft].Weight
                //   = Excel.XlBorderWeight.xlThick;

                //   excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeRight].Weight
                //   = Excel.XlBorderWeight.xlThick;

                //   excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeTop].Weight
                //   = Excel.XlBorderWeight.xlThick;

                //   excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeBottom].Weight
                //   = Excel.XlBorderWeight.xlThick;
                   break;

               //Линия вокруг ячейки отмена 
               case (108):

               //    excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle
                //   = Excel.XlLineStyle.xlLineStyleNone;

                //   excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle
                 //  = Excel.XlLineStyle.xlLineStyleNone;

                 //  excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle
                  // = Excel.XlLineStyle.xlLineStyleNone;

                 //  excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle
                 //  = Excel.XlLineStyle.xlLineStyleNone;
                   break;

               //Линия вокруг ячейки двойная 
               case (109):

               //    excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle
               //    = Excel.XlLineStyle.xlDouble;

               //    excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle
               //    = Excel.XlLineStyle.xlDouble;

                //   excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle
                //   = Excel.XlLineStyle.xlDouble;

                 //  excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle
                 //  = Excel.XlLineStyle.xlDouble;
                   break;

               //Линия вокруг ячейки пунктирная 
               case (110):

               //    excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle
               //    = Excel.XlLineStyle.xlDashDot;

                 //  excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle
                //   = Excel.XlLineStyle.xlDashDot;

              //     excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle
                //   = Excel.XlLineStyle.xlDashDot;

                //   excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle
                 //  = Excel.XlLineStyle.xlDashDot;
                   break;

               //Линия вокруг ячейки точками 
               case (111):

                 //  excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle
                 //  = Excel.XlLineStyle.xlDot;

                 //  excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle
                 //  = Excel.XlLineStyle.xlDot;

                //   excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle
                 //  = Excel.XlLineStyle.xlDot;

                 //  excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle
                //   = Excel.XlLineStyle.xlDot;
                   break;

               //Линия сверху ячейки нормальная 
               case (112):

                 //  excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle
                 //  = Excel.XlLineStyle.xlContinuous;


                //   excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeTop].Weight
                //   = Excel.XlBorderWeight.xlMedium;
                   break;

               //Линия сверху ячейки тонкая 
               case (113):

                //   excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeTop].Weight
                 //  = Excel.XlBorderWeight.xlThin;
                   break;

               //Линия сверху ячейки толстая 
               case (114):

                //   excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeTop].Weight
                 //  = Excel.XlBorderWeight.xlThick;
                   break;

               //Линия сверху ячейки отмена 
               case (115):

                //   excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle
                 //  = Excel.XlLineStyle.xlLineStyleNone;
                   break;

               //Линия сверху ячейки двойная 
               case (116):

                //   excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle
                //   = Excel.XlLineStyle.xlDouble;
                   break;

               //Линия сверху ячейки пунктирная 
               case (117):

               //    excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle
                //   = Excel.XlLineStyle.xlDashDot;
                   break;

               //Линия сверху ячейки точками 
               case (118):

               //    excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle
               //    = Excel.XlLineStyle.xlDot;
                   break;

               //Линия слева ячейки нормальная 
               case (119):

               //    excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle
                //   = Excel.XlLineStyle.xlContinuous;


                //   excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeLeft].Weight
                //   = Excel.XlBorderWeight.xlMedium;
                   break;

               //Линия слева ячейки тонкая 
               case (120):

               //    excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeLeft].Weight
               //    = Excel.XlBorderWeight.xlThin;
                   break;

               //Линия слева ячейки толстая 
               case (121):

                //   excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeLeft].Weight
                //   = Excel.XlBorderWeight.xlThick;
                   break;

               //Линия слева ячейки отмена 
               case (122):

                //   excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle
                 //  = Excel.XlLineStyle.xlLineStyleNone;
                   break;

               //Линия слева ячейки двойная 
               case (123):

               //    excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle
                //   = Excel.XlLineStyle.xlDouble;
                   break;

               //Линия слева ячейки пунктирная 
               case (124):

                //   excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle
               //    = Excel.XlLineStyle.xlDashDot;
                   break;

               //Линия слева ячейки точками 
               case (125):

                 //  excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle
                 //  = Excel.XlLineStyle.xlDot;
                   break;

               //Линия снизу ячейки нормальная 
               case (126):

               //    excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle
                //   = Excel.XlLineStyle.xlContinuous;


                 //  excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeBottom].Weight
                 //  = Excel.XlBorderWeight.xlMedium;
                   break;

               //Линия снизу ячейки тонкая 
               case (127):

                //   excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeBottom].Weight
                //   = Excel.XlBorderWeight.xlThin;
                   break;

               //Линия снизу ячейки толстая 
               case (128):

                 //  excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeBottom].Weight
                //   = Excel.XlBorderWeight.xlThick;
                   break;

               //Линия снизу ячейки отмена 
               case (129):

                //   excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle
                //   = Excel.XlLineStyle.xlLineStyleNone;
                   break;

               //Линия снизу ячейки двойная 
               case (130):

                 //  excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle
                //   = Excel.XlLineStyle.xlDouble;
                   break;

               //Линия снизу ячейки пунктирная 
               case (131):

                 //  excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle
                //   = Excel.XlLineStyle.xlDashDot;
                   break;

               //Линия снизу ячейки точками 
               case (132):

                 //  excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle
                 //  = Excel.XlLineStyle.xlDot;
                   break;

               //Линия справа ячейки нормальная 
               case (133):

                 //  excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle
                 //  = Excel.XlLineStyle.xlContinuous;


                 //  excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeRight].Weight
                //   = Excel.XlBorderWeight.xlMedium;
                   break;

               //Линия справа ячейки тонкая 
               case (134):

               //    excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeRight].Weight
               //    = Excel.XlBorderWeight.xlThin;
                   break;

               //Линия справа ячейки толстая 
               case (135):

               //    excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeRight].Weight
              //     = Excel.XlBorderWeight.xlThick;
                   break;
               //Линия справа ячейки отмена 
               case (136):

               //    excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle
                //   = Excel.XlLineStyle.xlLineStyleNone;
                   break;

               //Линия справа ячейки двойная 
               case (137):

                //   excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle
                //   = Excel.XlLineStyle.xlDouble;
                   break;

               //Линия справа ячейки пунктирная 
               case (138):

                 //  excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle
                //   = Excel.XlLineStyle.xlDashDot;
                   break;

               //Линия вокруг ячейки точками 
               case (139):

               //    excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle
               //    = Excel.XlLineStyle.xlDot;
                   break;

               //Толщина линии всей ячейки 1 
               case (140):

                //   excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders.Weight = Excel.XlBorderWeight.xlHairline;
                   break;

               //Толщина линии всей ячейки 2 
               case (141):

                //   excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders.Weight = Excel.XlBorderWeight.xlThin;
                   break;

               //Толщина линии всей ячейки 3 
               case (142):

                //   excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders.Weight = Excel.XlBorderWeight.xlMedium;
                   break;

               //Толщина линии всей ячейки 4
               case (143):

                 //  excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders.Weight = Excel.XlBorderWeight.xlThick;
                   break;

               //Толщина линии вокруг ячейки 1
               case (144):

                //   excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeLeft].Weight
                //   = Excel.XlBorderWeight.xlHairline;

                //   excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeRight].Weight
                //   = Excel.XlBorderWeight.xlHairline;

               //    excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeTop].Weight
                //   = Excel.XlBorderWeight.xlHairline;

                 //  excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeBottom].Weight
                //   = Excel.XlBorderWeight.xlHairline;
                   break;


               //Толщина линии вокруг ячейки 2
               case (145):

                 //  excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeLeft].Weight
                //   = Excel.XlBorderWeight.xlThin;

                //   excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeRight].Weight
               //    = Excel.XlBorderWeight.xlThin;

                //   excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeTop].Weight
               //    = Excel.XlBorderWeight.xlThin;

               //    excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeBottom].Weight
               //    = Excel.XlBorderWeight.xlThin;
                   break;

               //Толщина линии вокруг ячейки 3
               case (146):

                //   excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeLeft].Weight
               //    = Excel.XlBorderWeight.xlMedium;

               //    excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeRight].Weight
               //    = Excel.XlBorderWeight.xlMedium;

                //   excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeTop].Weight
                 //  = Excel.XlBorderWeight.xlMedium;

                //   excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeBottom].Weight
                //   = Excel.XlBorderWeight.xlMedium;
                   break;

               //Толщина линии вокруг ячейки 4
               case (147):

                //   excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeLeft].Weight
                 //  = Excel.XlBorderWeight.xlThick;

                 //  excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeRight].Weight
                 //  = Excel.XlBorderWeight.xlThick;

                 //  excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeTop].Weight
                 //  = Excel.XlBorderWeight.xlThick;

                 //  excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeBottom].Weight
                 //  = Excel.XlBorderWeight.xlThick;
                   break;

               //Толщина линии сверху ячейки 1  
               case (148):

                 //  excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeTop].Weight
                //   = Excel.XlBorderWeight.xlHairline;
                   break;

               //Толщина линии сверху ячейки 2  
               case (150):

                //   excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeTop].Weight
                //   = Excel.XlBorderWeight.xlThin;
                   break;

               //Толщина линии сверху ячейки 3 
               case (151):

                 //  excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeTop].Weight
                 //  = Excel.XlBorderWeight.xlMedium;
                   break;

               //Толщина линии сверху ячейки 4  
               case (152):

                //   excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeTop].Weight
                //   = Excel.XlBorderWeight.xlThick;
                   break;

               //Толщина линии снизу ячейки 1  
               case (153):

                //   excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeBottom].Weight
                //   = Excel.XlBorderWeight.xlHairline;
                   break;

               //Толщина линии снизу ячейки 2  
               case (154):

                //   excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeBottom].Weight
                //   = Excel.XlBorderWeight.xlThin;
                   break;

               //Толщина линии снизу ячейки 3 
               case (155):

                //   excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeBottom].Weight
                //   = Excel.XlBorderWeight.xlMedium;
                   break;

               //Толщина линии снизу ячейки 4  
               case (156):

                 //  excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeBottom].Weight
                //   = Excel.XlBorderWeight.xlThick;
                   break;

               //Толщина линии справа ячейки 1  
               case (157):

                //   excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeRight].Weight
                //   = Excel.XlBorderWeight.xlHairline;
                   break;

               //Толщина линии справа ячейки 2  
               case (158):

              //     excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeRight].Weight
             //      = Excel.XlBorderWeight.xlThin;
                   break;

               //Толщина линии справа ячейки 3 
               case (159):
               //    excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeRight].Weight
               //    = Excel.XlBorderWeight.xlMedium;
                   break;

               //Толщина линии справа ячейки 4  
               case (160):
               //    excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeRight].Weight
              //     = Excel.XlBorderWeight.xlThick;
                   break;

               //Толщина слева справа ячейки 1  
               case (161):

               //    excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeLeft].Weight
                //   = Excel.XlBorderWeight.xlHairline;
                   break;

               //Толщина линии справа ячейки 2  
               case (162):

              //     excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeLeft].Weight
             //      = Excel.XlBorderWeight.xlThin;
                   break;

               //Толщина линии справа ячейки 3 
               case (163):

             //      excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeLeft].Weight
             //      = Excel.XlBorderWeight.xlMedium;
                   break;

               //Толщина линии справа ячейки 4  
               case (164):

               //    excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeLeft].Weight
               //    = Excel.XlBorderWeight.xlThick;
                   break;

               //Формат строки общий 
               case (165):
               //    (excelWorksheet.Cells[Convert.ToInt32(param[0]), 1] as Excel.Range).EntireRow.NumberFormat = "0";
                   break;

               //Формат строки числовой 
               case (166):
               //    (excelWorksheet.Cells[Convert.ToInt32(param[0]), 1] as Excel.Range).EntireRow.NumberFormat = "0,00";
                   break;

               //Формат строки текстовый 
               case (167):
               //    (excelWorksheet.Cells[Convert.ToInt32(param[0]), 1] as Excel.Range).EntireRow.NumberFormat = "@";
                   break;

               //Формат строки дата 
               case (168):
                 //  (excelWorksheet.Cells[Convert.ToInt32(param[0]), 1] as Excel.Range).EntireRow.NumberFormat = "d, mmmm, yyyy";
                   break;

               //Формат колонки общий 
               case (169):
                //   (excelWorksheet.Cells[1, Convert.ToInt32(param[0])] as Excel.Range).EntireColumn.NumberFormat = "0";
                   break;

               //Формат колонки числовой 
               case (170):
               //    (excelWorksheet.Cells[1, Convert.ToInt32(param[0])] as Excel.Range).EntireColumn.NumberFormat = "0,00";
                   break;

               //Формат колонки текстовый 
               case (171):
                //   (excelWorksheet.Cells[1, Convert.ToInt32(param[0])] as Excel.Range).EntireColumn.NumberFormat = "@";
                   break;

               //Формат колонки дата 
               case (172):
                 //  (excelWorksheet.Cells[1, Convert.ToInt32(param[0])] as Excel.Range).EntireColumn.NumberFormat = "d, mmmm, yyyy";
                   break;

               //Формат ячейки общий 
               case (173):
                //   excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).NumberFormat = "0";
                   break;

               //Формат ячейки числвой 
               case (174):
                  // excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).NumberFormat = "0,00";
                   break;

               //Формат ячейки текст 
               case (175):
                 //  excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).NumberFormat = "@";
                   break;

               //Формат ячейки дата 
               case (176):
                //   excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).NumberFormat = "d, mmmm, yyyy";
                   break;

               //Подчеркивание символов в ячейке тонкой линией 
               case (177):
              //     (excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])] as Excel.Range).Characters[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])].Font.Underline
                //       = Excel.XlUnderlineStyle.xlUnderlineStyleSingle;
                   break;


               //Подчеркивание символов в ячейке жирной линией 
               case (179):
               //    (excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])] as Excel.Range).Characters[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])].Font.Underline
                //       = Excel.XlUnderlineStyle.xlUnderlineStyleDouble;
                   break;

               //Подчеркивание символов в ячейке отмена
               case (180):
               //    (excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])] as Excel.Range).Characters[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])].Font.Underline
               //        = Excel.XlUnderlineStyle.xlUnderlineStyleNone;
                   break;

               //Жирность символов в ячейке
               case (181):
              //     (excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])] as Excel.Range).Characters[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])].Font.Bold
                  //     = true;
                   break;

               //Жирность символов в ячейке отмена
               case (182):
              //     (excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])] as Excel.Range).Characters[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])].Font.Bold
                 //      = false;
                   break;

               //Наклонность символов в ячейке
               case (183):

                 //  (excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])] as Excel.Range).Characters[Convert.ToInt32(param[2]), Convert.ToInt32(param[2])].Font.Italic
                 //  = true;
                   break;

               //Наклонность символов в ячейке отмена
               case (184):

               //    (excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])] as Excel.Range).Characters[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])].Font.Italic
                  //     = false;
                   break;

               //Размер символов в ячейке
               case (185):
                //   (excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])] as Excel.Range).Characters[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])].Font.Size
                 //  = Convert.ToDouble((param[4]));
                   break;

               //Автоподбор высоты
               case (186):
               //    (excelWorksheet.Cells[1, Convert.ToInt32(param[0])] as Excel.Range).EntireColumn.AutoFit();
                   break;

               //Выравнивание по горизонтали строки по ширине
               case (189):
                //   (excelWorksheet.Cells[Convert.ToInt32(param[0]), 1] as Excel.Range).EntireRow.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                   break;

               //Выравнивание по горизонтали колонки по ширине
               case (190):
                //   (excelWorksheet.Cells[1, Convert.ToInt32(param[0])] as Excel.Range).EntireColumn.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                   break;

               //Выравнивание по горизонтали ячейки по ширине 
               case (191):
               //    excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                   break;

               //Добавить листов
               case (192):                   
                   oSheets.insertNewByName(param[0], (short)0x7FFF);                   
               //    excelWorkbook.Worksheets.Add(Type.Missing, Type.Missing, Convert.ToInt32(param[0]), Type.Missing);
                   break;

               //Название листа листов
               case (193):
                   //oSheets.hasByName(param[0]);
               //    Excel.Worksheet xlSheet = (excelWorkbook.Worksheets.get_Item(Convert.ToInt32(param[0])) as Excel.Worksheet);
                //   xlSheet.Name = param[1];
                   break;

               //Текущий лист
               case (194):
                   oSheet = (ooo.sheet.XSpreadsheet)oSheets.getByName((param[0])).Value;
              //     ((Excel.Worksheet)excelWorkbook.Sheets[Convert.ToInt32(param[0])]).Select(Type.Missing);
                   break;
           }
       }
      

    }
        
}