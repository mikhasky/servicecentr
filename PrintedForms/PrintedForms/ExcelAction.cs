﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Excel = Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;
using System.Reflection;
using Microsoft.Office;
using FirebirdSql.Data.FirebirdClient;
using System.IO;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;
using System.Data;
using MKP.Printing.Helper;
using System.Security.AccessControl;
using System.Security;
using System.Security.Permissions;
using System.Drawing;

namespace MKP.Printing
{
    public static class ExcelAction
    {
        private static bool initialized { get; set; } = false;
        public static Excel.Application excelApp { get; set; } = new Excel.Application();
        public static Excel.Sheets excelSheets { get; set; }
        public static Excel.Worksheet excelWorksheet { get; set; }
        public static Excel.Workbook excelWorkbook { get; set; }
        public static string[] CombineColumns { get; set; }
        public static string[] CompareColumns { get; set; }
        public static string Temp { get; set; }
        public static Excel.Range range { get; set; }
        public static int VisType { get; internal set; }

        public static FMain frm;        
        //System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
        /* System.Globalization.CultureInfo oldCI = System.Threading.Thread.CurrentThread.CurrentCulture;
         System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");*/
        //private static Excel excel;
        //Получаем рабочую книгу 
        //private static Document doc;       

        private static string lPath = "//test//";
        
        public static void Perform(int action, string[] param)
        {            
            System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("ru-RU");
            Thread.CurrentThread.CurrentUICulture =  Thread.CurrentThread.CurrentCulture;
            switch (action)
            {
                //Формат А3
                case(0):
                    excelWorksheet.PageSetup.PaperSize = Excel.XlPaperSize.xlPaperA3;                    
                    break;

                //Формат А4
                case (1):
                    excelWorksheet.PageSetup.PaperSize = Excel.XlPaperSize.xlPaperA4;
                    break;

                //Формат А5
                case (2):
                    excelWorksheet.PageSetup.PaperSize = Excel.XlPaperSize.xlPaperA5;
                    break;

                //Формат письмо 
                case (3):
                    excelWorksheet.PageSetup.PaperSize = Excel.XlPaperSize.xlPaperLetter;
                    break;

                //Ориентация книга
                case (4):
                    excelWorksheet.PageSetup.Orientation = Excel.XlPageOrientation.xlPortrait;
                    break;

                //Ориентация альбом
                case (5):
                    excelWorksheet.PageSetup.Orientation = Excel.XlPageOrientation.xlLandscape;
                    break;

                //Центрировать на странице горизонтально
                case (6):
                    excelWorksheet.PageSetup.CenterHorizontally = true;
                    break;

                //Центрировать на странице вертикально
                case (7):
                    excelWorksheet.PageSetup.CenterVertically = true;
                    break;

                //Работа EXCEL
                case (8):
                    bool flag = ExcelAction.VisType == 1;
                    if (flag)
                    {
                        ExcelAction.excelApp.Visible = true;
                    }
                    else
                    {
                        bool visible = ExcelAction.excelApp.Visible;
                        if (visible)
                        {
                            ExcelAction.excelApp.Visible = false;
                        }
                    }
                    break;

                //Закрыть EXCEL
                case (9):
                    excelApp.Visible = false;
                    break;

                //Поля листа
                case (10):
                    excelWorksheet.PageSetup.LeftMargin = excelApp.InchesToPoints(0.393700787401575 * Convert.ToDouble(param[0]));
                    excelWorksheet.PageSetup.RightMargin = excelApp.InchesToPoints(0.393700787401575 * Convert.ToDouble(param[1]));
                    excelWorksheet.PageSetup.TopMargin = excelApp.InchesToPoints(0.393700787401575 * Convert.ToDouble(param[2]));
                    excelWorksheet.PageSetup.BottomMargin = excelApp.InchesToPoints(0.393700787401575 * Convert.ToDouble(param[3]));
                    break;

                //Заливка ячейки
                case (11):
                    //excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Interior.ColorIndex = 15;
                    excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Interior.ColorIndex = 15;
                    break;

                //Вывод фото в ячейку
                case (12):
                    using (FbConnection SelectConnection = new FbConnection(param[2]))
                    {
                        SelectConnection.Open();
                        FbCommand command = new FbCommand("SELECT foto3 FROM foto WHERE foto1 = 0", SelectConnection);
                        FbDataReader r = command.ExecuteReader();
                        r.Read();
                        byte[] blob = (byte[])r["foto3"];
                        if (File.Exists("image.jpg"))
                        {
                            File.Delete("image.jpg");
                        }
                        FileStream fs = new FileStream("image.jpg", FileMode.Create); 
                        fs.Write(blob,0,blob.Length);
                        fs.Close();
                        Excel.Range oRange = excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]);
                        float Left = (float)((double)oRange.Left);
                        float Top = (float)((double)oRange.Top);
                        string path = Path.GetFullPath("image.jpg");
                        excelWorksheet.Shapes.AddPicture(path, Microsoft.Office.Core.MsoTriState.msoFalse,
                            Microsoft.Office.Core.MsoTriState.msoCTrue, Left, Top, 121, 111);
                    }
                    break;

                //Вывод герба в ячейку
                case (13):
                    //excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],
                    //   excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Interior.ColorIndex = 15;
                    //FileStream stream = new FileStream("image.jpg", FileMode.Create);
                    //stream.Write(stream, 0, stream.Length);
                    //stream.Close();
                  
                   /* Excel.Range oRange = excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], (Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]);
                    float Left = (float)((double)oRange.Left);
                    float Top = (float)((double)oRange.Top);
                    string path = Path.GetFullPath("image.jpg");
                    excelWorksheet.Shapes.AddPicture(path, Microsoft.Office.Core.MsoTriState.msoFalse,
                            Microsoft.Office.Core.MsoTriState.msoCTrue, Left, Top, 121, 111);*/
                    break;

                //Вывод штрих-кода в ячейку
                case (14):
                    using (FbConnection SelectConnection = new FbConnection(param[2]))
                    {
                        SelectConnection.Open();
                        FbCommand command = new FbCommand("SELECT foto4 FROM foto WHERE foto1 = 0", SelectConnection);
                        FbDataReader r = command.ExecuteReader();
                        r.Read();
                        byte[] blob = (byte[])r["foto4"];
                        if (File.Exists("image.jpg"))
                        {
                            File.Delete("image.jpg");
                        }
                        FileStream fs = new FileStream("image.jpg", FileMode.Create);
                        fs.Write(blob, 0, blob.Length);
                        fs.Close();
                        Excel.Range oRange = excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], (Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]);
                        float Left = (float)((double)oRange.Left);
                        float Top = (float)((double)oRange.Top);
                        string path = Path.GetFullPath("image.jpg");
                        excelWorksheet.Shapes.AddPicture(path, Microsoft.Office.Core.MsoTriState.msoFalse,
                            Microsoft.Office.Core.MsoTriState.msoCTrue, Left, Top, 121, 111);
                    }
                    break;
                    //Excel.Range oRange = excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],
                    //   (Convert.ToInt32(param[2]), Convert.ToInt32(param[3])] as Excel.Range).Interior.ColorIndex = 15;
                    break;

                //Текст ячейки
                case (15):
                    //(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])] as Excel.Range).Text = param[2];
                    for (int i = 0; i < param.Length; i++)
                        param[i] = param[i].Replace("$3", "");
                    (excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])] as Excel.Range).NumberFormat = "@";
                        excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])] = param[2].ToString();
                    //(excelWorksheet.Cells[Convert.ToInt32(param[0]), 1] as Excel.Range)
                    break;

                //НазваниеОбъединение ячеек (стр; 10; стр; 24) листа
                case (16):
                    Excel.Worksheet excelWorksheet1 = (Excel.Worksheet)excelSheets.get_Item(Convert.ToInt32(param[0]));
                    excelWorksheet1.Name = param[1];
                    excelWorksheet1.PageSetup.HeaderMargin = excelApp.InchesToPoints(0);
                    excelWorksheet1.PageSetup.FooterMargin = excelApp.InchesToPoints(0);
                    excelWorksheet1.PageSetup.Zoom = false;
                    excelWorksheet1.PageSetup.FitToPagesWide = 1;
                    excelWorksheet1.PageSetup.FitToPagesTall = 100;
                    try
                    {
                        excelWorksheet1.PageSetup.PrintQuality = 600;
                    }
                    catch { }
                    break;

                //Текущий лист
                case (17):
                    excelWorksheet = (Excel.Worksheet)excelSheets.get_Item(Convert.ToInt32(param[0]));
                    break;

                //Создать книгу EXCEL или Создать альбом EXCEL 
                case (18):                
                    
                    //excel = new Excel();
                    excelApp = new Excel.Application();
                    //doc = new Document(excel.Document);
                    
                    excelApp.StandardFont = "Times New Roman";
                    excelApp.StandardFontSize = 12;
                    excelApp.DisplayAlerts = false;
                    excelApp.SheetsInNewWorkbook = 1;
                    excelWorkbook = excelApp.Workbooks.Add();
                    excelApp.Visible = false;
                    excelSheets = excelWorkbook.Worksheets;
                    excelWorksheet = (Excel.Worksheet)excelSheets.get_Item(1);
                    excelWorksheet.Name = param[0];
                    
                    excelWorksheet.PageSetup.LeftMargin = excelApp.InchesToPoints(0.393700787401575 * Convert.ToDouble(param[1]));
                    excelWorksheet.PageSetup.RightMargin = excelApp.InchesToPoints(0.393700787401575 * Convert.ToDouble(param[2]));
                    excelWorksheet.PageSetup.TopMargin = excelApp.InchesToPoints(0.393700787401575 * Convert.ToDouble(param[3]));
                    excelWorksheet.PageSetup.BottomMargin = excelApp.InchesToPoints(0.393700787401575 * Convert.ToDouble(param[4]));
                    excelWorksheet.PageSetup.HeaderMargin = excelApp.InchesToPoints(0);
                    excelWorksheet.PageSetup.FooterMargin = excelApp.InchesToPoints(0);
                    excelWorksheet.PageSetup.Zoom = false;
                    excelWorksheet.PageSetup.FitToPagesWide = 1;
                    excelWorksheet.PageSetup.FitToPagesTall = 100;
                    try
                    {
                        excelWorksheet.PageSetup.PrintQuality = 600;                        
                    }
                    catch { }
                    if (param[4] == "0")
                    {
                        excelWorksheet.PageSetup.Orientation = Excel.XlPageOrientation.xlLandscape;
                    }
                    else if (param[4] == "1")
                    {
                        excelWorksheet.PageSetup.Orientation = Excel.XlPageOrientation.xlPortrait;
                    }

                    break;
                case (19):

                    //excel = new Excel();
                    excelApp = new Excel.Application();
                    //doc = new Document(excel.Document);

                    excelApp.StandardFont = "Times New Roman";
                    excelApp.StandardFontSize = 12;
                    excelApp.DisplayAlerts = false;
                    excelApp.SheetsInNewWorkbook = 1;
                    excelWorkbook = excelApp.Workbooks.Add();
                    excelApp.Visible = false;
                    excelSheets = excelWorkbook.Worksheets;
                    excelWorksheet = (Excel.Worksheet)excelSheets.get_Item(1);
                    excelWorksheet.Name = param[0];

                    excelWorksheet.PageSetup.LeftMargin = excelApp.InchesToPoints(0.393700787401575 * Convert.ToDouble(param[1]));
                    excelWorksheet.PageSetup.RightMargin = excelApp.InchesToPoints(0.393700787401575 * Convert.ToDouble(param[2]));
                    excelWorksheet.PageSetup.TopMargin = excelApp.InchesToPoints(0.393700787401575 * Convert.ToDouble(param[3]));
                    excelWorksheet.PageSetup.BottomMargin = excelApp.InchesToPoints(0.393700787401575 * Convert.ToDouble(param[4]));
                    excelWorksheet.PageSetup.HeaderMargin = excelApp.InchesToPoints(0);
                    excelWorksheet.PageSetup.FooterMargin = excelApp.InchesToPoints(0);
                    excelWorksheet.PageSetup.Zoom = false;
                    excelWorksheet.PageSetup.FitToPagesWide = 1;
                    excelWorksheet.PageSetup.FitToPagesTall = 100;
                    try
                    {
                        excelWorksheet.PageSetup.PrintQuality = 600;
                    }
                    catch { }
                    
                        excelWorksheet.PageSetup.Orientation = Excel.XlPageOrientation.xlLandscape;  

                    break;
                //Создать альбом EXCEL ( !!!!!!! )
                //case (19):
                //    excelWorksheet = (Excel.Worksheet)excelSheets.get_Item(0);
                //    excelApp = new Excel.Application();
                //    excelApp.DisplayAlerts = false;
                //    excelApp.Workbooks.Add();
                //    excelApp.Visible = false;
                //    break;

                //Сохранить EXCEL
                case (20):

                    foreach (char c in System.IO.Path.GetInvalidFileNameChars())
                    {
                        if (c == '\\')
                            continue;
                        param[0] = param[0].Replace(c, '_');
                        param[1] = param[1].Replace(c, '_');
                    }
                    if (string.IsNullOrWhiteSpace(Temp))
                    {
                        foreach (var driver in DriveInfo.GetDrives().Where(i=>i.DriveType == DriveType.Fixed))
                        {
                            Temp = driver.RootDirectory.Name + "Temp";                        
                            if (GetPermissionToExport(driver.RootDirectory.Name))
                            {
                                if (!Directory.Exists(Temp))
                                {                                    
                                    Directory.CreateDirectory(Temp);
                                }                               
                                if(GetPermissionToExport(Temp))
                                    break;
                            }  
                        }
                    }
                    if (GetPermissionToExport(Temp))
                    {
                        if (!Directory.Exists(Temp + "\\" + param[0]))
                        {
                            //if (Temp.Contains(@":\"))
                            //{
                            //    if (!NotDenied(new DirectoryInfo(Temp.Remove(Temp.IndexOf(@":\") + 2)).GetAccessControl()))
                            //        return;
                            //}
                            if (!Directory.Exists(Temp))
                            {

                                Directory.CreateDirectory(Temp);
                            }

                            Directory.CreateDirectory(Temp + "\\" + param[0]);
                        }
                        param[0] = Temp + "\\" + param[0] + "\\" + param[1];
                        excelWorkbook.SaveAs(param[0], Type.Missing,
                            Type.Missing, Type.Missing, Type.Missing, Type.Missing, Excel.XlSaveAsAccessMode.xlNoChange,
                            Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);

                        bool flag6 = ExcelAction.VisType == 1;

                        if (flag6)
                        {
                            ExcelAction.excelApp.Visible = true;
                        }
                        else
                        {
                            bool visible2 = ExcelAction.excelApp.Visible;
                            if (visible2)
                            {
                                ExcelAction.excelApp.Visible = false;
                            }
                        }
                    }
                    else
                    {
                        if (!ExcelAction.excelApp.Visible)
                            ExcelAction.excelApp.Visible = true;
                    }
                   
                    break;

                //ОТКРЫТЬ EXCEL
                case (21):
                    if (excelApp == null)
                    {
                        excelApp = new Excel.Application();
                        excelApp.DisplayAlerts = false;
                        excelWorkbook = excelApp.Workbooks.Open(param[0] + "\\" + param[1],
                                            1, false, 5, "", "", false, Excel.XlPlatform.xlWindows, "",
                                            true, false, 1, true, false, false);
                       excelSheets = excelWorkbook.Worksheets;
                       excelWorksheet = (Excel.Worksheet)excelSheets.get_Item(1);
                    }
                    break;

                //Разрыв страницы
                case (22):
                    excelWorksheet.PageSetup.Zoom = 100;   //Convert.ToInt32(param[0])    
                    excelWorksheet.HPageBreaks.Add((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), 1]);
                    break;

                //Открыть Word ( !!!!!!!!!!! )
                //case (23):
                    //excelWorksheet.PageSetup.Zoom = 100;
                    //break;

                //Заменить в Word-е ( !!!!!!!!!!! )
                //case (24):
                    //excelWorksheet.PageSetup.Zoom = 100;
                    //break;

                //Сохранить Word ( !!!!!!!!!!! )
                //case (24):
                //excelWorksheet.PageSetup.Zoom = 100;
                //break;

                //Заголовок страниц
                case (25):
                    excelWorksheet.PageSetup.PrintTitleRows = '$' + param[0] + ":$" + param[1]; 
                    break;

                //Добавить строку EXCEL ( !!!!!!!!!! )
                case (26):
                    excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], (Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).EntireRow.Insert();
                    break;

                //Верхний колонтитул слева
                case (27):
                    excelWorksheet.PageSetup.LeftHeader = param[0];
                    break;

                //Верхний колонтитул по центру
                case (28):
                    excelWorksheet.PageSetup.CenterHeader = param[0];
                    break;

                //Верхний колонтитул справа
                case (29):
                    excelWorksheet.PageSetup.RightHeader = param[0];
                    break;

                //Нижний колонтитул слева
                case (30):
                    excelWorksheet.PageSetup.LeftFooter = param[0];
                    break;

                //Нижний колонтитул по центру
                case (31):
                    excelWorksheet.PageSetup.CenterFooter = param[0];
                    break;

                //Нижний колонтитул справа
                case (32):
                    excelWorksheet.PageSetup.RightFooter = param[0];
                    break;

                //Объединение ячеек
                case (33):
                    //Excel.Range ran = excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]);
                    // ran.Merge();
                    //(Excel.Range)
                 
                    excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], (Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Merge();
                   // excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],
                    //                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Merge();
                    break;                    
                //Ширина колонки
                case (34):
                    param[1] = (param[1] == "0") ? "6" : param[1];
                    (excelWorksheet.Cells[1, Convert.ToInt32(param[0])] as Excel.Range).EntireColumn.ColumnWidth = (Convert.ToDouble(param[1])); //Convert.ToInt32(param[1]);
                    break;

                //Ширина колонок
                case (35):
                    excelWorksheet.Range[GetExcelColumnName(Convert.ToInt32(param[0])) +
                         "1:" + GetExcelColumnName(Convert.ToInt32(param[1])) + '1'].ColumnWidth =(Convert.ToDouble(param[2])); 
                    break;

                //Высота строки 
                case (36):
                    (excelWorksheet.Cells[Convert.ToInt32(param[0]), 1] as Excel.Range).EntireRow.RowHeight = (Convert.ToDouble(param[1]));
                    break;

                //Высота строк 
                case (37):
                     excelWorksheet.Range['A'+param[0]+":A"+param[1]].RowHeight = (Convert.ToDouble(param[2]));
                    break;

                //Перенос текста строки
                case (38):
                    (excelWorksheet.Cells[Convert.ToInt32(param[0]), 1] as Excel.Range).EntireRow.WrapText = true;
                    break;

                //Перенос текста колонки
                case (39):
                    (excelWorksheet.Cells[1, Convert.ToInt32(param[0])] as Excel.Range).EntireColumn.WrapText = true;
                    break;

                //Перенос текста ячейки
                case (40):
                    excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]),
                        Convert.ToInt32(param[1])], (Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).WrapText = true;
                    break;

                //Отмена переноса текста строки
                case (41):
                    (excelWorksheet.Cells[Convert.ToInt32(param[0]), 1] as Excel.Range).EntireRow.WrapText = false;
                    break;

                //Отмена переноса текста колонки
                case (42):
                    (excelWorksheet.Cells[1, Convert.ToInt32(param[0])] as Excel.Range).EntireColumn.WrapText = false;
                    break;

                //Отмена переноса текста ячейки
                case (43):
                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).WrapText = false;
                    break;

                //Поворот текста строки
                case (44):
                    (excelWorksheet.Cells[Convert.ToInt32(param[0]), 1] as Excel.Range).EntireRow.Orientation = Convert.ToInt32(param[1]);
                    break;

                //Поворот текста колонки
                case (45):
                    (excelWorksheet.Cells[1, Convert.ToInt32(param[0])] as Excel.Range).EntireColumn.Orientation = Convert.ToInt32(param[1]);
                    break;

                //Поворот текста ячейки
                case (46):
                    //excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],
                    //    excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Orientation = Convert.ToInt32(param[4]);
                    excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], (Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Orientation = Convert.ToInt32(param[4]);
                    break;

                //Размер шрифта строки
                case (47):
                    (excelWorksheet.Cells[Convert.ToInt32(param[0]), 1] as Excel.Range).EntireRow.Font.Size = (Convert.ToDouble(param[1]));
                    break;

                //Размер шрифта колонки
                case (48):
                    (excelWorksheet.Cells[1, Convert.ToInt32(param[0])] as Excel.Range).EntireColumn.Font.Size = (Convert.ToDouble(param[1]));
                    break;

                //Размер шрифта ячейки
                case (49):
                        //excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Font.Size = Convert.ToInt32(param[2]);
                    excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], (Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Font.Size = (Convert.ToDouble(param[4]));
                    break; 
                //Название шрифта строки
                case (50):
                    (excelWorksheet.Cells[Convert.ToInt32(param[0]), 1] as Excel.Range).EntireRow.Font.Name = param[1];
                    break;

                //Название шрифта колонки
                case (51):
                    (excelWorksheet.Cells[1, Convert.ToInt32(param[0])] as Excel.Range).EntireColumn.Font.Name = param[1];
                    break;

                //Название шрифта ячейки
                case (52):
                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Font.Name = param[4];
                    break;

                //Выравнивание по вертикали строки по верхнему краю 
                case (53):
                    (excelWorksheet.Cells[Convert.ToInt32(param[0]), 1] as Excel.Range).EntireRow.VerticalAlignment = Excel.XlVAlign.xlVAlignTop;
                    break;

                //Выравнивание по вертикали строки по центру
                case (54):
                    (excelWorksheet.Cells[Convert.ToInt32(param[0]), 1] as Excel.Range).EntireRow.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
                    break;

                //Выравнивание по вертикали строки по нижнему краю 
                case (55):
                    (excelWorksheet.Cells[Convert.ToInt32(param[0]), 1] as Excel.Range).EntireRow.VerticalAlignment = Excel.XlVAlign.xlVAlignBottom;
                    break;

                //Выравнивание по вертикали строки по высоте
                case (56):
                    (excelWorksheet.Cells[Convert.ToInt32(param[0]), 1] as Excel.Range).EntireRow.VerticalAlignment = Excel.XlVAlign.xlVAlignDistributed;
                    break;

                //Выравнивание по вертикали колонки по верхнему краю 
                case (57):
                    (excelWorksheet.Cells[1, Convert.ToInt32(param[0])] as Excel.Range).EntireColumn.VerticalAlignment = Excel.XlVAlign.xlVAlignTop;
                    break;

                //Выравнивание по вертикали колонки по центру
                case (58):
                    (excelWorksheet.Cells[1, Convert.ToInt32(param[0])] as Excel.Range).EntireColumn.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
                    break;

                //Выравнивание по вертикали колонки по нижнему краю 
                case (59):
                    (excelWorksheet.Cells[1, Convert.ToInt32(param[0])] as Excel.Range).EntireColumn.VerticalAlignment = Excel.XlVAlign.xlVAlignBottom;
                    break;

                //Выравнивание по вертикали колонки по высоте
                case (60):
                    (excelWorksheet.Cells[1, Convert.ToInt32(param[0])] as Excel.Range).EntireColumn.VerticalAlignment = Excel.XlVAlign.xlVAlignDistributed;
                    break;

                //Выравнивание по вертикали ячейки по верхнему краю 
                case (61):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], (Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).VerticalAlignment = Excel.XlVAlign.xlVAlignTop;
                    break;

                //Выравнивание по вертикали ячейки по центру
                case (62):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], (Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
                    break;

                //Выравнивание по вертикали ячейки по нижнему краю 
                case (63):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], (Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).VerticalAlignment = Excel.XlVAlign.xlVAlignBottom;
                    break;

                //Выравнивание по вертикали ячейки по высоте
                case (64):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], (Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).VerticalAlignment = Excel.XlVAlign.xlVAlignDistributed;
                    break;

                //Выравнивание по горизонтали строки по значению 
                case (65):
                    (excelWorksheet.Cells[Convert.ToInt32(param[0]), 1] as Excel.Range).EntireRow.HorizontalAlignment = Excel.XlHAlign.xlHAlignFill;
                    break;

                //Выравнивание по горизонтали строки по левому краю
                case (66):
                    (excelWorksheet.Cells[Convert.ToInt32(param[0]), 1] as Excel.Range).EntireRow.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
                    break;

                //Выравнивание по горизонтали строки по центру 
                case (67):
                    (excelWorksheet.Cells[Convert.ToInt32(param[0]), 1] as Excel.Range).EntireRow.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                    break;

                //Выравнивание по горизонтали строки по правому краю
                case (68):
                    (excelWorksheet.Cells[Convert.ToInt32(param[0]), 1] as Excel.Range).EntireRow.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
                    break;

                //Выравнивание по горизонтали колонки по значению 
                case (69):
                    (excelWorksheet.Cells[1, Convert.ToInt32(param[0])] as Excel.Range).EntireColumn.HorizontalAlignment = Excel.XlHAlign.xlHAlignFill;
                    break;

                //Выравнивание по горизонтали колонки по левому краю
                case (70):
                    (excelWorksheet.Cells[1, Convert.ToInt32(param[0])] as Excel.Range).EntireColumn.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
                    break;

                //Выравнивание по горизонтали колонки по центру 
                case (71):
                    (excelWorksheet.Cells[1, Convert.ToInt32(param[0])] as Excel.Range).EntireColumn.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                    break;

                //Выравнивание по горизонтали колонки по правому краю
                case (72):
                    (excelWorksheet.Cells[1, Convert.ToInt32(param[0])] as Excel.Range).EntireColumn.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
                    break;

                //Выравнивание по горизонтали ячейки по значению 
                case (73):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], (Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).HorizontalAlignment = Excel.XlHAlign.xlHAlignFill;
                    break;

                //Выравнивание по горизонтали ячейки по левому краю
                case (74):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], (Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
                    break;

                //Выравнивание по горизонтали ячейки по центру 
                case (75):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], (Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                    break;

                //Выравнивание по горизонтали ячейки по правому краю
                case (76):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], (Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
                    break;

                //Жирный шрифт строки
                case (77):
                    (excelWorksheet.Cells[Convert.ToInt32(param[0]), 1] as Excel.Range).EntireRow.Font.Bold = true;
                    break;

                //Жирный шрифт строки отмена
                case (78):
                    (excelWorksheet.Cells[Convert.ToInt32(param[0]), 1] as Excel.Range).EntireRow.Font.Bold = false;
                    break;

                //Жирный шрифт колонки
                case (79):
                    (excelWorksheet.Cells[1, Convert.ToInt32(param[0])] as Excel.Range).EntireColumn.Font.Bold = true;
                    break;

                //Жирный шрифт колонки отмена
                case (80):
                    (excelWorksheet.Cells[1, Convert.ToInt32(param[0])] as Excel.Range).EntireColumn.Font.Bold = false;
                    break;

                //Жирный шрифт ячейки
                case (81):
                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], (Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Font.Bold = true;
                    break;

                //Жирный шрифт ячейки отмена 
                case (82):
                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], (Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Font.Bold = false;
                    break;

                //Наклонный шрифт строки
                case (83):
                    (excelWorksheet.Cells[Convert.ToInt32(param[0]), 1] as Excel.Range).EntireRow.Font.Italic = true;
                    break;

                //Наклонный шрифт строки отмена
                case (84):
                    (excelWorksheet.Cells[Convert.ToInt32(param[0]), 1] as Excel.Range).EntireRow.Font.Italic = false;
                    break;

                //Наклонный шрифт колонки
                case (85):
                    (excelWorksheet.Cells[1, Convert.ToInt32(param[0])] as Excel.Range).EntireColumn.Font.Italic = true;
                    break;

                //Наклонный шрифт колонки отмена
                case (86):
                    (excelWorksheet.Cells[1, Convert.ToInt32(param[0])] as Excel.Range).EntireColumn.Font.Italic = false;
                    break;

                //Наклонный шрифт ячейки
                case (87):
                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], (Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Font.Italic = true;
                    break;

                //Наклонный шрифт ячейки отмена 
                case (88):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], (Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Font.Italic = false;
                    break;

                //Подчеркивание шрифта строки тонкой линией 
                case (89):
                    (excelWorksheet.Cells[Convert.ToInt32(param[0]), 1] as Excel.Range).EntireRow.Font.Underline = Excel.XlUnderlineStyle.xlUnderlineStyleSingle;
                    break;

                //Подчеркивание шрифта строки толстой линией 
                case (90):
                    (excelWorksheet.Cells[Convert.ToInt32(param[0]), 1] as Excel.Range).EntireRow.Font.Underline = Excel.XlUnderlineStyle.xlUnderlineStyleDouble;
                    break;

                //Подчеркивание шрифта строки отмена 
                case (91):
                    (excelWorksheet.Cells[Convert.ToInt32(param[0]), 1] as Excel.Range).EntireRow.Font.Underline = Excel.XlUnderlineStyle.xlUnderlineStyleNone;
                    break;

                //Подчеркивание шрифта колонки тонкой линией 
                case (92):
                    (excelWorksheet.Cells[1, Convert.ToInt32(param[0])] as Excel.Range).EntireColumn.Font.Underline = Excel.XlUnderlineStyle.xlUnderlineStyleSingle;
                    break;

                //Подчеркивание шрифта колонки толстой линией 
                case (93):
                    (excelWorksheet.Cells[1, Convert.ToInt32(param[0])] as Excel.Range).EntireColumn.Font.Underline = Excel.XlUnderlineStyle.xlUnderlineStyleDouble;
                    break;

                //Подчеркивание шрифта колонки отмена 
                case (94):
                    (excelWorksheet.Cells[1, Convert.ToInt32(param[0])] as Excel.Range).EntireColumn.Font.Underline = Excel.XlUnderlineStyle.xlUnderlineStyleNone;
                    break;

                //Подчеркивание шрифта ячейки тонкой линией 
                case (95):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], (Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Font.Underline = Excel.XlUnderlineStyle.xlUnderlineStyleSingle;
                    break;

                //Подчеркивание шрифта ячейки толстой линией 
                case (96):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], (Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Font.Underline = Excel.XlUnderlineStyle.xlUnderlineStyleDouble;
                    break;

                //Подчеркивание шрифта ячейки отмена 
                case (97):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], (Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Font.Underline = Excel.XlUnderlineStyle.xlUnderlineStyleNone;
                    break;

                //Линия всей ячейки нормальная 
                case (98):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], (Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], (Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders.Weight = Excel.XlBorderWeight.xlMedium;
                    break;

                //Линия всей ячейки тонкая 
                case (99):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], (Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders.Weight = Excel.XlBorderWeight.xlThin;
                    break;

                //Линия всей ячейки толстая 
                case (100):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], (Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders.Weight = Excel.XlBorderWeight.xlThick;
                    break;

                //Линия всей ячейки отмена 
                case (101):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], (Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders.LineStyle = Excel.XlLineStyle.xlLineStyleNone;
                    break;

                //Линия всей ячейки двойная 
                case (102):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], (Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders.LineStyle = Excel.XlLineStyle.xlDouble;
                    break;

                //Линия всей ячейки пунктирная 
                case (103):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], (Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders.LineStyle = Excel.XlLineStyle.xlDashDot;
                    break;

                //Линия всей ячейки точками 
                case (104):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], (Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders.LineStyle = Excel.XlLineStyle.xlDot;
                    break;

                //Линия вокруг ячейки нормальная 
                case (105):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], (Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle 
                        = Excel.XlLineStyle.xlContinuous;

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], (Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle
                        = Excel.XlLineStyle.xlContinuous;

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], (Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle
                        = Excel.XlLineStyle.xlContinuous;

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], (Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle
                        = Excel.XlLineStyle.xlContinuous;


                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], (Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeLeft].Weight
                        = Excel.XlBorderWeight.xlMedium;

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], (Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeRight].Weight
                        = Excel.XlBorderWeight.xlMedium;

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], (Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeTop].Weight
                        = Excel.XlBorderWeight.xlMedium;

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], (Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeBottom].Weight
                        = Excel.XlBorderWeight.xlMedium;

                    break;

                //Линия вокруг ячейки тонкая 
                case (106):
 
                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], (Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeLeft].Weight
                        = Excel.XlBorderWeight.xlThin;

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], (Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeRight].Weight
                        = Excel.XlBorderWeight.xlThin;

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], (Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeTop].Weight
                        = Excel.XlBorderWeight.xlThin;

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], (Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeBottom].Weight
                        = Excel.XlBorderWeight.xlThin;
                    break;

                //Линия вокруг ячейки толстая 
                case (107):
 
                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], (Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeLeft].Weight
                        = Excel.XlBorderWeight.xlThick;

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], (Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeRight].Weight
                        = Excel.XlBorderWeight.xlThick;

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], (Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeTop].Weight
                        = Excel.XlBorderWeight.xlThick;

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], (Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeBottom].Weight
                        = Excel.XlBorderWeight.xlThick;
                    break;

                //Линия вокруг ячейки отмена 
                case (108):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], (Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle
                        = Excel.XlLineStyle.xlLineStyleNone;

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], (Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle
                        = Excel.XlLineStyle.xlLineStyleNone;

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], (Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle
                        = Excel.XlLineStyle.xlLineStyleNone;

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], (Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle
                        = Excel.XlLineStyle.xlLineStyleNone;
                    break;

                //Линия вокруг ячейки двойная 
                case (109):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], (Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle
                        = Excel.XlLineStyle.xlDouble;

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], (Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle
                        = Excel.XlLineStyle.xlDouble;

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], (Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle
                        = Excel.XlLineStyle.xlDouble;

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], (Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle
                        = Excel.XlLineStyle.xlDouble;
                    break;

                //Линия вокруг ячейки пунктирная 
                case (110):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], (Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle
                        = Excel.XlLineStyle.xlDashDot;

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], (Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle
                        = Excel.XlLineStyle.xlDashDot;

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], (Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle
                        = Excel.XlLineStyle.xlDashDot;

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], (Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle
                        = Excel.XlLineStyle.xlDashDot;
                    break;

                //Линия вокруг ячейки точками 
                case (111):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle
                        = Excel.XlLineStyle.xlDot;

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle
                        = Excel.XlLineStyle.xlDot;

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle
                        = Excel.XlLineStyle.xlDot;

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle
                        = Excel.XlLineStyle.xlDot;
                    break;

                //Линия сверху ячейки нормальная 
                case (112):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle
                        = Excel.XlLineStyle.xlContinuous;


                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeTop].Weight
                        = Excel.XlBorderWeight.xlMedium;
                    break;

                //Линия сверху ячейки тонкая 
                case (113):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeTop].Weight
                        = Excel.XlBorderWeight.xlThin;
                    break;

                //Линия сверху ячейки толстая 
                case (114):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeTop].Weight
                        = Excel.XlBorderWeight.xlThick;
                    break;

                //Линия сверху ячейки отмена 
                case (115):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle
                        = Excel.XlLineStyle.xlLineStyleNone;
                    break;

                //Линия сверху ячейки двойная 
                case (116):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle
                        = Excel.XlLineStyle.xlDouble;
                    break;

                //Линия сверху ячейки пунктирная 
                case (117):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle
                        = Excel.XlLineStyle.xlDashDot;
                    break;

                //Линия сверху ячейки точками 
                case (118):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle
                        = Excel.XlLineStyle.xlDot;
                    break;

                //Линия слева ячейки нормальная 
                case (119):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle
                        = Excel.XlLineStyle.xlContinuous;


                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeLeft].Weight
                        = Excel.XlBorderWeight.xlMedium;
                    break;

                //Линия слева ячейки тонкая 
                case (120):

                       excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeLeft].Weight
                       = Excel.XlBorderWeight.xlThin;
                    break;

                //Линия слева ячейки толстая 
                case (121):

                       excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeLeft].Weight
                       = Excel.XlBorderWeight.xlThick;
                    break;

                //Линия слева ячейки отмена 
                case (122):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle
                        = Excel.XlLineStyle.xlLineStyleNone;
                    break;

                //Линия слева ячейки двойная 
                case (123):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle
                        = Excel.XlLineStyle.xlDouble;
                    break;

                //Линия слева ячейки пунктирная 
                case (124):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle
                        = Excel.XlLineStyle.xlDashDot;
                    break;

                //Линия слева ячейки точками 
                case (125):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle
                        = Excel.XlLineStyle.xlDot;
                    break;

                //Линия снизу ячейки нормальная 
                case (126):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle
                        = Excel.XlLineStyle.xlContinuous;


                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeBottom].Weight
                        = Excel.XlBorderWeight.xlMedium;
                    break;

                //Линия снизу ячейки тонкая 
                case (127):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeBottom].Weight
                        = Excel.XlBorderWeight.xlThin;
                    break;

                //Линия снизу ячейки толстая 
                case (128):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeBottom].Weight
                        = Excel.XlBorderWeight.xlThick;
                    break;

                //Линия снизу ячейки отмена 
                case (129):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle
                        = Excel.XlLineStyle.xlLineStyleNone;
                    break;

                //Линия снизу ячейки двойная 
                case (130):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle
                        = Excel.XlLineStyle.xlDouble;
                    break;

                //Линия снизу ячейки пунктирная 
                case (131):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle
                        = Excel.XlLineStyle.xlDashDot;
                    break;

                //Линия снизу ячейки точками 
                case (132):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle
                        = Excel.XlLineStyle.xlDot;
                    break;

                //Линия справа ячейки нормальная 
                case (133):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle
                        = Excel.XlLineStyle.xlContinuous;


                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeRight].Weight
                        = Excel.XlBorderWeight.xlMedium;
                    break;

                //Линия справа ячейки тонкая 
                case (134):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeRight].Weight
                        = Excel.XlBorderWeight.xlThin;
                    break;

                //Линия справа ячейки толстая 
                case (135):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeRight].Weight
                        = Excel.XlBorderWeight.xlThick;
                    break;
                //Линия справа ячейки отмена 
                case (136):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle
                        = Excel.XlLineStyle.xlLineStyleNone;
                    break;

                //Линия справа ячейки двойная 
                case (137):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle
                        = Excel.XlLineStyle.xlDouble;
                    break;

                //Линия справа ячейки пунктирная 
                case (138):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle
                        = Excel.XlLineStyle.xlDashDot;
                    break;

                //Линия вокруг ячейки точками 
                case (139):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle
                        = Excel.XlLineStyle.xlDot;
                    break;

                //Толщина линии всей ячейки 1 
                case (140):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders.Weight = Excel.XlBorderWeight.xlHairline;
                    break;

                //Толщина линии всей ячейки 2 
                case (141):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders.Weight = Excel.XlBorderWeight.xlThin;
                    break;

                //Толщина линии всей ячейки 3 
                case (142):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders.Weight = Excel.XlBorderWeight.xlMedium;
                    break;

                //Толщина линии всей ячейки 4
                case (143):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders.Weight = Excel.XlBorderWeight.xlThick;
                    break;

                //Толщина линии вокруг ячейки 1
                case (144):

                       excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeLeft].Weight
                       = Excel.XlBorderWeight.xlHairline;

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeRight].Weight
                        = Excel.XlBorderWeight.xlHairline;

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeTop].Weight
                        = Excel.XlBorderWeight.xlHairline;

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeBottom].Weight
                        = Excel.XlBorderWeight.xlHairline;
                    break;


                //Толщина линии вокруг ячейки 2
                case (145):

                       excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeLeft].Weight
                       = Excel.XlBorderWeight.xlThin;

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeRight].Weight
                        = Excel.XlBorderWeight.xlThin;

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeTop].Weight
                        = Excel.XlBorderWeight.xlThin;

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeBottom].Weight
                        = Excel.XlBorderWeight.xlThin;
                    break;

                //Толщина линии вокруг ячейки 3
                case (146):

                       excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeLeft].Weight
                       = Excel.XlBorderWeight.xlMedium;

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeRight].Weight
                        = Excel.XlBorderWeight.xlMedium;

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeTop].Weight
                        = Excel.XlBorderWeight.xlMedium;

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeBottom].Weight
                        = Excel.XlBorderWeight.xlMedium;
                    break;

                //Толщина линии вокруг ячейки 4
                case (147):

                       excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeLeft].Weight
                       = Excel.XlBorderWeight.xlThick;

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeRight].Weight
                        = Excel.XlBorderWeight.xlThick;

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeTop].Weight
                        = Excel.XlBorderWeight.xlThick;

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeBottom].Weight
                        = Excel.XlBorderWeight.xlThick;
                    break;

                //Толщина линии сверху ячейки 1  
                case (148):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeTop].Weight
                        = Excel.XlBorderWeight.xlHairline;
                    break;

                //Толщина линии сверху ячейки 2  
                case (150):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeTop].Weight
                        = Excel.XlBorderWeight.xlThin;
                    break;

                //Толщина линии сверху ячейки 3 
                case (151):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeTop].Weight
                        = Excel.XlBorderWeight.xlMedium;
                    break;

                //Толщина линии сверху ячейки 4  
                case (152):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeTop].Weight
                        = Excel.XlBorderWeight.xlThick;
                    break;

                //Толщина линии снизу ячейки 1  
                case (153):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeBottom].Weight
                        = Excel.XlBorderWeight.xlHairline;
                    break;

                //Толщина линии снизу ячейки 2  
                case (154):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeBottom].Weight
                        = Excel.XlBorderWeight.xlThin;
                    break;

                //Толщина линии снизу ячейки 3 
                case (155):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeBottom].Weight
                        = Excel.XlBorderWeight.xlMedium;
                    break;

                //Толщина линии снизу ячейки 4  
                case (156):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeBottom].Weight
                        = Excel.XlBorderWeight.xlThick;
                    break;

                //Толщина линии справа ячейки 1  
                case (157):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeRight].Weight
                        = Excel.XlBorderWeight.xlHairline;
                    break;

                //Толщина линии справа ячейки 2  
                case (158):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeRight].Weight
                        = Excel.XlBorderWeight.xlThin;
                    break;

                //Толщина линии справа ячейки 3 
                case (159):
                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeRight].Weight
                        = Excel.XlBorderWeight.xlMedium;
                    break;

                //Толщина линии справа ячейки 4  
                case (160):
                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeRight].Weight
                        = Excel.XlBorderWeight.xlThick;
                    break;

                //Толщина слева справа ячейки 1  
                case (161):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeLeft].Weight
                        = Excel.XlBorderWeight.xlHairline;
                    break;

                //Толщина линии справа ячейки 2  
                case (162):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeLeft].Weight
                        = Excel.XlBorderWeight.xlThin;
                    break;

                //Толщина линии справа ячейки 3 
                case (163):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeLeft].Weight
                        = Excel.XlBorderWeight.xlMedium;
                    break;

                //Толщина линии справа ячейки 4  
                case (164):

                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).Borders[Excel.XlBordersIndex.xlEdgeLeft].Weight
                        = Excel.XlBorderWeight.xlThick;
                    break;

                //Формат строки общий 
                case (165):
                    (excelWorksheet.Cells[Convert.ToInt32(param[0]), 1] as Excel.Range).EntireRow.NumberFormat = "0";
                    break;

                //Формат строки числовой 
                case (166):
                    (excelWorksheet.Cells[Convert.ToInt32(param[0]), 1] as Excel.Range).EntireRow.NumberFormat = "0,00";
                    break;

                //Формат строки текстовый 
                case (167):
                    (excelWorksheet.Cells[Convert.ToInt32(param[0]), 1] as Excel.Range).EntireRow.NumberFormat = "@";
                    break;

                //Формат строки дата 
                case (168):
                    (excelWorksheet.Cells[Convert.ToInt32(param[0]), 1] as Excel.Range).EntireRow.NumberFormat = "d, mmmm, yyyy";
                    break;

                //Формат колонки общий 
                case (169):
                    (excelWorksheet.Cells[1, Convert.ToInt32(param[0])] as Excel.Range).EntireColumn.NumberFormat = "0";
                    break;

                //Формат колонки числовой 
                case (170):
                    (excelWorksheet.Cells[1, Convert.ToInt32(param[0])] as Excel.Range).EntireColumn.NumberFormat = "0,00";
                    break;

                //Формат колонки текстовый 
                case (171):
                    (excelWorksheet.Cells[1, Convert.ToInt32(param[0])] as Excel.Range).EntireColumn.NumberFormat = "@";
                    break;

                //Формат колонки дата 
                case (172):
                    (excelWorksheet.Cells[1, Convert.ToInt32(param[0])] as Excel.Range).EntireColumn.NumberFormat = "d, mmmm, yyyy";
                    break;

                //Формат ячейки общий 
                case (173):
                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).NumberFormat = "0";
                    break;

                //Формат ячейки числвой 
                case (174):
                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).NumberFormat = "0,00";
                    break;

                //Формат ячейки текст 
                case (175):
                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).NumberFormat = "@";
                    break;

                //Формат ячейки дата 
                case (176):
                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).NumberFormat = "d, mmmm, yyyy";
                    break;

                //Подчеркивание символов в ячейке тонкой линией 
                case (177):
                    (excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])] as Excel.Range).Characters[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])].Font.Underline 
                        = Excel.XlUnderlineStyle.xlUnderlineStyleSingle;
                    break;


                //Подчеркивание символов в ячейке жирной линией 
                case (179):
                    (excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])] as Excel.Range).Characters[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])].Font.Underline
                        = Excel.XlUnderlineStyle.xlUnderlineStyleDouble;
                    break;

                //Подчеркивание символов в ячейке отмена
                case (180):
                    (excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])] as Excel.Range).Characters[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])].Font.Underline
                        = Excel.XlUnderlineStyle.xlUnderlineStyleNone;
                    break;

                //Жирность символов в ячейке
                case (181):
                    (excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])] as Excel.Range).Characters[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])].Font.Bold
                        = true;
                    break;

                //Жирность символов в ячейке отмена
                case (182):
                    (excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])] as Excel.Range).Characters[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])].Font.Bold
                        = false;
                    break;

                //Наклонность символов в ячейке
                case (183):

                        (excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])] as Excel.Range).Characters[Convert.ToInt32(param[2]), Convert.ToInt32(param[2])].Font.Italic
                        = true;
                    break;

                //Наклонность символов в ячейке отмена
                case (184):

                    (excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])] as Excel.Range).Characters[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])].Font.Italic
                        = false;
                    break;

                //Размер символов в ячейке
                case (185):
                        (excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])] as Excel.Range).Characters[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])].Font.Size
                        = Convert.ToDouble((param[4]));
                    break;

                //Автоподбор высоты
                case (186):
                    (excelWorksheet.Cells[1, Convert.ToInt32(param[0])] as Excel.Range).EntireColumn.AutoFit();
                    break;

                //Выравнивание по горизонтали строки по ширине
                case (189):
                    (excelWorksheet.Cells[Convert.ToInt32(param[0]), 1] as Excel.Range).EntireRow.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                    break;

                //Выравнивание по горизонтали колонки по ширине
                case (190):
                    (excelWorksheet.Cells[1, Convert.ToInt32(param[0])] as Excel.Range).EntireColumn.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                    break;

                //Выравнивание по горизонтали ячейки по ширине 
                case (191):
                        excelApp.get_Range((Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])],(Excel.Range)excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                    break;

                //Добавить листов
                case (192):
                    excelWorkbook.Worksheets.Add(Type.Missing, Type.Missing, Convert.ToInt32(param[0]), Type.Missing);
                    break;

                //Название листа листов
                case (193):
                    Excel.Worksheet xlSheet = (excelWorkbook.Worksheets.get_Item(Convert.ToInt32(param[0])) as Excel.Worksheet);
                        xlSheet.Name = param[1];
                    break;

                //Текущий лист
                case (194):
                    ((Excel.Worksheet)excelWorkbook.Sheets[Convert.ToInt32(param[0])]).Select(Type.Missing);
                    break;
                //Ширина ячеек
                case (195):
                    excelApp.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])], excelWorksheet.Cells[Convert.ToInt32(param[2]), Convert.ToInt32(param[3])]).EntireColumn.ColumnWidth = (Convert.ToDouble(param[4]));
                    break;
                    //Линия X,Y
                case (196):
                    xlSheet = (excelWorkbook.Worksheets.get_Item(Convert.ToInt32(param[0])) as Excel.Worksheet);
                    xlSheet.PageSetup.BlackAndWhite = true;
                    xlSheet.Shapes.AddLine((float)Convert.ToDouble(param[1]), (float)Convert.ToDouble(param[2]), (float)Convert.ToDouble(param[3]), (float)Convert.ToDouble(param[4]));
                    break;
                    //Спрятать столбец
                case (197):
                    (excelWorksheet.Cells[1, Convert.ToInt32(param[0])] as Excel.Range).EntireColumn.Hidden = true;
                    //excelWorksheet.Columns.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])]).Hidden = true;
                    break;
                //Спрятать строку
                case (198):
                    (excelWorksheet.Cells[Convert.ToInt32(param[0]), 1] as Excel.Range).EntireRow.Hidden = true;
                    //excelWorksheet.Columns.get_Range(excelWorksheet.Cells[Convert.ToInt32(param[0]), Convert.ToInt32(param[1])]).Hidden = true;
                    break;
                case 199:
                    for (int i = 0; i < frm.specMacro.Count; i++)
                    {
                        if (frm.specMacro[i].FullText.ToLower() == param[2].Replace("$","").ToLower())
                        {
                            PutInRange(Convert.ToInt32(param[0]), Convert.ToInt32(param[1]), frm.specMacro[i].value); break;
                        }
                    }                    
                    break;
                case 200:
                    for (int i = 0; i < frm.specMacro.Count; i++)
                    {
                        if (frm.specMacro[i].FullText.ToLower() == param[2].Replace("$", "").ToLower())
                        {
                            PutInRangeWithSummaryRows(Convert.ToInt32(param[0]), Convert.ToInt32(param[1]), frm.specMacro[i].value); break;
                        }
                    }
                    break;
                case 201:
                    autoFitRange(int.Parse(param[0]), int.Parse(param[1]), int.Parse(param[2]), int.Parse(param[3]));
                    break;
                    
            }
            System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("ru-RU");
            Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
        }
        static string[] GetSumaryColumns(DataTable dt)
        {
            if (dt.Rows.Count == 0)
                return new string[0];
            var rez = dt.Columns.Cast<DataColumn>().Where(i => i.Prefix == "RowArea").Select(i => i.ColumnName).ToList();
            rez.Remove(rez.Last());
            return rez.ToArray();

        }
        static void PutInRangeWithSummaryRows(int topRow, int leftCol, object source)
        {
            if (!(source is DataTable))
                throw new NotImplementedException();
            else
            {
                var data = (source as DataTable).Copy();
                AddSummaryRows(ref data);
                PutInRange(topRow, leftCol, data);
            }
        }
        static void PutInRange(int topRow, int leftCol, object source)
        {
            var data = new object[0, 0];
            if (source is DataTable)
                data = (source as DataTable).ToArray();
            else
                data = source as object[,];
            if (data == null)
                throw new NullReferenceException();
            FillRange(topRow, leftCol, data);
        }
        static void AddSummaryRows(ref DataTable dt)
        {
            var SummaryColumns = GetSumaryColumns(dt);
            var rez = dt.AsEnumerable().GroupByMany(SummaryColumns).ToList();
            foreach (var sumRows in rez)
            {
                foreach (var sumRow in sumRows.listSummaryRows)
                {
                    if (dt.Rows.IndexOf(sumRow.Key) > -1)
                        dt.Rows.InsertAt(sumRow.Value, dt.Rows.IndexOf(sumRow.Key) + 1);
                }
            }
            int i = SummaryColumns.Length;
        }
        static void FillRange(int topRow, int leftCol, object[,] dt)
        {
            int rows = dt.GetUpperBound(0) + 1;
            int cols = dt.GetUpperBound(1) + 1;
            Excel.Worksheet sheet = (Excel.Worksheet)excelApp.ActiveSheet;
            object leftTop = excelWorksheet.Cells[topRow, leftCol];
            object rightBottom = excelWorksheet.Cells[topRow + rows - 1, leftCol + cols - 1];
            Excel.Range _range = excelWorksheet.Range[leftTop, rightBottom];
            _range.Value2 = dt;
            SetStyle(_range);
        }
        static void autoFitRange(int topRow, int leftCol, int botomRow, int rightCol)
        {
            object leftTop = excelWorksheet.Cells[topRow, leftCol];
            object rightBottom = excelWorksheet.Cells[topRow + botomRow, leftCol + rightCol];
            Excel.Range _range = excelWorksheet.Range[leftTop, rightBottom];
            //_range.EntireColumn.AutoFit();
            _range.EntireRow.AutoFit();
        }
        static void SetStyle(Excel.Range _range)
        {            
            object[] border = { Excel.XlBordersIndex.xlEdgeLeft,
                                             Excel.XlBordersIndex.xlEdgeTop,
                                             Excel.XlBordersIndex.xlEdgeBottom,
                                             Excel.XlBordersIndex.xlEdgeRight,
                                             Excel.XlBordersIndex.xlInsideVertical,
                                             Excel.XlBordersIndex.xlInsideHorizontal};
            foreach (object t in border)
            {
                _range.Borders[(Excel.XlBordersIndex)t].LineStyle = Excel.XlLineStyle.xlContinuous;
                _range.Borders[(Excel.XlBordersIndex)t].Weight = Excel.XlBorderWeight.xlThin;
                _range.Borders[(Excel.XlBordersIndex)t].ColorIndex = Excel.XlColorIndex.xlColorIndexAutomatic;
            }
            _range.EntireColumn.AutoFit();
            _range.EntireRow.AutoFit();
        }
        private static string GetExcelColumnName(int columnNumber)
        {
            int dividend = columnNumber;
            string columnName = String.Empty;
            int modulo;

            while (dividend > 0)
            {
                modulo = (dividend - 1) % 26;
                columnName = Convert.ToChar(65 + modulo).ToString() + columnName;
                dividend = (int)((dividend - modulo) / 26);
            }

            return columnName;
        }
        public static string GetOneCellStringData(int col, int row)
        {
            object leftTop = excelWorksheet.Cells[row, col];
            Excel.Range _range = excelWorksheet.Range[leftTop];
            string val = "";

            if (_range?.Value2 != null && string.IsNullOrWhiteSpace(_range.Value2.ToString()))
                val = _range.Value2.ToString();
            return val;
        }
        public static bool GetPermissionToExport(string pathOrFileName)
        {
            var permissionSet = new PermissionSet(PermissionState.None);
            var writePermission = new FileIOPermission(FileIOPermissionAccess.Write, pathOrFileName);
            permissionSet.AddPermission(writePermission);
            return permissionSet.IsSubsetOf(AppDomain.CurrentDomain.PermissionSet);
        }
        private static bool NotDenied(DirectorySecurity di)
        {
            var rules = di.GetAccessRules(true, true, typeof(System.Security.Principal.SecurityIdentifier));
            foreach (FileSystemAccessRule rule in rules)
            {
                if (FileSystemRights.Read==rule.FileSystemRights)
                {
                    MessageBox.Show("Нет доступа к папке");
                    return false;
                }
            }
            return true;
        }
    }
}
